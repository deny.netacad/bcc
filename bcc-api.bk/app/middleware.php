<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

// middleware untuk validasi api key
//$app->add(function ($request, $response, $next) {
//
//    $key = $request->getQueryParam("key");
//    $app_config = json_decode(file_get_contents(__DIR__ . '/app_config.json'), true) ;
//
//    if(!isset($key)){
//        return $response->withJson(["status" => "API Key required"], 401);
//    }
//
//    if($key == $app_config['key']){
//        return $response = $next($request, $response);
//    }
//
//    return $response->withJson(["status" => "Unauthorized"], 401);
//
//});

$app_setting = json_decode(file_get_contents(__DIR__ . '/app_config.json'), true) ;

//$container["JwtAuthentication"] = function ($container) {
//    return new Tuupola\Middleware\JwtAuthentication([
//        "path" => ["/"],
//        "ignore" => ["/login", "/info"],
//        "secret" => getenv("JWT_SECRET"),
//        "logger" => $container["logger"],
//        "attribute" => false,
//        "relaxed" => ["192.168.50.52", "127.0.0.1", "localhost"],
//        "error" => function ($response, $arguments) {
//            return new UnauthorizedResponse($arguments["message"], 401);
//        },
//        "before" => function ($request, $arguments) use ($container) {
//            $container["token"]->populate($arguments["decoded"]);
//        }
//    ]);
//};

$app->add(new Slim\Middleware\JwtAuthentication([
    "path" => ["/main_page", "/perusahaan", "/joblist"],
    "secret" => $app_setting['key'],
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "secure" => false,
    "before" => function ($request, $arguments) use ($container) {
        $container["token"] = ($arguments["decoded"]);
    },
    "error" => function ($response, $arguments) {
        $data["statusCode"] = 401;
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));
