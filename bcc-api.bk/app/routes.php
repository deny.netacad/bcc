<?php

//From PHPMailer
use Firebase\JWT\JWT;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Require autoload.php of PHPMailer
require_once '../vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

function checkToken(\Psr\Container\ContainerInterface $container, Request $request)
{
    if (!isset($request->getHeaders()['Authorization'])) {
        return null;
    }
    try {
        $authHeader = $request->getHeaders()['Authorization'];
        $tokenData = $authHeader[0];
        $arr = explode(" ", $tokenData);

        $jwt = $arr[1];
        $settings = $container->get('settings');
        $secret = $settings['jwt']['secret'];
        $alg = "HS256";

        $decodeData = JWT::decode($jwt, $secret, [$alg]);
        return $decodeData;
    } catch (Exception $exception) {
        return null;
    }

}
//$container['upload_directory_verifikasi'] = 'C:\\xampp\\htdocs\\sibos-api\\public\\image'; //<--- di server
$container['upload_directory_verifikasi'] = '../../admin/uploads/kk_ktp'; //<--- di server
$container['upload_directory_aduan'] = '../../admin/uploads/pegaduan'; //<--- di server
// Routes

$app->get("test", function (Request $request, Response $response) {
    return $response->withJson(["status" => "success"], 200);
});
$app->get('/', App\Action\HomeAction::class)
    ->setName('home');


//API
$app->post('/login', \App\Action\Auth\AuthAction::class);
$app->post('/registration', \App\Action\Auth\Registration::class);
$app->get('/check_username', \App\Action\Auth\CheckUsername::class);
$app->get('/pendidikan', \App\Action\Master\PendidikanAction::class);
$app->get('/provinsi', \App\Action\Provinsi\ProvinsiAction::class);
$app->get('/jenis_usaha', \App\Action\JenisUsaha\JenisUsahaAction::class);

$app->get('/kabko', \App\Action\KabupatenKota\ListAllKabkoAction::class);
$app->get('/kabko/{provinsi_id}', \App\Action\KabupatenKota\ListAllKabkoWhereProvinsiAction::class);

$app->get('/kecamatan', \App\Action\Kecamatan\KecamatanListAction::class);
$app->get('/kecamatan/{kabko_id}', \App\Action\Kecamatan\KecamatanListAction::class);


//restricted
$app->get('/main_page', \App\Action\MainPage\MainPageAction::class);
$app->get('/perusahaan/{id}', \App\Action\Perusahaan\PerusahaanAction::class);
$app->get('/joblist', App\Action\Job\JobListAction::class);
$app->get('/joblist/search', \App\Action\Job\JobListSearchAction::class);
$app->get('/joblist/search_tipe', \App\Action\Job\JobListSearchTipeAction::class);



function moveUploadedFile($directory, UploadedFile $uploadedFile, $filename)
{
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    return $filename;
}

$app->get('/forgotpassword', function (Request $request, Response $response) {
    $requestParamter = $request->getParsedBody();
    $email = $requestParamter['email'];
    $id = $requestParamter['id'];
    sendVerificationEmail($email, $id);

});


//Function to send mail,
function sendVerificationEmail($email, $id)
{
    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {


        $mail->Host = "smtp.gmail.com";
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->SMTPAuth = true;                                   // Enable SMTP authentication
        $mail->Username = 'dinsosbogorkab@gmail.com';                     // SMTP username
        $mail->Password = 'D1n5052020';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Recipients
        $mail->setFrom('dinsosbogorkab@gmail.com');
        $mail->addAddress('dihidrogen.monoksida@gmail.com');     // Add a recipient
//        $mail->addReplyTo($email);

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body = 'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

}

