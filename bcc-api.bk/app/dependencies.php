<?php
// DIC configuration
$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// database PDO
//pgsql:host=localhost;port=5432;dbname=pagila;user=postgres;password=postgres
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
    $server = $settings['driver'] . ":host=" . $settings['host'] . ";dbname=" . $settings['dbname'];
    $conn = new PDO($server, $settings["user"], $settings["pass"]);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $conn;
};


// Activating routes in a subfolder
$container['environment'] = function () {
    $scriptName = $_SERVER['SCRIPT_NAME'];
    $_SERVER['SCRIPT_NAME'] = dirname(dirname($scriptName)) . '/' . basename($scriptName);
    return new Slim\Http\Environment($_SERVER);
};

$container[App\Action\HomeAction::class] = function ($c) {
    return new App\Action\HomeAction();
};

$container[App\Action\Job\JobListAction::class] = function ($c) {
    return new App\Action\Job\JobListAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Auth\AuthAction::class] = function ($c) {
    return new App\Action\Auth\AuthAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Auth\Registration::class] = function ($c) {
    return new App\Action\Auth\Registration($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Auth\CheckUsername::class] = function ($c) {
    return new App\Action\Auth\CheckUsername($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Provinsi\ProvinsiAction::class] = function ($c) {
    return new App\Action\Provinsi\ProvinsiAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Provinsi\ProvinsiAction::class] = function ($c) {
    return new App\Action\Provinsi\ProvinsiAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Master\PendidikanAction::class] = function ($c) {
    return new App\Action\Master\PendidikanAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\JenisUsaha\JenisUsahaAction::class] = function ($c) {
    return new App\Action\JenisUsaha\JenisUsahaAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\KabupatenKota\ListAllKabkoAction::class] = function ($c) {
    return new \App\Action\KabupatenKota\ListAllKabkoAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\KabupatenKota\ListAllKabkoWhereProvinsiAction::class] = function ($c) {
    return new \App\Action\KabupatenKota\ListAllKabkoWhereProvinsiAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Kecamatan\KecamatanListAction::class] = function ($c) {
    return new \App\Action\Kecamatan\KecamatanListAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\MainPage\MainPageAction::class] = function ($c) {
    return new App\Action\MainPage\MainPageAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Perusahaan\PerusahaanAction::class] = function ($c) {
    return new App\Action\Perusahaan\PerusahaanAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Job\JobListAction::class] = function ($c) {
    return new App\Action\Job\JobListAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Job\JobListSearchAction::class] = function ($c) {
    return new App\Action\Job\JobListSearchAction($c->get('logger'), $c->get('db'), $c);
};
$container[App\Action\Job\JobListSearchTipeAction::class] = function ($c) {
    return new App\Action\Job\JobListSearchTipeAction($c->get('logger'), $c->get('db'), $c);
};
