<?php

$db_setting = json_decode(file_get_contents(__DIR__ . '/app_config.json'), true) ;

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        'jwt' => [
            'secret' => $db_setting['key']
        ],

        'db' => [
            'host' => $db_setting["host"],
            'user' => $db_setting['username'],
            'pass' => $db_setting['password'],
            'dbname' => $db_setting['database'],
            'driver' => 'mysql'
        ],

//        'db' => [
////            'host' => '103.157.96.26',
//            'host' => '35.187.151.230',
////            'host' => '10.140.0.3',
//            'user' => 'postgres',
//            'pass' => 'postgres',
////            'pass' => 'pass@word1',
//            'dbname' => 'db_sibos',
//            'driver' => 'pgsql'
//        ],
    ],
];
