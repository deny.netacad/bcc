<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 6/12/2021
 * Time: 8:04 AM
 */

namespace App\Domain\Job;


use Psr\Log\LoggerInterface;

class JobRepository
{

    /**
     * @var PDO The database connection
     */
    private $connection;
    private $logger;

    /**
     * Constructor.
     *
     * @param \PDO $connection The database connection
     * @param LoggerInterface $logger
     */
    public function __construct(\PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }


    public function count()
    {
        $sql = "SELECT count(*) as count
              FROM  ngi_joblist a 
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_jobcategory c on a.idcategory= c.id
              LEFT JOIN ngi_salary d on a.upah_kerja = d.id
              LEFT JOIN ngi_kecamatan e on a.idkecamatan= e.subdistrict_id";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetch();
        return $data['count'];
    }

    public function findAll(int $limit, int $offset)
    {
        $sql = "SELECT a.idperusahaan, a.tipe,a.id, a.title, a.deskripsi, a.date_start, a.date_end, b.nama_perusahaan,
              b.profile_url, c.category, a.upah_kerja, d.range as range_salary, d.name as name_salary , a.jml_pekerja,
              a.min_pendidikan,  e.city as kota, e.subdistrict_name as kecamatan, e.province as provinsi
              FROM  ngi_joblist a 
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_jobcategory c on a.idcategory= c.id
              LEFT JOIN ngi_salary d on a.upah_kerja = d.id
              LEFT JOIN ngi_kecamatan e on a.idkecamatan= e.subdistrict_id
          
            
              ORDER BY a.id desc limit {$limit} offset ${offset}  ";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }

    public function countByKeyword(String $keyword)
    {
        $sql = "SELECT count(*) as count
              FROM  ngi_joblist a 
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_jobcategory c on a.idcategory= c.id
              LEFT JOIN ngi_salary d on a.upah_kerja = d.id
              WHERE a.title like '%{$keyword}%' OR a.deskripsi  like '%{$keyword}%' 
              OR c.category  like '%{$keyword}%' OR b.nama_perusahaan  like '%{$keyword}%' ";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetch();
        return $data['count'];
    }

    public function findAllByKeyword(int $limit, int $offset, String $keyword)
    {
        $sql = "SELECT a.idperusahaan, a.tipe,a.id, a.title, a.deskripsi, a.date_start, a.date_end, b.nama_perusahaan,
              b.profile_url, c.category, a.upah_kerja, d.range as range_salary, d.name as name_salary 
              FROM  ngi_joblist a 
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_jobcategory c on a.idcategory= c.id
              LEFT JOIN ngi_salary d on a.upah_kerja = d.id
              WHERE a.title like '%{$keyword}%' OR a.deskripsi  like '%{$keyword}%' 
              OR c.category  like '%{$keyword}%' OR b.nama_perusahaan  like '%{$keyword}%' 
              
              ORDER BY a.id desc limit {$limit} offset ${offset}  ";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }


    public function countByCategory( String $keyword, String $tipe)
    {
        $sql = "SELECT count(*) as count
              FROM  ngi_joblist a 
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_jobcategory c on a.idcategory= c.id
              LEFT JOIN ngi_salary d on a.upah_kerja = d.id
              WHERE (a.title like '%{$keyword}%' OR a.deskripsi  like '%{$keyword}%' 
              OR b.nama_perusahaan  like '%{$keyword}%') AND  a.tipe = '{$tipe}'
              ";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetch();
        return $data['count'];
    }

    public function findAllByCategory(int $limit, int $offset,  String $keyword, String $tipe)
    {
        $sql = "SELECT a.idperusahaan, a.tipe,a.id, a.title, a.deskripsi, a.date_start, a.date_end, b.nama_perusahaan,
              b.profile_url, c.category, a.upah_kerja, d.range as range_salary, d.name as name_salary , a.qualification,
              a.responsibility, a.next_step
              FROM  ngi_joblist a 
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_jobcategory c on a.idcategory= c.id
              LEFT JOIN ngi_salary d on a.upah_kerja = d.id
              WHERE (a.title like '%{$keyword}%' OR a.deskripsi  like '%{$keyword}%' 
              OR b.nama_perusahaan  like '%{$keyword}%') AND a.tipe= '{$tipe}'
              
              ORDER BY a.id desc limit {$limit} offset ${offset}  ";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }


}