<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 27/02/2021
 * Time: 12:39
 */

namespace App\Domain\MainPage;


use Psr\Log\LoggerInterface;
use Slim\Logger;

class MainPageRespository
{
    /**
     * @var PDO The database connection
     */
    private $connection;
    private $logger;

    /**
     * Constructor.
     *
     * @param \PDO $connection The database connection
     * @param LoggerInterface $logger
     */
    public function __construct(\PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }


    public function getData()
    {
        $news = $this->getNews();
        $joblist = $this->getJoblist();
        $event = $this->getEvent();
        $data=['message' => 'OK','news'=>mb_convert_encoding($news, 'UTF-8', 'UTF-8'),
            'job_list'=>mb_convert_encoding($joblist, 'UTF-8', 'UTF-8'),
            'events'=> mb_convert_encoding($event, 'UTF-8', 'UTF-8')];
//        $data=['message' => 'OK','news'=>$news, 'job_list'=>$joblist, 'events'=>$event];
        return $data;
    }

    public function getJoblist()
    {
        $sql = "SELECT a.nama_perusahaan, a.idperusahaan, a.photo , (select count(*) as jml_lowongan from ngi_joblist b where b.id = a.idperusahaan) as jml_lowongan FROM ngi_perusahaan a
ORDER BY jml_lowongan desc limit 10";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }

    public function getNews()
    {
        $sql = "SELECT art_id, art_title, art_date, art_content, art_thumb FROM  section_article ORDER BY art_id desc limit 5 offset 0";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }

    public function getEvent()
    {
        $sql = "SELECT * FROM mast_event ORDER BY id desc limit 10 offset 0";
        $sth = $this->connection->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }

}