<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:25
 */

namespace App\Domain\PerwakilanDaerah;


use Psr\Log\LoggerInterface;

class PerwakilanDaerahRepository
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }


    public function findAll(int $offset = 0, int $limit = 10): array
    {


        $sql = "SELECT a.id, a.tingkat, a.nama, a.ketua, b.nama_lengkap as nama_ketua, a.alamat_kantor, a.telp, a.email, a.alamat_web,
          a.desa_id FROM perwakilan_pw a 
          left join pengamal b on a.ketua = b.id order by a.tingkat asc limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }


    public function countAll(): int
    {
        $sql = "SELECT count(*) as count FROM perwakilan_pw";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result['count'];

    }

    public function findById(int $id): object
    {
        $sql = "SELECT a.id, a.tingkat, a.nama, a.ketua, b.nama_lengkap as nama_ketua, a.alamat_kantor, a.telp,
          a.email, a.alamat_web, a.desa_id , c.nama as desa, d.nama as kecamatan,  d.id as kecamatan_id, e.nama as kabko FROM perwakilan_pw a 
          left join pengamal b on a.ketua = b.id 
          left join desa c on a.desa_id = c.id 
          left join kecamatan d on c.kecamatan_id = d.id 
          left join kabupaten_kota e on d.kabupaten_kota_id = e.id 
          where a.id=" . $id;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result;

    }


    public function sk_personil(int $idPerwakilan)
    {
        $sql = "SELECT 	id,	no_sk,	tgl_sk,	masa_khidmah,	
                berlaku_mulai,berlaku_sampai, aktif,	perwakilan_pw FROM sk_personil
              WHERE perwakilan_pw={$idPerwakilan} and aktif=true limit 1";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if (!$result) {
            return null;
        }
        return $result;

    }

    public function updateSK(array $sk): int
    {
        $sql = "UPDATE sk_personil SET no_sk=:no_sk,tgl_sk=:tgl_sk,masa_khidmah=:masa_khidmah,
          berlaku_mulai=:berlaku_mulai,berlaku_sampai=:berlaku_sampai,aktif=:aktif,perwakilan_pw=:perwakilan_pw WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "no_sk" => $sk['no_sk'],
            "tgl_sk" => $sk['tgl_sk'],
            "masa_khidmah" => $sk['masa_khidmah'],
            "berlaku_mulai" => $sk['berlaku_mulai'],
            "berlaku_sampai" => $sk['berlaku_sampai'],
            "aktif" => $sk['aktif'],
            "perwakilan_pw" => $sk['perwakilan_pw'],
            "id" => $sk['id']
        ];
        $stmt->execute($data);
        $result = $this->connection->lastInsertId();
        return $result;
    }

    public function saveSK(array $sk, LoggerInterface $logger): int
    {
        $sql = "INSERT INTO sk_personil(no_sk, tgl_sk, masa_khidmah, berlaku_mulai, berlaku_sampai, aktif, perwakilan_pw) 
            VALUES (:no_sk, :tgl_sk, :masa_khidmah, :berlaku_mulai, :berlaku_sampai, :aktif, :perwakilan_pw)";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "no_sk" => $sk['no_sk'],
            "tgl_sk" => $sk['tgl_sk'],
            "masa_khidmah" => $sk['masa_khidmah'],
            "berlaku_mulai" => $sk['berlaku_mulai'],
            "berlaku_sampai" => $sk['berlaku_sampai'],
            "aktif" => true,
            "perwakilan_pw" => $sk['perwakilan_pw']
        ];
        $isok = $stmt->execute($data);
        $result = $this->connection->lastInsertId();

        if ($isok) {
            $sql_update = "UPDATE sk_personil SET aktif=0 WHERE id != :id and perwakilan_pw = :perwakilan_pw";
            $data2 = [
                "id" => $result,
                "perwakilan_pw" => $sk['perwakilan_pw']
            ];
            $stmt2 = $this->connection->prepare($sql_update);
            $result = $stmt2->execute($data2);

            $logger->info('update sk ' . $result);
        }

        return $result;

    }


    public function updatePersonil(array $personil, LoggerInterface $logger): bool
    {
        $sql = "UPDATE personil_pw SET perwakilan_pw_id=:perwakilan_pw_id,
        unit_departemen_id=:unit_departemen_id],pengamal_id=:pengamal_id,
        jenis_jabatan=:jenis_jabatan,sk_personil=:sk_personil, no_urut=:no_urut WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "perwakilan_pw_id" => $personil['perwakilan_pw_id'],
            "unit_departemen_id" => $personil['unit_departemen_id'],
            "pengamal_id" => $personil['pengamal_id'],
            "jenis_jabatan" => $personil['jenis_jabatan'],
            "sk_personil" => $personil['sk_personil'],
            "no_urut" => $personil['no_urut'],
            "id" => $personil['id']
        ];
        return $stmt->execute($data);
    }


    public function deletePersonil(int $id, LoggerInterface $logger): int
    {
        $sql = "DELETE FROM personil_pw WHERE id = {$id}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $this->connection->lastInsertId();

        return $result;
    }

    public function savePersonil(array $personil, LoggerInterface $logger): int
    {
        $sql = "INSERT INTO personil_pw (perwakilan_pw_id, unit_departemen_id, pengamal_id, jenis_jabatan, sk_personil, no_urut)
              VALUES (:perwakilan_pw_id,:unit_departemen_id,:pengamal_id,:jenis_jabatan,:sk_personil, :no_urut)";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "perwakilan_pw_id" => $personil['perwakilan_pw_id'],
            "unit_departemen_id" => $personil['unit_departemen_id'],
            "pengamal_id" => $personil['pengamal_id'],
            "jenis_jabatan" => $personil['jenis_jabatan'],
            "sk_personil" => $personil['sk_personil'],
            "no_urut" => $personil['no_urut']
        ];
        $stmt->execute($data);
        $result = $this->connection->lastInsertId();

        return $result;
    }


    public function personilById(int $id)
    {
        $sql = "SELECT a.id,  a.perwakilan_pw_id,  a.unit_departemen_id, d.nama as unit,  a.jenis_jabatan, 
              a.pengamal_id,  a.jenis_jabatan, b.nama as jabatan, c.nama_lengkap as nama_personil ,
              c.no_hp FROM personil_pw a
              left join jenis_jabatan b on a.jenis_jabatan = b.id
              left join unit_departemen d on a.unit_departemen_id = d.id
              left join pengamal c on a.pengamal_id = c.id
              WHERE a.id={$id} order by a.no_urut asc ";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result;

    }


    public function personil(int $idPerwakilan): array
    {
        $sql = "SELECT a.id,  a.perwakilan_pw_id,  a.unit_departemen_id, d.nama as unit,  a.jenis_jabatan, 
              a.pengamal_id,  a.jenis_jabatan, b.nama as jabatan, c.nama_lengkap as nama_personil ,
              c.no_hp FROM personil_pw a
              left join jenis_jabatan b on a.jenis_jabatan = b.id
              left join unit_departemen d on a.unit_departemen_id = d.id
              left join pengamal c on a.pengamal_id = c.id
              WHERE a.perwakilan_pw_id={$idPerwakilan} order by a.no_urut asc ";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;

    }

    public function save(array $perwakilan): int
    {
        $sql = "INSERT INTO perwakilan_pw (tingkat, nama, ketua, alamat_kantor, telp,
                 email, alamat_web, desa_id) VALUES 
                 (:tingkat, :nama, :ketua, :alamat_kantor, :telp,
                 :email, :alamat_web, :desa_id)";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "tingkat" => $perwakilan['tingkat'],
            "nama" => $perwakilan['nama'],
            "ketua" => $perwakilan['ketua'],
            "alamat_kantor" => $perwakilan['alamat_kantor'],
            "telp" => $perwakilan['telp'],
            "email" => $perwakilan['email'],
            "alamat_web" => $perwakilan['alamat_web'],
            "desa_id" => $perwakilan['desa_id']
        ];

        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }

    public function update(array $perwakilan): int
    {
        $sql = "UPDATE perwakilan_pw SET 
              tingkat=:tingkat,nama=:nama,
              ketua=:ketua,alamat_kantor=:alamat_kantor,
              telp=:telp,email=:email,alamat_web=:alamat_web,
              desa_id=:desa_id WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "tingkat" => $perwakilan['tingkat'],
            "nama" => $perwakilan['nama'],
            "ketua" => $perwakilan['ketua'],
            "alamat_kantor" => $perwakilan['alamat_kantor'],
            "telp" => $perwakilan['telp'],
            "email" => $perwakilan['email'],
            "alamat_web" => $perwakilan['alamat_web'],
            "desa_id" => $perwakilan['desa_id'],
            "id" => $perwakilan['id']
        ];

        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }


}