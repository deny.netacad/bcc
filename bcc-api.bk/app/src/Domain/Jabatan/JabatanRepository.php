<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 21:52
 */

namespace App\Domain\Jabatan;


use Monolog\Logger;
use Psr\Log\LoggerInterface;


class JabatanRepository
{

    private $connection;
    private $logger;

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger): void
    {
        $this->logger = $logger;
    }


    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function findAll(int $offset = 0, int $limit = 50): array
    {
        $sql = "select * from jenis_jabatan limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

}