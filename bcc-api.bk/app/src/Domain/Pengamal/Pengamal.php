<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 27/02/2021
 * Time: 10:41
 */

namespace App\Domain\Pengamal;


class Pengamal
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }


    public function findAll(int $offset = 0, int $limit = 10): array
    {
        $sql = "SELECT * FROM pengamal limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function countAll(): int
    {
        $sql = "SELECT count(*) as count FROM pengamal";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result['count'];
    }

    public function findAllByNama(string $nama, int $offset = 0, int $limit = 100): array
    {
        $sql = "SELECT * FROM pengamal WHERE nama_lengkap like :nama limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "nama" => '%'.$nama.'%'
        ];
        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function findByIdDetail(int $id): object
    {
        $sql = "SELECT  a.id, a.nama_lengkap, a.alamat,
              a.nik, a.tempat_lahir, a.tgl_lahir, a.jenis_kelamin, a.desa_id, a.kelompok_jamaah, a.no_hp, d.id as kabko_id, d.nama as kabko,
              c.id as kecamatan_id, c.nama as kecamatan, b.nama as desa FROM pengamal a  
              left join desa b on a.desa_id = b.id 
              left join kecamatan c on b.kecamatan_id = c.id 
              left join kabupaten_kota d on c.kabupaten_kota_id = d.id 
              WHERE a.id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "id" => $id
        ];
        $stmt->execute($data);
        $result = $stmt->fetchObject();
        return $result;
    }

    public function findById(int $id): object
    {
        $sql = "SELECT * FROM pengamal WHERE id=:id ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "id" => $id
        ];
        $stmt->execute($data);
        $result = $stmt->fetchObject();
        return $result;
    }

    public function findByDesaId(int $desaId): array
    {
        $sql = "SELECT * FROM pengamal WHERE desa_id=:desa_id ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "desa_id" => $desaId
        ];
        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;
    }


    public function findByDesaIdForJamaah(int $desaId): array
    {
        $sql = "SELECT * FROM pengamal WHERE desa_id=:desa_id and jamaah_id is null";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "desa_id" => $desaId
        ];
        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function save(array $pengamal): int
    {
        $sql = "INSERT INTO pengamal (nama_lengkap, alamat, 
                nik, tempat_lahir, tgl_lahir, jenis_kelamin, 
                desa_id, kelompok_jamaah, no_hp, jamaah_id) VALUES 
                (:nama_lengkap,:alamat,:nik,:tempat_lahir,:tgl_lahir,:jenis_kelamin,:desa_id,
                :kelompok_jamaah,:no_hp, :jamaah_id) ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "nama_lengkap" => $pengamal['nama_lengkap'],
            "alamat" => $pengamal['alamat'],
            "nik" => $pengamal['nik'],
            "tempat_lahir" => $pengamal['tempat_lahir'],
            "tgl_lahir" => $pengamal['tgl_lahir'],
            "jenis_kelamin" => $pengamal['jenis_kelamin'],
            "desa_id" => $pengamal['desa_id'],
            "kelompok_jamaah" => $pengamal['kelompok_jamaah'],
            "no_hp" => $pengamal['no_hp'],
            "jamaah_id" => $pengamal['jamaah_id']
        ];


        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }

    public function update(array $pengamal, int $id): bool
    {
        $sql = "UPDATE pengamal SET nama_lengkap=:nama_lengkap, alamat=:alamat,
              nik=:nik, tempat_lahir=:tempat_lahir,tgl_lahir=:tgl_lahir,
              jenis_kelamin=:jenis_kelamin, desa_id =:desa_id, kelompok_jamaah=:kelompok_jamaah, no_hp=:no_hp 
              WHERE  id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "nama_lengkap" => $pengamal['nama_lengkap'],
            "alamat" => $pengamal['alamat'],
            "nik" => $pengamal['nik'],
            "tempat_lahir" => $pengamal['tempat_lahir'],
            "tgl_lahir" => $pengamal['tgl_lahir'],
            "jenis_kelamin" => $pengamal['jenis_kelamin'],
            "desa_id" => $pengamal['desa_id'],
            "kelompok_jamaah" => $pengamal['kelompok_jamaah'],
            "no_hp" => $pengamal['no_hp'],
            "id" => $id
        ];

        $success = $stmt->execute($data);
        return $success;
    }


    public function updatePengamalJamaah(int $jamaah_id, array $ids): bool
    {
        $sql = "UPDATE pengamal SET jamaah_id=:jamaah_id
              WHERE  id=:id";
        $stmt = $this->connection->prepare($sql);
        foreach ($ids as $id) {
            $data = [
                "jamaah_id" => $jamaah_id == 0 ? null : $jamaah_id,
                "id" => $id['id']
            ];
            $stmt->execute($data);
        }

        return true;
    }

    public function delete(int $id): bool
    {
        $sql = "DELETE FROM pengamal  WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'id' => $id,
        ];

        $status = $stmt->execute($data);
        return $status;
    }
}