<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 06/04/2021
 * Time: 11:33
 */

namespace App\Domain\Role;


class RoleRepository
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }


    public function findAll($offset, $limit): array
    {
        $sql = "SELECT * FROM role limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }

    public function countAll() : int
    {
        $sql = "SELECT count(*) as count FROM role";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch()['count'];
        return $result;

    }


    public function findById(int $id): object
    {
        $sql = "SELECT * FROM role where id={$id}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result;

    }
    public function save(array $role): int
    {
        $sql = "INSERT INTO role (nama, kode) VALUES 
                (:nama,:kode) ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "nama" => $role['nama'],
            "kode" => $role['kode']
        ];


        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }


    public function update(array $role): bool
    {
        $sql = "UPDATE role set nama=:nama, kode=:kode WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "nama" => $role['nama'],
            "kode" => $role['kode'],
            "id" => $role['id']
        ];

        $result = $stmt->execute($data);
        return $result;
    }

    public function delete(int $id): bool
    {
        $sql = "DELETE from  role WHERE id={$id}";
        $stmt = $this->connection->prepare($sql);

        $reult =  $stmt->execute();
        return $reult;
    }
}