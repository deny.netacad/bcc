<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 15:51
 */

namespace App\Domain\Kecamatan\Repository;

use PDO;
use Psr\Log\LoggerInterface;

class KecamatanRepository
{
    private $connection;


    /**
     * KecamatanRepository constructor.
     * @param PDO $connection
     * @param LoggerInterface $logger
     */
    public function __construct(PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
    }


    /**
     * @return array
     */
    public function listAll()
    {
        $sql = "SELECT * FROM ngi_kecamatan";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    /**
     * @param $kabkoId
     * @return array
     */
    public function listAllWhereKabko($kabkoId)
    {
        $sql = "SELECT * FROM ngi_kecamatan where city_id={$kabkoId}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
}