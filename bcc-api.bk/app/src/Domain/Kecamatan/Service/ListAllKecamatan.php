<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 19:37
 */

namespace App\Domain\Kecamatan\Service;


use App\Domain\Kecamatan\Repository\KecamatanRepository;

class ListAllKecamatan
{
    /**
     * @var UserCreatorRepository
     */
    private $repository;

    public function __construct(KecamatanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findAll(): array
    {

        return $this->repository->listAll();
    }

}