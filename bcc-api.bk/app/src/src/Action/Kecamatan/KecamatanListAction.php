<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 16:19
 */

namespace App\Action\Kecamatan;


use App\Action\Action;
use App\Domain\Kecamatan\Repository\KecamatanRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;


class KecamatanListAction extends Action
{

    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        parent::__construct($logger, $connection, $container);
    }


    protected function action()
    {
        $kecamatanRepository = new KecamatanRepository($this->connection, $this->logger);
        if (isset($this->args['kabko_id'])) {
            $kabkoId = (int)$this->args['kabko_id'];
            $result = $kecamatanRepository->listAllWhereKabko($kabkoId);
        } else {
            $result = $kecamatanRepository->listAll();
        }

        return $this->respondWithData($result, 200);
    }
}