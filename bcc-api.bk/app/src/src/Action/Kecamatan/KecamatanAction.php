<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 15:59
 */

namespace App\Action\Kecamatan;


use App\Action\Action;
use App\Action\AuthencticatedAction;
use Psr\Log\LoggerInterface;
use App\Domain\Kecamatan\Repository\KecamatanRepository;
use Slim\App;


abstract class KecamatanAction extends AuthencticatedAction
{

    public $kecamatanRepository;

    /**
     * KecamatanAction constructor.
     * @param LoggerInterface $logger
     * @param KecamatanRepository $kecamatanRepository
     * @param App $app
     */
    public function __construct(LoggerInterface $logger, KecamatanRepository $kecamatanRepository, App $app)
    {
        parent::__construct($logger, $app);
        $this->kecamatanRepository = $kecamatanRepository;
    }


}