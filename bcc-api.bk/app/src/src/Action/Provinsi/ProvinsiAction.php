<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:33
 */

namespace App\Action\Provinsi;


use App\Action\Action;
use App\Domain\Provinsi\ProvinsiRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ProvinsiAction
{

    private $container;
    private $connection;
    private $logger;

    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        $provinsiRepository = new ProvinsiRepository($this->connection, $this->logger);
        $result = $provinsiRepository->findAll();
        $this->logger->info("result " . json_encode($result));
        return $response->withJson(['statusCode' => 200, 'data' => $result]);
    }
}