<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 21:00
 */

namespace App\Action\Provinsi;

use App\Action\AuthencticatedAction;
use Psr\Log\LoggerInterface;
use Slim\App;

abstract class AbsProvinsiAction extends AuthencticatedAction
{
    public function __construct(LoggerInterface $logger, App $app)
    {
        parent::__construct($logger, $app);
    }

}