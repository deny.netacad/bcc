<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 29/05/2021
 * Time: 18:26
 */

namespace App\Action\Auth;


use App\Domain\User\Repository\UserRespository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class CheckUsername
{
    private $container;
    private $connection;
    private $logger;

    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->container = $container;
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        $userRepository = new UserRespository($this->connection, $this->logger);
        $username = $request->getQueryParams()['username'];

        $countUsername = $userRepository->heckUsername($username) + $userRepository->heckUsernamePerusahaan($username);
        $is_exixst = $countUsername > 0;

        $result = ['is_exist' => $is_exixst];
        $response->getBody()->write(json_encode($result));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);

    }

}