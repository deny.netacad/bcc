<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:03
 */

namespace App\Action\Desa;


use App\Action\Action;
use App\Action\AuthencticatedAction;
use App\Domain\Desa\DesaRepository;
use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Exception\HttpBadRequestException;

class DesaAction extends AuthencticatedAction
{

    private $desaRepository;

    public function __construct(LoggerInterface $logger, DesaRepository $desaRepository,  App $app)
    {
        parent::__construct($logger, $app);
        $this->desaRepository = $desaRepository;
    }

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
//        parent::setTokenData();
//        if (!$this->isAllowedUser()) {
//            return $this->notAllowedMessage();
//        }

        $params = $this->request->getQueryParams();

        if (isset($params['kabko_id'])) {
            $kabkoId = (int)$params['kabko_id'];
            $this->logger->info('kabko id ' . $kabkoId);
            $result = $this->desaRepository->findAllByKabupatenKotaId($kabkoId);
            return $this->respondWithData($result);
        } elseif (isset($params['kecamatan_id'])) {
            $kecamatanId = (int)$params['kecamatan_id'];
            $result = $this->desaRepository->findAllByKecamatanId($kecamatanId);
            return $this->respondWithData($result);
        } elseif (isset($params['nama'])) {
            $nama = $params['nama'];
            $result = $this->desaRepository->findAllByNama($nama);
            return $this->respondWithData($result);
        } elseif (isset($params['id'])) {
            $id = (int)$params['id'];
            $result = $this->desaRepository->findById($id);
            return $this->respondWithData($result);
        } else {
            $result = $this->desaRepository->findAll();
            return $this->respondWithData($result);
        }
    }
}