<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:33
 */

namespace App\Action\Perusahaan;


use App\Action\Action;
use App\Action\AuthencticatedAction;
use App\Domain\User\Repository\UserRespository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class PerusahaanAction extends Action
{

    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        parent::__construct($logger, $connection, $container);
    }


    protected function action()
    {
        $userRespository = new UserRespository($this->connection, $this->logger);
        $result = $userRespository->findPerusahaanbyId($this->args['id']);
        $this->logger->info("result " . json_encode($result));
        return $this->respondWithData($result, 200);
    }
}