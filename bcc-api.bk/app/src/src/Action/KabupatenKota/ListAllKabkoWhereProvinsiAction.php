<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 15:38
 */

namespace App\Action\KabupatenKota;


use App\Action\Action;
use App\Domain\KabKo\Repository\KabupatenKotaRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class ListAllKabkoWhereProvinsiAction extends Action
{
   public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
   {
       parent::__construct($logger, $connection, $container);
   }

    protected function action()
    {
        $allKabupatenKotaService = new KabupatenKotaRepository($this->connection, $this->logger);
        $result = $allKabupatenKotaService->listAllKabupateKotaWhereProvinsi($this->args['provinsi_id']);
        return $this->respondWithData($result, 200);
    }
}