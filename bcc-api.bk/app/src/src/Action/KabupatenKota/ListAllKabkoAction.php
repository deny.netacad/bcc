<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 15:38
 */

namespace App\Action\KabupatenKota;


use App\Domain\KabKo\Repository\KabupatenKotaRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ListAllKabkoAction
{

    private $container;
    private $connection;
    private $logger;

    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args)
    {

        $allKabupatenKotaService = new KabupatenKotaRepository($this->connection, $this->logger);
        $result = $allKabupatenKotaService->listAllKabupateKota();
        return $response->withJson(['statusCode' => 200, 'data' => $result]);
    }
}