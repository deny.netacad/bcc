<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 22:34
 */

namespace App\Action\Pengamal;


use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\Pengamal\Pengamal;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

class PengamalCreatorAction extends PengamalAction
{

    private $repository;

    public function __construct(LoggerInterface $logger, Pengamal $repository)
    {
        parent::__construct($logger, $repository);
        $this->repository = $repository;
    }


    private function validateNewPengamal(array $data): void
    {
        $errors = [];

        // Here you can also use your preferred validation library

        if (empty($data['nama_lengkap'])) {
            $errors['nama_lengkap'] = 'Input required nama_lengkap';
        }

        if (empty($data['alamat'])) {
            $errors['alamat'] = 'Input required alamat';
        }

        if (empty($data['jenis_kelamin'])) {
            $errors['jenis_kelamin'] = 'Input required jenis_kelamin (\'laki-laki\', \'perempuan\')';
        }

        if (empty($data['desa_id'])) {
            $errors['desa_id'] = 'Input required desa_id';
        }

        if (empty($data['kelompok_jamaah'])) {
            $errors['kelompok_jamaah'] = 'Input required kelompok_jamaah (\'bapak\', \'ibu\', \'remaja\', \'kanak-kanak\')';
        }

        /*elseif (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = 'Invalid email address';
        }*/

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        $data = (array)$this->request->getParsedBody();
        // TODO: Implement action() method.

        $this->validateNewPengamal($data);

        $dataId = $this->repository->save($data);

        // Transform the result into the JSON representation
        $result = [
            'jamaah_id' => $dataId
        ];

        // Build the HTTP response
        $this->response->getBody()->write((string)json_encode($result));

        return $this->response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }
}