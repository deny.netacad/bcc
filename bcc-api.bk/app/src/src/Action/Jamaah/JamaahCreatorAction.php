<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 22:34
 */

namespace App\Action\Jamaah;


use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\Jamaah\JamaahRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Exception\HttpBadRequestException;

class JamaahCreatorAction extends JamaahAction
{

    private $repository;

    public function __construct(LoggerInterface $logger, JamaahRepository $repository, App $app)
    {
        parent::__construct($logger, $app);
        $this->repository = $repository;
    }


    private function validateNewJamaah(array $data): void
    {
        $errors = [];

        // Here you can also use your preferred validation library

        if (empty($data['nama'])) {
            $errors['nama'] = 'Input required';
        }

        if (empty($data['imam_jamaah'])) {
            $errors['imam_jamaah'] = 'Input required';
        }

        if (empty($data['jenis'])) {
            $errors['jenis'] = 'Input required';
        }

        if (empty($data['desa_id'])) {
            $errors['desa_id'] = 'Input required';
        }


        /*elseif (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = 'Invalid email address';
        }*/

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {

        parent::setTokenData();
        if (!$this->isAllowedUser()) {
            return $this->notAllowedMessage();
        }

        $data = (array)$this->request->getParsedBody();
        // TODO: Implement action() method.

        $this->validateNewJamaah($data);

        $dataId = $this->repository->save($data);

        // Transform the result into the JSON representation
        $result = [
            'jamaah_id' => $dataId
        ];

        // Build the HTTP response
        $this->response->getBody()->write((string)json_encode($result));

        return $this->response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }
}