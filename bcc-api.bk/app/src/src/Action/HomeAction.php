<?php

namespace App\Action;

use Slim\Http\Request;
use Slim\Http\Response;

class HomeAction
{
    public function __invoke(Request $request, Response $response, $args)
    {
        $response->getBody()->write(json_encode(['status' => 'ok']));
        return $response;
    }
}
