<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 15:33
 */

namespace App\Domain\KabKo\Repository;

use PDO;
use Psr\Log\LoggerInterface;

class KabupatenKotaRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;
    private $logger;


    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     * @param LoggerInterface $logger
     */
    public function __construct(PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }


    /**
     * Insert user row.
     *
     * @param array $user The user
     *
     * @return int The new ID
     */
    public function listAllKabupateKota()
    {
        $sql = "SELECT * FROM ngi_kota";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    /**
     * @param int $idProvinsi
     * @return array
     */
    public function listAllKabupateKotaWhereProvinsi(int $idProvinsi)
    {
        $sql = "SELECT * FROM ngi_kota where province_id = {$idProvinsi}";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
}