<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:25
 */

namespace App\Domain\PerwakilanDaerah;


use Psr\Log\LoggerInterface;

class SKPersonilRepository
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }


    public function findAll(int $offset = 0, int $limit = 10): array
    {
        $sql = "SELECT id, no_sk, tgl_sk, masa_khidmah, berlaku_mulai, berlaku_sampai, aktif, perwakilan_pw FROM sk_personil order by id desc limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }



}