<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 15:56
 */

namespace App\Domain\Kecamatan\Service;


use App\Domain\Kecamatan\Repository\KecamatanRepository;

class ListKecamatanDiKabupatenKotaService
{
    /**
     * @var UserCreatorRepository
     */
    private $repository;

    public function __construct(KecamatanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listAllKecamatanDiKabupatenKota(int $kabkoId):array
    {
        $data = $this->repository->listAllWhereKabko($kabkoId);
        return $data;
    }
}