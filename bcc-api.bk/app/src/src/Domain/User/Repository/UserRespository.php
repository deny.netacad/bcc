<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 27/02/2021
 * Time: 12:39
 */

namespace App\Domain\User\Repository;


use Psr\Log\LoggerInterface;
use Slim\Logger;

class UserRespository
{
    /**
     * @var PDO The database connection
     */
    private $connection;
    private $logger;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(\PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }


    public function auth(array $input)
    {
        $sql = "SELECT * FROM user_pelamar WHERE username=:username or email=:username";
        $sth = $this->connection->prepare($sql);
        $sth->bindParam("username", $input['username']);
        $sth->execute();
        $user = $sth->fetchObject();

        $this->logger->info("data " . json_encode($user));
        if ($user)
            return $user;
        else {
            $sql = "SELECT * FROM ngi_perusahaan WHERE username=:username or email=:username";
            $sth = $this->connection->prepare($sql);
            $sth->bindParam("username", $input['username']);
            $sth->execute();
            $user = $sth->fetchObject();

            $this->logger->info("data user perusahaan " . json_encode($user));
            if ($user)
                return $user;
            else
                return null;
        }

    }

    /**
     * @param array $perusahaan
     * @return int
     */
    public function savePerusahaan(array $perusahaan)
    {
        $sql = "INSERT INTO ngi_perusahaan (username, pass, pass_ori, nama_perusahaan, alamat, email, telp, lokasi, jenisusaha, 
                noktpdir, nm_dir, photo, flag, reason, log_pengajuan, province_id, kab_id, fasilitas, kec_id, profile_url, no_hp, 
                link_enkripsi, verified, verified_disnaker, announce, ttg_anda, tahun, program, magang, limbah)
                VALUES (:username, :pass, :pass_ori, :nama_perusahaan, :alamat, :email, :telp, :lokasi, :jenisusaha, :noktpdir, 
                :nm_dir, :photo, :flag, :reason, :log_pengajuan, :province_id, :kab_id, :fasilitas, :kec_id, :profile_url, 
                :no_hp, :link_enkripsi, :verified, :verified_disnaker, :announce, :ttg_anda, :tahun, :program, :magang, :limbah)";


        $stmt = $this->connection->prepare($sql);

        $data = ['username' => $perusahaan['username'],
            'pass' => MD5($perusahaan['pass']),
            'pass_ori' => MD5($perusahaan['pass']),
            'nama_perusahaan' => $perusahaan['nama_perusahaan'],
            'alamat' => isset($perusahaan['alamat']) ? $perusahaan['alamat'] : null,
            'email' => $perusahaan['email'],
            'telp' => $perusahaan['telp'],
            'lokasi' => isset($perusahaan['lokasi']) ? $perusahaan['lokasi'] : null,
            'jenisusaha' => isset($perusahaan['jenisusaha']) ? $perusahaan['jenisusaha'] : null,
            'noktpdir' => $perusahaan['noktpdir'],
            'nm_dir' => $perusahaan['nm_dir'],
            'photo' => isset($perusahaan['photo']) ? $perusahaan['photo'] : null,
            'flag' => isset($perusahaan['flag']) ? $perusahaan['flag'] : null,
            'reason' => isset($perusahaan['reason']) ? $perusahaan['reason'] : null,
            'log_pengajuan' => isset($perusahaan['log_pengajuan']) ? $perusahaan['log_pengajuan'] : null,
            'province_id' => isset($perusahaan['province_id']) ? $perusahaan['province_id'] : null,
            'kab_id' => isset ($perusahaan['kab_id']) ? $perusahaan['kab_id'] : null,
            'fasilitas' => isset ($perusahaan['fasilitas']) ? $perusahaan['fasilitas'] : null,
            'kec_id' => isset ($perusahaan['kec_id']) ? $perusahaan['kec_id'] : null,
            'profile_url' => isset ($perusahaan['profile_url']) ? $perusahaan['profile_url'] : null,
            'no_hp' => isset($perusahaan['no_hp']) ? $perusahaan['no_hp'] : null,
            'link_enkripsi' => isset($perusahaan['link_enkripsi']) ? $perusahaan['link_enkripsi'] : null,
            'verified' => false,
            'verified_disnaker' => false,
            'announce' => isset ($perusahaan['announce']) ? $perusahaan['announce'] : null,
            'ttg_anda' => isset($perusahaan['ttg_anda']) ? $perusahaan['ttg_anda'] : null,
            'tahun' => isset ($perusahaan['tahun']) ? $perusahaan['tahun'] : null,
            'program' => isset ($perusahaan['program']) ? $perusahaan['program'] : null,
            'magang' => isset($perusahaan['magang']) ? $perusahaan['magang'] : null,
            'limbah' => isset ($perusahaan['limbah']) ? $perusahaan['limbah'] : null];
        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }

    public function savePelamar(array $pelamar)
    {
        $sql = "INSERT INTO user_pelamar (username, email, password, password_ori, nama, no_ktp, no_telp, 
         register, tmp_lahir, tgl_lahir, gender, agama, wn, sts, pendidikan, ins_pend, jurusan, tahun_lulus, ttg_anda, skill, 
         education, exp, province_id, city_id, kecamatan_id, link_enkripsi, verified, profile_url, photo, alamat, subs_email) 
         VALUES (:username, :email, :password, :password_ori, :nama, :no_ktp, :no_telp, :register, :tmp_lahir, :tgl_lahir,
        :gender, :agama, :wn, :sts, :pendidikan, :ins_pend, :jurusan, :tahun_lulus, :ttg_anda, :skill, :education, :exp, 
           :province_id, :city_id, :kecamatan_id, :link_enkripsi, :verified, :profile_url, :photo, :alamat, :subs_email)";


        $stmt = $this->connection->prepare($sql);

        $data = [
            'username' => $pelamar['username'],
            'email' => $pelamar['email'],
            'password' => MD5($pelamar['password']),
            'password_ori' => MD5($pelamar['password']),
            'nama' => $pelamar['nama'],
            'no_ktp' => $pelamar['no_ktp'],
            'no_telp' => isset($pelamar['no_telp']) ? $pelamar['no_telp'] : null,
            'register' => isset($pelamar['register']) ? $pelamar['register'] : null,
            'tmp_lahir' => isset($pelamar['tmp_lahir']) ? $pelamar['tmp_lahir'] : null,
            'tgl_lahir' => isset($pelamar['tgl_lahir']) ? $pelamar['tgl_lahir'] : null,
            'gender' => isset($pelamar['gender']) ? $pelamar['gender'] : null,
            'agama' => isset($pelamar['agama']) ? $pelamar['agama'] : null,
            'wn' => isset($pelamar['wn']) ? $pelamar['wn'] : null,
            'sts' => isset($pelamar['sts']) ? $pelamar['sts'] : null,
            'pendidikan' => isset($pelamar['pendidikan']) ? $pelamar['pendidikan'] : null,
            'ins_pend' => isset($pelamar['ins_pend']) ? $pelamar['ins_pend'] : null,
            'jurusan' => isset($pelamar['jurusan']) ? $pelamar['jurusan'] : null,
            'tahun_lulus' => isset($pelamar['tahun_lulus']) ? $pelamar['tahun_lulus'] : null,
            'ttg_anda' => isset($pelamar['ttg_anda']) ? $pelamar['ttg_anda'] : null,
            'skill' => isset($pelamar['skill']) ? $pelamar['skill'] : null,
            'education' => isset($pelamar['education']) ? $pelamar['education'] : null,
            'exp' => isset($pelamar['exp']) ? $pelamar['exp'] : null,
            'province_id' => isset($pelamar['province_id']) ? $pelamar['province_id'] : null,
            'city_id' => isset($pelamar['city_id']) ? $pelamar['city_id'] : null,
            'kecamatan_id' => isset($pelamar['kecamatan_id']) ? $pelamar['kecamatan_id'] : null,
            'link_enkripsi' => isset($pelamar['link_enkripsi']) ? $pelamar['link_enkripsi'] : null,
            'verified' => false,
            'profile_url' => isset($pelamar['profile_url']) ? $pelamar['profile_url'] : null,
            'photo' => isset($pelamar['photo']) ? $pelamar['photo'] : null,
            'alamat' => isset($pelamar['alamat']) ? $pelamar['alamat'] : null,
            'subs_email' => isset($pelamar['subs_email']) ? $pelamar['subs_email'] : null
        ];
        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }


    public function findPerusahaanbyId( $id)
    {
        $sql = "select  nama_perusahaan, alamat, email, telp, lokasi, jenisusaha, 
                noktpdir, nm_dir, photo, flag, reason, log_pengajuan, province_id, kab_id, fasilitas, kec_id, profile_url, no_hp, 
                link_enkripsi, announce, ttg_anda, tahun, program, magang, limbah from  ngi_perusahaan where idperusahaan = {$id}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result;
    }

    public function checkNikPelamar( $nik)
    {
        $sql = "select count(*) as count  from  user_pelamar where no_ktp = {$nik}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result->count;
    }

    public function heckUsername($username)
    {
        $sql = "select count(*) as count  from  user_pelamar where username = :username";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['username' => $username]);
        $result = $stmt->fetchObject();
        return $result->count;
    }

    public function heckUsernamePerusahaan( $username)
    {
        $sql = "select count(*) as count  from  ngi_perusahaan where username = :username";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['username' => $username]);
        $result = $stmt->fetchObject();
        return $result->count;
    }
}