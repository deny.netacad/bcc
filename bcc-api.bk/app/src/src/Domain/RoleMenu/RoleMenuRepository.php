<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 06/04/2021
 * Time: 15:48
 */

namespace App\Domain\RoleMenu;


class RoleMenuRepository
{  private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }



    public function findAll(): array
    {
        $sql = "SELECT a.id,a.menu_id, b.menu, b.is_root, b.icon, b.no_urut, a.access_read, a.access_write, a.access_delete, a.access_approve, a.role_id FROM role_menu a 
                left join menu b on b.id = a.menu_id
                order by b.no_urut asc ";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }

    public function save(array $role): int
    {
        $sql = "INSERT INTO role_menu (menu_id, role_id, access_read, access_write, access_delete, access_approve) VALUES 
                (:menu_id, :role_id, :access_read, :access_write, :access_delete, :access_approve) ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "menu_id" => $role['menu_id'],
            "role_id" => $role['role_id'],
            "access_read" => $role['access_read'],
            "access_write" => $role['access_write'],
            "access_delete" => $role['access_delete'],
            "access_approve" => $role['access_approve']
        ];


        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }


    public function update(array $role): bool
    {
        $sql = "UPDATE menu set menu=:menu, root=:root, icon=:icon , url=:url WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            "menu" => $role['menu'],
            "root" => $role['root'],
            "icon" => $role['icon'],
            "url" => $role['url'],
            "id" => $role['id']
        ];

        $result = $stmt->execute($data);
        return $result;
    }

    public function delete(int $id): bool
    {
        $sql = "DELETE from  menu WHERE id={$id}";
        $stmt = $this->connection->prepare($sql);

        $reult =  $stmt->execute();
        return $reult;
    }

}