<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:25
 */

namespace App\Domain\Master;


use Psr\Log\LoggerInterface;

class MasteriRepository
{
    private $connection;

    public function __construct(\PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
    }


    public function pendidikan()
    {
        $sql = "SELECT * FROM ngi_master_pendidikan";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }

}