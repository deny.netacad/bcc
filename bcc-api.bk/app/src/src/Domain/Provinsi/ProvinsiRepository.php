<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:25
 */

namespace App\Domain\Provinsi;


use Psr\Log\LoggerInterface;

class ProvinsiRepository
{
    private $logger;
    private $connection;

    public function __construct(\PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }


    public function findAll()
    {
        $sql = "SELECT * FROM ngi_provinsi";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }

}