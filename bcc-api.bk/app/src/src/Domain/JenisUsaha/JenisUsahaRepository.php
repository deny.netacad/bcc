<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:25
 */

namespace App\Domain\JenisUsaha;


use Psr\Log\LoggerInterface;

class JenisUsahaRepository
{
    private $logger;
    private $connection;

    public function __construct(\PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    public function findAll()
    {
        $sql = "SELECT * FROM mast_jenisusaha";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }

}