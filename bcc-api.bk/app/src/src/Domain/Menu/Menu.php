<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 06/04/2021
 * Time: 11:38
 */

namespace App\Domain\Menu;


class Menu
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function findAll(): array
    {
        $sql = "SELECT * FROM role";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

    }

    public function findAllMenu(int $offset = 0, int $limit = 10): array
    {
        $sql = "SELECT * FROM menu limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
    public function findById(int $id): object
    {
        $sql = "SELECT * FROM menu WHERE id={$id}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result;
    }

    public function save(array $menu): int
    {
        $sql = "INSERT INTO menu (menu, root, icon, url, is_root, no_urut) VALUES 
                (:menu, :root, :icon, :url, :is_root, :no_urut) ";
        $stmt = $this->connection->prepare($sql);
        $isRoot =  isset($menu['is_root'])? true : false ;
        $data = [
            "menu" => $menu['menu'],
            "root" => $menu['root'],
            "icon" => $menu['icon'],
            "url" => $menu['url'],
            "is_root" => $isRoot,
            "no_urut" => $menu['no_urut']
        ];


        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }


    public function update(array $menu): bool
    {
        $sql = "UPDATE menu set menu=:menu, root=:root, icon=:icon , url=:url, is_root=:is_root, no_urut=:no_urut WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $isRoot = isset($menu['is_root']) ?  true : false;
        $data = [
            "menu" => $menu['menu'],
            "root" => $menu['root'],
            "icon" => $menu['icon'],
            "url" => $menu['url'],
            "is_root" => $isRoot,
            "no_urut" => $menu['no_urut'],
            "id" => $menu['id']
        ];

        $result = $stmt->execute($data);
        return $result;
    }

    public function delete(int $id): bool
    {
        $sql = "DELETE from  menu WHERE id={$id}";
        $stmt = $this->connection->prepare($sql);

        $reult =  $stmt->execute();
        return $reult;
    }


    public function countAll(): int
    {
        $sql = "SELECT count(*) from  menu";
        $stmt = $this->connection->prepare($sql);

        $reult =  $stmt->execute();
        return $reult;
    }
}