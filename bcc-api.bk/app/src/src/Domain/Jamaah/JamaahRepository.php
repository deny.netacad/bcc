<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 21:52
 */

namespace App\Domain\Jamaah;


use Monolog\Logger;
use Psr\Log\LoggerInterface;


class JamaahRepository
{

    private $connection;
    private $logger;

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger): void
    {
        $this->logger = $logger;
    }


    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function findAll(int $offset = 0, int $limit = 10): array
    {
        $sql = "select * from jamaah limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function countAnggotaJamaah(int $jamaahId): int
    {
        $sql = "select count(*) as count from pengamal where jamaah_id =:id_jamaah";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['id_jamaah' => $jamaahId]);
        $result = $stmt->fetch();
        return $result['count'];
    }


    public function findAllAnggota(int $jamaahId, int $offset = 0, int $limit = 10): array
    {
        $sql = "select c.id as id_pengamal, c.nama_lengkap as nama_pengamal,c.nik, c.tempat_lahir,c.tgl_lahir, c.jenis_kelamin,
                c.kelompok_jamaah, c.no_hp from pengamal c 
                where c.jamaah_id=:id_jamaah
                limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['id_jamaah' => $jamaahId]);
        $result = $stmt->fetchAll();
        return $result;
    }


    public function findAllDetail(int $offset = 0, int $limit = 10): array
    {
        $sql = "select a.id, a.nama, a.imam_jamaah_id as imam_jamaah_id, a.jenis_jamaah_id, a.desa_id,
                b.nama_lengkap as imam_jamaah, b.no_hp, d.nama as desa, e.nama as kecamatan, e.id as kecamatan_id,
                c.nama as jenis_jamaah, f.nama as kabko,  f.id as kabko_id from jamaah a 
                left join pengamal b on a.imam_jamaah_id = b.id
                left join jenis_jamaah c on a.jenis_jamaah_id = c.id
                left join desa d on a.desa_id = d.id
                left join kecamatan e on d.kecamatan_id = e.id
                left join kabupaten_kota f on e.kabupaten_kota_id = f.id
                limit {$limit} offset {$offset}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }


    public function findByIdDetail(int $id): object
    {
        $sql = "select a.id, a.nama, a.imam_jamaah_id as imam_jamaah_id, a.jenis_jamaah_id, a.desa_id,
                b.nama_lengkap as imam_jamaah, b.no_hp, d.nama as desa, e.nama as kecamatan, e.id as kecamatan_id,
                c.nama as jenis_jamaah, f.nama as kabko,  f.id as kabko_id from jamaah a 
                left join pengamal b on a.imam_jamaah_id = b.id
                left join jenis_jamaah c on a.jenis_jamaah_id = c.id
                left join desa d on a.desa_id = d.id
                left join kecamatan e on d.kecamatan_id = e.id
                left join kabupaten_kota f on e.kabupaten_kota_id = f.id
                where a.id = {$id}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result;
    }


    /**
     * @param int $offset
     * @param int $limit
     * @param int $desaId
     * @return array
     */
    public function findAllByDesa(int $offset = 0, int $limit = 10, int $desaId): array
    {
        $sql = "select * from jamaah WHERE desa_id=:desa_id  limit {$limit} offset {$offset} ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'offset' => $offset,
            'limit' => $limit
        ];
        if ($desaId != null && $desaId > 0) {
            $data['desa_id'] = $desaId;
        }
        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function findAllByKecamatan(int $offset = 0, int $limit = 10, int $kecamatanId): array
    {
        $sql = "select * from jamaah a left join desa b on a.desa_id = b.id 
              WHERE b.kecamatan_id=:kecamatan_id  limit {$limit} offset {$offset} ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'offset' => $offset,
            'limit' => $limit,
            'kecamatan_id' => $kecamatanId
        ];

        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param int $kabupatenKotaId
     * @return array
     */
    public function findAllByKabupaten(int $offset = 0, int $limit = 10, int $kabupatenKotaId): array
    {
        $sql = "select * from jamaah a 
              left join desa b on a.desa_id = b.id 
              left join kecamatan c on c.id= b.kecamatan_id
              WHERE c.kabupaten_kota_id=:kabupaten_kota_id  limit {$limit} offset {$offset} ";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'offset' => $offset,
            'limit' => $limit,
            'kabupaten_kota_id' => $kabupatenKotaId
        ];

        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;
    }


    public function saveAnggota(array $anggota): int
    {
        $sql = "INSERT INTO anggota_jamaah (jamaah_id, pengamal_id) 
              VALUES (:jamaah_id, :pengamal_id)";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'jamaah_id' => $anggota['jamaah_id'],
            'pengamal_id' => $anggota['pengamal_id']
        ];

        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }

    public function save(array $jamaah): int
    {
        $sql = "INSERT INTO jamaah (nama, imam_jamaah_id, jenis_jamaah_id, desa_id) 
              VALUES (:nama, :imam_jamaah, :jenis, :desa_id)";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'nama' => $jamaah['nama'],
            'imam_jamaah' => $jamaah['imam_jamaah'],
            'jenis' => $jamaah['jenis'],
            'desa_id' => $jamaah['desa_id'],
        ];

        $stmt->execute($data);
        $reult = $this->connection->lastInsertId();
        return $reult;
    }

    public function updateWithLog(LoggerInterface $logger, array $jamaah): bool
    {

        $id = $jamaah['id'];
        $sql = "UPDATE jamaah SET nama=:nama,imam_jamaah_id=:imam_jamaah,
              jenis_jamaah_id=:jenis,desa_id=:desa_id WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'nama' => $jamaah['nama'],
            'imam_jamaah' => $jamaah['imam_jamaah'],
            'jenis' => $jamaah['jenis'],
            'desa_id' => $jamaah['desa_id'],
            'id' => $id,
        ];

        $logger->info("data to update " . $data);
        $success = $stmt->execute($data);
        return $success;
    }


    public function update(array $jamaah): bool
    {

        $id = $jamaah['id'];
        $sql = "UPDATE jamaah SET nama=:nama,imam_jamaah_id=:imam_jamaah,
              jenis_jamaah_id=:jenis,desa_id=:desa_id WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'nama' => $jamaah['nama'],
            'imam_jamaah' => $jamaah['imam_jamaah'],
            'jenis' => $jamaah['jenis'],
            'desa_id' => $jamaah['desa_id'],
            'id' => $id,
        ];

        $success = $stmt->execute($data);
        return $success;
    }

    public function delete(int $id): bool
    {
        $sql = "DELETE FROM jamaah  WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = [
            'id' => $id,
        ];

        try {
            $status = $stmt->execute($data);
            return $status;
        } catch (\Exception $exception) {
            return false;
        }
    }
}