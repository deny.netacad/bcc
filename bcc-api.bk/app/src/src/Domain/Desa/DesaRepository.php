<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 20:03
 */

namespace App\Domain\Desa;


class DesaRepository
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function findAll(): array
    {
        $sql = "SELECT * FROM desa";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }


    public function findAllByNama($nama): array
    {
        $sql = "SELECT * FROM desa WHERE nama like '%{$nama}%'";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function findById($id): array
    {
        $sql = "SELECT * FROM desa where id=:id";
        $stmt = $this->connection->prepare($sql);
        $data = ['id' => $id];
        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;

    }
    public function findAllByKecamatanId($kecamatanId): array
    {
        $sql = "SELECT * FROM desa where kecamatan_id=:kecamatan_id";
        $stmt = $this->connection->prepare($sql);
        $data = ['kecamatan_id' => $kecamatanId];
        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;

    }

    public function findAllByKabupatenKotaId($kabupatenKotaId): array
    {
        $sql = "SELECT * FROM desa a 
            left join kecamatan b on a.kecamatan_id = b.id 
            where b.kabupaten_kota_id=:kabupaten_kota_id";
        $stmt = $this->connection->prepare($sql);
        $data = ['kabupaten_kota_id' => $kabupatenKotaId];
        $stmt->execute($data);
        $result = $stmt->fetchAll();
        return $result;

    }
}