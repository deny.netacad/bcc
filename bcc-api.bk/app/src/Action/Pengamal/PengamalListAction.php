<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 22:16
 */

namespace App\Action\Pengamal;


use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\Jamaah\JamaahRepository;
use App\Domain\Pengamal\Pengamal;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

class PengamalListAction extends PengamalAction
{

    private $repository;

    public function __construct(LoggerInterface $logger, Pengamal $repository)
    {
        parent::__construct($logger, $repository);
        $this->repository = $repository;
    }

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        $params = $this->request->getQueryParams();

        if (isset($params['nama'])) {
            $nama = (string)$params['nama'];
            $this->logger->info('search nama ' . $nama);
            $result = $this->repository->findAllByNama($nama);
            return $this->respondWithData($result);

        } elseif (isset($params['id'])) {
            $id = (int)$params['id'];
            $result = $this->repository->findById($id);
            return $this->respondWithData($result);

        } elseif (isset($params['desa_id'])) {
            $id = (int)$params['desa_id'];
            $result = $this->repository->findByDesaId($id);
            return $this->respondWithData($result);

        } else {
            $result = $this->repository->findAll();
            return $this->respondWithData($result);
        }

    }
}