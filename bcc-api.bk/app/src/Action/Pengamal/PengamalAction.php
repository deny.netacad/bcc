<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 22:16
 */

namespace App\Action\Pengamal;


use App\Action\Action;
use App\Domain\Pengamal\Pengamal;
use Psr\Log\LoggerInterface;

abstract class PengamalAction extends Action
{

    public function __construct(LoggerInterface $logger, Pengamal $repository)
    {
        parent::__construct($logger, $repository);
    }

}