<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 27/02/2021
 * Time: 12:38
 */

namespace App\Action\Auth;

use App\Domain\User\Repository\UserRespository;
use Firebase\JWT\JWT;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Http\Request;
use Slim\Http\Response;

class AuthAction
{
    private $container;
    private $connection;
    private $logger;

    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @param $app
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        $input = $request->getParsedBody();

        $userRepository = new UserRespository($this->connection, $this->logger);
        $user = $userRepository->auth($input);
        // verify email address.
        if (!$user) {
            return $response->withJson(['message' => 'User not found.', 'token' => null], 401);
        }

        $pwd = $input['password'];

        $password_db = isset($user->password) ? $user->password : $user->pass;
        if (MD5($pwd) == $password_db) {
            $settings = $this->container->get('settings'); // get settings array.

            $payload = ['id' => isset($user->iduser) ? $user->iduser : $user->idperusahaan,
                'username' => $user->username];
            $token = JWT::encode($payload, $settings['jwt']['secret'], "HS256");

            return $response->withJson(['statusCode'=>200, 'data'=>['message' => 'login success', 'token' => $token]], 200);
        } else {
            return $response->withJson(['statusCode'=>200,'data'=>['message' => 'password not match', 'token' => null]], 401);
        }
    }

}