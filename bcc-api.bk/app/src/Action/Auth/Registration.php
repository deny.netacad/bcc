<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 29/05/2021
 * Time: 18:26
 */

namespace App\Action\Auth;


use App\Action\Action;
use App\Domain\User\Repository\UserRespository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class Registration extends Action
{
    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        parent::__construct($logger, $connection, $container);
    }


    protected function action()
    {
        $input = $this->request->getParsedBody();
        $user_type = $input['user_type'];

        $userRepository = new UserRespository($this->connection, $this->logger);

        if ($user_type == 'pelamar') {
            $isNik = $userRepository->checkNikPelamar($input['no_ktp']);
            if ($isNik > 0) {
                $result = [
                    'id' => -1,
                    'message' => 'NIK sudah digunakan'
                ];
            } else {
                $dataId = $userRepository->savePelamar($input);
                // Transform the result into the JSON representation
                $result = [
                    'id' => $dataId,
                    'message' => ''
                ];
            }

            // Build the HTTP response
            $this->response->getBody()->write((string)json_encode($result));

            return $this->response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else if ($user_type == 'perusahaan') {
            $dataId = $userRepository->savePerusahaan($input);
            $result = [
                'id' => $dataId,
                'message' => ''
            ];

            // Build the HTTP response
            $this->response->getBody()->write((string)json_encode($result));

            return $this->response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);

        } else {
            $result = [
                'id' => -1
            ];
            // Build the HTTP response
            $this->response->getBody()->write((string)json_encode($result));

            return $this->response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(203, "parameter user_type diperlukan");
        }
    }
}