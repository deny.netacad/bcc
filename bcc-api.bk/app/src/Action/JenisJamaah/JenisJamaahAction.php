<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 22:52
 */

namespace App\Action\JenisJamaah;


use App\Action\Action;
use App\Action\AuthencticatedAction;
use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\JenisJamaah\JenisJamaah;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Exception\HttpBadRequestException;

class JenisJamaahAction extends AuthencticatedAction
{

    private $repo;

    public function __construct(LoggerInterface $logger, JenisJamaah $repo, App $app)
    {
        parent::__construct($logger, $app);

        $this->repo = $repo;
    }


    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        parent::setTokenData();
        if (!$this->isAllowedUser()) {
            return $this->notAllowedMessage();
        }

        $result = $this->repo->findAll();
        return $this->respondWithData($result);
    }
}