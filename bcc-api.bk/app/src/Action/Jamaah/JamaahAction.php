<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 22:16
 */

namespace App\Action\Jamaah;



use App\Action\AuthencticatedAction;
use Psr\Log\LoggerInterface;
use Slim\App;

abstract class JamaahAction extends AuthencticatedAction
{

    public function __construct(LoggerInterface $logger, App $app)
    {
        parent::__construct($logger, $app);
    }

}