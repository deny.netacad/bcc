<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 22:16
 */

namespace App\Action\Jamaah;


use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\Jamaah\JamaahRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Exception\HttpBadRequestException;

class JamaahListAction extends JamaahAction
{

    private $repository;

    public function __construct(LoggerInterface $logger, JamaahRepository $repository, App $app)
    {
        parent::__construct($logger, $app);
        $this->repository = $repository;
    }

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        parent::setTokenData();
        if (!$this->isAllowedUser()) {
            return $this->notAllowedMessage();
        }
        $result = $this->repository->findAll();
        return $this->respondWithData($result);
    }
}