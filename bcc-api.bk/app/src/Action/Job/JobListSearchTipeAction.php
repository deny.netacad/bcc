<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 6/12/2021
 * Time: 8:02 AM
 */

namespace App\Action\Job;


use App\Action\Action;
use App\Domain\Job\JobRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class JobListSearchTipeAction extends Action
{
    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        parent::__construct($logger, $connection, $container);
    }

 
    protected function action()
    {

        $jobRepository = new JobRepository($this->connection, $this->logger);

        $limit = $this->request->getQueryParams()['limit'];
        $offset = $this->request->getQueryParams()['offset'];
        $keyword = $this->request->getQueryParams()['keyword'];
        $tipe = $this->request->getQueryParams()['tipe'];

        $data = $jobRepository->findAllByCategory($limit, $offset, $keyword, $tipe);
        $count = $jobRepository->countByCategory($keyword, $tipe);
        $result = ['data' => $data, 'count' => $count];

        $this->logger->info("result " . json_encode($result));
        return $this->respondWithData($result, 200);
    }

}