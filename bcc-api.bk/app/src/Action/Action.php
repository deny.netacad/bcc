<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 6/15/2021
 * Time: 12:12 AM
 */

namespace App\Action;


use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class Action
{


    /**
     * @var Request
     */
    public $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;

    protected $container;
    protected $connection;
    protected $logger;

    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        return $this->action();
    }

    abstract protected function action();


    protected function respondWithData($data, $code)
    {
        $this->response->withHeader('Content-Type','application/json; charset=utf-8' );
       return $this->response->withJson(['statusCode'=>$code, 'data'=> $data]);
    }
}