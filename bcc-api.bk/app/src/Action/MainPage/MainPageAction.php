<?php
/**
 * Created by PhpStorm.
 * User: asrofiridho
 * Date: 25/02/2021
 * Time: 21:32
 */

namespace App\Action\MainPage;

use App\Action\Action;
use App\Domain\MainPage\MainPageRespository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class MainPageAction extends Action
{
    public function __construct(LoggerInterface $logger, \PDO $connection, ContainerInterface $container)
    {
        parent::__construct($logger, $connection, $container);
    }


    protected function action()
    {
        $repository = new MainPageRespository($this->connection, $this->logger);
        $result = $repository->getData();
        return $this->respondWithData($result, 200);

    }
}