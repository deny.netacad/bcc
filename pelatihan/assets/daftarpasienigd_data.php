<script>
$(document).ready(function(){
	//doEvent();
	
	
	$('a.btn-transaksi').click(function(){
		var index = $('a.btn-transaksi').index($(this));
		var vId1 = $("span[class=ed1]:eq("+index+")").html();
		var vId2 = $("span[class=ed2]:eq("+index+")").html();
		var vId3 = $("span[class=ed3]:eq("+index+")").html();
		var vId4 = $("span[class=ed4]:eq("+index+")").html();
		var vId5 = $("span[class=ed5]:eq("+index+")").html();
		var vId6 = $("span[class=ed6]:eq("+index+")").html();
		$.ajax({
			url:'<?=base_url()?>rawatinap/page/view/transaksipasien',
			type:'post',
			data:{ 'nopendaftaran': vId1,'kdkelas':vId2,'kdpegawai':vId3,'namalengkap':vId4,
			'kdruangan':vId5,'nocm':vId6
			},
			beforeSend:function(){},
			success:function(response){
				$('#div-transaksi').html(response);
				$('#myModal').modal('show',function(){
					keyboard:false
				});
				
				return false;
			}
		});
		
		
		return false;
	});
	
	$('a.btn-pulang').click(function(){
		var index = $('a.btn-pulang').index($(this));
		var vId1 = $("span[class=ed1]:eq("+index+")").html();
		var vId2 = $("span[class=ed2]:eq("+index+")").html();
		var vId3 = $("span[class=ed3]:eq("+index+")").html();
		var vId4 = $("span[class=ed4]:eq("+index+")").html();
		var vId5 = $("span[class=ed5]:eq("+index+")").html();
		var vId6 = $("span[class=ed6]:eq("+index+")").html();
		var vId7 = $("span[class=ed7]:eq("+index+")").html();
		var vId8 = $("span[class=ed8]:eq("+index+")").html();
		var vId9 = $("span[class=ed9]:eq("+index+")").html();
		var vId10 = $("span[class=ed10]:eq("+index+")").html();
		$.ajax({
			url:'<?=base_url()?>rawatinap/page/view/formpasienpulang',
			type:'post',
			data:{ 'nopendaftaran': vId1,'kdkelas':vId2,'kdpegawai':vId3,'namalengkap':vId4,
			'kdruangan':vId5,'nocm':vId6,'namapasien':vId7,'nori':vId8,'tgmasuk':vId9,'kdkamar':vId10
			},
			beforeSend:function(){},
			success:function(response){
				$('#div-pulang').html(response);
				$('#myModal2').modal('show',function(){
					keyboard:false
				});
				
				return false;
			}
		});		
		
		return false;
	});
	
	$('a.btn-batal').click(function(){
		var index = $('a.btn-batal').index($(this));
		var vId1 = $("span[class=ed1]:eq("+index+")").html();
		var vId2 = $("span[class=ed10]:eq("+index+")").html();
		
		if(confirm("Yakin akan Membatalkan Pasien ?")){
		$.ajax({
			url:'<?=base_url()?>rawatinap/rawatinap/batalri',
			type:'post',
			data:{ 'nopendaftaran': vId1,'kdkamar':vId2
			},
				
				beforeSend:function(){},
				success:function(response){
					refreshData();				
					return false;
				}
			
		});
		}
		
		return false;
	});
	
});


</script>



<?php

	$where = " WHERE a.stspulang = 0 and a.kdruangan='".$this->session->userdata('kdruanganaktif')."' ";
	$per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= " and (a.nopendaftaran like '%".$cari."%' or a.nocm like '%".$cari."%' or b.namalengkap like '%".$cari."%'
		or d.namalengkap like '%".$cari."%'
	)";
	
	$n = intval($this->uri->segment(5));
	
	$q = "select b.namalengkap,a.nopendaftaran,a.nocm,b.tglahir,a.tgmasuk,d.namalengkap as namadokter,c.namakamar,a.kdruangan,a.kdpegawai,a.nori,a.kdkelas,a.kdkamar
	,date_format(a.tgmasuk,'%d-%m-%Y %H:%i:%s') as tgmasuk_
	from pasiendaftar_ri a inner join pasien b on a.nocm=b.nocm inner join kamar c on a.kdkamar = c.kdkamar inner join mast_pegawai d on a.kdpegawai = d.kdpegawai
	";
	
	$rs = $this->db->query("$q $where");
	$row = $rs->row_array();
	$totrows = $rs->num_rows();

	$config['base_url'] = base_url().'rawatinap/page/data/daftarpasienri';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("$q $where order by a.nopendaftaran limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
	
	
	
	
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
<span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
<?=$data['pagination']?>
</div>
<img id="noimage" style="display:none" />
<table class="table table-hover table-bordered">
  <colgroup>
	<col style="width: 2%" />
	<col>
	<col>
	<col>
	<col>
	<col>
	<col>
	<col style="width: 10%" />
	
  </colgroup>
  <thead>
	<tr>
	  <th>
	  <div class="text-center">#</div>
	  </th>
	  <th>
	  <div class="text-center">NO. PENDAFTARAN</div>
	  </th>
	  <th>
	  <div class="text-center">NO. CM</div>
	  </th>
	  <th>
	  <DIV class="text-center">NAMA PASIEN</DIV>		
	  <DIV class="text-center">TGL. LAHIR</DIV>
	  </th>
	  <th>
	  <div class="text-center">TGL. MASUK</div>
	  </th> 
	  <th>
	  <div class="text-center">KAMAR</div>
	  </th>
	  <th>
	  <div class="text-center">Dokter</div>
	  </th>	    
	  <th><div class="text-center"> PROSES </div></th>
	</tr>
  </thead>
 	<?php
	foreach($data['posts']->result() as $item){ $n++;
	?>
	
	<tr>
	  <td align="center"><?=$n?>.</td>
	  <td>
	  <span class="ed1" style="display:none"><?=$item->nopendaftaran?></span>
	  <span class="ed2" style="display:none"><?=$item->kdkelas?></span>
	  <span class="ed3" style="display:none"><?=$item->kdpegawai?></span>
	  <span class="ed4" style="display:none"><?=$item->namadokter?></span>	  
	  <span class="ed5" style="display:none"><?=$item->kdruangan?></span>
	  <span class="ed6" style="display:none"><?=$item->nocm?></span>
	  <span class="ed7" style="display:none"><?=$item->namalengkap?></span>
	  <span class="ed8" style="display:none"><?=$item->nori?></span>
	  <span class="ed9" style="display:none"><?=$item->tgmasuk?></span>
	  <span class="ed10" style="display:none"><?=$item->kdkamar?></span>
	  
	  <div class="text-left"><?=$item->nopendaftaran?></div>
	  </td>
	  <td>
	  <div class="text-left"><?=$item->nocm?></div>
	  </td>
	  <td>
	  <div class="text-left"><?=$item->namalengkap?></div>
	  <div class="text-left"><?=$item->tglahir?></div>
	    </td>
	  <td>
	  <div class="text-left"><?=$item->tgmasuk_?></div>
	  </td>
	  <td>
	  <div class="text-center"><?=$item->namakamar?></div>
	  </td>
	  <td>
	  <div class="text-center"><?=$item->namadokter?></div>
	  </td>
	  <td>
		<div class="text-center">
			<a href="#" class="btn-transaksi" title="Transaksi Pelayanan" data-toggle="tooltip" style="font-size:30px"><i class="icon-stethoscope"></i></a>
			<a href="#" class="btn-pulang" title="Pulang" data-toggle="tooltip" style="font-size:30px;color:red"><i class="icon-arrow-right"></i></a>
			<a href="#" class="btn-batal" title="Batalkan Pasien" data-toggle="tooltip" style="font-size:30px;color:red"><i class="icon-remove-sign"></i></a>
		</div>
	  </td>
	</tr>
	<?php
	}
	?>
  </tbody>
</table>
 
