/*

enterform.js
created by Agung Nugroho ( desamedia.com ) ( dinustech.com )
Copyright 2012
How to use ? it's very simple !

$('#elementform').enterform();

How to reinitialize for example in ajax call ? 

$.ajax({ 
	type: 'post',
	url: 'some_url_here'
	dataType:'html',
	success:function(response){
		// code here
		$('#elementform').enterform('reinitialize');
	}
});

if you want to prevent several element for this enterform? just add class like this 

<input type="text" id="name" name="name" class="nofocus">

you can custom this plugin as you want

Hope you like it ! 
agindanoe@gmail.com

*/
function resetForm($form) {
	
    $form.find('input[type=text],input[type=hidden], input[type=password], input[type=file], textarea').val('');
    $form.find('input[type=radio], input[type=checkbox]')
         .removeAttr('checked').removeAttr('selected');
	//$form.validate().resetForm();
}


function str_replace(haystack, needle, replacement) {
	var temp = haystack.split(needle);
	return temp.join(replacement);
}

function CommaFormatted(amount)
{
	amount = str_replace(amount,",","");
	var delimiter = ","; // replace comma if desired
	var a = amount.split('.',2)
	var d = new Array();
	if(a[1]!=undefined){
		d = a[1];
	}
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3)
	{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}

(function($) {
    
    $.enterform = function(el, options) {
        
        var base = this;
        base.$arrForm = Array("","input","select","radio","textarea","checkbox","button");
        base.$el = $(el);
                
        base.$el.data("enterform", base);
        
        base.init = function() {    
			try{
			//console.log('init');
			//base.$el.attr('autocomplete','off');
			base.$el.attr('onkeypress','javascript:if(event.which==13) return false');
			base.options = $.extend({},$.enterform.defaultOptions, options);
			base.$elform = base.$el.find("input").not('.nofocus').not('[readonly]');
			base.$el.unbind('keypress');
			//console.log(base.$elform.length);
			base.$elform.each(function(index,item){
				if($.inArray($(item).prop('tagName').toLowerCase(),base.$arrForm)){
					$(item).unbind('keypress');
					$(item).keypress(function(e){
						if(!e.shiftKey){
							if(e.which==13){
								if(base.$elform.eq((index+1))) base.$elform.eq((index+1)).focus();
								//console.log('enter');
							}
						}
					});
				}
			});
			
			base.$el.bind('keypress',function(e){
				if(e.which==13){
					return false;
				}
			});
				
			}catch(e){ console.log(e.message); }
        };
        
        base.reinitialize = function() {
            try{
			base.destroy();
			base.init();
			}catch(e){ console.log(e.message); }
        };
		
		base.destroy = function(){
			 try{
				 try{
				 base.$elform.unbind("keypress");
				 }catch(e){ console.log(e.message); }
				 base.$elform.each(function(index,item){
					if($.inArray($(item).prop('tagName').toLowerCase(),base.$arrForm)){
						try{
						$(item).unbind("keypress");
						$(item).element = null;
						}catch(e){ alert(e.message); }
					}
				 });
				 //base.$el.removeData('enterform');
				 base.$el.element = null;
				 //console.log("destroooy");
			 }catch(e){ console.log(e.message); }
		};
        
        base.init();
    };
    
    $.enterform.defaultOptions = { };
    
    $.fn.enterform = function(options) {
        if( typeof(options)=='string' ){
			var safeGuard = $(this).data('enterform');	
			if( safeGuard ){
				switch( options ){
					case "reinitialize":
						safeGuard.reinitialize();
					break;
				}
			}
		}else{
			return this.each(function(){
				 (new $.enterform(this, options));
			});
		}
    };
	
	$.fn.focusTo = function(el) {
		try{
		this.unbind('keypress');
		//console.log('focusto');
		}catch(e){ alert(e.message); }
		return this.keypress(function(e){
			 if(!e.shiftKey){
				 if(e.which==13){
					el.focus();
				 }
			 }
		});
    };
	
	$.fn.onClickSubmit = function(el){
		return this.keypress(function(e){
			if(e.which==13){
				el.submit();
			}
		});
	};
	
	
    
})(jQuery);

(function($){
		$.loading = function(el,options){
			var base = this;
			base.$el = $(el);
			base.$el.data('loading',base);
			
			base.init = function() {    
				base.$el.after('<i class="icon-spinner icon-spin"></i>');
			}
			base.stop = function() {
				base.$el.parent().find('.icon-spinner').eq(0).remove();
			}
			base.init();
		};
		
		$.loading.defaultOptions = { };
		
		$.fn.loading = function(options) {
			if( typeof(options)=='string' ){
				var safeGuard = $(this).data('loading');	
				if( safeGuard ){
					switch( options ){
						case "stop":
							safeGuard.stop();
						break;
					}
				}
			}else{
				return this.each(function(){
					 (new $.loading(this, options));
				});
			}
		};
	})(jQuery);