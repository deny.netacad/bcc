function inherits(a, b) {
	function tempCtor() {};
	tempCtor.prototype = b.prototype;
	a.superClass_ = b.prototype;
	a.prototype = new tempCtor();
	a.prototype.constructor = a
}
function MarkerLabel_(a, b, c) {
	this.marker_ = a;
	this.handCursorURL_ = a.handCursorURL;
	this.labelDiv_ = document.createElement("div");
	this.labelDiv_.style.cssText = "position: absolute; overflow: hidden;";
	this.eventDiv_ = document.createElement("div");
	this.eventDiv_.style.cssText = this.labelDiv_.style.cssText;
	this.eventDiv_.setAttribute("onselectstart", "return false;");
	this.eventDiv_.setAttribute("ondragstart", "return false;");
	this.crossDiv_ = MarkerLabel_.getSharedCross(b)
}
inherits(MarkerLabel_, google.maps.OverlayView);
MarkerLabel_.getSharedCross = function (a) {
	var b;
	if (typeof MarkerLabel_.getSharedCross.crossDiv === "undefined") {
		b = document.createElement("img");
		b.style.cssText = "position: absolute; z-index: 1000002; display: none;";
		b.style.marginLeft = "-8px";
		b.style.marginTop = "-9px";
		b.src = a;
		MarkerLabel_.getSharedCross.crossDiv = b
	}
	return MarkerLabel_.getSharedCross.crossDiv
};
MarkerLabel_.prototype.onAdd = function () {
	var c = this;
	var d = false;
	var f = false;
	var g;
	var h, cLngOffset;
	var i;
	var j;
	var k;
	var l;
	var m = 20;
	var n = "url(" + this.handCursorURL_ + ")";
	var o = function (e) {
		if (e.preventDefault) {
			e.preventDefault()
		}
		e.cancelBubble = true;
		if (e.stopPropagation) {
			e.stopPropagation()
		}
	};
	var p = function () {
		c.marker_.setAnimation(null)
	};
	this.getPanes().overlayImage.appendChild(this.labelDiv_);
	this.getPanes().overlayMouseTarget.appendChild(this.eventDiv_);
	if (typeof MarkerLabel_.getSharedCross.processed === "undefined") {
		this.getPanes().overlayImage.appendChild(this.crossDiv_);
		MarkerLabel_.getSharedCross.processed = true
	}
	this.listeners_ = [google.maps.event.addDomListener(this.eventDiv_, "mouseover", function (e) {
		if (c.marker_.getDraggable() || c.marker_.getClickable()) {
			this.style.cursor = "pointer";
			google.maps.event.trigger(c.marker_, "mouseover", e)
		}
	}), google.maps.event.addDomListener(this.eventDiv_, "mouseout", function (e) {
		if ((c.marker_.getDraggable() || c.marker_.getClickable()) && !f) {
			this.style.cursor = c.marker_.getCursor();
			google.maps.event.trigger(c.marker_, "mouseout", e)
		}
	}), google.maps.event.addDomListener(this.eventDiv_, "mousedown", function (e) {
		f = false;
		if (c.marker_.getDraggable()) {
			d = true;
			this.style.cursor = n
		}
		if (c.marker_.getDraggable() || c.marker_.getClickable()) {
			google.maps.event.trigger(c.marker_, "mousedown", e);
			o(e)
		}
	}), google.maps.event.addDomListener(document, "mouseup", function (a) {
		var b;
		if (d) {
			d = false;
			c.eventDiv_.style.cursor = "pointer";
			google.maps.event.trigger(c.marker_, "mouseup", a)
		}
		if (f) {
			if (j) {
				b = c.getProjection().fromLatLngToDivPixel(c.marker_.getPosition());
				b.y += m;
				c.marker_.setPosition(c.getProjection().fromDivPixelToLatLng(b));
				try {
					c.marker_.setAnimation(google.maps.Animation.BOUNCE);
					setTimeout(p, 1406)
				} catch(e) {}
			}
			c.crossDiv_.style.display = "none";
			c.marker_.setZIndex(g);
			i = true;
			f = false;
			a.latLng = c.marker_.getPosition();
			google.maps.event.trigger(c.marker_, "dragend", a)
		}
	}), google.maps.event.addListener(c.marker_.getMap(), "mousemove", function (a) {
		var b;
		if (d) {
			if (f) {
				a.latLng = new google.maps.LatLng(a.latLng.lat() - h, a.latLng.lng() - cLngOffset);
				b = c.getProjection().fromLatLngToDivPixel(a.latLng);
				if (j) {
					c.crossDiv_.style.left = b.x + "px";
					c.crossDiv_.style.top = b.y + "px";
					c.crossDiv_.style.display = "";
					b.y -= m
				}
				c.marker_.setPosition(c.getProjection().fromDivPixelToLatLng(b));
				if (j) {
					c.eventDiv_.style.top = (b.y + m) + "px"
				}
				google.maps.event.trigger(c.marker_, "drag", a)
			} else {
				h = a.latLng.lat() - c.marker_.getPosition().lat();
				cLngOffset = a.latLng.lng() - c.marker_.getPosition().lng();
				g = c.marker_.getZIndex();
				k = c.marker_.getPosition();
				l = c.marker_.getMap().getCenter();
				j = c.marker_.get("raiseOnDrag");
				f = true;
				c.marker_.setZIndex(1000000);
				a.latLng = c.marker_.getPosition();
				google.maps.event.trigger(c.marker_, "dragstart", a)
			}
		}
	}), google.maps.event.addDomListener(document, "keydown", function (e) {
		if (f) {
			if (e.keyCode === 27) {
				j = false;
				c.marker_.setPosition(k);
				c.marker_.getMap().setCenter(l);
				google.maps.event.trigger(document, "mouseup", e)
			}
		}
	}), google.maps.event.addDomListener(this.eventDiv_, "click", function (e) {
		if (c.marker_.getDraggable() || c.marker_.getClickable()) {
			if (i) {
				i = false
			} else {
				google.maps.event.trigger(c.marker_, "click", e);
				o(e)
			}
		}
	}), google.maps.event.addDomListener(this.eventDiv_, "dblclick", function (e) {
		if (c.marker_.getDraggable() || c.marker_.getClickable()) {
			google.maps.event.trigger(c.marker_, "dblclick", e);
			o(e)
		}
	}), google.maps.event.addListener(this.marker_, "dragstart", function (a) {
		if (!f) {
			j = this.get("raiseOnDrag")
		}
	}), google.maps.event.addListener(this.marker_, "drag", function (a) {
		if (!f) {
			if (j) {
				c.setPosition(m);
				c.labelDiv_.style.zIndex = 1000000 + (this.get("labelInBackground") ? -1 : +1)
			}
		}
	}), google.maps.event.addListener(this.marker_, "dragend", function (a) {
		if (!f) {
			if (j) {
				c.setPosition(0)
			}
		}
	}), google.maps.event.addListener(this.marker_, "position_changed", function () {
		c.setPosition()
	}), google.maps.event.addListener(this.marker_, "zindex_changed", function () {
		c.setZIndex()
	}), google.maps.event.addListener(this.marker_, "visible_changed", function () {
		c.setVisible()
	}), google.maps.event.addListener(this.marker_, "labelvisible_changed", function () {
		c.setVisible()
	}), google.maps.event.addListener(this.marker_, "title_changed", function () {
		c.setTitle()
	}), google.maps.event.addListener(this.marker_, "labelcontent_changed", function () {
		c.setContent()
	}), google.maps.event.addListener(this.marker_, "labelanchor_changed", function () {
		c.setAnchor()
	}), google.maps.event.addListener(this.marker_, "labelclass_changed", function () {
		c.setStyles()
	}), google.maps.event.addListener(this.marker_, "labelstyle_changed", function () {
		c.setStyles()
	})]
};
MarkerLabel_.prototype.onRemove = function () {
	var i;
	this.labelDiv_.parentNode.removeChild(this.labelDiv_);
	this.eventDiv_.parentNode.removeChild(this.eventDiv_);
	for (i = 0; i < this.listeners_.length; i++) {
		google.maps.event.removeListener(this.listeners_[i])
	}
};
MarkerLabel_.prototype.draw = function () {
	this.setContent();
	this.setTitle();
	this.setStyles()
};
MarkerLabel_.prototype.setContent = function () {
	var a = this.marker_.get("labelContent");
	if (typeof a.nodeType === "undefined") {
		this.labelDiv_.innerHTML = a;
		this.eventDiv_.innerHTML = this.labelDiv_.innerHTML
	} else {
		this.labelDiv_.innerHTML = "";
		this.labelDiv_.appendChild(a);
		a = a.cloneNode(true);
		this.eventDiv_.appendChild(a)
	}
};
MarkerLabel_.prototype.setTitle = function () {
	this.eventDiv_.title = this.marker_.getTitle() || ""
};
MarkerLabel_.prototype.setStyles = function () {
	var i, labelStyle;
	this.labelDiv_.className = this.marker_.get("labelClass");
	this.eventDiv_.className = this.labelDiv_.className;
	this.labelDiv_.style.cssText = "";
	this.eventDiv_.style.cssText = "";
	labelStyle = this.marker_.get("labelStyle");
	for (i in labelStyle) {
		if (labelStyle.hasOwnProperty(i)) {
			this.labelDiv_.style[i] = labelStyle[i];
			this.eventDiv_.style[i] = labelStyle[i]
		}
	}
	this.setMandatoryStyles()
};
MarkerLabel_.prototype.setMandatoryStyles = function () {
	this.labelDiv_.style.position = "absolute";
	this.labelDiv_.style.overflow = "hidden";
	if (typeof this.labelDiv_.style.opacity !== "undefined" && this.labelDiv_.style.opacity !== "") {
		this.labelDiv_.style.MsFilter = "\"progid:DXImageTransform.Microsoft.Alpha(opacity=" + (this.labelDiv_.style.opacity * 100) + ")\"";
		this.labelDiv_.style.filter = "alpha(opacity=" + (this.labelDiv_.style.opacity * 100) + ")"
	}
	this.eventDiv_.style.position = this.labelDiv_.style.position;
	this.eventDiv_.style.overflow = this.labelDiv_.style.overflow;
	this.eventDiv_.style.opacity = 0.01;
	this.eventDiv_.style.MsFilter = "\"progid:DXImageTransform.Microsoft.Alpha(opacity=1)\"";
	this.eventDiv_.style.filter = "alpha(opacity=1)";
	this.setAnchor();
	this.setPosition();
	this.setVisible()
};
MarkerLabel_.prototype.setAnchor = function () {
	var a = this.marker_.get("labelAnchor");
	this.labelDiv_.style.marginLeft = -a.x + "px";
	this.labelDiv_.style.marginTop = -a.y + "px";
	this.eventDiv_.style.marginLeft = -a.x + "px";
	this.eventDiv_.style.marginTop = -a.y + "px"
};
MarkerLabel_.prototype.setPosition = function (a) {
	var b = this.getProjection().fromLatLngToDivPixel(this.marker_.getPosition());
	if (typeof a === "undefined") {
		a = 0
	}
	this.labelDiv_.style.left = Math.round(b.x) + "px";
	this.labelDiv_.style.top = Math.round(b.y - a) + "px";
	this.eventDiv_.style.left = this.labelDiv_.style.left;
	this.eventDiv_.style.top = this.labelDiv_.style.top;
	this.setZIndex()
};
MarkerLabel_.prototype.setZIndex = function () {
	var a = (this.marker_.get("labelInBackground") ? -1 : +1);
	if (typeof this.marker_.getZIndex() === "undefined") {
		this.labelDiv_.style.zIndex = parseInt(this.labelDiv_.style.top, 10) + a;
		this.eventDiv_.style.zIndex = this.labelDiv_.style.zIndex
	} else {
		this.labelDiv_.style.zIndex = this.marker_.getZIndex() + a;
		this.eventDiv_.style.zIndex = this.labelDiv_.style.zIndex
	}
};
MarkerLabel_.prototype.setVisible = function () {
	if (this.marker_.get("labelVisible")) {
		this.labelDiv_.style.display = this.marker_.getVisible() ? "block": "none"
	} else {
		this.labelDiv_.style.display = "none"
	}
	this.eventDiv_.style.display = this.labelDiv_.style.display
};
function MarkerWithLabel(a) {
	a = a || {};
	a.labelContent = a.labelContent || "";
	a.labelAnchor = a.labelAnchor || new google.maps.Point(0, 0);
	a.labelClass = a.labelClass || "markerLabels";
	a.labelStyle = a.labelStyle || {};
	a.labelInBackground = a.labelInBackground || false;
	if (typeof a.labelVisible === "undefined") {
		a.labelVisible = true
	}
	if (typeof a.raiseOnDrag === "undefined") {
		a.raiseOnDrag = true
	}
	if (typeof a.clickable === "undefined") {
		a.clickable = true
	}
	if (typeof a.draggable === "undefined") {
		a.draggable = false
	}
	if (typeof a.optimized === "undefined") {
		a.optimized = false
	}
	a.crossImage = a.crossImage || "http" + (document.location.protocol === "https:" ? "s": "") + "://maps.gstatic.com/intl/en_us/mapfiles/drag_cross_67_16.png";
	a.handCursor = a.handCursor || "http" + (document.location.protocol === "https:" ? "s": "") + "://maps.gstatic.com/intl/en_us/mapfiles/closedhand_8_8.cur";
	a.optimized = false;
	this.label = new MarkerLabel_(this, a.crossImage, a.handCursor);
	google.maps.Marker.apply(this, arguments)
}
inherits(MarkerWithLabel, google.maps.Marker);
MarkerWithLabel.prototype.setMap = function (a) {
	google.maps.Marker.prototype.setMap.apply(this, arguments);
	this.label.setMap(a)
};