/* 
jquery.plugin.mytable V.1 Copyright July 2012
Author : Jpod Agung Nugroho 
Please do not remove author if you think my plugin is important for you.
Thanks
*/

/* How to use */
/*
$('#btn_addrow').mytable({
	table: $("#table-id"),
	data: ["auto","col-1","col-2","col-3"],
	onAddrow: function(param,data,no){
		alert('You can do some case here ;) ...');
		$('.btn_delrow').eq(no).delRow({ 
			id: no,
			onDelrow1:function(index){
				alert(index);
			}
		});
	}
});
*/

$.fn.mytable = function(options,method) {

	var defaults = {
		table: '#table-row',
		data: [1,2,3,4],
		classAuto: 'auto',
		beforeAddrow:function(){ },
		onAddrow:function(tableid,data,no){ }
	};
	
	var options = jQuery.extend(defaults, options);
	
	return this.each(function() {
		  var o =options;
		  var obj = $(this);
		  $(this).click(function(e){
			e.stopPropagation();
			e.preventDefault();

			o.beforeAddrow();
			
			var no = $.inject_row( $(o.table), o.data,o );
			o.onAddrow(o.table,o.data,no);
		  });
	});
	
};

$.inject_row = function( table_body, data,o ){
	var rowCount = $(table_body).find(' >tbody >tr').length;
	var no = rowCount+1;
	var row_str = '<tr>';
	$.each( data, function(index,item){
		if(item==o.classAuto) item='<span class="'+o.classAuto+'">'+no+'</span>';
		row_str += '<td>'+item+'</td>';
	});
	row_str += '</tr>';
	$(table_body).find("tbody").append(row_str);
	
	return (no-1);
};

$.fn.delRow = function(options){
	var defaults = {
		id: '',
		tableid: '',
		needconfirm: true,
		confirm: 'Yakin akan menghapus item ini?',
		'classAuto': 'auto',
		onDelrow1:function(no){ },
		onBeforeDelrow:function(no){}
	};
	var options = jQuery.extend(defaults, options);
	
	return this.each(function() {
		try{
		var o = options;
		var obj = $(this);
		var rowdel = obj.parent().parent().find("span."+o.classAuto+"").eq(0).html();
			obj.click(function(e){
				e.stopPropagation();
				e.preventDefault();

				if(o.needconfirm==true){
					if(confirm(o.confirm)){
						try{
						var rowdel = obj.parent().parent().parent().find("span."+o.classAuto+"").eq(0).html();
							o.onBeforeDelrow(rowdel);
							obj.parent().parent().parent().remove();
							var no = 0;
							$('.'+o.classAuto).each(function(index,item){
								var no = (index+1);
								$(item).html(no);
							});
						}catch(e){
							alert(e.message);
						}
						o.onDelrow1(rowdel);
					}
				}else{
					try{
					var rowdel = obj.parent().parent().find("span."+o.classAuto+"").eq(0).html();
						obj.parent().parent().remove();
						var no = 0;
						
						$(o.tableid+' .'+o.classAuto).each(function(index,item){
							var no = (index+1);
							$(item).html(no);
						});
					}catch(e){
						alert(e.message);
					}
					o.onDelrow1(rowdel);
				}
				
			});
		}catch(e){ alert(e.message); }
	});
};

$.fn.outer = function() {
	return $( $('<div></div>').html(this.clone()) ).html();
}

$.fn.centerEl = function(){
	return '<div class="text-center">'+this.outer()+'</div>';
}



$.create = function() {
    if (arguments.length == 0) return [];
    var args = arguments[0] || {}, elem = null, elements = null;
    var siblings = null;

    // In case someone passes in a null object,
    // assume that they want an empty string.
    if (args == null) args = "";
    if (args.constructor == String) {
        if (arguments.length > 1) {
            var attributes = arguments[1];
                if (attributes.constructor == String) {
                            elem = document.createTextNode(args);
                            elements = [];
                            elements.push(elem);
                            siblings =
        jQuery.create.apply(null, Array.prototype.slice.call(arguments, 1));
                            elements = elements.concat(siblings);
                            return elements;

                    } else {
                            elem = document.createElement(args);

                            // Set element attributes.
                            var attributes = arguments[1];
                            for (var attr in attributes)
                                jQuery(elem).attr(attr, attributes[attr]);

                            // Add children of this element.
                            var children = arguments[2];
                            children = jQuery.create.apply(null, children);
                            jQuery(elem).append(children);

                            // If there are more siblings, render those too.
                            if (arguments.length > 3) {
                                    siblings =
        jQuery.create.apply(null, Array.prototype.slice.call(arguments, 3));
                                    return [elem].concat(siblings);
                            }
                            return elem;
                    }
            } else return document.createTextNode(args);
      } else {
              elements = [];
              elements.push(args);
              siblings =
        jQuery.create.apply(null, (Array.prototype.slice.call(arguments, 1)));
              elements = elements.concat(siblings);
              return elements;
      }
};