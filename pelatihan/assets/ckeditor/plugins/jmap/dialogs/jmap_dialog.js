CKEDITOR.dialog.add( 'jmapDialog', function ( editor )
{
	return {
		title : 'Insert New Map (JPODMaps)',
		minWidth : 600,
		minHeight : 400,
		contents :
		[
			{
				id : 'tab1',
				label : 'Basic Settings',
				elements: [{
					type: "html",
					id: "previewHtml",
					html: '<iframe src="' + editor.path + "dialogs/jmap.html" + '" style="width: 100%; height: ' + 540 + 'px" hidefocus="true" frameborder="0" ' + 'id="cke_docProps_preview_iframe"></iframe>',
					
				}]
			}
		],
		onShow: function () {
			
		},
		onLoad: function () {
			this.on("resize", function (b) {
				document.getElementById("cke_docProps_preview_iframe").style.height = b.data.height + "px";
				triggerResize && triggerResize.call && triggerResize(b.data.height - 60);
			});
		},
		onOk: function () {
			/*
			try{
			var ret = self.parent.generateCodeMap();
			for(attrname in ret.objects){
				console.log(attrname);
			}
			editor.insertHtml('<img class="jmap_img" src="http://maps.google.com/maps/api/staticmap?center='
			+ret.lat+','+ret.lng+'&zoom='+ret.zoom+'&size='+ret.width+'x'+ret.height
			+'&maptype='+ret.type+'&sensor=false&language=&markers=color:red|label:none|'+ret.lat+','+ret.lng+'" title="semarang" style="border:1px solid #CECECE;">');
			}catch(e){ alert(e); }
			*/
			var b = '<img class="jmap_img" contenteditable="false" data_script="' + encodeURIComponent(JSON.stringify(generateCodeMap())) + '" src="' + generateStatMap(generateCodeMap()) + '"/>';
            var c = CKEDITOR.dom.element.createFromHtml(b);
            editor.insertElement(c);
		}
	};
});