(function( $ ){

  var methods = {
    init : function( options ) { 
      // THIS 
    },
    show : function( ) {
      // IS
	  alert('test');
    },
    hide : function( ) { 
      // GOOD
    },
    update : function( content ) { 
      // !!! 
	  alert('tset');
    }
  };

  $.fn.myPlugin = function( method ) {
    
    // Method calling logic
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.myPlugin' );
    }    
	
  
  };
  
})( jQuery );
