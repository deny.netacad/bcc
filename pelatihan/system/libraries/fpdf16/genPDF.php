<?php
require('fpdf.php');

$consThn 	= $_REQUEST['thn'];
$consNip 	= $_REQUEST['nip'];
$urut = (($_REQUEST['newurut']>0)?"_".$_REQUEST['newurut']:"");



class PDF extends FPDF
{
//Page header
function Header()
{
	//Logo
	$filename = "logo.png";
	$serverName =  $_SERVER['HTTP_HOST'];
	$img = imagecreatefrompng("http://".$serverName."/efilingpemalang/bargen/test.php?id=".$_REQUEST['nip']);
	imagepng($img,$filename);
	$this->Image($filename,10,4,33);
	//Arial bold 15
	#unlink($filename);
	$this->SetFont('Arial','',10);
	//Move to the right
	$this->Cell(80);
	//Title
	//Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
	$this->Cell(100,0,$_REQUEST['nm'],0,0,'');
	//Line break
	$this->Ln(20);
}

//Page footer
function Footer()
{
	//Position at 1.5 cm from bottom
	$this->SetY(-15);
	//Arial italic 8
	$this->SetFont('Arial','I',8);
	//Page number
	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

}

$idjendok = explode(",",$_REQUEST['idjendok']);

foreach($idjendok as $item):
	$consJendok = $_REQUEST['nip']."_".$item;
	$consDir 	= $_SERVER['DOCUMENT_ROOT']."/efl/data/".$consThn."/".$consNip;
	$lnJendok 	= strlen($consJendok);
	$saveFilename = $consDir."/".$consJendok.".pdf";
	unlink($saveFilename);
	
	//Instanciation of inherited class
	$pdf=new PDF();
	$pdf->AliasNbPages();
	
	$d = dir($consDir);
	$n = 0;
	while (false !== ($entry = $d->read())) {
		$extarr = explode(".",$entry);
		$ext = $extarr[count($extarr)-1];
		if($entry!="." && $entry!=".." && $ext=="png" && substr($entry,0,$lnJendok)==$consJendok){
			$n++;
			echo $n;
			$pdf->AddPage("p");
			$pdf->Image($consDir."/".$entry,10,20,180);
		}
	}
	$d->close();
	$pdf->Output($saveFilename);
endforeach;
?>
