<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {

	public function index()
	{
		if($this->session->userdata('is_login_diperindag')){
			$this->load->view('main',$this->data);
		}else{
			redirect('/login');
		}
	}
	
}