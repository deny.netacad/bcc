<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?=$site_title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?=$def_css?>bootstrap.css" rel="stylesheet">
    <link href="<?=$def_css?>bootstrap-responsive.css" rel="stylesheet">
	<link href="<?=$def_css?>custom.css" rel="stylesheet">
	
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <!--<link rel="shortcut icon" href="../assets/ico/favicon.png">-->
	<script src="<?=$def_js?>jquery.js"></script>
	
    <script src="<?=$def_js?>bootstrap.js"></script>
	<script src="<?=$def_js?>holder.js"></script>
	<script src="<?=$def_js?>enterform.js"></script>
	<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
	
	<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">
	<link href="<?=$def_js?>plugins/contextmenu/jquery.contextMenu.css" rel="stylesheet">
	<link href="<?=$def_js?>plugins/parallax-slider/css/parallax-slider.css" rel="stylesheet">
	<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.min.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/conntextmenu/jquery.contextMenu.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/highcharts.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/parallax-slider/js/jquery.cslider.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/parallax-slider/js/modernizr.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/jpodtable/jquery.jpodtable.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/terbilang/jquery.terbilang.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/mask/jquery.mask.js"></script>
	<link href="<?=$def_css?>jquery-ui.css" rel="stylesheet">
	<script src="<?=$def_js?>plugins/ui/js/jquery-ui-1.10.4.custom.min.js"></script>
	<script>
	$(document).ready(function(){
		$(".dropdown").hover(            
			function() {
				$('.dropdown-menu', this).stop( true, true ).slideDown("fast");
				$(this).toggleClass('open');        
			},
			function() {
				$('.dropdown-menu', this).stop( true, true ).slideUp("fast");
				$(this).toggleClass('open');       
			}
		);
	});
	</script>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
	  
      <div class="navbar-inner">
        <div class="container-fluid">
		
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <a class="brand" href="<?=base_url()?>"><i class="icon-leaf"></i> <?=$site_heading?></a>
		  <br>
          <div class="nav-collapse collapse">
			<div class="pull-right">
				<ul class="nav nav-list leftmenu">
						<ul class="nav">
							<li class="">
							<a href="<?=base_url()?>"><i class="icon-home"></i> Home</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>SKPD <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Daftar SKPD</a></li>
									<li><a href="#">Tambah Baru</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>Informasi Publik <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Daftar Informasi Publik (DIP)</a></li>
									<li><a href="#">Tambah DIP</a></li>
									<li><a href="#">Kategori DIP</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>Permintaan <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Daftar Permintaan</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>Keberatan <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Daftar Keberatan</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>Laporan <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Laporan Permintaan</a></li>
									<li><a href="#">Laporan Keberatan</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>Pengguna <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Daftar Pengguna</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>Pengguna Publik <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Daftar Pengguna Publik</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-multimenu>SMS <b class="caret"></b></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">SMS</a></li>
								</ul>
							</li>
						</ul>
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <b class="caret"></b></a>
							<ul class="dropdown-menu pull-right">
								<li>
									<div class="media">
									  <a class="pull-left" href="#">
										<img class="media-object" data-src="<?=$def_js?>holder.js/50x50">
									  </a>
									  
									  <div class="media-body">
										<h6 class="media-heading"><?=$this->session->userdata('username')?></h6>
										<i><?=$this->session->userdata('nama')?></i>
									  </div>
									</div>
								</li>
								<li><a href="<?=base_url()?>auth/gantipass"><i class="icon-pencil"></i> Ganti Password</a></li>
								<li><a href="<?=base_url()?>auth/logout"><i class="icon-off"></i> Logout</a></li>
							</ul>
						</li>
				</ul>
			</div> 
          </div>

		  
		</div>
      </div>
    </div>
	<!-- isi content -->
	<div class="container-fluid fill" style="margin-top:70px">
	  <?php
	  /*
	  if(isset($page)){
		$this->load->view($module.'/'.$page);
	  }else{
		$data['modules'] = $this->session->userdata('def_mod');
		$this->load->view($this->session->userdata('def_mod').'/default',$data);
	  }
	  */
	  ?>
	</div>
	<!-- isi content sampe sini -->
  </body>
</html>
