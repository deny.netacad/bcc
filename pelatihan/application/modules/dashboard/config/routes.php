<?php 
$route['(dashboard)/skpd']	= "page/load/skpd";
$route['(dashboard)/dip']	= "page/load/dip";
$route['(dashboard)/permintaan']	= "page/load/permintaan";
$route['(dashboard)/keberatan']	= "page/load/keberatan";
$route['(dashboard)/pengguna']	= "page/load/pengguna";

$route['(dashboard)/lap_permintaan']	= "page/load/lap_permintaan";
$route['(dashboard)/lap_keberatan']	= "page/load/lap_keberatan";
$route['(dashboard)/peserta'] = "page/load/peserta";
$route['(dashboard)/perusahaan'] = "page/load/perusahaan";
$route['(dashboard)/kegiatan'] = "page/load/kegiatan";

#tambahan
$route['(dashboard)/postingpeserta'] = "page/load/postingpeserta";
$route['(dashboard)/pelatihan'] = "page/load/pelatihan";
$route['(dashboard)/pendaftaranpeserta'] = "page/load/pendaftaranpeserta";
