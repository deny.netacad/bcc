<script>
	
function dipcatFormatResult(data) {
	var markup= "<li>"+ data.judul+" <br> <small><b>"+data.tempat+"</b> , "+data.tgl_mulai+"</small</li>";
	return markup;
}

function dipcatFormatSelection(data) {
	return data.judul;
}
	
	$(document).ready(function(){
		
		
		$("#id_pelatihan,#id_pelatihan2").select2({
		id: function(e) { return e.id }, 
		placeholder: "Pilih Kategori",
		minimumInputLength: 0,
		multiple: false,
		allowClear:true,
		ajax: { 
			url: "<?=base_url()?><?=$this->modules?>/ajaxfile/caripelatihan",
			dataType: 'jsonp',
			type:'POST',
			quietMillis: 100,
			data: function (term, page) {
				return {
					keyword: term, //search term
					per_page: 5, // page size
					page: page, // page number
				};
			},
			results: function (data, page) { 
				var more = (page * 5) < data.result;
				return {results: data.rows, more: more};
			}
		},
		formatResult: dipcatFormatResult, 
		formatSelection: dipcatFormatSelection
	});
		
		$(document).on('click','a[title="Cetak"]',function(){
			var $this = $('a[title="Cetak"]');
			var index = $this.index($(this));
			var arr_param = $this.eq(index).attr('data').split("|");
				$('#form-print #id_peserta_print').val(arr_param[0]);
				$('#form-print #id_pelatihan_print').val(arr_param[1]);
				$('#form-print').submit();
				//alert(arr_param[0]+'  '+arr_param[1]);
			return false;
		});
		
			$('#id_pelatihan').on('select2-selecting',function(e){
			showFaktur(e.object.id);
			$('#idpel').val(e.object.id);
			});
			$('#id_pelatihan2').on('select2-selecting',function(e){
			showFaktur2(e.object.id);
			$('#idpel2').val(e.object.id);
			});
		
		$('.dt').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		});
		//showFaktur();
		
		$('.dt').mask('99-99-9999');
  	
		$(document).on('change','#checkall',function(){
			$('input[name*="checkthis"]').prop('checked',$(this).prop('checked'));
		});
		
		$(document).on('change','#checkallppn',function(){
			$('input[name*="withppn"]').prop('checked',$(this).prop('checked'));
		});
		
		$(document).on('submit','#form-posting',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					showFaktur('');
					
				});		
			}else{
				showFaktur('');
			}
		});
		
		$(document).on('reset','#form-posting',function(){
			resetForm($('#form-posting'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
		});
		
		showFaktur('');
		
		$(document).on('submit','#form-posting2',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					showFaktur2('');
					
				});		
			}else{
				showFaktur2('');
			}
		});
		
		$(document).on('reset','#form-posting2',function(){
			resetForm($('#form-posting2'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
		});
		
		
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					showFaktur('');
				break;
				case 1:
					showFaktur2('');
				break;
				case 2:
					showFaktur3('');
				break;
				
			}
		});
		
		showSetting();
	});
	
	function showFaktur(vid){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>dashboard/page/data/postingpeserta',
			data:{ '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>','id_pelatihan':vid
			 },
			beforeSend:function(){
				$('#result').loading('stop');
				$('#result').loading();
			},
			success:function(response){
				$('#result').loading('stop');
				$('#result').html(response);
			}
		});
	}
	
	function showFaktur2(vid){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>dashboard/page/data/postingpeserta2',
			data:{'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>','id_pelatihan':vid
			  },
			beforeSend:function(){
				$('#result2').loading('stop');
				$('#result2').loading();
			},
			success:function(response){
				$('#result2').loading('stop');
				$('#result2').html(response);
			}
		});
	}
	
	
</script>
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#posting" data-toggle="tab"><i class="icon-pencil"></i> Daftar Calon Peserta Pelatihan</a></li>
	<li class=""><a href="#posting2" data-toggle="tab"><i class="icon-pencil"></i> Daftar Peserta Terverifikasi</a></li>
</ul><br>
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="posting">
       <div class="row-fluid">
			<div class="span12">
				<div class="form-group">
						<label for="id_pelatihan">Nama Kegiatan Pelatihan </label>
						<div>
						<input type="text" name="id_pelatihan" id="id_pelatihan" class="form-control" style="width:650px" />
						</div>
					</div>
					 <form id="form-print" name="form-print" target="_blank" action="<?=base_url()?>dashboard/page/pdf/kartu?size=A4&o=L" method="POST" >
			   <input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

					<input type="hidden" id="id_pelatihan_print" name="id_pelatihan_print" value="" />
					<input type="hidden" id="id_peserta_print" name="id_peserta_print" value="" />
					
				</form>
				<form name="form-posting" id="form-posting" method="post" action="<?=base_url()?>dashboard/page/postingpeserta" enctype="multipart/form-data">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

					<div class="row-fluid">
						<div class="pull-right">
							<div class="spinner"></div>
							<input type="submit" id="submit" name="submit" value="POSTING" class="btn btn-primary" />
							<!--<a target="_blank" href="<?=base_url()?>dashboard/page/pdf/postingpeserta"><input type="button" id="btn-export" value="Cetak PDF" class="btn btn-small" />
</a>-->
						</div>
					</div>
					
				
				
					<div id="result"  style="overflow:auto;width:100%">
					
					</div>
				</form>
			</div>
	   </div>
	</div>
	<div class="tab-pane" id="posting2">
       <div class="row-fluid">
			<div class="span12">
				<div class="form-group">
						<label for="id_pelatihan2">Nama Kegiatan Pelatihan </label>
						<div>
						<input type="text" name="id_pelatihan2" id="id_pelatihan2" class="form-control" style="width:650px" />
						</div>
					</div>
				<form name="form-posting2" id="form-posting2" method="post" action="<?=base_url()?>dashboard/page/unpostingpeserta" enctype="multipart/form-data">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

					<div class="row-fluid">
						<div class="pull-right">
							<div class="spinner"></div>
							<input type="submit" id="submit" name="submit" value="Kembalikan Posting" class="btn btn-warning" />
						<!--	<a target="_blank" href="<?=base_url()?>dashboard/page/pdf/unpostingpeserta"><input type="button" id="btn-export" value="Cetak PDF" class="btn btn-small" />
</a>-->
						</div>
					</div>
					<div id="result2" style="overflow:auto;width:100%">
					
					</div>
				</form>
			</div>
	   </div>
	</div>
</div>

<