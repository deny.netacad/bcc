<script>$(document).ready(function(){			refreshData();			$(document).on('click','a[Title="Edit"]',function(){		var $this = $('a[Title="Edit"]');		var index = $this.index($(this));		var vusername = $this.eq(index).attr('data');		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/peserta_form',
			type:'post',
			data:{ 'username': vusername },
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$(document).on('click','a[Title="Delete"]',function(){		var $this = $('a[Title="Delete"]');		var index = $this.index($(this));		var vusername = $this.eq(index).attr('data');		if(confirm('Hapus Data...?')){			$.ajax({				url:'<?=base_url()?><?=$this->modules?>/page/delPeserta',				type:'post',				data:{ 'username': vusername },				beforeSend:function(){},				success:function(response){					var ret = $.parseJSON(response);					alert(ret.text);					refreshData();				}			});		}	});		$(document).on('click','#btn_add',function(){		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/peserta_form',
			type:'post',
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$(document).on('click','#btnfilter',function(){		refreshData();	});
		});function refreshData(){		$.ajax({				type:'post',				url:'<?=base_url()?><?=$this->modules?>/page/data/peserta',				data:{ 			'per_page':$('#per_page').val(),'cari':$('#cari').val()			,'username':$('#username').val()			,'email':$('#email').val()			,'user_role':$('#user_role').val()			,'dob':$('#dob').val()		},				beforeSend:function(){						$('#spinner').loading('stop');						$('#spinner').loading();				},		success:function(response){						$('#spinner').loading('stop');			$('#result').html(response);				}		});}	</script><ul class="breadcrumb">  <li><a href="#">PESERTA</a> <span class="divider"></span></li>  <li class="active">DAFTAR PESERTA</li></ul><ul class="nav nav-tabs" id="myTab">	<li class="active"><a href="#daftar" data-toggle="tab"><i class="icon-table"></i> Daftar Peserta</a></li></ul><div class="tab-content" style="overflow:visible">	<div class="tab-pane active" id="daftar">		<div class="row">		<br>			<div class="col-md-12">				<div class="box box-warning">				<div class="box-body">				<div class="form-group">					<label for="username">Berdasarkan Nama</label>					<input type="text" class="form-control" name="username" id="username" placeHolder="Nama Pengguna" style="width:90%" />				</div>				<div class="form-group">					<label for="dob">Berdasarkan Tanggal Lahir</label>					<input type="text" class="form-control" name="dob" id="dob" placeHolder="Tanggal Lahir" style="width:90%" />				</div>																<div class="form-group">					<label for="email">Berdasarkan Email</label>					<input type="text" class="form-control" name="email" id="email" style="width:90%" />				</div>				<!--<div class="form-group">					<label for="dip_catid">Berdasarkan Role</label>					<select id="user_role" name="user_role" class="form-control">						<option value="">-Semua-</option>						<option value="1">PPID Kabupaten</option>						<option value="2">PPID SKPD</option>						<option value="3">Admin SKPD</option>						<option value="4">Dokumentasi</option>						<option value="5">Meja Informasi</option>					</select>				</div>-->				<?php				if($this->session->userdata('role')==1){				?>			<div class="form-group">				<label for=""></label>				<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/peserta?size=A1"><i class="fa fa-file-pdf-o"></i></a>			</div>			<?php			}			?>							<div class="form-group">				<input type="button" id="btnfilter" name="btnfilter" value="FILTER" class="btn btn-warning" />							<input type="button" id="btn_add" name="btn_add" value="TAMBAH PESERTA" class="btn btn-primary" />				<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/peserta?size=A1"><i class="icon icon-print"></i></a>				<div id="spinner" style="display:inline-block"></div>			 </div>		 </div>			</div>		</div>		  		<div id="result"></div>	</div></div><!-- Modal --><div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">  <div class="modal-dialog modal-lg">	<div class="modal-content">	  <div class="modal-header">		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-remove"></i></button>		<h4 class="modal-title" id="myModalLabel">UPDATE PESERTA</h4>	  </div>	  <div class="modal-body">		<div class="video" id="div-detail">					</div>	  </div>	</div>  </div></div>