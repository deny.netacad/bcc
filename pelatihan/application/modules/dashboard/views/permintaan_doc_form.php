<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url()?>assets/ckeditor/adapters/jquery.js"></script>
<script>
	function dipcatFormatResult(data) {
		var markup= "<li>"+ data.dip_catid+"-"+data.dip_cat+"</li>";
		return markup;
	}

	function dipcatFormatSelection(data) {
		return data.dip_cat;
	}

	function dipfoiFormatResult(data) {
		var markup= "<li>"+ data.dip_foi+"</li>";
		return markup;
	}

	function dipfoiFormatSelection(data) {
		return data.dip_foi;
	}
	
	function diptoiFormatResult(data) {
		var markup= "<li>"+ data.dip_toi+"</li>";
		return markup;
	}

	function diptoiFormatSelection(data) {
		return data.dip_toi;
	}
	
	$(document).ready(function(){
		
		$('.date').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		}).on('changeDate',function(){
		});
		
		$("#form1 #dip_catid").select2({
			id: function(e) { return e.dip_catid }, 
			placeholder: "Pilih Kategori",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?><?=$this->modules?>/ajaxfile/caridipcat",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.result;
					return {results: data.rows, more: more};
				}
			},
			formatResult: dipcatFormatResult, 
			formatSelection: dipcatFormatSelection
		});
		
		$("#form1 #dip_foi").select2({
			id: function(e) { return e.dip_foi }, 
			placeholder: "Bentuk Informasi",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?><?=$this->modules?>/ajaxfile/caridipfoi",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.result;
					return {results: data.rows, more: more};
				}
			},
			formatResult: dipfoiFormatResult, 
			formatSelection: dipfoiFormatSelection
		});
		
		$("#form1 #dip_toi").select2({
			id: function(e) { return e.dip_toi }, 
			placeholder: "Jenis Informasi",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?><?=$this->modules?>/ajaxfile/caridiptoi",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.result;
					return {results: data.rows, more: more};
				}
			},
			formatResult: diptoiFormatResult, 
			formatSelection: diptoiFormatSelection
		});
		
		$('#alert1').hide();
		
		
		var ck;
		
		var config = {
			toolbar:'Basic',
			width:'100%',
			height:300
		};
		ck = CKEDITOR.instances.dip_desc;
		
		if(ck){
			CKEDITOR.remove(ck);
		}
		
		$('#dip_desc').ckeditor(config);
		
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					$('#form1').trigger('reset');
					$('#myModal').modal('hide');
					refreshData();
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			$('#idparent').select2('val','');
			$('#idparent').select2('focus');
		});
		
	});
	
</script>
	
<form id="form1" name="form1" action="<?=base_url()?><?=$this->modules?>/page/saveDip" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" id="id_edit" name="id_edit" value="" />
	<div class="row-fluid">
		<div class="span6">
			<div class="accordion-heading">
				<div class="control-group">
					<label class="control-label" for="Text">Kode DIP</label>
					<div class="controls">
						<input type="text" id="dip_code" name="dip_code" style="width:80px" placeHolder="KODE DIP" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="dip_title">Judul DIP</label>
					<div class="controls">
						<input type="text" id="dip_title" name="dip_title" class="input-block-level" maxlength="100" required placeHolder="Judul dip" value="<?=$item->dip_title?>" />
					</div>
				</div>
				<div class="accordion-heading">
					<div class="control-group">
						<label class="control-label" for="dip_responsible">Penanggungjawab<br />Penerbitan</label>
						<div class="controls">
							<input type="text" name="dip_responsible" id="dip_responsible" class="input-block-level" placeHolder="Penanggungjawab Penerbitan" value="<?=$item->dip_responsible?>" />
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="dip_foi">Bentuk Informasi</label>
					<div class="controls">
						<input type="text" id="dip_foi" name="dip_foi" style="min-width:80%" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="dip_toi">Jenis Informasi</label>
					<div class="controls">
						<input type="text" id="dip_toi" name="dip_toi" style="min-width:80%" />
					</div>
				</div>
				<div class="control-group">
					Deskripsi DIP<br />
					<textarea name="dip_desc" id="dip_desc"><?=$item->dip_desc?></textarea>
				</div>
			</div>
		</div> <!-- end of span5 -->
		<div class="span6">
			<div class="control-group">
				<div class="controls">
					<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
					<input type="reset" class="btn" value="Batal" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="dip_stspub">Status Publikasi</label>
				<div class="controls">
					<input type="radio" id="dip_stspub1" name="dip_stspub" /> Draft
					<input type="radio" id="dip_stspub2" name="dip_stspub" /> Terbit
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="Text">Kategori</label>
				<div class="controls">
					<input type="text" name="dip_catid" id="dip_catid" style="min-width:90%" />
					<br />
					<input type="text" name="dip_cat" id="dip_cat" class="input-block-level" placeHolder="Kategori Baru?" style="width:90%" />
					<br />
					<input type="button" id="btncatid" value="TAMBAH KATEGORI" class="btn btn-small btn-success" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="Text">File</label>
				<div class="controls">
					<input type="file" name="dip_file" id="dip_file"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="dip_datestart">Retensi Arsip</label>
				<div class="controls">
					<input type="text" name="dip_datestart" id="dip_datestart" class="date" placeHolder="Mulai Tanggal" />
					<input type="text" name="dip_dateend" id="dip_dateend" class="date" placeHolder="Sampai Tanggal" />
				</div>
			</div>
			
		</div>
	</div>
	<div class="row-fluid">
		<div class="span11">
			
			</div>
			
		</div>
	</div>
</form>
