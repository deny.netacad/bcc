<!DOCTYPE html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <title><?=$site_title?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?=base_url()?>assets/back/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/back/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?=base_url()?>assets/back/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?=base_url()?>assets/back/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- demo style -->
        <link href="<?=base_url()?>assets/back/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?=base_url()?>assets/back/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?=base_url()?>assets/back/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?=base_url()?>assets/back/css/AdminLTE.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/front/js/plugins/lightbox/css/lightbox.css" rel="stylesheet">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

   
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <!--<link rel="shortcut icon" href="../assets/ico/favicon.png">-->
	<script src="<?=base_url()?>assets/back/js/jquery.js"></script>
    <script src="<?=base_url()?>assets/back/js/bootstrap.js"></script>
	<script src="<?=base_url()?>assets/back/js/holder.js"></script>
	<script src="<?=base_url()?>assets/back/js/enterform.js"></script>
	<link href="<?=base_url()?>assets/back/css/datepicker.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/back/js/plugins/select2/select2.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/back/css/custom.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/back/js/plugins/contextmenu/jquery.contextMenu.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/back/js/plugins/parallax-slider/css/parallax-slider.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/back/css/icomoon/style.css" rel="stylesheet">
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/select2/select2.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/debounce/jquery-debounce.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/contextmenu/jquery.contextMenu.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!--
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/highcharts/highcharts.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/highcharts/modules/exporting.js"></script>
	-->
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/parallax-slider/js/jquery.cslider.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/parallax-slider/js/modernizr.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/debounce/jquery-debounce.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/jpodtable/jquery.jpodtable.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/terbilang/jquery.terbilang.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/highcharts/highcharts.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/highcharts/modules/data.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/highcharts/modules/exporting.js"></script>
	
	<link rel="stylesheet" href="<?=base_url()?>assets/back/js/plugins/growl/css/jquery.growl.css">
	<script type="text/javascript" src="<?=base_url()?>assets/back/js/plugins/growl/js/jquery.growl.js"></script>
	<link rel="stylesheet" href="<?=$def_js?>plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
	<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
	<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
	<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>

	<script src="<?php echo base_url();?>assets/back/ckeditor/ckeditor.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
	<script src="<?=base_url()?>assets/front/js/plugins/lightbox/js/lightbox.min.js"></script>
	<script>
		$(function(){
			$.growl.error({ title: "YOUR IP ADDRESS", message: '<?=$this->input->ip_address()?>',location:'br',size:'large',duration:1000 });	
		});
	</script>

  </head>
<body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
              <a href="<?=base_url()?>" class="logo" style="font-family:verdana">
              DISNAKER
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                      
					  					  					  
					  <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tasks"></i>
                                <span class="label label-warning">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">Anda Memiliki 0 Permintaan Informasi</li>
                                <li>
								
								       
                                   
                                </li>
                                <li class="footer">
								    <a href="<?=base_url()?>dashboard/notifications">Lihat Semua Notifikasi</a>
                                </li>
                            </ul>
                        </li>
                        
                        <!-- Notifications: style can be found in dropdown.less -->
                     
                        <!-- Tasks: style can be found in dropdown.less -->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                  <span> <?=$this->session->userdata('user_name')?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
									<div class="img-circle" style="font-size:50px;color:green">
										<i class="fa fa-user"></i>
									</div>
                                    <p>
									<?=$this->session->userdata('user_name')?>  
									<small> <?=$this->session->userdata('user_realname')?></small>
                                   </p>
                                </li>
                                <!-- Menu Body -->
                                <!--
								<li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Member</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sponsor</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Jaringan</a>
                                    </div>
                                </li>
								-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                    <a href="<?=base_url()?>auth/gantipass" class="btn btn-default btn-flat" >Change Password</a>
                                        </div>
                                    <div class="pull-right">
                                         <a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                                   </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <div class="img-circle" style="font-size:50px;color:green">
								<i class="fa fa-user"></i>
							</div>
                        </div>
                        <div class="pull-left info">
                            <p><?=$this->session->userdata('user_name')?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                   <ul class="sidebar-menu">
                        <li>
                            <a href="<?=base_url()?>dashboard">
                                <i class="fa fa-home"></i> <span>Dashboard</span>
                            </a>
                        </li>
						<!--<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="fa fa-bank"></i> SKPD <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/skpd" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Daftar SKPD</a></li>
							</ul>
						</li>-->
						<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="glyphicon glyphicon-user"></i> Pendaftaran Pelatihan <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/postingpeserta" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Verifikasi Peserta Baru   </a></li>
								<li><a href="<?=base_url()?>dashboard/pelatihan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Posting Pelatihan</a></li>
								<!--<li><a href="<?=base_url()?>dashboard/kegiatan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Daftar Kegiatan</a></li>-->
							</ul>
						</li>
							<!--<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="fa fa-users"></i> Pengguna <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/pengguna" style="margin-left: 10px;"><i class="fa fa-users"></i> Manajemen Pengguna</a></li>
							</ul>
						</li>
					<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="fa fa-file-archive-o"></i> Dokumentasi Informasi <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/dip" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Daftar Informasi</a></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="fa fa-comments-o"></i> Permintaan Informasi <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/permintaan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Daftar Pemintaan</a></li>
							</ul>
						</li>
						<!--<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="fa fa-comments-o"></i> Pengajuan Keberatan <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/keberatan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Daftar Pengajuan</a></li>
							</ul>
						</li>-->
						<!--<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="fa fa-file-pdf-o"></i> Laporan <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/lap_permintaan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Laporan Permintaan</a></li>
								<li><a href="<?=base_url()?>dashboard/lap_keberatan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Laporan Keberatan</a></li>
							</ul>
						</li>-->
						
							
						<!--<li class="treeview">
							<a href="#" class="treeview-toggle" data-toggle="treeview">
								<i class="fa fa-bar-chart-o"></i> Master Update <i class="fa fa-angle-left pull-right"></i>
							</a>
						<ul class="treeview-menu pull">
								<li><a href="<?=base_url()?>dashboard/lap_permintaan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Update Artikel</a></li>
								<li><a href="<?=base_url()?>dashboard/lap_keberatan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Update Gallery</a></li>
							</ul>
						
					</ul>
					</li>-->
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Selamat Datang <?=$this->session->userdata('user_name')?></h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?php
					  if(isset($page)){
						$this->load->view($this->modules.'/'.$page);
					  }else{
						$data['modules'] = $this->session->userdata('def_mod');
						$this->load->view($this->session->userdata('def_mod').'/default',$data);
					  }
					?>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <script src="<?=base_url()?>assets/back/js/AdminLTE/app.js" type="text/javascript"></script>
		<p class="copy">Page rendered in <strong>0.1001</strong> seconds</p>
    </body>
  
</html>
