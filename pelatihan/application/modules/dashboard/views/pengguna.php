<script>$(document).ready(function(){			refreshData();			$(document).on('click','a[Title="Edit"]',function(){		var $this = $('a[Title="Edit"]');		var index = $this.index($(this));		var vuser_id = $this.eq(index).attr('data');		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/pengguna_form',
			type:'post',
			data:{ 'user_id': vuser_id },
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$(document).on('click','a[Title="Delete"]',function(){		var $this = $('a[Title="Delete"]');		var index = $this.index($(this));		var vuser_id = $this.eq(index).attr('data');		if(confirm('Hapus Data...?')){			$.ajax({				url:'<?=base_url()?><?=$this->modules?>/page/delPengguna',				type:'post',				data:{ 'user_id': vuser_id },				beforeSend:function(){},				success:function(response){					var ret = $.parseJSON(response);					alert(ret.text);					refreshData();				}			});		}	});		$(document).on('click','#btn_add',function(){		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/pengguna_form',
			type:'post',
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$(document).on('click','#btnfilter',function(){		refreshData();	});
		});function refreshData(){		$.ajax({				type:'post',				url:'<?=base_url()?><?=$this->modules?>/page/data/pengguna',				data:{ 			'per_page':$('#per_page').val(),'cari':$('#cari').val()			,'user_id':$('#user_id').val()			,'user_name':$('#user_name').val()			,'user_email':$('#user_email').val()			,'user_role':$('#user_role').val()		},				beforeSend:function(){						$('#spinner').loading('stop');						$('#spinner').loading();				},		success:function(response){						$('#spinner').loading('stop');			$('#result').html(response);				}		});}	</script><ul class="breadcrumb">  <li><a href="#">PENGGUNA</a> <span class="divider"></span></li>  <li class="active">DAFTAR PENGGUNA</li></ul><ul class="nav nav-tabs" id="myTab">	<li class="active"><a href="#daftar" data-toggle="tab"><i class="icon-table"></i> Daftar Pengguna</a></li></ul><div class="tab-content" style="overflow:visible">	<div class="tab-pane active" id="daftar">		<div class="row">			<div class="col-md-6">				<div class="form-group">
					<label for="no_sep">Berdasarkan Waktu</label>
					<input type="text" class="form-control" name="user_id" id="user_id" placeHolder="ID PENGGUNA" />
				</div>				<div class="form-group">					<label for="dip_title">Berdasarkan Nama</label>					<input type="text" class="form-control" name="user_name" id="user_name" placeHolder="Nama Pengguna" style="width:90%" />				</div>							</div>			<div class="col-md-6">				<div class="form-group">					<label for="dip_catid">Berdasarkan Email</label>					<input type="text" class="form-control" name="user_email" id="user_email" style="width:90%" />				</div>				<div class="form-group">					<label for="dip_catid">Berdasarkan Role</label>					<select id="user_role" name="user_role" class="form-control">						<option value="">-Semua-</option>						<option value="1">PPID Kabupaten</option>						<option value="2">PPID SKPD</option>						<option value="3">Admin SKPD</option>						<option value="4">Dokumentasi</option>						<option value="5">Meja Informasi</option>					</select>				</div>			</div>			<?php				if($this->session->userdata('role')==1){				?>			<div class="form-group">				<label for=""></label>				<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/pengguna?size=A4"><i class="fa fa-file-pdf-o"></i></a>			</div>			<?php			}			?>		</div>		<div class="row">			<div class="span12 text-center">				<input type="button" id="btnfilter" name="btnfilter" value="FILTER" class="btn btn-warning" />			</div>		</div>		<div class="row">			<div class="pull-right">				<input type="button" id="btn_add" name="btn_add" value="TAMBAH PENGGUNA" class="btn btn-primary" />				<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/pengguna?size=A4"><i class="icon icon-print"></i></a>				<div id="spinner" style="display:inline-block"></div>			 </div>		  </div>		<div id="result"></div>	</div></div><!-- Modal --><div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">  <div class="modal-dialog modal-lg">	<div class="modal-content">	  <div class="modal-header">		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-remove"></i></button>		<h4 class="modal-title" id="myModalLabel">UPDATE PENGGUNA</h4>	  </div>	  <div class="modal-body">		<div class="video" id="div-detail">					</div>	  </div>	</div>  </div></div>