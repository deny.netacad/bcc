<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url()?>assets/ckeditor/adapters/jquery.js"></script>
<script>

</script>
<script>
$(document).ready(function(){
	$('#thn').datepicker({
		format:'yyyy',
		autoclose:true,		minViewMode:2,		ViewMode:2
	}).on('changeDate',function(){
		refreshData();
	});

	$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					refreshData();
				break;			
			}
	});		
	refreshData();
	
});
	
	$(document).ready(function(){
		
		$('#alert1').hide();
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					$('#form1').trigger('reset');
					$('#myModal').modal('hide');
					refreshData();
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			$('#idparent').select2('val','');
			$('#idparent').select2('focus');
			$('#myModal').modal('hide');
		});
		
		<?php
		if($this->input->post('id_kegiatan')!=''){
		$rs = $this->db->query("select a.* from kegiatan a
		where a.nama_kegiatan='".$this->input->post('id_kegiatan')."'
		");
		$item = $rs->row();
		?>
			$('#id_edit').val('<?=$this->input->post('id_kegiatan')?>');
		<?php
		}
		?>
		
	});
	
</script>
	
<form id="form1" name="form1" action="<?=base_url()?><?=$this->modules?>/page/saveKegiatan"  method="post" enctype="multipart/form-data">
	<input type="hidden" id="id_edit" name="id_edit" value="" />
	<div class="row">
		<div class="col-md-10">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="nama_kegiatan">Nama Kegiatan Pelatihan</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="text" id="nama_kegiatan" class="form-control" name="nama_kegiatan" maxlength="100" required placeHolder="Nama Kegiatan" value="<?=$item->nama_kegiatan?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="tempat_kegiatan">Tempat Kegiatan Pelatihan</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="text" id="tempat_kegiatan" class="form-control" name="tempat_kegiatan" maxlength="100" required placeHolder="Tempat Kegiatan" value="<?=$item->tempat_kegiatan?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="jenis_pelatihan">Jenis Pelatihan Pelatihan</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="text" id="jenis_pelatihan" class="form-control" name="jenis_pelatihan" maxlength="100" required placeHolder="Jenis Pelatihan" value="<?=$item->jenis_pelatihan?>" />
					</div>
				</div>
				<div class="col-md-4">
							<div class="form-group">
								<label for="date_start">Kegiatan dari Tanggal</label>
								<div>
								<input type="text" name="date_start" id="date_start" class="form-control date" placeHolder="dari" style="width:100px" />
								</div>
							</div>		
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label for="date_end">Kegiatan sampai Tanggal</label>
								<input type="text" name="date_end" id="date_end" class="form-control date" placeHolder="sampai" style="width:100px" />
							</div>
						</div>
		</div> <!-- end of span5 -->
		<div class="col-md-12">
			<div class="form-group">
				<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
				<input type="reset" class="btn" value="Batal" />
			</div>
			
		</div>
	</div>
</form>
