
<script>
<?php
$where =' where 0=0 ';
if($this->input->post('thn')!=''){
	$where.=' and year(req_date)="'.$this->input->post('thn').'" ';
}
$skpdname = "";
if($this->input->post('skpd_id')!=''){
	$where.=' and skpd_id="'.$this->input->post('skpd_id').'" ';
	$rsskpd = $this->db->get_where("ref_skpd",array("skpd_id"=>$this->input->post('skpd_id')));
	$itemskpd = $rsskpd->row();
	$skpdname = ucwords(strtolower($itemskpd->skpd_name));
}
$rs = $this->db->query('SELECT SUM(1) AS "total",SUM(IF(req_sts=1,1,0)) AS "diproses",
SUM(IF(req_sts=2,1,0)) AS "selesai",
SUM(IF(req_sts=3,1,0)) AS "ditolak"
FROM req_keberatan $where');
$item = $rs->row();
$total = $item->total;
$diproses = round(($item->diproses/$item->total)*100,2);
$selesai = round(($item->selesai/$item->total)*100,2);
$ditolak = round(($item->ditolak/$item->total)*100,2);
?>

		 $('#graph2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Pengajuan Keberatan <?=$skpdname?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Permintaan Informasi',
            data: [
                ['Selesai',   <?=number_format($selesai)?>],
                {
                    name: 'Ditolak',
                    y: <?=number_format($ditolak)?>,
                    sliced: true,
                    selected: true
                },
                ['Diproses',    <?=number_format($diproses)?>],
            ]
        }]
    });
		

    




</script>
<div class="row-fluid">
	<?php
	#echo "<pre>";
	#print_r($this->session->userdata);
	#echo "</pre>";
	?>
	<div align="center">
		<h4>DATA STATISTIK</h4>
	</div>
</div>
<div id="graph2" style="width:95%"></div>

</div>
