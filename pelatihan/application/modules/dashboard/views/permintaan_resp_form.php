<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url()?>assets/ckeditor/adapters/jquery.js"></script>
<?php
$rs = $this->db->query("
select a.*,b.skpd_name from req_info a
left join ref_skpd b on a.skpd_id=b.skpd_id
where a.req_id='".$this->input->post('req_id')."'");
$item = $rs->row();
?>
<script>
	function skpdFormatResult(data) {
        var markup= "<li>"+ data.skpd_id+"-"+data.skpd_name+"</li>";
        return markup;
    }

    function skpdFormatSelection(data) {
        return data.skpd_name;
    }	
	
	$(document).ready(function(){
		
		$('.date').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		}).on('changeDate',function(){
		});
		
		
		$('#alert1').hide();
		
		$("#skpd_id").select2({
			id: function(e) { return e.skpd_id }, 
			placeholder: "Pilih SKPD",
			minimumInputLength: 0,
			multiple: false,
			allowClear:true,
			ajax: { 
				url: "<?=base_url()?><?=$this->modules?>/ajaxfile/cariskpd",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.result;
					return {results: data.rows, more: more};
				}
			},
			formatResult: skpdFormatResult, 
			formatSelection: skpdFormatSelection
		});
		
		
		
		var ck;
		
		var config = {
			toolbar:'Basic',
			width:'100%',
			height:200
		};
		ck = CKEDITOR.instances.req_respon;
		
		if(ck){
			CKEDITOR.remove(ck);
		}
		
		$('#req_respon').ckeditor(config);
		
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					$('#form1').trigger('reset');
					$('#myModal2').modal('hide');
					refreshData();
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('#form1 #req_sts').select2('val','');	
			$('#form1 #skpd_id').select2('val','');	
		});
		
		$('#form1 #req_sts').select2();
		
		$('#fileupload').fileupload({
			dataType: 'json',
			formData: { 'filename':'<?=date('YmdHis')?>' },
			done: function (e, data) {
				 var vfilepath = data.result.upload_data.full_path;
				var vfilename = data.result.upload_data.file_name;
				var vhtml = '<div><input type="hidden" name="dip_file[]" value="'+vfilename+'" />'+vfilename+' <a class="delthis" data="'+vfilepath+'" href="#">[x]</a></div>';
				$('#progress .bar').after(vhtml);
				$('#progress .bar').html('');
				$('#progress .bar').css('width','0%');
				
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('#progress .bar').css(
					'width',
					progress + '%'
				);
				
			}
		});
		
	});
	
</script>
	
<form id="form1" name="form1" action="<?=base_url()?><?=$this->modules?>/page/saveResponse" class="form-vertical" method="post" enctype="multipart/form-data">
	<input type="hidden" id="req_id" name="req_id" value="<?=$this->input->post('req_id')?>" />
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label" for="no_sep">Keterangan</label>
				<textarea name="req_respon" id="req_respon"><?=$item->req_respon?></textarea>
			</div>
		</div> <!-- end of span5 -->
		<div class="col-md-6">
			<div class="form-group">
				<label></label>
				<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
				<input type="reset" class="btn" value="Batal" />
			</div>
			<div class="form-group">
				<label class="control-label" for="skpd_id">Diteruskan ke SKPD</label>
				<input type="hidden" id="skpd_id" name="skpd_id" style="min-width:90%" class="form-control" />
				<script>
				$('#form1 #skpd_id').select2('data',{ 'skpd_id':'<?=$item->skpd_id?>','skpd_name':'<?=$item->skpd_name?>' });
				</script>
			</div>
			<div class="form-group">
				<label for="req_sts">Status</label>
				<div>
				<select id="req_sts" name="req_sts" style="width:90%" class="form-control">
					<option value="">Pending</option>
					<option value="1" <?=(($item->req_sts==1)?"selected":"")?>>Diproses</option>
					<option value="2" <?=(($item->req_sts==2)?"selected":"")?>>Diterima</option>
					<option value="3" <?=(($item->req_sts==3)?"selected":"")?>>Ditolak</option>
				</select>
				</div>
			</div>
			<div class="form-group">
					<label class="control-label" for="Text"></label>
					<span class="btn btn-success fileinput-button">
						<i class="fa fa-upload"></i>
						<span> Upload File...</span>
						<input id="fileupload" type="file" name="dip_file" data-url="<?=base_url()?>dashboard/do_upload_dipm" multiple>
					</span>
					
					<div id="progress">
						<div class="bar" style="width: 0%;"></div>
					</div>
					<?php
					if($this->input->post('req_id')!=''){
						$path = BASEPATH."../upliddir";
						foreach($rs->result() as $item2){
							if($item2->dip_file!=''){
							echo '<div><input type="hidden" name="dip_file[]" value="'.$item2->dip_file.'" />'
							.'<a href="'.base_url().'upliddir/'.$item2->dip_file.'" target="_blank">'.$item2->dip_file.'</a> <a class="delthis btn btn-danger" data="'.$path.$item2->dip_file.'" href="#"><i class="fa fa-trash-o"></i></a></div>';
							}
						}
					}
					?>
					
				</div>
		</div>
	</div>
</form>
