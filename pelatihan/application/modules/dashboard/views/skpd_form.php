<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url()?>assets/ckeditor/adapters/jquery.js"></script>
<script>
	function skpdFormatResult(data) {
        var markup= "<li>"+ data.skpd_id+"<br />"+data.skpd_name+"</li>";
        return markup;
    }

    function skpdFormatSelection(data) {
        return data.skpd_id+"-"+data.skpd_name;
    }
	
	
	$(document).ready(function(){
		$('#alert1').hide();
		
		
		$("#skpd_parentid").select2({
			id: function(e) { return e.skpd_id }, 
			placeholder: "Pilih Parent SKPD",
			minimumInputLength: 0,
			multiple: false,
			allowClear: true,
			ajax: { 
				url: "<?=base_url()?><?=$this->modules?>/ajaxfile/cariskpd",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.result;
					return {results: data.rows, more: more};
				}
			},
			formatResult: skpdFormatResult, 
			formatSelection: skpdFormatSelection
		});
		$('#skpd_parentid').select2('focus');
		<?php
		if($this->input->post('skpd_id')!=''){
		$rs = $this->db->query("select a.*,ifnull(b.skpd_name,'') as skpd_parentname from  ref_skpd a
		left join ref_skpd b on a.skpd_parentid=b.skpd_id
		where a.skpd_id='".$this->input->post('skpd_id')."'");
		$item = $rs->row();
		?>
		$('#id_edit').val('<?=$this->input->post('skpd_id')?>');
		$('#id_edit2').val('<?=$item->skpd_user?>');
		$('#skpd_parentid').select2('data',{'skpd_id':'<?=$item->skpd_parentid?>','skpd_name':'<?=$item->skpd_parentname?>'});
		<?php
		}
		?>
		var ck;
		
		var config = {
			width:'100%',
			height:300
		};
		ck = CKEDITOR.instances.skpd_desc;
		
		if(ck){
			CKEDITOR.remove(ck);
		}
		
		$('#skpd_desc').ckeditor(config);
		
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					$('#form1').trigger('reset');
					$('#myModal').modal('hide');
					refreshData();
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			$('#idparent').select2('val','');
			$('#idparent').select2('focus');
			$('#myModal').modal('hide');
		});
		
	});
	
</script>
<form id="form1" name="form1" action="<?=base_url()?><?=$this->modules?>/page/saveSkpd" method="post" enctype="multipart/form-data">
		<input type="hidden" id="id_edit" name="id_edit" value="" />
		<input type="hidden" id="id_edit2" name="id_edit2" value="" />
			<div class="row">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="Text">Parent SKPD</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="hidden" id="skpd_parentid" class="form-control" name="skpd_parentid" style="min-width:80%" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="skpd_id">Kode SKPD</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="text" id="skpd_id" class="form-control" name="skpd_id" maxlength="12" required placeHolder="9.9.9" value="<?=$item->skpd_id?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="skpd_name">Nama SKPD</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="text" id="skpd_name" class="form-control" name="skpd_name" maxlength="100" required placeHolder="Nama SKPD" value="<?=$item->skpd_name?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="skpd_email">E-mail SKPD</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="email" name="skpd_email" id="skpd_email" class="form-control" placeHolder="Valid Email" value="<?=$item->skpd_email?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="skpd_addr">Alamat SKPD</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<textarea name="skpd_addr" id="skpd_addr" class="form-control" style="width:80%;height:80px" placeHolder="Alamat SKPD"><?=$item->skpd_addr?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="Text">User SKPD</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="text" name="skpd_user" class="form-control" id="skpd_user" placeHolder="Username SKPD" value="<?=$item->skpd_user?>" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label" for="Text">Password SKPD</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<input type="password" name="skpd_pass" class="form-control" id="skpd_pass" placeHolder="Password SKPD" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="Text">&nbsp;</label>
					<div class="input-group col-xs-12 col-sm-8 col-md-8 col-lg-9">
						<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
						<input type="reset" class="btn" value="Batal" />
					</div>
				</div>
			</div>
		</form>