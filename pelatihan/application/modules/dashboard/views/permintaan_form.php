<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url()?>assets/ckeditor/adapters/jquery.js"></script>
<script>
	function skpdFormatResult(data) {
        var markup= "<li>"+ data.skpd_id+"-"+data.skpd_name+"</li>";
        return markup;
    }

    function skpdFormatSelection(data) {
        return data.skpd_name;
    }	
	
	$(document).ready(function(){
		
		$('.date').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		}).on('changeDate',function(){
		});
		
		
		$('#alert1').hide();
		
		$("#skpd_id").select2({
			id: function(e) { return e.skpd_id }, 
			placeholder: "Pilih SKPD",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?><?=$this->modules?>/ajaxfile/cariskpd",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.result;
					return {results: data.rows, more: more};
				}
			},
			formatResult: skpdFormatResult, 
			formatSelection: skpdFormatSelection
		});
		
		
		
		var ck;
		
		var config = {
			toolbar:'Basic',
			width:'100%',
			height:300
		};
		ck = CKEDITOR.instances.app_desc;
		
		if(ck){
			CKEDITOR.remove(ck);
		}
		
		$('#app_desc').ckeditor(config);
		
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					$('#form1').trigger('reset');
					$('#myModal').modal('hide');
					refreshData();
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			$('#skpd_id').select2('val','');
			$('#skpd_id').select2('focus');
		});
		
	});
	
</script>
	
<form id="form1" name="form1" action="<?=base_url()?><?=$this->modules?>/page/saveDip" class="form-vertical" method="post" enctype="multipart/form-data">
	<input type="hidden" id="id_edit" name="id_edit" value="" />
	<div class="row-fluid">
		<div class="span6">
			<div class="accordion-heading">
				<div class="control-group">
					<label class="control-label" for="Text">Tanggal</label>
					<div class="controls">
						<input type="text" id="app_date" name="app_date" placeHolder="Tanggal Permintaan" value="<?=date('d-m-Y H:i:s')?>" style="width:200px" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="dip_title">Informasi Yang Dimohon</label>
					<div class="controls">
						<input type="text" id="dip_title" name="dip_title" class="input-block-level" maxlength="100" required placeHolder="Informasi Yang Dimohon" value="<?=$item->dip_title?>" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="no_sep">Dokumen</label>
					<div class="controls">
						<textarea name="app_desc" id="app_desc"></textarea>
					</div>
				</div>
			</div>
		</div> <!-- end of span5 -->
		<div class="span6">
			<div class="control-group">
				<div class="controls">
					<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
					<input type="reset" class="btn" value="Batal" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="dip_stspub">Ubah Penugasan</label>
				<div class="controls">
					<input type="text" name="skpd_id" id="skpd_id" style="min-width:90%" />
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span11">
			
			</div>
			
		</div>
	</div>
</form>
