<script>
$(document).ready(function(){	
	refreshData();
	
	$(document).on('click','a[Title="Edit"]',function(){
		var $this = $('a[Title="Edit"]');
		var index = $this.index($(this));
		var vskpd_id = $this.eq(index).attr('data');
		try{
		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/skpd_form',
			type:'post',
			data:{ 'skpd_id': vskpd_id },
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});
		}catch(e){ alert(e.message); }
	});
	
	$(document).on('click','a[Title="Delete"]',function(){
		var $this = $('a[Title="Delete"]');
		var index = $this.index($(this));
		var vskpd_id = $this.eq(index).attr('data');
		if(confirm('Hapus Data...?')){
			$.ajax({
				url:'<?=base_url()?><?=$this->modules?>/page/delSkpd',
				type:'post',
				data:{ 'skpd_id': vskpd_id },
				beforeSend:function(){},
				success:function(response){
					var ret = $.parseJSON(response);
					alert(ret.text);
					refreshData();
				}
			});
		}
	});
	
	$(document).on('click','#btn_add',function(){
		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/skpd_form',
			type:'post',
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});
	});
	
	$(document).on('click','#daftar a.ajax-replace', function(){
		$.ajax({
			type:'post',
			url:$(this).attr('href'),
			data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
			beforeSend:function(){
				$('#spinner').loading('stop');
				$('#spinner').loading();
			},
			success:function(response){
				$('#spinner').loading('stop');
				$('#result').html(response);
			}
		});
		return false;
	});
	
	$('#per_page').change(function(){
		refreshData();
	});
	
	$('#cari').keyup( $.debounce(250,refreshData));
	
});

function refreshData(){	
	$.ajax({		
		type:'post',		
		url:'<?=base_url()?><?=$this->modules?>/page/data/skpd',		
		data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },		
		beforeSend:function(){			
			$('#spinner').loading('stop');			
			$('#spinner').loading();		
		},
		success:function(response){			
			$('#spinner').loading('stop');
			$('#result').html(response);		
		}	
	});
}
	
</script>

<ul class="breadcrumb">
  <li><a href="#">SKPD</a> <span class="divider"></span></li>
  <li class="active">Daftar SKPD</li>
</ul>

<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#daftar" data-toggle="tab"><i class="icon-table"></i> Daftar SKPD</a></li>
</ul>

<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="daftar">
		<br />
		<div class="row">
			<div class="col-md-12 box-body">
				<form role="form" class="form-inline" >
					<?php
					if($this->session->userdata('user_role')==1){
					?>
					<div class="form-group">
					  <label for="cari">&nbsp;</label>
					  <div class="input-group">
						  <input type="button" class="btn" id="btn_add" name="btn_add" value="TAMBAH SKPD" />
					  </div>
					</div>
					<?php
					}
					?>
					<div class="form-group">
						<label for=""></label>
						<div class="input-group">
							<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/skpd?size=A4"><i class="fa fa-file-pdf-o"></i></a>
							<div id="spinner" style="display:inline-block"></div>
						</div>
					</div>
					<div class="form-group pull-right">
					  <label for="cari">Pencarian</label>
					  <div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-search"></i>
						  </div>
						  <input type="text" class="form-control" id="cari" name="cari" style="min-width:300px" placeHolder="Pencarian" />
					  </div>
					</div>
					<div class="form-group">
					  <label for="member_dob">Tampilan </label>
					  <div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-list-ol"></i>
							</div>
							<select id="per_page" class="form-control" name="per_page" style="width:60px">
							  <option value="">-</option>
							  <option value="4">4</option>
							  <option value="25">25</option>
							  <option value="50">50</option>
							</select>
					  </div>
					  <label for="member_dob">Halaman</label>
					</div>
				</form>
			</div>
		</div>
		<br />
		<div class="row">
			<div id="result" class="col-md-12" style="overflow:auto"></div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-remove"></i></button>
		<h4 class="modal-title" id="myModalLabel">SKPD</h4>
	  </div>
	  <div class="modal-body">
		<div class="video" id="div-detail">
			
		</div>
	  </div>
	</div>
  </div>
</div>