<script>$(document).ready(function(){			refreshData();			$(document).on('click','a[Title="Edit"]',function(){		var $this = $('a[Title="Edit"]');		var index = $this.index($(this));		var vid_kegiatan = $this.eq(index).attr('data');		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/kegiatan_form',
			type:'post',
			data:{ 'id_kegiatan': vid_kegiatan },
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$(document).on('click','a[Title="Delete"]',function(){		var $this = $('a[Title="Delete"]');		var index = $this.index($(this));		var vid_kegiatan = $this.eq(index).attr('data');		if(confirm('Hapus Data...?')){			$.ajax({				url:'<?=base_url()?><?=$this->modules?>/page/delkegiatan',				type:'post',				data:{ 'id_kegiatan': vid_kegiatan },				beforeSend:function(){},				success:function(response){					var ret = $.parseJSON(response);					alert(ret.text);					refreshData();				}			});		}	});		$(document).on('click','#btn_add',function(){		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/kegiatan_form',
			type:'post',
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$(document).on('click','#btnfilter',function(){		refreshData();	});
		});function refreshData(){		$.ajax({				type:'post',				url:'<?=base_url()?><?=$this->modules?>/page/data/kegiatan',				data:{ 			'per_page':$('#per_page').val(),'cari':$('#cari').val()			,'id_kegiatan':$('#id_kegiatan').val()			,'nama_kegiatan':$('#nama_kegiatan').val()			,'tgl_kegiatan':$('#tgl_kegiatan').val()			,'tempat_kegiatan':$('#tempat_kegiatan').val()					},				beforeSend:function(){						$('#spinner').loading('stop');						$('#spinner').loading();				},		success:function(response){						$('#spinner').loading('stop');			$('#result').html(response);				}		});}	</script><ul class="breadcrumb">  <li><a href="#">KEGIATAN PELATIHAN</a> <span class="divider"></span></li>  <li class="active">DAFTAR KEGIATAN PELATIHAN</li></ul><ul class="nav nav-tabs" id="myTab">	<li class="active"><a href="#daftar" data-toggle="tab"><i class="icon-table"></i>Sunting Daftar Kegiatan Pelatihan</a></li></ul><div class="tab-content" style="overflow:visible">	<div class="tab-pane active" id="daftar">		<div class="row">						<div class="col-md-6">			<div class="form-group">					<label for="nama_kegiatan">Berdasarkan Nama Pelatihan</label>					<input type="text" class="form-control" name="nama_kegiatan" id="nama_kegiatan" placeHolder="Nama Kegiatan" style="width:90%" />				</div>				<div class="form-group">					<label for="tempat_kegiatan">Berdasarkan Tempat Pelatihan</label>					<input type="text" class="form-control" name="tempat_kegiatan" id="tempat_kegiatan" placeHolder="Tempat Kegiatan"style="width:90%" />				</div>					<div class="form-group">					<label for="tgl_kegiatan">Berdasarkan Tanggal Pelatihan</label>					<input type="text" class="form-control" name="tgl_kegiatan" id="tgl_kegiatan" placeHolder="Tanggal Kegiatan" style="width:90%" />				</div>							</div>			<?php				if($this->session->userdata('role')==1){				?>			<div class="form-group">				<label for=""></label>				<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/kegiatan?size=A4"><i class="fa fa-file-pdf-o"></i></a>			</div>			<?php			}			?>		</div>		<div class="row">			<div class="span12 text-center">				<input type="button" id="btnfilter" name="btnfilter" value="FILTER" class="btn btn-warning" />			</div>		</div>		<div class="row">			<div class="span30 text-right">				<input type="button" id="btn_add" name="btn_add" value="TAMBAH KEGIATAN PELATIHAN" class="btn btn-primary" />				<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/kegiatan?size=A4"><i class="icon icon-print"></i></a>				<div id="spinner" style="display:inline-block"></div>			 </div>		  </div>		<div id="result"></div>	</div></div><!-- Modal --><div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">  <div class="modal-dialog modal-lg">	<div class="modal-content">	  <div class="modal-header">		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-remove"></i></button>		<h4 class="modal-title" id="myModalLabel">UPDATE KEGIATAN PELATIHAN</h4>	  </div>	  <div class="modal-body">		<div class="video" id="div-detail">					</div>	  </div>	</div>  </div></div>