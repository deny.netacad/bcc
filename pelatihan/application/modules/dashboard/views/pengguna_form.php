<script>
	
	$(document).ready(function(){
		
		$('#alert1').hide();
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					$('#form1').trigger('reset');
					$('#myModal').modal('hide');
					refreshData();
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			$('#idparent').select2('val','');
			$('#idparent').select2('focus');
			$('#myModal').modal('hide');
		});
		
		<?php
		if($this->input->post('user_id')!=''){
		$rs = $this->db->query("select a.* from ref_user a
		where a.user_id='".$this->input->post('user_id')."'
		");
		$item = $rs->row();
		?>
			$('#id_edit').val('<?=$this->input->post('user_id')?>');
		<?php
		}
		?>
		
	});
	
</script>
	
<form id="form1" name="form1" action="<?=base_url()?><?=$this->modules?>/page/savePengguna"  method="post" enctype="multipart/form-data">
	<input type="hidden" id="id_edit" name="id_edit" value="" />
	<div class="row">
		<div class="col-md-6">
				<div class="form-group">
					<label for="Text">User / Pengguna</label>
					<input type="text" class="form-control" id="user_name" name="user_name" style="width:200px" placeHolder="User/Pengguna" required value="<?=$item->user_name?>" />
				</div>
				<div class="form-group">
					<label for="user_email">Email</label>
					<input type="email" id="user_email" name="user_email" class="form-control" maxlength="100" required placeHolder="Email Pengguna" value="<?=$item->user_email?>" />
				</div>
				<div class="form-group">
					<label for="dip_responsible">Peran Pengguna</label>
					<select id="user_role" name="user_role" class="form-control">
						<?php
						if($this->session->userdata('user_role')==1){
						?>
						<option value="1" <?=(($item->user_role==1)?"selected":"")?>>PPID Kabupaten</option>
						<?php
						}
						?>
						<option value="2" <?=(($item->user_role==2)?"selected":"")?>>PPID SKPD</option>
						<option value="3" <?=(($item->user_role==3)?"selected":"")?>>Admin SKPD</option>
						<option value="4" <?=(($item->user_role==4)?"selected":"")?>>Dokumentasi</option>
						<option value="5" <?=(($item->user_role==5)?"selected":"")?>>Meja Informasi</option>
					</select>
				</div>
				<div class="form-group">
					<label for="Text">Password Pengguna</label>
					<input type="password" id="user_pass" class="form-control" name="user_pass" style="width:200px" placeHolder="Password" required value="" />
				</div>	
				

		</div> <!-- end of span5 -->
		<div class="col-md-6">
			<div class="form-group">
				<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
				<input type="reset" class="btn" value="Batal" />
			</div>
			
		</div>
	</div>
</form>
