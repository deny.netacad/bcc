<script>
$(document).ready(function(){
	$('#alert2').hide();
	
});
</script>
<div id="alert2" class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Sukses!</strong> Data telah dihapus
</div>
<?php
	$where = " ";
	$per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= " where (a.tempat like '%".$cari."%' or a.judul like '%".$cari."%' )";
	#echo $where;
	
	$n = intval($this->uri->segment(5));
	$q = "select *,date_format(a.tgl_mulai,'%d %M %Y') as log,date_format(a.tgl_ahir,'%d %M %Y') as log2 from ref_pelatihan a

	";
	$rs = $this->db->query("$q $where ");
	$row = $rs->row_array();
	$totrows = $rs->num_rows();

	$config['base_url'] = base_url().'auth/page/data/pelatihan';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("$q $where  order by a.id limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
</div>
<img id="noimage" style="display:none" />
<table class="table table-hover">
  <colgroup>
	<col class="con0" style="auto" />
	<col class="con0" style="auto" />
	<col class="con0" style="auto"/>
	<col class="con0" style="width:50px" />
  </colgroup>
  <thead class="breadcrumb">
	<tr>
	  <th>#</th>
	  <th>JUDUL</th>
	  <th>TANGGAL PELAKSANAAN</th>
	 
	  <th>TEMPAT</th>
	  <th><div class="text-center">PROSES</div></th>
	</tr>
  </thead>
  <tbody>
	<?php
	foreach($data['posts']->result() as $item){ $n++;
	?>
	<tr>
	  <td align="center"><?=$n?>.</td>
	  <td>
	  <span class="ed1" style="display:none"><?=$item->id?></span>
	  <div class="text-left"><?=$item->judul?></div>
	  </td>
	  <td>
	  <div class="text-center"><?=$item->log?></div>
	    <div class="text-center"><?=$item->log2?></div> 
	  </td>
	  
	   <td>
	  <div class="text-left"><?=$item->tempat?></div>
	   
	  </td>
	  <td>
		<div class="text-center">
			<a href="#" title="Edit" data="<?=$item->id?>"><i class="icomoon-pencil2"></i></a>
			<a href="#" title="Delete" data="<?=$item->id?>"><i class="icomoon-remove red"></i></a>
			
		</div>
	  </td>
	</tr>
	<?php
	}
	?>
  </tbody>
</table>
<span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
<?=$data['pagination']?>
