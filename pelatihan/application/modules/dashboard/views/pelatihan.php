<script>
	
	$(document).ready(function(){
	
	
		try{
			$('#form1').enterform();
			$('#kode').focus();
		}catch(e){
			alert(e);
		}
		
		var config = {
				toolbar:'Basic',
				width:'100%',
				height:200
			};
			ck = CKEDITOR.instances.isi;
			
			if(ck){
				CKEDITOR.remove(ck);
			}
			
			$('#isi').ckeditor(config);
			
		$('.date').datepicker({
		format:'dd-mm-yyyy',
		autoclose:true
		}).on('changeDate',function(){
		
		});
	

		$(document).on('submit','#form1',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					$('#div-detail').html('');
					var ret = $.parseJSON(data);
					alert(ret.text);
					refreshData();
					resetForm($('#form1'));	
				});		
			}else{
				refreshData();
			}
		});
		
		$(document).on('reset','#form1',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('#kode').focus();
		});
		
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup( $.debounce(250,refreshData));
		
	

		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 
				'per_page':$('#per_page').val(),
				'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			
				},
				beforeSend:function(){
					$('#spinner').loading('stop');
					$('#spinner').loading();
				},
				success:function(response){
					$('#spinner').loading('stop');
					$('#result').html(response);
				}
			});
			return false;
		});
		
		$(document).on('click','a[title="Edit"]',function(){
			var $this = $('a[title="Edit"]');
			var index = $this.index($(this));
			$.ajax({
				type:'post',
				url:'<?=base_url()?>dashboard/page/editPelatihan',
				data:{ 
				'id':$this.eq(index).attr('data'),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			
				},
				beforeSend:function(){
					$('#spinner').loading('stop');
					$('#spinner').loading();
				},
				success:function(response){
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					var keynot = new Array("");
					var keyin = new Array("id");
					var keyout = new Array("id_edit");
					for(attrname in ret){
						if($.inArray(attrname,keynot)==-1){
							$('#'+attrname).val(ret[attrname]);
						}
					}
					for(attrname in ret){
						if($.inArray(attrname,keyin)!=-1){
							$('#'+keyout[$.inArray(attrname,keyin)]).val(ret[attrname]);
						}
					}
						$('#nama_file').val(ret.dip_file);
					//alert(ret.dip_file);
						$('#div-detail').html('<embed src="<?=base_url()?>upliddir/'+ret.dip_file+'" width="100%" style="min-height:350px" />');

				}
			});
		});
		
		$(document).on('click','a[title="Delete"]',function(){
			var $this = $('a[title="Delete"]');
			var index = $this.index($(this));
			if(confirm('Yakin hapus item...?')){
				$.ajax({
					type:'post',
					url:'<?=base_url()?>dashboard/page/delPelatihan',
					data:{
					'id':$this.eq(index).attr('data')
			
					},
					beforeSend:function(){
						$('#spinner').loading('stop');
						$('#spinner').loading();
					},
					success:function(response){
						
						$('#spinner').loading('stop');
						var ret = $.parseJSON(response);
						alert(ret.text);
						
						refreshData();
					}
				});
			}
		});
		
		$('#fileupload').fileupload({
				dataType: 'json',
				formData: { 'filename':'<?=date('YmdHis')?>' },
				done: function (e, data) {
					 var vfilepath = data.result.upload_data.full_path;
					var vfilename = data.result.upload_data.file_name;
					var suggestTitle = vfilename.replace(/-/g," ").split(".");
					
					$('#form1 #dip_title').val(suggestTitle[0]);
					var vhtml = '<div><input type="hidden" name="dip_file[]" value="'+vfilename+'" /><a href="<?=base_url()?>upliddir/'+vfilename+'" target="_blank">'+vfilename+'</a>&nbsp;<a class="delthis btn btn-danger" data="'+vfilepath+'" href="#"><i class="fa fa-trash-o"></i></a></div>';
					$('#myModal #progress .bar').after(vhtml);
					$('#myModal #progress .bar').html('');
					$('#myModal #progress .bar').css('width','0%');
					var filetype = data.result.upload_data.file_ext.replace('.','');
					$('#nama_file').val(data.result.upload_data.file_name);
					$('#path_file').val(data.result.upload_data.file_path);
					$('#size_file').val(data.result.upload_data.file_size);
					//alert(filetype);
				if(data.result.upload_data.file_ext.replace('.','')=="pdf" || data.result.upload_data.file_ext.replace('.','')=="jpeg" ){
					$('#div-detail').html('<embed src="'+data.result.upload_data.full_path.replace('D:/html/disperindag/','../')+'" width="100%" style="min-height:350px"/>');
					//$('#div-detail').html('<embed src="<?=base_url()?>upliddir/'+ret.dip_file+'" width="100%" style="min-height:350px" />');

			
				}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#myModal #progress .bar').css(
						'width',
						progress + '%'
					);
					
				}
			});
		
		$('#alert1').hide();
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#kode').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
	});

function refreshData(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>dashboard/page/data/pelatihan',
		data:{ 
		'per_page':$('#per_page').val(),
		'cari':$('#cari').val(),
		'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			
		},
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});
}

</script>
<ul class="breadcrumb">
  <li><a href="#"><i class="icomoon-settings"></i> Setting</a> <span class="divider">/</span></li>
  <li class="active">Pengumuman</li>
</ul>

<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#update" data-toggle="tab"><i class="icomoon-file3"></i> Update</a></li>
	<li class=""><a href="#daftar" data-toggle="tab"><i class="icomoon-table2"></i> Daftar</a></li>
</ul>

<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
			
		<form id="form1" name="form1" action="<?=base_url()?>dashboard/page/savePelatihan" class="form-horizontal" method="post" enctype="multipart/form-data">
		
			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<div class="row-fluid">
				<div class="span9">
					<div class="accordion-heading">
					
					
						<BR><div class="control-group">
							<label class="control-label" for="rak">Judul</label>
							<div class="controls">
								<input type="text" class="form-control" id="judul" name="judul" maxlength="100" required  /><span id="info"></span> 
								<input type="hidden" class="form-control" id="nama_file" name="nama_file" maxlength="100" required  />
								<input type="hidden" class="form-control" id="path_file" name="path_file" maxlength="100" required  />
								<input type="hidden" class="form-control" id="size_file" name="size_file" maxlength="100" required  />
								
						
							</div>
						</div>
						
						<BR><div class="control-group">
							<label class="control-label" for="rak">Tempat Pelaksanaan</label>
							<div class="controls">
								<input type="text" class="form-control" id="tempat" name="tempat" maxlength="100" required  />
							</div>
						</div>
						
						<BR><div class="control-group">
							<label class="control-label" for="rak">Tanggal Pelaksanaan</label>
							<div class="controls">
								<input type="text" class="form-controls date" id="tgl_mulai" name="tgl_mulai" maxlength="100" required  /> s.d. 
						<input type="text" class="form-controls date" id="tgl_ahir" name="tgl_ahir" maxlength="100" required  />
						
							</div>
							</div>
							
							
						<br>
					
                              
                                <div class='control-group'>
                                   
                                        <textarea name="isi" id="isi" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                   
                                </div><br>
								<div class="control-group">
					<span class="btn btn-success fileinput-button">
						<i class="fa fa-upload"></i>
						<span> Upload File Pdf...</span>
						<input id="fileupload" type="file" name="dip_file"  data-url="<?=base_url()?>dashboard/do_upload_dipm" multiple>
					</span>
					<div id="progress">
						<div class="bar" style="width: 0%;"></div>
					</div>
					<div id="div-detail"></div>
					<?php 
					if($this->input->post('id')!=''){
						$path = BASEPATH."../upliddir";
						foreach($rs->result() as $item2){
							echo '<div><input type="hidden" name="dip_file[]" value="'.$item2->dip_file.'" />'
							.'<a href="'.base_url().'upliddir/'.$item2->dip_file.'" target="_blank">'.$item2->dip_file.'</a> <a class="delthis btn btn-danger" data="'.$path.$item2->dip_file.'" href="#"><i class="fa fa-trash-o"></i></a></div>';
						}
					}
					?>
				</div>
								
								
								
								
                       	<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
				</div> <!-- end of span5 -->
			</div>
		</form>
	</div>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="pull-right">
				<div id="spinner" style="display:inline-block"></div>
				<div class="input-append form-inline">
				  <input type="text" class="form-control" id="cari" name="cari" placeHolder="Cari ..." />
				  <div class="btn-group">
					<select class="form-control"  id="per_page" name="per_page" style="width:60px">
					  <option value="">-</option>
					  <option value="4">4</option>
					  <option value="25">25</option>
					  <option value="50">50</option>
					</select>
				  </div>
				</div>        
				
			 </div>
		  </div>
		<div id="result"></div>
	</div>
</div>