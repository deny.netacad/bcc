<link rel="stylesheet" href="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>
<script>
$(document).ready(function(){
	 $('#form1 #scanfile').fileupload({
		dataType: 'json',
		formData: { 'filename':'<?=date('YmdHis')?>' },
		done: function (e, data) {
			
			try{
			var vfilepath = data.result.upload_data.full_path;
			var vfilename = data.result.upload_data.file_name;
			var vhtml = '<div class="clear"></div>'+
			'<div><label>&nbsp;</label><div><input type="hidden" name="scanfile[]" value="'+vfilename+'" />'+vfilename+' <a class="delthis" data="'+vfilepath+'" href="#">[x]</a></div></div>';
			
			$('#form1 #progress .bar').after(vhtml);
			$('#form1 #progress .bar').html('');
			$('#form1 #progress .bar').css('width','0%');
			}catch(e){ alert(e.message); }
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#form1 #progress .bar').css(
				'width',
				progress + '%'
			);
			
		}
	});
	
	$('#form2 #scanfile').fileupload({
		dataType: 'json',
		formData: { 'filename':'<?=date('YmdHis')?>' },
		done: function (e, data) {
			
			try{
			var vfilepath = data.result.upload_data.full_path;
			var vfilename = data.result.upload_data.file_name;
			var vhtml = '<div class="clear"></div>'+
			'<div><label>&nbsp;</label><div><input type="hidden" name="scanfile[]" value="'+vfilename+'" />'+vfilename+' <a class="delthis" data="'+vfilepath+'" href="#">[x]</a></div></div>';
			
			$('#form2 #progress .bar').after(vhtml);
			$('#form2 #progress .bar').html('');
			$('#form2 #progress .bar').css('width','0%');
			}catch(e){ alert(e.message); }
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#form2 #progress .bar').css(
				'width',
				progress + '%'
			);
			
		}
	});
	
	$(document).on('click','a.delthis',function(){
		
		var $this = $('a.delthis');
		var index = $this.index($(this));
		
		var vdata = $this.eq(index).attr('data');
		$.ajax({
			type:'post',
			url:'<?=base_url()?>portal/delUpload',
			data:{ 'full_path': vdata },
			beforeSend:function(){
				
			},
			success:function(response){
				var ret = $.parseJSON(response);
				alert(ret.text);
				$this.eq(index).parent().parent().remove();
			}
		});
	});
	
	$("#form1").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form1').trigger('reset');
			});		
		}else{
			
			$('#form1').trigger('reset');
		}
	});
	
	$('#form1').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
	$("#form2").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form2').trigger('reset');
			});		
		}else{
			
			$('#form2').trigger('reset');
		}
	});
	
	
	$('#form2').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
});
</script>
<html>
	<head>
		<title></title>
		<style type="text/css">
			div.scrollbar {
				padding:3px;
				overflow:auto;
				width:auto;
				height:600px;
				border:1px solid grey
			}
		</style>
	</head>
	<body>
		<div class="scrollbar"><header class="main-header">
    <div class="container">
        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Dashboard</a></li>
            <li class="active">Tambah Perusahaan</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class=""><a href="#dip2" data-toggle="tab"><i class="fa fa-building"></i> Daftar Sebagai Perusahaan</a></li>
			</ul>
			<div class="tab-content">
				
				<div class="#" id="dip2">
					<div class="panel panel-primary">
						<div class="panel-heading">Form Pemohon Perusahaan</div>
						<div class="panel-body">
						<h4>DATA PERUSAHAAN</h4>
							<form id="form2" name="form2" action="<?=base_url()?>portal/sendRegister" class="form-horizontal" method="post" enctype="multipart/form-data">
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Surat Pendirian *</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="identity" id="identity" class="form-control" required="">
										<option value="5">Akta Pendirian</option>
										<option value="6">Lainnya</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">No. Surat Pendirian *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="noid" name="noid" placeholder="No. Surat Pendirian">
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Scan File Surat Pendirian *</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<span class="btn btn-success fileinput-button" style="float:left;cursor:pointer">
										<span style="cursor:pointer"><i class="fa fa-image"></i> Add files...</span>
										<input type="file" name="scanfile" class="textfield" id="scanfile" value="" data-url="portal/do_upload_scanfile" />
									</span>
								</div>
							  </div>
							  <div class="form-group">
								<label for="username" class="col-sm-2 control-label">Username</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="username" name="username" placeholder="Username">
								</div>
							  </div>
							  <div class="form-group">
								<label for="userpass" class="col-sm-2 control-label">Password</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="password" class="form-control" id="userpass" name="userpass" placeholder="Password">
								</div>
							  </div>
							  <div class="form-group">
								<label for="userpass2" class="col-sm-2 control-label">Konfirmasi Password</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="password" class="form-control" id="userpass2" name="userpass2" placeholder="Ulangi Password">
								</div>
							  </div>
							  <div class="form-group">
								<label for="fullname" class="col-sm-2 control-label">Nama Penanggung Jawab *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Nama Penanggung Jawab">
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="sex" id="sex" name="sex" class="form-control" required="">
										<option value="1">Laki-laki</option>
										<option value="2">Perempuan</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="pob" class="col-sm-2 control-label">Tempat Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="pob" name="pob" placeholder="Tempat Lahir">
								</div>
							  </div>
							  <div class="form-group">
								<label for="dob" class="col-sm-2 control-label">Tanggal Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control date" id="dob" name="dob" placeholder="dd-mm-YYYY">
								</div>
							  </div>
							  <div class="form-group">
								<label for="education" class="col-sm-2 control-label">Pendidikan Terakhir *</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="education" id="education" class="form-control" required>
										<option value="1">SD</option>
										<option value="2">SMP</option>
										<option value="3">SMA</option>
										<option value="4">Diploma</option>
										<option value="5">Sarjana</option>
										<option value="6">Pasca Sarjana</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="job" class="col-sm-2 control-label">Pekerjaan *</label>
								<div class="col-sm-2 col-md-4 col-lg-3">
									<select name="job" name="id" class="form-control" required>
										<option value="1">Wiraswasta</option>
										<option value="2">Buruh</option>
										<option value="3">Petani</option>
										<option value="4">Nelayan</option>
										<option value="5">PNS</option>
										<option value="6">Swasta</option>
										<option value="7">Pedagang</option>
										<option value="8">Pekerja rumah tangga</option>
										<option value="9">LSM</option>
										<option value="10">Tenaga Pendidik</option>
										<option value="11">Pengacara</option>
										<option value="12">Tenaga Kesehatan</option>
										<option value="13">Peneliti</option>
										<option value="14">Seniman</option>
										<option value="15">Wartawan</option>
										<option value="16">Mahasiswa/Pelajar</option>
										<option value="17">Lain-lain</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="addr" class="col-sm-2 control-label">Alamat *</label>
								<div class="col-sm-10 col-lg-4">
								  <textarea class="form-control" id="addr" name="addr" placeholder="Alamat"></textarea>
								</div>
							  </div>
							  <div class="form-group">
								<label for="city" class="col-sm-2 control-label">Kota *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="city" name="city" placeholder="Kota">
								</div>
							  </div>
							  <div class="form-group">
								<label for="postcode" class="col-sm-2 control-label">Kode Pos *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="postcode" name="postcode" placeholder="5 digit">
								</div>
							  </div>
							  <div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="email" class="form-control" id="email" name="email" placeholder="Email">
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_home" class="col-sm-2 control-label">Telp. Rumah *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_home" name="phone_home" placeholder="No. Telp Rumah">
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_mobile" class="col-sm-2 control-label">Telp. Mobile *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_mobile" name="phone_mobile" placeholder="No. HP">
								</div>
							  </div>
							 
							  <div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
								  <button type="submit" class="btn btn-ar btn-primary">Daftar</button>
								</div>
							  </div>
							</form>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div> <!-- row -->
</div>
</div>
	