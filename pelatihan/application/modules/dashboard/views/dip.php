<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script><script type="text/javascript" src="<?=base_url()?>assets/ckeditor/adapters/jquery.js"></script><script>function dipcatFormatResult(data) {
	var markup= "<li>"+ data.dip_catid+"-"+data.dip_cat+"</li>";
	return markup;
}

function dipcatFormatSelection(data) {
	return data.dip_cat;
}function dipfoiFormatResult(data) {	var markup= "<li>"+ data.dip_foi+"</li>";	return markup;}function dipfoiFormatSelection(data) {	return data.dip_foi;}$(document).ready(function(){		$('.date').datepicker({
		format:'dd-mm-yyyy',
		autoclose:true
	}).on('changeDate',function(){
		
	});		$(document).on('click','a.delthis',function(){		var $this = $('a.delthis');		var index = $this.index($(this));		var vdata = $this.eq(index).attr('data');		$.ajax({			type:'post',			url:'<?=base_url()?>dashboard/delUpload',			data:{ 'full_path': vdata },			beforeSend:function(){				$('#spinner').loading('stop');				$('#spinner').loading();			},			success:function(response){				var ret = $.parseJSON(response);				alert(ret.text);				$this.eq(index).parent().remove();			}		});	});		refreshData();			$(document).on('click','a[Title="Edit"]',function(){		var $this = $('a[Title="Edit"]');		var index = $this.index($(this));		var vdip_id = $this.eq(index).attr('data');		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/dip_form',
			type:'post',
			data:{ 'dip_id': vdip_id },
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
									
				return false;
			}
		});	});		$(document).on('click','a[Title="Delete"]',function(){		var $this = $('a[Title="Delete"]');		var index = $this.index($(this));		var vdip_id = $this.eq(index).attr('data');		if(confirm('Hapus Data...?')){			$.ajax({				url:'<?=base_url()?><?=$this->modules?>/page/delDip',				type:'post',				data:{ 'dip_id': vdip_id },				beforeSend:function(){},				success:function(response){					var ret = $.parseJSON(response);					alert(ret.text);					refreshData();				}			});		}	});		$(document).on('click','#btn_add',function(){		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/dip_form',
			type:'post',
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$("#dip_catid").select2({
		id: function(e) { return e.dip_catid }, 
		placeholder: "Pilih Kategori",
		minimumInputLength: 0,
		multiple: false,		allowClear:true,
		ajax: { 
			url: "<?=base_url()?><?=$this->modules?>/ajaxfile/caridipcat",
			dataType: 'jsonp',
			type:'POST',
			quietMillis: 100,
			data: function (term, page) {
				return {
					keyword: term, //search term
					per_page: 5, // page size
					page: page, // page number
				};
			},
			results: function (data, page) { 
				var more = (page * 5) < data.result;
				return {results: data.rows, more: more};
			}
		},
		formatResult: dipcatFormatResult, 
		formatSelection: dipcatFormatSelection
	});		$("#dip_foi").select2();		$(document).on('click','#form1 #btncatid',function(){		if($('#form1 #dip_cat').val()!=''){			$.ajax({				type:'post',				url:'<?=base_url()?>dashboard/page/saveDipcat',				data:{ 'dip_cat':$('#form1 #dip_cat').val() },				beforeSend:function(){									},				success:function(response){					var ret = $.parseJSON(response);					alert(ret.text);					$('#form1 #dip_cat').val('');				}			});		}	});		$(document).on('click','#btnfilter',function(){		refreshData();	});
		
					});function refreshData(){		$.ajax({				type:'post',				url:'<?=base_url()?><?=$this->modules?>/page/data/dip',				data:{ 			'per_page':$('#per_page').val(),'cari':$('#cari').val()			,'dip_datestart':$('#dip_datestart').val()			,'dip_dateend':$('#dip_dateend').val()			,'dip_title':$('#dip_title').val()			,'dip_catid':$('#dip_catid').val()			,'dip_responsible':$('#dip_responsible').val()			,'dip_foi':$('#dip_foi').val()		},				beforeSend:function(){						$('#spinner').loading('stop');						$('#spinner').loading();				},		success:function(response){						$('#spinner').loading('stop');			$('#result').html(response);				}		});}	</script><ul class="breadcrumb">  <li><a href="#">DAFTAR INFORMASI PUBLIK</a> <span class="divider"></span></li>  <li class="active">DAFTAR DIP</li></ul><ul class="nav nav-tabs" id="myTab">	<li class="active"><a href="#daftar" data-toggle="tab"><i class="icon-table"></i> Daftar DIP</a></li></ul><div class="tab-content" style="overflow:visible">	<div class="tab-pane active" id="daftar">		<br />		<div class="row">			<div class="box-body">				<form role="form">				<div class="col-md-6">					<div class="row">						<div class="col-md-6">							<div class="form-group">								<label for="no_sep">Dokumen dari Tanggal</label>								<div>								<input type="text" name="dip_datestart" id="dip_datestart" class="form-control date" placeHolder="dari" style="width:100px" />								</div>							</div>							<div class="form-group">								<label for="no_sep">Dokumen sampai Tanggal</label>								<input type="text" name="dip_dateend" id="dip_dateend" class="form-control date" placeHolder="sampai" style="width:100px" />							</div>						</div>						<div class="col-md-6">							<div class="form-group">								<label for="dip_foi">Bentuk Informasi</label>								<select id="dip_foi" name="dip_foi" class="form-control" style="min-width:80%">									<option value="">-Pilh-</option>									<?php									$rsf = $this->db->get("dip_foi");									foreach($rsf->result() as $itemfoi){									?>									<option value="<?=$itemfoi->dip_foi?>"><?=ucfirst($itemfoi->dip_foi)?></option>									<?php									}									?>								</select>							</div>							<div class="form-group">								<label for="dip_title">Judul</label>								<input type="text" name="dip_title" id="dip_title" class="form-control" placeHolder="Judul" />							</div>							</div>					</div>				</div>				<div class="col-md-6">					<div class="form-group">						<label for="dip_catid">Kategori</label>						<div>						<input type="text" name="dip_catid" id="dip_catid" class="form-control" style="width:230px" />						</div>					</div>					<div class="form-group">						<label for="dip_responsible">Penerbit</label>						<input type="text" name="dip_responsible" class="form-control" id="dip_responsible" placeHolder="Penerbit" />					</div>						<div class="form-group">						<label for="dip_foi">&nbsp;</label>						<input type="button" id="btnfilter" name="btnfilter" value="FILTER" class="btn btn-warning" />					</div>				</div>				</form>			</div>		</div>		<br />		<div class="row">			<div class="col-md-6">				<input type="button" id="btn_add" name="btn_add" value="TAMBAH DIP" class="btn btn-primary" />				<!--<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/dip?size=A4"><i class="fa fa-file-pdf-o"></i></a>-->				<div id="spinner" style="display:inline-block"></div>			 </div>		</div>		<br />		<div class="row">			<div class="col-md-12" id="result" style="overflow:auto"></div>		</div>	</div></div><!-- Modal --><div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">  <div class="modal-dialog modal-lg">	<div class="modal-content">	  <div class="modal-header">		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>		<h4 class="modal-title" id="myModalLabel">UPDATE DIP</h4>	  </div>	  <div class="modal-body" style="overflow:auto;">		<div class="video" id="div-detail">					</div>	  </div>	  <div class="modal-footer">			  </div>	  	</div>  </div></div>