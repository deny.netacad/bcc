<script>$(document).ready(function(){			refreshData();	<?php	if($this->input->get('req_id')!=''){	?>	//$('#req_id')	<?php	}	?>		$(document).on('click','a[class="btndetail"]',function(){		var $this = $('a[class="btndetail"]');		var index = $this.index($(this));		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/permintaan_form',
			type:'post',
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});		$(document).on('click','a.response',function(){		var $this = $('a.response');		var index = $this.index($(this));		var vreq_id = $this.eq(index).attr('data');		$.ajax({			url:'<?=base_url()?><?=$this->modules?>/page/view/permintaan_resp_form',			type:'post',			data:{ 'req_id':vreq_id },			beforeSend:function(){},			success:function(response){				$('#div-detail2').html(response);				$('#myModal2').modal('show',function(){					keyboard:true				});								return false;			}		});	});		$(document).on('click','input[name="btndoc"]',function(){		var $this = $('input[name="btndoc"]');		var index = $this.index($(this));		var vreq_id = $this.eq(index).attr('data');		$.ajax({			url:'<?=base_url()?><?=$this->modules?>/page/view/permintaan_doc_form',			type:'post',			beforeSend:function(){},			success:function(response){				$('#div-detail3').html(response);				$('#myModal3').modal('show',function(){					keyboard:true				});								return false;			}		});	});		$(document).on('click','#btn_add',function(){		$.ajax({
			url:'<?=base_url()?><?=$this->modules?>/page/view/permintaan_form',
			type:'post',
			beforeSend:function(){},
			success:function(response){
				$('#div-detail').html(response);
				$('#myModal').modal('show',function(){
					keyboard:true
				});
				
				return false;
			}
		});	});				$(document).on('click','#btnfilter',function(){		refreshData();	});		$(document).on('click','a.ajax-replace', function(){
		$.ajax({
			type:'post',
			url:$(this).attr('href'),
			data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
			beforeSend:function(){
				$('#spinner').loading('stop');
				$('#spinner').loading();
			},
			success:function(response){
				$('#spinner').loading('stop');
				$('#result').html(response);
			}
		});
		return false;
	});			$(document).on('click','a.delete',function(){		var $this = $('a.delete');		var index = $this.index($(this));		var data = $this.eq(index).attr('data');		if(confirm('Yakin hapus..?')){			$.ajax({
				type:'post',
				url:'<?=base_url()?>dashboard/delPermintaan',
				data:{ 'req_id':data },
				beforeSend:function(){
				},
				success:function(response){
					var ret = $.parseJSON(response);					alert(ret.text);
					refreshData();
				}
			});		}	});});function refreshData(){		$.ajax({				type:'post',				url:'<?=base_url()?><?=$this->modules?>/page/data/permintaan',				data:{ 			'per_page':$('#per_page').val(),'cari':$('#cari').val() 			,'no_id':$('#no_id').val(),'fullname':$('#fullname').val(),'req_id':$('#req_id').val()			,'req_title':$('#req_title').val(),'req_sts':$('#req_sts').val()			,'req_type':$('#req_type').val()		},				beforeSend:function(){						$('#spinner').loading('stop');						$('#spinner').loading();				},		success:function(response){						$('#spinner').loading('stop');			$('#result').html(response);				}		});}	</script><ul class="breadcrumb">  <li><a href="#">DAFTAR PERMINTAAN</a> <span class="divider"></span></li>  <li class="active">DAFTAR PERMINTAAN</li></ul><ul class="nav nav-tabs" id="myTab">	<li class="active"><a href="#daftar" data-toggle="tab"><i class="icon-table"></i> Daftar Permintaan</a></li></ul><div class="tab-content" style="overflow:visible">	<div class="tab-pane active" id="daftar">		<div class="row">			<div class="col-md-6">				<div class="form-group">
					<label for="no_id">Berdasarkan ID Pemohon</label>
					<input type="text" class="form-control" name="no_id" id="no_id" placeHolder="ID Pemohon" style="width:90%" />
				</div>				<div class="form-group">					<label for="fullname">Berdasarkan Nama Pemohon</label>					<input type="text" class="form-control" name="fullname" id="fullname" placeHolder="Nama Pemohon" style="width:90%" />				</div>				<div class="form-group">					<label for="req_id">Berdasarkan No. Permintaan</label>					<input type="text" class="form-control" name="req_id" id="req_id" placeHolder="Nomor Permintaan" value="<?=$this->input->get('req_id')?>" style="width:90%" />				</div>			</div>			<div class="col-md-6">				<div class="form-group">					<label for="req_title">Berdasarkan Judul</label>					<input type="text" class="form-control" name="req_title" id="req_title" placeHolder="Judul" style="width:90%" />				</div>				<div class="form-group">					<label for="req_sts">Berdasarkan Status Permintaan</label>					<select class="form-control" id="req_sts" name="req_sts" style="width:90%">						<option value="">Semua Status</option>						<option value="1">Diproses</option>						<option value="2">Selesai</option>						<option value="3">Ditolak</option>					</select>				</div>				<div class="form-group">					<label for="req_type">Berdasarkan Pengiriman</label>					<select class="form-control" id="req_type" name="req_type" style="width:90%">						<option value="">Semua Pengiriman</option>						<option value="1">Download</option>						<option value="2">Datang Langsung</option>					</select>				</div>				<div class="form-group">					<label></label>					<input type="button" id="btnfilter" name="btnfilter" value="FILTER" class="btn btn-warning" />				</div>			</div>		</div>		<div class="row">			<div class="col-md-6 pull-right">				<a class="exp-pdf" target="_blank" href="<?=base_url()?>dashboard/page/pdf/permintaan?size=A4"><i class="icon icon-print"></i></a>			</div>		</div>		<div class="row">			<div class="col-md-12" id="result" style="overflow:auto"></div>		</div>	</div></div><!-- Modal -->
<!-- Modal --><div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">  <div class="modal-dialog modal-lg">	<div class="modal-content">	  <div class="modal-header">		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>		<h4 class="modal-title" id="myModalLabel">UPDATE PERMINTAAN</h4>	  </div>	  <div class="modal-body" style="overflow:auto;">		<div class="video" id="div-detail">					</div>	  </div>	  <div class="modal-footer">			  </div>	  	</div>  </div></div><!-- Modal --><div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">  <div class="modal-dialog modal-lg">	<div class="modal-content">	  <div class="modal-header">		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>		<h4 class="modal-title" id="myModalLabel">UPDATE RESPON</h4>	  </div>	  <div class="modal-body" style="overflow:auto;">		<div class="video" id="div-detail2">					</div>	  </div>	  <div class="modal-footer">			  </div>	  	</div>  </div></div><!-- Modal --><div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">  <div class="modal-dialog modal-lg">	<div class="modal-content">	  <div class="modal-header">		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>		<h4 class="modal-title" id="myModalLabel">TAMBAH DOKUMEN</h4>	  </div>	  <div class="modal-body" style="overflow:auto;">		<div class="video" id="div-detail">					</div>	  </div>	  <div class="modal-footer">			  </div>	  	</div>  </div></div>