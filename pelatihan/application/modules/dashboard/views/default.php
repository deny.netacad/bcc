<?php
$rs = $this->db->query("select count(*) as j from ref_pelatihan where date_format(tgl_mulai,'%Y-%m')='".date('Y-m')."'")->row();
$jmldokumen = $rs->j;



$rs = $this->db->query("select count(*) as j from ref_pendaftaran where date_format(log_posting,'%Y-%m')='".date('Y-m')."'")->row();
$jmlpendaftaran = $rs->j;



$rs = $this->db->query("select count(*) as j from ref_pelatihan where tgl_ahir < '".date('Y-m-d')."'")->row();
$jmlsudah = $rs->j;



$rs = $this->db->query("select count(*) as j from ref_pelatihan where tgl_ahir > '".date('Y-m-d')."'")->row();
$jmlbelum = $rs->j;



$rs = $this->db->query("select count(*) as j from ref_pendaftaran where date_format(log_posting,'%Y-%m')='".date('Y-m')."' and is_posting=0")->row();
$jmlmangkir = $rs->j;



$rs = $this->db->query("select count(*) as j from ref_pendaftaran where date_format(log_posting,'%Y-%m')='".date('Y-m')."' and is_posting=1")->row();
$jmlmasuk = $rs->j;


$rs = $this->db->query("select count(*) as j from req_keberatan a
inner join req_info b on a.req_id=b.req_id
 where b.skpd_id='".$this->session->userdata('skpd_id')."'");
$item = $rs->row();
$jmlkeberatan = $item->j;

$rs = $this->db->query("
select 
	sum(if(req_sts=1,1,0)) as jmldiproses, 
	sum(if(req_sts=2,1,0)) as jmlselesai,
	sum(if(req_sts=3,1,0)) as jmlditolak
from req_info 
where skpd_id='".$this->session->userdata('skpd_id')."'");
$item = $rs->row();
$jmldiproses = $item->jmldiproses;
$jmlselesai = $item->jmlselesai;
$jmlditolak = $item->jmlditolak;
?>
<script>


</script>
<!-- Small boxes (Stat box) -->
<div class="row">	
	<div class="col-lg-4 col-xs-12 col-md-4">
		<div class="small-box bg-green">
			<div class="inner">
				<h3>Dokumen Pelatihan</h3>
				<p><?=$jmldokumen?> Dokumen</p>
			</div>
			<div class="icon"><i class="ion ion-stats-bars"></i></div>
			<a href="<?=base_url()?>" class="small-box-footer">s/d <?=date('M Y')?></a>
		</div>
	</div><!-- ./col -->

	<div class="col-lg-4 col-xs-12 col-md-4">
	<div class="small-box bg-yellow">
		<div class="inner">
			<h3>Jumlah Pendaftar </h3>
			<p><?=$jmlpendaftaran?> Permintaan</p>
				
		</div>
		<div class="icon"><i class="ion ion-stats-bars"></i></div>
		<a href="<?=base_url()?>" class="small-box-footer">s/d <?=date('M Y')?></a>
	</div>
	</div><!-- ./col -->

	<div class="col-lg-4 col-xs-12 col-md-4">
	<div class="small-box bg-red">
		<div class="inner">
			<h3>Pelatihan Sudah berjalan</h3>
			<p><?=$jmlsudah?> Pelatihan</p>
		
		</div>
		<div class="icon"><i class="ion ion-stats-bars"></i></div>
		<a href="<?=base_url()?>" class="small-box-footer">s/d <?=date('M Y')?></a>
	</div>
	</div><!-- ./col -->
</div><!-- ./col -->

<div class="row">
	<div class="col-lg-4 col-xs-12 col-md-4">
		<div class="small-box bg-green">
			<div class="inner">
				<h3>Pelatihan Belum berjalan</h3>
				<p><?=$jmlbelum?> Pelatihan</p>
			</div>
			<div class="icon"><i class="ion ion-stats-bars"></i></div>
			<a href="<?=base_url()?>" class="small-box-footer">s/d <?=date('M Y')?></a>
		</div>
	</div><!-- ./col -->

	<div class="col-lg-4 col-xs-12 col-md-4">
	<div class="small-box bg-yellow">
		<div class="inner">
			<h3>Peserta Mangkir </h3>
			<p><?=$jmlmangkir?> Tidak Di posting</p>
		</div>
		<div class="icon"><i class="ion ion-stats-bars"></i></div>
		<a href="<?=base_url()?>" class="small-box-footer">s/d <?=date('M Y')?></a>
	</div>
	</div><!-- ./col -->

	<div class="col-lg-4 col-xs-12 col-md-4">
	<div class="small-box bg-red">
		<div class="inner">
			<h3>Peserta Hadir</h3>
			<p><?=$jmlmasuk?> Peserta Teregister</p>
			</div>
		<div class="icon"><i class="ion ion-stats-bars"></i></div>
		<a href="<?=base_url()?>" class="small-box-footer">s/d <?=date('M Y')?></a>
	</div>
	</div><!-- ./col -->
</div><!-- ./col -->



<!-- Modal -->
<!--<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-remove"></i></button>
		<h4 class="modal-title" id="myModalLabel">Smart Searching</h4>
	  </div>
	  <div class="modal-body">
		<div class="video" id="div-detail">
			<div class="row">
				<div class="col-md-12" style="text-align:center">
					<div style="margin:0 auto;display:inline-block"><img src="<?=base_url()?>assets/front/img/smart_search.png" class="img-responsive" /></div>
					<div class="form-group">
						<input type="text" id="search" name="search"  style="width:80%;border:none;" placeholder="Cari apapun yang anda mau?" />
					</div>
				</div>
				<div class="col-md-12" style="text-align:center">
					<iframe id="doc-content" src="" style="width:90%; height:350px;max-height:400px;" frameborder="0"></iframe>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>-->