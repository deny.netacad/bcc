<link rel="stylesheet" href="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>
<script>
	function dipcatFormatResult(data) {
	var markup= "<li>"+ data.judul+" <br> <small><b>"+data.tempat+"</b> , "+data.tgl_mulai+"</small</li>";
	return markup;
	}

	function dipcatFormatSelection(data) {
		return data.judul;
	}

$(document).ready(function(){
		
		$('#div-utama').hide();

		$('#id_pelatihan').on('select2-selecting',function(e){
		$('#div-utama').show(1000);
		});	
		
		$('#id_pelatihan').on('select2-removed',function(e){
		$('#div-utama').hide(1000);
		$('#identity,#username').val('');
		});	
		
		
		$("#id_pelatihan,#id_pelatihan2").select2({
		id: function(e) { return e.id }, 
		placeholder: "Pilih Kategori",
		minimumInputLength: 0,
		multiple: false,
		allowClear:true,
		ajax: { 
			url: "<?=base_url()?><?=$this->modules?>/ajaxfile/caripelatihan",
			dataType: 'jsonp',
			type:'POST',
			quietMillis: 100,
			data: function (term, page) {
				return {
					keyword: term, //search term
					per_page: 5, // page size
					page: page, // page number
				};
			},
			results: function (data, page) { 
				var more = (page * 5) < data.result;
				return {results: data.rows, more: more};
			}
		},
		formatResult: dipcatFormatResult, 
		formatSelection: dipcatFormatSelection
	});
	

$(document).on('submit','#form1',function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				top.location="<?=base_url()?>";
			});		
		}
	});
	
	
$(document).on('submit','#form2',function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				top.location="<?=base_url()?>";
			});		
		}
	});
	
});
</script>	
	<div class="col-md-12">
			
			<!-- Kegiatan -->
						<!--<ul class="nav nav-tabs nav-tabs-ar">
				
				<li class=""><a href="#" data-toggle="tab"><i class="fa fa-building"></i> JENIS KEGIATAN</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="#">
					<div class="panel panel-primary">
						<div class="panel-heading">PILIH JENIS KEGIATAN YANG DIADAKAN</div>
						<div class="panel-body">
							<form id="form1" name="form1" action="<?=base_url()?>portal/sendRegister" class="form-horizontal" method="post" enctype="multipart/form-data">	  
							  
							  <div class="form-group">
								<label for="nama_kegiatan" class="col-sm-2 control-label">Nama Kegiatan</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="nama_kegiatan" name="nama_kegiatan" placeholder="Nama Kegiatan">
								</div>
							  </div>
							   <div class="form-group">
								<label for="tempat_kegiatan" class="col-sm-2 control-label">Tempat Kegiatan</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="tempat_kegiatan" name="tempat_kegiatan" placeholder="Tempat Kegiatan">
								</div>
							  </div>
							   <div class="form-group">
								<label for="tgl_kegiatan" class="col-sm-2 control-label">Tanggal Kegiatan</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="tgl_kegiatan" name="tgl_kegiatan" placeholder="yyyy-mm-dd">
								</div>
							  </div>
							  
							</form>
						</div>
					</div>
				</div>-->
			<!-- Nav tabs -->
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#dip1" data-toggle="tab"><i class="fa fa-user"></i> Daftar Sebagai Peserta</a></li>
				<li class=""><a href="#dip2" data-toggle="tab"><i class="fa fa-building"></i> Daftar Sebagai Perusahaan</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="dip1">
					<div class="panel "><div class="panel-body">
							<form id="form1" name="form1" action="<?=base_url()?>portal/sendRegister" class="form-horizontal" method="post" enctype="multipart/form-data">
							  <h3>DATA PERSONIL</h3>
						
								
								<div class="form-group">
								<label for="noid" class="col-sm-2 control-label">ID PELATIHAN</label>
								<div class="col-sm-10 col-lg-4">
								<input type="text" name="id_pelatihan" id="id_pelatihan" class="form-control" style="width:650px" />
								</div>
								</div>
								<div id="div-utama">
							    <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Jenis Pengenal</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="identity" id="identity" class="form-control" required1="">
										<option value="1">KTP</option>
										<option value="2">SIM</option>
										<option value="3">PASSPORT</option>
										<option value="4">KTM</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">No. Identitas</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="noid" name="noid" placeholder="No. Identitas Sesuai Jenis Pengenal">
								</div>
							  </div>
							  <!--<div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Scan File Identitas</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<span class="btn btn-success fileinput-button" style="float:left;cursor:pointer">
										<span style="cursor:pointer"><i class="fa fa-image"></i> Add files...</span>
										<input type="file" name="scanfile" class="textfield" id="scanfile" value="" data-url="portal/do_upload_scanfile" />
									</span>
								</div>
							  </div>-->
							  <div class="form-group">
								<label for="username" class="col-sm-2 control-label">Username</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="username" name="username" placeholder="Username">
								</div>
							  </div>
							  <div class="form-group">
								<label for="userpass" class="col-sm-2 control-label">Password</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="password" class="form-control" id="userpass" name="userpass" placeholder="Password">
								</div>
							  </div>
							  <div class="form-group">
								<label for="userpass2" class="col-sm-2 control-label">Konfirmasi Password</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="password" class="form-control" id="userpass2" name="userpass2" placeholder="Ulangi Password">
								</div>
							  </div>
							  <div class="form-group">
								<label for="fullname" class="col-sm-2 control-label">Nama Lengkap *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Nama Lengkap">
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="sex" id="sex" name="sex" class="form-control" required1="">
										<option value="1">Laki-laki</option>
										<option value="2">Perempuan</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="pob" class="col-sm-2 control-label">Tempat Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="pob" name="pob" placeholder="Tempat Lahir">
								</div>
							  </div>
							  <div class="form-group">
								<label for="dob" class="col-sm-2 control-label">Tanggal Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control date" id="dob" name="dob" placeholder="yyyy-mm-dd">
								</div>
							  </div>
							  <div class="form-group">
								<label for="education" class="col-sm-2 control-label">Pendidikan Terakhir *</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="education" id="education" class="form-control" required1>
										<option value="1">SD</option>
										<option value="2">SMP</option>
										<option value="3">SMA</option>
										<option value="4">Diploma</option>
										<option value="5">Sarjana</option>
										<option value="6">Pasca Sarjana</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="job" class="col-sm-2 control-label">Pekerjaan *</label>
								<div class="col-sm-2 col-md-4 col-lg-3">
									<select name="job" id="job" class="form-control" required1>
										<option value="1">Wiraswasta</option>
										<option value="2">Buruh</option>
										<option value="3">Petani</option>
										<option value="4">Nelayan</option>
										<option value="5">PNS</option>
										<option value="6">Swasta</option>
										<option value="7">Pedagang</option>
										<option value="8">Pekerja rumah tangga</option>
										<option value="9">LSM</option>
										<option value="10">Tenaga Pendidik</option>
										<option value="11">Pengacara</option>
										<option value="12">Tenaga Kesehatan</option>
										<option value="13">Peneliti</option>
										<option value="14">Seniman</option>
										<option value="15">Wartawan</option>
										<option value="16">Mahasiswa/Pelajar</option>
										<option value="17">Lain-lain</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="addr" class="col-sm-2 control-label">Alamat *</label>
								<div class="col-sm-10 col-lg-4">
								  <textarea class="form-control" id="addr" name="addr" placeholder="Alamat"></textarea>
								</div>
							  </div>
							  <div class="form-group">
								<label for="city" class="col-sm-2 control-label">Kabupaten / Kota *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="city" name="city" placeholder="Kota">
								</div>
							  </div>
							  <div class="form-group">
								<label for="postcode" class="col-sm-2 control-label">Kode Pos *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="postcode" name="postcode" placeholder="5 digit">
								</div>
							  </div>
							  <div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="email" class="form-control" id="email" name="email" placeholder="Email">
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_home" class="col-sm-2 control-label">Telp. Rumah *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_home" name="phone_home" placeholder="No. Telp Rumah">
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_mobile" class="col-sm-2 control-label">Telp. Mobile *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_mobile" name="phone_mobile" placeholder="No. HP">
								</div>
							  </div>
							  			<div class="span6">
						<div class="accordion-heading">
						<h3>DATA PERUSAHAAN</h3>
						 <div class="form-group">
								<label for="nama_perusahaan" class="col-sm-2 control-label">Nama Perusahaan *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan">
								</div>
							  </div>
							   <div class="form-group">
								<label for="alamat_perusahaan" class="col-sm-2 control-label">Alamat Perusahaan*</label>
								<div class="col-sm-10 col-lg-4">
								  <textarea class="form-control" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Alamat Perusahaan"></textarea>
								</div>
							  </div>
							   <div class="form-group">
								<label for="city_perusahaan" class="col-sm-2 control-label">Kabupaten / Kota *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="city_perusahaan" name="city_perusahaan" placeholder="Kab / Kota">
								</div>
							  </div>
							   <div class="form-group">
								<label for="jabatan" class="col-sm-2 control-label">Jabatan *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan">
								</div>
							  </div>
							   <div class="form-group">
								<label for="skl_perusahaan" class="col-sm-2 control-label">Skala Perusahaan *</label>
								<div class="col-sm-10 col-lg-4">
								 <input type="text" class="form-control" id="skl_perusahaan" name="skl_perusahaan" placeholder="Besar / Kecil">
								</div>
							  </div>
					</div>
				</div>
							  
							   
							  <div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
								  <button type="submit" class="btn btn-ar btn-primary">Daftar</button>
								</div>
							  </div>
							  </div> <!-- tutup div utama -->
							</form>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="dip2">
					<div class="panel">
						<div class="panel-body">
							<form id="form2" name="form2" action="<?=base_url()?>portal/sendRegister" class="form-horizontal" method="post" enctype="multipart/form-data">
							  	<div class="form-group">
								<label for="noid" class="col-sm-2 control-label">ID PELATIHAN</label>
								<div class="col-sm-10 col-lg-4">
								<input type="text" name="id_pelatihan2" id="id_pelatihan2" class="form-control" style="width:650px" />
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Surat Pendirian *</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="identity" id="identity" class="form-control" required1="">
										<option value="5">Akta Pendirian</option>
										<option value="6">Lainnya</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">No. Surat Pendirian *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="noid" name="noid" placeholder="No. Surat Pendirian">
								</div>
							  </div>
							
							  <div class="form-group">
								<label for="username" class="col-sm-2 control-label">Username</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="username" name="username" placeholder="Username">
								</div>
							  </div>
							  <div class="form-group">
								<label for="userpass" class="col-sm-2 control-label">Password</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="password" class="form-control" id="userpass" name="userpass" placeholder="Password">
								</div>
							  </div>
							  <div class="form-group">
								<label for="userpass2" class="col-sm-2 control-label">Konfirmasi Password</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="password" class="form-control" id="userpass2" name="userpass2" placeholder="Ulangi Password">
								</div>
							  </div>
							  <div class="form-group">
								<label for="fullname" class="col-sm-2 control-label">Nama Penanggung Jawab *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Nama Penanggung Jawab">
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="sex" id="sex" name="sex" class="form-control" required1="">
										<option value="1">Laki-laki</option>
										<option value="2">Perempuan</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="pob" class="col-sm-2 control-label">Tempat Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="pob" name="pob" placeholder="Tempat Lahir">
								</div>
							  </div>
							  <div class="form-group">
								<label for="dob" class="col-sm-2 control-label">Tanggal Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control date" id="dob" name="dob" placeholder="dd-mm-YYYY">
								</div>
							  </div>
							  <div class="form-group">
								<label for="education" class="col-sm-2 control-label">Pendidikan Terakhir *</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="education" id="education" class="form-control" required1>
										<option value="1">SD</option>
										<option value="2">SMP</option>
										<option value="3">SMA</option>
										<option value="4">Diploma</option>
										<option value="5">Sarjana</option>
										<option value="6">Pasca Sarjana</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="job" class="col-sm-2 control-label">Pekerjaan *</label>
								<div class="col-sm-2 col-md-4 col-lg-3">
									<select name="job" name="id" class="form-control" required1>
										<option value="1">Wiraswasta</option>
										<option value="2">Buruh</option>
										<option value="3">Petani</option>
										<option value="4">Nelayan</option>
										<option value="5">PNS</option>
										<option value="6">Swasta</option>
										<option value="7">Pedagang</option>
										<option value="8">Pekerja rumah tangga</option>
										<option value="9">LSM</option>
										<option value="10">Tenaga Pendidik</option>
										<option value="11">Pengacara</option>
										<option value="12">Tenaga Kesehatan</option>
										<option value="13">Peneliti</option>
										<option value="14">Seniman</option>
										<option value="15">Wartawan</option>
										<option value="16">Mahasiswa/Pelajar</option>
										<option value="17">Lain-lain</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="addr" class="col-sm-2 control-label">Alamat *</label>
								<div class="col-sm-10 col-lg-4">
								  <textarea class="form-control" id="addr" name="addr" placeholder="Alamat"></textarea>
								</div>
							  </div>
							  <div class="form-group">
								<label for="city" class="col-sm-2 control-label">Kota *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="city" name="city" placeholder="Kota">
								</div>
							  </div>
							  <div class="form-group">
								<label for="postcode" class="col-sm-2 control-label">Kode Pos *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="postcode" name="postcode" placeholder="5 digit">
								</div>
							  </div>
							  <div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="email" class="form-control" id="email" name="email" placeholder="Email">
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_home" class="col-sm-2 control-label">Telp. Rumah *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_home" name="phone_home" placeholder="No. Telp Rumah">
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_mobile" class="col-sm-2 control-label">Telp. Mobile *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_mobile" name="phone_mobile" placeholder="No. HP">
								</div>
							  </div>
							 
							  <div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
								  <button type="submit" class="btn btn-ar btn-primary">Daftar</button>
								</div>
							  </div>
							</form>
						</div>
					</div>
				</div>
			</div>
        </div>
   