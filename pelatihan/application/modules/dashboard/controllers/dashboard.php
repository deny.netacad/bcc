<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	
	
	function __construct()

	{

		parent::__construct();

		$this->modules = "dashboard";

	}

	
	public function index()
	{
		if($this->session->userdata('is_login_disperindag')){
			$this->load->view('dashboard',$this->data);
		}else{
			redirect('/login');
		}
	}
	
	function graphStatistik(){
		$dt1 = rand(10,100);
		$dt2 = rand(10,100);
		$dt3 = rand(10,100);
		$dt4 = rand(10,100);
		$dt5 = rand(10,100);
		$dt6 = rand(10,100);
		$dt7 = rand(10,100);
		$dt8 = rand(10,100);
		$dt9 = rand(10,100);
		$dt10 = rand(10,100);
		$dt11 = rand(10,100);
		$dt12 = rand(10,100);
		$dt1_ = rand(10,$dt1);
		$dt2_ = rand(10,$dt2);
		$dt3_ = rand(10,$dt3);
		$dt4_ = rand(10,$dt4);
		$dt5_ = rand(10,$dt5);
		$dt6_ = rand(10,$dt6);
		$dt7_ = rand(10,$dt7);
		$dt8_ = rand(10,$dt8);
		$dt9_ = rand(10,$dt9);
		$dt10_ = rand(10,$dt10);
		$dt11_ = rand(10,$dt11);
		$dt12_ = rand(10,$dt12);
		$dt1__ = $dt1-$dt1_;
		$dt2__ = $dt2-$dt2_;
		$dt3__ = $dt3-$dt3_;
		$dt4__ = $dt4-$dt4_;
		$dt5__ = $dt5-$dt5_;
		$dt6__ = $dt6-$dt6_;
		$dt7__ = $dt7-$dt7_;
		$dt8__ = $dt8-$dt8_;
		$dt9__ = $dt9-$dt9_;
		$dt10__ = $dt10-$dt10_;
		$dt11__ = $dt11-$dt11_;
		$dt12__ = $dt12-$dt12_;
		echo "Kategori,Jan,Feb,Mar,Apr,Mei,Jun,Jul,Ags,Sep,Okt,Nov,Des\n";
		echo "Permintaan DIP,".$dt1.",".$dt2.",".$dt3.",".$dt4.",".$dt5.",".$dt6.",".$dt7.",".$dt8.",".$dt9.",".$dt10.",".$dt11.",".$dt12."\n";	
		echo "Selesai,".$dt1_.",".$dt2_.",".$dt3_.",".$dt4_.",".$dt5_.",".$dt6_.",".$dt7_.",".$dt8_.",".$dt9_.",".$dt10_.",".$dt11_.",".$dt12_."\n";	
		echo "Ditolak,".$dt1__.",".$dt2__.",".$dt3__.",".$dt4__.",".$dt5__.",".$dt6__.",".$dt7__.",".$dt8__.",".$dt9__.",".$dt10__.",".$dt11__.",".$dt12__."\n";	
		echo "Proses,".rand(10,$dt1__).",".rand(10,$dt2__).",".rand(10,$dt3__).",".rand(10,$dt4__).",".rand(10,$dt5__).",".rand(10,$dt6__).",".rand(10,$dt7__).",".rand(10,$dt8__).",".rand(10,$dt9__).",".rand(10,$dt10__).",".rand(10,$dt11__).",".rand(10,$dt12__)."\n";	
		
	}
	
	function autoNoDip(){
		$rs = $this->db->query("SELECT CONCAT_WS('','D',LPAD(IFNULL(MAX(dip_id)+1,1),5,'0')) AS kode FROM ref_dip");
		$item = $rs->row();
		echo $item->kode;
	}
	
	function delUpload(){
		$full_path = $this->input->post('full_path');
		$filearr = explode("/",$full_path);
		$datawhere['dip_file'] = $filearr[count($filearr)-1];
		$this->db->delete("ref_dip_file",$datawhere);
		if(file_exists($full_path)){
			unlink($full_path);
			$ret['text'] = "file terhapus";
		}else{
			$ret['text'] = "file tidak ketemu";
		}
		echo json_encode($ret);
	}
	
	function do_upload_file(){
		$path_parts = pathinfo($_FILES["files"]["name"]);
		$extension = $path_parts['extension'];
		
		$config['upload_path'] = BASEPATH.'../upliddir/';
		$config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx|jpg|pdf|mp3|mp4|MP4';
		$config['max_size']	= '50000000';
		$config['file_name'] = $this->input->post('filename').".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('files'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
	}
	
	function do_upload_dip(){
		$path_parts = pathinfo($_FILES["dip_file"]["name"]);
		$extension = $path_parts['extension'];
		
		$config['upload_path'] = BASEPATH.'../upliddir/';
		$config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx|jpg|pdf|mp3|mp4|MP4';
		$config['max_size']	= '50000000';
		$config['file_name'] = $this->input->post('filename').".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('dip_file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
	}
	
	function do_upload_dipm(){
		$path_parts = pathinfo($_FILES["dip_file"]["name"]);
		$extension = $path_parts['extension'];
		#$filename = str_replace("_"," ",$_FILES["dip_file"]["name"]);
		$filename = str_replace("_"," ",date('Ymd'));
	
		$filename = str_replace("-"," ",$filename);
		$filename = str_replace($extension,"",$filename);
		$config['upload_path'] = BASEPATH.'../upliddir/';
		$config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx|jpg|pdf|mp3|mp4|MP4';
		$config['max_size']	= '50000000';
		$config['file_name'] = url_title($filename).".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('dip_file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
	}
	
	function delPermintaan(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(count($data)>0){
			if(!$this->db->delete("req_info",$data)){
				$ret['text'] = 'Item tidak berhasil dihapus';
			}else{
				$this->db->delete("resp_req",$data);
				$ret['text'] = 'Item berhasil dihapus';
			}
		}else{
			$ret['text'] = 'Terjadi kesalahan';
		}
		echo json_encode($ret);
	}
	
}