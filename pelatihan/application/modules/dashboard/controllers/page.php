<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->modules = "dashboard";
		$this->data = $this->config->item('data');
		if (!$this->session->userdata('is_login_disperindag'))
		{
			redirect('login');
		}
	}

	function postingpeserta(){
		$error = false;
		$ret['text'] = "Data berhasil diposting..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		#yang di update
		#$dataflag['logterahir'] = date('Y-m-d h:i:s');			
		#$dataflag['is_posting'] = 1;
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					#syarat Update
					#$databonus['member_id'] = $data['member_id'];
					#$databonus['tgl'] = $data['tgl'];
					#$databonus['is_posting'] = 0;
					#$this->db->update("ngi_daily_statemen",$dataflag,$databonus);
					$this->db->query("update ref_pendaftaran set is_posting=1 , log_posting=NOW() where
					id='".$data['id_pelatihan']."'  and is_posting=0 ");
					
				}
			}
		}
		echo json_encode($ret);
	}
	
	
	function unpostingpeserta(){
		$error = false;
		$ret['text'] = "Data berhasil diposting..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		#yang di update
		#$dataflag['logterahir'] = date('Y-m-d h:i:s');			
		#$dataflag['is_posting'] = 1;
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					#syarat Update
					#$databonus['member_id'] = $data['member_id'];
					#$databonus['tgl'] = $data['tgl'];
					#$databonus['is_posting'] = 0;
					#$this->db->update("ngi_daily_statemen",$dataflag,$databonus);
					$this->db->query("update ref_pendaftaran set is_posting=0 , log_posting=NOW() where
					id='".$data['id_pelatihan']."'  and is_posting=1 ");
					
				}
			}
		}
		echo json_encode($ret);
	}
	
	
	
	function editAkun(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->query("select a.*,b.namaakun as parent from 
		mast_akun a 
		left join mast_akun b on a.idparent=b.kdakun
		where a.kdakun='".$data['kdakun']."'
		");
		
		echo json_encode($rs->row());
	}
	
	
	function delSkpd(){
		$ret['text'] = "Data terhapus...";
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("ref_skpd",$data)){
			$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			echo json_encode($ret);
			exit;
		}
		echo json_encode($ret);
	}	
	
	
	
	function saveSkpd(){
		$arrnot 	= array("","id_edit","id_edit2","skpd_pass");
		$keyedit 	= array("","id_edit","id_edit2");
		$keyindex 	= array("","skpd_id","skpd_user");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		
		if($this->input->post('id_edit')!='' && $this->input->post('id_edit')!=$this->input->post('skpd_id')){
			$datax['skpd_id'] = $this->input->post('skpd_id');
			$datawhere['skpd_id'] = $this->input->post('id_edit');
			$this->db->update("ref_skpd_file",$datax,$datawhere);
			$this->db->update("ref_user",$datax,$datawhere);
			
			if($this->input->post('id_edit2')!='' && $this->input->post('id_edit2')!=$this->input->post('skpd_user')){
				$datax = array();
				$datawhere = array();
				$datax['user_name'] = $this->input->post('skpd_user');
				$datawhere['user_name'] = $this->input->post('id_edit2');
				$datawhere['skpd_id'] = $this->input->post('skpd_id');
				$this->db->update("ref_user",$datax,$datawhere);
			}	
		}else{
			if($this->input->post('id_edit2')!='' && $this->input->post('id_edit2')!=$this->input->post('skpd_user')){
				$datax = array();
				$datawhere = array();
				$datax['user_name'] = $this->input->post('skpd_user');
				$datawhere['user_name'] = $this->input->post('id_edit2');
				$datawhere['skpd_id'] = $this->input->post('skpd_id');
				$this->db->update("ref_user",$datax,$datawhere);
			}	
		}
		
		// cek apakah username dan skpd_id sudah ada di ref_user ?
		
		$datax = array();
		$datawhere = array();
		$datawhere['skpd_id'] = $this->input->post('skpd_id');
		$datawhere['user_name'] = $this->input->post('skpd_user');
		$rs = $this->db->get_where("ref_user",$datawhere);
		
		if($rs->num_rows()>0){
			$item = $rs->row();
			if($this->input->post('skpd_pass')!=''){
				$datax['user_pass'] = md5($this->input->post('skpd_pass'));
				$this->db->update("ref_user",$datax,$datawhere);
			}
		}else{
			$datax['skpd_id'] = $this->input->post('skpd_id');
			$datax['user_name'] = $this->input->post('skpd_user');
			$datax['user_realname'] = 'SKPD '.$this->input->post('skpd_id');	
			$datax['user_email'] = $this->input->post('skpd_email');
			$datax['user_role'] = 2;
			$datax['user_pass'] = md5($this->input->post('skpd_pass'));
			$this->db->insert("ref_user",$datax);
		}
		
		$this->dashboard_mod->save("ref_skpd",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
			
	}
	#kegiatan
	function saveKegiatan(){
		
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id_kegitan");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->dashboard_mod->save("id_kegiatan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,null,null,$keynum);
		
	}
	function delKegiatan(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(count($data)>0){
			if(!$this->db->delete("kegiatan",$data)){
				$ret['text'] = 'Item tidak berhasil dihapus';
			}else{
				$this->db->delete("kegiatan",$data);
				$ret['text'] = 'Item berhasil dihapus';
			}
		}else{
			$ret['text'] = 'Terjadi kesalahan';
		}
		echo json_encode($ret);
	}
	
	function editPelatihan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->query("select *,date_format(tgl_mulai,'%d-%m-%Y') as tgl_mulai,date_format(tgl_ahir,'%d-%m-%Y') as tgl_ahir from 
		ref_pelatihan 
		
		where id='".$data['id']."'
		");
		
		echo json_encode($rs->row());
	}
	
	
	function saveDipcat(){
		
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","dip_catid");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->dashboard_mod->save("ref_dip_cat",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,null,null,$keynum);
		
	}
	
	function delDip(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(count($data)>0){
			if(!$this->db->delete("ref_dip",$data)){
				$ret['text'] = 'Item tidak berhasil dihapus';
			}else{
				$this->db->delete("ref_dip_file",$data);
				$ret['text'] = 'Item berhasil dihapus';
			}
		}else{
			$ret['text'] = 'Terjadi kesalahan';
		}
		echo json_encode($ret);
	}

	// 	function delPelatihan(){
	// 	foreach($_POST as $key=>$value){
	// 		$data[$key] = $value;
	// 	}
	// 	if(count($data)>0){
	// 		if(!$this->db->delete("ref_pelatihan",$data)){
	// 			$ret['text'] = 'Item tidak berhasil dihapus';
	// 		}else{
	// 			$this->db->delete("ref_pelatihan",$data);
	// 			$ret['text'] = 'Item berhasil dihapus';
	// 		}
	// 	}else{
	// 		$ret['text'] = 'Terjadi kesalahan';
	// 	}
	// 	echo json_encode($ret);
	// }
	
	function saveDip(){
		
		$arrnot 	= array("","id_edit","dip_cat");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","dip_id");
		$keydate	= array("","dip_datestart","dip_dateend");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		foreach($_POST as $key=>$value){
			if(!is_array($value)){
				if(array_search($key,$arrnot)==''){
					if(array_search($key,$keyin)!=""){
						$keys =  array_keys($keyin,$key);
						$key = $keyout[$keys[0]];
					}
					
					if(array_search($key,$keydate)!=''){
						$value = (($value!='')?$value:"00-00-0000");
						$v = explode(" ",$value);
						$val = explode("-",$v[0]);
						if(count($v)>1){
							$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
						}else{
							$value = $val[2]."-".$val[1]."-".$val[0];
						}
					}
					$data[$key] = $value;
				}
			}
		}
		$data['skpd_id'] = $this->session->userdata('skpd_id');
		$data['user_id'] = $this->session->userdata('user_id');
		$data['createon'] = date('Y-m-d H:i:s');
		$data['dip_stspub'] = $this->input->post('dip_stspub');
		
		
		if($this->input->post('id_edit')!=''){
			$dt['dip_id'] = $this->input->post('id_edit');
			if(!$this->db->update("ref_dip",$data,$dt)){
				$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			}else{
				$ret['text'] = "Data berhasil diupdate";
			}
		}else{
			if(!$this->db->insert("ref_dip",$data)){
				$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			}else{
				$ret['text'] = "Data berhasil disimpan";
			}
		}
		
		$rs = $this->db->get_where("ref_dip",array("dip_code"=>$data['dip_code']));
		$item = $rs->row();
		$data2['dip_id'] = $item->dip_id;
		$this->db->delete("ref_dip_file",$data2);
		
		foreach($_POST['dip_file'] as $key=>$value){
			$data2['dip_file'] = $value;
			$this->db->insert("ref_dip_file",$data2);
		}
		echo json_encode($ret);
		
	}
	
	
	function savePelatihan(){
		
		$arrnot 	= array("","id_edit","nama_file","size_file","path_file");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("","tgl_mulai","tgl_ahir");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		foreach($_POST as $key=>$value){
			if(!is_array($value)){
				if(array_search($key,$arrnot)==''){
					if(array_search($key,$keyin)!=""){
						$keys =  array_keys($keyin,$key);
						$key = $keyout[$keys[0]];
					}
					
					if(array_search($key,$keydate)!=''){
						$value = (($value!='')?$value:"00-00-0000");
						$v = explode(" ",$value);
						$val = explode("-",$v[0]);
						if(count($v)>1){
							$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
						}else{
							$value = $val[2]."-".$val[1]."-".$val[0];
						}
					}
					$data[$key] = $value;
				}
			}
		}
		
		$data['dip_file'] = $this->input->post('nama_file');
		$data['id_user'] = $this->session->userdata('user_name');
		
		
		if($this->input->post('id_edit')!=''){
			$dt['id'] = $this->input->post('id_edit');
			if(!$this->db->update("ref_pelatihan",$data,$dt)){
				$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			}else{
				$ret['text'] = "Data berhasil diupdate";
			}
		}else{
			if(!$this->db->insert("ref_pelatihan",$data)){
				$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			}else{
				$ret['text'] = "Data berhasil disimpan";
			}
		}
	
		
		$data2['dip_file']=$this->input->post('nama_file');
		$data2['dip_path']=$this->input->post('path_file');
		$data2['dip_size']=$this->input->post('size_file');
		
			$this->db->insert("ref_dip_file",$data2);
		
		echo json_encode($ret);
		
	}
	
	function savePengguna(){
		
		$arrnot 	= array("","id_edit","user_pass");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","user_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$data['skpd_id'] = $this->session->userdata('skpd_id');
		if($this->input->post('user_pass')=='' && $this->input->post('id_edit')!=''){
			
			$rs = $this->db->get_where("ref_user",array("user_id"=>$this->input->post('id_edit')));
			$item = $rs->row();
			$data['user_pass'] = $item->user_pass;
		}else{
			$data['user_pass'] = md5($this->input->post('user_pass'));
		}
		$this->dashboard_mod->save("ref_user",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
		
	}
	function delPelatihan(){
		$ret['text'] = "Data terhapus...";
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("ref_pelatihan",$data)){
			$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			echo json_encode($ret);
			exit;
		}
		echo json_encode($ret);
	}	
	
	function delPengguna(){
		$ret['text'] = "Data terhapus...";
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("ref_user",$data)){
			$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			echo json_encode($ret);
			exit;
		}
		echo json_encode($ret);
	}	
	
	function saveResponse(){
		$dt['req_id'] = $this->input->post('req_id');
		$data['req_respon'] = $this->input->post('req_respon');
		$data['req_sts'] = $this->input->post('req_sts');
		$data['skpd_id'] = $this->input->post('skpd_id');
		$data['dip_file'] = $_POST['dip_file'][0];
		if(!$this->db->update("req_info",$data,$dt)){
			$ret['text'] = "Respon Gagal Terkirim\n".$this->db->_error_message();
		}else{
			$ret['text'] = "Respon Terkirim";
		}
		echo json_encode($ret);
	}
	#to do pendaftar
function sendRegister(){
		$keynot = array("","scanfile","submit","userpass2");
		$data = array();
		foreach($_POST as $key=>$value){
			if(array_search($key,$keynot)==''){
				$data[$key] = $value;
			}
		}
		
		$data2['userpass'] = $this->input->post('userpass2');
		$ret = array();
		
		if($data['userpass']!=$data2['userpass']){
			$ret['text'] = "Konfirmasi Password tidak cocok";
			echo json_encode($ret);
		}else{
			$data['scanfile'] = $_POST['scanfile'][0];
			
			if(!$this->db->insert("ref_applicant",$data)){
				$ret['text'] = "Pendaftaran tidak berhasil".$this->db->_error_message();
				echo json_encode($ret);
			}else{
				$ret['text'] = "Pendaftaran berhasil";
				echo json_encode($ret);
			}

		}
		
		
	}
	
	function updateRegister(){
		$dt['username'] = $this->session->userdata('username');
		$keynot = array("","scanfile","submit","userpass2");
		$data = array();
		foreach($_POST as $key=>$value){
			if(array_search($key,$keynot)==''){
				$data[$key] = $value;
			}
		}
		$data['scanfile'] = $_POST['scanfile'][0];
		if(!$this->db->update("ref_applicant",$data,$dt)){
			$ret['text'] = "Update Profil tidak berhasil\n".$this->db->_error_message();
			echo json_encode($ret);
			exit;
		}else{
			$ret['text'] = "Update Profil Berhasil";
			echo json_encode($ret);
			exit;
		}
		
	}
	
	function updatepassRegister(){
		$dt['username'] = $this->session->userdata('username');
		$dt['userpass'] = $this->input->post('userpass');
		$data['userpass'] = $this->input->post('userpass');
		$data2['userpass'] = $this->input->post('userpass2');
		$data3['userpass'] = $this->input->post('userpass3');
		$rs = $this->db->get_where("ref_applicant",$dt);
		if($rs->num_rows()==0){
			$ret['text'] = "Password lama salah";
			echo json_encode($ret);
			exit;
		}else{
			if($data2['userpass']!=$data3['userpass']){
				$ret['text'] = "Konfirmasi Password baru salah";
				echo json_encode($ret);
				exit;
			}else{
				if(!$this->db->update("ref_applicant",$data3,$dt)){
					$ret['text'] = "Update Password tidak berhasil\n".$this->db->_error_message();
					echo json_encode($ret);
					exit;
				}else{
					$ret['text'] = "Update Password Berhasil";
					echo json_encode($ret);
					exit;
				}
			}
		}
	}
	
	function do_upload_scanfile(){
		$path_parts = pathinfo($_FILES["scanfile"]["name"]);
		$extension = $path_parts['extension'];
		
		$config['upload_path'] = BASEPATH.'../upliddir/';
		$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
		$config['max_size']	= '5000000';
		$config['file_name'] = $this->input->post('filename').".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('scanfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
		
	}
	
	

}