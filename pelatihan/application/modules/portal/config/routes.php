<?php 
$route['(portal)'] = "portal/default";
$route['(portal)/visimisi'] 			= "page/loadportal/visimisi";
$route['(portal)/tugaskewenangan'] 		= "page/loadportal/tugaskewenangan";
$route['(portal)/alur_all'] 			= "page/loadportal/alur_all";
$route['(portal)/alurdip'] 				= "page/loadportal/alurdip";
$route['(portal)/alurpermohonan'] 		= "page/loadportal/alurpermohonan";
$route['(portal)/dip_data'] 			= "page/loadportal/dip_data";
$route['(portal)/dip_timeline'] 		= "page/loadportal/dip_timeline";
$route['(portal)/dip_statistik'] 		= "page/loadportal/dipstatistik";
$route['(portal)/masuk'] 				= "page/loadportal/masuk";
$route['(portal)/signup'] 				= "page/loadportal/signup";
$route['(portal)/dip_detail'] 			= "page/loadportal/dip_detail";
$route['(portal)/dip_detail/(:num)'] 	= "page/loadportal/$1/$2";
$route['(portal)/profile'] 				= "page/loadportal/profile";
$route['(portal)/reqinfo'] 				= "page/loadportal/reqinfo";
$route['(portal)/gantipass'] 			= "page/loadportal/gantipass";

