<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>DISNAKER | KAB. BOGOR</title>

    <link rel="shortcut icon" href="<?=base_url()?>assets/front/img/favicon2.png" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="<?=base_url()?>assets/front/css/preload.css" rel="stylesheet">
    
    <!-- Compiled in vendors.js -->
    <!--
    <link href="<?=base_url()?>assets/front/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/front/css/bootstrap-switch.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/front/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/front/css/animate.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/front/css/slidebars.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/front/css/lightbox.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/front/css/jquery.bxslider.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/front/css/buttons.css" rel="stylesheet">
    -->

    <link href="<?=base_url()?>assets/front/css/vendors.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/front/css/syntaxhighlighter/shCore.css" rel="stylesheet" >

    <link href="<?=base_url()?>assets/front/css/style-blue3.css" rel="stylesheet" title="default">
    <link href="<?=base_url()?>assets/front/css/width-full.css" rel="stylesheet" title="default">
	<link href="<?=base_url()?>assets/front/css/custom.css" rel="stylesheet" title="default">
    
	<link href="<?=base_url()?>assets/front/js/plugins/growl/css/jquery.growl.css" rel="stylesheet" title="default">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="<?=base_url()?>assets/front/js/html5shiv.min.js"></script>
        <script src="<?=base_url()?>assets/front/js/respond.min.js"></script>
    <![endif]-->
	<script src="<?=base_url()?>assets/front/js/vendors.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/plugins/select2/select2.js"></script>
	<link href="<?=base_url()?>assets/front/js/plugins/select2/select2.css" rel="stylesheet">
	<script type="text/javascript" src="<?=base_url()?>assets/js/plugins/debounce/jquery-debounce.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/plugins/debounce/jquery-debounce.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/plugins/growl/js/jquery.growl.js"></script>

	<script>
		function skpdFormatResult(data) {
			var markup= "<li>"+ data.skpd_id+"-"+data.skpd_name+"</li>";
			return markup;
		}

		function skpdFormatSelection(data) {
			return data.skpd_name;
		}
		
		

		$(document).ready(function(){
			 
		$(document).on('submit','#form11',function(event) {
				event.preventDefault();
				var $form = $( this );
				$('input').each(function(index,item){
					$(item).attr('disabled',false);
				});
				$('select').each(function(index,item){
					$(item).attr('disabled',false);
				});
				
				var url = $form.attr( 'action' );
				
				if(confirm('Login Admin ..?')){
					$.post(url, $form.serialize(),
					function(data) {
						window.location = "<?=base_url()?>auth";
						var ret = $.parseJSON(data);
						alert(ret.text);
						//top.location="<?=base_url()?>/auth";
					});		
				}
			});
			
			
			$(document).on('submit','#form-cek',function(event) {
				event.preventDefault();
				var $form = $( this );
				$('input').each(function(index,item){
					$(item).attr('disabled',false);
				});
				$('select').each(function(index,item){
					$(item).attr('disabled',false);
				});
				
				var url = $form.attr( 'action' );
				
				if(confirm('Cek Sertifikat ..?')){
					$.post(url, $form.serialize(),
					function(data) {
						//window.location = "<?=base_url()?>"+ret.text+"";
						var ret = $.parseJSON(data);
						//alert(ret.text);
						top.location="<?=base_url()?>?id="+ret.text+"";
					});		
				}
			});
			
			$("#skpd_id").select2({
				id: function(e) { return e.skpd_id }, 
				placeholder: "Login Sebagai",
				minimumInputLength: 0,
				multiple: false,
				ajax: { 
					url: "<?=base_url()?>dashboard/ajaxfile/cariskpd",
					dataType: 'jsonp',
					type:'POST',
					quietMillis: 100,
					data: function (term, page) {
						return {
							keyword: term, //search term
							per_page: 5, // page size
							page: page, // page number
						};
					},
					results: function (data, page) { 
						var more = (page * 5) < data.result;
						return {results: data.rows, more: more};
					}
				},
				formatResult: skpdFormatResult, 
				formatSelection: skpdFormatSelection
			});
		});
	</script>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>


<div id="sb-site">
<div class="boxed">

<header id="header-full-top" class="hidden-xs header-full-dark">
    <div class="container">
        <div class="header-full-title">
            <h1><a href="<?=base_url()?>">Pendaftaran Pelatihan&nbsp;&nbsp;</a></h1>
			<p class="animated fadeInRight">&nbsp;&nbsp;<span>DISNAKER </span> KAB. BOGOR</p>
        </div>
        <nav class="top-nav">
            <ul class="top-nav-social">
                <li><a href="#" class="animated fadeIn animation-delay-7 twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-8 facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-9 google-plus"><i class="fa fa-google-plus"></i></a></li>
            </ul>

            <div class="dropdown animated fadeInDown animation-delay-11">
                <a href="<?=base_url()?>dashboard"><i class="fa fa-unlock-alt"></i> Login Back Office</a>
               <!-- <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated fadeInUp">
                    <form role="form" class="loginform" action="<?=base_url()?>auth/login" id="form11" name="form11" method="post">
                        <h4>Login Back Office</h4>

                        <div class="form-group">
                            <div class="input-group login-input">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Username">
                            </div>
                            <br>
                            <div class="input-group login-input">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" class="form-control" id="user_pass" name="user_pass" placeholder="Password">
                            </div>
							<br>
							<div class="input-group login-input">
                                <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                <input type="text" id="skpd_id" name="skpd_id" placeholder="Login Sebagai" style="min-width:244px;width:244px;height:34px;">
                            </div>
                            <button type="submit" class="btn btn-ar btn-primary pull-right">Login</button>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>-->
            </div> <!-- dropdown -->

            <div class="dropdown animated fadeInDown animation-delay-13">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-search-box animated fadeInUp">
                    <form role="form">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-ar btn-primary" type="button">Go!</button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                </div>
            </div> <!-- dropdown -->
			<div class="dropdown animated fadeInDown animation-delay-13">
				<a href="javascript:void(0);" class="sb-toggle-right" title="Login Pemohon"><i class="fa fa-user animated pulse infinite"></i></a>
			</div>
        </nav>
    </div> <!-- container -->
</header> <!-- header-full -->
<nav class="navbar navbar-static-top navbar-default navbar-header-full yamm" role="navigation" id="header">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?=base_url()?>/pelatihan">Pendaftaran Pelatihan <span>DISNAKER KAB. BOGOR</span></a>
			<nav class="top-nav hidden-sm hidden-md hidden-lg">
				<ul class="top-nav-social hidden-xs">
					<li><a href="#" class="animated fadeIn animation-delay-7 twitter"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#" class="animated fadeIn animation-delay-8 facebook"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#" class="animated fadeIn animation-delay-9 google-plus"><i class="fa fa-google-plus"></i></a></li>
				</ul>

				<div class="dropdown animated fadeInDown animation-delay-11">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-unlock-alt"></i> <span class="hidden-sm hidden-xs">Login Back Office</span></a>
					<div class="dropdown-menu dropdown-menu-right dropdown-login-box animated fadeInUp">
						<form role="form">
							<h4>Login Back Office</h4>

							<div class="form-group">
								<div class="input-group login-input">
									<span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
									<input type="text" class="form-control" id="user_name" name="user_name" placeholder="Username">
								</div>
								<br>
								<div class="input-group login-input">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="text" class="form-control" id="user_name" name="user_name" placeholder="Username">
								</div>
								<br>
								<div class="input-group login-input">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="text" id="skpd_id" name="skpd_id" placeholder="SKPD" style="min-width:244px;width:244px">
								</div>
								<button type="submit" class="btn btn-ar btn-primary pull-right">Login</button>
								<div class="clearfix"></div>
							</div>
						</form>
					</div>
				</div> <!-- dropdown -->

				<div class="dropdown animated fadeInDown animation-delay-13">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
					<div class="dropdown-menu dropdown-menu-right dropdown-search-box animated fadeInUp">
						<form role="form">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search...">
								<span class="input-group-btn">
									<button class="btn btn-ar btn-primary" type="button">Go!</button>
								</span>
							</div><!-- /input-group -->
						</form>
					</div>
				</div> <!-- dropdown -->
				<div class="dropdown animated fadeInDown animation-delay-13">
					<a href="javascript:void(0);" class="sb-toggle-right" title="Login Pemohon"><i class="fa fa-user animated pulse infinite"></i></a>
				</div>
			</nav>
        </div> <!-- navbar-header -->

        <!-- Login pemohon -->
        
		<div class="pull-right">
            
        </div>
		
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="#">
                    <a href="<?=base_url()?>">Home</a>
                </li>
				<li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Tentang Disnaker </a>
                     <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="<?=base_url()?>portal/visimisi">Visi & Misi</a></li>
						<li><a href="<?=base_url()?>portal/tugaskewenangan">Tugas dan Kewenangan</a></li>
                    </ul>
                </li>
				<li class="#">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Alur Pendaftaran Online</a>
                     <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="<?=base_url()?>portal/alurdip">Alur Pendaftaran Peserta Pelatihan</a></li>
						<!--<li><a href="<?=base_url()?>portal/alurpermohonan">Alur Permohonan Informasi & Keberatan</a></li>-->
                    </ul>
                </li>
				<!--<li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Data DISNAKER </a>
                     <ul class="dropdown-menu dropdown-menu-left">
                        <!--<li><a href="<?=base_url()?>portal/dip_data">Data Informasi Publik</a></li>
						<li><a href="<?=base_url()?>portal/dip_timeline">Timeline Data</a></li>
						<li><a href="<?=base_url()?>portal/dip_statistik">Statistik Pelayanan Data</a></li>
                    </ul>
                </li>-->
             </ul>
        </div><!-- navbar-collapse -->
    </div><!-- container -->
</nav>
 <?php
   if(!isset($page)){
		$this->load->view("portal/default");
   }else{
		$this->load->view("portal/".$page);
   }
?>   
<footer id="footer">
    <p>&copy; 2019 <a href="<?=base_url()?>/pelatihan">DISNAKER KABUPATEN BOGOR</a>, All rights reserved.</p>
</footer>

</div> <!-- boxed -->
</div> <!-- sb-site -->
<div class="sb-slidebar sb-right">
    <?php
	if(!$this->session->userdata('is_login_pelatihanuser')){
	?>
	<div class="slidebar-social-icons">
		 <form role="form" id="form1" name="form1" method="post" action="<?=base_url()?>portal/login" enctype="multiparth/form-data">
               
			<h4>Login Pemohon</h4>
			<div class="form-group">
				<div class="input-group login-input">
					<span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
					<input type="text" class="form-control" placeholder="Username" id="username" name="username" required>
				</div>
				<br>
				<div class="input-group login-input">
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
				</div>
				<br>
				
				<button type="submit" class="btn btn-ar btn-primary pull-right">Login</button>
				<br>
				<a href="<?=base_url()?>portal/signup"><h4>Belum Terdaftar ?</h4></a>
				<div class="clearfix"></div>
			</div>
		</form>
	</div>
	<?php
	}else{
	?>
	<div class="slidebar-social-icons">
		<h4>Hi, <?=$this->session->userdata('username')?></h4>
		<ul class="slidebar-menu">
			<li><a href="<?=base_url()?>portal/profile">Profil</a></li>
			<li><a href="<?=base_url()?>portal/gantipass">Ganti Password</a></li>
			<li><a href="<?=base_url()?>portal/logout">Logout</a></li>
		</ul>
	</div>
	<?php
	}
	?>
</div> <!-- sb-slidebar sb-right -->


<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>

<!-- Scripts -->
<!-- Compiled in vendors.js -->
<!--
<script src="<?=base_url()?>assets/front/js/jquery.min.js"></script>
<script src="<?=base_url()?>assets/front/js/jquery.cookie.js"></script>
<script src="<?=base_url()?>assets/front/js/imagesloaded.pkgd.min.js"></script>
<script src="<?=base_url()?>assets/front/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/front/js/bootstrap-switch.min.js"></script>
<script src="<?=base_url()?>assets/front/js/wow.min.js"></script>
<script src="<?=base_url()?>assets/front/js/slidebars.min.js"></script>
<script src="<?=base_url()?>assets/front/js/jquery.bxslider.min.js"></script>
<script src="<?=base_url()?>assets/front/js/holder.js"></script>
<script src="<?=base_url()?>assets/front/js/buttons.js"></script>
<script src="<?=base_url()?>assets/front/js/jquery.mixitup.min.js"></script>
<script src="<?=base_url()?>assets/front/js/circles.min.js"></script>
<script src="<?=base_url()?>assets/front/js/masonry.pkgd.min.js"></script>
<script src="<?=base_url()?>assets/front/js/jquery.matchHeight-min.js"></script>
-->


<!-- Syntaxhighlighter -->
<script src="<?=base_url()?>assets/front/js/syntaxhighlighter/shCore.js"></script>
<script src="<?=base_url()?>assets/front/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="<?=base_url()?>assets/front/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="<?=base_url()?>assets/front/js/app.js"></script>
<script src="<?=base_url()?>assets/front/js/index.js"></script>


<script>
	
</script>
</body>

</html>
