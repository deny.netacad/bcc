<?php
	$where = " where a.username='".$this->session->userdata('username')."' ";
	$per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= " and (a.req_info like '%".$cari."%' or a.req_title like '%".$cari."%'
 or c.skpd_name like '%".$cari."%'
	)";
	
	$n = intval($this->uri->segment(5));
	$q = "SELECT a.*,b.job,b.city,c.skpd_name,d.req_feedback,e.alasan FROM req_info a
	INNER JOIN ref_applicant b ON a.username=b.username
	LEFT JOIN ref_skpd c ON a.skpd_id=c.skpd_id
	left join 
	(
	SELECT aa.feedback_id,aa.req_feedback,aa.req_id FROM req_feedback aa
	WHERE aa.feedback_id=(
	SELECT MAX(bb.feedback_id) FROM req_feedback bb
	WHERE aa.req_id=bb.req_id)
	) d on a.req_id=d.req_id
	left join 
	(
	SELECT aa.keberatan_id,aa.alasan,aa.req_id FROM req_keberatan aa
	WHERE aa.keberatan_id=(
	SELECT MAX(bb.keberatan_id) FROM req_keberatan bb
	WHERE aa.req_id=bb.req_id)
	) e on a.req_id=e.req_id
	";
	$rs = $this->db->query("$q $where group by a.req_id");
	
	$row = $rs->row_array();
	$totrows = $rs->num_rows();
	$config['base_url'] = base_url().'portal/page/data/reqinfo_keberatan';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("
	$q $where  group by a.req_id
	ORDER BY b.tmstamp DESC,d.feedback_id desc limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
	
?>
<div class="blog-pagination">
	<div class="pages blogpages">
		<span class="paginate_info pageof">Page <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
		<?=$data['pagination']?>
	</div>
</div>
<table class="table table-bordered">
	<colgroup>
		<col style="width:25px"></col>
		<col style="width:80px"></col>
		<col style="width:auto"></col>
		<col style="width:auto"></col>
		<col style="width:auto"></col>
		<col style="width:auto"></col>
		<col style="width:auto"></col>
		<col style="width:auto"></col>
		<col style="width:100px"></col>
	</colgroup>
  <thead>
	  <tr>
		<th>#</th>
		<th>Tanggal</th>
		<th>Nama Pemohon</th>
		<th>Kota</th>
		<th>Pekerjaan</th>
		<th>Info yang dimohon</th>
		<th>Skpd</th>
		<th>Waktu</th>
		<th>Respon</th>
		<th>Aksi</th>
	  </tr>
  </thead>
  <tbody>
  <?php
  $n = 0;
  $job = array("","Wiraswasta","Buruh","Petani","Nelayan","PNS","Swasta","Pedagang","Pekerja Rumah Tangga","LSM","Tenaga Pendidik","Pengacara","Tenaga Kesehatan"
								,"Peneliti","Seniman","Wartawan","Mahasiswa/Pelajar","Lain-lain");
								
  foreach($data['posts']->result() as $item){
  $n++;
  ?>
	  <tr>
		<td><?=$n?></td>
		<td><?=$item->req_date?></td>
		<td><?=$this->session->userdata('fullname')?></td>
		<td><?=$item->city?></td>
		<td><?=$job[$item->job]?></td>
		<td><?=$item->req_info?></td>
		<td><?=$item->skpd_name?></td>
		<td><?=$item->tmstamp?></td>
		<td>
		<div>
		<?=$item->req_respon?><?=$item->req_reason?>
		</div>
		<div>
		<a href="#" style="color:#3399ff"><?=$item->req_feedback?></a>
		</div>
		<div>
		<a href="#"><?=$item->alasan?></a>
		</div>
		<div>
		<?php
		if($item->dip_file!=''){
		?>
		<a href="<?=base_url()?>portal/page/downloadfile2/<?=$item->req_id?>"><?=$item->dip_file?></a>
		<?php
		}
		?>
		</div>
		</td>
		<td>
		<a href="#" class="btn small orange keberatan" data="<?=$item->req_id?>" title="Ajukan Keberatan">&nbsp;Ajukan Keberatan&nbsp;</a>
		<a href="#" class="btn small blue feedback" data="<?=$item->req_id?>" title="Beri Feedback">&nbsp;Beri Feedback&nbsp;</a>
		</td>
	  </tr>
	<?php
	}
	?>
  </tbody>
</table>   