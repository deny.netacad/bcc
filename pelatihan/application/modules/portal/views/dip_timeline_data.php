<h2 class="no-margin-top">Jadwal Pelatihan Dinperindag Jateng</h2>
<?php
	$per_page = 3;
	$start = (($this->uri->segment(5)>0)?$this->uri->segment(5):0);
	$icon = array("video"=>"fa-file-video-o","pdf"=>"fa-file-pdf-o","image"=>"fa-file-image-o");
	$iconcolor = array("icon-ar-warning","icon-ar-success","icon-ar-help");
	
	$rs = $this->db->query("SELECT a.dip_file,a.id,DATE_FORMAT(a.log,'%d/%m/%Y') AS createon,a.judul as dip_title,a.isi as dip_desc 
					,MONTHNAME(a.log) AS month_name
					FROM ref_pelatihan a 
					inner join ref_dip_file b on a.dip_file=b.dip_file
					where b.dip_file is not null
					ORDER BY a.log DESC");
	
	$totrows = $rs->num_rows();
	$config['base_url'] = base_url().'portal/page/data/dip_timeline';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);
	
	$data['pagination'] = $this->pagination->create_links();
	$rs = $this->db->query("SELECT a.dip_file,a.id,DATE_FORMAT(a.log,'%d/%m/%Y') AS createon,a.judul as dip_title,a.isi as dip_desc 
					,MONTHNAME(a.log) AS month_name
					FROM ref_pelatihan a 
					inner join ref_dip_file b on a.dip_file=b.dip_file
					where b.dip_file is not null
					ORDER BY a.log DESC LIMIT ".$start.",".$per_page."");
	$i = 0;
?>
<div class="blog-pagination">
	<div class="pages blogpages">
		<span class="paginate_info pageof">Page <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
		<?=$data['pagination']?>
	</div>
</div>
<ul class="timeline-2">
	<?php
	foreach($rs->result() as $item){ 
		$te=explode('.',$item->dip_file);
	?>
	<li class="animated fadeInRight animation-delay-8">
		<time class="timeline-time" datetime=""><?=$item->createon?> <span><?=$item->month_name?></span></time>
		<i class="timeline-2-point"></i>
		<div class="panel panel-default">
			<div class="panel-heading"><?=$item->dip_title?></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-4">
						<span class="icon-ar icon-ar-xl-lg icon-ar-square <?=$iconcolor[$i]?>"><i class="fa <?=$icon[strtolower($te[1])]?>"></i></span>
					</div>
					<div class="col-sm-8">
						<p><?=substr(strip_tags($item->dip_desc,''),0,100)?> ...</p>
						<a href="<?=base_url()?>portal/dip_detail/<?=$item->id?>" class="btn btn-small btn-warning">Lihat</a>
					</div>
				</div>
			</div>
		</div>
	</li>
	<?php
	$i++;
	}
	?>
</ul>