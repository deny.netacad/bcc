<link rel="stylesheet" href="<?=$def_js?>plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>
<script>
$(document).ready(function(){
	 
	
	$("#form1").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form1').trigger('reset');
			});		
		}else{
			
			$('#form1').trigger('reset');
		}
	});
	
	$('#form1').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
	
	$('#myTab li a').on('click',function(e){
		switch($(this).attr('href')){
			case "#tab-sample2":
				refreshData2();
			break;
		}
	});
	
	$('#per_page').change(function(){
		refreshData();
	});
	
	$('#cari').keyup( $.debounce(250,refreshData2));
	
	$(document).on('click','a.ajax-replace', function(){
		$.ajax({
			type:'post',
			url:$(this).attr('href'),
			data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
			beforeSend:function(){
			},
			success:function(response){
				$('#result').html(response);
			}
		});
		return false;
	});
	
	$(document).on('click','a.keberatan',function(){
		var $this = $('a.keberatan');
		var index = $this.index($(this));
		$.ajax({
			type:'post',
			url:'<?=base_url()?>portal/page/view/reqinfo_keberatan_form',
			data:{ 'req_id':$this.eq(index).attr('data') },
			beforeSend:function(){
				
			},
			success:function(response){
				$('#result2').html(response);
			}
		});
	});
	
	$(document).on('click','a.feedback',function(){
		var $this = $('a.feedback');
		var index = $this.index($(this));
		$.ajax({
			type:'post',
			url:'<?=base_url()?>portal/page/view/reqinfo_feedback_form',
			data:{ 'req_id':$this.eq(index).attr('data') },
			beforeSend:function(){
				
			},
			success:function(response){
				$('#result2').html(response);
			}
		});
	});
	
});

function refreshData2(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/reqinfo_keberatan',
		data:{ 'per_page':$('#tab-sample2 #per_page').val(),'cari':$('#tab-sample2 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result2').html(response);
		}
	});
}
</script>
<header class="main-header">
    <div class="container">
        <h1 class="page-title">Permintaan Informasi</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Permintaan Informasi</li>
        </ol>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#tab-sample1" data-toggle="tab">Form Permintaan Informasi</a></li>
				<li class=""><a href="#tab-sample2" data-toggle="tab">Form Pengajuan Keberatan / Feedback</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab-sample1">
				<!-- Contact Form Start //-->
				<?php
				if($this->session->userdata('is_login_pelatihanuser')){
					$data['username'] = $this->session->userdata('username');
				}else{
					$data['username'] = '';
					redirect('portal/masuk');
				}
				
				$rs = $this->db->get_where("ref_applicant",$data);
				$item = $rs->row();
				$job = array("","Wiraswasta","Buruh","Petani","Nelayan","PNS","Swasta","Pedagang","Pekerja Rumah Tangga","LSM","Tenaga Pendidik","Pengacara","Tenaga Kesehatan"
				,"Peneliti","Seniman","Wartawan","Mahasiswa/Pelajar","Lain-lain");
				?>
				<form id="form1" name="form1" action="<?=base_url()?>portal/saveReqinfo" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label for="noid" class="col-sm-2 control-label">Nama Pemohon <em>*</em></label> 
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="text" name="fullname" class="form-control" id="fullname" value="<?=$item->fullname?>" readonly />                        
						</div>
					</div>
					<div class="form-group">
						<label for="noid" class="col-sm-2 control-label">Pekerjaan <em>*</em></label> 
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="text" name="job" class="form-control" id="job" value="<?=$job[$item->job]?>" readonly />                        
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Alamat <em>*</em></label>
						<div class="col-sm-2 col-md-8 col-lg-8">
						<textarea name="addr" id="addr" class="form-control" cols="2" rows="2"><?=$item->addr?></textarea>
						</div>
					</div>	
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Kota <em>*</em></label> 
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="text" name="city" class="form-control" id="city" value="<?=$item->city?>">                        
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Kode POS <em>*</em></label> 
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="text" name="postcode" class="form-control" id="postcode" value="<?=$item->postcode?>">                        
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Email <em>*</em></label> 
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="email" name="email" class="form-control" id="email" value="<?=$item->email?>">                        
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Telp. Rumah <em>*</em></label> 
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="text" name="phone_home" class="form-control" id="phone_home" value="<?=$item->phone_home?>">                        
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Telp. Mobile <em>*</em></label> 
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="text" name="phone_mobile" class="form-control" id="phone_mobile" value="<?=$item->phone_mobile?>">                        
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Judul Dokumen <em>*</em></label>
						<div class="col-sm-2 col-md-8 col-lg-8">
						<input type="text" name="req_title" class="form-control" id="req_title" value="" placeHolder="Judul/Nama Dokumen">                        
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Isi Dokumen <em>*</em></label>
						<div class="col-sm-2 col-md-8 col-lg-8">
						<textarea name="req_info" id="req_info" class="form-control" cols="2" rows="4"></textarea>
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Alasan Permintaan Informasi <em>*</em></label>
						<div class="col-sm-2 col-md-8 col-lg-8">
						<textarea name="req_alasan" id="req_alasan" class="form-control" cols="2" rows="4"></textarea>
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">SKPD <em>*</em></label>                        
						<?php
						$rs_skpd = $this->db->get("ref_skpd");
						?>		
						<div class="col-sm-2 col-md-8 col-lg-8">		
						<select name="skpd_id" id="skpd_id" class="form-control">
							<option value="">| Kosongkan jika ditujukan ke PPID Kabupaten</option>
							<?php
							foreach($rs_skpd->result() as $itemskpd){
							?>
							<option value="<?=$itemskpd->skpd_id?>" <?=(($item->skpd_id==$itemskpd->skpd_id)?"selected":"")?>><?=$itemskpd->skpd_name?></option>
							<?php
							}
							?>
						</select> 
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">Metode Mendapatkan Informasi <em>*</em></label>                           
						<div class="col-sm-2 col-md-8 col-lg-8">
						<select name="req_type" id="req_type" class="form-control" required>
							<option value="1" <?=(($item->req_type==1)?"selected":"")?>>Download</option>
							<option value="2" <?=(($item->req_type==2)?"selected":"")?>>Datang Langsung</option>
						</select> 
						</div>
					</div>
					<div class="form-group">	
						<label for="noid" class="col-sm-2 control-label">&nbsp;</label>
						<div class="col-sm-2 col-md-8 col-lg-8">
						<button type="submit" name="submit" class="buttoncontact" id="buttonsend">Submit</button>
						</div>
					</div>	
				</form>
				</div>
				<div class="tab-pane" id="tab-sample2">
					<div class="panel panel-primary">
						<div class="panel-heading">Filter Pencarian Data</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="cari" name="cari" placeholder="Cari Judul/Kategori/Ringkasan/Kode Dokumen/Penanggung Jawab">
								<select class="form-control" id="per_page" name="per_page">
									<option>10</option>
									<option>20</option>
									<option>30</option>
									<option>40</option>
									<option>50</option>
								</select>
								<button type="submit" class="btn btn-ar btn-primary">Cari</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="result2" style="overflow:auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>