<section class="carousel-section">
    <div id="carousel-example-generic" class="carousel carousel-razon slide" data-ride="carousel" data-interval="5000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-7">
                            <div class="carousel-caption">
                                <div class="carousel-text">
                                   <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">Pendaftaran Online Pelatihan</h1>
                                   <h2 class="animated fadeInDownBig animation-delay-5  crousel-subtitle">DISNAKER KAB. BOGOR</h2>
                                   <ul class="list-unstyled carousel-list">
                                       <li class="animated bounceInLeft animation-delay-11"><i class="fa fa-check"></i>Pendaftaran Online Peserta Pelatihan</li>
                                       <li class="animated bounceInLeft animation-delay-13"><i class="fa fa-check"></i>Pendaftaran Pelatihan Antar Perusahaan</li>
                                       <li class="animated bounceInLeft animation-delay-15"><i class="fa fa-check"></i>Kartu Pendaftaran Pencari Kerja (AK.I)</li>
                                   </ul>
                                   <p class="animated fadeInUpBig animation-delay-17">Persembahan <span>DISNAKER KAB. BOGOR</span></p>
                               </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-5 hidden-xs carousel-img-wrap">
                            <div class="carousel-img">
                                <img src="<?=base_url()?>assets/front/img/demo/pre.png" class="img-responsive animated bounceInUp animation-delay-3" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8">
                            <div class="carousel-caption">
                                <div class="carousel-text">
                                   <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">Pelayanan Disnaker Kab Bogor </h1>
                                   <!--<h2 class="animated fadeInDownBig animation-delay-5  crousel-subtitle">Dasar hukum </h2>-->
                                   <ul class="list-unstyled carousel-list">
                                       <li class="animated bounceInLeft animation-delay-11"><i class="fa fa-check"></i>Izin Pendirian Lembaga Latihan Kerja</li>
                                       <li class="animated bounceInLeft animation-delay-13"><i class="fa fa-check"></i>Izin Penggunaan Peralatan K3</li>
									    <li class="animated bounceInLeft animation-delay-12"><i class="fa fa-check"></i>Pencatatan Perjanjian Kerja Waktu Tertentu (PKWT)</li>
										<li class="animated bounceInLeft animation-delay-12"><i class="fa fa-check"></i>Pencatatan Serikat Pekerja/ Serikat Buruh (SP/SB)</li>
										 <li class="animated bounceInLeft animation-delay-11"><i class="fa fa-check"></i>Pendaftaran Perjanjian Kerja Bersama (PKB)</li>
                                       <li class="animated bounceInLeft animation-delay-12"><i class="fa fa-check"></i>Pengesahan Peraturan Perusahaan (PP)</li>
                                   </ul>
                                   <p class="animated fadeInUpBig animation-delay-17">Demi menunjang tujuan strategis dan pemanfaatan teknologi informasi</p>
                               </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4 hidden-xs carousel-img-wrap">
                            <div class="carousel-img">
                                <img src="<?=base_url()?>assets/front/img/demo/pre.png" class="img-responsive animated bounceInUp animation-delay-3" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7 col-sm-9">
                            <div class="carousel-caption">
                                <div class="carousel-text">
                                   <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">Pelayanan Disnaker Kab Bogor  </h1>
                                   <!--<h2 class="animated fadeInDownBig animation-delay-5  crousel-subtitle">Serentak Tertib Administrasi Dokumentasi Seluruh SKPD Kab. Merauke</h2>-->
                                   <ul class="list-unstyled carousel-list">
                                      
									   <li class="animated bounceInLeft animation-delay-13"><i class="fa fa-check"></i>Penyelesaian Kasus Perselisishan Hubungan Industrial</li>
									   <li class="animated bounceInLeft animation-delay-14"><i class="fa fa-check"></i>Perpanjangan Izin / Penambahan Jurusan</li>
									   <li class="animated bounceInLeft animation-delay-15"><i class="fa fa-check"></i>Rekomendasi Jaminan Sosial Tenaga Kerja (JAMSOSTEK)</li>
									   <li class="animated bounceInLeft animation-delay-16"><i class="fa fa-check"></i>Wajib Lapor Ketenagakerjaan (WLK)</li>
									   <li class="animated bounceInLeft animation-delay-16"><i class="fa fa-check"></i>Pelayanan Pembuatan Kartu Pendaftaran Transmigrasi</li>
									   
                                   </ul>
                                   <p class="animated fadeInUpBig animation-delay-17">Mendukung transparansi dokumentasi informasi</p>
                               </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5 col-sm-3 hidden-xs carousel-img-wrap">
                            <div class="carousel-img">
                                <img src="<?=base_url()?>assets/front/img/demo/pre.png" class="img-responsive animated bounceInUp animation-delay-3" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</section> <!-- carousel -->

<section class="margin-bottom">
    <div class="container">
        <div class="row">
		<div class="col-md-12 ">
	   <div class="content-box box-default animated fadeInUp animation-delay-12">
	      <?php 
		  if($this->input->get('id')!=''){
		  $temp=explode('/',$this->input->get('id'));
		  $rs=$this->db->query("select *,date_format(b.dob,'%d %M %Y') as dob,date_format(c.tgl_mulai,'%d %M %Y') as tgl_mulai,date_format(c.tgl_ahir,'%d %M %Y') as tgl_ahir from ref_pendaftaran a
		left join ref_applicant b on a.id_applicant=b.id
		left join ref_pelatihan c on c.id=a.id_pelatihan

		  where id_applicant='".$temp[3]."' and id_pelatihan='".$temp[4]."' and is_posting=1 ");
		  $jml=$rs->num_rows();
		  $item=$rs->row();
		  //echo $jml;
		  if($jml>0){
			  
			  
		  
		  ?>
		  <h2 class="no-margin-top"> <i class="glyphicon glyphicon-list-alt"></i> Selamat Data anda Terverifikasi oleh Kami</h2>
		  <center><hr>
		  <table width="60%" >
			<tr bgcolor=red><td></td><td width=10%></td><td></td><td></td></tr>
		 		 
		 <tr><td>Nama Peserta</td><td width=10%>:</td><td><?=$item->fullname?></td><td></td></tr>
		  <tr><td>Mail Peserta</td><td>:</td><td><?=$item->email?></td><td></td></tr>
		  <tr><td>Nomor ID</td><td>:</td><td><?=$item->noid?></td><td></td></tr>
		  <tr><td>Tempat , Tanggal Lahir </td><td>:</td><td><?=$item->pob?>, <?=$item->dob?></td><td></td></tr>
		
		  
		  <tr><td>Nama Pelatihan</td><td width=10%>:</td><td><?=$item->judul?></td><td></td></tr>
		  <tr><td>Tempat Pelatihan</td><td>:</td><td><?=$item->tempat?></td><td></td></tr>
		  <tr><td>Pelaksanaan</td><td>:</td><td><?=$item->tgl_mulai?> s.d. <?=$item->tgl_ahir?></td><td></td></tr>
		
		  </table>
		  
		  <br>
		   <ul class="bxslider" id="home-block">
                   
					<li>
                        <blockquote class="blockquote-color">
                            <p>Data Anda Terverifikasi , Silahkan ikuti Pelatihan Lain nya.</p>
                            <footer>Disnaker Kab. Bogor</footer>
                        </blockquote>
                    </li>
					
                </ul>
		  </center>
		  <?php }else{?>
		  <br><br>
			  <ul class="bxslider" id="home-block">
                   
					<li>
                        <blockquote class="blockquote-color">
                            <p>Sertifikat Ilegal ! , Sertifikat anda tidak terdaftar di sistem digital sertifikat .</p>
                            <footer>Disnaker Kab. Bogor</footer>
                        </blockquote>
                    </li>
					
                </ul>
			  
			  
		  <?php }}?>
		  </div>
	   </div>
	   <br>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default animated fadeInUp animation-delay-10">
                    <a href="<?=base_url()?>portal/alur_all">
					<span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="fa fa-recycle"></i></span>
                    <h4 class="content-box-title">Manfaat Pendaftaran Online</h4>
					</a>
                    <p>Mekanisme Pelayanan yang ditujukan Untuk Mempermudah Sistem Pendaftaran Pelatihan</p>
					
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default animated fadeInUp animation-delay-14">
                    <a href="<?=base_url()?>portal/dip_data">
					<span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="glyphicon glyphicon-list-alt"></i></span>
                    <h4 class="content-box-title">Pendaftaran Mudah dan Cepat</h4>
					</a>
                    <p>Pendaftaran Online Pelatihan Disnaker Kab. Bogor lebih mudah dan efisien</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default animated fadeInUp animation-delay-16">
					<a href="<?=base_url()?>portal/reqinfo">
                    <span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="glyphicon glyphicon-download-alt"></i></span>
                    <h4 class="content-box-title">E-SERTIFICATE</h4>
					</a>
                    <p>Download Sertifikat secara manual setelah mengikuti pelatihan yang diselenggarakan oleh Dinas Tenaga Kerja Kab. Bogor</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default animated fadeInUp animation-delay-12">
                    <a href="<?=base_url()?>portal/dip_statistik">
					<span class="icon-ar icon-ar-lg icon-ar-round icon-ar-inverse"><i class="fa fa-bar-chart-o"></i></span>
                    <h4 class="content-box-title">Statistik Pendaftaran</h4>
                    </a>
					<p>Laporan Statistik berupa Informasi Pendaftaran,jumlah Pendaftar, berupa grafik dan penjelasan dan didownload berupa laporan pdf</p>
					
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <section class="margin-bottom">
        <p class="lead lead-lg text-center primary-color margin-bottom">Mengenal <strong>Pendaftaran Online Peserta Pelatihan Disnaker Kab. Bogor</strong></p>
       <div class="row">
	   
            <div class="col-md-6">
                <h2 class="no-margin-top">Portal Pelatihan</h2>
                <?php
				$rs = $this->db->query("select  * from ngi_datacenter_desc order by tmstamp desc limit 0,1");
				$item = $rs->row();
				?>
				<?=$item->content?>
            </div>
            <div class="col-md-6" id="dip_timeline">
				<script>
					$(function(){
						$(document).on('click','#dip_timeline a.ajax-replace', function(){
							$.ajax({
								type:'post',
								url:$(this).attr('href'),
								data:{},
								beforeSend:function(){
								},
								success:function(response){
									$('#dip_timeline').html(response);
								}
							});
							return false;
						});
					});
				</script>
				<h2 class="no-margin-top">JADWAL PELATIHAN </h2>
				<h4 class="right-line">Jadwal Pelatihan Disnaker Kab. Bogor</h4>
				
				<?php
					$per_page 	= 3;
					$start 		= 0;
					$icon 		= array("video"=>"fa-file-video-o","pdf"=>"fa-file-pdf-o","image"=>"fa-file-image-o");
					$iconcolor 	= array("video"=>"icon-ar-warning","pdf"=>"icon-ar-success","image"=>"icon-ar-help");
					
					/*$rs = $this->db->query("SELECT a.dip_id,DATE_FORMAT(a.createon,'%d/%m/%Y') AS createon,a.dip_title,a.dip_foi,a.dip_desc 
					,MONTHNAME(a.createon) AS month_name
					FROM ref_dip a 
					inner join ref_dip_file b on a.dip_id=b.dip_id
					where b.dip_file is not null
					ORDER BY a.createon DESC");
					*/
					$rs = $this->db->query("SELECT a.dip_file,a.id,DATE_FORMAT(a.log,'%d/%m/%Y') AS createon,a.judul as dip_title,a.isi as dip_desc 
					,MONTHNAME(a.log) AS month_name
					FROM ref_pelatihan a 
					inner join ref_dip_file b on a.dip_file=b.dip_file
					where b.dip_file is not null
					ORDER BY a.log DESC");
					
					$totrows = $rs->num_rows();
					$config['base_url'] = base_url().'portal/page/data/dip_timeline';

					$config['total_rows'] 			= $totrows;
					$config['per_page'] 			= $per_page;
					$config['uri_segment'] 			= 5;
					$config['is_ajax_paging']    	= TRUE;

					$this->pagination->initialize($config);
					
					$data['pagination'] = $this->pagination->create_links();
					/*$rs = $this->db->query("SELECT a.dip_id,DATE_FORMAT(a.createon,'%d/%m/%Y') AS createon,a.dip_title,a.dip_foi,a.dip_desc 
					,MONTHNAME(a.createon) AS month_name
					FROM ref_dip a 
					inner join ref_dip_file b on a.dip_id=b.dip_id
					where b.dip_file is not null
					ORDER BY a.createon DESC LIMIT ".$start.",".$per_page."");
					*/
					$rs = $this->db->query("SELECT a.dip_file,a.id,DATE_FORMAT(a.log,'%d/%m/%Y') AS createon,a.judul as dip_title,a.isi as dip_desc 
					,MONTHNAME(a.log) AS month_name
					FROM ref_pelatihan a 
					inner join ref_dip_file b on a.dip_file=b.dip_file
					where b.dip_file is not null
					ORDER BY a.log DESC LIMIT ".$start.",".$per_page."");
					$i = 0;
				?>
				<div class="blog-pagination">
					<div class="pages blogpages">
						<span class="paginate_info pageof">Page <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
						<?=$data['pagination']?>
					</div>
				</div>
				<ul class="timeline-2">
					<?php
					foreach($rs->result() as $item){ 
					$te=explode('.',$item->dip_file);
					?>
					<li class="animated fadeInRight animation-delay-8">
						<time class="timeline-time" datetime=""><?=$item->createon?> <span><?=$item->month_name?></span></time>
						<i class="timeline-2-point"></i>
						<div class="panel panel-default">
							<div class="panel-heading"><?=$item->dip_title?></div>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-4">
										<span class="icon-ar icon-ar-xl-lg icon-ar-square <?=$iconcolor[strtolower($te[1])]?>"><i class="fa fa-suitcase"></i></span>
									</div>
									<div class="col-sm-8">
										<p><?=substr(strip_tags($item->dip_desc,''),0,100)?> ...</p>
										<a href="<?=base_url()?>portal/dip_detail/<?=$item->id?>" class="btn btn-small btn-warning">Lihat</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<?php
					$i++;
					}
					?>
				</ul>
            </div>


       </div>
   </section>

   <!--<section class="margin-bottom">
       <h2 class="section-title">Gallery Pelatihan </h2>
       <div class="bxslider-controls">
            <span id="bx-prev4"></span>
            <span id="bx-next4"></span>
        </div>
        <ul class="bxslider" id="latest-works">
          <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w1.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w1.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Launching Pendaftaran Online </h4>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w2.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w2.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Pelaksanaan Pelatihan</h4>
                    </div>
                </div>
            </div>
          </li>
          <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w3.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w3.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Pelatihan Dinperindag Jateng</h4>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w4.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w4.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Pelatiahan di Hotel Siliwangi</h4>
                    </div>
                </div>
            </div>
          </li>
          <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w5.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w5.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Pelatihan Semarang</h4>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w6.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w6.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Launching Portal Pendaftaran</h4>
                    </div>
                </div>
            </div>
          </li>
          <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w7.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w7.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Pelatihan Hotel Patra Jasa</h4>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w8.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w8.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Pelatihan Tegal</h4>
                    </div>
                </div>
            </div>
          </li>
          <li>
            <div class="img-caption-ar">
                <img src="<?=base_url()?>assets/front/img/demo/w9.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="<?=base_url()?>assets/front/img/demo/w9.jpg" class="animated fadeInDown" data-lightbox="example-set"><i class="fa fa-plus"></i>Zoom</a>
                        <h4 class="caption-title">Pelatihan Hotel Siliwangi</h4>
                    </div>
                </div>
            </div>
        </li>
        </ul>
   </section>-->

   <section>
       <!--<p class="slogan text-center"> <span>Satu Hati Satu Tujuan</span></p>-->
        <h2 class="section-title">Cek Register Certificate Number</h2>
        <div class="row">
            <div class="col-md-6">
			<form name="form-cek" id="form-cek" method="post" action="<?=base_url()?>portal/page/ceksertifikat" enctype="multipart/form-data">
			<div class="form-group">
						<label for="id_pelatihan"> </label>
						<div>
						<input type="text" name="id_pelatihan" id="id_pelatihan" class="form-control" 	/>
						</div>
					</div>
					<input type="submit" id="submit" name="submit" value="CEK SERTIFIKAT" class="btn btn-primary" />
							
			</form>
			
                <br>	
                 <ul class="bxslider" id="home-block">
                   
					<li>
                        <blockquote class="blockquote-color">
                            <p>Cek Keaslian Sertifikat Anda Disini , dengan cara memasukan Nomor registrasi pada form diatas.</p>
                            <footer>Admin Portal Pelatihan</footer>
                        </blockquote>
                    </li>
					
                </ul>
            </div>
			<!--
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-6"><img src="<?=base_url()?>assets/front/img/demo/support_merauke.png" alt="" class="img-responsive"></div>
                    <div class="col-md-4 col-sm-4 col-xs-6"><img src="<?=base_url()?>assets/front/img/demo/support_kemendagri.png" alt="" class="img-responsive"></div>
                    <div class="col-md-4 col-sm-4 col-xs-6"><img src="<?=base_url()?>assets/front/img/demo/support_papua.png" alt="" class="img-responsive"></div>
                </div>	
            </div>-->
        </div>
   </section>

</div>

