<header class="main-header">
    <div class="container">
        <h1 class="page-title">Alur Permohonan Informasi & Keberatan</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="#">Alur Data Center</a></li>
            <li class="active">Alur Permohonan Informasi & Keberatan</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
			<img src="<?=base_url()?>assets/front/img/alur_permohonan_informasi.jpg" class="img-responsive" style="margin:0 auto;border:1px solid #cccccc" />
			</div>
        </div>
    </div> <!-- row -->
</div>