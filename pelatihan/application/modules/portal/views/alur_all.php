<header class="main-header">
    <div class="container">
        <h1 class="page-title">Mekanisme Pelayanan</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Mekanisme Pelayanan</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					  <i class="fa fa-recycle"></i> Mekanisme Dokumentasi Informasi
					</a>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in">
				  <div class="panel-body">
					<img src="<?=base_url()?>assets/front/img/alur_dokumentasi_informasi.jpg" class="img-responsive" style="margin:0 auto;border:1px solid #cccccc" />
				  </div>
				</div>
			  </div>
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
					  <i class="fa fa-recycle"></i> Mekanisme Permohonan Informasi
					</a>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse">
				  <div class="panel-body">
					<img src="<?=base_url()?>assets/front/img/alur_permohonan_informasi.jpg" class="img-responsive" style="margin:0 auto;border:1px solid #cccccc" />
				  </div>
				</div>
			  </div>
			</div>
        </div>
    </div> <!-- row -->
</div>