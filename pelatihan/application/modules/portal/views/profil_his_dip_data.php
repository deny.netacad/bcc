<script>
$(document).ready(function(){

	$(document).on('click','.lmrzknews',function(){
		var $this = $('.lmrzknews');
		var index = $this.index($(this));
		var vdata = $this.eq(index).attr('data').split('|');
		$('.sertifikat').html(' <embed src="'+vdata[1]+'" width="100%" style="min-height:350px" />');
		$('#myModal').modal('show');
	});
	
	$(document).on('click','.lmrzknews2',function(){
		var $this = $('.lmrzknews2');
		var index = $this.index($(this));
		var vdata = $this.eq(index).attr('data').split('|');
		$('.sertifikat2').html(' <embed src="'+vdata[1]+'" width="100%" style="min-height:350px" />');
		$('#myModal2').modal('show');
	});
});
</script>

<?php
	$where = " ";
	$per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= "  and (b.fullname like '%".$cari."%' or c.judul like '%".$cari."%'
	 or c.tempat like '%".$cari."%' or b.username like '%".$cari."%' or b.noid like '%".$cari."%')";
	
	$n = intval($this->uri->segment(5));
	$q = "select *,a.is_posting as is_posting,a.id_applicant as id ,a.id_pelatihan as id_pelatihan,date_format(c.tgl_mulai,'%d %M %Y') as tgl_mulai,c.`dip_file` AS dip_file from ref_pendaftaran a 
	left join ref_applicant b on a.id_applicant=b.id
	left join ref_pelatihan c on a.id_pelatihan=c.id
	";
	$rs = $this->db->query("$q where a.id_applicant='".$this->session->userdata('id_peserta')."' $where  ");
	$row = $rs->row_array();
	$totrows = $rs->num_rows();
	$config['base_url'] = base_url().'portal/page/data/profil_his_dip';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("
	$q where a.id_applicant='".$this->session->userdata('id_peserta')."' $where
	ORDER BY a.log DESC limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
	
?>
<div class="blog-pagination">
	<div class="pages blogpages">
		<span class="paginate_info pageof">Page <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
		<?=$data['pagination']?>
	</div>
</div>
<table class="table table-hover">
  <thead>
	  <tr>
		<th>#</th>
		<th>Nama Pelatihan</th>
		<!--<th>Tanggal</th>-->
		<th>Tanggal Pelaksanaan</th>
		<th>Lokasi Pelaksanaan</th>
		
		
		<th  align="center">Dokumen Pelatihan</th>
		<th colspan="2" align="center">Sertifikat Pelatihan</th>
	  </tr>
  </thead>
  <tbody>
  <?php
  $n = 0;
  foreach($data['posts']->result() as $item){
  $n++;
  ?>
	  <tr>
		<td><?=$n?></td>
		<td><?=$item->judul?></td>
		
		<td><?=$item->tgl_mulai?></td>
		<td><?=$item->tempat?></td>
		
		<td align=center>
		<!--<a href="<?=base_url()?>portal/page/downloadfile/<?=$item->id?>" class="download" title="Download Dokumen" data="<?=$item->dip_id?>">
			<i class="fa fa-download"></i>
		</a>
		      
		<a href="#" class="feedback" data="<?=$item->id?>" title="Beri Feedback untuk dokumen ini">
			<i class="fa fa-reply"></i>
		</a>-->
		<a href="#" data="pdf|<?=base_url()?>upliddir/<?=$item->dip_file?>" class="lmrzknews ">file Pelatihan</a>
		</td>
		<td align=center>
		<?php if($item->is_posting==1){?>
        <a href="#" data="pdf|<?=base_url()?>portal/page/pdf/sertifikat?&o=L&id=<?=$item->id?>&p=<?=$item->id_pelatihan?>" class="lmrzknews2"> <i class="fa fa-download"></i> sertifikat</a>
        <?php }?>
		</td>
	  </tr>
	<?php
	}
	?>
  </tbody>
</table>   


<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">File Dokumen Pelatihan</h4>
      </div>
      <div class="modal-body sertifikat">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="myModal2" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Sertifikat Digital</h4>
      </div>
      <div class="modal-body sertifikat2">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->