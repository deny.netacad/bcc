<header class="main-header">
    <div class="container">
        <h1 class="page-title">Statistik Pelayanan Data</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Statistik Pelayanan Data</li>
        </ol>
    </div>
</header>

<div class="container" id="container">
    <div class="row">
        <div class="col-md-12">
			
			<!-- Nav tabs -->
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#dip1" data-toggle="tab"><i class="fa fa-bar-chart-o"></i> Data Informasi Pendaftar</a></li>
				<li class=""><a href="#dip2" data-toggle="tab"><i class="fa fa-bar-chart-o"></i> Statistik Pendaftar Kegiatan</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="dip1">
					<div class="panel panel-primary">
						<div class="panel-heading">Cari Informasi Pendaftar</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="idskpd" placeholder="Pilih Pendaftaran" />
								<button type="submit" class="btn btn-ar btn-primary">Lihat Statistik</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="graph1">
						<div class="row">
							<div class="col-sm-6 col-md-4">
								<div class="text-center">
									<div class="circle" id="circles-9"><div class="circles-wrp" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="120" height="120"><path fill="transparent" stroke="#eee" stroke-width="10" d="M 59.988797973796764 5.000001140776291 A 55 55 0 1 1 59.923606103406065 5.000053054820469 Z"></path><path fill="transparent" stroke="#54c8eb" stroke-width="10" d="M 59.988797973796764 5.000001140776291 A 55 55 0 1 1 42.93975410934479 7.712831304894863 "></path></svg><div class="circles-text" style="position: absolute; top: 0px; left: 0px; text-align: center; width: 100%; font-size: 42px; height: 120px; line-height: 120px;">95%</div></div></div>
									<h4 class="text-center">Diproses</h4>
									<p class="small-font">Data permohonan diproses</p>
								</div>
							</div>
							<div class="col-sm-6 col-md-4">
								<div class="text-center">
									<div class="circle" id="circles-10"><div class="circles-wrp" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="120" height="120"><path fill="transparent" stroke="#eee" stroke-width="10" d="M 59.988797973796764 5.000001140776291 A 55 55 0 1 1 59.923606103406065 5.000053054820469 Z"></path><path fill="transparent" stroke="#ac60d0" stroke-width="10" d="M 59.988797973796764 5.000001140776291 A 55 55 0 0 1 83.46628943615865 109.74267041583542 "></path></svg><div class="circles-text" style="position: absolute; top: 0px; left: 0px; text-align: center; width: 100%; font-size: 42px; height: 120px; line-height: 120px;">43%</div></div></div>
									<h4 class="text-center">Ditolak</h4>
									<p class="small-font">Data Permohonan ditolak</p>
								</div>
							</div>
							<div class="col-sm-6 col-md-4">
								<div class="text-center">
									<div class="circle" id="circles-11"><div class="circles-wrp" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="120" height="120"><path fill="transparent" stroke="#eee" stroke-width="10" d="M 59.988797973796764 5.000001140776291 A 55 55 0 1 1 59.923606103406065 5.000053054820469 Z"></path><path fill="transparent" stroke="#d9534f" stroke-width="10" d="M 59.988797973796764 5.000001140776291 A 55 55 0 1 1 27.721437831187735 104.53194835524403 "></path></svg><div class="circles-text" style="position: absolute; top: 0px; left: 0px; text-align: center; width: 100%; font-size: 42px; height: 120px; line-height: 120px;">60%</div></div></div>
									<h4 class="text-center">Selesai</h4>
									<p class="small-font">Data permohonan selesai</p>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="tab-pane" id="dip2">
					<div class="panel panel-primary">
						<div class="panel-heading">Filter SKPD</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="idskpd" placeholder="Pilih SKPD" />
								<button type="submit" class="btn btn-ar btn-primary">Lihat Statistik</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="graph2"></div>
				</div>
			</div>
        </div>
    </div> <!-- row -->
</div>