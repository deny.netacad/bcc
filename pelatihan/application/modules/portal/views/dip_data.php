<script>
$(document).ready(function(){
	
	refreshData();
	
	$('#myTab li a').on('click',function(e){
		switch($(this).attr('href')){
			case "#tab-sample1":
				refreshData();
			break;
		}
	});
	
	$('#per_page').change(function(){
		refreshData();
	});
	
	$('#cari').keyup( $.debounce(250,refreshData));
	
	$(document).on('click','#tab-sample1 a.ajax-replace', function(){
		$.ajax({
			type:'post',
			url:$(this).attr('href'),
			data:{ 'per_page':$('#tab-sample1 #per_page').val(),'cari':$('#tab-sample1 #cari').val() },
			beforeSend:function(){
				
			},
			success:function(response){
				$('#tab-sample1 #result').html(response);
			}
		});
		return false;
	});
	
	
	
});

function refreshData(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/dip_data',
		data:{ 'per_page':$('#tab-sample1 #per_page').val(),'cari':$('#tab-sample1 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#tab-sample1 #result').html(response);
		}
	});
}
</script>
<header class="main-header">
    <div class="container">
        <h1 class="page-title">Data Informasi Publik</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Data Informasi Publik</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
			
			<!-- Nav tabs -->
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#dip1" data-toggle="tab"><i class="glyphicon glyphicon-list-alt"></i> Daftar Informasi Publik</a></li>
			</ul>
			<div class="tab-content" id="tab-sample1">
				<div class="tab-pane active" id="dip1">
					<div class="panel panel-primary">
						<div class="panel-heading">Filter Pencarian Data</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="cari" name="cari" placeholder="Cari Judul/Kategori/Ringkasan/Kode Dokumen/Penanggung Jawab">
								<select class="form-control" id="per_page" name="per_page">
									<option>10</option>
									<option>20</option>
									<option>30</option>
									<option>40</option>
									<option>50</option>
								</select>
								<button type="submit" class="btn btn-ar btn-primary">Cari</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="result">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Judul</th>
									<!--<th>Tanggal</th>-->
									<th>Kategori</th>
									<th>Ringkasan</th>
									<th>Penanggungjawab<br>Penerbitan</th>
									<th>Kode Dok.</th>
									<th>Tipe</th>
									<th>Jenis Informasi</th>
									<th>Retensi</th>
									<th>Ukuran File</th>
									<th colspan="2" align="center">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Materi Workshop DIP</td>
									<td>Umum</td>
									<td>
									Materi Workshop mengenai pembuatan daftar informasi publik (DIP)
									</td>
									<td>Data Center Kab.Merauke</td>
									<td>a02</td>
									<td>Pdf</td>
									<td>Setiap Saat</td>
									<td>2014-11-27</td>
									<td></td>
									<td>
										<a href="#" class="downloadfile" title="Download Dokumen" data="3">
											<i class="fa fa-download"></i>
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div> <!-- row -->
</div>