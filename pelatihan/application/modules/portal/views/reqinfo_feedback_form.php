<script>

	$("#form2").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form2').trigger('reset');
			});		
		}else{
			
			$('#form2').trigger('reset');
		}
	});

	$('#form2').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});

</script>
<div id="contact-form-area">
	<!-- Contact Form Start //-->
	<?php
	if($this->session->userdata('is_login_pelatihanuser')){
		$data['username'] = $this->session->userdata('username');
	}else{
		$data['username'] = '';
		redirect('portal/masuk');
	}
	
	$rs = $this->db->query("select a.*,b.req_title,b.req_alasan,b.req_info,b.req_type from ref_applicant a 
	inner join req_info b on a.username=b.username
	where a.username='".$data['username']."'
	and b.req_id='".$this->input->post('req_id')."'
	
	");
	$item = $rs->row();
	$job = array("","Wiraswasta","Buruh","Petani","Nelayan","PNS","Swasta","Pedagang","Pekerja Rumah Tangga","LSM","Tenaga Pendidik","Pengacara","Tenaga Kesehatan"
	,"Peneliti","Seniman","Wartawan","Mahasiswa/Pelajar","Lain-lain");
	?>
	<form id="form2" name="form2" action="<?=base_url()?>portal/saveFeedback" class="form-horizontal" method="post" enctype="multipart/form-data">
		<input type="hidden" name="req_id" id="req_id" value="<?=$this->input->post('req_id')?>" />                        
		<fieldset> 
		<label>Nama Pemohon <em>*</em></label> 
		<input type="text" name="fullname" class="form-control" id="fullname" value="<?=$item->fullname?>" readonly />                        
		<label>Pekerjaan <em>*</em></label> 
		<input type="text" name="job" class="form-control" id="job" value="<?=$job[$item->job]?>" readonly />                        
		<label>Alamat <em>*</em></label>
		<textarea name="addr" id="addr" class="form-control" cols="2" rows="2"><?=$item->addr?></textarea>
		<div class="clear"></div> 
		<label>&nbsp;</label>
		<label>Kota <em>*</em></label> 
		<input type="text" name="city" class="form-control" id="city" value="<?=$item->city?>">                        
		<label>Kode POS <em>*</em></label> 
		<input type="text" name="postcode" class="form-control" id="postcode" value="<?=$item->postcode?>">                        
		<label>Email <em>*</em></label> 
		<input type="email" name="email" class="form-control" id="email" value="<?=$item->email?>">                        
		<label>Telp. Rumah <em>*</em></label> 
		<input type="text" name="phone_home" class="form-control" id="phone_home" value="<?=$item->phone_home?>">                        
		<label>Telp. Mobile <em>*</em></label> 
		<input type="text" name="phone_mobile" class="form-control" id="phone_mobile" value="<?=$item->phone_mobile?>">                        
		<label>Judul Permintaan <em>*</em></label>
		<input type="text" name="req_title" class="form-control" id="req_title" value="<?=$item->req_title?>">                        
		<label>Isi Dokumen <em>*</em></label>
		<textarea name="req_info" id="req_info" class="form-control" cols="2" rows="4"><?=$item->req_info?></textarea>
		<label>Keterangan Feedback<em>*</em></label>
		<textarea name="req_feedback" id="req_feedback" class="form-control" cols="2" rows="4"></textarea>
		<div class="clear"></div> 

		<div class="clear"></div> 
		<label>&nbsp;</label>
		<button type="submit" name="submit" class="buttoncontact" id="buttonsend">Submit</button>
		<span class="loading" style="display: none;">Please wait..</span>
		<div class="clear"></div>
		</fieldset> 
	</form>
	<!-- Contact Form End //-->
</div>                            
