<script>
	$(document).ready(function(){
		$(document).on('click','a.download',function(){
			$.ajax({
				type:'post',
				url: $(this).attr('href'),
				data:{  },
				beforeSend:function(){
					
				},
				success:function(response){
					var ret = $.parseJSON(response);
					if(ret.error==0){
						return true;
					}else{
						$.growl.warning({ title: "Notification", message: ret.text,location:'tc',size:'large',duration:2000 });	
						if(ret.error==2){
							window.location='<?=base_url()?>portal/masuk';
						}
						return false;
					}
					
				}
			});
		});
		
	});
</script>
<header class="main-header">
    <div class="container">
        <h1 class="page-title">Data Detail Informasi Pelatihan</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Data Detail</li>
        </ol>
    </div>
</header>

<div class="container" id="container">
    <div class="row">
	 <div class="col-md-12">
            <div class="title-logo animated fadeInDown animation-delay-5">Portal <span>PELATIHAN DISNAKER KAB. BOGOR</span></div>
        </div>
        <div class="col-md-7">
			
			<!-- Nav tabs -->
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#dip1" data-toggle="tab"><i class="fa fa-file-pdf-o"></i> Data Detail Pelatihan</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="dip1">
					<?php
					$rs = $this->db->query("SELECT a.tempat,a.tgl_mulai,a.id_user,a.id,DATE_FORMAT(a.log,'%d/%m/%Y') AS createon,DATE_FORMAT(a.tgl_mulai,'%d/%m/%Y') AS mulai,DATE_FORMAT(a.tgl_ahir,'%d/%m/%Y') AS ahir,a.judul as 					dip_title,a.isi as dip_desc ,a.dip_file
											,MONTHNAME(a.log) AS month_name,b.dip_file,b.dip_path
											FROM ref_pelatihan a 
											left join ref_dip_file b on a.dip_file=b.dip_file
					where a.id='".$this->uri->segment(3)."'");
					$i = 0;
					$item = $rs->row();
					$temp=explode('.',$item->dip_file);
					?>
					<h2 class="center"><?=$item->dip_title?></h2>
					<h4 class="right-line"><?=$item->createon?> by <?=$item->id_user?></h4>
					
					
					<p style="text-align:justify">
					<?php 
					echo $item->dip_desc;
					?>
					</p>
					<h4 >Dimulai Tanggal    :<?=$item->mulai?> s.d <?=$item->ahir?></h3>
					<h4 >Lokasi Pelaksanaan  :<?=$item->tempat?></h3>
					<?php
					$re=$this->db->query("select * from ref_pendaftaran where id_applicant='".$this->session->userdata('id_peserta')."' and id_pelatihan='".$item->id."'");
					$jml=$re->num_rows();
					
					if( date('Y-m-d')>=$item->tgl_mulai and $jml<1){
					?>
					<a href="<?=base_url()?>portal/page/daftarpelatihan/<?=$item->id?>" class="download btn btn-sm btn-warning" style="width:100%">Klik Disini, Daftar Pelatihan Sekarang</a>
					
					<?php
					}else if($jml==1){ ?>
						
							<p><a><span class="icon-ar-inverse"><i class="fa fa-check"></i></span></a> Selamat <?=$this->session->userdata('fullname')?>, Anda Telah Terdaftar Sebagai Peserta</p>
				
					<?php }
					
					else{
					?>
					
					<p><a><span class="icon-ar-inverse"><i class="fa fa-chain-broken"></i></span></a> Maaf, Pendaftaran Telah ditutup oleh sistem Kami</p>
					<?php
					}?>
					<?php
					/*if($temp[1]=='jpg'){
						$temp=  str_replace('D:/html/disperindag/', base_url() ,$item->dip_path);
						//echo $item->dip_file;
						$temp .="".$item->dip_file."";
						echo "<img src=".$temp." width='40%' height=40%>";
					}*/
					if($rs->num_rows() > 0 && $item->dip_file != ""){
					
					?>
						<br><br><embed src="<?=base_url()?>upliddir/<?=$item->dip_file?>" width="100%"  />

					<?php } ?>
				</div>
				
			</div>
        </div>
		
		<div class="col-md-5">
            <h2 class="right-line">Sekilas Pendaftaran Pelatihan</h2>
            <div class="panel-group" id="accordion">
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					  <i class="fa fa-user">  Akun</i> 
					</a>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in">
				  <div class="panel-body">
					<p>Yang harus dilakukan pertama kali oleh calon peserta pelatihan adalah membuat akun,akun ini berfungsi sebagai syarat untuk pengajuan pendaftaran pelatihan.</p>
				  </div>
				</div>
			  </div>
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					  <i class="fa fa-desktop">  Login</i> 
					</a>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse">
				  <div class="panel-body">
					<p>Peserta yang sudah membuat akun selanjutnya melakukan login dengan username dan password yang sudah dibuat sebelumnya.</p>
				  </div>
				</div>
			  </div>
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
					  <i class="fa fa-lightbulb-o"></i> Keuntungan Pendaftaran Online
					</a>
				</div>
				<div id="collapseThree" class="panel-collapse collapse">
				  <div class="panel-body">
					<p>1. Pendaftaran menjadi fleksibel dan cepat. </p>
					<p>2. Mudah dan bisa diakses melalui laptop / smartphone anda. </p>
				  </div>
				</div>
			  </div>
			</div>
			
			 <div class="" id="dip_timeline">
				<script>
					$(function(){
						$(document).on('click','#dip_timeline a.ajax-replace', function(){
							$.ajax({
								type:'post',
								url:$(this).attr('href'),
								data:{},
								beforeSend:function(){
								},
								success:function(response){
									$('#dip_timeline').html(response);
								}
							});
							return false;
						});
					});
				</script>
				<h2 class="no-margin-top">JADWAL PELATIHAN LAINNYA </h2>
			<?php
					$per_page 	= 2;
					$start 		= 0;
					$icon 		= array("video"=>"fa-file-video-o","pdf"=>"fa-file-pdf-o","image"=>"fa-file-image-o");
					$iconcolor 	= array("video"=>"icon-ar-warning","pdf"=>"icon-ar-success","image"=>"icon-ar-help");
					
					/*$rs = $this->db->query("SELECT a.dip_id,DATE_FORMAT(a.createon,'%d/%m/%Y') AS createon,a.dip_title,a.dip_foi,a.dip_desc 
					,MONTHNAME(a.createon) AS month_name
					FROM ref_dip a 
					inner join ref_dip_file b on a.dip_id=b.dip_id
					where b.dip_file is not null
					ORDER BY a.createon DESC");
					*/
					$rs = $this->db->query("SELECT a.dip_file,a.id,DATE_FORMAT(a.log,'%d/%m/%Y') AS createon,a.judul as dip_title,a.isi as dip_desc 
					,MONTHNAME(a.log) AS month_name
					FROM ref_pelatihan a 
					inner join ref_dip_file b on a.dip_file=b.dip_file
					where b.dip_file is not null
					ORDER BY a.log DESC");
					
					$totrows = $rs->num_rows();
					$config['base_url'] = base_url().'portal/page/data/dip_timeline';

					$config['total_rows'] 			= $totrows;
					$config['per_page'] 			= $per_page;
					$config['uri_segment'] 			= 5;
					$config['is_ajax_paging']    	= TRUE;

					$this->pagination->initialize($config);
					
					$data['pagination'] = $this->pagination->create_links();
					/*$rs = $this->db->query("SELECT a.dip_id,DATE_FORMAT(a.createon,'%d/%m/%Y') AS createon,a.dip_title,a.dip_foi,a.dip_desc 
					,MONTHNAME(a.createon) AS month_name
					FROM ref_dip a 
					inner join ref_dip_file b on a.dip_id=b.dip_id
					where b.dip_file is not null
					ORDER BY a.createon DESC LIMIT ".$start.",".$per_page."");
					*/
					$rs = $this->db->query("SELECT a.dip_file,a.id,DATE_FORMAT(a.log,'%d/%m/%Y') AS createon,a.judul as dip_title,a.isi as dip_desc 
					,MONTHNAME(a.log) AS month_name
					FROM ref_pelatihan a 
					inner join ref_dip_file b on a.dip_file=b.dip_file
					where b.dip_file is not null
					ORDER BY a.log DESC LIMIT ".$start.",".$per_page."");
					$i = 0;
				?>
				<div class="blog-pagination">
					<div class="pages blogpages">
						<span class="paginate_info pageof">Page <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
						<?=$data['pagination']?>
					</div>
				</div>
				<ul class="timeline-2">
					<?php
					foreach($rs->result() as $item){ 
					$te=explode('.',$item->dip_file);
					?>
					<li class="animated fadeInRight animation-delay-8">
						<time class="timeline-time" datetime=""><?=$item->createon?> <span><?=$item->month_name?></span></time>
						<i class="timeline-2-point"></i>
						<div class="panel panel-default">
							<div class="panel-heading"><?=$item->dip_title?></div>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-4">
										<span class="icon-ar icon-ar-xl-lg icon-ar-square <?=$iconcolor[strtolower($te[1])]?>"><i class="fa <?=$icon[strtolower($te[1])]?>"></i></span>
									</div>
									<div class="col-sm-8">
										<p><?=substr(strip_tags($item->dip_desc,''),0,100)?> ...</p>
										<a href="<?=base_url()?>portal/dip_detail/<?=$item->id?>" class="btn btn-small btn-warning">Lihat</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<?php
					$i++;
					}
					?>
				</ul>
            </div>
        </div>
    </div> <!-- row -->
	
	
           


       </div>
