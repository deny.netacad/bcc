<link rel="stylesheet" href="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
<script src="<?=base_url()?>assets/front/js/plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>
<script>
$(document).ready(function(){
	 $('#form1 #scanfile').fileupload({
		dataType: 'json',
		formData: { 'filename':'<?=date('YmdHis')?>' },
		done: function (e, data) {
			
			try{
			var vfilepath = data.result.upload_data.full_path;
			var vfilename = data.result.upload_data.file_name;
			var vhtml = '<div class="form-group">'
						+'<label for="scan_file" class="col-sm-2 control-label">&nbsp;</label>'
						+'<div class="col-sm-2 col-md-3 col-lg-2">'
						+'<input type="hidden" name="scanfile[]" value="'+vfilename+'" />'+vfilename+' '
						+'<a class="delthis" data="'+vfilepath+'" href="#"><span><i class="fa fa-remove"></i></span></a></div></div>';
			$.growl.warning({ title: "Notification", message: data.result.upload_data.file_name +' upload success' ,location:'tc',size:'large',duration:3000 });				
			$('#form1 #progress .bar').after(vhtml);
			$('#form1 #progress .bar').html('');
			$('#form1 #progress .bar').css('width','0%');
			}catch(e){ alert(e.message); }
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#form1 #progress .bar').css(
				'width',
				progress + '%'
			);
			
		}
	});
	
	$(document).on('click','a.delthis',function(){
		if(confirm('Hapus file?')){
			var $this = $('a.delthis');
			var index = $this.index($(this));
			
			var vdata = $this.eq(index).attr('data');
			$.ajax({
				type:'post',
				url:'<?=base_url()?>portal/delUpload',
				data:{ 'full_path': vdata },
				beforeSend:function(){
					
				},
				success:function(response){
					var ret = $.parseJSON(response);
					$.growl.warning({ title: "Notification", message: ret.text,location:'tc',size:'large',duration:3000 });	
					$this.eq(index).parent().parent().remove();
				}
			});
		}
	});
	
	$("#form1").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				$.growl.warning({ title: "Notification", message: ret.text,location:'tc',size:'large',duration:3000 });	
				$('#form1').trigger('reset');
			});		
		}else{
			
			$('#form1').trigger('reset');
		}
	});
	
	$('#form1').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var target = $(e.target).attr("href");
		switch(target){
			case "#dip2":
				refreshData2();
			break;
			case "#dip3":
				refreshData3();
			break;
			case "#dip4":
				refreshData4();
			break;
		}
	});
	
	$(document).on('click','#dip2 a.ajax-replace', function(){
		$.ajax({
			type:'post',
			url:$(this).attr('href'),
			data:{ 'per_page':$('#dip2 #per_page').val(),'cari':$('#dip2 #cari').val() },
			beforeSend:function(){
				
			},
			success:function(response){
				$('#dip2 #result2').html(response);
			}
		});
		return false;
	});
	
	$(document).on('click','a.download',function(){
		$.ajax({
			type:'post',
			url: $(this).attr('href'),
			data:{  },
			beforeSend:function(){
				
			},
			success:function(response){
				var ret = $.parseJSON(response);
				if(ret.error==0){
					return true;
				}else{
					$.growl.warning({ title: "Notification", message: ret.text,location:'tc',size:'large',duration:2000 });	
					if(ret.error==2){
						window.location='<?=base_url()?>portal/masuk';
					}
					return false;
				}
				
			}
		});
	});
	
	$(document).on('click','a.feedback',function(){
		var $this = $('a.feedback');
		var index = $this.index($(this));
		$.ajax({
			url:'<?=base_url()?>portal/page/view/feedback',
			type:'post',
			data:{ 'dip_id': $this.eq(index).attr('data') },
			beforeSend:function(){},
			success:function(response){
				$('#div-content').html(response);
				$('#myModal3').modal('show',function(){
					keyboard:false
				});
				
				return false;
			}
		});	

	});
	
	$(document).on('submit','#form-feedback',function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		$.post(url, $form.serialize(),
		function(data) {
			window.location = "#";
			var ret = $.parseJSON(data);
			$.growl.warning({ title: "Notification", message: ret.text,location:'tc',size:'large',duration:3000 });	
			$('button[data-dismiss="modal"]').trigger('click');
		});		
		
	});
	
});

function refreshData2(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/profil_his_dip',
		data:{ 'per_page':$('#dip2 #per_page').val(),'cari':$('#dip2 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result2').html(response);
		}
	});
}

function refreshData3(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/profil_req_info',
		data:{ 'per_page':$('#dip3 #per_page').val(),'cari':$('#dip3 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result3').html(response);
		}
	});
	
	
}

function refreshData4(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/profil_keberatan',
		data:{ 'per_page':$('#dip4 #per_page').val(),'cari':$('#dip4 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result4').html(response);
		}
	});
}
</script>
<?php
if($this->session->userdata('is_login_pelatihanuser')){
	$data['username'] = $this->session->userdata('username');
}else{
	$data['username'] = '';
	redirect('portal/masuk');
}
$rs = $this->db->get_where("ref_applicant",$data);
$item = $rs->row();
?>
<header class="main-header">
    <div class="container">
        <h1 class="page-title">PROFIL <?=$this->session->userdata('username')?></h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active"><?=$this->session->userdata('username')?></li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
				<ul class="nav nav-tabs nav-tabs-ar">
				
				<li class=""><a href="dip1" data-toggle="tab"><i class="fa fa-building"></i> JENIS KEGIATAN</a></li>
			</ul>
			<div class="tab-content">
				<!--<div class="tab-pane active" id="#">
					<div class="panel panel-primary">
						<div class="panel-heading">PILIH JENIS KEGIATAN YANG DIADAKAN</div>
						<div class="panel-body">
							<form id="form1" name="form1" action="<?=base_url()?>portal/updateRegister" class="form-horizontal" method="post" enctype="multipart/form-data">	  
							  <div class="form-group">
								<label for="nama_kegiatan" class="col-sm-2 control-label">Nama Kegiatan</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="nama_kegiatan" name="nama_kegiatan" placeholder="Nama Kegiatan">
								</div>
							  </div>
							   <div class="form-group">
								<label for="tempat_kegiatan" class="col-sm-2 control-label">Tempat Kegiatan</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="tempat_kegiatan" name="tempat_kegiatan" placeholder="Tempat Kegiatan">
								</div>
							  </div>
							   <div class="form-group">
								<label for="tgl_kegiatan" class="col-sm-2 control-label">Tanggal Kegiatan</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="tgl_kegiatan" name="tgl_kegiatan" placeholder="yyyy-mm-dd">
								</div>
							  </div>
							  
							</form>
						</div>
					</div>
				</div>-->
			<!-- Nav tabs -->
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#dip1" data-toggle="tab"><i class="fa fa-user"></i> Data Anda</a></li>
				<li class=""><a href="#dip2" data-toggle="tab"><i class="fa fa-list-alt"></i> Riwayat Pelatihan</a></li>
				<!--<li class=""><a href="#dip3" data-toggle="tab"><i class="fa fa-list-alt"></i> Sertifikat </a></li>
				<li class=""><a href="#dip4" data-toggle="tab"><i class="fa fa-list-alt"></i> Jadwal Pelatihan</a></li>-->
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="dip1">
					<div class="panel panel-primary">
						<div class="panel-heading">Data <?=$this->session->userdata('username')?></div>
						
						<div class="panel-body">
						<h3>DATA PERSONIL</h3>
							<form id="form1" name="form1" action="<?=base_url()?>portal/updateRegister" class="form-horizontal" method="post" enctype="multipart/form-data">
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Jenis Pengenal</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="identity" id="identity" class="form-control" required="">
										<option value="1" <?=(($item->identity==1)?"selected":"")?>>KTP</option>
										<option value="2" <?=(($item->identity==2)?"selected":"")?>>SIM</option>
										<option value="3" <?=(($item->identity==3)?"selected":"")?>>PASSPORT</option>
										<option value="4" <?=(($item->identity==4)?"selected":"")?>>KTM</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">No. Identitas</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="noid" value="<?=$item->noid?>" placeholder="No. Identitas Sesuai Jenis Pengenal" required>
								</div>
							  </div>
							  <!--<div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Scan File Identitas</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<span class="btn btn-success fileinput-button" style="float:left;cursor:pointer">
										<span style="cursor:pointer"><i class="fa fa-image"></i> Add files...</span>
										<input type="file" name="scanfile" class="textfield" id="scanfile" value="" data-url="portal/do_upload_scanfile" />
									</span>
								</div>
							  </div>-->
							  <div id="progress">
									<div class="bar textfield" style="width: 0%;color:orange"></div>
							  </div>
							  <?php
							  $path = BASEPATH."../upliddir";
							  $filescan = $path.'/'.$item->scanfile;
							  if($item->scanfile!='' && file_exists($filescan)){
							  ?>
							  <div class="form-group">
								<label for="scan_file" class="col-sm-2 control-label">&nbsp;</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<?php
									echo '<input type="hidden" name="scanfile[]" value="'.$item->scanfile.'" />'
										.'<a href="'.base_url().'upliddir/'.$item->scanfile.'" data-lightbox="'.$item->scanfile.'" target="_blank">'.$item->scanfile.'</a>&nbsp;'
										.'<a class="delthis" data="'.$path.'/'.$item->scanfile.'" href="#"><span><i class="fa fa-remove"></i></span></a><br/>';
									?>
								</div>
							  </div>
							  <?php
							  }
							  ?>
							  <div class="form-group">
								<label for="username" class="col-sm-2 control-label">Username</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="username" placeholder="Username" value="<?=$item->username?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="fullname" class="col-sm-2 control-label">Nama Lengkap *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="fullname" placeholder="Nama Lengkap" value="<?=$item->fullname?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="noid" class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="sex" id="sex" class="form-control" required>
										<option value="1" <?=(($item->sex==1)?"selected":"")?>>Laki-Laki</option>
										<option value="2" <?=(($item->sex==2)?"selected":"")?>>Perempuan</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="pob" class="col-sm-2 control-label">Tempat Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="pob" name="pob" placeholder="Tempat Lahir" value="<?=$item->pob?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="dob" class="col-sm-2 control-label">Tanggal Lahir *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control date" id="dob" placeholder="dd-mm-YYYY"  value="<?=$item->dob?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="education" class="col-sm-2 control-label">Pendidikan Terakhir *</label>
								<div class="col-sm-2 col-md-3 col-lg-2">
									<select name="education" id="education" class="form-control" required>
										<option value="1" <?=(($item->education==1)?"selected":"")?>>SD</option>
										<option value="2" <?=(($item->education==2)?"selected":"")?>>SMP</option>
										<option value="3" <?=(($item->education==3)?"selected":"")?>>SMA</option>
										<option value="4" <?=(($item->education==4)?"selected":"")?>>Diploma</option>
										<option value="5" <?=(($item->education==5)?"selected":"")?>>Sarjana</option>
										<option value="6" <?=(($item->education==6)?"selected":"")?>>Pasca Sarjana</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="job" class="col-sm-2 control-label">Pekerjaan *</label>
								<div class="col-sm-2 col-md-4 col-lg-3">
									<select name="job" id="job" class="form-control" required>
										<?php
										$jobarr = array("","Wiraswasta","Buruh","Petani","Nelayan","PNS","Swasta","Pedagang","Pekerja Rumah Tangga",
										"LSM","Tenaga Pendidik","Pengacara","Tenaga Kesehatan","Peneliti","Seniman","Wartawan","Mahasiswa/Pelajar"
										,"Lain-lain");
										?>
										<option value="1" <?=(($item->job==1)?"selected":"")?>>Wiraswasta</option>
										<option value="2" <?=(($item->job==2)?"selected":"")?>>Buruh</option>
										<option value="3" <?=(($item->job==3)?"selected":"")?>>Petani</option>
										<option value="4" <?=(($item->job==4)?"selected":"")?>>Nelayan</option>
										<option value="5" <?=(($item->job==5)?"selected":"")?>>PNS</option>
										<option value="6" <?=(($item->job==6)?"selected":"")?>>Swasta</option>
										<option value="7" <?=(($item->job==7)?"selected":"")?>>Pedagang</option>
										<option value="8" <?=(($item->job==8)?"selected":"")?>>Pekerja rumah tangga</option>
										<option value="9" <?=(($item->job==9)?"selected":"")?>>LSM</option>
										<option value="10" <?=(($item->job==10)?"selected":"")?>>Tenaga Pendidik</option>
										<option value="11" <?=(($item->job==11)?"selected":"")?>>Pengacara</option>
										<option value="12" <?=(($item->job==12)?"selected":"")?>>Tenaga Kesehatan</option>
										<option value="13" <?=(($item->job==13)?"selected":"")?>>Peneliti</option>
										<option value="14" <?=(($item->job==14)?"selected":"")?>>Seniman</option>
										<option value="15" <?=(($item->job==15)?"selected":"")?>>Wartawan</option>
										<option value="16" <?=(($item->job==16)?"selected":"")?>>Mahasiswa/Pelajar</option>
										<option value="17" <?=(($item->job==17)?"selected":"")?>>Lain-lain</option>
									</select>
								</div>
							  </div>
							  <div class="form-group">
								<label for="addr" class="col-sm-2 control-label">Alamat *</label>
								<div class="col-sm-10 col-lg-4">
								  <textarea class="form-control" id="addr" name="addr" placeholder="Alamat" required><?=$item->addr?></textarea>
								</div>
							  </div>
							  <div class="form-group">
								<label for="city" class="col-sm-2 control-label">Kabupaten / Kota *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="city" name="city" placeholder="Kota" value="<?=$item->city?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="postcode" class="col-sm-2 control-label">Kode Pos *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="postcode" name="postcode" placeholder="5 digit" value="<?=$item->postcode?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?=$item->email?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_home" class="col-sm-2 control-label">Telp. Rumah *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_home" name="phone_home" placeholder="No. Telp Rumah" value="<?=$item->phone_home?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="phone_mobile" class="col-sm-2 control-label">Telp. Mobile *</label>
								<div class="col-sm-10 col-md-4 col-lg-3">
								  <input type="text" class="form-control" id="phone_mobile" name="phone_mobile" placeholder="No. HP" value="<?=$item->phone_mobile?>" required>
								</div>
							  </div>
							  <h3>DATA PERUSAHAAN</h3>
							  <div class="form-group">
								<label for="nama_perusahaan" class="col-sm-2 control-label">Nama Perusahaan *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" value="<?=$item->nama_perusahaan?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<label for="alamat_perusahaan" class="col-sm-2 control-label">Alamat Perusahaan*</label>
								<div class="col-sm-10 col-lg-4">
								  <textarea class="form-control" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Alamat Perusahaan" required><?=$item->alamat_perusahaan?></textarea>
								</div>
							  </div>
							  <div class="form-group">
								<label for="city_perusahaan" class="col-sm-2 control-label">Kabupaten / Kota *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="city_perusahaan" name="city_perusahaan" placeholder="Kabupaten / Kota" value="<?=$item->city_perusahaan?>" required>
								</div>
								</div>
								<div class="form-group">
								<label for="jabatan" class="col-sm-2 control-label">Jabatan Perusahaan *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan Perusahaan" value="<?=$item->jabatan?>" required>
								</div>
							  </div>
							 
							  <div class="form-group">
								<label for="skl_perusahaan" class="col-sm-2 control-label">Skala Perusahaan *</label>
								<div class="col-sm-10 col-lg-4">
								  <input type="text" class="form-control" id="skl_perusahaan" name="skl_perusahaan" placeholder="Skala Perusahaan" value="<?=$item->skl_perusahaan?>" required>
								</div>
							  </div>
							  <div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
								  <button type="submit" class="btn btn-ar btn-primary">Simpan</button>
								</div>
							  </div>
							</form>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="dip2">
					<div class="panel panel-primary">
						<div class="panel-heading">Filter Pencarian Data</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="cari" placeholder="Cari Judul/Kategori/Ringkasan/Kode Dokumen/Penanggung Jawab">
								<select class="form-control" id="select">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
								<button type="submit" class="btn btn-ar btn-primary">Cari</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="result2">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Judul</th>
								<!--<th>Tanggal</th>-->
								<th>Kategori</th>
								<th>Ringkasan</th>
								<th>Penanggungjawab<br>Penerbitan</th>
								<th>Kode Dok.</th>
								<th>Tipe</th>
								<th>Jenis Informasi</th>
								<th>Retensi</th>
								<th>Ukuran File</th>
								<th colspan="2" align="center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Materi Workshop DIP</td>
								<td>Umum</td>
								<td>
								Materi Workshop mengenai pembuatan daftar informasi publik (DIP)
								</td>
								<td>Data Center Kab.Merauke</td>
								<td>a02</td>
								<td>Pdf</td>
								<td>Setiap Saat</td>
								<td>2014-11-27</td>
								<td></td>
								<td>
									<a href="#" class="downloadfile" title="Download Dokumen" data="3">
										<i class="fa fa-download"></i>
									</a>
									<a href="#" class="feedback" title="Feedback Dokumen" data="3">
										<i class="fa fa-reply"></i>
									</a>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
				
				<!-- History permintaan data -->
				<div class="tab-pane" id="dip3">
					<div class="panel panel-primary">
						<div class="panel-heading">Filter Pencarian Data</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="cari" placeholder="Cari Judul/Kategori/Ringkasan/Kode Dokumen/Penanggung Jawab">
								<select class="form-control" id="select">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
								<button type="submit" class="btn btn-ar btn-primary">Cari</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="result3">
					<table class="table table-hover">
						<thead>
						  <tr>
							<th>#</th>
							<th>Tanggal</th>
							<th>Nama Pemohon</th>
							<th>Kota</th>
							<th>Pekerjaan</th>
							<th>Info yang dimohon</th>
							<th>Skpd</th>
							<th>Waktu</th>
							<th>Respon</th>
							<th colspan="2" align="center">Aksi</th>
						  </tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>2014-12-04</td>
								<td>AGUNG</td>
								<td>SEMARANG</td>
								<td>1</td>
								<td>Minta data anu</td>
								<td>DINAS PENDIDIKAN</td>
								<td>2014-12-04 06:48:03</td>
								<td>
								<div></div>
								</td>
								<td>
								<a href="#" class="response" data="1" title="Lihat Respon dari Permintaan ini">
									<i class="fa fa-comment-o"></i>
								</a>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
				
				<!-- History permintaan keberatan -->
				<div class="tab-pane" id="dip4">
					<div class="panel panel-primary">
						<div class="panel-heading">Filter Pencarian Data</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="cari" placeholder="Cari Judul/Kategori/Ringkasan/Kode Dokumen/Penanggung Jawab">
								<select class="form-control" id="select">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
								<button type="submit" class="btn btn-ar btn-primary">Cari</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="result4">
					<table class="table table-hover">
						<thead>
							<tr>
							<th>#</th>
							<th>Tanggal</th>
							<th>Nama Pemohon</th>
							<th>Kota</th>
							<th>Pekerjaan</th>
							<th>Info yang dimohon</th>
							<th>Alasan Keberatan</th>
							<th>Tanggapan</th>
							<th>SKPD</th>
							<th colspan="2" align="center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>2014-12-04</td>
								<td>AGUNG</td>
								<td>SEMARANG</td>
								<td>1</td>
								<td>Minta data anu</td>
								<td>Pokoknya keberatan</td>
								<td>Hedeh</td>
								<td>
									<div>DPU Pengairan</div>
								</td>
								<td>
								<a href="#" class="response" data="1" title="Lihat Respon dari Permintaan ini">
									<i class="fa fa-comment-o"></i>
								</a>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
				<!--ganti password-->
				<div class="tab-pane" id="dip5">
					<div class="panel panel-primary">
						<div class="panel-heading">Ganti Password</div>
						<div class="panel-body">
							<form class="form-inline" role="form">
							  <div class="form-group pull-right">
								<input type="text" class="form-control std" id="cari" placeholder="Cari Judul/Kategori/Ringkasan/Kode Dokumen/Penanggung Jawab">
								<select class="form-control" id="select">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
								<button type="submit" class="btn btn-ar btn-primary">Cari</button>
							  </div>
							</form>
						</div>
					</div>
					<div id="result4">
					<table class="table table-hover">
						<thead>
							<tr>
							<th>#</th>
							<th>Tanggal</th>
							<th>Nama Pemohon</th>
							<th>Kota</th>
							<th>Pekerjaan</th>
							<th>Info yang dimohon</th>
							<th>Alasan Keberatan</th>
							<th>Tanggapan</th>
							<th>SKPD</th>
							<th colspan="2" align="center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>2014-12-04</td>
								<td>AGUNG</td>
								<td>SEMARANG</td>
								<td>1</td>
								<td>Minta data anu</td>
								<td>Pokoknya keberatan</td>
								<td>Hedeh</td>
								<td>
									<div>DPU Pengairan</div>
								</td>
								<td>
								<a href="#" class="response" data="1" title="Lihat Respon dari Permintaan ini">
									<i class="fa fa-comment-o"></i>
								</a>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
        </div>
    </div> <!-- row -->
</div>

<!-- Modal -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
	<div class="modal-content" id="div-content">
	  
	</div>
  </div>
</div>
