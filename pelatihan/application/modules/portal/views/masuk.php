<?php
if($this->session->userdata('is_login_pelatihanuser')){
	header('location: '.base_url().'portal/profile');
}
?>
<header class="main-header">
    <div class="container">
        <h1 class="page-title">Login Peserta</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Login Peserta</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="center-block logig-form">
        <div class="panel panel-primary">
            <div class="panel-heading">Login Form</div>
            <div class="panel-body">
                <form role="form" id="form1" name="form1" method="post" action="<?=base_url()?>portal/login" enctype="multiparth/form-data">
                    <div class="form-group">
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" placeholder="Username" id="username" name="username">
                        </div>
                        <br>
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password" id="userpass" name="userpass">
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" id="checkbox_remember">
                            <label for="checkbox_remember">Remember me</label>
                        </div>
                        <button type="submit" class="btn btn-ar btn-primary pull-right">Login</button>
                        <hr class="dotted margin-10">
                        <a href="<?=base_url()?>portal/signup" class="btn btn-ar btn-warning">Belum terdaftar?</a>
                      
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>