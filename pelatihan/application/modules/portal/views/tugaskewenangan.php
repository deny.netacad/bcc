<header class="main-header">
    <div class="container">
        <h1 class="page-title">Tugas dan Kewenangan</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="#">Tentang Disnaker Kab. Bogor</a></li>
            <li class="active">Tugas dan Kewenangan</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-logo animated fadeInDown animation-delay-5">Portal <span>Unit Pelaksana Teknis – Balai Latihan Kerja</span></div>
        </div>
        <div class="col-md-7">

            <p>Sesuai Peraturan Bupati Nomor 65 Tahun 2008 Tentang Pembentukan, Organisasi dan Tata Kerja Unit Pelaksana Teknis (UPT) Balai Latihan Kerja pada Dinas Sosial, Tenaga Kerja dan Transmigrasi Kabupaten Bogor tugas dan fungsinya adalah :</p>
            <p style="text-align:justify">1) Tugas Unit Pelaksana Teknis adalah melaksanakan sebagian tugas, tanggungjawab dan wewenang teknis Dinas.
			<p>2) Fungsi Unit Pelaksana Teknis adalah :
<p style="padding-left: 30px;">a) Penyelenggaran ketatausahaan UPT;</p>
<p style="padding-left: 30px;">b) Penyusunan perencanaan program pelatihan;</p>
<p style="padding-left: 30px;">c) Perekrutan dan seleksi peserta pelatihan;</p>
<p style="padding-left: 30px;">d) Penyelenggaraan pelatihan;</p>
<p style="padding-left: 30px;">f) Pelayanan informasi pelatihan; dan</p>
<p style="padding-left: 30px;">g) Pelaksanaan tugas lain yang diberikan oleh Kepala Dinas.</p>
			<p>3) Dalam melaksanakan tugas dan fungsinya Unit Pelaksana Teknis dipimpin oleh seorang Kepala Unit Pelaksana Teknis dan dibantu oleh Sub Bagian Tata Usaha.</p>
						</p>
        </div>
         <div class="col-md-5">
            <h2 class="right-line">Sekilas Pendaftaran Pelatihan Disnaker Kab. Bogor</h2>
            <div class="panel-group" id="accordion">
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					  <i class="fa fa-user">  Akun</i> 
					</a>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in">
				  <div class="panel-body">
					<p>Yang harus dilakukan pertama kali oleh calon peserta pelatihan adalah membuat akun,akun ini berfungsi sebagai syarat untuk pengajuan pendaftaran pelatihan.</p>
				  </div>
				</div>
			  </div>
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					  <i class="fa fa-desktop">  Login</i> 
					</a>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse">
				  <div class="panel-body">
					<p>Peserta yang sudah membuat akun selanjutnya melakukan login dengan username dan password yang sudah dibuat sebelumnya.</p>
				  </div>
				</div>
			  </div>
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
					  <i class="fa fa-lightbulb-o"></i> Keuntungan Pendaftaran Online
					</a>
				</div>
				<div id="collapseThree" class="panel-collapse collapse">
				  <div class="panel-body">
					<p>1. Pendaftaran menjadi fleksibel dan cepat. </p>
					<p>2. Mudah dan bisa diakses melalui laptop / smartphone anda. </p>
				  </div>
				</div>
			  </div>
			</div>
        </div>
    </div> <!-- row -->
</div>