<?php
	$where = " where a.username='".$this->session->userdata('username')."' ";
	$per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= " and (a.req_text like '%".$cari."%' )";
	
	$n = intval($this->uri->segment(5));
	$q = "SELECT a.*,b.job,b.city,c.skpd_name FROM req_info a
	INNER JOIN ref_applicant b ON a.username=b.username
	LEFT JOIN ref_skpd c ON a.skpd_id=c.skpd_id
	";
	$rs = $this->db->query("$q $where ");
	
	$row = $rs->row_array();
	$totrows = $rs->num_rows();
	$config['base_url'] = base_url().'portal/page/data/profil_req_info';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("
	$q $where  
	ORDER BY b.tmstamp DESC limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
	
?>
<div class="blog-pagination">
	<div class="pages blogpages">
		<span class="paginate_info pageof">Page <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
		<?=$data['pagination']?>
	</div>
</div>
<table class="table table-hover">
  <colgroup>
	<col style="width:30px;" />
	<col style="width:100px;" />
  </colgroup>
  <thead>
	  <tr>
		<th>#</th>
		<th>Tanggal</th>
		<th>Nama Pemohon</th>
		<th>Kota</th>
		<th>Pekerjaan</th>
		<th>Info yang dimohon</th>
		<th>Skpd</th>
		<th>Waktu</th>
		<th>Respon</th>
		<th colspan="2" align="center">Aksi</th>
	  </tr>
  </thead>
  <tbody>
  <?php
  $n = 0;
	$jobarr = array("","Wiraswasta","Buruh","Petani","Nelayan","PNS","Swasta","Pedagang","Pekerja Rumah Tangga",
	"LSM","Tenaga Pendidik","Pengacara","Tenaga Kesehatan","Peneliti","Seniman","Wartawan","Mahasiswa/Pelajar"
	,"Lain-lain");
  foreach($data['posts']->result() as $item){
  $n++;
  ?>
	  <tr>
		<td><?=$n?></td>
		<td><?=$item->req_date?></td>
		<td><?=$this->session->userdata('fullname')?></td>
		<td><?=$item->city?></td>
		<td><?=$jobarr[$item->job]?></td>
		<td><?=$item->req_info?></td>
		<td><?=$item->skpd_name?></td>
		<td><?=$item->tmstamp?></td>
		<td>
		<div><?=$item->req_respon?></div>
		</td>
		<td>
		<a href="#" class="response" data="<?=$item->req_id?>" title="Lihat Respon dari Permintaan ini">
			<i class="fa fa-comment-o"></i>
		</a>
		</td>
	  </tr>
	<?php
	}
	?>
  </tbody>
</table>   