<header class="main-header">
    <div class="container">
        <h1 class="page-title">JADWAL PELATIHAN DINPERINDAG</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Jadwal Pelatihan Dinperindag Jateng</li>
        </ol>
    </div>
</header>

<div class="container">
    <section>
        <h3 class="timeline-title"><span>2015</span></h3>
        <div class="row timeline">
            <div class="col-sm-6 timeline-left">
                <?php
					$icon = array("fa-file-video-o","fa-file-pdf-o","fa-file-image-o");
					$iconcolor = array("icon-ar-warning","icon-ar-success","icon-ar-help");
					$arrdata[0] = ' DINPERINDAG JATENG';
					$arrdata[1] = 'Peraturan Bupati Merauke no. 45 tentang Tupoksi DPU Pengairan 2008';
					$arrdata[2] = 'Struktur Organisasi Kominfo Kab. Merauke';
					$arrdata2[0] = 'PENDAFTARAN PESERTA PELATIHAN by <span>DINAS PERINDUSTRIAN DAN PERDAGANGAN PROVINSI JAWA TENGAH</span>';
					$arrdata2[1] = 'Peraturan Bupati Merauke no. 45 tentang Tupoksi DPU Pengairan 2008 by <span>DPU Pengairan</span>';
					$arrdata2[2] = 'Struktur Organisasi Kominfo Kab. Merauke by <span>Kominfo Kab. Merauke</span>';
					for($i=0;$i<3;$i++){
				?>
				<div class="timeline-event timeline-event-left animated bounceInLeft animation-delay-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><strong><?=$arrdata[$i]?></strong> - 15 Jan</div>
                        <div class="panel panel-default">
							<div class="panel-heading"><?=$arrdata[$i]?></div>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-4">
										<span class="icon-ar icon-ar-xl-lg icon-ar-square <?=$iconcolor[$i]?>"><i class="fa <?=$icon[$i]?>"></i></span>
									</div>
									<div class="col-sm-8">
										<p><?=$arrdata2[$i]?></p>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
				<?php
				}
				?>
            </div>
            <div class="col-sm-6 timeline-right">
                <?php
				for($i=0;$i<3;$i++){
				?>
				<div class="timeline-event timeline-event-right animated bounceInLeft animation-delay-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><strong><?=$arrdata[$i]?></strong> - 28 Jan</div>
                        <div class="panel panel-default">
							<div class="panel-heading"><?=$arrdata[$i]?></div>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-4">
										<span class="icon-ar icon-ar-xl-lg icon-ar-square <?=$iconcolor[$i]?>"><i class="fa <?=$icon[$i]?>"></i></span>
									</div>
									<div class="col-sm-8">
										<p><?=$arrdata2[$i]?></p>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
				<?php
				}
				?>
            </div>
        </div>
    </section>
	<section id="div-more">
		<a href="#"><h3 class="timeline-title"><span>More</span></h3></a>
	</section>
</div>