<?php
	$where = " ";
	$per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= " where (a.dip_title like '%".$cari."%' or a.dip_desc like '%".$cari."%'
	 or a.dip_responsible like '%".$cari."%' or a.dip_code like '%".$cari."%' or a.dip_foi like '%".$cari."%'
	  or a.dip_toi like '%".$cari."%' or a.dip_stspub like '%".$cari."%'
	   or d.dip_cat like '%".$cari."%'
	)";
	
	$n = intval($this->uri->segment(5));
	$q = "SELECT a.*,c.dip_file,c.dip_size,d.dip_cat FROM ref_dip a
	LEFT JOIN his_dip_applicant b ON a.dip_id=b.dip_id
	INNER JOIN ref_dip_file c ON a.dip_id=c.dip_id
	INNER JOIN ref_dip_cat d ON a.dip_catid=d.dip_catid
	";
	$rs = $this->db->query("$q $where  group by a.dip_id");
	$row = $rs->row_array();
	$totrows = $rs->num_rows();
	$config['base_url'] = base_url().'portal/page/data/dip_data';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("
	$q $where group by a.dip_id 
	ORDER BY b.tmstamp DESC limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
	
?>
<div class="blog-pagination">
	<div class="pages blogpages">
		<span class="paginate_info pageof">Page <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
		<?=$data['pagination']?>
	</div>
</div>
<table class="table table-hover">
  <thead>
	  <tr>
		<th>#</th>
		<th>Judul</th>
		<!--<th>Tanggal</th>-->
		<th>Kategori</th>
		<th>Ringkasan</th>
		<th>Penanggungjawab<br />Penerbitan</th>
		<th>Kode Dok.</th>
		<th>Tipe</th>
		<th>Jenis Informasi</th>
		<th>Retensi</th>
		<th>Ukuran File</th>
		<th colspan="2" align="center">Aksi</th>
	  </tr>
  </thead>
  <tbody>
  <?php
  $n = 0;
  foreach($data['posts']->result() as $item){
  $n++;
  ?>
	  <tr>
		<td><?=$n?></td>
		<td><?=$item->dip_title?></td>
		<!--<td><?=$item->dip_datestart?></td>-->
		<td><?=$item->dip_cat?></td>
		<td><?=$item->dip_desc?></td>
		<td><?=$item->dip_responsible?></td>
		<td><?=$item->dip_code?></td>
		<td><?=$item->dip_foi?></td>
		<td><?=$item->dip_toi?></td>
		<td><?=$item->dip_dateend?></td>
		<td><?=$item->dip_size?></td>
		<td>
			<a href="<?=base_url()?>portal/page/downloadfile/<?=$item->dip_id?>" class="downloadfile" title="Download Dokumen" data="<?=$item->dip_id?>">
			<i class="fa fa-download"></i>
			</a>
		</td>
	  </tr>
	<?php
	}
	?>
  </tbody>
</table>   