<header class="main-header">
    <div class="container">
        <h1 class="page-title">Visi & Misi</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="#">Tentang Disnaker Kab. Bogor</a></li>
            <li class="active">Visi & Misi</li>
        </ol>
    </div>
</header>
<div class="container" id="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-logo animated fadeInDown animation-delay-5">Portal <span>PELATIHAN DISNAKER KAB. BOGOR</span></div>
        </div>
        <div class="col-md-7">
            <h2 class="right-line">Visi</h2>
            <p style="text-align:justify">Terwujudnya Tenaga Kerja yang Berdaya Saing dan Profesional, Transmigrasi yang Produktif,serta Iklim Ketenagakerjaan yang Kondusif</p>
			<h2 class="right-line">Misi</h2>
            <p style="text-align:justify">
			<ul style="list-style-type: circle;">
<li>Meningkatkan kapasitas dan profesionalisme aparatur.</li>
<li>Mengurangi tingkat pengangguran melalui pelatihan kerja yang berkarakter, perluasan kesempatan kerja serta transmigrasi yang produktif.</li>
<li>Meningkatkan kenyamanan, ketenangan dan keselamatan kerja serta perlindungan hak-hak normatif pekerja dan pengusaha.</li>
<li>Meningkatkan fungsi dan peran sarana hubungan industrial serta pengetahuan pekerja, pengguna dan pemberi kerja.</li>
</ul>        </div>
        <div class="col-md-5">
            <h2 class="right-line">Sekilas Pendaftaran Pelatihan Disnaker Kab. Bogor</h2>
            <div class="panel-group" id="accordion">
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					  <i class="fa fa-user">  Akun</i> 
					</a>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in">
				  <div class="panel-body">
					<p>Yang harus dilakukan pertama kali oleh calon peserta pelatihan adalah membuat akun,akun ini berfungsi sebagai syarat untuk pengajuan pendaftaran pelatihan.</p>
				  </div>
				</div>
			  </div>
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					  <i class="fa fa-desktop">  Login</i> 
					</a>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse">
				  <div class="panel-body">
					<p>Peserta yang sudah membuat akun selanjutnya melakukan login dengan username dan password yang sudah dibuat sebelumnya.</p>
				  </div>
				</div>
			  </div>
			  <div class="panel panel-default">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
					  <i class="fa fa-lightbulb-o"></i> Keuntungan Pendaftaran Online
					</a>
				</div>
				<div id="collapseThree" class="panel-collapse collapse">
				  <div class="panel-body">
					<p>1. Pendaftaran menjadi fleksibel dan cepat. </p>
					<p>2. Mudah dan bisa diakses melalui laptop / smartphone anda. </p>
				  </div>
				</div>
			  </div>
			</div>
        </div>
    </div> <!-- row -->
</div>