<header class="main-header">
    <div class="container">
        <h1 class="page-title">Permohonan Data & Informasi</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="#">Alur</a></li>
            <li class="active">Permohonan Data & Informasi</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-logo animated fadeInDown animation-delay-5">Portal <span>e-Data Center</span></div>
        </div>
        <div class="col-md-12">
            <h2 class="right-line">Alur Permohonan Informasi</h2>
            <p style="text-align:justify">Merencanakan, melaksanakan, mengkoordinasikan dan mengendalikan pengumpulan informasi, 
			pengklasifikasian informasi, pendokumentasian informasi dan pelayanan informasi 
			dari Pejabat Pengelola Informasi dan Dokumentasi SKPD Pembantu;
			Menyimpan, mendokumentasikan, menyediakan dan/atau memberikan pelayanan informasi kepada publik;
			Mengolah dan mengklasifikasi informasi dan dokumentasi secara sistematis berdasarkan tugas pokok dan 
			fungsi organisasi serta kategori informasi;Melaporkan hasil pelaksanaan tugasnya kepada Bupati Merauke 
			melalui Sekretaris Daerah Kabupaten Merauke.</p>
			<h2 class="right-line">Kewenangan</h2>
            <p style="text-align:justify">
			Menolak memberikan informasi yang dikecualikan sesuai dengan ketentuan peraturan perundang-undangan;
			Meminta dan memperoleh informasi dari unit kerja/komponen/satuan kerja yang menjadi cakupan kerjanya;
			Mengkoordinasikan pemberian pelayanan informasi dengan Pejabat Pengelola Informasi dan Dokumentasi SKPD
			Pembantu dan/atau Pejabat fungsional yang menjadi cakupan kerjanya;
			</p>
        </div>
    </div> <!-- row -->
</div>