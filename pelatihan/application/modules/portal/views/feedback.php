<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
<h4 class="modal-title" id="myModalLabel">Feedback dokumen ini</h4>
</div>
<form id="form-feedback" name="form-feedback" action="<?=base_url()?>portal/saveFeedback" class="form-horizontal" method="post" enctype="multipart/form-data">
<div class="modal-body">
		<?php
		$rs = $this->db->query("SELECT a.dip_id,DATE_FORMAT(a.createon,'%d/%m/%Y') AS createon,a.dip_title,a.dip_foi,a.dip_desc 
		,MONTHNAME(a.createon) AS month_name,b.dip_file,b.dip_path
		FROM ref_dip a 
		left join ref_dip_file b on a.dip_id=b.dip_id
		where a.dip_id='".$this->input->post('dip_id')."'");
		$i = 0;
		$item = $rs->row();
		?>
	  <div class="form-group">
		<label for="noid" class="col-sm-2 control-label">File</label>
		<div class="col-sm-2 col-md-3 col-lg-6">
		<h2 style="margin-top:0px"><?=$item->dip_title?></h3>
		<p>Jenis File : <?=$item->dip_foi?></p>
		<p>
		<?php 
		echo substr(strip_tags($item->dip_desc,""),0,100).' ...';
		?>
		</p>	
		</div>
	  </div>
	  <div class="form-group">
		<label for="noid" class="col-sm-2 control-label">Isi Feedback</label>
		<div class="col-sm-2 col-md-3 col-lg-6">
			<textarea id="req_feedback" name="req_feedback" style="width:100%"></textarea>
		</div>
	  </div>
	
</div>
<div class="modal-footer">
<button type="button" class="btn btn-ar btn-default" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-ar btn-primary">Kirim Feedback</button>
</div>
</form>