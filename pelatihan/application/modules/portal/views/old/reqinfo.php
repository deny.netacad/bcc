<link rel="stylesheet" href="<?=$def_js?>plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>
<script>
$(document).ready(function(){
	 
	
	$("#form1").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form1').trigger('reset');
			});		
		}else{
			
			$('#form1').trigger('reset');
		}
	});
	
	$('#form1').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
	
	$('#myTab li a').on('click',function(e){
		switch($(this).attr('href')){
			case "#tab-sample2":
				refreshData2();
			break;
		}
	});
	
	$('#per_page').change(function(){
		refreshData();
	});
	
	$('#cari').keyup( $.debounce(250,refreshData2));
	
	$(document).on('click','a.ajax-replace', function(){
		$.ajax({
			type:'post',
			url:$(this).attr('href'),
			data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
			beforeSend:function(){
			},
			success:function(response){
				$('#result').html(response);
			}
		});
		return false;
	});
	
	$(document).on('click','a.keberatan',function(){
		var $this = $('a.keberatan');
		var index = $this.index($(this));
		$.ajax({
			type:'post',
			url:'<?=base_url()?>portal/page/view/reqinfo_keberatan_form',
			data:{ 'req_id':$this.eq(index).attr('data') },
			beforeSend:function(){
				
			},
			success:function(response){
				$('#result2').html(response);
			}
		});
	});
	
	$(document).on('click','a.feedback',function(){
		var $this = $('a.feedback');
		var index = $this.index($(this));
		$.ajax({
			type:'post',
			url:'<?=base_url()?>portal/page/view/reqinfo_feedback_form',
			data:{ 'req_id':$this.eq(index).attr('data') },
			beforeSend:function(){
				
			},
			success:function(response){
				$('#result2').html(response);
			}
		});
	});
	
});

function refreshData2(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/reqinfo_keberatan',
		data:{ 'per_page':$('#tab-sample2 #per_page').val(),'cari':$('#tab-sample2 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result2').html(response);
		}
	});
}


</script>
<section class="content-wrapper">       
            <div class="row">
                <div class="twelve column">
                    <ul class="tabs" id="myTab">
                        <li class="active"><a href="#tab-sample1">Form Permintaan Informasi</a></li>
						<li class=""><a href="#tab-sample2">Form Pengajuan Keberatan / Feedback</a></li>
                    </ul>
                    <div class="tab_container">
                        <div id="tab-sample1" class="tab_content" style="display: block;">                            
							<div id="contact-form-area">
								<!-- Contact Form Start //-->
								<?php
								if($this->session->userdata('is_login_pelatihanuser')){
									$data['username'] = $this->session->userdata('username');
								}else{
									$data['username'] = '';
									redirect('portal/masuk');
								}
								
								$rs = $this->db->get_where("ref_applicant",$data);
								$item = $rs->row();
								$job = array("","Wiraswasta","Buruh","Petani","Nelayan","PNS","Swasta","Pedagang","Pekerja Rumah Tangga","LSM","Tenaga Pendidik","Pengacara","Tenaga Kesehatan"
								,"Peneliti","Seniman","Wartawan","Mahasiswa/Pelajar","Lain-lain");
								?>
								<form id="form1" name="form1" action="<?=base_url()?>portal/saveReqinfo" class="contactform" method="post" enctype="multipart/form-data">
									<fieldset> 
									<label>Nama Pemohon <em>*</em></label> 
									<input type="text" name="fullname" class="textfield" id="fullname" value="<?=$item->fullname?>" readonly />                        
									<label>Pekerjaan <em>*</em></label> 
									<input type="text" name="job" class="textfield" id="job" value="<?=$job[$item->job]?>" readonly />                        
									<label>Alamat <em>*</em></label>
									<textarea name="addr" id="addr" class="textarea" cols="2" rows="2"><?=$item->addr?></textarea>
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<label>Kota <em>*</em></label> 
									<input type="text" name="city" class="textfield" id="city" value="<?=$item->city?>">                        
									<label>Kode POS <em>*</em></label> 
									<input type="text" name="postcode" class="textfield" id="postcode" value="<?=$item->postcode?>">                        
									<label>Email <em>*</em></label> 
									<input type="email" name="email" class="textfield" id="email" value="<?=$item->email?>">                        
									<label>Telp. Rumah <em>*</em></label> 
									<input type="text" name="phone_home" class="textfield" id="phone_home" value="<?=$item->phone_home?>">                        
									<label>Telp. Mobile <em>*</em></label> 
									<input type="text" name="phone_mobile" class="textfield" id="phone_mobile" value="<?=$item->phone_mobile?>">                        
									<label>Judul Dokumen <em>*</em></label>
									<input type="text" name="req_title" class="textfield" id="req_title" value="" placeHolder="Judul/Nama Dokumen">                        
									<label>Isi Dokumen <em>*</em></label>
									<textarea name="req_info" id="req_info" class="textarea" cols="2" rows="4"></textarea>
									<label>Alasan Permintaan Informasi <em>*</em></label>
									<textarea name="req_alasan" id="req_alasan" class="textarea" cols="2" rows="4"></textarea>
									<label>SKPD <em>*</em></label>                        
									<?php
									$rs_skpd = $this->db->get("ref_skpd");
									?>			
									<select name="skpd_id" id="skpd_id" class="textfield">
										<option value="">| Kosongkan jika ditujukan ke PPID Kabupaten</option>
										<?php
										foreach($rs_skpd->result() as $itemskpd){
										?>
										<option value="<?=$itemskpd->skpd_id?>" <?=(($item->skpd_id==$itemskpd->skpd_id)?"selected":"")?>><?=$itemskpd->skpd_name?></option>
										<?php
										}
										?>
									</select> 
									<div class="clear"></div> 
									<label>Metode Mendapatkan Informasi <em>*</em></label>                           
									<select name="req_type" id="req_type" class="textfield" required>
										<option value="1" <?=(($item->req_type==1)?"selected":"")?>>Download</option>
										<option value="2" <?=(($item->req_type==2)?"selected":"")?>>Datang Langsung</option>
									</select> 
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<button type="submit" name="submit" class="buttoncontact" id="buttonsend">Submit</button>
									<span class="loading" style="display: none;">Please wait..</span>
									<div class="clear"></div>
									</fieldset> 
								</form>
								<!-- Contact Form End //-->
							</div>                            
                        </div>
                                                            
                        <div id="tab-sample2" class="tab_content" style="display: none;">                            
                            <div class="twelve"> 
								<input type="text" id="cari" name="cari" class="six" placeHolder="Cari Informasi..." />
								<select id="per_page" name="per_page" style="width:60px">
								  <option value="">-</option>
								  <option value="4">4</option>
								  <option value="25">25</option>
								  <option value="50">50</option>
								</select>
							</div>
							<div class="twelve" id="result2" style="overflow-y:auto"> 
								 
							</div>
                        </div>
						
                                                                                                
                    </div>	
                </div>              
                
                <div class="twelve column">
                    <hr>
               </div>
               
            </div>             
        </section>