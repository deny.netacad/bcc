<!-- content section start here -->
        <section class="content-wrapper">    	
            <div class="row">        
                <div class="column">
                	<h3 class="margin-bottom"><span class="highlight">TUGAS DAN KEWENANGAN</span></h3>
                    <p class="lead"></p>
                    
                    <p>
						Tugas dan Kewenangan<br />

						Tugas :<br />
						Merencanakan, melaksanakan, mengkoordinasikan dan mengendalikan pengumpulan informasi, pengklasifikasian informasi, pendokumentasian informasi dan pelayanan informasi dari Pejabat Pengelola Informasi dan Dokumentasi (PPID) Pembantu;Menyimpan, mendokumentasikan, menyediakan dan/atau memberikan pelayanan informasi kepada public;Mengolah dan mengklasifikasi informasi dan dokumentasi secara sistematis berdasarkan tugas pokok dan fungsi organisasi serta kategori informasi;Melaporkan hasil pelaksanaan tugasnya kepada Bupati Malang melalui Sekretaris Daerah Kabupaten Malang.
						<br /> 
						Kewenangan :<br />
						Menolak memberikan informasi yang dikecualikan sesuai dengan ketentuan peraturan perundang-undangan;Meminta dan memperoleh informasi dari unit kerja/komponen/satuan kerja yang menjadi cakupan kerjanya;Mengkoordinasikan pemberian pelayanan informasi dengan Pejabat Pengelola Informasi dan Dokumentasi (PPID) Pembantu dan/atau Pejabat fungsional yang menjadi cakupan kerjanya;
					</p>
                    	
                </div>
            </div>            	                 
        </section>
        <!-- content section end here --> 