<link rel="stylesheet" href="<?=$def_js?>plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>
<script>
$(document).ready(function(){
	 $('#form1 #scanfile').fileupload({
		dataType: 'json',
		formData: { 'filename':'<?=date('YmdHis')?>' },
		done: function (e, data) {
			
			try{
			var vfilepath = data.result.upload_data.full_path;
			var vfilename = data.result.upload_data.file_name;
			var vhtml = '<div class="clear"></div>'+
			'<div><label>&nbsp;</label><div><input type="hidden" name="scanfile[]" value="'+vfilename+'" />'+vfilename+' <a class="delthis" data="'+vfilepath+'" href="#">[x]</a></div></div>';
			
			$('#form1 #progress .bar').after(vhtml);
			$('#form1 #progress .bar').html('');
			$('#form1 #progress .bar').css('width','0%');
			}catch(e){ alert(e.message); }
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#form1 #progress .bar').css(
				'width',
				progress + '%'
			);
			
		}
	});
	
	$('#form2 #scanfile').fileupload({
		dataType: 'json',
		formData: { 'filename':'<?=date('YmdHis')?>' },
		done: function (e, data) {
			
			try{
			var vfilepath = data.result.upload_data.full_path;
			var vfilename = data.result.upload_data.file_name;
			var vhtml = '<div class="clear"></div>'+
			'<div><label>&nbsp;</label><div><input type="hidden" name="scanfile[]" value="'+vfilename+'" />'+vfilename+' <a class="delthis" data="'+vfilepath+'" href="#">[x]</a></div></div>';
			
			$('#form2 #progress .bar').after(vhtml);
			$('#form2 #progress .bar').html('');
			$('#form2 #progress .bar').css('width','0%');
			}catch(e){ alert(e.message); }
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#form2 #progress .bar').css(
				'width',
				progress + '%'
			);
			
		}
	});
	
	$(document).on('click','a.delthis',function(){
		
		var $this = $('a.delthis');
		var index = $this.index($(this));
		
		var vdata = $this.eq(index).attr('data');
		$.ajax({
			type:'post',
			url:'<?=base_url()?>portal/delUpload',
			data:{ 'full_path': vdata },
			beforeSend:function(){
				
			},
			success:function(response){
				var ret = $.parseJSON(response);
				alert(ret.text);
				$this.eq(index).parent().parent().remove();
			}
		});
	});
	
	$("#form1").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form1').trigger('reset');
			});		
		}else{
			
			$('#form1').trigger('reset');
		}
	});
	
	$('#form1').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
	$("#form2").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form2').trigger('reset');
			});		
		}else{
			
			$('#form2').trigger('reset');
		}
	});
	
	$('#form2').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
});
</script>
<section class="content-wrapper">       
            <div class="row">
                <div class="twelve column">
                    <ul class="tabs">
                        <li class="active"><a href="#tab-sample1">Pendaftaran Individu</a></li>
                        <li class=""><a href="#tab-sample2">Pendaftaran Organisasi</a></li>
                    </ul>
                    <div class="tab_container">
                        <div id="tab-sample1" class="tab_content" style="display: block;">                            
							<div id="contact-form-area">
								<!-- Contact Form Start //-->
								<form id="form1" name="form1" action="<?=base_url()?>portal/sendRegister" class="contactform" method="post" enctype="multipart/form-data">
									<fieldset> 
									<label>Kartu Pengenal <em>*</em></label>                           
									<select name="identity" id="identity" class="textfield" required>
										<option value="1">KTP</option>
										<option value="2">SIM</option>
										<option value="3">PASSPORT</option>
										<option value="4">KTM</option>
									</select> 
									<label>No. Identitas <em>*</em></label> 
									<input type="text" name="noid" class="textfield" id="noid" value="" required />                        
									<label>Scan File Identitas <em>*</em></label>
									<span class="btn btn-success fileinput-button" style="float:left;cursor:pointer">
										<span style="cursor:pointer">Add files...</span>
										<!-- The file input field used as target for the file upload widget -->
										<input type="file" name="scanfile" class="textfield" id="scanfile" value="" data-url="<?=base_url()?>portal/do_upload_scanfile" />
									</span>
									<div class="clear"></div>
									
									<label>&nbsp;</label>
									<div id="progress">
										<div class="bar textfield" style="width: 0%;color:orange"></div>
									</div>
									<label>Username <em>*</em></label> 
									<input type="text" name="username" class="textfield" id="username" value="" required />                        
									<label>Password <em>*</em></label> 
									<input type="password" name="userpass" class="textfield" id="userpass" value="">                        
									<label>Konfirmasi Password <em>*</em></label> 
									<input type="password" name="userpass2" class="textfield" id="userpass2" value="">                        
									<label>nama Lengkap <em>*</em></label> 
									<input type="text" name="fullname" class="textfield" id="fullname" value="">                        
									<label>Jenis Kelamin <em>*</em></label> 
									<select name="sex">
										<option value="1">Laki-Laki</option>
										<option value="2">Perempuan</option>
									</select>
									<label>Tempat Lahir <em>*</em></label> 
									<input type="text" name="pob" class="textfield" id="pob" value="">                        
									<label>Tanggal Lahir <em>*</em></label> 
									<input type="text" name="dob" class="textfield" id="dob" value="">                        
									<label>Pendidikan Terakhir <em>*</em></label> 
									<select name="education">
										<option value="1">SD</option>
										<option value="2">SMP</option>
										<option value="3">SMA</option>
										<option value="4">Diploma</option>
										<option value="5">Sarjana</option>
										<option value="6">Pasca Sarjana</option>
									</select>
									<label>Pekerjaan <em>*</em></label> 
									<select name="job">
										<option value="1">Wiraswasta</option>
										<option value="2">Buruh</option>
										<option value="3">Petani</option>
										<option value="4">Nelayan</option>
										<option value="5">PNS</option>
										<option value="6">Swasta</option>
										<option value="7">Pedagang</option>
										<option value="8">Pekerja rumah tangga</option>
										<option value="9">LSM</option>
										<option value="10">Tenaga Pendidik</option>
										<option value="11">Pengacara</option>
										<option value="12">Tenaga Kesehatan</option>
										<option value="13">Peneliti</option>
										<option value="14">Seniman</option>
										<option value="15">Wartawan</option>
										<option value="16">Mahasiswa/Pelajar</option>
										<option value="17">Lain-lain</option>
									</select>
									<label>Alamat <em>*</em></label>
									<textarea name="addr" id="addr" class="textarea" cols="2" rows="6"></textarea>
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<label>Kota <em>*</em></label> 
									<input type="text" name="city" class="textfield" id="city" value="">                        
									<label>Kode POS <em>*</em></label> 
									<input type="text" name="postcode" class="textfield" id="postcode" value="">                        
									<label>Email <em>*</em></label> 
									<input type="email" name="email" class="textfield" id="email" value="">                        
									<label>Telp. Rumah <em>*</em></label> 
									<input type="text" name="phone_home" class="textfield" id="phone_home" value="">                        
									<label>Telp. Mobile <em>*</em></label> 
									<input type="text" name="phone_mobile" class="textfield" id="phone_mobile" value="">                        
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<button type="submit" name="submit" class="buttoncontact" id="buttonsend">Submit</button>
									<span class="loading" style="display: none;">Please wait..</span>
									<div class="clear"></div>
									</fieldset> 
								</form>
								<!-- Contact Form End //-->
							</div>                            
                        </div>
                                                            
                        <div id="tab-sample2" class="tab_content" style="display: none;">                            
                            <div id="contact-form-area">
								<!-- Contact Form Start //-->
								<form id="form2" name="form2" action="<?=base_url()?>portal/sendRegister" class="contactform" method="post" enctype="multipart/form-data">
									<fieldset> 
									<label>Surat Pendirian <em>*</em></label>                           
									<select name="identity">
										<option value="5">Akta Pendirian</option>
										<option value="6">Lainnya</option>
									</select>
									<label>No. Surat Pendirian <em>*</em></label> 
									<input type="text" name="noid" class="textfield" id="noid" value="">                        
									<label>Scan File Surat Pendirian <em>*</em></label>
									<span class="btn btn-success fileinput-button" style="float:left;cursor:pointer">
										<span style="cursor:pointer">Add files...</span>
										<!-- The file input field used as target for the file upload widget -->
										<input type="file" name="scanfile" class="textfield" id="scanfile" value="" data-url="<?=base_url()?>portal/do_upload_scanfile" />
									</span>
									<div class="clear"></div>
									
									<label>&nbsp;</label>
									<div id="progress">
										<div class="bar textfield" style="width: 0%;color:orange"></div>
									</div>
									<label>Username <em>*</em></label> 
									<input type="text" name="username" class="textfield" id="username" value="" required />                        
									
									<label>Password <em>*</em></label> 
									<input type="password" name="userpass" class="textfield" id="userpass" value="">                        
									<label>Konfirmasi Password <em>*</em></label> 
									<input type="password" name="userpass2" class="textfield" id="userpass2" value="">                        
									<label>Nama Organisasi <em>*</em></label> 
									<input type="text" name="fullname" class="textfield" id="fullname" value="">                        
									<label>Jenis Kelamin <em>*</em></label> 
									<select name="sex">
										<option value="1">Laki-Laki</option>
										<option value="2">Perempuan</option>
									</select>
									<label>Tempat Lahir <em>*</em></label> 
									<input type="text" name="pob" class="textfield" id="pob" value="">                        
									<label>Tanggal Lahir <em>*</em></label> 
									<input type="text" name="dob" class="textfield" id="dob" value="">                        
									<label>Pendidikan Terakhir <em>*</em></label> 
									<select name="education">
										<option value="1">SD</option>
										<option value="2">SMP</option>
										<option value="3">SMA</option>
										<option value="4">Diploma</option>
										<option value="5">Sarjana</option>
										<option value="6">Pasca Sarjana</option>
									</select>
									<label>Pekerjaan <em>*</em></label> 
									<select name="job">
										<option value="1">Wiraswasta</option>
										<option value="2">Buruh</option>
										<option value="3">Petani</option>
										<option value="4">Nelayan</option>
										<option value="5">PNS</option>
										<option value="6">Swasta</option>
										<option value="7">Pedagang</option>
										<option value="8">Pekerja rumah tangga</option>
										<option value="9">LSM</option>
										<option value="10">Tenaga Pendidik</option>
										<option value="11">Pengacara</option>
										<option value="12">Tenaga Kesehatan</option>
										<option value="13">Peneliti</option>
										<option value="14">Seniman</option>
										<option value="15">Wartawan</option>
										<option value="16">Mahasiswa/Pelajar</option>
										<option value="17">Lain-lain</option>
									</select>
									<label>Alamat <em>*</em></label>
									<textarea name="addr" id="addr" class="textarea" cols="2" rows="6"></textarea>
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<label>Kota <em>*</em></label> 
									<input type="text" name="city" class="textfield" id="city" value="">                        
									<label>Kode POS <em>*</em></label> 
									<input type="text" name="postcode" class="textfield" id="postcode" value="">                        
									<label>Email <em>*</em></label> 
									<input type="email" name="email" class="textfield" id="email" value="">                        
									<label>Telp. Rumah <em>*</em></label> 
									<input type="text" name="phone_home" class="textfield" id="phone_home" value="">                        
									<label>Telp. Mobile <em>*</em></label> 
									<input type="text" name="phone_mobile" class="textfield" id="phone_mobile" value="">                        
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<button type="submit" name="submit" class="buttoncontact" id="buttonsend">Submit</button>
									<span class="loading" style="display: none;">Please wait..</span>
									<div class="clear"></div>
									</fieldset> 
								</form>
								<!-- Contact Form End //-->
							</div>       
                        </div>
                                                                                                
                    </div>	
                </div>              
                
                <div class="twelve column">
                    <hr>
               </div>
               
            </div>             
        </section>