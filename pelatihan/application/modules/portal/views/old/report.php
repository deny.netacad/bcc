<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/highcharts.js"></script>
<script>
$(document).ready(function(){
	
	$('#myTab li a').on('click',function(e){
		switch($(this).attr('href')){
			case "#tab-sample1":
				refreshData1();
			break;
			case "#tab-sample2":
				refreshData2();
			break;
		}
	});
	
	refreshData1();
	
	$('#tab-sample1 #per_page').change(function(){
		refreshData1();
	});
	
	$('#tab-sample1 #cari').keyup( $.debounce(250,refreshData1));
	
	$('#tab-sample2 #per_page').change(function(){
		refreshData2();
	});
	
	$('#tab-sample2 #cari').keyup( $.debounce(250,refreshData2));
	
	
});

function refreshData1(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/report1',
		data:{ 'per_page':$('#tab-sample1 #per_page').val(),'cari':$('#tab-sample1 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result1').html(response);
		}
	});
}


function refreshData2(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/report2',
		data:{ 'per_page':$('#tab-sample2 #per_page').val(),'cari':$('#tab-sample2 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result2').html(response);
		}
	});
}


</script>
<section class="content-wrapper">       
            <div class="row">
                <div class="twelve column">
                    <ul class="tabs" id="myTab">
                        <li class="active"><a href="#tab-sample1">Permohonan Informasi</a></li>
						<li class=""><a href="#tab-sample2">Pengajuan Keberatan</a></li>
                    </ul>
                    <div class="tab_container">
                        <div id="tab-sample1" class="tab_content" style="display: block;">                            
							<div class="twelve"> 
								<input type="text" id="cari" name="cari" class="six" placeHolder="Cari Informasi..." />
								<select id="per_page" name="per_page" style="width:60px">
								  <option value="">-</option>
								  <option value="4">4</option>
								  <option value="25">25</option>
								  <option value="50">50</option>
								</select>
							</div>
							<div class="twelve" id="result1" style="overflow-y:auto"> 
								 
							</div>
                        </div>
                                                            
                        <div id="tab-sample2" class="tab_content" style="display: none;">                            
                            <div class="twelve"> 
								<input type="text" id="cari" name="cari" class="six" placeHolder="Cari Informasi..." />
								<select id="per_page" name="per_page" style="width:60px">
								  <option value="">-</option>
								  <option value="4">4</option>
								  <option value="25">25</option>
								  <option value="50">50</option>
								</select>
							</div>
							<div class="twelve" id="result2" style="overflow-y:auto"> 
								 
							</div>
                        </div>
						
                                                                                                
                    </div>	
                </div>              

               </div>             
        </section>