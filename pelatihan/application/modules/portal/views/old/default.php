 <!-- slideshow start here -->
        <section id="slideshow-container">
            <div class="banner">            
                
                <ul>      
                    <!-- slide 1 -->
                    <li data-transition="papercut">
                        <img src="<?=base_url()?>assets/portal/images/slideshow/sld_imgbg1.jpg" alt="">
                        <div class="caption general_caption fade" data-x="-65" data-y="7" data-speed="400" data-start="700" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree1.png" alt=""></div>                       
                        <div class="caption general_caption fade" data-x="335" data-y="57" data-speed="400" data-start="100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree3.png" alt=""></div>
                        <div class="caption general_caption lfb" data-x="85" data-y="79" data-speed="6000" data-endspeed="1000" data-start="1500" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_pad1.png" alt="">
                        </div>
                        <div class="caption general_caption fade" data-x="-5" data-y="188" data-speed="400" data-start="1100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree2.png" alt=""></div>
                        <div class="caption general_caption fade" data-x="855" data-y="177" data-speed="400" data-start="700" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree4.png" alt=""></div>                    
                        <div class="caption general_caption fade" data-x="900" data-y="330" data-speed="400" data-start="1100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree5.png" alt=""></div>
                        
                        <div class="caption general_caption lfb ltb home-media1"  data-x="420" data-y="250" data-speed="900" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutExpo" data-endeasing="easeInExpo">
                        	<h5>Mekanisme dan Standar Pelayanan Publik</h5>
                        </div>                        
						<div class="caption general_caption lfb ltb home-media2"  data-x="420" data-y="290" data-speed="1400" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                        	<div class="slide-separator"></div>
                        </div>
                        <div class="caption general_caption lfb ltb "  data-x="420" data-y="310" data-speed="900" data-endspeed="1800" data-start="800" data-end="8500" data-easing="easeOutExpo" data-endeasing="easeInExpo">
                            <p>Pelayanan Informasi Publik didasari oleh Undang-Undang<br />
							No. 14 Tahun 2008 dan Peraturan Pemerintah<br/>
							No. 61 Tahun 2010</p>
                        </div>
                        <div class="caption general_caption lfb ltb"  data-x="420" data-y="400" data-speed="800" data-endspeed="2000" data-start="1300" data-end="8500" data-easing="easeOutExpo" data-endeasing="easeInExpo">
                            <a href="#" class="button-plain small orange">Selengkapnya <span class="plain-arrow">&gt;</span></a>
                        </div>
                        
                        <div class="caption general_caption lft" data-x="855" data-y="25" data-speed="8000" data-endspeed="1000" data-start="100" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird1.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="555" data-y="90" data-speed="8000" data-endspeed="1000" data-start="800" data-end="8400" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird2.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="795" data-y="210" data-speed="8000" data-endspeed="1000" data-start="600" data-end="8800" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird3.png" alt="">
                        </div>
                    </li>
                    
                    <!-- slide 2 -->
                    <li data-transition="papercut">
                        <img src="<?=base_url()?>assets/portal/images/slideshow/sld_imgbg1.jpg" alt="">
                        <div class="caption general_caption fade" data-x="-20" data-y="187" data-speed="400" data-start="700" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree7.png" alt=""></div>                     
                        <div class="caption general_caption fade" data-x="780" data-y="37" data-speed="400" data-start="100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree9.png" alt=""></div>
                        <div class="caption general_caption lft" data-x="720" data-y="200" data-speed="6000" data-endspeed="1000" data-start="1000" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_padinfo.png" alt="">
                        </div>
                        <div class="caption general_caption fade" data-x="-75" data-y="305" data-speed="400" data-start="1100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree6.png" alt=""></div>
                        <div class="caption general_caption fade" data-x="538" data-y="225" data-speed="400" data-start="700" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree8.png" alt=""></div>  
                        
                        <div class="caption general_caption lfb ltb home-media1"  data-x="219" data-y="173" data-speed="900" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                        	<h5 class="smallmargin-bottom">Daftar Informasi Publik</h5>
                        </div>                        
                        <div class="caption general_caption lfb ltb home-media2"  data-x="221" data-y="223" data-speed="1400" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                        	<div class="slide-separator"></div>
                        </div>
                        <div class="caption general_caption lfb ltb "  data-x="219" data-y="243" data-speed="900" data-endspeed="1800" data-start="800" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                            <p>Semua informasi yang bisa diakses oleh publik<br />dirangkum dalam Daftar Informasi Publik (DIP)</p>
                        </div>
                       <div class="caption general_caption lfb ltb"  data-x="219" data-y="293" data-speed="800" data-endspeed="2000" data-start="1300" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                            <a href="#" class="button-plain small orange">Selengkapnya <span class="plain-arrow">&gt;</span></a>
                        </div>
                        
                        <div class="caption general_caption lft" data-x="555" data-y="155" data-speed="8000" data-endspeed="1000" data-start="100" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird4.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="85" data-y="90" data-speed="8000" data-endspeed="1000" data-start="800" data-end="8400" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird2.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="940" data-y="40" data-speed="8000" data-endspeed="1000" data-start="600" data-end="8800" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird3.png" alt="">
                        </div>
                    </li>
                    
                    <!-- slide 3 -->
                    <li data-transition="papercut">
                        <img src="<?=base_url()?>assets/portal/images/slideshow/sld_imgbg1.jpg" alt="">  
                        <div class="caption general_caption fade" data-x="110" data-y="195" data-speed="400" data-start="700" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree11.png" alt=""></div>                 
                        <div class="caption general_caption fade" data-x="-45" data-y="112" data-speed="400" data-start="100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree10.png" alt=""></div>              
                        <div class="caption general_caption fade" data-x="800" data-y="100" data-speed="400" data-start="400" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree13.png" alt=""></div> 
                        <div class="caption general_caption fade" data-x="747" data-y="-20" data-speed="400" data-start="1100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree12.png" alt=""></div>                   
                        <div class="caption general_caption fade" data-x="15" data-y="10" data-speed="400" data-start="1400" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree14.png" alt=""></div> 
                        
                        <div class="caption general_caption lfb ltb home-media1"  data-x="365" data-y="173" data-speed="900" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                        	<h5 class="smallmargin-bottom">Permintaan Informasi</h5>
                        </div>                        
                        <div class="caption general_caption lfb ltb home-media2"  data-x="365" data-y="213" data-speed="1400" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                        	<div class="slide-separator"></div>
                        </div>
                        <div class="caption general_caption lfb ltb"  data-x="365" data-y="233" data-speed="900" data-endspeed="1800" data-start="800" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                            <p>Informasi yang belum tersedia di DIP,<br /> dapat diperoleh dengan mengajukan Permintaan Informasi</p>
                        </div>
                       <div class="caption general_caption lfb ltb"  data-x="480" data-y="305" data-speed="800" data-endspeed="2000" data-start="1300" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                            <a href="#" class="button-plain small orange">Selengkapnya <span class="plain-arrow">&gt;</span></a>
                        </div>
                        
                        <div class="caption general_caption lft" data-x="350" data-y="25" data-speed="8000" data-endspeed="1000" data-start="100" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird4.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="100" data-y="70" data-speed="8000" data-endspeed="1000" data-start="800" data-end="8400" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird2.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="615" data-y="105" data-speed="8000" data-endspeed="1000" data-start="600" data-end="8800" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird2.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="850" data-y="40" data-speed="8000" data-endspeed="1000" data-start="600" data-end="8800" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird5.png" alt="">
                        </div>
						<div class="caption general_caption lft" data-x="720" data-y="200" data-speed="6000" data-endspeed="1000" data-start="1000" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_padhelpdesk.png" alt="">
                        </div>
                    </li>
					
					<!-- slide 4 -->
                    <li data-transition="papercut">
                        <img src="<?=base_url()?>assets/portal/images/slideshow/sld_imgbg1.jpg" alt="">
                        <div class="caption general_caption fade" data-x="-20" data-y="187" data-speed="400" data-start="700" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree10.png" alt=""></div>                     
                        <div class="caption general_caption fade" data-x="880" data-y="37" data-speed="400" data-start="100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree11.png" alt=""></div>
                        <div class="caption general_caption lft" data-x="620" data-y="100" data-speed="6000" data-endspeed="1000" data-start="1000" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_pad2.png" alt="">
                        </div>
                        <div class="caption general_caption fade" data-x="-75" data-y="305" data-speed="400" data-start="1100" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree12.png" alt=""></div>
                        <div class="caption general_caption fade" data-x="538" data-y="225" data-speed="400" data-start="700" data-easing="easeOutExpo" data-endeasing="easeInExpo"><img src="<?=base_url()?>assets/portal/images/slideshow/object_tree13.png" alt=""></div>  
                        
                        <div class="caption general_caption lfb ltb home-media1"  data-x="219" data-y="173" data-speed="900" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                        	<h5 class="smallmargin-bottom">Laporan Kinerja</h5>
                        </div>                        
                        <div class="caption general_caption lfb ltb home-media2"  data-x="221" data-y="223" data-speed="1400" data-endspeed="900" data-start="550" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                        	<div class="slide-separator"></div>
                        </div>
                        <div class="caption general_caption lfb ltb "  data-x="219" data-y="243" data-speed="900" data-endspeed="1800" data-start="800" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                            <p>Semua informasi yang bisa diakses oleh publik<br />dirangkum dalam Daftar Informasi Publik (DIP)</p>
                        </div>
                       <div class="caption general_caption lfb ltb"  data-x="219" data-y="293" data-speed="800" data-endspeed="2000" data-start="1300" data-end="8500" data-easing="easeOutBack" data-endeasing="easeInBack">
                            <a href="#" class="button-plain small orange">Selengkapnya <span class="plain-arrow">&gt;</span></a>
                        </div>
                        
                        <div class="caption general_caption lft" data-x="555" data-y="155" data-speed="8000" data-endspeed="1000" data-start="100" data-end="8000" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird4.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="85" data-y="90" data-speed="8000" data-endspeed="1000" data-start="800" data-end="8400" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird2.png" alt="">
                        </div>
                        <div class="caption general_caption lft" data-x="940" data-y="40" data-speed="8000" data-endspeed="1000" data-start="600" data-end="8800" data-easing="easeOutElastic" data-endeasing="easeInExpo">
                        	<img src="<?=base_url()?>assets/portal/images/slideshow/object_bird3.png" alt="">
                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
                    
            </div>    	
        </section>
		
 <!-- content section start here -->
        <section class="content-wrapper">    	
            <div class="row">        
                <div class="six column margin-top">
                	<h3 class="margin-bottom"><span class="highlight">Saya ingin melakukan ?</span></h3>
                    <ul class="check">
                      <li><a href="<?=base_url()?>portal/reqinfo">Mengajukan Permohonan Informasi</a></li>                    
                      <li><a href="<?=base_url()?>portal/reqinfo">Mengajukan Keberatan Layanan Informasi</a></li>
                      <li><a href="<?=base_url()?>portal/dip">Melihat Daftar Informasi Publik</a></li>
                      <li><a href="<?=base_url()?>portal/report">Melihat Laporan Pelayanan Informasi</a></li>
                    </ul>
                </div>
                
                <div class="six column margin-top">
                	<ul class="client-box">
                        <li><a href="http://jatimprov.sip-ppid.net/" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client1.png" alt="" class="retina" /></a></li>
                        <li><a href="http://http://ppid.malangkab.go.id/" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client2.png" alt="" class="retina" /></a></li>
                        <li><a href="http://ttukab.sip-ppid.net/" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client3.png" alt="" class="retina" /></a></li>
                        <li><a href="http://ngadakab.sip-ppid.net/" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client4.png" alt="" class="retina" /></a></li>
                        <li><a href="http://ntbprov.sip-ppid.net/" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client5.png" alt="" class="retina" /></a></li>
                        <li><a href="http://ukp.go.id/" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client6.png" alt="" class="retina" /></a></li>
                        <li><a href="http://kominfo.go.id" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client7.png" alt="" class="retina" /></a></li>
                        <li><a href="http://kemendagri.go.id" target="_blank"><img src="<?=base_url()?>assets/portal/images/sample_images/client8.png" alt="" class="retina" /></a></li>
                        <li><a href="http://ppid.kabmerauke.go.id"><img src="<?=base_url()?>assets/portal/images/sample_images/client9.png" alt="" class="retina" /></a></li>              
                    </ul>
                </div>
            </div>            	                 
        </section>
        <!-- content section end here --> 
        
        <!-- featured column section start here -->
        <section class="featured-column bg-border2">
            <div class="row">
            	<div class="twelve column text-center smallmargin-bottom2">
            		<h3>Kami Melayani Permintaan</h3>
                </div>
            
                <div class="three column animated-column text-center mobile-two">
                    <a href="<?=base_url()?>portal/mekanisme" style="color:#3399ff">
					<div class="circle-border small">                           
                        <i class="icon-slidersasc"></i>
                    </div>    
                    <h4>Mekanisme Standar Pelayanan Publik</h4> 
                    <p>Pelayanan Informasi Publik didasari oleh Undang-undang no 14 tahun 2008 dan Peraturan Pemerintah no 61 tahun 2010</p>
					</a>
                </div>            
                <div class="three column animated-column text-center mobile-two">
                    <a href="<?=base_url()?>portal/dip" style="color:#3399ff">
					<div class="circle-border small">                           
                        <i class="icon-iphone"></i>
                    </div>    
                    <h4>Daftar Informasi Publik</h4> 
                    <p>Semua informasi yang bisa diakses oleh publik dirangkum dalam Daftar Informasi Publik (DIP)</p>
					</a>
                </div>            
                <div class="three column animated-column text-center mobile-two">
                    <a href="<?=base_url()?>portal/reqinfo" style="color:#3399ff">
						<div class="circle-border small">                           
							<i class="icon-wallet"></i>
						</div>    
						<h4>Permintaan Informasi</h4> 
						<p>Informasi yang belum tersedia di DIP, dapat diperoleh dengan mengajukan Permintaan Informasi</p>
					</a>
                </div>            
                <div class="three column animated-column text-center mobile-two">
					<a href="<?=base_url()?>portal/report" style="color:#3399ff">
                    <div class="circle-border small">                           
                        <i class="icon-terminal"></i>
                    </div>    
                    <h4>Laporan Kinerja</h4> 
                    <p>Setiap permintaan dan keberatan direkam dalam Laporan Kinerja</p>
					</a>
                </div>
            </div>   
        </section>
        <!-- featured column section end here -->		
        <!-- slideshow end here -->  