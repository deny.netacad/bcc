<section class="content-wrapper">    	
<div class="row">        
<div class="twelve column">
<h3 class="margin-bottom"><span class="highlight">FAQ</span></h3>
<div class="box-full-left-page">
        
        <div class="post-text"><p><strong>Apa PPID itu?</strong></p>
<p>Pejabat Pengelola Informasi dan Dokumentasi yang selanjutnya disebut PPID adalah pejabat yang bertanggung jawab di bidang penyimpanan, pendokumentasian, penyediaan, dan/atau pelayanan informasi di Badan Publik dan bertanggungjawab langsung kepada atasan PPID. Sedangkan Atasan PPID adalah pejabat yang merupakan atasan langsung pejabat yang bersangkutan dan/atau atasan dari atasan langsung pejabat yang bersangkutan. PPID wajib dimiliki oleh setiap badan publik PPID sebagaimana ditetapkan dalam Undang-Undang No. 14 &nbsp;Tahun 2008 tentang Keterbukaan Informasi Publik.</p>
<p>&nbsp;</p>
<p><strong>Apa entitas badan publik?</strong></p>
<p>Entitas badan publik adalah sebuah entitas yang terdiri dari 1 PPID Utama dan beberapa PPID Pembantu. Misalnya entitas badan publik Kabupaten X terdiri atas 1 PPID Utama di tingkat Kabupaten X dan beberapa PPID Pembantu dapat disetiap setiap SKPD (satuan kerja perangkat daerah). Atau misalnya entitas badan publik sebuah kementrian/lembaga terdiri atas 1 PPID Utama di tingkat kementrian/lembaga dan beberapa PPID Pembantu di unit kerja atau komponennya.</p>
<p>&nbsp;</p>
<p><strong>Apa SIP PPID itu?</strong></p>
<p>SIP PPID &nbsp;adalah kependekan dari Sistem Informasi Publik Pejabat Pengelola Informasi dan Dokumentasi. SIP PPID adalah sistem informasi untuk pengelolaan dan pelayanan informasi yang dijalankan oleh PPID dan terintegrasi dalam sebuah entitas badan publik dan dijalankan melalui komputasi awan (cloud computing).</p>
<p>&nbsp;</p>
<p><strong>Platform apakah yang digunakan SIP PPID?</strong></p>
<p>Ada 2 model flatform SIP PPID, berbasis web dan hibrid. Flatform berbasis web dapat dijalankan oleh PPID yang insfrastruktur jaringan internetnya baik serta jarak antara PPID Utama dan PPID Pembantu tidak terkendala jarak maupun jaringan internet. Sedangkan flatform hibrid dapat dijalankan oleh PPID dengan insfrastruktur jaringan internetnya kurang baik dan serta jarak antara PPID Utama dan PPID Pembantu terkendala jarak maupun jaringan internet.</p>
<p>SIP PPID berbasis web berjalan secara online melalui website yang dapat diakses melalui berbagai macam platform melalui jaringan internet dengan menggunakan salah satu web browser. SIP PPID offline dijalankan dengan antar muka desktop.</p>
<p>Pilihan flatform ini dilakukan oleh PPID sesuai karakteristik dan dukungan jaringan internet yang dimiliki.</p>
<p>&nbsp;</p>
<p><strong>Bagaimana diagram konteks SIP PPID?</strong></p>
<p>SIP PPID mengolah proses-proses pengumpulan, pendokumentasian dan pengarsipan, publikasi dan pelayanan informasi yang dilakukan oleh PPID dan menyajikannya kepada publik. Diagram konteks SIP PPID digambarkan sebagai berikut:</p>
<p></p>
<p>&nbsp;</p>
<p><strong>Bagaimana SIP PPID bekerja?</strong></p>
<p>SIP PPID membantu PPID dalam pengumpulan, pendokumentasian dan pengarsipan, publikasi dan pelayanan informasi terintegrasi dalam sebuah entitas badan publik. Artinya proses-proses pengumpulan, pendokumentasian dan pengarsipan, publikasi dan pelayanan informasi dilakukan secara terintegrasi antara PPID Pembantu/PPID unit kerja ke PPID Utamanya.</p>
<p>SIP PPID menggunakan pendekatan berdasarkan levelitas dimana �Provinsi� akan menjadi penghubung untuk PPID-PPID di level kabupaten dan kota dibawahnya. PPID-PPID di level kabupaten dan kota secara realtime akan mengirimkan laporan operasional kepada SIP PPID Provinsi. Selanjutnya SIP PPID akan melakukan sinkronisasi akan mengirimkannya ke SIP PPID Kemendagri. Pada akhirnya proses-proses PPID akan terakumulasi di SIP PPID Kemendagri.</p>
<p>Setiap entitas PPID (kabupaten, kota dan provinsi) akan berhubungan melalui cloud aplikasi website. Website akan melakukan sinkronisasi di entitas PPID akan melakukan sinkronisasi dari aplikasi offine di SKPD, unit kerja atau kompinen dibawahnya. Melalui cloud aplikasi website juga entitas PPID (kabupaten, kota dan provinsi) akan terhubung dengan SIP PPID Kemendagri.&nbsp;Untuk PPID yang menjalankan SIP PPID dengan flatform berbasis web, maka semua proses akan dilakukan melalui website SIP PPID.&nbsp;</p>
<p>Diagram alur dibawah dapat menjelaskan bagaimana posisi aplikasi offline dan posisi SIP PPID menurut levelitasnya.</p>
<p></p>
<p>&nbsp;</p>
<p><strong>Apakah operasional SIP PPID membutuhkan internet 24 jam?&nbsp;</strong></p>
<p>Untuk operasional SIP PPID dengan flatform hibrid tidak membutuhkan internet yang berjalan 24 jam penuh. Aplikasi offline dapat bekerja penuh dalam pengumpulan, pendokumentasian dan pengarsipan, publikasi dan pelayanan informasi. Internet dibutuhkan �hanya� saat sinkronisasi aplikasi offline ke aplikasi web.</p>
<p>Untuk PPID yang menjalankan SIP PPID dengan flatform berbasis web, maka semua proses akan dilakukan membutuhkan jaringan internet.&nbsp;</p>
<p>&nbsp;</p>
<p>Apa manfaat umum yang dapat diperoleh dari implementasi SIP PPID?</p>
<p>- Memudahkan PPID dalam proses pengumpulan, pendokumentasian, pelayanan dan pelaporan hasil pelayanan informasi badan publik, sehingga menjadi lebih efektif dan efisien.&nbsp;</p>
<p>- Mempercepat respon PPID terhadap permohonan informasi dan permohonan keberatan.&nbsp;</p>
<p>- Membantu masyarakat dalam mendapatkan pelayanan informasi secara mudah&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Untuk memohon informasi apakah dikenakan biaya?</strong></p>
<p>Sebagaimana ditetapkan dalam UU KIP maka permohonan informasi oleh masyarakat melalui SIP PPID dilakukan dengan prinsip cepat dan tepat waktu, biaya ringan, dan cara sederhana. Dengan SIP PPID permohonan informasi dilakukan secara gratis. (atau dengan ketentuan lain yang bersifat khusus)</p>
<p>&nbsp;</p>
<p><strong>Apakah pemanfaatan SIP PPID oleh PPID?</strong></p>
<p>Secara prinsip aplikasi SIP PPID dapat dimanfaatkan oleh PPID-PPID secara mudah dan implementasinya dilakukan secara cepat. Untuk aplikasi offline dapat diinstallkan melalui CD installer yang dapat diperoleh secara gratis.&nbsp;</p>
<p>&nbsp;</p></div>
        <!-- post-text end -->
</div>
    </div>
</div>
</section>