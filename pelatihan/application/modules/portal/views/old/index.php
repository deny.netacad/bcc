<!DOCTYPE html>
<!--[if IE 8 ]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>
<meta charset="utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>SIP PPID Kabupaten Sampang</title>

<!-- ////////////////////////////////// -->
<!-- //     Retina Bookmark Icon     // -->
<!-- ////////////////////////////////// -->
<link rel="apple-touch-icon-precomposed" href="apple-icon.png"/>

<!-- ////////////////////////////////// -->
<!-- //     Retina Bookmark Icon     // -->
<!-- ////////////////////////////////// -->
<link rel="apple-touch-icon-precomposed" href="apple-icon.png"/>

<!-- ////////////////////////////////// -->
<!-- //      Stylesheets Files       // -->
<!-- ////////////////////////////////// -->
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/style.css"/>
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/media.css"/>
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/revolution.css" media="screen"/>
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/media-slideshow.css" media="screen"/>
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/noscript.css" media="screen,all" id="noscript"/>

<!-- ////////////////////////////////// -->
<!-- //     Google Webfont Files     // -->
<!-- ////////////////////////////////// -->
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/font.css"/>
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/font2.css"/>
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/font3.css"/>
<link rel="stylesheet" href="<?=base_url()?>assets/portal/css/font4.css"/>

<!-- ////////////////////////////////// -->
<!-- //        Favicon Files         // -->
<!-- ////////////////////////////////// -->
<link rel="shortcut icon" href<?=base_url()?>assets/portal/images/favicon.ico"/>

<!-- ////////////////////////////////// -->
<!-- //      Javascript Files        // -->
<!-- ////////////////////////////////// -->

<script src="<?=base_url()?>assets/js/jquery.min172.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.easing-1.3.min.js"></script>
<script src="<?=base_url()?>assets/portal/js/masonry.min.js"></script>
<script src="<?=base_url()?>assets/portal/js/modernizr.js"></script>
<script src="<?=base_url()?>assets/portal/js/superfish.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.parallax-1.1.3.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.li-scroller.1.0.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.themepunch.plugins.min.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?=base_url()?>assets/portal/js/mediaelement-and-player.min.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.fancybox.js?v=2.0.6"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.fancybox-media.js?v=1.0.3"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.donutchart.js"></script>
<script src="<?=base_url()?>assets/portal/js/jflickrfeed.min.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.twitter.js"></script>
<script src="<?=base_url()?>assets/portal/js/accordion-functions.js" ></script>
<script src="<?=base_url()?>assets/portal/js/theme-functions.js"></script>
<script src="<?=base_url()?>assets/portal/js/jquery.retina.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.min.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.js"></script>
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1040,
startheight:463,
hideThumbs:0,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:1040,
soloArrowLeftVOffset:259,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:90,
soloArrowRightVOffset:259,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});	
</script>
<script>
$(window).load(function() {
//Masonry
$('#pf-wrapper2').masonry({
itemSelector : '.item',
columnWidth: 1,
});	

});
</script>

<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>
<body>
	<div id="box-wrapper">
        <!-- header start here -->
        <header>
                
            <!-- top info start here -->
            <div id="top-info">
                <div class="row">
                    <div class="twelve column top-desc">
						
                        <ul class="topmenu">
                            <li class="tablet-hide"><i class="icon-checkin"></i><span>Jl. Raya Pleyang 1A, SAMPANG, 69216. Indonesia</span></li>
                            <li><i class="icon-phonealt"></i><span>(0323)-326135</span></li>
                            <li class="mobile-hide"><a  href="mailto:ppid@sampangkab.go.id"><i class="icon-envelope"></i><span>ppid@sampangkab.go.id</span></a></li>
							<?php
							if($this->session->userdata('is_login_pelatihanuser')){
							?>
							<li><a href="<?=base_url()?>portal/profil" title="Profil Pengguna"><i class="icon-user"></i> Hi, <?=$this->session->userdata('username')?></a></li>
							<?php
							}
							?>
                        </ul>
						
                    </div>
                </div>
            </div>
            <!-- top info end here -->
            
            <div class="row">
                <div class="twelve column">
                
                    <div id="left-header">
                        <!-- logo start here -->
                        <div id="logo">
                            <a href="<?=base_url()?>"><img src="<?=base_url()?>assets/portal/images/logo-ppid.png" alt="main-logo" class="retina"/></a>
                        </div>           
                        <!-- logo end here -->
                    </div>
                    
                    <div id="right-header">
                        <div id="mainmenu-wrapper">
                            <div class="left-menu">
                                    
                                <!-- navigation start here -->
                                <nav id="mainmenu">
                                    <ul id="menu">
                                        <li class="dropdown selected"><a href="<?=base_url()?>">Home</a></li> 
                                        <li class="dropdown"><a href="#">PPID Kab. Sampang</a>
                                            <ul> 
                                                <li><a href="<?=base_url()?>portal/visimisi">Visi dan Misi</a></li>
                                                <li><a href="<?=base_url()?>portal/tugaskewenangan">Tugas dan Kewenangan</a></li>
                                                <li><a href="<?=base_url()?>portal/strukturppid">Struktur PPID</a></li>
                                            </ul>
                                        </li>                               
                                        <li class="dropdown"><a href="#">Proses Bisnis SIP PPID</a>
                                            <ul> 
                                                <li><a href="<?=base_url()?>portal/prosesbisnis">Proses Dokumentasi Informasi</a></li>
                                                <li><a href="<?=base_url()?>portal/permohonanonline">Permohonan Informasi Melalui Website/Online</a></li>
                                                <li><a href="<?=base_url()?>portal/permohonanoffline">Permohonan Informasi Melalui Desktop/Offline</a></li>
                                                <li><a href="<?=base_url()?>portal/permohonanskpdlain">Permohonan Informasi Melalui SKPD Lain</a></li>
                                            </ul>
                                        </li>                                        
										<!--<li class="dropdown"><a href="#">Berita</a></li>-->
                                        <li class="dropdown"><a href="<?=base_url()?>portal/faq">FAQ</a></li>
                                        <li><a href="<?=base_url()?>portal/kontak">Kontak</a></li>
										<li class="dropdown"><a href="<?=base_url()?>portal/register">Daftar</a></li>
										<?php
										if($this->session->userdata('is_login_pelatihanuser')){
										?>
										<li class="dropdown"><a href="#"><i class="icon-user"></i></a>
                                            <ul> 
												<li><a href="<?=base_url()?>portal/profil" title="Profil Pengguna">Profil <?=$this->session->userdata('username')?></a></li>
												<li><a href="<?=base_url()?>portal/gantipass" title="Ganti Password">Ganti Password</a></li>
												<li><a href="<?=base_url()?>portal/logout" title="Logout"><i class="icon-off"></i> Logout</a></li>
                                            </ul>
                                        </li>    
										<?php
										}else{
										?>
										<li class="dropdown"><a href="<?=base_url()?>portal/masuk">Masuk</a></li>
										<?php
										}
										?>
                                    </ul>
                                </nav>
                                <!-- navigation end here --> 
                                    
                            </div>
                            
                            <div class="right-menu" id="top-search">
                                <a class="trigger" href="#"><i class="icon-search"></i></a>
                                <div class="search-panel">
                                    <ul>
                                        <li>
                                        <form id="search" action="#" method="get">
                                            <fieldset class="search-fieldset">
                                                <input type="text" id="search-form" value="type and hit enter" onblur="if (this.value == ''){this.value = 'type and hit enter'; }" onfocus="if (this.value == 'type and hit enter') {this.value = ''; }" />
                                            </fieldset>      						
                                        </form>
                                        </li>
                                    </ul>
                                </div>
								
								
                            </div>                  
                            
                        </div>
                        <?php
						if(!isset($page)){
						?>
                        <div id="slogan-wrapper" class="text-center">
                            <h3 class="smallmargin-bottom">Sistem Informasi Publik Pejabat Pengelola Informasi dan Dokumentasi</h3>
                            <p class="lead mobile-hide">Selamat datang di portal resmi SIP PPID Kabupaten Sampang</p>
                        </div>
						<?php
						}else{
							$this->load->view("portal/".$page."_header");	
						}
						?>
                        
                    </div>
                
                </div>
            </div>    
                    
        </header>
        <!-- header end here -->
        
           
       <?php
	   if(!isset($page)){
			$this->load->view("portal/default");
	   }else{
			$this->load->view("portal/".$page);
	   }
	   ?>   
       
        
        <!-- footer start here -->
        <footer>      
            <div class="row">
                <div class="eight column">
                	<ul class="no-bullet footer-list smallmargin-top2">
                    	<li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms and Conditions</a></li>
                        <li><a href="#">Aturan Umum</a></li>
                        <li><a href="#">Sitemap</a></li>
                        <li class="link-box"><a href="mailto:ppid@sampangkab.go.id"><i class="icon-postalt"></i>ppid@sampangkab.go.id</a></li>
                    </ul>
                    <p>&copy; 2014. All rights reserved. Developed by Agmantara.</p>                    
                </div>
                
                <div class="four column">
                	<ul class="social-list circle-social">
                        <li><a href="#"><i class="social-facebook"></i></a></li>
                        <li><a href="#"><i class="social-twitter"></i></a></li>
                        <li><a href="#"><i class="social-dribbble"></i></a></li>
                        <li><a href="#"><i class="social-linkedin"></i></a></li>
                    </ul>    
                </div>
            </div>        
        </footer>
        <!-- footer end here -->
    </div>

<script>$('#noscript').remove();</script>
</body>
</html>