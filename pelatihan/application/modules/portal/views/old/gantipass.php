
<script>
$(document).ready(function(){
	
	$("#form1").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form1').trigger('reset');
			});		
		}else{
			
			$('#form1').trigger('reset');
		}
	});
	
	$('#form1').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
});
</script>
<section class="content-wrapper">       
            <div class="row">
                <div class="twelve column">
                    <ul class="tabs">
                        <li class="active"><a href="#tab-sample1">Ganti Password</a></li>
                    </ul>
                    <div class="tab_container">
                        <div id="tab-sample1" class="tab_content" style="display: block;">                            
							<div id="contact-form-area">
								<!-- Contact Form Start //-->
								<?php
								$data['username'] = $this->session->userdata('username');
								$rs = $this->db->get_where("ref_applicant",$data);
								$item = $rs->row();
								?>
								<form id="form1" name="form1" action="<?=base_url()?>portal/updatepassRegister" class="contactform" method="post" enctype="multipart/form-data">
									<fieldset> 
									<label>Password Sekarang <em>*</em></label> 
									<input type="password" name="userpass" class="textfield" id="userpass" value="">                        
									<label>Password Baru <em>*</em></label> 
									<input type="password" name="userpass2" class="textfield" id="userpass2" value="">                        
									<label>Konfirmasi Password Baru <em>*</em></label> 
									<input type="password" name="userpass3" class="textfield" id="userpass3" value="">                                               
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<button type="submit" name="submit" class="buttoncontact" id="buttonsend">Simpan</button>
									<span class="loading" style="display: none;">Please wait..</span>
									<div class="clear"></div>
									</fieldset> 
								</form>
								<!-- Contact Form End //-->
							</div>                            
                        </div>
                    </div>	
                </div>              
                
                <div class="twelve column">
                    <hr>
               </div>
               
            </div>             
        </section>