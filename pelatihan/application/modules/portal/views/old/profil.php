<link rel="stylesheet" href="<?=$def_js?>plugins/jquery-file-upload-9.8/css/jquery.fileupload.css">
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.iframe-transport.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload-9.8/js/jquery.fileupload.js"></script>
<script>
$(document).ready(function(){
	 $('#form1 #scanfile').fileupload({
		dataType: 'json',
		formData: { 'filename':'<?=date('YmdHis')?>' },
		done: function (e, data) {
			
			try{
			var vfilepath = data.result.upload_data.full_path;
			var vfilename = data.result.upload_data.file_name;
			var vhtml = '<div class="clear"></div>'+
			'<div><label>&nbsp;</label><div><input type="hidden" name="scanfile[]" value="'+vfilename+'" />'+vfilename+' <a class="delthis" data="'+vfilepath+'" href="#">[x]</a></div></div>';
			
			$('#form1 #progress .bar').after(vhtml);
			$('#form1 #progress .bar').html('');
			$('#form1 #progress .bar').css('width','0%');
			}catch(e){ alert(e.message); }
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#form1 #progress .bar').css(
				'width',
				progress + '%'
			);
			
		}
	});
	
	$('#form2 #scanfile').fileupload({
		dataType: 'json',
		formData: { 'filename':'<?=date('YmdHis')?>' },
		done: function (e, data) {
			
			try{
			var vfilepath = data.result.upload_data.full_path;
			var vfilename = data.result.upload_data.file_name;
			var vhtml = '<div class="clear"></div>'+
			'<div><label>&nbsp;</label><div><input type="hidden" name="scanfile[]" value="'+vfilename+'" />'+vfilename+' <a class="delthis" data="'+vfilepath+'" href="#">[x]</a></div></div>';
			
			$('#form2 #progress .bar').after(vhtml);
			$('#form2 #progress .bar').html('');
			$('#form2 #progress .bar').css('width','0%');
			}catch(e){ alert(e.message); }
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#form2 #progress .bar').css(
				'width',
				progress + '%'
			);
			
		}
	});
	
	$(document).on('click','a.delthis',function(){
		if(confirm('Hapus file?')){
			var $this = $('a.delthis');
			var index = $this.index($(this));
			
			var vdata = $this.eq(index).attr('data');
			$.ajax({
				type:'post',
				url:'<?=base_url()?>portal/delUpload',
				data:{ 'full_path': vdata },
				beforeSend:function(){
					
				},
				success:function(response){
					var ret = $.parseJSON(response);
					alert(ret.text);
					$this.eq(index).parent().parent().remove();
				}
			});
		}
	});
	
	$("#form1").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form1').trigger('reset');
			});		
		}else{
			
			$('#form1').trigger('reset');
		}
	});
	
	$('#form1').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
	$("#form2").submit(function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form2').trigger('reset');
			});		
		}else{
			
			$('#form2').trigger('reset');
		}
	});
	
	$('#form2').on('reset',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('a.delthis').parent().parent().remove();
	});
	
	$('#myTab li a').on('click',function(e){
		switch($(this).attr('href')){
			case "#tab-sample2":
				refreshData2();
			break;
			case "#tab-sample3":
				refreshData3();
			break;
			case "#tab-sample4":
				refreshData4();
			break;
		}
	});
	
	$(document).on('click','#tab-sample2 a.ajax-replace', function(){
		$.ajax({
			type:'post',
			url:$(this).attr('href'),
			data:{ 'per_page':$('#tab-sample2 #per_page').val(),'cari':$('#tab-sample2 #cari').val() },
			beforeSend:function(){
				
			},
			success:function(response){
				$('#tab-sample2 #result2').html(response);
			}
		});
		return false;
	});
	
	$(document).on('click','a.dxownloadfile',function(){
		var $this = $('a.dxownloadfile');
		var index = $this.index($(this));
		$.ajax({
			type:'post',
			url:'<?=base_url()?>portal/page/downloadfile',
			data:{ 'dip_id':$this.eq(index).attr('data') },
			beforeSend:function(){
			},
			success:function(response){
			
			}
		});
	});
	
});

function refreshData2(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/profil_his_dip',
		data:{ 'per_page':$('#tab-sample2 #per_page').val(),'cari':$('#tab-sample2 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result2').html(response);
		}
	});
}

function refreshData3(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/profil_req_info',
		data:{ 'per_page':$('#tab-sample3 #per_page').val(),'cari':$('#tab-sample3 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result3').html(response);
		}
	});
}

function refreshData4(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>portal/page/data/profil_keberatan',
		data:{ 'per_page':$('#tab-sample4 #per_page').val(),'cari':$('#tab-sample4 #cari').val() },
		beforeSend:function(){
			
		},
		success:function(response){
			$('#result4').html(response);
		}
	});
}
</script>
<section class="content-wrapper">       
            <div class="row">
                <div class="twelve column">
                    <ul class="tabs" id="myTab">
                        <li class="active"><a href="#tab-sample1">Profil Pemohon</a></li>
                        <li class=""><a href="#tab-sample2">Riwayat Dokumen</a></li>
						<li class=""><a href="#tab-sample3">Permintaan Informasi</a></li>
						<li class=""><a href="#tab-sample4">Pengajuan Keberatan</a></li>
                    </ul>
                    <div class="tab_container">
                        <div id="tab-sample1" class="tab_content" style="display: block;">                            
							<div id="contact-form-area">
								<!-- Contact Form Start //-->
								<?php
								if($this->session->userdata('is_login_pelatihanuser')){
									$data['username'] = $this->session->userdata('username');
								}else{
									$data['username'] = '';
									redirect('portal/masuk');
								}
								$rs = $this->db->get_where("ref_applicant",$data);
								$item = $rs->row();
								?>
								<form id="form1" name="form1" action="<?=base_url()?>portal/updateRegister" class="contactform" method="post" enctype="multipart/form-data">
									<fieldset> 
									<label>Kartu Pengenal <em>*</em></label>                           
									<select name="identity" id="identity" class="textfield" required>
										<option value="1" <?=(($item->identity==1)?"selected":"")?>>KTP</option>
										<option value="2" <?=(($item->identity==2)?"selected":"")?>>SIM</option>
										<option value="3" <?=(($item->identity==3)?"selected":"")?>>PASSPORT</option>
										<option value="4" <?=(($item->identity==4)?"selected":"")?>>KTM</option>
									</select> 
									<label>No. Identitas <em>*</em></label> 
									<input type="text" name="noid" class="textfield" id="noid" value="<?=$item->noid?>" required />                        
									<label>Scan File Identitas <em>*</em></label>
									<span class="btn btn-success fileinput-button" style="float:left;cursor:pointer">
										<span style="cursor:pointer">Add files...</span>
										<!-- The file input field used as target for the file upload widget -->
										<input type="file" name="scanfile" class="textfield" id="scanfile" value="" data-url="<?=base_url()?>portal/do_upload_scanfile" />
									</span>
									<div class="clear"></div>
									
									<label>&nbsp;</label>
									<div id="progress">
										<div class="bar textfield" style="width: 0%;color:orange"></div>
									</div>
									<?php
									if($item->scanfile!=''){
										$path = BASEPATH."../upliddir";
										echo '<div class="clear"></div>'
											.'<div><label>&nbsp;</label><div><input type="hidden" name="scanfile[]" value="'.$item->scanfile.'" />'
											.'<a href="'.base_url().'upliddir/'.$item->scanfile.'" target="_blank">'.$item->scanfile.'</a> <a class="delthis" data="'.$path.'/'.$item->scanfile.'" href="#">[x]</a></div></div>';
									}
									?>
									<label>Username <em>*</em></label> 
									<input type="text" name="username" class="textfield" id="username" value="<?=$item->username?>" required />                        
									<label>Nama Lengkap <em>*</em></label> 
									<input type="text" name="fullname" class="textfield" id="fullname" value="<?=$item->fullname?>">                        
									<label>Jenis Kelamin <em>*</em></label> 
									<select name="sex">
										<option value="1" <?=(($item->sex==1)?"selected":"")?>>Laki-Laki</option>
										<option value="2" <?=(($item->sex==2)?"selected":"")?>>Perempuan</option>
									</select>
									<label>Tempat Lahir <em>*</em></label> 
									<input type="text" name="pob" class="textfield" id="pob" value="<?=$item->pob?>">                        
									<label>Tanggal Lahir <em>*</em></label> 
									<input type="text" name="dob" class="textfield" id="dob" value="<?=$item->dob?>">                        
									<label>Pendidikan Terakhir <em>*</em></label> 
									<select name="education">
										<option value="1" <?=(($item->education==1)?"selected":"")?>>SD</option>
										<option value="2" <?=(($item->education==2)?"selected":"")?>>SMP</option>
										<option value="3" <?=(($item->education==3)?"selected":"")?>>SMA</option>
										<option value="4" <?=(($item->education==4)?"selected":"")?>>Diploma</option>
										<option value="5" <?=(($item->education==5)?"selected":"")?>>Sarjana</option>
										<option value="6" <?=(($item->education==6)?"selected":"")?>>Pasca Sarjana</option>
									</select>
									<label>Pekerjaan <em>*</em></label> 
									<select name="job">
										<option value="1" <?=(($item->job==1)?"selected":"")?>>Wiraswasta</option>
										<option value="2" <?=(($item->job==2)?"selected":"")?>>Buruh</option>
										<option value="3" <?=(($item->job==3)?"selected":"")?>>Petani</option>
										<option value="4" <?=(($item->job==4)?"selected":"")?>>Nelayan</option>
										<option value="5" <?=(($item->job==5)?"selected":"")?>>PNS</option>
										<option value="6" <?=(($item->job==6)?"selected":"")?>>Swasta</option>
										<option value="7" <?=(($item->job==7)?"selected":"")?>>Pedagang</option>
										<option value="8" <?=(($item->job==8)?"selected":"")?>>Pekerja rumah tangga</option>
										<option value="9" <?=(($item->job==9)?"selected":"")?>>LSM</option>
										<option value="10" <?=(($item->job==10)?"selected":"")?>>Tenaga Pendidik</option>
										<option value="11" <?=(($item->job==11)?"selected":"")?>>Pengacara</option>
										<option value="12" <?=(($item->job==12)?"selected":"")?>>Tenaga Kesehatan</option>
										<option value="13" <?=(($item->job==13)?"selected":"")?>>Peneliti</option>
										<option value="14" <?=(($item->job==14)?"selected":"")?>>Seniman</option>
										<option value="15" <?=(($item->job==15)?"selected":"")?>>Wartawan</option>
										<option value="16" <?=(($item->job==16)?"selected":"")?>>Mahasiswa/Pelajar</option>
										<option value="17" <?=(($item->job==17)?"selected":"")?>>Lain-lain</option>
									</select>
									<label>Alamat <em>*</em></label>
									<textarea name="addr" id="addr" class="textarea" cols="2" rows="6"><?=$item->addr?></textarea>
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<label>Kota <em>*</em></label> 
									<input type="text" name="city" class="textfield" id="city" value="<?=$item->city?>">                        
									<label>Kode POS <em>*</em></label> 
									<input type="text" name="postcode" class="textfield" id="postcode" value="<?=$item->postcode?>">                        
									<label>Email <em>*</em></label> 
									<input type="email" name="email" class="textfield" id="email" value="<?=$item->email?>">                        
									<label>Telp. Rumah <em>*</em></label> 
									<input type="text" name="phone_home" class="textfield" id="phone_home" value="<?=$item->phone_home?>">                        
									<label>Telp. Mobile <em>*</em></label> 
									<input type="text" name="phone_mobile" class="textfield" id="phone_mobile" value="<?=$item->phone_mobile?>">                        
									<div class="clear"></div> 
									<label>&nbsp;</label>
									<button type="submit" name="submit" class="buttoncontact" id="buttonsend">Submit</button>
									<span class="loading" style="display: none;">Please wait..</span>
									<div class="clear"></div>
									</fieldset> 
								</form>
								<!-- Contact Form End //-->
							</div>                            
                        </div>
                                                            
                        <div id="tab-sample2" class="tab_content" style="display: none;">                            
                            <div class="twelve"> 
								<input type="text" id="cari" name="cari" class="six" placeHolder="Cari Judul/Kategori/Ringkasan/Kode Dokumen/Penanggungjawab..." />
								<select id="per_page" name="per_page" style="width:60px">
								  <option value="">-</option>
								  <option value="4">4</option>
								  <option value="25">25</option>
								  <option value="50">50</option>
								</select>
							</div>
							<div class="twelve" id="result2" style="overflow-y:auto"> 
								 
							</div>
                        </div>
						<div id="tab-sample3" class="tab_content" style="display: none;">                            
                            <div class="twelve"> 
								<input type="text" id="cari" name="cari" class="six" placeHolder="Cari ..." />
								<select id="per_page" name="per_page" style="width:60px">
								  <option value="">-</option>
								  <option value="4">4</option>
								  <option value="25">25</option>
								  <option value="50">50</option>
								</select>
							</div>
							<div class="twelve" id="result3" style="overflow-y:auto"> 
								 
							</div>
                        </div>
						<div id="tab-sample4" class="tab_content" style="display: none;">                            
                            <div class="twelve"> 
								<input type="text" id="cari" name="cari" class="six" placeHolder="Cari ..." />
								<select id="per_page" name="per_page" style="width:60px">
								  <option value="">-</option>
								  <option value="4">4</option>
								  <option value="25">25</option>
								  <option value="50">50</option>
								</select>
							</div>
							<div class="twelve" id="result4" style="overflow-y:auto"> 
								 
							</div>
                        </div>
                                                                                                
                    </div>	
                </div>              
                
                <div class="twelve column">
                    <hr>
               </div>
               
            </div>             
        </section>