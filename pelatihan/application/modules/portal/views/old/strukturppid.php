<!-- content section start here -->
        <section class="content-wrapper">    	
            <div class="row">        
                <div class="column">
                	<h3 class="margin-bottom"><span class="highlight">STRUKTUR PPID</span></h3>
                    <p class="lead"></p>
                    
                    <p>
					Struktur PPID

					<table>
						<tr>
							<td>NO.</td>
							<td STYLE="width:300px">JABATAN DALAM TIM</td>
							<td STYLE="min-width:500px">KETERANGAN</td>
						</tr>
						<tr>
							<td>1.</td>
							<td>Atasan PPID</td>
							<td>Sekretaris Daerah</td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Ketua</td>
							<td>Kepala Dinas Perhubungan, Komunikasi dan Informatika  Kabupaten Sampang.</td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Sekretaris</td>
							<td>Kepala  Bidang Komunikasi dan Informatika pada Dinas Perhubungan, Komunikasi dan Informatika Kabupaten Sampang.</td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Bidang-bidang :
							<ul>
								<li>Bidang Dokumentasi dan Informasi</li>
								<li>Bidang Pengolah Data dan Klasifikasi Informasi</li>
								<li>Bidang Penyelesaian Sengketa Informasi</li>
							</ul>
							</td>
							<td>
							<br />
							Kepala Bagian Hubungan Masyarakat Sekretariat Daerah Kabupaten Sampang;
							<br />
							Kepala Bagian Pengelola Data Elektronik  Sekretariat Daerah Kabupaten Sampang;
							<br />
							Kepala Bagian Hukum Sekretariat Daerah Kabupaten Sampang;
							
							</td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Anggota</td>
							<td>Pejabat Pengelola Informasi Dan Dokumentasi(PPID) Pembantu seluruh Satuan Kerja Perangkat Daerah Kabupaten Sampang.</td>
						</tr>
					</table>					
					</p>
                    	
                </div>
            </div>            	                 
        </section>
        <!-- content section end here --> 