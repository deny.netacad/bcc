<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Webservices extends MY_Controller {
	
	var $modules = "portal";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index(){
		redirect('/');
	}
	
	function getSkpd(){
		$rs = $this->db->query("SELECT a.skpd_id,a.skpd_name FROM ref_skpd a");
		$arr['skpd'] = $rs->result();
		header('Content-type: application/json');
		echo json_encode($arr);
	}
	
	function getDocument(){
		$rs = $this->db->query("SELECT a.*,b.dip_cat FROM ref_dip a
		INNER JOIN ref_dip_cat b ON a.dip_catid=b.dip_catid");
		$arr['document'] = $rs->result();
		header('Content-type: application/json');
		echo json_encode($arr);
	}
	
	function getUserPassword($user_name,$user_pass,$skpd_id){
		$data['user_name'] = $user_name;
		$data['user_pass'] = md5($user_pass);
		$data['skpd_id'] = $skpd_id;
		$rs = $this->db->get_where("ref_user",$data);
		return $rs;
	}
	
	function ah_login_api(){
		if (isset($_POST['tag']) && $_POST['tag'] != '') {
			// get tag
			$tag = $_POST['tag'];

			// response Array
			$response = array("tag" => $tag, "success" => 0, "error" => 0);

			// check for tag type
			if ($tag == 'login') {
				// Request type is check Login
				$user_name = $_POST['user_name'];
				$user_pass = $_POST['user_pass'];
				$skpd_id = $_POST['skpd_id'];

				// check for user
				$rs = $this->getUserPassword($user_name, $user_pass,$skpd_id);
				if ($rs->num_rows>0) {
					$user = $rs->row();
					// user found
					// echo json with success = 1
					$response["success"] = 1;
					$response["user_id"] = $user->user_id;
					$response["user"]["user_name"] = $user->user_name;
					$response["user"]["user_id"] = $user->user_id;
					$response["user"]["user_pass"] = $user->user_pass;
					$response["user"]["user_role"] = $user->user_role;
					$response["user"]["skpd_id"] = $user->skpd_id;
					$response["user"]["created_at"] = $user->createon;
					$response["user"]["updated_at"] = $user->createon;
					echo json_encode($response);
				} else {
					// user not found
					// echo json with error = 1
					$response["error"] = 1;
					$response["error_msg"] = "Incorrect email or password!";
					echo json_encode($response);
				}
			} else {
				echo "Invalid Request";
			}
		} else {
			echo "Access Denied";
		}
		
		
	}
	
	
}