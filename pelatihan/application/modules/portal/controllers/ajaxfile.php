<?php

class Ajaxfile extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function cariloket(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " (
		nama_loket like '%".strtolower($keyword)."%' or 
		id like '%".strtolower($keyword)."%'
		)
		";
		$q = "select * from loket where $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q order by id limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function carijentiket(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " (
		jenis_tiket like '%".strtolower($keyword)."%' or 
		id like '%".strtolower($keyword)."%'
		)
		";
		$q = "select * from jenis_tiket where $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q order by id limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function carinotiket(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " (
		no_tiket like '%".strtolower($keyword)."%'
		)
		";
		$q = "select * from t_tiketing where $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q order by no_tiket desc limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
		
	}
	
	function cariban(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " (
		no_ban like '%".strtolower($keyword)."%'
		)
		";
		$q = "select * from ban where $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q order by no_ban desc limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
		
	}
	
	function caribaju(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " (
		no_baju like '%".strtolower($keyword)."%'
		or ukuran like '%".strtolower($keyword)."%'
		or sex like '%".strtolower($keyword)."%'
		)
		";
		$q = "select *,if(sex=1,'Laki-laki','Perempuan') as sex from baju where $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q order by no_baju desc limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function cariloker(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " (
		no_loker like '%".strtolower($keyword)."%'
		or letak_loker like '%".strtolower($keyword)."%'
		)
		";
		$q = "select * from loker where $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q order by no_loker desc limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	
}
?>