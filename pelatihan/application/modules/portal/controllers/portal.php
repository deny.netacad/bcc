<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Portal extends MY_Controller {
	var $modules = "portal";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index(){
		$this->load->view('index',$this->data);
	}
	
	function foo(){
		echo "test";
	}
	
		
	function sendRegister(){
		$keynot = array("","scanfile","submit","userpass2");
		$data = array();
		foreach($_POST as $key=>$value){
			if(array_search($key,$keynot)==''){
				$data[$key] = $value;
			}
		}
		
		$data2['userpass'] = $this->input->post('userpass2');
		$ret = array();
		
		if($data['userpass']!=$data2['userpass']){
			$ret['text'] = "Konfirmasi Password tidak cocok";
			echo json_encode($ret);
		}else{
			$data['scanfile'] = $_POST['scanfile'][0];
			
			if(!$this->db->insert("ref_applicant",$data)){
				$ret['text'] = "Pendaftaran tidak berhasil".$this->db->_error_message();
				echo json_encode($ret);
			}else{
				$ret['text'] = "Pendaftaran berhasil";
				echo json_encode($ret);
			}

		}
		
		
	}
	
	function updateRegister(){
		$dt['username'] = $this->session->userdata('username');
		$keynot = array("","scanfile","submit","userpass2");
		$data = array();
		foreach($_POST as $key=>$value){
			if(array_search($key,$keynot)==''){
				$data[$key] = $value;
			}
		}
		$data['scanfile'] = $_POST['scanfile'][0];
		if(!$this->db->update("ref_applicant",$data,$dt)){
			$ret['text'] = "Update Profil tidak berhasil\n".$this->db->_error_message();
			echo json_encode($ret);
			exit;
		}else{
			$ret['text'] = "Update Profil Berhasil";
			echo json_encode($ret);
			exit;
		}
		
	}
	
	function updatepassRegister(){
		$dt['username'] = $this->session->userdata('username');
		$dt['userpass'] = $this->input->post('userpass');
		$data['userpass'] = $this->input->post('userpass');
		$data2['userpass'] = $this->input->post('userpass2');
		$data3['userpass'] = $this->input->post('userpass3');
		$rs = $this->db->get_where("ref_applicant",$dt);
		if($rs->num_rows()==0){
			$ret['text'] = "Password lama salah";
			echo json_encode($ret);
			exit;
		}else{
			if($data2['userpass']!=$data3['userpass']){
				$ret['text'] = "Konfirmasi Password baru salah";
				echo json_encode($ret);
				exit;
			}else{
				if(!$this->db->update("ref_applicant",$data3,$dt)){
					$ret['text'] = "Update Password tidak berhasil\n".$this->db->_error_message();
					echo json_encode($ret);
					exit;
				}else{
					$ret['text'] = "Update Password Berhasil";
					echo json_encode($ret);
					exit;
				}
			}
		}
	}
	
	function do_upload_scanfile(){
		$path_parts = pathinfo($_FILES["scanfile"]["name"]);
		$extension = $path_parts['extension'];
		
		$config['upload_path'] = BASEPATH.'../upliddir/';
		$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
		$config['max_size']	= '5000000';
		$config['file_name'] = $this->input->post('filename').".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('scanfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
		
	}
	
	
	function delUpload(){
		$full_path = $this->input->post('full_path');
		if(file_exists($full_path)){
			unlink($full_path);
			$ret['text'] = "file terhapus";
		}else{
			$ret['text'] = "file tidak ketemu";
		}
		echo json_encode($ret);
	}
	
	function login2(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("ref_applicant",$data);
		if($rs->num_rows()>0){
			$user = $rs->row();
			$sesi = array(
			'username'=>$user->username
			,'fullname'=>$user->fullname
			,'id_peserta'=>$user->id
			,'identity'=>$user->identity
			,'tmstamp'=>date("Y-m-d H:i:s")
			,'ip'=>$this->input->ip_address()
			,'is_login_pelatihanuser'=>true
			);
			
			$this->session->set_userdata($sesi);    
			$data2['ip'] = $this->session->userdata('ip');
			$data2['islogin'] = 1;
			$dt['username'] = $this->session->userdata('username');
			$this->db->update("ref_applicant",$data,$dt);
			redirect('portal/profile');
		}else{
			redirect('portal/masuk?failed');
		}
	}
	
	function login(){

		foreach($_POST as $key=>$value){

			$data[$key] = $value;

		}

		$rs = $this->db->get_where("ref_applicant",$data);

		if($rs->num_rows()>0){

			$user = $rs->row();

			$sesi = array(

			'username'=>$user->username

			,'fullname'=>$user->fullname

			,'id_peserta'=>$user->id

			,'identity'=>$user->identity

			,'tmstamp'=>date("Y-m-d H:i:s")

			,'ip'=>$this->input->ip_address()

			,'is_login_pelatihanuser'=>true

			);

			

			$this->session->set_userdata($sesi);    

			$data2['ip'] = $this->session->userdata('ip');

			$data2['islogin'] = 1;

			$dt['username'] = $this->session->userdata('username');

			$this->db->update("ref_applicant",$data,$dt);

			redirect('portal/profile');

		}else{

			redirect('portal/masuk?failed');

		}

	}

	
	function getPeserta($iduser){
		$this->db2 = $this->load->database('disnaker', TRUE);
		$rs = $this->db2->query("SELECT * FROM ngi_jobapplied a
				LEFT JOIN user_pelamar b ON a.id_pelamar=b.iduser
				WHERE STATUS='0' and iduser=='".$iduser."'");
		return $rs;
 	} 
 
	function loginpesertapelatihan(){
		// foreach($_POST as $key=>$value){
		// 	$data[$key] = $value;
		// }
		// $rs = $this->db->get_where("user_pelamar",$data);
		// if($rs->num_rows()>0){
		// 	$user = $rs->row();
		// 	$sesi = array(
		// 	'username'=>$user->username
		// 	,'fullname'=>$user->fullname
		// 	,'id_peserta'=>$user->id
		// 	,'identity'=>$user->identity
		// 	,'tmstamp'=>date("Y-m-d H:i:s")
		// 	,'ip'=>$this->input->ip_address()
		// 	,'is_login_pelatihanuser'=>true
		// 	);
			
		// 	$this->session->set_userdata($sesi);    
		// 	$data2['ip'] = $this->session->userdata('ip');
		// 	$data2['islogin'] = 1;
		// 	$dt['username'] = $this->session->userdata('username');
		// 	//$this->db->update("ref_applicant",$data,$dt);
		// 	redirect('portal/profile');
		// }else{
		// 	redirect('portal/masuk?failed');
		// }
		$q = $this->db->query('select iduser, nama, verified, email from user_pelamar where (email = "'.$_POST['username'].'" or username = "'.$_POST['username']. '" ) and password = "'.md5($_POST['password']).'"');

			if($q->num_rows() > 0){ 
			$data = $q->row();

			if($data->verified == 1) {
				$setdata = array(
					'is_login_pelatihanuser' => true,
					'id' => $data->iduser,
					'nama' => $data->nama
				);

				$this->session->set_userdata($setdata);

				echo json_encode(array('success' => true, 'tipe' => 'pelamar'));
				redirect('portal/profile');
			}else{
				echo json_encode(array('success' => false, 'email' => $data->email, 'tipe' => 'pelamar'));
				
			}
		}else{
			redirect('portal/masuk?failed');
		}
		
	}
	

	function logout(){
		$this->db->query("update ref_applicant set lastlogin = NOW() 
		where username = '".$this->session->userdata('username')."'");  
		$this->session->sess_destroy();
		redirect('https://bogorcareercenter.bogorkab.go.id/pelatihan/');
		// echo "test";
	}

	function saveGantipass(){
		$dt['username'] = $this->session->userdata('username');
		$dt['userpass'] = $this->input->post('userpass');
		$data['userpass'] = $this->input->post('userpass');
		$data2['userpass'] = $this->input->post('userpass2');
		$data3['userpass'] = $this->input->post('userpass3');
		$rs = $this->db->get_where("ref_applicant",$dt);
		if($rs->num_rows()==0){
			$ret['text'] = "Password lama salah";
			echo json_encode($ret);
			exit;
		}else{
			if($data2['userpass']!=$data3['userpass']){
				$ret['text'] = "Konfirmasi Password baru salah";
				echo json_encode($ret);
				exit;
			}else{
				if(!$this->db->update("ref_applicant",$data3,$dt)){
					$ret['text'] = "Update Password tidak berhasil\n".$this->db->_error_message();
					echo json_encode($ret);
					exit;
				}else{
					$ret['text'] = "Update Password Berhasil";
					echo json_encode($ret);
					exit;
				}
			}
		}
	}
	
	function saveReqinfo(){
		$keynot = array("","addr","city","email","fullname","job","phone_home","phone_mobile","postcode");
		$data = array();
		$data['username'] = $this->session->userdata('username');
		$data['req_date'] = $this->portal_mod->now();
		
		foreach($_POST as $key=>$value){
			if(array_search($key,$keynot)==''){
				$data[$key] = $value;
			}
		}
		if($data['skpd_id']==''){
			$data['is_to_kab'] = 1;
		}else{
			$data['is_to_kab'] = 0;
		}
		
		if(!$this->db->insert("req_info",$data)){
			$ret['text'] = "Permintaan tidak terkirim";
			echo json_encode($ret);
			exit;
		}else{
			$ret['text'] = "Permintaan telah terkirim";
			echo json_encode($ret);
		}
	}

	
	function saveKeberatan(){
		$keynot = array("","addr","city","email","fullname","job","phone_home","phone_mobile","postcode","skpd_id","req_title","req_info");
		$data = array();
		$data['username	'] = $this->session->userdata('username');
		$data['keberatan_date'] = $this->portal_mod->now();
		
		foreach($_POST as $key=>$value){
			if(array_search($key,$keynot)==''){
				$data[$key] = $value;
			}
		}
		
		if(!$this->db->insert("req_keberatan",$data)){
			$ret['text'] = "Keberatan tidak terkirim";
			echo json_encode($ret);
			exit;
		}else{
			$ret['text'] = "Keberatan telah terkirim";
			echo json_encode($ret);
		}
	}
	
	function saveFeedback(){
		$keynot = array("","addr","city","email","fullname","job","phone_home","phone_mobile","postcode","skpd_id","req_title","req_info");
		$data = array();
		$data['username	'] = $this->session->userdata('username');
		$data['feedback_date'] = $this->portal_mod->now();
		
		foreach($_POST as $key=>$value){
			if(array_search($key,$keynot)==''){
				$data[$key] = $value;
			}
		}
		
		if(!$this->db->insert("req_feedback",$data)){
			$ret['text'] = "Feedback tidak terkirim";
			echo json_encode($ret);
			exit;
		}else{
			$ret['text'] = "Feedback telah terkirim";
			echo json_encode($ret);
		}
	}


}