<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	
	var $modules = "portal";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index(){
		$this->load->view('index',$this->$data);
	}
	
	function downloadfile(){
		if($this->session->userdata('username')!=''){
		$rs = $this->db->query("select * from ref_dip_file
			where dip_id='".$this->uri->segment(4)."' order by tmstamp desc");
			$item = $rs->row();
			$path = BASEPATH."../upliddir";
			$path_file = $path."/".$item->dip_file;
			$pathfile = file_get_contents($path_file,1);
			$name = $item->dip_file;
			$filearr = explode(".",$item->dip_file);
			$file_ext =  $filearr[count($filearr)-1];
			$file_download = $this->session->userdata('username').'_'.md5(date('YmdHis')).'.'.$file_ext;
			if(file_exists($path_file) && $item->dip_file!=''){
				force_download($file_download, $pathfile);
				$ret['error'] = 0;
				$ret['text' ] = 'File telah didownload';
			}else{
				header('location:'.base_url().'portal/dip_detail/'.$this->uri->segment(4).'?error=1');
			}
		}else{
			header('location:'.base_url().'portal/masuk/');
		}
		echo json_encode($ret);
	}
	
	function daftarpelatihan(){
		if($this->session->userdata('is_login_pelatihanuser')!=''){
		$rs = $this->db->query("insert into ref_pendaftaran set id_applicant='".$this->session->userdata('id_peserta')."' , id_pelatihan='".$this->uri->segment(4)."'");
			
				if($rs==true){
				
				$ret['error'] = 0;
				$ret['text' ] = 'Pendaftaran Berhasil';
			}else{
				header('location:'.base_url().'portal/dip_detail/'.$this->uri->segment(4).'?error=1');
			}
		}else{
			header('location:'.base_url().'portal/masuk/');
		}
		echo json_encode($ret);
	}
	
	function ceksertifikat(){
		
				
				$ret['error'] = 0;
				$ret['text' ] = $this->input->post('id_pelatihan');
			
			
		echo json_encode($ret);
	}
	
	
	
	function downloadfile2(){
		if($this->session->userdata('username')!=''){
			$rs = $this->db->query("select * from req_info
			where req_id='".$this->uri->segment(4)."' order by tmstamp desc");
			$item = $rs->row();
			$path = BASEPATH."../upliddir";
			$pathfile = file_get_contents($path."/".$item->dip_file);
			$name = $item->dip_file;
			force_download($name, $pathfile);
		}else{
			redirect('portal/masuk');
		}
	}
	
	function getSkpd(){
		$rs = $this->db->query("SELECT a.skpd_id,a.skpd_name FROM ref_skpd a");
		echo json_encode($rs->result());
	}
	
	
}