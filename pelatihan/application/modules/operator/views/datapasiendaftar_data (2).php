<div class="col-md-6">
<div class="box-body">
		<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
		
		<form id="form1" name="form1" action="<?=base_url()?>auth/page/saveRak" class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<div class="row-fluid">
				<div class="span9">
					<div class="accordion-heading">
						<BR><div class="control-group">
							<label class="control-label" for="kode">Kode Rak</label>
							<div class="controls">
								<input type="text" class="form-control"id="kode" name="kode" maxlength="100" required placeHolder="Kode Rak" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="rak">Rak</label>
							<div class="controls">
								<input type="text" class="form-control"id="rak" name="rak" maxlength="100" required placeHolder="Nama Rak" /><span id="info"></span> 
							</div>
						</div><br>
						<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
				</div> <!-- end of span5 -->
			</div>
		</form>
	</div>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="pull-right">
				<div id="spinner" style="display:inline-block"></div>
				<div class="input-append form-inline">
				  <input type="text" class="form-control" id="cari" name="cari" placeHolder="Cari ..." />
				  <div class="btn-group">
					<select class="form-control"  id="per_page" name="per_page" style="width:60px">
					  <option value="">-</option>
					  <option value="4">4</option>
					  <option value="25">25</option>
					  <option value="50">50</option>
					</select>
				  </div>
				</div>        
				
			 </div>
		  </div>
		<div id="result"></div>
	</div>
</div>
		</div>
		</div>