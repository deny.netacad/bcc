<style>
	.photo-thumb{
		float:left;border:1px solid #ccc;margin-right:5px;background-size:100% auto;background-repeat:no-repeat;background-position:center center;width:50px;height:50px;cursor:pointer;
	}
</style>
<script>
$(document).ready(function(){
	$('#alert2').hide();
	
});
</script>
<div id="alert2" class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Sukses!</strong> Data telah dihapus
</div>
<?php
	$where = " ";
	$per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= " where (a.kode_barang like '%".$cari."%' or a.nama_barang like '%".$cari."%' )";
	#echo $where;
	
	$n = intval($this->uri->segment(5));
	$q = "select a.*,b.satuan from barang a 
	left join m_satuan b on a.idsatuan=b.id
	left join m_rak c on a.idrak=c.id
	";
	$rs = $this->db->query("$q $where");
	$row = $rs->row_array();
	$totrows = $rs->num_rows();

	$config['base_url'] = base_url().'auth/page/data/barang';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("$q $where order by a.kode_barang limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
<span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
<?=$data['pagination']?>
</div>
<img id="noimage" style="display:none" />
<table class="table table-hover">
  <colgroup>
	<col class="con0" style="width:25px" />
	<col class="con0" style="width:100px" />
	<col class="con0" style="width:auto" />
	<col class="con0" style="width:100px" />
	<col class="con0" style="width:100px" />
	<col class="con0" style="width:100px" />
	<col class="con0" style="width:100px" />
	<col class="con0" style="width:100px" />
	<col class="con0" style="width:100px" />
	<col class="con0" style="width:50px" />
  </colgroup>
  <thead class="breadcrumb">
	<tr>
	  <th>#</th>
	  <th>KODE BARANG</th>
	  <th>NAMA BARANG</th>
	  <th><div class="text-center">SATUAN</div></th>
	  <th><div class="text-right">HARGA BELI</div></th>
	  <th><div class="text-right">HARGA JUAL</div></th>
	  <th><div class="text-right">STOK</div></th>
	  <th><div class="text-right">STOK LIMIT</div></th>
	  <th><div class="text-center">RAK</div></th>
	</tr>
  </thead>
  <tbody>
	<?php
	foreach($data['posts']->result() as $item){ $n++;
	$files = explode(".",$item->foto_barang);
	if(count($files)>0){
		$file_thumb = $files[0]."_thumb.".$files[1];
	}
	?>
	<tr>
	  <td align="center"><?=$n?>.</td>
	  <td>
	  <div class="text-left"><?=$item->kode_barang?></div>
	  </td>
	  <td>
	  <div class="text-left"><?=$item->nama_barang?></div></td>
	  <td><div class="text-center"><?=$item->satuan?></div></td>
	  <td><div class="text-right"><?=number_format($item->harga_beli)?></div></td>
	  <td><div class="text-right"><?=number_format($item->harga_jual)?></div></td>
	  <td><div class="text-right"><?=number_format($item->stok)?></div></td>
	  <td><div class="text-right"><?=number_format($item->stok_limit)?></div></td>
	  <td><div class="text-center"><?=$item->rak?></div></td>
	  <td>
		<div class="text-center">
		</div>
	  </td>
	</tr>
	<?php
	}
	?>
  </tbody>
</table>