<script>
	
	
	$(document).ready(function(){
	$('#alert2').hide();
	
		try{
			$('#form22').enterform();
			$('#kode').focus();
		}catch(e){
			alert(e);
		}
		
		
		
		$(document).on('reset','#form22',function(){
			resetForm($('#form22'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('#kode').focus();
		});
		
		$('#per_page').change(function(){
			//refreshData();
		});
		
		$('#cari').keyup( $.debounce(250,refreshData));
		
		$('#rak').keyup(function(){
		var isi =$('#rak').val();
		var edit =$('#id_edit').val();
		
		$.post('<?=base_url()?>auth/page/cekDATA',{datanya:isi,tablenya:"m_rak",fieldnya:"rak"},
		
		function(data)
		{
				if(data == 1){
				$('#info').html('&nbsp;<font color=green>AVAILABLE</font>');
				$('#submit').removeAttr('disabled');
				$('#submit').val('Simpan');
				
			} 
			if(data == 0 ){
				$('#info').html('&nbsp;<font color=red>DUPLICATE</font>');
				$('#submit').attr('disabled','disabled');
				
				$('#submit').val('LOCK');
				
				if(edit==""){
			$('#submit').attr('disabled','disabled');
				
				}
				
			}

		});

});

		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
				beforeSend:function(){
					$('#spinner').loading('stop');
					$('#spinner').loading();
				},
				success:function(response){
					$('#spinner').loading('stop');
					$('#result').html(response);
				}
			});
			return false;
		});
		
		$(document).on('click','a[title="Edit"]',function(){
			var $this = $('a[title="Edit"]');
			var index = $this.index($(this));
			$.ajax({
				type:'post',
				url:'<?=base_url()?>auth/page/editRak',
				data:{ 'id':$this.eq(index).attr('data') },
				beforeSend:function(){
					$('#spinner').loading('stop');
					$('#spinner').loading();
				},
				success:function(response){
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					var keynot = new Array("");
					var keyin = new Array("id");
					var keyout = new Array("id_edit");
					for(attrname in ret){
						if($.inArray(attrname,keynot)==-1){
							$('#'+attrname).val(ret[attrname]);
						}
					}
					for(attrname in ret){
						if($.inArray(attrname,keyin)!=-1){
							$('#'+keyout[$.inArray(attrname,keyin)]).val(ret[attrname]);
						}
					}
				}
			});
		});
		
		$(document).on('click','a[title="Delete"]',function(){
			var $this = $('a[title="Delete"]');
			var index = $this.index($(this));
			if(confirm('Yakin hapus item...?')){
				$.ajax({
					type:'post',
					url:'<?=base_url()?>auth/page/delRak',
					data:{ 'id':$this.eq(index).attr('data') },
					beforeSend:function(){
						$('#spinner').loading('stop');
						$('#spinner').loading();
					},
					success:function(response){
						$('#spinner').loading('stop');
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
		});
		
		$('#alert1').hide();
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#kode').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
	});

function refreshData(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>auth/page/data/rak',
		data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});
}
	
	
</script>
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<!--<div id="alert2" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>-->
		
		<form id="form22" name="form22" action="<?=base_url()?>operator/page/savePasienbaru" class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<div class="row-fluid">
				<div class="span9">
					<div class="accordion-heading">
						<BR><div class="control-group">
							<label class="control-label" for="kode">Nama Pasien</label>
							<div class="controls">
								<input type="text" class="form-control"id="pasien_nama" name="pasien_nama" maxlength="100" required placeHolder="Nama Lengkap" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="rak">Alamat Pasien</label>
							<div class="controls">
								<input type="text" class="form-control"id="pasien_alamat" name="pasien_alamat" maxlength="100" required placeHolder="Alamat" /><span id="info"></span> 
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="rak">Telepon Pasien</label>
							<div class="controls">
								<input type="text" class="form-control"id="pasien_tlp" name="pasien_tlp" maxlength="100" required placeHolder="Telepon" /><span id="info"></span> 
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="rak">Umur Pasien</label>
							<div class="controls">
								<input type="text" class="form-control"id="umur" name="umur" maxlength="100" required placeHolder="umur dalam tahun" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="rak">No. KTP Pasien</label>
							<div class="controls">
								<input type="text" class="form-control"id="pasien_noktp" name="pasien_noktp" maxlength="100" required placeHolder="Nomor KTP" /><span id="info"></span> 
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="rak">Pasien Rujukan Dari</label>
							<div class="controls">
								<input type="text" class="form-control"id="pasien_rujukan_dari" name="pasien_rujukan_dari" maxlength="100" required placeHolder="Rujukan " /><span id="info"></span> 
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="rak">Jenis Kelamin</label>
							<select name="pasien_jk" id="pasien_jk" class="form-control">
							<option >| PILIH</option>
							<option value=0>PEREMPUAN</option>
								<option value=1>lAKI-LAKI</option>
							</select>
							
							
						</div>
						
						
						
						<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
				</div> <!-- end of span5 -->
			</div>
		</form>
	</div>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="pull-right">
				<div id="spinner" style="display:inline-block"></div>
				<div class="input-append form-inline">
				  <input type="text" class="form-control" id="cari" name="cari" placeHolder="Cari ..." />
				  <div class="btn-group">
					<select class="form-control"  id="per_page" name="per_page" style="width:60px">
					  <option value="">-</option>
					  <option value="4">4</option>
					  <option value="25">25</option>
					  <option value="50">50</option>
					</select>
				  </div>
				</div>        
				
			 </div>
		  </div>
		<div id="result"></div>
	</div>
</div>