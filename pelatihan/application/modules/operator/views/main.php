<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?=$site_title?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?=$def_css?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$def_css?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?=$def_css?>ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?=$def_css?>AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- demo style -->
        <style type="text/css">
            /* FROM HTTP://WWW.GETBOOTSTRAP.COM
             * Glyphicons
             *
             * Special styles for displaying the icons and their classes in the docs.
             */

            .bs-glyphicons {
                padding-left: 0;
                padding-bottom: 1px;
                margin-bottom: 20px;
                list-style: none;
                overflow: hidden;
            }
            .bs-glyphicons li {
                float: left;
                width: 25%;
                height: 115px;
                padding: 10px;
                margin: 0 -1px -1px 0;
                font-size: 12px;
                line-height: 1.4;
                text-align: center;
                border: 1px solid #ddd;
            }
            .bs-glyphicons .glyphicon {
                margin-top: 5px;
                margin-bottom: 10px;
                font-size: 24px;
            }
            .bs-glyphicons .glyphicon-class {
                display: block;
                text-align: center;
                word-wrap: break-word; /* Help out IE10+ with class names */
            }
            .bs-glyphicons li:hover {
                background-color: rgba(86,61,124,.1);
            }

            @media (min-width: 768px) {
                .bs-glyphicons li {
                    width: 12.5%;
                }
            }
        </style>
   
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

   
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <!--<link rel="shortcut icon" href="../assets/ico/favicon.png">-->
	<script src="<?=$def_js?>jquery.js"></script>
    <script src="<?=$def_js?>bootstrap.js"></script>
	<script src="<?=$def_js?>holder.js"></script>
	<script src="<?=$def_js?>enterform.js"></script>
	<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
	<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">
	<link href="<?=$def_css?>custom.css" rel="stylesheet">
	
	<link href="<?=$def_js?>plugins/contextmenu/jquery.contextMenu.css" rel="stylesheet">
	<link href="<?=$def_js?>plugins/parallax-slider/css/parallax-slider.css" rel="stylesheet">
	<link href="<?=$def_css?>icomoon/style.css" rel="stylesheet">
		<link rel="stylesheet" href="<?=$def_css?>spinners.css" type="text/css">
	
	<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.min.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/conntextmenu/jquery.contextMenu.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/highcharts.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/modules/exporting.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/parallax-slider/js/jquery.cslider.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/parallax-slider/js/modernizr.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/jpodtable/jquery.jpodtable.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/terbilang/jquery.terbilang.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/mask/jquery.mask.js"></script>
	<script>
		$(document).ready(function(){
			$('.dropdown-toggle').dropdown()
		});
	</script>
  </head>
  <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?=base_url()?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        
                        <!-- Notifications: style can be found in dropdown.less -->
                        
                        <!-- Tasks: style can be found in dropdown.less -->
                        
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span> <?=$this->session->userdata('username')?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?=base_url()?>/assets/img/avatar3.png" class="img-circle" alt="User Image" />
                                    <p>
                                        <?=$this->session->userdata('username')?>
                                        <small> <?=$this->session->userdata('nama')?></small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!--li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?=base_url()?>operator/gantipass" class="btn btn-default btn-flat" >Change Password</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?=base_url()?>assets/img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?=$this->session->userdata('username')?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" class="form-control"name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?=base_url()?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <!--li>
                            <a href="../widgets.html">
                                <i class="fa fa-th"></i> <span>Widgets</span> <small class="badge pull-right bg-green">new</small>
                            </a>
                        </li-->
                        <?php
							$this->load->view($this->uri->segment(1).'/menu');
							?>
						
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Setting Page
                        <small>Halaman Untuk Pengaturan </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                             <li class="active">Setting</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-xs-12'>
                           <?php
		if(isset($page)){
			$this->load->view($module.'/'.$page);
		}else{
			$this->load->view($module.'/default');
		}
	  ?>
	</div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!--script src="<?=$def_js?>jquery.min.js"></script>
        <script src="<?=$def_js?>bootstrap.min.js" type="text/javascript"></script-->
        <!-- AdminLTE App -->
        <script src="<?=$def_js?>AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?=$def_js?>AdminLTE/demo.js" type="text/javascript"></script>
    </body>
</html>
