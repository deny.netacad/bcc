<script>
$(document).ready(function(){

		  chart = new Highcharts.Chart({
            chart: {
                renderTo: 'graph1',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Jenis Sebaran KBLI'
            },
			  subtitle: {
                text: 'Source: dinperindag jateng'
            },
			
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
				<?php
				$rs = $this->db->query("select count(*) as jml,b.kbli from ikecil  a left join kbli b on a.kbli=b.id where a.kabupaten='".$this->session->userdata('kabupaten')."' group by a.kbli");
				foreach($rs->result() as $item){
				if($item->kbli==NULL) $item->kbli="TIDAK ADA";
				$datar .="['".$item->kbli."',".$item->jml."],";
				
				
				}
				?>
                
        
            series: [{
                type: 'pie',
                name: 'KBLI',
                data: [
                   <?=$datar?> ['Others',   0.7]
                ]
            }]
        });
		

    
});



</script>
<div class="row-fluid">
	<div align="center">
		<h4></h4>
	</div>
</div>
<div id="graph1" style="width:95%"></div>

</div>
