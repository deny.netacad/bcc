<script type="text/javascript" src="<?=$def_js?>enterform.js">

</script>
<script>
$(document).ready(function(){
		$('#mulai').datepicker({
		format:'yyyy',
		autoclose:true,
		viewMode: 2,
		minViewMode: 2
	});
		
		$('#ahir').datepicker({
		format:'yyyy',
		autoclose:true,
		viewMode: 2,
		minViewMode: 2
	});
	$('.dt').mask('99-99-9999');
});

</script>
<ul class="breadcrumb">
  <li><a href="#"><i class="icomoon-settings"></i> Setting</a> <span class="divider"></span></li>
  <li class="active">Laporan</li>
</ul>

<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#update" data-toggle="tab"><i class="icomoon-file3"></i> Update</a></li>
</ul>
<h3>Print Laporan Kabupaten </h3>
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		
		<form id="form1" name="form1" action="<?=base_url()?>operator/page/view/cetak" target="_blank" class="form-horizontal" method="post" enctype="multipart/form-data">
		<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<div class="row-fluid">
				<div class="span9">
					<div class="accordion-heading">
						<BR><div class="control-group ">
							<label class="control-label" for="tahun">Tahun  </label>
							<div class="controls">
								<input type="text" class="form-control" id="mulai" name="mulai" maxlength="10" style="width:60px" placeHolder="Tahun" value="<?=date('Y')?>" />
							</div>
						</div>
						<BR><div class="control-group ">
							<label class="control-label" for="tahun">Sampai Tahun  </label>
							<div class="controls">
								<input type="text" class="form-control" id="ahir" name="ahir" maxlength="10" style="width:60px" placeHolder="Tahun" value="<?=date('Y')?>" />
							</div>
						</div>
						
					
						<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" name="submit" class="btn btn-primary">Cetak</button>
								<button type="submit" class="btn btn-alert" id="button" name="download">Download</button>
            
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
				</div> <!-- end of span5 -->
			</div>
		</form>
	</div>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="pull-right">
				<div id="spinner" style="display:inline-block"></div>
				<div class="input-append">
				  <input type="text" class="form-control"id="cari" name="cari" placeHolder="Cari ..." />
				  <div class="btn-group">
					<select  id="per_page" name="per_page" style="width:60px">
					  <option value="">-</option>
					  <option value="4">4</option>
					  <option value="25">25</option>
					  <option value="50">50</option>
					</select>
				  </div>
				</div>        
				
			 </div>
		  </div>
		<div id="result"></div>
	</div>
</div>