<script>
	$(document).ready(function(){
		try{
		$('a.detail').tooltip({
			html:true,
			placement:'right'
		});
		}catch(e){
			alert(e);
		}
		
	});
</script><br><small>
History Kunjungan <br></small>


<table class="table table-bordered table-hover">
	<colgroup>
		<col style="width:28px" />
		<col style="width:200px" />
		<col style="width:150px" />
		<col style="width:150px" />
		<col style="width:200px" />
		<col style="width:100px" />
		<col style="width:120px" />
	</colgroup>
	<thead>
		<tr>
			<th><div class="text-center">NO</div></th>
			<th><div class="text-left">TANGGAL</div></th>
			
			<th><div class="text-center">DOKTER</div></th>
			
		</tr>
	</thead>
	<tbody>
		<?php
		$rs = $this->db->query("select *,date_format(a.tgl_periksa,'%d %M %Y') as tgl_periksa from tb_periksa a left join tb_dokter b on a.id_dokter=b.id where a.is_posting =0 and a.no_rm='".$this->input->post('no_rm')."' order by a.tgl_periksa desc");
		$n = 0;
		
		foreach($rs->result() as $item){ $n++;
		?>
		<tr>
			<td><?=$n?></td>
			<td><div class="text-left"><?=$item->tgl_periksa?></div>
			
			</td>
		
			<td><div class="text-left"><?=$item->dokter_nama?></div>
			</div>
			
			</td>
			
	</tr>
		<?php
		
		}
		?>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="2"><div class="text-right">TOTAL KUNJUNGAN</div></th>
			<th><div class="text-right"><?=number_format($n)?></div></th>
		</tr>
	</tfoot>
</table>