<?php
$tgl_1 = explode("-",$this->input->post('tgl_1'));
$tglawal = $tgl_1[0];
$blnawal = $tgl_1[1];
$thnawal = $tgl_1[2];

$tgl_2 = explode("-",$this->input->post('tgl_2'));
$tglawal2 = $tgl_2[0];
$blnawal2 = $tgl_2[1];
$thnawal2 = $tgl_2[2];


?>
<html>
<head>
	<title></title>
	<style>
         body{
         font-family: "Arial";
         font-size:9pt;
         line-height:1;
         }
         h1,h2,h3,h4{
         color:#2e74b5;
         font-weight:600;
         }
         table{
         border-collapse:collapse;
         }
         table td{
         padding:3px;
         vertical-align:top;
         }
         table th{
         border:1px solid #2e74b5;
         padding:3px;
         }
         table thead tr{
         background-color:#d0e3ff;
         }		
		 table.noborder th{		border:none;		padding:3px;	}		table.noborder td{		border:none;		padding:3px;		vertical-align:top;	}		table.noborder th{		border:none;		padding:3px;	}	
		 .center{
			text-align:center;
		 }
      </style>
</head>
<body>
<br>
<H2 style="text-align:center">LAPORAN PENJUALAN<br />
		LA MARIZK <SMALL>lamarizk.com</small><br />
		Periode <?=$tgl_1[0]?> <?=$bulan[intval($tgl_1[1])]?> <?=$tgl_1[2]?>  s.d. <?=$tgl_2[0]?> <?=$bulan[intval($tgl_2[1])]?> <?=$tgl_2[2]?>
		</h2>
		<table border="1" width="100%">

	<thead>

		<tr>

			<th width="25">#</th>

			<th width="80">Tanggal</th>

			<th>Total Item</th>

			

			<th>Total Harga Jual</th>

			
			<th>Total Harga Beli</th>
			
			<th>Total Laba </th>
			
		</tr>

	</thead>

	<tbody>

		<?php

		$q = "select a.tgl_keluar,sum(b.harga_jual) as harga_jual ,sum(b.jml) as jml_total,b.jml as jml , c.harga_beli as harga_beli,SUM(b.subtotal) as subtotal ,SUM(b.totalbeli) as totalbeli from barang_keluar a
		left join barang_keluar_detail b on a.no_keluar=b.no_keluar
		left join barang c on b.idbarang=c.id
where tgl_keluar between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."' group by a.tgl_keluar";

		$rs = $this->db->query($q);

		$no = 0;

		$arr[0] = 0;
$no=0;
		foreach($rs->result() as $item){ $n++;

		$arrtot[] = $item->pro_val;
		$totals=$totals+($item->subtotal-$item->totalbeli);
		$totalsjual=$totalsjual+($item->subtotal);
		$totalsbeli=$totalsbeli+($item->totalbeli);
		$no++;
		?>
		<tr>

			<th width="25"><?=$no?></th>

			<th width="80"><?=$item->tgl_keluar?></th>

			
	

			<th align='right'><?=number_format($item->jml_total)?></th>
			<th align='right'><?=number_format($item->subtotal)?></th>
			<th align='right'><?=number_format($item->totalbeli)?></th>
			<th align='right'><?=number_format($item->subtotal-$item->totalbeli)?></th>
	
			
		</tr>
		<?php }?>
		<tr>

			<th width="25" colspan=3>Grand Total</th>

			
	



			
			<th align='right'><?=number_format($totalsjual)?></th>


			
			<th align='right'><?=number_format($totalsbeli)?></th>
			
			<th align='right'><?=number_format($totals)?></th>
	
			
		</tr>
</tbody></table>
<table class="noborder" width="100%">
	<tr>
		<td align="center" width="100%" colspan="2">
			<p>&nbsp;</p>

			<b>..............., <?=date('d')?> <?=$bulan[intval(date('m'))]?> <?=date('Y')?></b>

			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<u><b>NAMA </b></u><br />

			<small><i>Finance</i></small>
		</td>
	</tr>
	<tr>

		<td align="center" width="50%">

			<p>&nbsp;</p>

			<b>Mengetahui,</b>
<p>&nbsp;</p>
			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<u><b> NAMA</b></u><br />

			<small><i>Direktur</i></small>

		</td>

		<td align="center" width="50%">

			<p>&nbsp;</p>

			<b>Mengetahui,</b>
<p>&nbsp;</p>
			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<u><b>NAMA DIRUT</b></u><br />

			<small><i>Direktur Utama</i></small>

		</td>

	</tr>

</table>
	<?php
	//print_r($_REQUEST);
	?>
</body>
</html>
