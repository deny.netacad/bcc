<script>
	var BASEURL = '<?=base_url()?>';
	var urlUpload = '<?=base_url()?>operator/page/do_uploadphoto';
</script>
  <style>
  /* Absolute Center CSS Spinner */
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  opacity:0.7;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

</style>
<style>

.bar {
    height: 18px;
    background: green;
}
.fade {
	opacity: 0;
	-webkit-transition: opacity .15s linear;
	transition: opacity .15s linear;
}

.avatar{
	float:left;
	width:30px;
	height:30px;
	margin-right:5px;
	background:url('<?=base_url()?>assets/img/user-circle.png') no-repeat center center;
	background-size:100% 100%;
}

#photobox{
	position:relative;width:250px;height:203px;border:1px solid #ccc;border-radius:10px;
	vertical-align:bottom;
	
	display:table-cell;
	
}
</style>
<link rel="stylesheet" href="<?=$def_js?>plugins/jquery-file-upload/css/jquery.fileupload-ui.css">
<script src="<?=$def_js?>plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/tmpl.min.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/tmpl.min.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script>
	
	function _formatBitrate(bits) {
		if (typeof bits !== 'number') {
			return '';
		}
		if (bits >= 1000000000) {
			return (bits / 1000000000).toFixed(2) + ' Gbit/s';
		}
		if (bits >= 1000000) {
			return (bits / 1000000).toFixed(2) + ' Mbit/s';
		}
		if (bits >= 1000) {
			return (bits / 1000).toFixed(2) + ' kbit/s';
		}
		return bits.toFixed(2) + ' bit/s';
	}

	function _formatPercentage(floatValue) {
		return (floatValue * 100).toFixed(2) + ' %';
	}


	function _formatFileSize(bytes) {
		if (typeof bytes !== 'number') {
			return '';
		}
		if (bytes >= 1000000000) {
			return (bytes / 1000000000).toFixed(2) + ' GB';
		}
		if (bytes >= 1000000) {
			return (bytes / 1000000).toFixed(2) + ' MB';
		}
		return (bytes / 1000).toFixed(2) + ' KB';
	}


	function _formatTime(seconds) {
		var date = new Date(seconds * 1000),
			days = Math.floor(seconds / 86400);
		days = days ? days + 'd ' : '';
		return days +
			('0' + date.getUTCHours()).slice(-2) + ':' +
			('0' + date.getUTCMinutes()).slice(-2) + ':' +
			('0' + date.getUTCSeconds()).slice(-2);
	}

	function _formatPercentage(floatValue) {
		return (floatValue * 100).toFixed(2) + ' %';
	}
	
	function _formatPercentage2(floatValue) {
		return (floatValue * 100).toFixed(2) + '%';
	}

	function _renderExtendedProgress(data) {
		return _formatBitrate(data.bitrate) + ' | ' +
			_formatTime(
				(data.total - data.loaded) * 8 / data.bitrate
			) + ' | ' +
			_formatPercentage(
				data.loaded / data.total
			) + ' | ' +
			_formatFileSize(data.loaded) + ' / ' +
			_formatFileSize(data.total);
	}
	
	function kecamatanFormatResult(data) {
        var markup= "<li>"+ data.kecamatan+"</li>";
        return markup;
    }

    function kecamatanFormatSelection(data) {
        return data.kecamatan;
    }
	function kabupatenFormatResult(data) {
        var markup= "<li>"+ data.kabupaten+"</li>";
        return markup;
    }

    function kabupatenFormatSelection(data) {
        return data.kabupaten;
    }
	
	function rakFormatResult(data) {
        var markup= "<li>"+ data.kelurahan +"</li>";
        return markup;
    }

    function rakFormatSelection(data) {
        return data.kelurahan;
    }
	function kbliFormatResult(data) {
        var markup= "<li>"+ data.nama +"<br/>"+ data.kbli+" <b>"+data.tahun+"</b></li>";
        return markup;
    }

    function kbliFormatSelection(data) {
        return data.nama;
    }
	
	
	$(document).ready(function(){
		$('.sembunyikan').hide();
			
			$('#kabupaten').on('select2-selecting',function(){
			$('#alm_kec').select2('data',null);
			$('#alm_kelurahan').select2('data',null);
			
			});
			
			$('#alm_kec').on('select2-selecting',function(){
			$('#alm_kelurahan').select2('data',null);
			});
		
			$('#kbli').on('select2-selecting',function(){
			$('.sembunyikan').show('slow');
			});
		
	
	
		$(document).on('click','#prosen',function(){
			var vhargabeli = 0;
			var vprosen = 0;
			var vhargajual = 0;
			
			if($(this).prop('checked')){
				vhargabeli = (($('#harga_beli').cleanVal()=='')?0:parseFloat($('#harga_beli').cleanVal()));
				vprosen = (($('#prosen').val()=='')?0:parseFloat($('#prosen').val()));
				vhargajual = (parseFloat(vhargabeli)*parseFloat(vprosen))+parseFloat(vhargabeli);
				$('#harga_jual').val(vhargajual);
				$('#harga_jual').trigger('keyup');
				$('#stok_limit').focus();
			}else{
				$('#harga_jual').val('');	
				$('#harga_jual').focus();
			}
		});
		
		try{
		$('#fileupload').fileupload({
			url:'<?=base_url()?>operator/page/do_uploadphoto',
			formData:{ 'username':'<?=$this->session->userdata('username')?>' },
			autoUpload: true,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			maxFileSize: 15000000, // 15 MB
		});
		
		$('#fileupload').bind('fileuploadprogress', function (e, data) {
			$('#progress_caption').html(_renderExtendedProgress(data));
			$('#progress').show();
			$('#progress').css('width',_formatPercentage2(data.loaded / data.total));
		});
		
		$('#fileupload').bind('fileuploaddone', function (e, data) {
			var ret = $.parseJSON(data.result);
			$('#progress_caption').html('');
			$('#photobox').css({
				'background-image':'url('+ret.files[0].thumbnailUrl+')',
				'background-size':'100% auto',
				'background-repeat':'no-repeat',
				'background-position':'center center'
			});
			$('#progress').css('width','0%');
			$('#progress').hide();
			$('#btn-remove').show();
			$('#btn-remove').attr('data',ret.files[0].name+'|'+ret.files[0].name_thumb);
			$('#gambar').val(ret.files[0].name);
			$('#alm_kec').select2('focus');
			
		});
		
		$(document).on('click','#btn-remove',function(){
			$.ajax({
				type:'post',
				url:'<?=base_url()?>operator/page/delPhoto',
				data:{ 'filename':$(this).attr('data') },
				beforeSend:function(){
				},
				success:function(response){
					resetPhotoBox();
				}
			});
		});
		
		$('#btn-remove').hide();
		}catch(e){
			alert(e.message);
		}
		
		
		$('#form1').enterform();
		$('#kode_dataikm').focus();
		$('#nama_dataikm').focusTo($('#fileupload'));
		
		$('.num').mask('000,000,000,000,000', {reverse: true});
		
		$(document).on('submit','#form1',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					refreshData();
$('#alm_kec').select2('data',null);
			$('#alm_kelurahan').select2('data',null);
$('#kbli').select2('data',null);
			$('#kabupaten').select2('data',null);
			
			
					resetForm($('#form1'));	
				});		
			}else{
				refreshData();
			}
			$('#form1').trigger('reset');
		});
		
		$(document).on('reset','#form1',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('#kode_dataikm').focus();
			$('#alm_kec').select2('data',null);
			$('#idrak').select2('data',null);
			$('#kabupaten').select2('data',null);
			
			resetPhotoBox();
		});
		
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup( $.debounce(250,refreshData));
		
		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
				beforeSend:function(){
					$('#spinner').loading('stop');
					$('#spinner').loading();
				},
				success:function(response){
					$('#spinner').loading('stop');
					$('#result').html(response);
				}
			});
			return false;
		});
		
		$(document).on('click','a[title="Edit"]',function(){
			var $this = $('a[title="Edit"]');
			
			var index = $this.index($(this));
			$.ajax({
				type:'post',
				url:'<?=base_url()?>operator/page/editBarang',
				data:{ 'id':$this.eq(index).attr('data') },
				beforeSend:function(){
					$('#spinner').loading('stop');
					$('#spinner').loading();
				},
				success:function(response){
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					var keynot = new Array("tahun","id_kab");
					var keyin = new Array("id");
					var keyout = new Array("id_edit");
					for(attrname in ret){
						if($.inArray(attrname,keynot)==-1){
							$('#'+attrname).val(ret[attrname]);
						}
					}
					for(attrname in ret){
						if($.inArray(attrname,keyin)!=-1){
							$('#'+keyout[$.inArray(attrname,keyin)]).val(ret[attrname]);
						}
					}
					$('#kabupaten').select2('data',{'id':ret.id_kabx,'kabupaten':ret.kabupaten});	
					$('#alm_kec').select2('data',{'id':ret.id_kecx,'kecamatan':ret.kecamatan});
					$('#alm_kelurahan').select2('data',{'id':ret.id_kelx,'kelurahan':ret.kelurahan});
					$('#kbli').select2('data',{'id':ret.id_kblix,'nama':ret.nama,'tahun':ret.tahun});
					
					var file_photo = $('#gambar').val().split(".");
					var file_photo_thumb = file_photo[0]+'_thumb.'+file_photo[1];
					$('#photobox').css({
						'background-image':'url(<?=base_url()?>upliddir/'+file_photo_thumb+')',
						'background-size':'100% auto',
						'background-repeat':'no-repeat',
						'background-position':'center center'
					});
					$('#progress').css('width','0%');
					$('#progress').hide();
					$('#btn-remove').show();
					$('#btn-remove').attr('data',$('#gambar').val()+'|'+file_photo_thumb);
					$('.sembunyikan').show();
		
					
				}
			});
		});
		
		$(document).on('click','a[title="Delete"]',function(){
			var $this = $('a[title="Delete"]');
			var index = $this.index($(this));
			if(confirm('Yakin hapus item...?')){
				$.ajax({
					type:'post',
					url:'<?=base_url()?>operator/page/delBarang',
					data:{ 'id':$this.eq(index).attr('data') },
					beforeSend:function(){
						$('#spinner').loading('stop');
						$('#spinner').loading();
					},
					success:function(response){
						$('#spinner').loading('stop');
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
		});
		
		$('#alert1').hide();
		
		$("#kabupaten").select2({
			id: function(e) { return e.id }, 
			placeholder: "kabupaten",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?>operator/ajaxfile/carikabu",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
						
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: kabupatenFormatResult, 
			formatSelection: kabupatenFormatSelection
		});
		
		
		
		$("#alm_kec").select2({
			id: function(e) { return e.id }, 
			placeholder: "Kecamatan",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?>operator/ajaxfile/carikecamatan",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
						kabupatennya : $('#kabupaten').val()
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: kecamatanFormatResult, 
			formatSelection: kecamatanFormatSelection
		});
		$('#kabupaten').on('select2-blur',function(e){
		
			$('#').focus();
			
		});
		$('#alm_kec').on('select2-blur',function(e){
		
			$('#').focus();
			
		});
		
		
		$('#stok_limit').on('keypress',function(e){
			if(e.which==13) $('#idrak').select2('focus');
		});
		
		$('#idrak').on('select2-blur',function(e){
		
			$('#idjenis').select2('focus');
		});
		
		
		$("#alm_kelurahan").select2({
			id: function(e) { return e.id_kelurahan }, 
			placeholder: "Kelurahan",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?>operator/ajaxfile/carikelurahan",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
						kecamatannya : $('#alm_kec').val()
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: rakFormatResult, 
			formatSelection: rakFormatSelection
		});
		
		$("#kbli").select2({
			id: function(e) { return e.id }, 
			placeholder: "KBLI",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?>operator/ajaxfile/carikbli",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
					
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: kbliFormatResult, 
			formatSelection: kbliFormatSelection
		});
		$('#idrak').on('select2-blur',function(e){
			$('#idjenis').select2('focus');
		});
	
		$('#tahun').datepicker({
		format:'yyyy',
		autoclose:true,
		viewMode: 2,
		minViewMode: 2
	}).on('changeDate',function(){
		refreshData();
	});

		
		$(document).on('click','div[rel="prev-photo"]',function(){
			var $this = $('div[rel="prev-photo"]');
			var index = $this.index($(this));
			var ret = $this.eq(index).attr('data').split("|");
			$('#div-detail').html('<center><img src="'+ret[0]+'" /></center>');
			$('#item-desc').html(ret[1]);
			$('#myModal').modal('show',function(){
				keyboard:true
			});
		});
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#id').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
	});

function refreshData(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>operator/page/data/dataikm',
		data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
		beforeSend:function(){
			$('#spinner').hide();
			
			$('#spinner').show();
			//$('#spinner').loading('stop');
			//$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').hide();
			$('#result').html(response);
		}
	});
}
	
function resetPhotoBox(){
	$('#photobox').css({
		'background-image':'url(<?=base_url()?>assets/img/no-photo.jpg)',
		'background-size':'100% auto',
		'background-repeat':'no-repeat',
		'background-position':'center center'
	});	
	$('#btn-remove').hide();
	$('#gambar').val('');
}	
</script>
<ul class="breadcrumb">
  <li><a href="#"><i class="icomoon-settings"></i> Input</a> <span class="divider"></span></li>
  <li class="active">Data IKM</li>
</ul>

<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#update" data-toggle="tab"><i class="icomoon-file3"></i> Update</a></li>
	<li class=""><a href="#daftar" data-toggle="tab"><i class="icomoon-table2"></i> Daftar</a></li>
</ul>

<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
		<br><br>
		<form id="form1" name="form1" action="<?=base_url()?>operator/page/saveikecil"class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="col-md-6">
		<div class="box box-warning">
			<!-- form start -->
			  <div class="box-body">
			
						
						
						<BR><div class="control-group">
							<label class="control-label" for="nama_produk">Nama Produk</label>
							<div class="controls">
								<input type="text" class="form-control"class="form-control"id="nama_produk" name="nama_produk" maxlength="100" required1 placeHolder="Nama Barang" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="nama_perusahaan">Nama Perusahaan</label>
							<div class="controls">
								<input type="text" class="form-control"id="nama_perusahaan" name="nama_perusahaan" maxlength="20" required1 placeHolder="Nama Perusahaan" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="nama_pemilik">Nama Pemilik</label>
							<div class="controls">
								<input type="text" class="form-control"id="nama_pemilik" name="nama_pemilik" maxlength="20" required1 placeHolder="Nama Pemilik" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="kbli">KBLI</label>
							<div class="controls">
								<input type="hidden" id="kbli" name="kbli" style="width:100%"/>
							</div>
						</div>
						
						
						<BR><div class="control-group ">
							<label class="control-label" for="tahun">Tahun  </label>
							<div class="controls">
								<input type="text" class="form-control" id="tahun" name="tahun" maxlength="10" style="width:60px" placeHolder="Tahun" value="<?=date('Y')?>" />
							</div>
						</div>
						<BR><div class="control-group ">
							<label class="control-label" for="tahun">TK LK  </label>
							<div class="controls">
								<input type="text" class="form-control num " id="ngi_tklk" name="ngi_tklk" maxlength="10"  placeHolder=""  />
							</div>
						</div>
						<BR><div class="control-group ">
							<label class="control-label" for="tahun">TK PR  </label>
							<div class="controls">
								<input type="text" class="form-control num" id="ngi_tkpr" name="ngi_tkpr" maxlength="10"  placeHolder=""  />
							</div>
						</div>
					<BR><div class="control-group ">
							<label class="control-label" for="tahun">Izin Usaha </label>
							<div class="controls">
								<input type="text" class="form-control" id="ngi_izinusaha" name="ngi_izinusaha" maxlength="10" placeHolder=""  />
							</div>
						</div>
										
					<BR><div class="control-group ">
							<label class="control-label" for="fileupload">Gambar</label>
							<div class="controls">
								<div id="photobox" style="background-image:url(<?=base_url()?>assets/img/no-photo.jpg);background-size:100% 100%">
									<div style="display:block;margin-top:20px;width:100%;text-align:center">
										<span class="btn btn-success fileinput-button">
											<i class="icomoon-image2"></i>
											<span></span>
											<input id="fileupload" type="file" name="fileupload" />
										</span>
										<span class="btn btn-warning" id="btn-remove">
											<i class="icomoon-remove2"></i>
											<span></span>
										</span>
										<div id="progress_caption" style="color:green;font-size:small"></div>
										<div id="progress" class="progress" style="background-image:none;background-color:green;height:3px;border-radius:none;margin-top:10px;width:0%">
											<div class="progress-bar progress-bar-success"></div>
										</div>
										<input type="hidden" id="gambar" name="gambar" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> 
				</div>
				<!-- end of span6 -->
				<div class="span6 sembunyikan">
					
	<div class="col-md-6">
		<div class="box box-primary">
			<!-- form start -->
			  <div class="box-body">
			   
			  

					<BR><div class="control-group">
							<label class="control-label" for="alm_desa">Alamat Desa</label>
							<div class="controls">
								<input type="text" class="form-control"id="alm_desa" name="alm_desa" maxlength="20" required1 placeHolder="Alamat Desa" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="kabupaten">Kabupaten</label>
							<div class="controls">
								<input type="hidden" id="kabupaten" name="kabupaten" style="width:100%"/>
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="alm_kec">Kecamatan</label>
							<div class="controls">
								<input type="hidden" id="alm_kec" name="alm_kec" style="width:100%"/>
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="alm_kelurahan">Kelurahan</label>
							<div class="controls">
								<input type="hidden" id="alm_kelurahan" name="alm_kelurahan" style="width:100%"/>
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="alm_tlp">No Telp</label>
							<div class="controls">
								<input type="text" class="form-control"id="alm_tlp" name="alm_tlp" maxlength="20" required1 placeHolder="No Telp" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="tenaga_kerja">Tenaga kerja</label>
							<div class="controls">
								<input type="text" class="form-control num" id="tenaga_kerja" name="tenaga_kerja" maxlength="20" required1 placeHolder="Tenaga Kerja" />
							</div>
						</div>
                
						<BR><div class="control-group">
							<label class="control-label" for="nilai_investasi">Nilai Investasi</label>
							<div class="controls">
								<input type="text" class="form-control"id="nilai_investasi" name="nilai_investasi" maxlength="20" required1 placeHolder="Nilai Investasi" />
							</div>
						</div>
                    		
						
						
						<BR><div class="control-group">
							<label class="control-label" for="prod_volume">Jumlah Kapasitas Produksi</label>
							<div class="controls">
								<input type="text" class="form-control num" id="prod_volume" name="prod_volume" maxlength="20"  required1 placeHolder="Volume Produk" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="prod_satuan">Satuan Nilai Produksi</label>
							<div class="controls">
								<input type="text" class="form-control" id="prod_satuan" name="prod_satuan" maxlength="20"  required1 placeHolder="Satuan Produk" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="prod_nilai">Nilai Bahan Baku / Bahan Penolong</label>
							<div class="controls">
								<input type="text" class="form-control num" id="prod_nilai" name="prod_nilai" maxlength="20"  required1 placeHolder="nilai Produk" />
							</div>
						</div>
						
						<BR><div class="control-group">
							<label class="control-label" for="Bahan_jenis">Bahan Jenis</label>
							<div class="controls">
								<input type="text" class="form-control"id="Bahan_jenis" name="Bahan_jenis" maxlength="20" required1 placeHolder="Bahan Jenis" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="bahan_volume">Volume Bahan</label>
							<div class="controls">
								<input type="text" class="form-control num" id="bahan_volume" name="bahan_volume" maxlength="20"  required1 placeHolder="Volume Bahan" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="bahan_satuan">Satuan Bahan</label>
							<div class="controls">
								<input type="text" class="form-control" id="bahan_satuan" name="bahan_satuan" maxlength="20"  required1 placeHolder="Satuan Bahan" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="bahan_nilai">nilai Bahan</label>
							<div class="controls">
								<input type="text" class="form-control num" id="bahan_nilai" name="bahan_nilai" maxlength="20"  required1 placeHolder="nilai Bahan" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="energi_jenis">Jenis Energi</label>
							<div class="controls">
								<input type="text" class="form-control"id="energi_jenis" name="energi_jenis" maxlength="20" required1 placeHolder="Jenis Energi" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="energi_kapasitas">Kapasitas Energi</label>
							<div class="controls">
								<input type="text" class="form-control"id="energi_kapasitas" name="energi_kapasitas" maxlength="20" required1 placeHolder="Kapasitas Energi" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="pemasaran_vol">Volume Pemasaran</label>
							<div class="controls">
								<input type="text" class="form-control num" id="pemasaran_vol" name="pemasaran_vol" maxlength="20"  required1 placeHolder="Volume Pemasaran" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="pemasaran_nilai">nilai Ekspor</label>
							<div class="controls">
								<input type="text" class="form-control num" id="pemasaran_nilai" name="pemasaran_nilai" maxlength="20"  required1 placeHolder="nilai Pemasaran" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="pemasaran_tujuan">Negara Tujuan Ekspor</label>
							<div class="controls">
								<input type="text" class="form-control" id="pemasaran_tujuan" name="pemasaran_tujuan" maxlength="20"  required1 placeHolder="Tujuan Pemasaran" />
							</div>
						</div>
						
						 
						
						<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
					</div>
					
					</div>
				</div>
				<!-- end of span6 -->
			
		</form>
	</div><br>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="pull-right">
				<div class="input-append form-inline">
				  <input type="text" class="form-control" id="cari" name="cari" placeHolder="Cari ..." />
				  <!--div class="btn-group">
					<select class="form-control"  id="per_page" name="per_page" style="width:60px">
					  <option value="">-</option>
					  <option value="4">4</option>
					  <option value="25">25</option>
					  <option value="50">50</option>
					</select>
				  </div-->
				</div>        
				</div>
			 </div>
		 

		<div id="result" >
		<center>
	  <span class="ball-loader loading" id="spinner"></span>
    </center>
				</div>

				<!--<div class="loading" id="spinner">Loading&#8230;</div> -->
				  
				
				</div>
				
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 id="myModalLabel"><a id="btn-print2" title="Item Preview"><i class="icomoon-image2"></i></a> Item Preview <span id="item-desc"></span></h4>
  </div>
  <div class="modal-body">
    <div id="div-detail">
	
	</div>
  </div>
</div>
