<link rel="stylesheet" href="<?=$def_js?>plugins/growl/css/jquery.growl.css">
<script type="text/javascript" src="<?=$def_js?>plugins/growl/js/jquery.growl.js"></script>

<script>
	
	
	$(document).ready(function(){
		
		
		
		
		
		$('.dt').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		});
		//showFaktur();
		
		$('.dt').mask('99-99-9999');
  	
		$(document).on('change','#checkall',function(){
			$('input[name*="checkthis"]').prop('checked',$(this).prop('checked'));
		});
		
		$(document).on('change','#checkallppn',function(){
			$('input[name*="withppn"]').prop('checked',$(this).prop('checked'));
		});
		
		$(document).on('submit','#form-posting',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					showFaktur('');
					
				});		
			}else{
				showFaktur('');
			}
		});
		
		$(document).on('reset','#form-posting',function(){
			resetForm($('#form-posting'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
		});
		
		showFaktur('');
		
		$(document).on('submit','#form-posting2',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					showFaktur2('');
					
				});		
			}else{
				showFaktur2('');
			}
		});
		
		$(document).on('reset','#form-posting2',function(){
			resetForm($('#form-posting2'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
		});
		
		
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					showFaktur('');
				break;
				case 1:
					showFaktur2('');
				break;
				case 2:
					showFaktur3('');
				break;
				
			}
		});
		
		showSetting();
	});
	
	function showFaktur(){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>auth/page/data/confirmmember',
			data:{ '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			 },
			beforeSend:function(){
				$('#result').loading('stop');
				$('#result').loading();
			},
			success:function(response){
				$('#result').loading('stop');
				$('#result').html(response);
			}
		});
	}
	
	$(document).on('click','a.nohref',function(){
		var $this = $('a.nohref');
		var index = $this.index($(this));
		var vdata = $this.eq(index).attr('data').split("|");
		$.ajax({
			url:'<?=base_url()?>auth/page/view/test',
			type:'post',
			data:{ 
				'tgl': vdata[0],
				'pos': vdata[1],
			},
			beforeSend:function(){},
			success:function(response){
				try{
				$('#myModal-detail').html(response);
				$('#pos').html(((vdata[1]==1)?"Left":"Right"));
				$('#myModal').modal();
				$('#myModal').modal('show',function(){
					keyboard:false
				});
				}catch(e){ alert(e.message); }
				
				return false;
			}
		});
	});
	

	function showFaktur2(vid){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>auth/page/data/confirmmember2',
			data:{'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			  },
			beforeSend:function(){
				$('#result2').loading('stop');
				$('#result2').loading();
			},
			success:function(response){
				$('#result2').loading('stop');
				$('#result2').html(response);
			}
		});
	}
	
	
</script>
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#posting" data-toggle="tab"><i class="icon-pencil"></i> Pasien Mendaftar  </a></li>
	<li class=""><a href="#posting2" data-toggle="tab"><i class="icon-pencil"></i> Pasien Konfirmasi  </a></li>
</ul><br>
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="posting">
       <div class="row-fluid">
			<div class="span12">
				
				<form name="form-posting" id="form-posting" method="post" action="<?=base_url()?>auth/page/posting" enctype="multipart/form-data">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

					<div class="row-fluid">
						<div class="pull-right">
							<div class="spinner"></div>
							<input type="submit" id="submit" name="submit" value="POSTING" class="btn btn-primary" />
							<a target="_blank" href="<?=base_url()?>auth/page/pdf/posting?size=A4&o=L"><input type="button" id="btn-export" value="Cetak PDF" class="btn btn-small" />
</a>
						</div>
					</div>
					<div id="result"  style="overflow:auto;width:100%">
					
					</div>
				</form>
			</div>
	   </div>
	</div>
	<div class="tab-pane" id="posting2">
       <div class="row-fluid">
			<div class="span12">
				
				<form name="form-posting2" id="form-posting2" method="post" action="<?=base_url()?>auth/page/unposting" enctype="multipart/form-data">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

					<div class="row-fluid">
						<div class="pull-right">
							<div class="spinner"></div>
							<input type="submit" id="submit" name="submit" value="Kembalikan Posting" class="btn btn-warning" />
							<a target="_blank" href="<?=base_url()?>auth/page/pdf/unposting?size=A4&o=L"><input type="button" id="btn-export" value="Cetak PDF" class="btn btn-small" />
</a>
						</div>
					</div>
					<div id="result2" style="overflow:auto;width:100%">
					
					</div>
				</form>
			</div>
	   </div>
	</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Pasien Mendaftar <span id="pos"></span></h4>
      </div>
      <div class="modal-body">
        <div id="myModal-detail"></div>
		<p>&nbsp;</p>
      </div>
    </div>
  </div>
</div>	
