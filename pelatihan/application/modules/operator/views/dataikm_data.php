<style>
	.photo-thumb{
		float:left;border:1px solid #ccc;margin-right:5px;background-size:100% auto;background-repeat:no-repeat;background-position:center center;width:50px;height:50px;cursor:pointer;
	}
</style>
     
     
<script>
$(document).ready(function(){
	$('#alert2').hide();
	
});
</script>

<div id="alert2" class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Sukses!</strong> Data telah dihapus
</div>
<?php
	$where = " ";
	$per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= "and (a.nama_perusahaan like '%".$cari."%' or a.kbli like '%".$cari."%' )";
	#echo $where;
	
	$n = intval($this->uri->segment(5));
	$q = "select a.*,b.nama,a.id as id from ikecil a
		  left join kbli b on a.kbli=b.id where a.kabupaten='".$this->session->userdata('kabupaten')."' 
	";
	$rs = $this->db->query("$q $where");
	$row = $rs->row_array();
	$totrows = $rs->num_rows();

	$config['base_url'] = base_url().'operator/page/data/dataikm';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("$q $where order by a.nama_perusahaan limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
</div>
<img id="noimage" style="display:none" />
<br/><br/>
<table class="table table-hover">
  <colgroup>
	
	<col class="con0" style="width:auto" />
	<col class="con0" style="width:30%" />
	<col class="con0" style="width:auto" />
  </colgroup>
  <thead class="breadcrumb">
	<tr>
	  <th>#</th>
	  <th>NAMA PERUSAHAAN</th>
	  <th>NAMA KBLI</th>
	 
	  <th><div class="text-center">PROSES</div></th>
	</tr>
  </thead>
  <tbody>
	<?php
	foreach($data['posts']->result() as $item){ $n++;
	
	?>
	<tr>
	  <td align="center"><?=$n?>.</td>
	  <td><div class="text-left"><?=$item->nama_perusahaan?></div></td>
	  <td><div class="text-left"><?=$item->nama?></div></td>
	  <td>
		<div class="text-center">
			<a href="#" title="Edit" data="<?=$item->id?>"><i class="icomoon-pencil2"></i></a>
			<a href="#" title="Delete" data="<?=$item->id?>"><i class="icomoon-remove red"></i></a>
		</div>
	  </td>
	</tr>
	<?php
	}
	?>
  </tbody>
</table>
<span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
<?=$data['pagination']?>
