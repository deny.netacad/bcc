<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $modules = "operator";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auth/user');
		$this->load->model('auth/auth_list');
		
	}

	function index(){
		if ($this->session->userdata('is_login_rsudikm'))
		{
			$this->load->view($this->modules.'/main',$this->data);
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}
	
	function saveGantipass(){
		$data['ID'] = $this->session->userdata('iduser');
		$data['PASSWORD'] = md5($this->input->post('userpass'));
		$rs = $this->db->get_where("userlogin",$data);
		if($rs->num_rows()>0){
			if($this->input->post('userpass2')==$this->input->post('userpass3')){
				$dt['PASSWORD'] = md5($this->input->post('userpass2'));
				if(!$this->db->update("userlogin",$dt,$data)){
					$ret['text'] = "Terjadi Kesalahan\n".$this->db->_error_message();
				}else{
					$ret['text'] = "Password telah diganti";
				}
			}else{
				$ret['text'] = "Password Perulangan salah";
			}
		}else{
			$ret['text'] = "Password lama salah";
		}
		echo json_encode($ret);
	}
	
	function posting(){
		$error = false;
		$ret['text'] = "Data Pendaftaran berhasil diposting..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		#yang di update
		$dataflag['logkonfirm'] = date('Y-m-d h:i:s');

		$dataflag['username_konfir'] = $this->session->userdata('username');				
		$dataflag['is_posting'] = 1;
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					#syarat Update
					$syarat['no_rm'] = $data['no_rm'];
					#$databonus['tgl'] = $data['tgl'];
					$syarat['is_posting'] = 0;
					$this->db->update("ngi_pasien_online",$dataflag,$syarat);
					/*$this->db->query("update ngi_daily_statemen set is_posting=1 , logterahir=NOW() where
					member_id='".$data['member_id']."' and tgl<date(NOW()) and is_posting=0 and (jumlah-jumlah_potongan>0)");*/
					
				}
			}
		}
		echo json_encode($ret);
	}
	
	function unposting(){
		$error = false;
		$ret['text'] = "Data Pendaftaran berhasil dikembalikan..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		$dataflag['logkonfirm'] = date('Y-m-d h:i:s');			
		$dataflag['is_posting'] = 0;
		$dataflag['username_konfir'] = $this->session->userdata('username');				
		
		
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					$datadel2['no_rm'] = $data['no_rm'];
					//$datadel2['tgl'] = $data['tgl'];
					//$datadel2['logterahir'] = date('Y-m-d h:i:s');
					$datadel2['is_posting'] = 1;
					$this->db->update("ngi_pasien_online",$dataflag,$datadel2);
				}
			}
		}
		
		echo json_encode($ret);
	}

	function savePasienbaru(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keynum		= array("");
		$keyin		= array();
		$keyout		= array();
		
		$data['log_daftar'] = date('Y-m-d h:i:s');
		$data['id_user'] = $this->session->userdata('username');
		
		$this->operator_mod->save("tb_pasien",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,array(),$keynum);
		
		
	}
	
	function savePeriksa(){
		$arrnot 	= array("","id_edit","keluhan_awal1","keluhan_awal2","keluhan_awal3","keluhan_awal");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keynum		= array("");
		$keyin		= array();
		$keyout		= array();
		$data['tgl_periksa']=date('Y-m-d h:i:s');
		$data['keluhan_awal']="".$this->input->post('keluhan_awal')."|".$this->input->post('keluhan_awal1')."|".$this->input->post('keluhan_awal2')."|".$this->input->post('keluhan_awal3')."";
		
		
		$this->operator_mod->save("tb_periksa",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,array(),$keynum);
	}
	
	
	function saveDetailanggaran(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keynum		= array("","nilai");
		$keyin		= array();
		$keyout		= array();
		$this->operator_mod->save("ngi_detail_anggaran",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	
	
	
	function delDetailanggaran(){
		$ret['err']		= false;
		$ret['text']	= "Berhasil Dihapus";
		$q = "update ngi_detail_anggaran set flag='1' where id='".$this->input->post('id')."'";
		$rs = $this->db->query($q);
		echo json_encode($ret);
	}
	
	
	function delManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("mast_module",$data)){
			echo "Terjadi Kesalahan";
		}else{
			echo "Data Terhapus";
		}
	}
	function delmobilpelanggan(){
		$ret['err']		= false;
		$ret['text']	= "Berhasil Dihapus";
		$q = "update mobilpelanggan set flag='1' where id='".$this->input->post('id')."'";
		$rs = $this->db->query($q);
		echo json_encode($ret);
	}
	function editManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_module",$data);
		echo json_encode($rs->row());
	}
	
	function saveManmodul(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","idmodule");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->operator_mod->save("mast_module",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}
		
	function saveMobil(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->operator_mod->save("mobilpelanggan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}
	
		function saveAplikasi(){
		$arrnot 	= array("","id_edit","member_pass","member_captcha");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","member_id");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		
		$expiration = time()-600; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['member_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();

if ($row->count == 0)
{
			$ret['error'] = true;
			$ret['txt'] = "Terjadi Kesalahan";
			echo json_encode($ret);
				exit;
}else{
		$data['member_pass']=md5($this->input->post('member_pass'));
		$this->operator_mod->save("ngi_member",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data);
	}
	}
	
	
	function saveManhakakses(){
		$ret['error'] = false;
		$ret['txt'] = "Data Tersimpan";
		$arrnot = array("");
		
		$data['iduser'] = $this->input->post('iduser');
		$this->db->delete("mast_usermodule",$data);
		$this->db->delete("mast_default_module",$data);
		$data2['iduser'] = $data['iduser'];
		$data2['idmodule'] = $this->input->post('iddefault');
		if(!$this->db->insert("mast_default_module",$data2)){
			$ret['error'] = true;
			$ret['txt'] = "Terjadi Kesalahan";
			exit;
		}
		foreach($_POST['idmodule'] as $key=>$value){
			$data['idmodule'] = $value;
			$data['d'] = number_format($_POST['d'][$key],0,"","");
			$data['e'] = number_format($_POST['e'][$key],0,"","");
			$data['v'] = number_format($_POST['v'][$key],0,"","");
			$data['i'] = number_format($_POST['i'][$key],0,"","");
			if(!$this->db->insert("mast_usermodule",$data)){
				$ret['error'] = true;
				$ret['txt'] = "Terjadi Kesalahan";
				exit;
			}
		}
		echo json_encode($ret);
	}
	
	
	function editManuser(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_user",$data);
		echo json_encode($rs->row());
	}
	
	function saveManuser(){
		$arrnot 	= array("","id_edit","userpass"); # inputan yang tidak ada di table
		$keyedit 	= array("","id_edit"); # index untuk keperluan edit data
		$keyindex 	= array("","iduser"); 
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		# custom value
		$data['userpass_ori'] = $this->input->post('userpass');
		$data['userpass'] = md5($this->input->post('userpass'));
		$this->operator_mod->save("mast_user",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data);
	}
	
	function delManuser(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("mast_user",$data)){
				$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operator_mod->del_log("mast_user",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operator_mod->del_log("mast_user",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	function cekDATA2(){
		foreach($_POST as $key=>$value){
			if($key!='tablename') $data[$key] = $value;
		}
		$rs = $this->db->get_where($_POST['tablename'],$data);
		echo $rs->num_rows();
	}
	
	function cekDATA(){
	$datanya = $_POST['datanya'];
	$field=$_POST['fieldnya'];
	$table=$_POST['tablenya'];
	
	$rs = $this->db->query("select * from $table where $field =\"".$datanya."\"");
	if($rs->num_rows() > 0){
		$data = "0";
	}	
	else if($datanya==""){
	$data = "0";
	}
	else{
		$data = "1";
	}
	
	//echo $sql;
	echo $data;
	}
	
	
	function saveId(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","kode_toko");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$this->db->delete("identitas");
		$this->operator_mod->save("identitas",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function dataId(){
		$rs = $this->db->get("identitas");
		echo json_encode($rs->row());
		
	}
	
	# todo save rak
	function saveRak(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->operator_mod->save("m_rak",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),array(),$keynum);
	}
	
	function editRak(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "m_rak";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}
	
	function delRak(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("m_rak",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operator_mod->del_log("m_rak",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operator_mod->del_log("m_rak",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	#jenis
	
	function savejenis(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->operator_mod->save("m_jenis",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),array(),$keynum);
	}
	
	function editjenis(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "m_jenis";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}
	
	function deljenis(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("m_jenis",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operator_mod->del_log("m_jenis",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operator_mod->del_log("m_jenis",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	
	
	# todo save satuan
	function saveSatuan(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->operator_mod->save("m_satuan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editSatuan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "m_satuan";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}
	
	function delSatuan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("m_satuan",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operator_mod->del_log("m_satuan",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operator_mod->del_log("m_satuan",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	# todo save barang
	function saveBarang(){
		$arrnot 	= array("","id_edit","prosen");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("","harga_beli","harga_jual","stok","stok_limit","rak","point_bv","ngi_harga_jual_non","ngi_harga_jual_stokis","ngi_harga_jual_m_stokis");
		$keyout		= array();
		
		$this->operator_mod->save("barang",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editBarang(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data["a.".$key] =  $value;
		}
		$rs = $this->db->query("SELECT *,
		a.id AS id ,
		b.id AS id_kabx,
		c.id AS id_kecx,
		d.id_kelurahan AS id_kelx,
		d.kelurahan,
		e.id as id_kblix,
		e.nama,
		e.tahun
		
		
		FROM ikecil a
		LEFT JOIN kabupaten b ON a.kabupaten=b.id
		LEFT JOIN kecamatan c ON a.alm_kec=c.id
		LEFT JOIN kelurahan d ON a.alm_kelurahan=d.id_kelurahan
		LEFT JOIN kbli e ON a.kbli=e.id
		
		
		WHERE a.id='".$this->input->post('id')."'
		");
		echo json_encode($rs->row());
	}
	
	function savePaket(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","pack_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("","pack_bonus","pack_point","pack_limit_tf");
		$keyout		= array();
		
		$this->operator_mod->save("ngi_package",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function saveikecil(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array("");
		$keynum		= array("","tkpr","tklk");
		$keyout		= array("");
		
		$this->operator_mod->save("ikecil",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editPaket(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data["a.".$key] =  $value;
		}
		$rs = $this->db->query("select * from ngi_package a left join barang b on a.idbarang=b.id
		where a.pack_id='".$this->input->post('id')."'
		");
		echo json_encode($rs->row());
	}
	
	function delPaket(){
		$ret['text'] = "Data Berhasil Di Hapus";
		$rs=$this->db->query("update ngi_package set pack_isactive='0' where pack_id='".$this->input->post('id')."'");
		echo json_encode($ret);
	}
	
	
	function delBarang(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("ikecil",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("ikecil",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
				$foto_barang = explode(".",$item->gambar);
				$filepath = BASEPATH."../upliddir/".$item->gambar;
				$filepath2 = BASEPATH."../upliddir/".$foto_barang[0]."_thumb.".$foto_barang[1];
				if(file_exists($filepath)) unlink($filepath);
				if(file_exists($filepath2)) unlink($filepath2);
			}
			$this->operator_mod->del_log("ikecil",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operator_mod->del_log("ikecil",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	function delPhoto(){
		
		$files = explode("|",$this->input->post('filename'));
		$filename = BASEPATH.'../upliddir/'.$files[0];
		$filename1 = BASEPATH.'../upliddir/'.$files[1];
		$filename2 = BASEPATH.'../upliddir/'.$files[2];
		
		if(file_exists($filename)) unlink($filename);
		if(file_exists($filename1)) unlink($filename1);
		if(file_exists($filename2)) unlink($filename2);
	}
	
	# todo save supplier
	function saveSupplier(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("");
		$keyout		= array();
		
		$this->operator_mod->save("supplier",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editSupplier(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("supplier",$data);
		echo json_encode($rs->row());
	}
	
	function delSupplier(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("supplier",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("supplier",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operator_mod->del_log("supplier",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operator_mod->del_log("supplier",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	# todo save pelanggan
	function savePelanggan(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("");
		$keyout		= array();
		
		$this->operator_mod->save("pelanggan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editPelanggan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("pelanggan",$data);
		echo json_encode($rs->row());
	}
	
	function delPelanggan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("pelanggan",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("pelanggan",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operator_mod->del_log("pelanggan",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operator_mod->del_log("pelanggan",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
}