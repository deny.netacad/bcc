<?php

class Ajaxfile extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	
	
	function findpackage(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " and 
			(
			pack_name like '%".strtolower($keyword)."%' or 
			pack_id like '%".$keyword."%' 
			)
		";
		
		$rs = $this->db->query("select * from ngi_package where pack_isactive='1' $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ngi_package where pack_isactive='1' $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function findsponsor(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " 
			(
			member_name like '%".strtolower($keyword)."%' or 
			member_code like '%".$keyword."%' 
			)
		";
		
		$rs = $this->db->query("select * from ngi_member where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select a.*,b.pack_name from ngi_member a 
		inner join ngi_package b on a.pack_id=b.pack_id
		where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function findDownline(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$temp = $this->session->userdata('iduser');
		$where = " 
			
			(FIND_IN_SET(".$temp.",a.member_upline) and a.member_dwl is NULL and b.member_name like '%".$keyword."%') or  b.member_id='".$temp."' 
 
			
		";
		
		$rs = $this->db->query("select a.member_id,b.*,c.pack_name from ngi_member_foot a 
inner join ngi_member b on a.member_id=b.member_id
inner join ngi_package c on b.pack_id=c.pack_id
 where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select a.member_id,b.*,c.pack_name from ngi_member_foot a 
inner join ngi_member b on a.member_id=b.member_id
inner join ngi_package c on b.pack_id=c.pack_id

		where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function findDownlineAktifasi(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " 
			(
			member_name like '%".strtolower($keyword)."%' or 
			member_code like '%".$keyword."%' 
			)
		";
		
		$rs = $this->db->query("select * from ngi_member where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select a.*,b.pack_name,date_format(member_joindate,'%d %M %Y %h:%i:%s') as member_joindate from ngi_member a 
		inner join ngi_package b on a.pack_id=b.pack_id
		where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	
	
	
	
	
	
	
	
	function cariprovinsi(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " kdprovinsi like '%".$keyword."%' or provinsi like '%".$keyword."%'";
		
		$rs = $this->db->query("select * from mast_pro where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from mast_pro where $where limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
		
	}
	
	
	
	function caribarang(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " ( a.nama_barang like '%".$keyword."%' or a.kode_barang like '%".$keyword."%')";
		$q = "select a.*,format(a.harga_beli,0) as harga_beli,b.satuan
		from barang a
		left join m_satuan b on a.idsatuan=b.id
		where $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function caripaket(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " and ( nama_barang like '%".$keyword."%' or kode_barang like '%".$keyword."%')";
		$q = "select * from barang where idjenis='15' $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function carikabkota(){
		$keyword 	= $this->input->post('keyword');
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where (a.kabkota like '%".$keyword."%' or a.kdkabkota like '%".$keyword."%')";
		$q = "select a.*,b.provinsi from mast_kabkota a 
		inner join mast_pro b on left(a.kdkabkota,2)=b.kdprovinsi
		$where";
		$rs = $this->db->query("$q");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
	}
	
	function carikec(){
		$keyword 	= $this->input->post('keyword');
		$kdkabkota = $this->input->post('kdkabkota');
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " left(kdkec,5)='".$kdkabkota."' and (kec like '%".$keyword."%' or kdkec like '%".$keyword."%')";
		
		$rs = $this->db->query("select * from mast_kec where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from mast_kec where $where limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
	}
	
	function caripelanggan(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.member_name like '%".$keyword."%' or a.member_code like '%".$keyword."%' ";
		$q = "SELECT * from ngi_member a $where order by a.member_name ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function carikabu(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.kabupaten like '%".$keyword."%' ";
		$q = "SELECT * from kabupaten a $where order by a.kabupaten ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
		function carikecamatan(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.kecamatan like '%".$keyword."%' and id_kab='".$this->input->post('kabupatennya')."'";
		$q = "SELECT * from kecamatan a $where order by a.kecamatan ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function caripasien(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " 
			(
			pasien_nama like '%".strtolower($keyword)."%' or 
			pasien_alamat like '%".strtolower($keyword)."%' 
			)
		";
		
		$rs = $this->db->query("select * from tb_pasien where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select a.* from tb_pasien a 
		where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	
	function caridokter(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " 
			(
			dokter_nama like '%".strtolower($keyword)."%' or 
			dokter_alamat like '%".strtolower($keyword)."%' 
			)
		";
		
		$rs = $this->db->query("select * from tb_dokter where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select a.* from tb_dokter a 
		where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
		function carikelurahan(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.kelurahan like '%".$keyword."%'and id_kec='".$this->input->post('kecamatannya')."' ";
		$q = "SELECT * from kelurahan a $where order by a.kelurahan ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function carikbli(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.nama like '%".$keyword."%' or a.kbli like '%".$keyword."%'";
		$q = "SELECT * from kbli a $where order by a.nama ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function carisatuan(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.satuan like '%".$keyword."%' or a.kode like '%".$keyword."%' ";
		$q = "SELECT a.id,a.satuan from m_satuan a $where order by a.satuan ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function carijenis(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.jenis like '%".$keyword."%' or a.kode like '%".$keyword."%' ";
		$q = "SELECT a.id,a.jenis from m_jenis a $where order by a.jenis ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	function carirak(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.rak like '%".$keyword."%' or a.kode like '%".$keyword."%' ";
		$q = "SELECT a.kode,a.rak,a.id from m_rak a $where order by a.rak ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function cariKasir(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.nama like '%".$keyword."%' or a.username like '%".$keyword."%' ";
		$q = "SELECT * from mast_user a $where order by a.username ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	
	
}
?>