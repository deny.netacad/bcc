<?php

class Ajaxfile extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function cariprovinsi(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " kdprovinsi like '%".$keyword."%' or provinsi like '%".$keyword."%'";
		
		$rs = $this->db->query("select * from mast_pro where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from mast_pro where $where limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
		
	}
	
	function carikabkota(){
		$keyword 	= $this->input->post('keyword');
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where (a.kabkota like '%".$keyword."%' or a.kdkabkota like '%".$keyword."%')";
		$q = "select a.*,b.provinsi from mast_kabkota a 
		inner join mast_pro b on left(a.kdkabkota,2)=b.kdprovinsi
		$where";
		$rs = $this->db->query("$q");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
	}
	
	function carikec(){
		$keyword 	= $this->input->post('keyword');
		$kdkabkota = $this->input->post('kdkabkota');
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " left(kdkec,5)='".$kdkabkota."' and (kec like '%".$keyword."%' or kdkec like '%".$keyword."%')";
		
		$rs = $this->db->query("select * from mast_kec where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from mast_kec where $where limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
	}
	
	function carikel(){
		$keyword 	= $this->input->post('keyword');
		$kdkec = $this->input->post('kdkec');
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " left(kdkel,9)='".$kdkec."' and (kel like '%".$keyword."%' or kdkel like '%".$keyword."%')";
		
		$rs = $this->db->query("select * from mast_kel where $where ");
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from mast_kel where $where limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
	}
	
	
	
	
	/*function cariskpd2(){
		$keyword 	= $this->input->post('q');
		
		$per_page 	= intval($this->input->post('page_limit'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " having skpd like '%".$keyword."%' or a.idskpd like '%".$keyword."%' ";
		$q = "SELECT a.idskpd,a.skpd FROM skpd a 
		$where
		ORDER BY a.idskpd";
		$rs = $this->db->query($q);
		
		$arr['total'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
	}*/
	
	
	function caripegawai(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.nama like '%".$keyword."%' or a.nip like '%".$keyword."%' ";
		$q = "SELECT a.nip,a.nama from tb_01 a $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $page,$per_page");
		$arr['rows'] = $rs->result();
		echo json_encode($arr);
	}
	
	function cariijin(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where a.namaijin like '%".$keyword."%' or a.id like '%".$keyword."%' ";
		$q = "SELECT a.id,a.namaijin from mast_ijin a $where order by a.namaijin ";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	
	
}
?>