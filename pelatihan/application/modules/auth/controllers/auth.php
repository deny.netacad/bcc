<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends MY_Controller {
	var $modules = "auth";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('User');
	}
	
	function index(){
		if ($this->session->userdata('is_login_disperindag'))
		{
			redirect('dashboard');
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}

	function login(){		
		$this->User->login();
		redirect('dashboard');
	}
	
	function logout(){
		$this->User->logout();
		$this->session->sess_destroy();		
		redirect('index.php/login');
	}

	
	function graphStatistik(){		
		$rs = $this->db->query("
		SELECT LEFT(a.kdakun,1) AS kdakun,a.namaakun,IFNULL(SUM(IF(a.normalbalance='d',IFNULL((b.d-b.k),0)+a.saldoawal_d,
		IFNULL((b.k-b.d),0)+a.saldoawal_k
		)),0) AS jml_1 FROM mast_akun a 
		LEFT JOIN keuangan_trans b ON a.kdakun=LEFT(b.kdakun,1) and year(b.tgtran)<='".$this->input->post('thn')."'
		GROUP BY LEFT(a.kdakun,1)
		");
		echo "Akun,Jumlah\n";
		foreach($rs->result() as $item){
			echo $item->namaakun.",".$item->jml_1."\n";		
		}
	}

}