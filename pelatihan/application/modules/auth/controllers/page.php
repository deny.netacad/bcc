<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $modules = "auth";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auth/user');
		
	}

	function index(){
		if ($this->session->userdata('is_login_eticketing'))
		{
			$this->load->view('index',$this->$data);
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}

	
	function delManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("mast_module",$data)){
			echo "Terjadi Kesalahan";
		}else{
			echo "Data Terhapus";
		}
	}
	
	function editManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_module",$data);
		echo json_encode($rs->row());
	}
	
	function saveManmodul(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","idmodule");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->auth_mod->save("mast_module",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}
	
	
	
	function saveManhakakses(){
		$ret['error'] = false;
		$ret['txt'] = "Data Tersimpan";
		$arrnot = array("");
		
		$data['iduser'] = $this->input->post('iduser');
		$this->db->delete("ref_usermodule",$data);
		$this->db->delete("mast_default_module",$data);
		$data2['iduser'] = $data['iduser'];
		$data2['idmodule'] = $this->input->post('iddefault');
		if(!$this->db->insert("mast_default_module",$data2)){
			$ret['error'] = true;
			$ret['txt'] = "Terjadi Kesalahan";
			exit;
		}
		foreach($_POST['idmodule'] as $key=>$value){
			$data['idmodule'] = $value;
			$data['d'] = number_format($_POST['d'][$key],0,"","");
			$data['e'] = number_format($_POST['e'][$key],0,"","");
			$data['v'] = number_format($_POST['v'][$key],0,"","");
			$data['i'] = number_format($_POST['i'][$key],0,"","");
			if(!$this->db->insert("ref_usermodule",$data)){
				$ret['error'] = true;
				$ret['txt'] = "Terjadi Kesalahan";
				exit;
			}
		}
		echo json_encode($ret);
	}
	
	
	function editManuser(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("ref_user",$data);
		echo json_encode($rs->row());
	}
	
	function saveManuser(){
		$arrnot 	= array("","id_edit","userpass"); # inputan yang tidak ada di table
		$keyedit 	= array("","id_edit"); # index untuk keperluan edit data
		$keyindex 	= array("","iduser"); 
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		# custom value
		$data['userpass_ori'] = $this->input->post('userpass');
		$data['userpass'] = md5($this->input->post('userpass'));
		$this->auth_mod->save("ref_user",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data);
	}
	
	function saveGantipass(){
		
		$data['user_id'] = $this->session->userdata('user_id');
		$data['user_pass'] = md5($this->input->post('userpass'));
		$rs = $this->db->get_where("ref_user",$data);
		if($rs->num_rows()>0){
			if($this->input->post('userpass2')==$this->input->post('userpass3')){
				$dt['user_pass'] = md5($this->input->post('userpass2'));
				if(!$this->db->update("ref_user",$dt,$data)){
					$ret['text'] = "Terjadi Kesalahan\n".$this->db->_error_message();
				}else{
					$ret['text'] = "Password telah diganti";
				}
			}else{
				$ret['text'] = "Password Perulangan salah";
			}
		}else{
			$ret['text'] = "Password lama salah";
		}
		echo json_encode($ret);
	}
	
	

}