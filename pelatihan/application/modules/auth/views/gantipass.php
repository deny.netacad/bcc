<script>
$(document).ready(function(){
	
	$('#alert1').hide();	
	$('#userpass').focus();
	$(document).on('submit','#form1',function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		
		var url = $form.attr( 'action' );
		
		if(confirm('ubah password ..?')){
			$.post(url, $form.serialize(),
			function(data) {
				window.location = "#";
				var ret = $.parseJSON(data);
				alert(ret.text);
				$('#form1').trigger('reset');
			});		
		}else{
			$('#form1').trigger('reset');
		}
	});
	
	$(document).on('reset','#form1',function(){
		try{
		resetForm($('#form1'));
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('#userpass').focus();
		}catch(e){ alert(e); }
	});
});
</script>
<ul class="breadcrumb">
  <li><a href="#">Ganti Password</a> <span class="divider"></span></li>
</ul>

<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#update" data-toggle="tab"><i class="icon-pencil"></i> Update</a></li>
</ul>

<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<div id="alert1" class="alert alert-success">
		 
		</div>
		
		<form id="form1" name="form1" action="<?=base_url()?>auth/page/saveGantipass" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<div class="row">
				<div class="col-md-6">
						<div class="form-group">
							<label for="userpass">Password Sekarang</label>
							<input type="password" id="userpass" name="userpass" class="form-control" maxlength="100" required placeHolder="Password Sekarang" />
						</div>
						<div class="form-group">
							<label for="userpass2">Password Baru</label>
							<input type="password" id="userpass2" name="userpass2" class="form-control" maxlength="100" required placeHolder="Password Baru" />
						</div>
						<div class="form-group">
							<label for="userpass3">Ulangi Password Baru</label>
							<input type="password" id="userpass3" name="userpass3" class="form-control" maxlength="100" required placeHolder="Ulangi Password Pengganti" />
						</div>
						<div class="form-group">
							<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
							<input type="reset" class="btn" value="Batal" />
						</div>
				</div> <!-- end of span5 -->
			</div>
		</form>
	</div>
</div>