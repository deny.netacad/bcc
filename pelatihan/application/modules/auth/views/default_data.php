<script>
var options = {
    chart: {
        renderTo: 'graph1',
        defaultSeriesType: 'column'
    },
    title: {
        text: 'Financial Graph'
    },
    subtitle: {
        text: 'Source: finance.waterpark.com'
    },
    xAxis: {
        categories: []
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    legend: {
        layout: 'vertical',
        backgroundColor: '#FFFFFF',
        align: 'left',
        verticalAlign: 'top',
        x: 100,
        y: 70,
        floating: true,
        shadow: true
    },
    series: []
};
$.post('<?=base_url()?>auth/graphStatistik', {
    'thn': '<?=$this->input->post('thn')?>'
}, function (data) {
    var lines = data.split('\n');
    $.each(lines, function (lineNo, line) {
        var items = line.split(',');
        if (line != '') {
            if (lineNo == 0) {
                $.each(items, function (itemNo, item) {
                    if (itemNo > 0) options.xAxis.categories.push(item);
                });
            } else {
                var series = {
                    data: []
                };
                $.each(items, function (itemNo, item) {
                    if (itemNo == 0) {
                        series.name = item;
                    } else {
                        series.data.push(parseFloat(item));
                    }
                });
                options.series.push(series);
            }
        }
    });
    var chart = new Highcharts.Chart(options);
});
</script>
<div id="graph1"></div>