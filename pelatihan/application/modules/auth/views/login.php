<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?=$def_css?>bootstrap3/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=$def_css?>font-awesome/css/font-awesome.min.css">
<style>
/* Snippet One */
html{
	min-height:100%;
}
body {
	margin:0;
		
		background:url(<?=$def_img?>front1.jpg) no-repeat center center;
		background-size:100% 100%;
	
}

#wrapperlogo{
	background:url(<?=base_url()?>assets/images/logo-sampang-large-transp.png) no-repeat center center;
	background-size:auto 100%;
}

fa{
	display: inline-block;
	font-family: icomoon;
	font-style: normal;
	font-weight: normal;
	line-height: 1;
	-webkit-font-smoothing: antialiased;
}

div.member_signin {
    text-align: center;
	-webkit-animation: bounce 800ms ease-out;
	-moz-animation: bounce 800ms ease-out;
	-o-animation: bounce 800ms ease-out;
	animation: bounce 800ms ease-out;
	background-size:100% 100%;
	background:rgba(255,255,255,0.7);
}

div.member_signin  i.fa.fa-user {
    color: #FFF;
    background-color: #D50000;
    border-radius: 500px;
    font-size: 36px;
    padding: 15px 20px 15px 20px;
}

div.fa_user {
    margin-top: -47px;
    margin-bottom: 15px;
}

p.member {
    font-size: 19px;
    color: #888888;
    margin-bottom: 20px;
}

button.login {
    width: 100%;
    text-transform: uppercase;
}

form.loginform div.input-group {
    width: 100%;
}

form.loginform input[type="text"], form.loginform input[type="password"] {
    color: #6C6C6C;
    text-align: left;
}

p.forgotpass {
    margin-top: 10px;
}

p.forgotpass a {
    color: #F5683D;
}


/* Webkit, Chrome and Safari */

@-webkit-keyframes bounce {
  0% {
	-webkit-transform:translateY(-100%);
    opacity: 0;
  }
  5% {
  	-webkit-transform:translateY(-100%);
    opacity: 0;
  }
  15% {
  	-webkit-transform:translateY(0);
    padding-bottom: 5px;
  }
  30% {
  	-webkit-transform:translateY(-50%);
  }
  40% {
  	-webkit-transform:translateY(0%);
    padding-bottom: 6px;
  }
  50% {
  	-webkit-transform:translateY(-30%);
  }
  70% {
  	-webkit-transform:translateY(0%);
    padding-bottom: 7px;
  }
  80% {
  	-webkit-transform:translateY(-15%);
  }
  90% {
  	-webkit-transform:translateY(0%);
  	padding-bottom: 8px;
  }
  95% {
  	-webkit-transform:translateY(-10%);
  }
  97% {
  	-webkit-transform:translateY(0%);
  	padding-bottom: 9px;
  }
  99% {
  	-webkit-transform:translateY(-5%);
  }
  100% {
  	-webkit-transform:translateY(0);
  	padding-bottom: 9px;
    opacity: 1;
  }
}

/* Mozilla Firefox 15 below */
@-moz-keyframes bounce {
  0% {
	-moz-transform:translateY(-100%);
    opacity: 0;
  }
  5% {
  	-moz-transform:translateY(-100%);
    opacity: 0;
  }
  15% {
  	-moz-transform:translateY(0);
    padding-bottom: 5px;
  }
  30% {
  	-moz-transform:translateY(-50%);
  }
  40% {
  	-moz-transform:translateY(0%);
    padding-bottom: 6px;
  }
  50% {
  	-moz-transform:translateY(-30%);
  }
  70% {
  	-moz-transform:translateY(0%);
    padding-bottom: 7px;
  }
  80% {
  	-moz-transform:translateY(-15%);
  }
  90% {
  	-moz-transform:translateY(0%);
  	padding-bottom: 8px;
  }
  95% {
  	-moz-transform:translateY(-10%);
  }
  97% {
  	-moz-transform:translateY(0%);
  	padding-bottom: 9px;
  }
  99% {
  	-moz-transform:translateY(-5%);
  }
  100% {
  	-moz-transform:translateY(0);
  	padding-bottom: 9px;
    opacity: 1;
  }
}

/* Opera 12.0 */
@-o-keyframes bounce {
  0% {
	-o-transform:translateY(-100%);
    opacity: 0;
  }
  5% {
  	-o-transform:translateY(-100%);
    opacity: 0;
  }
  15% {
  	-o-transform:translateY(0);
    padding-bottom: 5px;
  }
  30% {
  	-o-transform:translateY(-50%);
  }
  40% {
  	-o-transform:translateY(0%);
    padding-bottom: 6px;
  }
  50% {
  	-o-transform:translateY(-30%);
  }
  70% {
  	-o-transform:translateY(0%);
    padding-bottom: 7px;
  }
  80% {
  	-o-transform:translateY(-15%);
  }
  90% {
  	-o-transform:translateY(0%);
  	padding-bottom: 8px;
  }
  95% {
  	-o-transform:translateY(-10%);
  }
  97% {
  	-o-transform:translateY(0%);
  	padding-bottom: 9px;
  }
  99% {
  	-o-transform:translateY(-5%);
  }
  100% {
  	-o-transform:translateY(0);
  	padding-bottom: 9px;
    opacity: 1;
  }
}

/* W3, Opera 12+, Firefox 16+ */
@keyframes bounce {
  0% {
	transform:translateY(-100%);
    opacity: 0;
  }
  5% {
  	transform:translateY(-100%);
    opacity: 0;
  }
  15% {
  	transform:translateY(0);
    padding-bottom: 5px;
  }
  30% {
  	transform:translateY(-50%);
  }
  40% {
  	transform:translateY(0%);
    padding-bottom: 6px;
  }
  50% {
  	transform:translateY(-30%);
  }
  70% {
  	transform:translateY(0%);
    padding-bottom: 7px;
  }
  80% {
  	transform:translateY(-15%);
  }
  90% {
  	transform:translateY(0%);
  	padding-bottom: 8px;
  }
  95% {
  	transform:translateY(-7%);
  }
  97% {
  	transform:translateY(0%);
  	padding-bottom: 9px;
  }
  99% {
  	transform:translateY(-3%);
  }
  100% {
  	transform:translateY(0);
  	padding-bottom: 9px;
    opacity: 1;
  }
}

</style>
<script src="<?=$def_js?>jquery.js"></script>
<script src="<?=$def_css?>bootstrap3/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>
<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">
<script>

function skpdFormatResult(data) {
	var markup= "<li>"+ data.skpd_id+"-"+data.skpd_name+"</li>";
	return markup;
}

function skpdFormatSelection(data) {
	return data.skpd_name;
}

$(document).ready(function(){
	$('#user_name').focus();
	$("#skpd_id").select2({
		id: function(e) { return e.skpd_id }, 
		placeholder: "Pilih Sebagai",
		minimumInputLength: 0,
		multiple: false,
		ajax: { 
			url: "<?=base_url()?>dashboard/ajaxfile/cariskpd",
			dataType: 'jsonp',
			type:'POST',
			quietMillis: 100,
			data: function (term, page) {
				return {
					keyword: term, //search term
					per_page: 5, // page size
					page: page, // page number
				};
			},
			results: function (data, page) { 
				var more = (page * 5) < data.result;
				return {results: data.rows, more: more};
			}
		},
		formatResult: skpdFormatResult, 
		formatSelection: skpdFormatSelection
	});
});
</script>
</head>
<body>
<div id="wrapperlog">
<div class="container" style="margin-top:5%">
  <div class="col-md-4 col-md-offset-4">
    <div class="panel member_signin">
      <div class="panel-body">
       <img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" />
        <p class="member">LOGIN ADMIN SDM DISNAKER</p>
        <form role="form" class="loginform" action="<?=base_url()?>auth/login" id="form1" name="form1" method="post">
          <div class="form-group">
            <label for="user_name" class="sr-only">Username</label>
            <div class="input-group">
              <input type="text" class="form-control" id="user_name" name="user_name"
                placeholder="Username">
            </div>
          </div>
          <div class="form-group">
            <label for="user_pass" class="sr-only">Password</label>
            <div class="input-group">
              <input type="password" class="form-control" id="user_pass" name="user_pass"
                placeholder="Password">
            </div>
          </div>
          <button type="submit" class="btn btn-primary btn-md login">LOG IN</button>
        </form>
        <p class="forgotpass"><a href="#" class="small">Lupa Password ?</a></p>
		
      </div>
    </div>
  </div>
</div>
</div>
</body>
</html>