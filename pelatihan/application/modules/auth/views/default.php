<script>
$(document).ready(function(){
	$('#thn').datepicker({
		format:'yyyy',
		autoclose:true,		minViewMode:2,		ViewMode:2
	}).on('changeDate',function(){
		refreshData();
	});

	$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					refreshData();
				break;			
			}
	});		
	refreshData();
	
});
function refreshData(){	$.ajax({
		type:'post',
		url:'<?=base_url()?>auth/page/data/default',
		data:{ 'thn':$('#thn').val() },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});}

</script>
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#statistik" data-toggle="tab"><i class="icon-pencil"></i> Statistik</a></li>
</ul>
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="statistik">			Tahun : <input type="text" id="thn" name="thn" value="<?=date('Y')?>" style="width:50px" />		<div id="result">		</div>
	
	</div>
</div>