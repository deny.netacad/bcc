<ul class="dropdown-menu">
	<li><a href="<?=base_url()?>auth/manmodul" tabindex="-1"><i class="icon-th-large"></i> Manajemen Modul</a></li>
	<li><a href="<?=base_url()?>auth/manuser" tabindex="-1"><i class="icon-user"></i> Manajemen User</a></li>
	<li><a href="<?=base_url()?>auth/manhakakses" tabindex="-1"><i class="icon-th-large"></i> Manajemen Hak Akses</a></li>
	
	<li><a href="<?=base_url()?>auth/daftarijin" tabindex="-1"><i class="icon-th-large"></i> Daftar Ijin</a></li>
	<li><a href="<?=base_url()?>auth/syaratijin" tabindex="-1"><i class="icon-th-large"></i> Syarat Perijinan</a></li>
	<li><a href="<?=base_url()?>auth/manjentl" tabindex="-1"><i class="icon-th-large"></i> Komponen Tarif Lingkungan (TL)</a></li>
	<li><a href="<?=base_url()?>auth/manjenil" tabindex="-1"><i class="icon-th-large"></i> Komponen Index Lokasi (IL)</a></li>
	<li><a href="<?=base_url()?>auth/manjenig" tabindex="-1"><i class="icon-th-large"></i> Komponen Index Gangguan (IG)</a></li>
	<li><a href="<?=base_url()?>auth/mankbli" tabindex="-1"><i class="icon-th-large"></i> Data KBLI</a></li>
	<li><a href="<?=base_url()?>auth/pejabat" tabindex="-1"><i class="icon-th-large"></i> Data Pejabat Penandatangan</a></li>
</ul>