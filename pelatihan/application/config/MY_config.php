<?php

$config['hari']		= array("","Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu");
$config['bulan']	= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus",
							"September","Oktober","November","Desember");
$config['data']['hari'] 	= $config['hari'];
$config['data']['bulan'] 	= $config['bulan'];
$config['data']['site_title']  		= 'Pendaftaran Peserta Pelatihan ';
$config['data']['site_heading']		= 'Pendaftaran Peserta Pelatihan ';
$config['data']['site_copyright']	= 'Copyright &copy; 2019 -';
$config['data']['site_desc']		= 'Pendaftaran Peserta Pelatihan ';
$config['data']['themes']			=  'admin/default/';

$config['data']['themesportal']		=  'portal/default/';

$config['base_url']					= '//'.$_SERVER['SERVER_NAME'].'/pelatihan/';

$config['data']['def_img']			=  $config['base_url'].'assets/img/';
$config['data']['def_css']			=  $config['base_url'].'assets/css/';
$config['data']['def_js']			=  $config['base_url'].'assets/js/';
$config['data']['job'] = array("","Wiraswasta","Buruh","Petani","Nelayan","PNS","Swasta","Pedagang","Pekerja Rumah Tangga","LSM","Tenaga Pendidik","Pengacara","Tenaga Kesehatan"
								,"Peneliti","Seniman","Wartawan","Mahasiswa/Pelajar","Lain-lain");
	
