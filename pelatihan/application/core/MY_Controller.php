<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller 
{
	var $data;
	var $modules;
	
    function __construct()
    {
        parent::__construct();
		$this->data = $this->config->item("data");
		$this->data['role'] = $this->session->userdata('role');
    }
	
	function replace($table,$arrdata,$arrwhere){
		$ret = true;
		$fields = $this->db->list_fields($table);
		if(in_array("iduser",$fields)){
			$arrdata['iduser'] = $this->session->userdata('iduser');
		}
		$rs = $this->db->get_where($table, $arrwhere);
		if($rs->num_rows()>0){
			if(!$this->db->update($table,$arrdata,$arrwhere)){
				echo $this->db->_error_message();
				$ret = false;
			}
		}else{
			if(!$this->db->insert($table,$arrdata)){
				echo $this->db->_error_message();
				echo $this->db->last_query();
				$ret = false;
			}
		}
		
		return $ret;
	}
	
	function qrcode($id=""){
		header("Content-Type: image/png");
		$params['data'] = $id;
		echo $this->ciqrcode->generate($params);
	}
	
	function bar128($id=""){
		#$this->load->library('bargen/class/BCGFont');
		$this->load->library('bargen/class/BCGColor');
		$this->load->library('bargen/class/BCGDrawing');
		
		$this->load->library('bargen/class/BCGcode128');
		#$this->load->library('bargen/class/BCGcode39');
		#$this->font = new BCGFont(BASEPATH.'libraries/bargen/class/font/tahoma.ttf', 28);
		$this->color_black = new BCGColor(0, 0, 0);
		$this->color_white = new BCGColor(255, 255, 255);
		
		$this->code = new BCGcode128();
		#$this->code->setScale(5); // Resolution
		#$this->code->setThickness(30); // Thickness
		$this->code->setForegroundColor($this->color_black); // Color of bars
		$this->code->setBackgroundColor($this->color_white); // Color of spaces
		#$this->code->setFont($this->font);
		$this->code->setFont(0);		// Font (or 0)
		$this->code->parse($id); // Text
		
		/* Here is the list of the arguments
		1 - Filename (empty : display on screen)
		2 - Background color */
		$this->drawing = new BCGDrawing('', $this->color_white);
		$this->drawing->setBarcode($this->code);
		$this->drawing->draw();
		// Header that says it is an image (remove it if you save the barcode to a file)
		header('Content-Type: image/png');
		// Draw (or save) the image into PNG format.
		$this->drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	}
	
	function bar39($id=""){
		#$this->load->library('bargen/class/BCGFont');
		$this->load->library('bargen/class/BCGColor');
		$this->load->library('bargen/class/BCGDrawing');
		
		#$this->load->library('bargen/class/BCGcode128');
		$this->load->library('bargen/class/BCGcode39');
		#$this->font = new BCGFont(BASEPATH.'libraries/bargen/class/font/tahoma.ttf', 28);
		$this->color_black = new BCGColor(0, 0, 0);
		$this->color_white = new BCGColor(255, 255, 255);
		
		$this->code = new BCGcode39();
		#$this->code->setScale(5); // Resolution
		#$this->code->setThickness(30); // Thickness
		$this->code->setForegroundColor($this->color_black); // Color of bars
		$this->code->setBackgroundColor($this->color_white); // Color of spaces
		#$this->code->setFont($this->font);
		$this->code->setFont(0);		// Font (or 0)
		$this->code->parse($id); // Text
		
		/* Here is the list of the arguments
		1 - Filename (empty : display on screen)
		2 - Background color */
		$this->drawing = new BCGDrawing('', $this->color_white);
		$this->drawing->setBarcode($this->code);
		$this->drawing->draw();
		// Header that says it is an image (remove it if you save the barcode to a file)
		header('Content-Type: image/png');
		// Draw (or save) the image into PNG format.
		$this->drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	}
	
	function pdf(){
		if($this->session->userdata('is_login_disperindag') || $this->session->userdata('is_login_pelatihanuser')){
			ini_set('memory_limit','1024M'); 
			
			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);	
			$this->load->library('pdf');
			
			$header = "<div style='float:right;width:330px;'>";
			$header.= "<div style='float:left;width:260px;color:#898989;line-height:1;text-align:right'>"
			."<small>"
			."PT. Agmantara Media Pratama<br />"
			."Gedung Griya Artha (BPD Jateng) Lt. 3<br />"
			."Jl. Pemuda No. 142<br />"
			."P 024-354234 F 024-232323<br />"
			."</small>"
			."</div>";
			$header.= "<div style='float:right;width:51px;text-align:center'><img src='".base_url()."assets/img/logo.png' width='50' border='0' /></div>";
			$header.= "</div>";
			$header.= "<div style='clear:both'></div>";
			$header.= "<div style='border:1px solid #CCCCCC'></div>";
			
			

			# 
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # A4
				break;
				case "tiket":
					$pdf = $this->pdf->load('en-GB-x', array(150,50)); # Tiket
				break;
				default:	
					$pdf = $this->pdf->load('en-GB-x', array(216,355)); # legal
				break;
			}
			
			switch($this->input->get('o')){
				case "L":
					$o = "L";
				break;
				default:
					$o = "P";
				break;
			}
			#$pdf->setHTMLHeader($header);
			#$pdf->SetWatermarkImage(base_url().'assets/img/wtm.png', 0.1, '',array(-20,30));
			$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('o')=='L'){				
				$pdf->SetDocTemplate(BASEPATH.'../assets/template_l.pdf',1);			
			}else{				
				$pdf->SetDocTemplate(BASEPATH.'../assets/template_p.pdf',1);			
			}
			
			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            25, // margin top
            15, // margin bottom
            1, // margin header
            5); // margin footer
			
			$pdf->SetDisplayMode('fullpage');
			
			
			//$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); 
			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;
			
		}else{
			echo "<script> history.back() </script>";
		}
	}
	
	function load(){		
		$this->data['module'] = $this->session->userdata['role'][$this->modules]['urlmodule'];
		$this->data['page'] = $this->uri->segment(2);
		$this->load->view('dashboard/dashboard',$this->data);
	}
	
	function loadportal(){		
		$this->data['module'] = $this->session->userdata['role'][$this->modules]['urlmodule'];
		$this->data['page'] = $this->uri->segment(2);
		$this->load->view('portal/index',$this->data);
	}
	
	function data(){
		$this->data['page'] = $this->uri->segment(4);
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'].'_data',$this->data);	
	}
	
	function cetak(){
		$this->data['page'] = $this->uri->segment(4);
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'].'_print',$this->data);	
	}
	
	function form(){
		$this->data['page'] = $this->uri->segment(4);
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'].'_form',$this->data);	
	}
	
	function view(){
		$this->data['page'] = $this->uri->segment(4);
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'],$this->data);	
	}
	
	
	
}