<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Model extends CI_Model 
{
	var $modules = "";
	
	function __construct()
    {
        parent::__construct();
    }
	
	function foo(){
		echo "foo";
	}
	
	function datenow(){

		$rs = $this->db->query("select date_format(now(),'%d-%m-%Y') as tg");

		$item = $rs->row();

		return $item->tg;

	}

	
	function toMySQLDate($value){
		$v = explode(" ",$value);
		$val = explode("-",$v[0]);
		if(count($v)>1){
			$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
		}else{
			$value = $val[2]."-".$val[1]."-".$val[0];
		}
		return $value;
	}
	
	function now(){
		$rs = $this->db->query("select now() as tg");
		$item = $rs->row();
		return $item->tg;
	}
	
	function nowtext(){
		$rs = $this->db->query("select date_format(now(),'%d-%m-%Y %H:%i:%s') as tg");
		$item = $rs->row();
		return $item->tg;
	}
	
	function nowdatetext(){
		$rs = $this->db->query("select date_format(now(),'%d-%m-%Y') as tg");
		$item = $rs->row();
		return $item->tg;
	}
	
	function searchForId($id, $array) {
	   foreach ($array as $key=>$val) {
			foreach ($array[$key] as $key2=>$val2){
				foreach($val2 as $key3=>$val3){
				   if ($key3 == $id) {
					   $this->session->set_userdata('kdruanganaktif',$key3);
					   $this->session->set_userdata('moduleaktif',$val3);
					   return $key3;
				   }
			   }
		   }
	   }
	   return null;
	}
	
	function namalengkap($gd="",$nm="",$gb=""){
		$gd = (($gd=='')?"":$gd.". ");
		$gb = (($gb=='')?"":", ".$gb);
		return $gd.$nm.$gb;
	}
	
	function replace($table,$arrdata,$arrwhere,$flag=0,&$text=""){
		$ret = true;
		$table_activity = "";
		$nip = "";
		
		
		$fields = $this->db->list_fields($table);
		
		
		$data['ip']			= $_SERVER['REMOTE_ADDR'];
		$data['tmstamp']	= $this->now();
		$data['useragent']	= $this->session->userdata('user_agent');
		$data['username']	= $this->session->userdata('username');
		if( array_key_exists($table,$this->arrtable)){
			$table_activity = $this->arrtable[$table];
		}
		
		if(in_array("nip",$fields)){
			$nip = 'NIP : '.$arrdata['nip'];
		}
		
		
		if($flag>0){
			#if($this->session->userdata['role'][$this->getCurModule()]['e']==1){
				if($table!="ref_user"){
					if(in_array("iduserupd",$fields)){
						$arrdata['iduserupd'] = $this->session->userdata('iduser');
					}
					if(in_array("updateon",$fields)){
						$arrdata['updateon'] = $this->now();
					}
				}
				$data['ket']		= 'akses update data di module '.$this->modules.' '.$table_activity.' '.$nip;
				$this->db->insert("log_user",$data);
				if(!$this->db->update($table,$arrdata,$arrwhere)) $ret = false;
			/*}else{
				$data['ket']		= 'akses update data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;
				$this->db->insert("log_user",$data);
				$ret = false;
				$text = "user tidak diperkenankan merubah data !";
			}*/
		}else{
			if(count($arrwhere)==0){
				#if($this->session->userdata['role'][$this->getCurModule()]['i']==1){
					if($table!="ref_user"){
						if(in_array("iduser",$fields)){
							$arrdata['iduser'] = $this->session->userdata('iduser');
						}
						if(in_array("createon",$fields)){
							$arrdata['createon'] = $this->now();
						}
					}
					$data['ket']		= 'akses insert data di module '.$this->modules.' '.$table_activity.' '.$nip;
					$this->db->insert("log_user",$data);
					if(!$this->db->insert($table,$arrdata)) $ret = false;
				/*}else{
					$data['ket']		= 'akses insert data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;
					$this->db->insert("log_user",$data);
					$ret = false;
					$text = "user tidak diperkenankan menginput data !";
				}*/
			}else{
				$rs = $this->db->get_where($table, $arrwhere);
				if($rs->num_rows()>0){
					#if($this->session->userdata['role'][$this->getCurModule()]['e']==1){
						if($table!="ref_user"){
							if(in_array("iduserupd",$fields)){
								$arrdata['iduserupd'] = $this->session->userdata('iduser');
							}
							if(in_array("updateon",$fields)){
								$arrdata['updateon'] = $this->now();
							}
						}
						$data['ket']		= 'akses update data di module '.$this->modules.' '.$table_activity.' '.$nip;
						$this->db->insert("log_user",$data);
						if(!$this->db->update($table,$arrdata,$arrwhere)) $ret = false;
					/*}else{
						$data['ket']		= 'akses update data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;
						$this->db->insert("log_user",$data);
						$ret = false;
						$text = "user tidak diperkenankan merubah data !";
					}*/
				}else{
					#if($this->session->userdata['role'][$this->getCurModule()]['e']==1){
						if($table!="ref_user"){
							if(in_array("iduser",$fields)){
								$arrdata['iduser'] = $this->session->userdata('iduser');
							}
							if(in_array("createon",$fields)){
								$arrdata['createon'] = $this->now();
							}
						}
						$data['ket']		= 'akses insert data di module '.$this->modules.' '.$table_activity.' '.$nip;
						$this->db->insert("log_user",$data);
						if(!$this->db->insert($table,$arrdata)) $ret = false;
					/*}else{
						$data['ket']		= 'akses update data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;
						$this->db->insert("log_user",$data);
						$ret = false;
						$text = "user tidak diperkenankan menginput data !";
					}*/
				}
			}
		}
		return $ret;
	}
	
	function replace2($table,$arrdata,$arrwhere,$flag=0,&$text=""){
		$ret = true;
		$fields = $this->db->list_fields($table);
		
		if($flag>0){
			if($this->session->userdata['role'][$this->getCurModule()]['e']==1){
				if($table!="ref_user"){
					if(in_array("iduserupd",$fields)){
						$arrdata['iduserupd'] = $this->session->userdata('iduser');
					}
					if(in_array("updateon",$fields)){
						$arrdata['updateon'] = $this->now();
					}
				}
				if(!$this->db->update($table,$arrdata,$arrwhere)) $ret = false;
			}else{
				$ret = false;
				$text = "user tidak diperkenankan merubah data !";
			}
		}else{
			if(count($arrwhere)==0){
				if($this->session->userdata['role'][$this->getCurModule()]['i']==1){
					if($table!="ref_user"){
						if(in_array("iduser",$fields)){
							$arrdata['iduser'] = $this->session->userdata('iduser');
						}
						if(in_array("createon",$fields)){
							$arrdata['createon'] = $this->now();
						}
					}
					if(!$this->db->insert($table,$arrdata)) $ret = false;
				}else{
					$ret = false;
					$text = "user tidak diperkenankan menginput data !";
				}
			}else{
				$rs = $this->db->get_where($table, $arrwhere);
				if($rs->num_rows()>0){
					if($this->session->userdata['role'][$this->getCurModule()]['e']==1){
						if($table!="ref_user"){
							if(in_array("iduserupd",$fields)){
								$arrdata['iduserupd'] = $this->session->userdata('iduser');
							}
							if(in_array("updateon",$fields)){
								$arrdata['updateon'] = $this->now();
							}
						}
						if(!$this->db->update($table,$arrdata,$arrwhere)) $ret = false;
					}else{
						$ret = false;
						$text = "user tidak diperkenankan merubah data !";
					}
				}else{
					if($this->session->userdata['role'][$this->getCurModule()]['e']==1){
						if($table!="ref_user"){
							if(in_array("iduser",$fields)){
								$arrdata['iduser'] = $this->session->userdata('iduser');
							}
							if(in_array("createon",$fields)){
								$arrdata['createon'] = $this->now();
							}
						}
						if(!$this->db->insert($table,$arrdata)) $ret = false;
					}else{
						$ret = false;
						$text = "user tidak diperkenankan menginput data !";
					}
				}
			}
		}
		return $ret;
	}
	
	function save2($tablename="",$arrnot=array(),$keyedit=array(),$keyindex=array(),$keydate=array(),$keyin=array(),$keyout=array(),$data=array(),$keydate2=array(),$keyarray=array(),$keynum2=array()){
		$data = $data;
		$dt = array();
		$flag = 0;
		$ret['err']		= false;
		$ret['text']	= "Data Tersimpan";	
		foreach($_POST as $key=>$value){
			if(array_search($key,$keyin)!=""){
				$keys =  array_keys($keyin,$key);
				$key = $keyout[$keys[0]];
			}
			
			if(array_search($key,$keydate)!=''){
				$value = (($value!='')?$value:"00-00-0000");
				$v = explode(" ",$value);
				$val = explode("-",$v[0]);
				if(count($v)>1){
					$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
				}else{
					$value = $val[2]."-".$val[1]."-".$val[0];
				}
			}
			if(array_search($key,$arrnot)==""){
				if(is_array($value)){
					foreach($value as $key2=>$val2){
						if(array_search($key,$keydate2)!=''){
							$v = explode(" ",$val2);
							$val = explode("-",$v[0]);
							if(count($v)>1){
								$val2 = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
							}else{
								$val2 = $val[2]."-".$val[1]."-".$val[0];
							}
						}
						if(array_search($key,$keynum2)!=''){
							$val2 = str_replace(",","",$val2);
						}
						
						$data2[$key2][$key] = $val2;
					}
				}else{
					$data[$key] = $value;
				}
			}
		}
		foreach($data as $key=>$value){
			if(array_search($key,$keyindex)!=''){
				$dt[$key] = $value;
			}
		}
		
		foreach($data2 as $key=>$val){
			foreach($val as $key2=>$val2){
				if(array_search($key2,$keyindex)!=''){
					$dt[$key2] = $val2;
				}
				$data[$key2] = $val2;
			}
			$rs = $this->db->get_where($tablename,$dt);
			if($rs->num_rows()>0){
					if(!$this->db->update($tablename,$data,$dt)){
						$ret['err']		= true;
						$ret['text']	= "Terjadi Kesalahan";	
						echo json_encode($ret);
						exit;
					}
			}else{
				if(!$this->db->insert($tablename,$data,$dt)){
					$ret['err']		= true;
					$ret['text']	= "Terjadi Kesalahan";	
					echo json_encode($ret);
					exit;
				}
			}
		}
		
		echo json_encode($ret);
	}
	
	function save3($tablename="",$arrnot=array(),$keyedit=array(),$keyindex=array(),$keydate=array(),$keyin=array(),$keyout=array(),$data=array(),$ret=array()){
		$data = $data;
		$dt = array();
		$flag = 0;
		$ret['err']		= false;
		$ret['text']	= "Data Tersimpan";	
		foreach($_POST as $key=>$value){
			if(array_search($key,$keyin)!=""){
				$keys =  array_keys($keyin,$key);
				$key = $keyout[$keys[0]];
			}
			
			if(array_search($key,$keydate)!=''){
				$value = (($value!='')?$value:"00-00-0000");
				$v = explode(" ",$value);
				$val = explode("-",$v[0]);
				if(count($v)>1){
					$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
				}else{
					$value = $val[2]."-".$val[1]."-".$val[0];
				}
			}
			if(array_search($key,$arrnot)==""){
				$data[$key] = $value;
			}
		}

		foreach($_POST as $key=>$value){
			if(array_search($key,$keyin)!=""){
				$keys =  array_keys($keyin,$key);
				$key = $keys[0];
			}
			if(array_search($key,$keyedit)!=""){
				$keys =  array_keys($keyedit,$key);
				if($this->input->post($key)!=""){
					$dt[$keyindex[$keys[0]]] = $value;
					$flag = 1;
				}
			}
		}
		if(!$this->replace($tablename,$data,$dt,$flag)){
			$ret['err']		= true;
			$ret['text']	= "Terjadi Kesalahan";	
		}

		return $ret;
	}
	
	function save($tablename="",$arrnot=array(),$keyedit=array(),$keyindex=array(),$keydate=array(),$keyin=array(),$keyout=array(),$data=array(),$ret=array(),$keynum=array()){
		$data = $data;
		$dt = array();
		$flag = 0;
		$ret['err']		= false;
		$ret['text']	= "Data Tersimpan";	
		foreach($_POST as $key=>$value){
			if(array_search($key,$keyin)!=""){
				$keys =  array_keys($keyin,$key);
				$key = $keyout[$keys[0]];
			}
			if(array_search($key,$keynum)!=''){
				$value = str_replace(",","",$value);
				$value = (($value>0)?$value:0);
			}
			if(array_search($key,$keydate)!=''){
				$value = (($value!='')?$value:"00-00-0000");
				$v = explode(" ",$value);
				$val = explode("-",$v[0]);
				if(count($v)>1){
					$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
				}else{
					$value = $val[2]."-".$val[1]."-".$val[0];
				}
			}
			if(array_search($key,$arrnot)==""){
				$data[$key] = $value;
			}
		}

		foreach($_POST as $key=>$value){
			if(array_search($key,$keyin)!=""){
				$keys =  array_keys($keyin,$key);
				$key = $keys[0];
			}
			if(array_search($key,$keyedit)!=""){
				$keys =  array_keys($keyedit,$key);
				
				if($this->input->post($key)!=""){
					$dt[$keyindex[$keys[0]]] = $value;
					$flag = 1;
				}
			}
		
		}

		if(!$this->replace($tablename,$data,$dt,$flag,$text)){
			$ret['err']		= true;
			$ret['text']	= "Terjadi Kesalahan\n".$text."\n".$this->db->_error_message();	
		}
		echo json_encode($ret);
	}
	
	function savereturn($tablename="",$arrnot=array(),$keyedit=array(),$keyindex=array(),$keydate=array(),$keyin=array(),$keyout=array(),$data=array(),$ret=array(),$keynum=array()){
		$data = $data;
		$dt = array();
		$flag = 0;
		$ret['err']		= false;
		$ret['text']	= "Data Tersimpan";	
		foreach($_POST as $key=>$value){
			if(array_search($key,$keyin)!=""){
				$keys =  array_keys($keyin,$key);
				$key = $keyout[$keys[0]];
			}
			if(array_search($key,$keynum)!=''){
				$value = str_replace(",","",$value);
				$value = (($value>0)?$value:0);
			}
			if(array_search($key,$keydate)!=''){
				$value = (($value!='')?$value:"00-00-0000");
				$v = explode(" ",$value);
				$val = explode("-",$v[0]);
				if(count($v)>1){
					$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];
				}else{
					$value = $val[2]."-".$val[1]."-".$val[0];
				}
			}
			if(array_search($key,$arrnot)==""){
				$data[$key] = $value;
			}
		}

		foreach($_POST as $key=>$value){
			if(array_search($key,$keyin)!=""){
				$keys =  array_keys($keyin,$key);
				$key = $keys[0];
			}
			if(array_search($key,$keyedit)!=""){
				$keys =  array_keys($keyedit,$key);
				
				if($this->input->post($key)!=""){
					$dt[$keyindex[$keys[0]]] = $value;
					$flag = 1;
				}
			}
		
		}

		if(!$this->replace($tablename,$data,$dt,$flag,$text)){
			$ret['err']		= true;
			$ret['text']	= "Terjadi Kesalahan\n".$text."\n".$this->db->_error_message();	
		}
		return json_encode($ret);
	}
	
	function Terbilang($x)
	{
		$abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		if ($x < 12)
		return " " . $abil[$x];
		elseif ($x < 20)
		return $this->Terbilang($x - 10) . "belas";
		elseif ($x < 100)
		return $this->Terbilang($x / 10) . " puluh" . $this->Terbilang($x % 10);
		elseif ($x < 200)
		return " seratus" .$this-> Terbilang($x - 100);
		elseif ($x < 1000)
		return $this->Terbilang($x / 100) . " ratus" . $this->Terbilang($x % 100);
		elseif ($x < 2000)
		return " seribu" . $this->Terbilang($x - 1000);
		elseif ($x < 1000000)
		return $this->Terbilang($x / 1000) . " ribu" . $this->Terbilang($x % 1000);
		elseif ($x < 1000000000)
		return $this->Terbilang($x / 1000000) . " juta" . $this->Terbilang($x % 1000000);
		elseif ($x < 100000000000)
		return $this->Terbilang($x / 1000000000) . " milyar" . $this->Terbilang($x % 1000000000);
	}
	
	function getCurModule(){
		return $this->modules;
	}
	
	function nip18($nip=''){
		return substr($nip,0,8)." ".substr($nip,9,6)." ".substr($nip,14,1)." ".substr($nip,-3);
	}
	
	function listModule($id="idmodule",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| PILIH MODUL</option>";
		$this->db->order_by("idmodule");
		$rs = $this->db->get("ref_module");
		foreach($rs->result() as $item){
		 $isSel = (($item->idmodule==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idmodule."\" $isSel >".$item->module."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listLoket($id="idloket",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| PILIH LOKET</option>";
		$this->db->order_by("id");
		$rs = $this->db->get("loket");
		foreach($rs->result() as $item){
		 $isSel = (($item->id==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->id."\" $isSel >".$item->nama_loket."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJendok($nm="idjendok",$empty="-",$sel=""){
		echo "<select name=\"$nm\" id=\"$nm\" style=\"width:250px\">";
		echo "<option value=\"\">$empty</option>";
		echo $this->showmenu2();
		echo "</select>";
	}
	
	function showmenu2($id='',$indent='1'){
		$rs= $this->db->get_where("efile_ref_jendok",array("parent"=>$id));
		$num = $rs->num_rows();
		$i = 0;
		while($i<$indent){
			$nbsp.="&nbsp;";
			$i++;
		}
		$style = (($id=='0')?" style=\"background-color:#EEEEEE\"":"");
		foreach($rs->result() as $item){
				$i++;
				echo "<option value=\"".$item->idjendok."\" ".$style.">$nbsp".$item->jendok."</option>";
				$this->showmenu2($item->idjendok,$indent+4);     
		}
	}
	
	function doPdf($nip="",$consDir="",$lnJendok="",$consJendok="",$saveFilename=""){
		//Instanciation of inherited class
		$this->load->library('fpdf16/pdf');
		$rs = $this->db->query("SELECT pns_pnsnam as nama "
		." FROM dbpns WHERE pns_pnsnip='".$nip."'");
		$row = $rs->row();
		$this->pdf = new PDF("http://".$_SERVER['SERVER_NAME']."/efilingpemalang/dokumen/bar128/".$nip,$nip,$row->nama);
		
		$this->pdf->AliasNbPages();
		$d = dir($consDir);
		$n = 0;
		while (false !== ($entry = $d->read())) {
			$extarr = explode(".",$entry);
			$ext = $extarr[count($extarr)-1];
			#if($entry!="." && $entry!=".." && $ext=="png" && substr($entry,0,$lnJendok)==$consJendok){
			# kondisi filter nama file dan numeric file ;)
			if($entry!="." && $entry!=".." && $ext=="jpg" && substr($entry,0,$lnJendok)==$consJendok && is_numeric(substr($entry,($lnJendok)+1,1))){
				$n++;
				#echo $n;
				$this->pdf->SetMargins(1,1);
				$this->pdf->AddPage("p");
				#$pdf->Image($consDir."/".$entry,55,190,100,0,'PNG');
				$this->pdf->Image($consDir."/".$entry,10,20,180,0,'JPG');
			}
		}
		$d->close();
		$this->pdf->Output($saveFilename);
	}
	
	
	function listAkun($id="kdakun",$sel="",$required=""){

		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto;min-width:300px;\">";

		$ret.="<option value=\"\">-</option>";

		$this->db->order_by("kdakun");

		$rs = $this->db->get("mast_akun");

		foreach($rs->result() as $item){

		 $isSel = (($item->kdakun==$sel)?"selected":"");

		 $indent = str_repeat("&nbsp;&nbsp;",strlen($item->kdakun));

		 $ret.="<option value=\"".$item->kdakun."\" $isSel >".$indent.$item->kdakun."-".$item->namaakun."</option>";

		}

		$ret.="</select>";

		return $ret;

	}
	
	function tarifTiket(){
		$rs = $this->db->query("
			SELECT a.*,b.tarif,b.tgtarif FROM jenis_tiket a
			LEFT JOIN(
			SELECT a2.id_jen_tiket,a2.tgtarif,a2.tarif FROM set_tarif_tiket a2
			INNER JOIN 
			(SELECT id_jen_tiket,MAX(tgtarif) AS tgtarif FROM set_tarif_tiket GROUP BY id_jen_tiket)
			a3 ON a2.id_jen_tiket=a3.id_jen_tiket AND a2.tgtarif=a3.tgtarif	
			) b ON a.id=b.id_jen_tiket
		");
		return $rs;
	}
}