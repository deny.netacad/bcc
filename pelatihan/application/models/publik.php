<?php
class publik extends CI_Model {

   function __construct()
    {
        parent::__construct();
    }
   
   function useragent(){
		if ($this->agent->is_browser())
		{
			$agent = $this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
			$agent = $this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
			$agent = $this->agent->mobile();
		}
		else
		{
			$agent = 'Unidentified User Agent';
		}

		return $agent.$this->agent->platform();
   }
   
   function menu($id=0){
		$data['idparent'] = $id;
		$this->db->order_by("urut");
		$rs = $this->db->get_where("a_menu",$data);
		foreach($rs->result() as $item){
			$data2['idparent'] = $item->idmenu;
			$rs2 = $this->db->get_where("a_menu",$data2);
			if($rs2->num_rows()>0){
				echo "<li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">".$item->menu." <b class=\"caret\"></b></a>";
				echo "\n<ul class=\"dropdown-menu\">";
				$this->menu($item->idmenu);
				echo "</ul>\n";
			}else{
				if($item->jenlink!='Url'){
					$url = base_url()."sites/page/".strtolower($item->jenlink)."/".$item->idmenu;
				}else{
					$url = $item->url;
				}
				echo "<li><a href=\"".$url."\">".$item->menu."</a>";
			}
			echo "</li>\n";
		}
   }		
   
     function listInstalasipemeriksaan($id="kdinstalasi",$sel="", $required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| Pilih Instalasi Pemeriksaan</option>";
		$rs = $this->db->query("select * from mast_instalasi ");
		foreach($rs->result() as $item){
		 $isSel = (($item->kdinstalasi==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->kdinstalasi."\" $isSel >".$item->nminstalasi."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listDokterpraktek($id="kddokter",$sel="", $required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| Pilih Dokter</option>";
		$rs = $this->db->query("select * from mast_dokter ");
		foreach($rs->result() as $item){
		 $isSel = (($item->kddokter==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->r."\" $isSel >".$item->nmdokter."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	
   
   function dataThnajaranaktif(){
	$rs = $this->db->get_where("mast_thnajaran",array("sts"=>1));
	return $rs->row();
   }
   
   function dataSemesteraktif(){
	$rs = $this->db->get_where("mast_semester",array("flag"=>1));
	return $rs->row();
   }
   
   
   
   function listKosong($empty="| PILIH"){
		$html = "<select>";
		$html.= "<option>$empty</option>";
		$html.= "</select>";
		echo $html;
   }
   
   function listHariakademik($id="hari",$sel="", $required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| PILIH HARI</option>";
		$rs = $this->db->query("select * from mast_hari where hari not in (select weekend from mast_set_ak_jadwal_1) order by hari");
		foreach($rs->result() as $item){
		 $isSel = (($item->hari==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->hari."\" $isSel >".$item->namahari."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	 function listThnajaran($id="thnajaran",$sel="", $required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| TAHUN AJARAN</option>";
		$rs = $this->db->query("select * from mast_thnajaran where sts=1 order by thnajaran");
		foreach($rs->result() as $item){
		 $isSel = (($item->thnajaran==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->thnajaran."\" $isSel >".$item->thnajaran."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listKelas($id="idkelas",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| PILIH KELAS</option>";
		$this->db->order_by("idkelas");
		$rs = $this->db->get("mast_kelas");
		foreach($rs->result() as $item){
		 $isSel = (($item->idkelas==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idkelas."\" $isSel >".$item->kelas."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listMapel($id="idmapel",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH MATA PELAJARAN</option>";
		$this->db->select("a.*");
		$this->db->from("mast_mapel a");
		$this->db->order_by("a.mapel");
		$rs = $this->db->get();
		foreach($rs->result() as $item){
		 $isSel = (($item->idmapel==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idmapel."\" $isSel >".$item->mapel."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listStsguru($id="stsguru",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| BAGIAN</option>";
		$ret.="<option value=\"Asisten\" ".(($sel=='Asisten')?"selected":"").">Asisten</option>";
		$ret.="<option value=\"Guru Honorer\" ".(($sel=='Guru Honorer')?"selected":"").">Guru Honorer</option>";
		$ret.="<option value=\"Guru Pelajaran\" ".(($sel=='Guru Pelajaran')?"selected":"").">Guru Pelajaran</option>";
		$ret.="</select>";
		return $ret;
	}
	
	function listMapelaspek($id="idaspek",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILHI ASPEK</option>";
		$data['idmapel'] = $this->input->post('idmapel');
		$this->db->where($data);
		$this->db->select("a.idaspek,b.aspek");
		$this->db->from("mast_mapel_aspek a");
		$this->db->join("mast_aspek b","a.idaspek=b.idaspek","inner");
		$this->db->order_by("a.idaspek");
		$rs = $this->db->get();
		foreach($rs->result() as $item){
		 $isSel = (($item->idaspek==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idaspek."\" $isSel >".$item->aspek."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listUjianke($id="ujianke",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it d4dig\" $required>";
		for($i=1;$i<=10;$i++){
			$ret.="<option value=\"".$i."\" ".(($sel==$i)?"selected":"").">".$i."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listPengajar($id="nip",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PENGAJAR</option>";
		$where = " and a.idmapel='".$this->input->post('idmapel')."'";
		$rs = $this->db->query("select a.nip,b.nama from ak_gurumapel a
		inner join mast_pegawai b on a.nip=b.nip
		where 0=0 $where order by b.nama,a.nip");
		foreach($rs->result() as $item){
		 $isSel = (($item->nip==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->nip."\" $isSel >".$item->nip." - ".$item->nama."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listGurumapel($id="nip",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH GURU</option>";
		$data['idjenpeg'] = 3;
		$this->db->where($data);
		$this->db->order_by("nama");
		$rs = $this->db->get("mast_pegawai");
		foreach($rs->result() as $item){
		 $isSel = (($item->nip==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->nip."\" $isSel >".$item->nip." - ".$item->nama."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listMapelguru($id="idmapel",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH MATA PELAJARAN</option>";
		$data['a.nip'] = $this->input->post('nip');
		$this->db->select("a.idmapel,b.mapel");
		$this->db->from("mast_mapel_n_guru a");
		$this->db->join("mast_mapel b","a.idmapel=b.idmapel","inner");
		$this->db->order_by("a.idmapel");
		$rs = $this->db->get();
		foreach($rs->result() as $item){
		 $isSel = (($item->idmapel==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idmapel."\" $isSel >".$item->mapel."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenmapel($id="idjenmapel",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JENIS MATA PELAJARAN</option>";
		$this->db->order_by("idjenmapel");
		$rs = $this->db->get("mast_jenmapel");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenmapel==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenmapel."\" $isSel >".$item->jenmapel."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenmutasi($id="idjenmutasi",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JENIS MUTASI</option>";
		$this->db->order_by("idjenmutasi");
		$rs = $this->db->get("mast_jenmutasi");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenmutasi==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenmutasi."\" $isSel >".$item->jenmutasi."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenpeg($id="idjenpeg",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JENIS PEGAWAI</option>";
		$this->db->order_by("idjenpeg");
		$rs = $this->db->get("mast_jenpeg");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenpeg==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenpeg."\" $isSel >".$item->jenpeg."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	
	
	function listIsakademik($id="isakademik",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| BAGIAN</option>";
		$ret.="<option value=\"1\" ".(($sel==1)?"selected":"").">Akademik</option>";
		$ret.="<option value=\"0\" ".(($sel==0)?"selected":"").">Non Akademik</option>";
		$ret.="</select>";
		return $ret;
	}
	
	function listStskawin($id="idstskawin",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| STATUS MARITAL</option>";
		$this->db->order_by("idstskawin");
		$rs = $this->db->get("mast_stskawin");
		foreach($rs->result() as $item){
		 $isSel = (($item->idstskawin==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idstskawin."\" $isSel >".$item->stskawin."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listGoldarah($id="idgoldarah",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| GOLONGAN DARAH</option>";
		$this->db->order_by("idgoldarah");
		$rs = $this->db->get("mast_goldarah");
		foreach($rs->result() as $item){
		 $isSel = (($item->idgoldarah==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idgoldarah."\" $isSel >".$item->goldarah."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenkel($id="idjenkel",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JENIS KELAMIN</option>";
		$this->db->order_by("idjenkel");
		$rs = $this->db->get("mast_jenkel");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenkel==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenkel."\" $isSel >".$item->jenkel."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listAgama($id="idagama",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH AGAMA</option>";
		$this->db->order_by("idagama");
		$rs = $this->db->get("mast_agama");
		foreach($rs->result() as $item){
		 $isSel = (($item->idagama==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idagama."\" $isSel >".$item->agama."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listPro($id="idpro",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH PROVINSI</option>";
		$this->db->order_by("idpro");
		$rs = $this->db->get("mast_pro");
		foreach($rs->result() as $item){
		 $isSel = (($item->idpro==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idpro."\" $isSel >".$item->pro."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listKabkota($id="idkabkota",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH KABUPATEN/KOTA</option>";
		$data['idpro'] = $this->input->post('idpro');
		$this->db->where($data);
		$this->db->order_by("idkabkota");
		$rs = $this->db->get("mast_kabkota");
		foreach($rs->result() as $item){
		 $isSel = (($item->idkabkota==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idkabkota."\" $isSel >".$item->kabkota."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listKec($id="idkec",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH KECAMATAN</option>";
		$data['idkabkota'] = $this->input->post('idkabkota');
		$this->db->where($data);
		$this->db->order_by("idkec");
		$rs = $this->db->get("mast_kec");
		foreach($rs->result() as $item){
		 $isSel = (($item->idkec==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idkec."\" $isSel >".$item->kec."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listKel($id="idkel",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH KELURAHAN</option>";
		$data['idkec'] = $this->input->post('idkec');
		$this->db->where($data);
		$this->db->order_by("idkel");
		$rs = $this->db->get("mast_kel");
		foreach($rs->result() as $item){
		 $isSel = (($item->idkel==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idkel."\" $isSel >".$item->kel."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listAkses($id="idakses",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH AKSES</option>";
		$this->db->order_by("idakses");
		$rs = $this->db->get("mast_akses");
		foreach($rs->result() as $item){
		 $isSel = (($item->idakses==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idakses."\" $isSel >".$item->akses."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listAspek($id="kdaspek",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| ASPEK NILAI</option>";
		$this->db->order_by("kdaspek");
		$rs = $this->db->get("mast_aspek");
		foreach($rs->result() as $item){
		 $isSel = (($item->kdaspek==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->kdaspek."\" $isSel >".$item->aspek."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listAspeknilai($id="idaspek",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| ASPEK NILAI</option>";
		$this->db->order_by("idaspek");
		$rs = $this->db->get("mast_aspeknilai");
		foreach($rs->result() as $item){
		 $isSel = (($item->idaspek==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idaspek."\" $isSel >".$item->aspek."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listGolru($id="idgolru",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| GOL.</option>";
		$this->db->order_by("idgolru");
		$rs = $this->db->get("mast_golruang");
		foreach($rs->result() as $item){
		 $isSel = (($item->idgolru==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idgolru."\" $isSel >".$item->golru."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJabfungum($id="idjabfungum",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JAB. FUNGSIONAL UMUM</option>";
		$this->db->order_by("idjabfungum");
		$rs = $this->db->get("mast_jabfungum");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjabfungum==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjabfungum."\" $isSel >".$item->jabfungum."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listStsaktif($id="sts",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required>";
		$ret.="<option value=\"\">| STATUS</option>";
		$ret.="<option value=\"1\" ".(($sel=='1')?"selected":"").">Aktif</option>";
		$ret.="<option value=\"0\" ".(($sel=='0')?"selected":"").">Tidak Aktif</option>";
		$ret.="</select>";
		return $ret;
	}
	
	function listRuang($id="idruang",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH RUANG KELAS</option>";
		$this->db->select("a.*,b.kelas");
		$this->db->from("mast_kelas_ruang a");
		$this->db->join("mast_kelas b","a.idkelas=b.idkelas","left");
		$this->db->order_by("a.idkelas,a.ruang");
		$rs = $this->db->get();
		foreach($rs->result() as $item){
		 $isSel = (($item->idruang==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idruang."\" $isSel >(".$item->kelas.") ".$item->ruang."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listRuangkelas($id="idruang",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH RUANG KELAS</option>";
		$this->db->select("a.*,b.kelas");
		$this->db->from("mast_kelas_ruang a");
		$this->db->join("mast_kelas b","a.idkelas=b.idkelas","left");
		$data['a.idkelas'] =$this->input->post('idkelas');
		$this->db->where($data);
		$this->db->order_by("a.idkelas,a.ruang");
		$rs = $this->db->get();
		foreach($rs->result() as $item){
		 $isSel = (($item->idruang==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idruang."\" $isSel >(".$item->kelas.") ".$item->ruang."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listRuangkelascomplete($id="idruang",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| PILIH RUANG KELAS</option>";
		$this->db->select("a.*,b.kelas");
		$this->db->select("count(c.nis) as jml",false);
		$this->db->from("mast_kelas_ruang a");
		$this->db->join("mast_kelas b","a.idkelas=b.idkelas","left");
		$this->db->join("mast_siswa c","a.idruang=c.idruang","left");
		$this->db->group_by("a.idruang");
		$data['a.idkelas'] =$this->input->post('idkelas');
		$this->db->where($data);
		$this->db->order_by("a.idkelas,a.ruang");
		$rs = $this->db->get();
		foreach($rs->result() as $item){
		 $isSel = (($item->idruang==$sel)?"selected":"");
		 $isFull = (($item->kapasitas<=$item->jml)?"true":"false");
		 $ret.="<option value=\"".$item->idruang."\" $isSel data=\"(".$item->kelas.") ".$item->ruang."\" isfull=\"".$isFull."\" >(".$item->kelas.") ".$item->ruang." Kapasitas : ".$item->kapasitas." Terisi : ".$item->jml."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listSemester($id="idsemester",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| SEMESTER</option>";
		$this->db->where(array("flag"=>1));
		$this->db->order_by("idsemester");
		$rs = $this->db->get("mast_semester");
		foreach($rs->result() as $item){
		 $isSel = (($item->idsemester==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idsemester."\" $isSel >".$item->semester."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenjad($id="idjenjad",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JENIS JADWAL</option>";
		$this->db->order_by("idjenjad");
		$rs = $this->db->get("psb_mast_jenjad");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenjad==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenjad."\" $isSel >".$item->jenjad."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenisuji($id="idjenisuji",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JENIS PENGUJIAN</option>";
		$this->db->order_by("idjenisuji");
		$rs = $this->db->get("mast_jenisuji");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenisuji==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenisuji."\" $isSel >".$item->jenisuji."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenisujimapel($id="idjenisuji",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JENIS PENGUJIAN</option>";
		
		$this->db->select("a.idjenisuji,b.jenisuji");
		$this->db->from("ak_jenisujimapel a");
		$this->db->join("mast_jenisuji b","a.idjenisuji=b.idjenisuji","inner");
		$data['a.idmapel'] = $this->input->post('idmapel');
		$this->db->where($data);
		$this->db->order_by("a.idjenisuji");
		
		$rs = $this->db->get();
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenisuji==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenisuji."\" $isSel >".$item->jenisuji."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenisujimultiple($id="idjenisuji",$sel="",$required=""){
		$ret = "<select id=\"$id\" name=\"$id\" class=\"multiselect\" $required style=\"width:auto\" multiple=\"multiple\">";
		$this->db->order_by("idjenisuji");
		$rs = $this->db->get("mast_jenisuji");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenisuji==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenisuji."\" $isSel >".$item->jenisuji."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listBulan($idselect = 'bln',$selected = '', $css = '')
	{
		$month2[1] = "Januari";
		$month2[2] = "Februari";
		$month2[3] = "Maret";
		$month2[4] = "April";
		$month2[5] = "Mei";
		$month2[6] = "Juni";
		$month2[7] = "Juli";
		$month2[8] = "Agustus";
		$month2[9] = "September";
		$month2[10] = "Oktober";
		$month2[11] = "November";
		$month2[12] = "Desember";
		$print = "\n<!-- List Bulan -->\n<select name=$idselect id=$idselect style=\"width:150px\">\n";
		$now = 1;
		for ($i = 1; $i <= 12; $i++) {
			$bulan = $month2[$i];
			if (strlen($i) == 1) {
				$i = "0" . $i;
			}
			if ($now == $selected) {
				$print .= "<option value=$now selected>$now | $bulan</option>\n";
			} else {
				$print .= "<option value=$i>$i | $bulan</option>\n";
			}
			$now = 1;
			$now = $now + $i;
		}
		$print .= "</select>\n<!-- List Bulan -->\n";
		print $print;
	}
	
	function listTahun($id="thn",$sel=""){
		$html="<select id=\"$id\" name=\"$id\" class=\" it\">";
		$html.="<option value=''>| TAHUN</option>";
		for($i=date('Y')-5;$i<=date('Y');$i++){
			$html.="<option value='$i' ".(($i==$sel)?"selected":"").">$i</option>";
		}
		$html.="</select>";
		return $html;		
	}
	
	function createcrosstab(
	$col_name="idagama",
	$col_alias="agama",
	$col_from=" from mast_agama",
	$row_name="jenkel",
	$row_alias="Kategori",
	$row_from="FROM psb_daftar inner join mast_jenkel using(idjenkel) GROUP BY jenkel")
	{
		$rs = $this->db->query("select $col_name as temp_col_name,$col_alias as temp_col_alias $col_from");
		foreach($rs->result() as $item){
			$xtab_query = $xtab_query." \tsum(if(".$col_name."=".$item->temp_col_name.",1,0)) 
			as `".$item->temp_col_alias."`,\n";
		}
		$xtab_query = substr($xtab_query,0,strlen($xtabquery)-2);
		$xtab_query = "SELECT ".$row_name." as `".$row_alias."`, ".$xtab_query." ".$row_from;

		return $this->db->query($xtab_query);
	}
	
	function createcrosstab_basic(
	$col_name="",
	$col_alias="",
	$col_from="",
	$row_name="",
	$row_alias="",
	$row_from="")
	{
		$rs = $this->db->query("select $col_name as temp_col_name,$col_alias as temp_col_alias $col_from");

		foreach($rs->result() as $item){
			$xtab_query = $xtab_query." \tsum(if(".$col_name."=".$item->temp_col_name.",1,0)) as `".$item->temp_col_alias."`,\n";
		}
		$xtab_query = substr($xtab_query,0,strlen($xtabquery)-2);
		if($xtab_query!=''){
		$xtab_query = "SELECT ".$xtab_query." ".$row_from;
		}else{
		$xtab_query = "SELECT SUM(IF(YEAR(tgllulus)=year(now()),1,0)) AS `".date('Y')."` FROM mast_alumni";
		}
		
		return $this->db->query($xtab_query);
	}
}
?>