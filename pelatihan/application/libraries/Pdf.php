<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class pdf {
    
    function pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($paramx=NULL,$spesificsize = null)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
         
        if ($paramx == NULL)
        {
            $paramx = '"en-GB-x","A4","","",10,10,10,10,6,3';          
        }
		if(is_array($spesificsize)){
			return new mPDF($paramx,$spesificsize);
		}else{
		    return new mPDF($paramx);
		}
    }
}