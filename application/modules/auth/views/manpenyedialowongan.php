 <script>
	
     $(document).ready(function(){
        
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup( $.debounce(250,refreshData));
		$.ajax({
            type:'post',
            url:'<?=base_url()?>auth/page/data/manpenyedialowongan',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);

                        if(ret.success){
                        	$('#form1').trigger('reset');
                        }

                        alert(ret.text);
                    });
            }else{
                // $('#form1').trigger('reset');
            }
        });

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
			$('#id_edit').val('');
        });
		
		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>auth/page/editmanevent',
				type: 'post',
				data: { 
					'id':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					$('#id_edit').val(ret['id']);
					for(attrname in ret){
						$('#'+attrname).val(ret[attrname]);
					}
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>auth/page/delPerusahaan',
					type: 'post',
					data: { 
						'idperusahaan':$this.eq(index).attr('data'),
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
		$(document).on('click','a[title="Detail"]',function(){
			var $this = $('a[title="Detail"]');
			var index = $this.index($(this));
			
			$.ajax({
				url:'<?=base_url()?>auth/page/view/userpenyedia_data',
				type:'post',
				data:{ 'idperusahaan':$this.eq(index).attr('data'),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){},
				success:function(response){
					$('#div-detail').html(response);
					$('#myModal').modal('show',function(){
						keyboard:true
					});
					return false;
				}
			});
		
		}).on('click','.acc', function(){
			if(confirm("Apa anda yakin ingin Aktivasi?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>auth/page/setaktiv',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						//table: 'ngi_perusahaan',
						val : '1'
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		}).on('click','.dec', function(){
			if(confirm("Apa anda yakin ingin menolak Aktivasi?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>auth/page/setaktiv',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						//table:'ngi_perusahaan',
						val : '2'
					},
					beforeSend:function(){
					var $this = $('a[title="Detail"]');
					var index = $this.index($(this));
			
					$.ajax({
						url:'<?=base_url()?>auth/page/view/userpenyedia_data',
						type:'post',
						data:{ 'idperusahaan':$this.eq(index).attr('data'),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
					});
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		})
	
	
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#title').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});

    });
	
	function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>auth/page/data/manpenyedialowongan',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <!--<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>-->
		  <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		 </ul>
		<div class="tab-content">
			
			<div class="tab-pane active" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Daftar Data Perusahaan</h3>
					  <div class="box-tools">
						<div class="input-group pull-right" style="width: 150px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:200px" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						  </div>
						</div>
						<div class="btn-group">
						<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="4">4</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					</div>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
		
		
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modal-title2" style="text-transform:capitalize">DETAIL PERUSAHAAN</h4>
          </div>
          <div class="modal-body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                             <div id="div-detail">
	
	</div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>