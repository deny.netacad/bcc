<script>
	$(document).ready(function(){
		$('#iduser').change(function(){
			loadUsermodule();
		});
		
		$(document).on('submit','#form2',function(event) {
			
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					$('#alert1').fadeIn(1000);
					$('#alert1').fadeOut(1000);
				});		
			}else{
				$('#tb-module tbody').html('');
			}
		});
		
		$(document).on('click','input[name^="all"]',function(){
			var $this = $('input[name^="all"]');
			var index = $this.index($(this));
			$('input[name^="d["]').eq(index).prop('checked',$this.eq(index).is(':checked'));
			$('input[name^="e["]').eq(index).prop('checked',$this.eq(index).is(':checked'));
			$('input[name^="v["]').eq(index).prop('checked',$this.eq(index).is(':checked'));
			$('input[name^="i["]').eq(index).prop('checked',$this.eq(index).is(':checked'));
		});
		
		$(document).on('click','input[name^="d["]',function(){
			var $this = $('input[name^="d["]');
			var index = $this.index($(this));
			setCheckAll(index);
		});
		$(document).on('click','input[name^="e["]',function(){
			var $this = $('input[name^="e["]');
			var index = $this.index($(this));
			setCheckAll(index);
		});
		$(document).on('click','input[name^="v["]',function(){
			var $this = $('input[name^="v["]');
			var index = $this.index($(this));
			setCheckAll(index);
		});
		$(document).on('click','input[name^="i["]',function(){
			var $this = $('input[name^="i["]');
			var index = $this.index($(this));
			setCheckAll(index);
		});
		
		
		
		$('#alert1').hide();
		
		
	});
	
	function loadUsermodule(){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>auth/page/data/manhakakses',
			data:{ 'iduser':$('#iduser').val(),
			'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
			beforeSend:function(){
				$('#result2').loading('stop');
				$('#result2').loading();
			},
			success:function(response){
				$('#result2').loading('stop');
				$('#result2').html(response);
			}
		});
	}
	
	function setCheckAll(index){
		var v_d = $('input[name^="d["]').eq(index).prop('checked');
		var v_e = $('input[name^="e["]').eq(index).prop('checked');
		var v_v = $('input[name^="v["]').eq(index).prop('checked');
		var v_i = $('input[name^="i["]').eq(index).prop('checked');
		if(v_d==false || v_e==false || v_v==false || v_i==false){
			$('input[name^="all["]').eq(index).prop('checked',false);
		}else{
			$('input[name^="all["]').eq(index).prop('checked',true);
		}
	}
	
</script>
<ul class="breadcrumb">
 <li><a href="#"><i class="fa fa-dashboard"></i> Settings</a></li> <span class="divider">/</span></li>
  <li class="active">Manajemen Hak Akses</li>
</ul>

<section class="content">
	<div class="box">
		<div class="box-header with-border">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active"><a href="#update" data-toggle="tab"><i class="icomoon-pencil2"></i> Update</a></li>
		</ul>
		</div>
		<div class="box-body">
			<div class="tab-content" style="overflow:visible">
				<div class="tab-pane active" id="update">
					<div id="alert1" class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert">x</button>
					  <strong>Sukses!</strong> Data telah tersimpan
					</div>
					<form id="form2" name="form2" action="<?=base_url()?>auth/page/saveManhakakses" class="form-horizontal" method="post" enctype="multipart/form-data">
						<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
						<BR><div class="control-group">
								<label class="control-label" for="iduser">Username </label>
								<div >
									<?php
										echo $this->auth_list->listUser();
									?>
								</div>
							</div><div id="result2"></div>
						</form>	
				</div>
			</div>
		</div>
	</div>
</section>
	
