<script>
$(document).ready(function(){
	$('#alert2').hide();
});
</script>
<div id="alert2" class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Sukses!</strong> Data telah dihapus
</div>
<?php
	$where = " ";
	$per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
	$cari = $this->input->post('cari');
	
	if($cari!="") $where.= " where (a.username like '%".$cari."%' or nama like '%".$cari."%')";
	
	$n = intval($this->uri->segment(5));
	$q = "select *,date_format(a.tmstamp,'%d-%m-%Y %H:%i:%s') as tmstamp_ from mast_user a left join mast_bidang b on a.idbidang=b.idbidang";
	$rs = $this->db->query("$q $where");
	$row = $rs->row_array();
	$totrows = $rs->num_rows();

	$config['base_url'] = base_url().'auth/page/data/manuser';

	$config['total_rows'] 			= $totrows;
	$config['per_page'] 			= $per_page;
	$config['uri_segment'] 			= 5;
	$config['is_ajax_paging']    	= TRUE;

	$this->pagination->initialize($config);

	$data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("$q $where order by a.iduser limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
	");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
<span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
<?=$data['pagination']?>
</div>
<img id="noimage" style="display:none" />
<table class="table table-hover">
  <colgroup>
	<col class="con0" style="width: 4%" />
	
  </colgroup>
  <thead class="breadcrumb">
	<tr>
	  <th>#</th>
	  <th>USERNAME</th>
	  <th>NAMA</th>
	  <th>BIDANG</th>
	  <th>CREATE DATE</th>
	  <th>PROSES</th>
	</tr>
  </thead>
  <tbody>
	<?php
	foreach($data['posts']->result() as $item){ $n++;
	?>
	<tr>
	  <td align="center"><?=$n?>.</td>
	  <td>
	  <div class="text-left"><?=$item->username?></div>
	  </td>
	  <td>
	  <div class="text-left"><?=$item->nama?></div>
	  </td>
	  <td>
	  <div class="text-left"><?=$item->bidang?></div>
	  </td>
	  <td>
	  <div class="text-left"><small><i class="icomoon-calendar"></i> <?=$item->tmstamp_?></small></div>
	  </td>
	  <td>
		<div class="text-center">
			<a href="#" title="Edit" data="<?=$item->iduser?>"><i class="icomoon-pencil2"></i></a>
			<a href="#" title="Delete" data="<?=$item->iduser?>"><i class="icomoon-remove red"></i></a>
		</div>
	  </td>
	</tr>
	<?php
	}
	?>
  </tbody>
</table>