
<html>
<head>
<style type="text/css">
            #tabelwarna1 { width: 567px; height:300px;}
            td {
                font: bold 11px Trebuchet MS;
                border-right: 0px solid #C1DAD7;
                border-bottom: 0px solid #C1DAD7;
                border-left: 0px solid #C1DAD7;
                padding: 1px 1px 1px 1px ;
            }
            .style1 {
                font-size: 10px;
                font-weight: bold;
            }
        </style>
</head>
<body>
<table width="200" border="0" cellspacing="1" cellpadding="1" id="tabelwarna1" style="border:1px solid #000;">
<tr>
<td valign="top">
<table width="100%" cellpadding="1" cellspacing="2">
<tr>
<td colspan="3"><strong><u>PENDIDIKAN FORMAL</u></strong></td>
</tr>
<tr>
<td width="156" valign="top">DIPLOMA III</td>
<td width="173" valign="top">: ANALISIS KIMIA</td>
<td width="48" valign="top">Th.2018</td>
</tr>
<tr>
<td colspan="3" height="10px">&nbsp;</td>
</tr>
<tr>
<td colspan="3" style="vertical-align:middle;"><hr size="1" width="385" align="left" /></td>
</tr>
<tr>
<td colspan="3"><strong><u>KETERAMPILAN</u></strong></td>
</tr>
</table>
</td>
<td>
<table width="150" cellpadding="1" cellspacing="2">
<tr>
<td width="142"><br />
<br />
<br />
<br />
<br />
</td>
</tr>
<tr>
<td style="text-align:center; font-weight:bold;"><span style="text-align:center; font-weight:bold;">PENGANTAR KERJA AHLI MADYA</span></td>
</tr>
<tr>
<td>
<br /><br /><br /><br /><br /><br />
</td>
</tr>
<tr>
<td style="text-align:center;"><span style="text-align:center;font-size:7.5pt">KURNIATI, SE. MM.</span></td>
</tr>
<tr>
<td style="border-top:1px solid #000;"><div align="center"><span style="text-align:center;font-size:7.5pt">NIP.197210151998032007</span></div></td>
</tr>
</table>
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
<table width="100%" cellpadding="1" cellspacing="1" id="tabelwarna1">
<tr>
<td colspan="23" align="right"><span class="style1">Kartu AK/I</span> </td>
</tr>
<tr>
<td rowspan="3"><img src="https://ayokitakerja.kemnaker.go.id/upload/kantor/3201.jpg" height="65" width="77" /></td>
<td colspan="22" style="text-align:center; font-weight:bold;font-size:11pt;vertical-align:middle;">PEMERINTAH <b>KABUPATEN BOGOR</b></td>
</tr>
<tr>
<td colspan="22" style="text-align:center; font-weight:bold;font-size:10pt;vertical-align:middle;"><b>DINAS TENAGA KERJA</b></td>
</tr>
<tr>
<td colspan="22" style="text-align:center; font-weight:bold;font-size:10pt;vertical-align:middle;"><b>Jl. Bersih No.02 Kompl. Pemda Cibinong Kab. Bogor, , Kabupaten Bogor, Provinsi Jawa Barat 16913 Telp : 8757668</b></td>
</tr>
<tr>
<td colspan="23" style="border:1px solid #333;text-align:center; font-weight:bold; height:20px; font-size:9pt;vertical-align:middle;">KARTU TANDA BUKTI PENDAFTARAN PENCARI KERJA</td>
</tr>
<tr>
<td colspan="2">No. Pendaftaran Pencari Kerja </td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;" width="19">3</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;" width="19">2</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;" width="19">0</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;" width="19">1</td>
<td width="19">&nbsp;</td>
<td style="border:1px solid #333; text-align:center; font-weight:bold;" width="19">0</td>
<td style="border:1px solid #333; text-align:center; font-weight:bold;" width="19">0</td>
<td style="border:1px solid #333; text-align:center; font-weight:bold;" width="19">0</td>
<td style="border:1px solid #333; text-align:center; font-weight:bold;" width="19">1</td>
<td style="border:1px solid #333; text-align:center; font-weight:bold;" width="19">9</td>
<td style="border:1px solid #333; text-align:center; font-weight:bold;" width="19">2</td>
<td style="border:1px solid #333; text-align:center; font-weight:bold;" width="19">1</td>
<td width="19">&nbsp;</td>
<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;">4</td>
<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;">2</td>
<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;">1</td>
<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;">9</td>
</tr>
<tr>
<td colspan="2">No. Induk Kependudukan</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">3</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">2</td>
<td>&nbsp;</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">1</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">2</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">6</td>
<td>&nbsp;</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">6</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">3</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">9</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">7</td>
<td>&nbsp;</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">0</td>
<td style="border:1px solid #333;text-align:center; font-weight:bold;">2</td>
</tr>
<tr>
<td rowspan="5" width="92" style="border:1px dashed #333;"><img src="https://ayokitakerja.kemnaker.go.id/upload/pencaker/4a4eaffc80f2a70767150e0b49728a8e.jpeg" width="90" height="110" /></td>
<td>&nbsp;Nama Lengkap</td>
<td>&nbsp;&nbsp;:</td>
<td colspan="18"> MOCHAMAD ZULFAN FIRDAUS</td>
</tr>
<tr>
<td>&nbsp;Tempat/Tgl.Lahir</td>
<td>&nbsp;&nbsp;:</td>
<td colspan="18">BOGOR / 06-03-1997</td>
</tr>
<tr>
<td>&nbsp;Jenis Kelamin</td>
<td>&nbsp;&nbsp;:</td>
<td colspan="18">LAKI - LAKI</td>
</tr>
<tr>
<td style="border-bottom:1px solid #fff;">&nbsp;Status</td>
<td>&nbsp;&nbsp;:</td>
<td colspan="18">BELUM KAWIN</td>
</tr>
<tr>
<td height="21">&nbsp;Agama</td>
<td>&nbsp;&nbsp;:</td>
<td colspan="18">ISLAM</td>
</tr>
<tr>
<td rowspan="3" style="font-size:6pt;text-align:center; font-weight:bold;vertical-align:middle;">Tanda Tangan<br />Pencari Kerja</td>
<td width="79">&nbsp;Alamat</td>
<td>&nbsp;&nbsp;:</td>
<td rowspan="2" colspan="18">KP CIPAYUNG RT 002/004 DS CIPAYUNG GIRANG KECAMATAN MEGAMENDUNG, KABUPATEN BOGOR, PROVINSI JAWA BARAT 16770</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;Telp/Hp</td>
<td>&nbsp;&nbsp;:</td>
<td colspan="18"> 081343996363 </td>
</tr>
<tr>
<td rowspan="2"></td>
<td width="79">&nbsp;Berlaku s.d </td>
<td>&nbsp;&nbsp;:</td>
<td colspan="18">11-04-2021</td>
</tr>
</table></td>
</tr>
</table>
<br><br>
</body>
</html>