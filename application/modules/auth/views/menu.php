<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-settings"></i> <span>&nbsp;Setting</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>auth/manmodul"><i class="text-orange icomoon-storage"></i>&nbsp;Manajemen Modul</a></li>
		<li><a href="<?=base_url()?>auth/manuser"><i class="text-orange icomoon-user"></i>&nbsp;Manajemen User</a></li>
		<li><a href="<?=base_url()?>auth/manhakakses"><i class="text-orange icomoon-key"></i>&nbsp;Manajemen Hak Akses</a></li>
		<li><a href="<?=base_url()?>auth/mankategorilowongan"><i class="text-orange icomoon-stack"></i>&nbsp;Manajemen Kategori Lowongan</a></li>
		<li><a href="<?=base_url()?>auth/manlowongankerja"><i class="text-orange icomoon-file"></i>&nbsp;Manajemen Lowongan Kerja</a></li>
		<li><a href="<?=base_url()?>auth/manevent"><i class="text-orange icomoon-tree"></i>&nbsp;Manajemen Event</a></li>
		<li><a href="<?=base_url()?>auth/manpenyedialowongan"><i class="text-orange icomoon-office"></i>&nbsp;Manajemen Penyedia Lowongan</a></li>
		<li><a href="<?=base_url()?>auth/mantipepenyedialowongan"><i class="text-orange icomoon-users"></i>&nbsp;Manajemen Tipe Penyedia Lowongan</a></li>
		<li><a href="<?=base_url()?>auth/manpostinglowongan"><i class="text-orange icomoon-profile"></i>&nbsp;Manajemen Posting Lowongan</a></li>
		<li><a href="<?=base_url()?>auth/mantrackingpelamar"><i class="text-orange icomoon-eye"></i>&nbsp;Manajemen Tracking & Kelola Pelamar</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-users"></i> <span>&nbsp;Report Pelamar</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>auth/lappelamar"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Semua Pelamar</a></li>
		<li><a href="<?=base_url()?>auth/lappelamarevent"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Pelamar Event</a></li>
		<li><a href="<?=base_url()?>auth/cetakak1"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Cetak AK-1</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-office"></i> <span>&nbsp;Report Penyedia Lowongan</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>auth/lappenyedialowongan"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Penyedia Lowongan</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-briefcase"></i> <span>&nbsp;Report Lowongan</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>auth/laplowongan"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Lowongan Per Perusahaan</span></a></li>
	</ul>
</li>
