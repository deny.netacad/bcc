<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	
function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}

	var xhr = $.ajax();
	$(document).ready(function(){
		
	
		
		var config_pengantar = {
            toolbar : 'MyToolbar',
            height: '350'
        };
		
		 $('#balasan').ckeditor(config_pengantar);
		
		$('.dt').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		});
		//showFaktur();
		
		$('.dt').mask('99-99-9999');
  	
		$(document).on('change','#checkall',function(){
			$('input[name*="checkthis"]').prop('checked',$(this).prop('checked'));
		});
		


		$(document).on('click','a[title=sendmail]', function(){
			var $this = $('a[title=sendmail]');
			//alert('ok');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>auth/page/detailpengaduan',
				type: 'post',
				data: { 
					'id':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(1).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					for(attrname in ret){
						if(attrname=='id'){
							$('#id_edit').val(ret[attrname]);
						}else{
							$('#'+attrname).val(ret[attrname]);
						}
					}
				}
			});
        });



		$(document).on('submit','#form-posting',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					showFaktur('');
					
				});		
			}else{
				showFaktur('');
			}
		});


		$(document).on('submit','#form1',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					//var ret = $.parseJSON(data);
					alert('Email Terkirim');
					//showFaktur('');
					
				});		
			}else{
				showFaktur('');
			}
		});
		
		$(document).on('reset','#form-posting',function(){
			resetForm($('#form-posting'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
		});
		
		showFaktur('');
		$('#alert1').hide();
		
		$(document).on('submit','#form-posting2',function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
					showFaktur2('');
					
				});		
			}else{
				showFaktur2('');
			}
		});
		
		$(document).on('reset','#form-posting2',function(){
			resetForm($('#form-posting2'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
		});
		
		
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:

					showFaktur('');
				break;
				case 1:
				//alert('ooo');
					//showFaktur('');
				break;
			}
		});
		
		showSetting();
	});
	
	function showFaktur(){
		//alert('masuk');
		$.ajax({
			type:'post',
			url:'<?=base_url()?>auth/page/data/inpengaduan',
			data:{'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			  },
			beforeSend:function(){
				$('#result').loading('stop');
				$('#result').loading();
			},
			success:function(response){
				$('#result').loading('stop');
				$('#result').html(response);
			}
		});
	}
	
	
	
</script>

<ul class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Settings</a></li> <span class="divider">/</span></li>
  <li class="active">Manajemen Pengaduan</li>
</ul>

<section class="content">
 <div class="box">
 <div class="box-header with-border">
	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#update" data-toggle="tab"><i class="icomoon-file3"></i> Data Pengaduan ASN</a></li>
		<li ><a href="#daftar" data-toggle="tab"><i class="icomoon-file3"></i> Form Detail Pengaduan ASN</a></li>

	</ul>
 </div>



<div class="box-body">
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
		
		<form name="form-posting" id="form-posting" method="post" action="<?=base_url()?>auth/page/postingBonuspending2" enctype="multipart/form-data">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

					<div class="row-fluid">
						<div class="pull-right">
							<div class="spinner"></div>
							<input type="submit" id="submit" name="submit" value="POSTING" class="btn btn-primary" />
						</div>
					</div>
					<div id="result">
					
					</div>
				</form>
	</div>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="">
				<div id="spinner" style="display:inline-block"></div>
				
<form id="form1" name="form1" action="<?=base_url()?>auth/page/sendmailpengaduan" class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
			<div class="row-fluid">
				<div class="span12">
					<div class="accordion-heading">
						<BR><div class="control-group">
							<label class="control-label" for="module">Pengirim</label>
							<div class="controls">
								<input type="text" class="form-control"id="registername" name="registername" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="module">Email</label>
							<div class="controls">
								<input type="text" class="form-control"id="registerreg" name="registerreg" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />
							</div>
						</div>
<BR><div class="control-group">
							<label class="control-label" for="module">Alamat</label>
							<div class="controls">
								<input type="text" class="form-control"id="registeraddr" name="registeraddr" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />
							</div>
						</div>
<BR><div class="control-group">
							<label class="control-label" for="module">Pekerjaan </label>
							<div class="controls">
								<input type="text" class="form-control"id="registerwork" name="registerwork" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />
							</div>
						</div>
<BR><div class="control-group">
							<label class="control-label" for="module">Telepon </label>
							<div class="controls">
								<input type="text" class="form-control"id="registertlp" name="registertlp" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />
							</div>
						</div>
<BR><div class="control-group">
							<label class="control-label" for="module">Kebutuhan Data</label>
							<div class="controls">
								<input type="text" class="form-control"id="registerwant" name="registerwant" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="module">digunakan Untuk </label>
							<div class="controls">
								<input type="text" class="form-control"id="registerfor" name="registerfor" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="module">Balasan  </label>
							<div class="controls">
								<textarea id="balasan" name="balasan" ></textarea>
							<!--	<input type="text" class="form-control"id="registertlp" name="registertlp" readonly class="inpxt-block-level" maxlength="" required placeHolder="Pemohon" />-->
							</div>
						</div>
						

						

												




						<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
				</div> <!-- end of span5 -->
			</div>
		</form>





			
			</div>
			
		</div>
		<div id="result"></div>			
	</div>
</div>
</div>
</section>



