<script>

	
	$(document).ready(function(){
		
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup(function(){
			refreshData();
		});
		
		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#size').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
		
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>auth/page/editMansize',
				type: 'post',
				data: { 
					'id':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					for(attrname in ret){
						if(attrname=='id'){
							$('#id_edit').val(ret[attrname]);
						}else{
							$('#'+attrname).val(ret[attrname]);
						}
					}
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>auth/page/delMansize',
					type: 'post',
					data: { 
						'id':$this.eq(index).attr('data')
						,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					$('#alert1').html(ret.text);
					$('#alert1').fadeIn(1000);
					$('#alert1').fadeOut(1000);
					$('#form1').trigger('reset');
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
		});
		
		$('#alert1').hide();
		
	});
	
	
	function refreshData(){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>auth/page/data/mansize',
			data:{ 
				'per_page':$('#per_page').val()
				,'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
			beforeSend:function(){
				$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
			},
			success:function(response){
				$('#result').html(response);
			}
		});
	}
	
</script>
<ul class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Settings</a></li> <span class="divider">/</span></li>
  <li class="active">Manajemen Size</li>
</ul>

<section class="content">
 <div class="box">
 <div class="box-header with-border">
	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#update" data-toggle="tab"><i class="icomoon-file3"></i> Update</a></li>
		<li class=""><a href="#daftar" data-toggle="tab"><i class="icomoon-table2"></i> Daftar</a></li>
	</ul>
 </div>

<div class="box-body">
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
		
		<form id="form1" name="form1" action="<?=base_url()?>auth/page/saveMansize" class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
			<div class="row-fluid">
				<div class="span9">
					<div class="accordion-heading">
						<BR><div class="control-group">
							<label class="control-label" for="kode_size">Kode Size</label>
							<div class="controls">
								<input type="text" class="form-control"id="kode_size" name="kode_size" class="inpxt-block-level" maxlength="" required placeHolder="Kode Size" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="nama_size">Nama Size</label>
							<div class="controls">
								<input type="text" class="form-control"id="nama_size" name="nama_size" class="input-block-level" maxlength="100" required placeHolder="Nama Size" />
							</div>
						</div><br>
						<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
				</div> <!-- end of span5 -->
			</div>
		</form>
	</div>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="pull-right">
				<div id="spinner" style="display:inline-block"></div>
				<div class="input-append form-inline">
				  <input type="text" class="form-control" id="cari" name="cari" placeHolder="Cari ..." />
					<div class="btn-group">
						<select class="form-control"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="4">4</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					</div>
				</div>   
			
			</div>
			
		</div>
		<div id="result"></div>			
	</div>
</div>
</div>
</section>
		
		
	