<script>
	function bidangFormatResult(data) {
        var markup= "<li>"+ data.bidang+"</li>";
        return markup;
    }

    function bidangFormatSelection(data) {
        return data.bidang;
    }
	
	
	$(document).ready(function(){
		
		
		$('#form1').enterform();
		$('#username').focus();
		$('#userpass').focusTo($('#submit'));
		
		$("#idbidang").select2({
			id: function(e) { return e.idbidang }, 
			placeholder: "Pilih Bidang",
			minimumInputLength: 0,
			multiple: false,
			allowClear:true,
			ajax: { 
				url: "<?=base_url()?>auth/ajaxfile/caribidang",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: bidangFormatResult, 
			formatSelection: bidangFormatSelection
		});
		
		// ajax pagination
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup(function(){
			refreshData();
		});
		
		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		
		$('#alert1').hide();
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
				break;
				case 1:
					refreshData();
				break;
			}
		});
		
		
		$('#submit').keydown(function(e){
			if(e.which==13){
				$('#form1').submit();
			}
		});
		
		$("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					$('#alert1').html(ret.text);
					$('#alert1').fadeIn(1000);
					$('#alert1').fadeOut(1000);
					$('#form1').trigger('reset');
				});		
			}else{
				$('#form1').trigger('reset');
			}
		});
		
		$('#form1').on('reset',function(){
			resetForm($('#form1'));
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			$('#idbidang').select2('val','');
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
		});
		
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>auth/page/editManuser',
				type: 'post',
				data: { 
					'iduser':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					for(attrname in ret){
						if(attrname=='iduser'){
							$('#id_edit').val(ret[attrname]);
						}else{
							$('#'+attrname).val(ret[attrname]);
						}
					}
					$('#idbidang').select2('data',{'idbidang':ret.idbidang,'bidang':ret.bidang});
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>auth/page/delManuser',
					type: 'post',
					data: { 
						'iduser':$this.eq(index).attr('data')
						,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
		
	});
	
	
	function refreshData(){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>auth/page/data/manuser',
			data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),
			'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
			beforeSend:function(){
				$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
			},
			success:function(response){
				$('#result').html(response);
			}
		});
	}
	
	
</script>
<ul class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Settings</a></li> <span class="divider">/</span></li>
  <li class="active">Manajemen User</li>
</ul>

 <section class="content">
 <div class="box">
 <div class="box-header with-border">
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#update" data-toggle="tab"><i class="icomoon-file3"></i> Update</a></li>
	<li class=""><a href="#daftar" data-toggle="tab"><i class="icomoon-table2"></i> Daftar</a></li>
</ul>

		
</div>
<div class="box-body">
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="update">
		<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
		
		<form id="form1" name="form1" action="<?=base_url()?>auth/page/saveManuser" class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id_edit" name="id_edit" value="" />
			<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
			<div class="row-fluid">
				<div class="span9">
					<div class="accordion-heading">
						<BR><div class="control-group">
							<label class="control-label" for="username">Username</label>
							<div class="controls">
								<input type="text" class="form-control" id="username" name="username" class="inpxt-block-level" maxlength="20" required placeHolder="Username" />
							</div>
						</div>
							<BR><div class="control-group">
							<label class="control-label" for="idjenis">Bidang </label>
							<div class="controls">
								<input type="hidden"  id="idbidang" name="idbidang" style="width:400px"/>
							</div>
						</div>
						
						<BR><div class="control-group">
							<label class="control-label" for="nama">Nama Lengkap</label>
							<div class="controls">
								<input type="text" class="form-control"id="nama" name="nama" class="input-block-level" maxlength="50" required placeHolder="Nama Lengkap" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="email">Email</label>
							<div class="controls">
								<input type="text" class="form-control"name="email" value="" id="email" maxlength="50" placeHolder="Valid Email" />
							</div>
						</div>
						<BR><div class="control-group">
							<label class="control-label" for="userpass">Password</label>
							<div class="controls">
								<input type="password" name="userpass" value="" class="form-control" id="userpass" maxlength="30" placeHolder="Password" />
							</div>
						</div>
						<BR><div class="control-group">
							<div class="controls">
								<button type="submit" id="submit" class="btn btn-primary">Simpan</button>
								<input type="reset" class="btn" value="Batal" />
							</div>
						</div>
					</div>
				</div> <!-- end of span5 -->
			</div>
		</form>
	</div>
	<div class="tab-pane" id="daftar">
		<div class="row-fluid">
			<div class="pull-right">
				<div id="spinner" style="display:inline-block"></div>
				<div class="input-append form-inline">
				  <input type="text" class="form-control" id="cari" name="cari" placeHolder="Cari ..." />
				  <div class="btn-group">
					<select class="form-control"  id="per_page" name="per_page" style="width:60px">
					  <option value="">-</option>
					  <option value="4">4</option>
					  <option value="25">25</option>
					  <option value="50">50</option>
					</select>
				  </div>
				</div>        
				
			</div>
		</div>
		<div id="result"></div>		
	</div>
	</div>
</div>
<div class="box-footer">        </div>
</div>
</section>
	