 <?php
$q = "SELECT a.*,DATE_FORMAT(a.log_pengajuan,'%d %M %Y %h:%i:%s') AS log_pengajuan,e.category AS jns_usaha,b.province AS propinsi,c.subdistrict_name AS kec,d.city_name AS kota FROM ngi_perusahaan a 
LEFT JOIN ngi_provinsi b ON a.province_id=b.id
LEFT JOIN ngi_kecamatan c ON a.kec_id=c.subdistrict_id
LEFT JOIN ngi_kota d ON a.kab_id=d.city_id
LEFT JOIN ngi_jobcategory e ON a.jenisusaha=e.id WHERE a.idperusahaan='".$this->input->post('idperusahaan')."' ";
$rs = $this->db->query($q);
$item = $rs->row();
?>

 <div class="modal-body">
 <section class="content">
		
      <div class="row">
        <div class="col-md-12">
		
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="User profile picture">

              <h4 class="profile-username text-center"><?=$item->nama_perusahaan?></h4>
			  <!--<p class="text-center" style="color:blue;"><?=$item->email?></p>-->
              <p class="text-muted text-center"><?=$item->jns_usaha?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Terdaftar Sejak</b> <a class="pull-right"><?=$item->log_pengajuan?></a>
                </li>
              </ul>

             <!-- <a href="#" class="btn btn-primary btn-block"><b>Download Resume</b></a>-->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About <?=$item->nama_perusahaan?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Alamat</strong>

              <p class="text-muted">
				<?=$item->alamat?>,<?=$item->kec?>,<?=$item->kota?><?=$item->propinsi?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted"><?=$item->alamat?> <?=$item->nm_kecamatan?></p>

              <hr>
			  <!--<p><?=$item->pass_ori?></p>
			  <p><?=$item->username?></p>-->
<!--
              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>-->
			  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-12">
		
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#timeline" data-toggle="tab">Histori <?=$item->nama_perusahaan?></a></li>
            </ul>
			
			 <?php
                                   $rs = $this->db->query("SELECT a.*,DATE_FORMAT(a.date_start,'%d %M %Y') AS dt_start,DATE_FORMAT(a.date_end, '%d %M %Y') AS dt_end,b.nama_perusahaan AS nm_perusahaan,c.subdistrict_name AS nm_kecmatan,d.category AS nm_category,e.nama_job AS nm_job FROM ngi_joblist a
LEFT JOIN ngi_perusahaan b ON a.idperusahaan=b.idperusahaan
LEFT JOIN ngi_kecamatan c ON a.idkecamatan=c.subdistrict_id
LEFT JOIN ngi_jobcategory d ON a.idcategory=d.id
LEFT JOIN ngi_job e ON a.posisi=e.id WHERE a.idperusahaan='".$this->input->post('idperusahaan')."' ORDER BY a.log DESC");
                                    foreach($rs->result() as $item2){
                                 
                                    ?>
            <div class="tab-content">
              <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          <?=$item2->dt_start?> s/d <?=$item2->dt_end?>
                        </span>
                  </li>
                  <li>
                    <i class="fa fa-user bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> <?=$item2->jam?></span>

                      <h3 class="timeline-header"> <a href="#"><?=$item2->title?></a> bagian&nbsp;<?=$item2->nm_job?></h3>
                    </div>
                  </li>
                 
                <?php } ?>
                
                 
                  <!-- END timeline item -->
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
</div>