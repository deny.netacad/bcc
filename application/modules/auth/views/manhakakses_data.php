
<script>
$(document).ready(function(){
	$('#alert2').hide();
	$('a[data-toggle="tooltip"]').tooltip({
		placement:'right'
	});
	
	$('a.del-row').each(function(index,item){
		$(item).click(function(){
			if(confirm('Hapus item ini ?')){
				$(item).parent().parent().parent().remove();
			}
		});
	});
	try{
	var elem1 = '<?=$this->auth_list->listModule("idmodule[]")?>';
	
	var elem11 = $.create('input',{
		type: 'checkbox',
		name: 'all[]',
		value: '1'
	});
	var elem2 = $.create('input',{
		type: 'checkbox',
		name: 'd[]',
		value: '1'
	});
	
	var elem3 = $.create('input',{
		type: 'checkbox',
		name: 'e[]',
		value: '1'
	});
	var elem4 = $.create('input',{
		type: 'checkbox',
		name: 'v[]',
		value: '1'
	});
	
	var elem5 = $.create('input',{
		type: 'checkbox',
		name: 'i[]',
		value: '1'
	});
	
	var elem6 = $('<a href="#" class="del-row" data-toggle="tooltip" title="Hapus Modul"><i class="icon-trash"></i></a>').centerEl();
		
	
	$('#btn-add').mytable({
		table: $("#tb-module"),
		data: ["auto",elem1,$(elem11).centerEl(),$(elem2).centerEl(),$(elem3).centerEl(),$(elem4).centerEl(),$(elem5).centerEl(),elem6 ],
		onAddrow: function(param,data,no){
			$('input[name^="idmodule["]').eq(no).attr('name','idmodule['+no+']');
			$('input[name^="d["]').eq(no).attr('name','d['+no+']');
			$('input[name^="e["]').eq(no).attr('name','e['+no+']');
			$('input[name^="v["]').eq(no).attr('name','v['+no+']');
			$('input[name^="i["]').eq(no).attr('name','i['+no+']');
			
			$('.del-row').eq(no).delRow({ 
				id: no,
				onBeforeDelrow:function(index){
					
				},
				onDelrow1:function(index){
					
				}
			});
		}
	});
	}catch(e){ alert(e.message); }
	
});
</script>
<div id="alert2" class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Sukses!</strong> Data telah dihapus
</div>
<?php
$rs = $this->db->query("select idmodule from mast_default_module where iduser='".$this->input->post('iduser')."'");
$item = $rs->row();
?>
<BR><div class="control-group">
	<label class="control-label" for="iddefault">Halaman Modul Default</label>
	<div class="controls">
		<?php
		echo $this->auth_list->listModule("iddefault",$item->idmodule);
		?>
	</div>
</div>
<table class="table table-hover" id="tb-module">
  <colgroup>
	<col class="con0" style="width: 4%" />
	
  </colgroup>
  <thead class="breadcrumb">
	<tr>
	  <th>#</th>
	  <th>MODULE</th>
	  <th><div class="text-center">ALL AKSES</div></th>
	  <th><div class="text-center">DELETE</div></th>
	  <th><div class="text-center">EDIT</div></th>
	  <th><div class="text-center">VIEW</div></th>
	  <th><div class="text-center">INSERT</div></th>
	  <th><div class="text-center">PROSES</div></th>
	</tr>
  </thead>
  <tbody>
	<?php
	$rs = $this->db->query("select a.*,b.d,b.e,b.v,b.i from mast_module a
	inner join mast_usermodule b on a.idmodule=b.idmodule
	where b.iduser='".$this->input->post('iduser')."'");
	$x = 0;
	$n = 0;
	foreach($rs->result() as $item){ $n++;
	$isall = $item->d+$item->e+$item->v+$item->i;
	?>
	<tr>
	  <td align="center"><span class="auto"><?=$n?></span></td>
	  <td>
	  <input type="hidden" name="idmodule[<?=$x?>]" value="<?=$item->idmodule?>" />
	  <div class="text-left"><?=$item->module?></div>
	  </td>
	  <td>
	  <div class="text-center">
		<input type="checkbox" name="all[<?=$x?>]" value="1" <?=(($isall==4)?"checked":"")?>/>
	  </div>
	  </td>
	  <td>
	  <div class="text-center">
		<input type="checkbox" name="d[<?=$x?>]" value="1" <?=(($item->d==1)?"checked":"")?>/>
	  </div>
	  </td>
	  <td>
	  <div class="text-center">
		<input type="checkbox" name="e[<?=$x?>]" value="1" <?=(($item->e==1)?"checked":"")?>/>
	  </div>
	  </td>
	  <td>
	  <div class="text-center">
		<input type="checkbox" name="v[<?=$x?>]" value="1" <?=(($item->v==1)?"checked":"")?>/>
	  </div>
	  </td>
	  <td>
	  <div class="text-center">
		<input type="checkbox" name="i[<?=$x?>]" value="1" <?=(($item->i==1)?"checked":"")?>/>
	  </div>
	  </td>
	  <td>
		<div class="text-center">
			<a href="#" class="del-row" data-toggle="tooltip" title="Hapus Modul"><i class="icomoon-remove red"></i></a>
		</div>
	  </td>
	</tr>
	<?php
	$x++;
	}
	?>
  </tbody>
  <tfoot>
	<tr>
		<th colspan="7">
		<a id="btn-add" class="btn btn-small btn-success" data-toggle="tooltip" title="Tambah Modul">Tambah Modules</a>
		<button type="submit" class="btn btn-small btn-primary"><i class="icon-save"></i> Simpan</button>
		</th>
	</tr>
  </tfoot>
</table>