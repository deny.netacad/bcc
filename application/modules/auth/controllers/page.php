<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $modules = "auth";

	function __construct()
	{
		parent::__construct();
		$this->load->model('auth/user');
		$this->load->model('auth/auth_list');

	}

	function index(){
		if ($this->session->userdata('is_login_portal_disnakerbogor'))
		{
			$this->load->view($this->modules.'/main',$this->data);
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}


	/*function balasmail(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}*/


	//========== new ===========//
	function saveGantipass(){

		$data['iduser'] = $this->session->userdata('user_id');
		$data['userpass'] = md5($this->input->post('userpass'));
		$rs = $this->db->get_where("mast_user",$data);
		if($rs->num_rows()>0){
			if($this->input->post('userpass2')==$this->input->post('userpass3')){
				$dt['userpass'] = md5($this->input->post('userpass2'));
				if(!$this->db->update("mast_user",$dt,$data)){
					$ret['text'] = "Terjadi Kesalahan\n".$this->db->_error_message();
				}else{
					$ret['text'] = "Password telah diganti";
				}
			}else{
				$ret['text'] = "Password Perulangan salah";
			}
		}else{
			$ret['text'] = "Password lama salah";
		}
		echo json_encode($ret);
	}

	function saveManmodul(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","idmodule");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->auth_mod->save("mast_module",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}

	function editManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_module",$data);
		echo json_encode($rs->row());
	}

	function detailppid(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("section_calonseller",$data);
		echo json_encode($rs->row());
	}

	function delManmodul(){
		$ret['text'] = 'Item terhapus';
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("mast_module",$data)){
			$ret['text'] = "Terjadi kesalahan";
		}else{
			$ret['text'] = "Item terhapus";
		}
		echo json_encode($ret);
	}

	function saveManhakakses(){
		$ret['error'] = false;
		$ret['txt'] = "Data Tersimpan";
		$arrnot = array("");

		$data['iduser'] = $this->input->post('iduser');
		$this->db->delete("mast_usermodule",$data);
		$this->db->delete("mast_default_module",$data);
		$data2['iduser'] = $data['iduser'];
		$data2['idmodule'] = $this->input->post('iddefault');
		if(!$this->db->insert("mast_default_module",$data2)){
			$ret['error'] = true;
			$ret['txt'] = "Terjadi Kesalahan";
			exit;
		}
		foreach($_POST['idmodule'] as $key=>$value){
			$data['idmodule'] = $value;
			$data['d'] = number_format($_POST['d'][$key],0,"","");
			$data['e'] = number_format($_POST['e'][$key],0,"","");
			$data['v'] = number_format($_POST['v'][$key],0,"","");
			$data['i'] = number_format($_POST['i'][$key],0,"","");
			if(!$this->db->insert("mast_usermodule",$data)){
				$ret['error'] = true;
				$ret['txt'] = "Terjadi Kesalahan";
				exit;
			}
		}
		echo json_encode($ret);
	}

	function sendmailppid(){
		error_reporting(0);
		$ret['error'] = false;
		$ret['text'] = "Data Terkirim";
		$this->db->query("update section_calonseller set balasan='".$this->input->post('balasan')."' , waktubalas='".date('Y-m-d h:i:s')."' where id='".$this->input->post('id_edit')."'");
		$this->auth_mod->kirimmail($this->input->post('registerreg'),'Tanggapan PPID DISPORAPAR','Salam <b>'.$user->registernama.'</b><br>'.$this->input->post('balasan').' ');


		echo json_encode($ret);

	}

	function saveManuser(){
		$arrnot 	= array("","id_edit","userpass"); # inputan yang tidak ada di table
		$keyedit 	= array("","id_edit"); # index untuk keperluan edit data
		$keyindex 	= array("","iduser");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		# custom value
		if($this->input->post('id_edit')!=''){
			$rs = $this->db->query("select * from mast_user where iduser='".$this->input->post('id_edit')."'");
			$item = $rs->row();
			if($item->userpass!=$this->input->post('userpass')){
				$data['userpass'] = md5($this->input->post('userpass'));
				$data['userpass_ori'] = $this->input->post('userpass');
			}
		}else{
			$data['userpass'] = md5($this->input->post('userpass'));
			$data['userpass_ori'] = $this->input->post('userpass');
		}
		$this->auth_mod->save("mast_user",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data);
	}

	function editManuser(){
		#foreach($_POST as $key=>$value){
		#	$data[$key] = $value;
		#}
		$rs = $this->db->query("select * from mast_user a left join mast_bidang b on a.idbidang=b.idbidang where iduser='".$this->input->post('iduser')."'");
		echo json_encode($rs->row());
	}

	function delManuser(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("mast_user",$data)){
				$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->auth_mod->del_log("mast_user",$data,$this->role[$this->modules]['d']);
		}else{
			$this->auth_mod->del_log("mast_user",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	function saveSortBidang(){
            $arr = explode(",",$_POST['bannerOrder']);
            $min = min($arr);
            foreach($arr as $key=>$value){
                $arr2 = explode("-",$value);
                $this->db->query("update mast_bidang set menu_order='".($key+$min)."' where idbidang='".$arr2[1]."'");
            }
        }


	function saveBidang(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","idbidang");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();


		$this->auth_mod->save("mast_bidang",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}


	function editManbidang(){
		$data['idbidang'] = $this->input->post('idbidang');
		$ret = $this->db->get_where('mast_bidang', $data)->row();
		echo json_encode($ret);
	}

	function delManbidang(){
		$data['idbidang'] = $this->input->post('idbidang');
		if(!$this->db->delete("mast_bidang",$data)){
			$ret['err']		= true;
			$ret['text']	= "Terjadi Kesalahan";
		}else{
			$ret['err']		= false;
			$ret['text']	= "Data Terhapus";
		}

		echo json_encode($ret);
	}

	//========== new ===========//




	function posting(){
		$error = false;
		$ret['text'] = "Data Pendaftaran berhasil diposting..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		#yang di update
		$dataflag['logkonfirm'] = date('Y-m-d h:i:s');

		$dataflag['username_konfir'] = $this->session->userdata('username');
		$dataflag['is_posting'] = 1;
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					#syarat Update
					$syarat['no_rm'] = $data['no_rm'];
					#$databonus['tgl'] = $data['tgl'];
					$syarat['is_posting'] = 0;
					$this->db->update("ngi_pasien_online",$dataflag,$syarat);
					/*$this->db->query("update ngi_daily_statemen set is_posting=1 , logterahir=NOW() where
					member_id='".$data['member_id']."' and tgl<date(NOW()) and is_posting=0 and (jumlah-jumlah_potongan>0)");*/

				}
			}
		}
		echo json_encode($ret);
	}

	function unposting(){
		$error = false;
		$ret['text'] = "Data Pendaftaran berhasil dikembalikan..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		$dataflag['logkonfirm'] = date('Y-m-d h:i:s');
		$dataflag['is_posting'] = 0;
		$dataflag['username_konfir'] = $this->session->userdata('username');


		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					$datadel2['no_rm'] = $data['no_rm'];
					//$datadel2['tgl'] = $data['tgl'];
					//$datadel2['logterahir'] = date('Y-m-d h:i:s');
					$datadel2['is_posting'] = 1;
					$this->db->update("ngi_pasien_online",$dataflag,$datadel2);
				}
			}
		}

		echo json_encode($ret);
	}


	function cekDATA2(){
		foreach($_POST as $key=>$value){
			if($key!='tablename') $data[$key] = $value;
		}
		$rs = $this->db->get_where($_POST['tablename'],$data);
		echo $rs->num_rows();
	}

	function cekDATA(){
	$datanya = $_POST['datanya'];
	$field=$_POST['fieldnya'];
	$table=$_POST['tablenya'];

	$rs = $this->db->query("select * from $table where $field =\"".$datanya."\"");
	if($rs->num_rows() > 0){
		$data = "0";
	}
	else if($datanya==""){
	$data = "0";
	}
	else{
		$data = "1";
	}

	echo $data;
	}


	function saveId(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","kode_toko");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$this->db->delete("identitas");
		$this->auth_mod->save("identitas",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}

	function dataId(){
		$rs = $this->db->get("identitas");
		echo json_encode($rs->row());

	}

	function saveJenis(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();

		$this->auth_mod->save("tb_jenis_pasien",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),array(),$keynum);
	}

	function editJenis(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "tb_jenis_pasien";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}

	function delJenis(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("tb_jenis_pasien",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->auth_mod->del_log("tb_jenis_pasien",$data,$this->role[$this->modules]['d']);
		}else{
			$this->auth_mod->del_log("tb_jenis_pasien",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}



	# todo save satuan
	function saveSatuan(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();

		$this->auth_mod->save("m_satuan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}

	function editSatuan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "m_satuan";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}

	function delSatuan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("m_satuan",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->auth_mod->del_log("m_satuan",$data,$this->role[$this->modules]['d']);
		}else{
			$this->auth_mod->del_log("m_satuan",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}

	# todo save barang
	function saveBarang(){
		$arrnot 	= array("","id_edit","prosen");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("","harga_beli","harga_jual","stok","stok_limit","rak","point_bv","ngi_harga_jual_non","ngi_harga_jual_stokis","ngi_harga_jual_m_stokis");
		$keyout		= array();

		$this->auth_mod->save("barang",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}

	function editBarang(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data["a.".$key] =  $value;
		}
		$rs = $this->db->query("select d.id as idjenis,d.jenis,a.*,b.rak,b.id as idrak,c.satuan,c.id as idsatuan from barang a
		left join m_rak b on a.idrak=b.id
		left join m_satuan c on a.idsatuan=c.id
		left join m_jenis d on a.idjenis=d.id
		where a.id='".$this->input->post('id')."'
		");
		echo json_encode($rs->row());
	}

	function savePaket(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","pack_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("","pack_bonus","pack_point","pack_limit_tf");
		$keyout		= array();

		$this->auth_mod->save("ngi_package",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}

	function editPaket(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data["a.".$key] =  $value;
		}
		$rs = $this->db->query("select * from ngi_package a left join barang b on a.idbarang=b.id
		where a.pack_id='".$this->input->post('id')."'
		");
		echo json_encode($rs->row());
	}

	function delPaket(){
		$ret['text'] = "Data Berhasil Di Hapus";
		$rs=$this->db->query("update ngi_package set pack_isactive='0' where pack_id='".$this->input->post('id')."'");
		echo json_encode($ret);
	}


	function delBarang(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("barang",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("barang",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
				$foto_barang = explode(".",$item->foto_barang);
				$filepath = BASEPATH."../upliddir/".$item->foto_barang;
				$filepath2 = BASEPATH."../upliddir/".$foto_barang[0]."_thumb.".$foto_barang[1];
				if(file_exists($filepath)) unlink($filepath);
				if(file_exists($filepath2)) unlink($filepath2);
			}
			$this->auth_mod->del_log("barang",$data,$this->role[$this->modules]['d']);
		}else{
			$this->auth_mod->del_log("barang",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}

	function delPhoto(){

		$files = explode("|",$this->input->post('filename'));
		$filename = BASEPATH.'../upliddir/'.$files[0];
		$filename1 = BASEPATH.'../upliddir/'.$files[1];
		$filename2 = BASEPATH.'../upliddir/'.$files[2];

		if(file_exists($filename)) unlink($filename);
		if(file_exists($filename1)) unlink($filename1);
		if(file_exists($filename2)) unlink($filename2);
	}

	# todo save supplier
	function saveSupplier(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("");
		$keyout		= array();

		$this->auth_mod->save("supplier",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}

	function editSupplier(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("supplier",$data);
		echo json_encode($rs->row());
	}

	function delSupplier(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("supplier",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("supplier",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->auth_mod->del_log("supplier",$data,$this->role[$this->modules]['d']);
		}else{
			$this->auth_mod->del_log("supplier",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}

	# todo save pelanggan
	function savePelanggan(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("");
		$keyout		= array();

		$this->auth_mod->save("pelanggan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}

	function editPelanggan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("pelanggan",$data);
		echo json_encode($rs->row());
	}

	function delPelanggan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("pelanggan",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("pelanggan",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->auth_mod->del_log("pelanggan",$data,$this->role[$this->modules]['d']);
		}else{
			$this->auth_mod->del_log("pelanggan",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}


	###########################################
	function saveMansize(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->auth_mod->save("section_size",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}

	function editMansize(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("section_size",$data);
		echo json_encode($rs->row());
	}

	function delMansize(){
		$ret['text'] = 'Item terhapus';
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("section_size",$data)){
			$ret['text'] = "Terjadi kesalahan";
		}else{
			$ret['text'] = "Item terhapus";
		}
		echo json_encode($ret);
	}

function saveMancolor(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->auth_mod->save("section_color",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}

	function editMancolor(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("section_color",$data);
		echo json_encode($rs->row());
	}

	function delMancolor(){
		$ret['text'] = 'Item terhapus';
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("section_color",$data)){
			$ret['text'] = "Terjadi kesalahan";
		}else{
			$ret['text'] = "Item terhapus";
		}
		echo json_encode($ret);
	}






	function saveManbrand(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->auth_mod->save("section_brand",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}

	function editManbrand(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("section_brand",$data);
		echo json_encode($rs->row());
	}

	function delManbrand(){
		$ret['text'] = 'Item terhapus';
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("section_brand",$data)){
			$ret['text'] = "Terjadi kesalahan";
		}else{
			$ret['text'] = "Item terhapus";
		}
		echo json_encode($ret);
	}


	function saveKategori(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();

		$this->auth_mod->save("section_category",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}


	function editKategori(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("section_category",$data);
		echo json_encode($rs->row());
	}

	function delKategori(){
		$ret['text'] = 'Item terhapus';
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("section_category",$data)){
			$ret['text'] = "Terjadi kesalahan";
		}else{
			$ret['text'] = "Item terhapus";
		}
		echo json_encode($ret);
	}
	function saveEventVideo(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","video_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['idbidang'] = $this->session->userdata('idbidang');

		$this->auth_mod->save("section_events_video",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}

	function editEventVideo(){
		$data['video_id'] = $this->input->post('video_id');
		$ret = $this->db->get_where('section_events_video', $data)->row();
		echo json_encode($ret);
	}

	function delEventVideo(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_events_video",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}

	function saveMenu(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();


		$this->auth_mod->save("menu",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}


	function editManmenu(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('menu', $data)->row();
		echo json_encode($ret);
	}

	function delManmenu(){
		$data['id'] = $this->input->post('id');
		$dt['sub_id'] = $this->input->post('id');
		if(!$this->db->delete("menu",$data)){
			$ret['err']		= true;
			$ret['text']	= "Terjadi Kesalahan";
		}else{
			$this->db->delete("dokumen", $dt);
			$ret['err']		= false;
			$ret['text']	= "Data Tersimpan";
		}

		echo json_encode($ret);
	}

	function saveLowongankerja(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ngi_job',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ngi_job',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editmanlowongankerja(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ngi_job', $data)->row();
		echo json_encode($ret);
	}

	function delmanlowongankerja(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ngi_job",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	function saveKategorilowongan(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ngi_jobcategory',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ngi_jobcategory',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editmankategorilowongan(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ngi_jobcategory', $data)->row();
		echo json_encode($ret);
	}

	function delmankategorilowongan(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ngi_jobcategory",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}


	function saveEvent(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('mast_event',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('mast_lowongankerja',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editmanevent(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('mast_event', $data)->row();
		echo json_encode($ret);
	}

	function delmanevent(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("mast_event",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

	function saveJenisusaha(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('mast_jenisusaha',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('mast_lowongankerja',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editjenisusaha(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('mast_jenisusaha', $data)->row();
		echo json_encode($ret);
	}

	function deljenisusaha(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("mast_jenisusaha",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}



	function savePasar(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ref_pasar_sub',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_pasar_sub',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editPasar(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_pasar_sub', $data)->row();
		echo json_encode($ret);
	}

	function delPasar(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_pasar_sub",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}


	function savePasarModern(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			$data['bend'] = '1';
			$data['fend'] = '0';
			$data['modern'] = '1';

			if($this->db->insert('ref_pasar',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_pasar',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editPasarModern(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_pasar', $data)->row();
		echo json_encode($ret);
	}

	function delPasarModern(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_pasar",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

	function saveKantin(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ref_kantin',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_kantin',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editKantin(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_kantin', $data)->row();
		echo json_encode($ret);
	}

	function delKantin(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_kantin",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

	function getKelurahan(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_kelurahan where idkec = "'.$id.'"')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->kelurahan.'</option>';
		}

		echo $output;
	}

	function saveKel(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ref_kelurahan',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_kelurahan',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}


	function editKel(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_kelurahan', $data)->row();
		echo json_encode($ret);
	}

	function delKel(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_kelurahan",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

	function detailpelamar(){
		$q2 = $this->db->query("SELECT DATE_FORMAT(a.log,'%d %M,%Y') AS tgl_daftar,b.*,d.nama_job AS nm_job FROM ngi_jobapplied a
LEFT JOIN user_pelamar b ON a.id_pelamar=b.iduser
LEFT JOIN ngi_joblist c ON a.id_job=c.id
LEFT JOIN ngi_job d ON c.posisi=d.id WHERE a.iduser = '".$_POST['iduser']."' ")->row();


		$output = 'sdsd';
		echo $output;

	}
	function setaktiv(){
		//$table = $_POST['table'];

		$data = $this->db->query('select email, username from ngi_perusahaan where idperusahaan = "'.$_POST['id'].'"')->row();
		if($_POST['val'] == '1'){
			$ket = 'Pengajuan akun Bogor Career Center perusahaan anda <b style="color:green">DITERIMA</b> oleh admin disnaker.<br>Silahkan gunakan username <b>'.$data->username.'</b> untuk login pada portal Bogor Career Center.';
			$btn = '<a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/login">Login</a>';
		}else{
			$ket = 'Pengajuan akun Bogor Career Center perusahaan anda <b style="color:red">DITOLAK</b> oleh admin disnaker.';
			$btn = '';
		}

		if($this->db->query('update ngi_perusahaan set verified_disnaker = "'.$_POST['val'].'" where idperusahaan = "'.$_POST['id'].'"')){
			 $from_email = "noreply@bogorcareercenter.bogorkab.go.id";
	          $to_email = $data->email;

	            $pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	          <html xmlns="http://www.w3.org/1999/xhtml">
	           <head>
	            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	            <title>Demystifying Email Design</title>
	            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	          </head>
	          <body style="margin: 0; padding: 0;">
	            <table border="0" cellpadding="0" cellspacing="0" width="100%">
	             <tr width="100%">
	              <td width="0%"></td>
	              <td width="100%">
	                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
	            <tr>
	            <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
	            <img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
	            </td>
	           </tr>
	           <tr>
	            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
	           <table border="0" cellpadding="0" cellspacing="0" width="100%">
	            <tr>
	            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
	           <b >Pemberitahuan verifikasi perusahaan</b>
	          </td>
	           </tr>
	           <tr>
	            <td style="padding: 20px 0 30px 0;" align="center">
	             '.$ket.'
	            </td>
	           </tr>
	           <tr>
	            <td align="center">
	             '.$btn.'
	            </td>
	           </tr>
	           </table>
	          </td>
	           </tr>
	           <tr>
	            <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
	             <table border="0" cellpadding="0" cellspacing="0" width="100%">
	           <tr>
	           <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
	           &reg; Bogor Career Center, 2019<br/>
	          </td>
	            <td align="right">

	          </td>
	           </tr>
	          </table>
	            </td>
	           </tr>
	           </table>
	              </td>
	              <td width="0%"></td>
	             </tr>
	          </table>
	          </body>
	          </html>';

	          $this->load->library('email');
	          $config = array();
	          $config['protocol'] = 'smtp';
	          $config['mailtype'] = 'html';
	          $config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
	          $config['smtp_user'] = 'superkarir@bcc.disnaker.bogorkab.go.id';
	          $config['smtp_pass'] = 'Superkarir99';
	          $config['smtp_port'] = 465;
	          $this->email->initialize($config);
	          $this->email->set_newline("\r\n");
	          // $this->email->set_header("Content-Type", "text/html");
	          $this->email->from($from_email, 'Bogor Career Center');
	          $this->email->to($to_email);
	          $this->email->subject('Bogor Career Center Verifikasi Perusahaan');
	          $this->email->message($pesan);
	          #$result = $this->email->send();
	          $this->email->send();

			echo 'Berhasil mengeset status';
		}else{
			echo 'Gagal mengeset status';
		}
	}
	function delPerusahaan(){
		$data['idperusahaan'] = $this->input->post('idperusahaan');
		if($this->db->delete("ngi_perusahaan",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

}
