<?php

class Ajaxfile extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	
	function caribidang(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where ( bidang like '%".$keyword."%' )";
		$q = "select * from mast_bidang $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function cariperusahaan(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where ( a.nama_perusahaan like '%".$keyword."%' or a.username  like '%".$keyword."%' )";
		$q = "select a.*, count(b.id) as jml from ngi_perusahaan a LEFT JOIN  
				(SELECT id, idperusahaan FROM ngi_joblist WHERE is_aktif = 1 AND NOW() < DATE_ADD(date_end, INTERVAL 1 DAY)) b
				ON b.idperusahaan = a.idperusahaan 
				$where GROUP BY a.idperusahaan 
				HAVING jml > 0 order by a.nama_perusahaan";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function caripencaker(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where ( nama like '%".$keyword."%' or no_ktp like '%".$keyword."%')";
		$q = "SELECT * FROM user_pelamar $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	
	
}
?>