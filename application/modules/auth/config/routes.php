<?php 
$route['(auth)/manbidang'] 	= "page/load/manbidang";
$route['(auth)/manmenu'] 	= "page/load/manmenu";
$route['(auth)/manuser'] 	= "page/load/manuser";
$route['(auth)/manmodul'] 	= "page/load/manmodul";
$route['(auth)/manhakakses'] 	= "page/load/manhakakses";
$route['(auth)/mansize']	= "page/load/mansize";
$route['(auth)/manbrand']	= "page/load/manbrand";

$route['(auth)/manuptd']	= "page/load/manuptd";
$route['(auth)/manpasar']	= "page/load/manpasar";
$route['(auth)/manpasarmodern']	= "page/load/manpasarmodern";
$route['(auth)/mankantin']	= "page/load/mankantin";
$route['(auth)/mankelurahan']	= "page/load/mankelurahan";
$route['(auth)/mancolor']	= "page/load/mancolor";
$route['(auth)/mankategori']	= "page/load/mankategori";

$route['(auth)/calonseller']	= "page/load/calonseller";
$route['(auth)/balasmail']	= "page/load/balasmail";

$route['(auth)/videogallery']	= "page/load/videogallery";
$route['(auth)/inpengaduan']	= "page/load/inpengaduan";


// DISNAKER BOGOR 
$route['(auth)/manlowongankerja']			= "page/load/manlowongankerja";
$route['(auth)/manevent']					= "page/load/manevent";
$route['(auth)/manpenyedialowongan']		= "page/load/manpenyedialowongan";
$route['(auth)/mantipepenyedialowongan']	= "page/load/mantipepenyedialowongan";
$route['(auth)/manpostinglowongan']			= "page/load/manpostinglowongan";
$route['(auth)/mantrackingpelamar']			= "page/load/mantrackingpelamar";
$route['(auth)/mankelolapelamar']			= "page/load/mankelolapelamar";
$route['(auth)/lappelamar']					= "page/load/lappelamar";
$route['(auth)/laptrackevent']				= "page/load/laptrackevent";
$route['(auth)/lappelamarevent']				= "page/load/lappelamarevent";
$route['(auth)/lappenyedialowongan']		= "page/load/lappenyedialowongan";
$route['(auth)/laplowongan']				= "page/load/laplowongan";
$route['(auth)/laptracklowongan']			= "page/load/laptracklowongan";
$route['(auth)/userpelamar']				= "page/load/userpelamar";
$route['(auth)/userpenyedia_data']				= "page/load/userpenyedia_data";
$route['(auth)/mankategorilowongan']				= "page/load/mankategorilowongan";
$route['(auth)/cetakak1']				= "page/load/cetakak1";
$route['(auth)/cetakak1_depan']				= "page/load/cetakak1_depan";






