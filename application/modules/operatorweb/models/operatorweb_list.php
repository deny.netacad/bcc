<?php
class Operatorweb_list extends MY_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	function listUser($id="iduser",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it form-control\" $required >";
		$ret.="<option value=\"\">| PILIH USER</option>";
		$this->db->order_by("iduser");
		$rs = $this->db->get("mast_user");
		foreach($rs->result() as $item){
		 $isSel = (($item->iduser==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->iduser."\" $isSel >".$item->username." - ".$item->nama."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listModule($id="idmodule",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it form-control\" $required>";
		$ret.="<option value=\"\">| PILIH USER</option>";
		$this->db->order_by("idmodule");
		$rs = $this->db->get("mast_module");
		foreach($rs->result() as $item){
		 $isSel = (($item->idmodule==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idmodule."\" $isSel >".$item->module."</option>";
		}
		$ret.="</select><br>";
		return $ret;
	}
		
		
	function listJabfungum($id="idjabfungum",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| FUNGSIONAL UMUM</option>";
		$this->db->order_by("idjabfungum");
		$rs = $this->db->get("a_jabfungum");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjabfungum==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjabfungum."\" $isSel >".$item->jabfungum."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listAgama($id="idagama",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| AGAMA</option>";
		$this->db->order_by("idagama");
		$rs = $this->db->get("a_agama");
		foreach($rs->result() as $item){
		 $isSel = (($item->idagama==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idagama."\" $isSel >".$item->agama."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJenkedudupeg($id="idjenkedudupeg",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| KEDUDUKAN PEGAWAI</option>";
		$this->db->order_by("idjenkedudupeg");
		$rs = $this->db->get("a_jenkedudupeg");
		foreach($rs->result() as $item){
		 $isSel = (($item->idjenkedudupeg==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idjenkedudupeg."\" $isSel >".$item->jenkedudupeg."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listStskawin($id="idstskawin",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| STATUS MARITAL</option>";
		$this->db->order_by("idstskawin");
		$rs = $this->db->get("a_stskawin");
		foreach($rs->result() as $item){
		 $isSel = (($item->idstskawin==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idstskawin."\" $isSel >".$item->stskawin."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listGolru($id="idgolru",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| GOLONGAN RUANG</option>";
		$this->db->order_by("idgolru");
		$rs = $this->db->get("a_golruang");
		foreach($rs->result() as $item){
		 $isSel = (($item->idgolru==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idgolru."\" $isSel >".$item->golru." - ".$item->pangkat."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function listJabstruk($id="idjabstruk",$sel="",$required=""){
		$arrnot = array("","idjabstruk");
		if(count($_POST)>0){
			foreach($_POST as $key=>$value){
				if(array_search($key,$arrnot)==""){
					$data[$key]=$value;
				}
			}
		}else{
			$data['idskpd'] = '';
		}
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:auto\">";
		$ret.="<option value=\"\">| JABATAN STRUKTURAL</option>";
		$this->db->where($data);
		$this->db->order_by("idskpd");
		$rs = $this->db->get("skpd");
		foreach($rs->result() as $item){
		 $isSel = (($item->idskpd==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idskpd."\" $isSel >".$item->jab." ".$item->skpd."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	
	public function get_all($parent_id, $level, &$items){
		$rs = $this->db->query('select idskpd,skpd from skpd where idparent=\''.$parent_id.'\' ')->result();
		if ($rs){
			foreach($rs as $item){
				$items['data'][] = $item;
				$items['level'][] = $level;
				$this->get_all($item->idskpd, $level+1, $items);
			}
		}else{
			return false;
		}
	}	
	
	function listEsl($id="idesl",$sel="",$required=""){
		$ret = "<select  id=\"$id\" name=\"$id\" class=\"it\" $required style=\"width:80px\">";
		$ret.="<option value=\"\">-</option>";
		$this->db->order_by("idesl");
		$rs = $this->db->get("a_esl");
		foreach($rs->result() as $item){
		 $isSel = (($item->idesl==$sel)?"selected":"");
		 $ret.="<option value=\"".$item->idesl."\" $isSel >".$item->esl."</option>";
		}
		$ret.="</select>";
		return $ret;
	}
	
	function familytree($id=0,$x=0){
		$data['idparent'] = $id;
		$x++;
		$this->db->order_by("idskpd");
		if(strlen($id)==2){
			$rs = $this->db->query("select a.idskpd,a.skpd,a.jab,a.path,b.nip
			,concat(if(length(b.gdp)>0,concat(b.gdp,' '),''),b.nama,if(length(b.gdb)>0,concat(',',b.gdb),'')) as namalengkap 
			from skpd a 
			left join tb_01 b on a.idskpd=b.idjabjbt
			where a.idparent='".$data['idparent']."' and a.idesl<=41");
		}else{
			$rs = $this->db->query("select a.idskpd,a.skpd,a.jab,a.path,b.nip
			,concat(if(length(b.gdp)>0,concat(b.gdp,' '),''),b.nama,if(length(b.gdb)>0,concat(',',b.gdb),'')) as namalengkap
			from skpd a 
			left join tb_01 b on a.idskpd=b.idjabjbt
			where a.idparent='".$data['idparent']."'");
		}
		foreach($rs->result() as $item){
			$data2['idparent'] = $item->idskpd;
			$rs2 = $this->db->get_where("skpd",$data2);
			$urlphoto = base_url()."assets/images/nophoto.jpg";
			$photo = "<i style=\"margin:0 auto;margin-top:5px;border:1px solid #CCCCCC;background:url(".$urlphoto.") 
			no-repeat center center;background-size:100% 100%;width:70px;height:80px;display:block;border-radius:5px;\"></i>";
			$photo_ = "";
			$div1 = "<div class=\"sotk-nama\">".$item->namalengkap."</div>";
			$div2 = "<div class=\"sotk-nip\">".$item->nip."</div>";
			if($rs2->num_rows()>0){
				echo "<li class=\"up-".$item->idesl."\"><a><span class=\"sotk-title\">".$item->skpd."</span>".$photo.$div2.$div1."</a>";
				echo "\n<ul>";
				$this->familytree($item->idskpd,$x);
				echo "</ul>\n";
			}else{
				echo "<li><a><span class=\"sotk-title\">".$item->skpd.$item->idesl."</span>".$photo.$div2.$div1."</a>";
			}
			echo "</li>\n";
		}
   }	
	
}