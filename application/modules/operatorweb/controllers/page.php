<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $modules = "operatorweb";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('operatorweb/user');
		$this->load->model('operatorweb/operatorweb_list');
		
	}

	function index(){
		if ($this->session->userdata('is_login_portal_disnakerbogor'))
		{
			if($this->cekRole()==false){
				redirect('auth');
			}else{
				$this->load->view($this->modules.'/main',$this->data);
			}
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}
	
	//========== new ===========//
	function saveGantipass(){
		
		$data['iduser'] = $this->session->userdata('user_id');
		$data['userpass'] = md5($this->input->post('userpass'));
		$rs = $this->db->get_where("mast_user",$data);
		if($rs->num_rows()>0){
			if($this->input->post('userpass2')==$this->input->post('userpass3')){
				$dt['userpass'] = md5($this->input->post('userpass2'));
				if(!$this->db->update("mast_user",$dt,$data)){
					$ret['text'] = "Terjadi Kesalahan\n".$this->db->_error_message();
				}else{
					$ret['text'] = "Password telah diganti";
				}
			}else{
				$ret['text'] = "Password Perulangan salah";
			}
		}else{
			$ret['text'] = "Password lama salah";
		}
		echo json_encode($ret);
	}
	
	function saveManmodul(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","idmodule");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->operatorweb_mod->save("mast_module",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}
	
	function editManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_module",$data);
		echo json_encode($rs->row());
	}
	
	function delManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("mast_module",$data)){
			echo "Terjadi Kesalahan";
		}else{
			echo "Data Terhapus";
		}
	}
	
	function saveManhakakses(){
		$ret['error'] = false;
		$ret['txt'] = "Data Tersimpan";
		$arrnot = array("");
		
		$data['iduser'] = $this->input->post('iduser');
		$this->db->delete("mast_usermodule",$data);
		$this->db->delete("mast_default_module",$data);
		$data2['iduser'] = $data['iduser'];
		$data2['idmodule'] = $this->input->post('iddefault');
		if(!$this->db->insert("mast_default_module",$data2)){
			$ret['error'] = true;
			$ret['txt'] = "Terjadi Kesalahan";
			exit;
		}
		foreach($_POST['idmodule'] as $key=>$value){
			$data['idmodule'] = $value;
			$data['d'] = number_format($_POST['d'][$key],0,"","");
			$data['e'] = number_format($_POST['e'][$key],0,"","");
			$data['v'] = number_format($_POST['v'][$key],0,"","");
			$data['i'] = number_format($_POST['i'][$key],0,"","");
			if(!$this->db->insert("mast_usermodule",$data)){
				$ret['error'] = true;
				$ret['txt'] = "Terjadi Kesalahan";
				exit;
			}
		}
		echo json_encode($ret);
	}
	
	
	function editManuser(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_user",$data);
		echo json_encode($rs->row());
	}
	
	function saveManuser(){
		$arrnot 	= array("","id_edit","userpass"); # inputan yang tidak ada di table
		$keyedit 	= array("","id_edit"); # index untuk keperluan edit data
		$keyindex 	= array("","iduser"); 
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		# custom value
		$data['userpass_ori'] = $this->input->post('userpass');
		$data['userpass'] = md5($this->input->post('userpass'));
		$this->operatorweb_mod->save("mast_user",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data);
	}
	
	function delManuser(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("mast_user",$data)){
				$ret['text'] = "Terjadi Kesalahan".$this->db->_error_message();
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operatorweb_mod->del_log("mast_user",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("mast_user",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	//========== new ===========//
	
	
	
	
	function savePasienbaru(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keynum		= array("");
		$keyin		= array();
		$keyout		= array();
		
		$data['log_daftar'] = date('Y-m-d h:i:s');
		$data['id_user'] = $this->session->userdata('username');
		
		$this->operatorweb_mod->save("tb_pasien",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,array(),$keynum);
		
		
	}
	
	function posting(){
		$error = false;
		$ret['text'] = "Data Pendaftaran berhasil diposting..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		#yang di update
		$dataflag['logkonfirm'] = date('Y-m-d h:i:s');

		$dataflag['username_konfir'] = $this->session->userdata('username');				
		$dataflag['is_posting'] = 1;
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					#syarat Update
					$syarat['no_rm'] = $data['no_rm'];
					#$databonus['tgl'] = $data['tgl'];
					$syarat['is_posting'] = 0;
					$this->db->update("ngi_pasien_online",$dataflag,$syarat);
					/*$this->db->query("update ngi_daily_statemen set is_posting=1 , logterahir=NOW() where
					member_id='".$data['member_id']."' and tgl<date(NOW()) and is_posting=0 and (jumlah-jumlah_potongan>0)");*/
					
				}
			}
		}
		echo json_encode($ret);
	}
	
	function unposting(){
		$error = false;
		$ret['text'] = "Data Pendaftaran berhasil dikembalikan..";
		$keynot = array("","submit","checkall");
		$keynot2 = array("","checkthis");
		$keydate = array("");
		$keyin = array("");
		$keyout = array("");
		$dataflag['logkonfirm'] = date('Y-m-d h:i:s');			
		$dataflag['is_posting'] = 0;
		$dataflag['username_konfir'] = $this->session->userdata('username');				
		
		
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				if($_POST[$key]['checkthis']==1){
					foreach($value as $key2=>$value2){
						if(array_search($key2,$keyin)!=""){
							$keys =  array_keys($keyin,$key2);
							$key2 = $keyout[$keys[0]];
						}
						if(array_search($key2,$keynot)==''){
							$data[$key2] = $value2;
						}
					}
					$datadel2['no_rm'] = $data['no_rm'];
					//$datadel2['tgl'] = $data['tgl'];
					//$datadel2['logterahir'] = date('Y-m-d h:i:s');
					$datadel2['is_posting'] = 1;
					$this->db->update("ngi_pasien_online",$dataflag,$datadel2);
				}
			}
		}
		
		echo json_encode($ret);
	}


	function saveDetailanggaran(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keynum		= array("","nilai");
		$keyin		= array();
		$keyout		= array();
		$this->operatorweb_mod->save("ngi_detail_anggaran",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	function delDetailanggaran(){
		$ret['err']		= false;
		$ret['text']	= "Berhasil Dihapus";
		$q = "update ngi_detail_anggaran set flag='1' where id='".$this->input->post('id')."'";
		$rs = $this->db->query($q);
		echo json_encode($ret);
	}
	
	
	
	function delmobilpelanggan(){
		$ret['err']		= false;
		$ret['text']	= "Berhasil Dihapus";
		$q = "update mobilpelanggan set flag='1' where id='".$this->input->post('id')."'";
		$rs = $this->db->query($q);
		echo json_encode($ret);
	}
	
		
	function saveMobil(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->operatorweb_mod->save("mobilpelanggan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}
	
	
	
	
	
	function cekDATA2(){
		foreach($_POST as $key=>$value){
			if($key!='tablename') $data[$key] = $value;
		}
		$rs = $this->db->get_where($_POST['tablename'],$data);
		echo $rs->num_rows();
	}
	
	function cekDATA(){
	$datanya = $_POST['datanya'];
	$field=$_POST['fieldnya'];
	$table=$_POST['tablenya'];
	
	$rs = $this->db->query("select * from $table where $field =\"".$datanya."\"");
	if($rs->num_rows() > 0){
		$data = "0";
	}	
	else if($datanya==""){
	$data = "0";
	}
	else{
		$data = "1";
	}
	
	echo $data;
	}
	
	
	function saveId(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","kode_toko");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$this->db->delete("identitas");
		$this->operatorweb_mod->save("identitas",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function dataId(){
		$rs = $this->db->get("identitas");
		echo json_encode($rs->row());
		
	}
	
	# todo save rak
	function saveDokter(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->operatorweb_mod->save("tb_dokter",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),array(),$keynum);
	}
	
	function editDokter(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "tb_dokter";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}
	
	function delDokter(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("tb_dokter",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operatorweb_mod->del_log("tb_dokter",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("tb_dokter",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	#jenis
	
	
	function editPasien(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "tb_pasien";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}
	
	function delPasien(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("tb_pasien",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operatorweb_mod->del_log("tb_pasien",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("tb_pasien",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	function saveJenis(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->operatorweb_mod->save("tb_jenis_pasien",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),array(),$keynum);
	}
	
	function editJenis(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "tb_jenis_pasien";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}
	
	function delJenis(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("tb_jenis_pasien",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operatorweb_mod->del_log("tb_jenis_pasien",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("tb_jenis_pasien",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	
	
	# todo save satuan
	function saveSatuan(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->operatorweb_mod->save("m_satuan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editSatuan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$tablename = "m_satuan";
		$rs = $this->db->get_where($tablename,$data);
		echo json_encode($rs->row());
	}
	
	function delSatuan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("m_satuan",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operatorweb_mod->del_log("m_satuan",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("m_satuan",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	# todo save barang
	function saveBarang(){
		$arrnot 	= array("","id_edit","prosen");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("","harga_beli","harga_jual","stok","stok_limit","rak","point_bv","ngi_harga_jual_non","ngi_harga_jual_stokis","ngi_harga_jual_m_stokis");
		$keyout		= array();
		
		$this->operatorweb_mod->save("barang",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editBarang(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data["a.".$key] =  $value;
		}
		$rs = $this->db->query("select d.id as idjenis,d.jenis,a.*,b.rak,b.id as idrak,c.satuan,c.id as idsatuan from barang a 
		left join m_rak b on a.idrak=b.id
		left join m_satuan c on a.idsatuan=c.id
		left join m_jenis d on a.idjenis=d.id
		where a.id='".$this->input->post('id')."'
		");
		echo json_encode($rs->row());
	}
	
	function savePaket(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","pack_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("","pack_bonus","pack_point","pack_limit_tf");
		$keyout		= array();
		
		$this->operatorweb_mod->save("ngi_package",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editPaket(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data["a.".$key] =  $value;
		}
		$rs = $this->db->query("select * from ngi_package a left join barang b on a.idbarang=b.id
		where a.pack_id='".$this->input->post('id')."'
		");
		echo json_encode($rs->row());
	}
	
	function delPaket(){
		$ret['text'] = "Data Berhasil Di Hapus";
		$rs=$this->db->query("update ngi_package set pack_isactive='0' where pack_id='".$this->input->post('id')."'");
		echo json_encode($ret);
	}
	
	
	function delBarang(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("barang",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("barang",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
				$foto_barang = explode(".",$item->foto_barang);
				$filepath = BASEPATH."../upliddir/".$item->foto_barang;
				$filepath2 = BASEPATH."../upliddir/".$foto_barang[0]."_thumb.".$foto_barang[1];
				if(file_exists($filepath)) unlink($filepath);
				if(file_exists($filepath2)) unlink($filepath2);
			}
			$this->operatorweb_mod->del_log("barang",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("barang",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	function delPhoto(){
		
		$files = explode("|",$this->input->post('filename'));
		$filename = BASEPATH.'../upliddir/'.$files[0];
		$filename1 = BASEPATH.'../upliddir/'.$files[1];
		$filename2 = BASEPATH.'../upliddir/'.$files[2];
		
		if(file_exists($filename)) unlink($filename);
		if(file_exists($filename1)) unlink($filename1);
		if(file_exists($filename2)) unlink($filename2);
	}
	
	# todo save supplier
	function saveSupplier(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("");
		$keyout		= array();
		
		$this->operatorweb_mod->save("supplier",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editSupplier(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("supplier",$data);
		echo json_encode($rs->row());
	}
	
	function delSupplier(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("supplier",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("supplier",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operatorweb_mod->del_log("supplier",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("supplier",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
	
	# todo save pelanggan
	function savePelanggan(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array("");
		$keyout		= array();
		
		$this->operatorweb_mod->save("pelanggan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,array(),array(),$keynum);
	}
	
	function editPelanggan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("pelanggan",$data);
		echo json_encode($rs->row());
	}
	
	function delPelanggan(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data[$key] =  $value;
		}
		$rs = $this->db->get_where("pelanggan",$data);
		$item = $rs->row();
		if($this->role[$this->modules]['d']==1){
			if(!$this->db->delete("pelanggan",$data)){
				$ret['text'] = "Terjadi Kesalahan";
			}else{
				$ret['text'] = "Item telah dihapus";
			}
			$this->operatorweb_mod->del_log("pelanggan",$data,$this->role[$this->modules]['d']);
		}else{
			$this->operatorweb_mod->del_log("pelanggan",$data,$this->role[$this->modules]['d']);
			$ret['text'] = "Akses ditolak";
		}
		echo json_encode($ret);
	}
		function saveMenu(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		
		$this->operatorweb_mod->save("menu",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	

	function editManmenu(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('menu', $data)->row();
		echo json_encode($ret);
	}

	function delManmenu(){
		$data['id'] = $this->input->post('id');
		$dt['menu_id'] = $this->input->post('id');
		if(!$this->db->delete("menu",$data)){
			$ret['err']		= true;
			$ret['text']	= "Terjadi Kesalahan";
		}else{
			//$this->db->delete("dokumen", $dt);
			$ret['err']		= false;
			$ret['text']	= "Data Terhapus";
		}

		echo json_encode($ret);
	}
	
	function saveSortMenu(){
            $arr = explode(",",$_POST['bannerOrder']);
            $min = min($arr);
            foreach($arr as $key=>$value){
                $arr2 = explode("-",$value);
                $this->db->query("update menu set menu_order='".($key+$min)."' where id='".$arr2[1]."'");
            }
        }
	
	function saveSortSlide(){
            $arr = explode(",",$_POST['bannerOrder']);
            $min = min($arr);
            foreach($arr as $key=>$value){
                $arr2 = explode("-",$value);
                $this->db->query("update section_slide set menu_order='".($key+$min)."' where slide_id='".$arr2[1]."'");
            }
        }
	
	function saveArticle(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","art_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['idbidang'] = $this->session->userdata('idbidang');
		$data['art_seo'] = url_title($this->input->post('art_title'));
		$this->operatorweb_mod->save("section_article",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editArticle(){
		$data['art_id'] = $this->input->post('art_id');
		$ret = $this->db->get_where('section_article', $data)->row();
		echo json_encode($ret);
	}
	
	function delArticle(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_article",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	
	function savePejabat(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id_pejabat");
		$keydate	= array("","art_date");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$this->operatorweb_mod->save("section_pejabat",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editPejabat(){
		$data['id_pejabat'] = $this->input->post('id_pejabat');
		$ret = $this->db->query("select * from section_pejabat a left join mast_bidang b on a.idbidang=b.idbidang where a.id_pejabat='".$data['id_pejabat']."'")->row();
		echo json_encode($ret);
	}
	
	function delPejabat(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_pejabat",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	
	function saveEventVideo(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","video_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['idbidang'] = $this->session->userdata('idbidang');
		
		$this->operatorweb_mod->save("section_events_video",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editEventVideo(){
		$data['video_id'] = $this->input->post('video_id');
		$ret = $this->db->get_where('section_events_video', $data)->row();
		echo json_encode($ret);
	}
	
	function delEventVideo(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_events_video",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	
	
	function loadAboutus(){
		$rs = $this->db->query(" select * from section_about_us where flag=1 order by doc_id desc limit 1");
		echo json_encode($rs->row());
	}
	
	function saveAboutus(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","doc_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$this->operatorweb_mod->save("section_about_us",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	
	
	
	function saveEvent(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","event_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');

		$data['idbidang'] = $this->session->userdata('idbidang');
		$this->operatorweb_mod->save("section_events",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);

	}
	
	function editEvent(){
		$data['event_id'] = $this->input->post('event_id');
		$ret = $this->db->get_where('section_events', $data)->row();
		echo json_encode($ret);
	}
	
	function delEvent(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_events",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	function saveManslide(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","slide_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('user_id');
		$this->operatorweb_mod->save("section_slide",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editManslide(){
		$data['slide_id'] = $this->input->post('slide_id');
		$ret = $this->db->get_where('section_slide', $data)->row();
		echo json_encode($ret);
	}
	
	function delManslide(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_slide",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	
	
	function saveMkepokmas(){
		$arrnot 	= array("","id_edit","ci_csrf_token","menu_id");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->operatorweb_mod->save("section_kepokmas",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	

	function editMkepokmas(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("section_kepokmas",$data);
		echo json_encode($rs->row());
	}
	
	function delMkepokmas(){
		$ret['text'] = 'Item terhapus';
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("section_kepokmas",$data)){
			$ret['text'] = "Terjadi kesalahan";
		}else{
			$ret['text'] = "Item terhapus";
		}
		echo json_encode($ret);
	}
	
	function saveKontak(){
		$this->db->query("truncate section_contact");
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","doc_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$this->operatorweb_mod->save("section_contact",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
		//echo $this->db->last_query();
		//$this->db->query("update section_contact set addr=".$this->input->post('addr')." ,iduser='".$this->session->userdata('iduser')."' ,bus_hour='".$this->input->post('bus_hour')."',maps='".$this->input->post('maps')."'");
		//$ret['text'] = 'Item Terupdate';
		//$ret['sql'] = "update from section_contact set addr='".$this->input->post('addr')."' ,iduser='".$this->session->userdata('iduser')."' ,bus_hour='".$this->input->post('bus_hour')."',maps='".$this->input->post('maps')."'";
		//echo json_encode($ret);
	}
	function saveSeo(){
		//$this->db->query("truncate section_contact");
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		//$data['iduser'] = $this->session->userdata('iduser');
		$this->operatorweb_mod->save("ref_seo",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
		//echo $this->db->last_query();
		//$this->db->query("update section_contact set addr=".$this->input->post('addr')." ,iduser='".$this->session->userdata('iduser')."' ,bus_hour='".$this->input->post('bus_hour')."',maps='".$this->input->post('maps')."'");
		//$ret['text'] = 'Item Terupdate';
		//$ret['sql'] = "update from section_contact set addr='".$this->input->post('addr')."' ,iduser='".$this->session->userdata('iduser')."' ,bus_hour='".$this->input->post('bus_hour')."',maps='".$this->input->post('maps')."'";
		//echo json_encode($ret);
	}
	
	function loadKontak(){
		$rs = $this->db->query("select * from section_contact order by doc_id desc limit 1");
		echo json_encode($rs->row());
	}

	function loadSeo(){
		$rs = $this->db->query("select * from ref_seo order by id desc limit 1");
		echo json_encode($rs->row());
	}
	
	
	function saveAgenda(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","agenda_id");
		$keydate	= array("","agenda_date");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['agenda_seo'] = url_title($this->input->post('agenda_title'));
		$this->operatorweb_mod->save("section_agenda",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editAgenda(){
		$data['agenda_id'] = $this->input->post('agenda_id');
		$ret = $this->db->query("select * from section_agenda a where a.agenda_id='".$data['agenda_id']."'")->row();
		echo json_encode($ret);
	}
	
	function delAgenda(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_agenda",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	
	function saveApps(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","apps_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['apps_seo'] = url_title($this->input->post('apps_title'));
		$this->operatorweb_mod->save("section_apps",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editApps(){
		$data['apps_id'] = $this->input->post('apps_id');
		$ret = $this->db->query("select * from section_apps a where a.apps_id='".$data['apps_id']."'")->row();
		echo json_encode($ret);
	}
	
	function delApps(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_apps",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	function saveLinks(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","links_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$this->operatorweb_mod->save("section_links",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editLinks(){
		$data['links_id'] = $this->input->post('links_id');
		$ret = $this->db->query("select * from section_links a where a.links_id='".$data['links_id']."'")->row();
		echo json_encode($ret);
	}
	
	function delLinks(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_links",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	function saveDocplan(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","docplan_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['docplan_seo'] = url_title($this->input->post('docplan_title'));
		$this->operatorweb_mod->save("section_docplan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editDocplan(){
		$data['docplan_id'] = $this->input->post('docplan_id');
		$ret = $this->db->query("select * from section_docplan a where a.docplan_id='".$data['docplan_id']."'")->row();
		echo json_encode($ret);
	}
	
	function delDocplan(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_docplan",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	

function saveKepokmas(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","docplan_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$this->operatorweb_mod->save("section_dockepokmas",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editKepokmas1(){
		$data['docplan_id'] = $this->input->post('docplan_id');
		$ret = $this->db->query("select a.*,c.title as nm_bahan from section_dockepokmas a left join section_kepokmas c on a.sub_id=c.id where a.docplan_id='".$data['docplan_id']."'")->row();
		echo json_encode($ret);
	}
	function editKepokmas(){
		$data = array();
		foreach($_POST as $key=>$value){
			$data["a.".$key] =  $value;
		}
		$rs = $this->db->query("select a.*,c.title as title from section_dockepokmas a left join section_kepokmas c on a.sub_id=c.id where a.docplan_id='".$data['docplan_id']."'");
		echo json_encode($rs->row());
	}
	function delKepokmas(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_dockepokmas",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	

function saveKepokmas2(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","docplan_id");
		$keydate	= array("");
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$this->operatorweb_mod->save("section_hargakomoditas",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
		
		
	}
	
	function editKepokmas2(){
		$data['docplan_id'] = $this->input->post('docplan_id');
		$ret = $this->db->query("SELECT a.*,b.nama AS nm_pasar,c.nama AS nm_user FROM section_hargakomoditas a
LEFT JOIN ref_pasar b ON a.pasar=b.id
LEFT JOIN mast_user c ON a.iduser=c.iduser where a.docplan_id='".$data['docplan_id']."'")->row();
		echo json_encode($ret);
	}
	function delKepokmas2(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_hargakomoditas",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}

	
	function saveKontenumum(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","doc_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['title_seo'] = url_title($this->input->post('title'));
		$this->operatorweb_mod->save("dokumen",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editKontenumum(){
		$data['doc_id'] = $this->input->post('doc_id');
		//$ret = $this->db->get_where('dokumen', $data)->row();
		$ret = $this->db->query("SELECT a.*,b.* FROM dokumen a 
LEFT JOIN section_category b ON a.menu_id=b.id  where a.doc_id='".$data['doc_id']."'")->row();
		echo json_encode($ret);
	}
	
	function delKontenumum(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("dokumen",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	
	function do_uploadphoto()
	{
		$file1 = $this->input->post('username');
		$file2 = explode(".",$_FILES['files']['name']);
		$ext = $file2[count($file2)-1];
		$md5 = md5_file($_FILES['files']['tmp_name']);
		
		$filename = $file1."_".$md5.".".$ext;
		$filename_thumb = $file1."_".$md5."_thumb.".$ext;
		$config['upload_path'] = FCPATH.'/upliddir/';
		$config['allowed_types'] = 'png|jpg';
		$config['max_size']	= '5000000';
		$config['file_name'] = $filename;
		if(!file_exists($config['upload_path'].$filename)){
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('files'))
			{
				$error = array('error' => $this->upload->display_errors());

				echo json_encode($error);
			}
			else
			{
				error_reporting(E_ALL);
				
				$data_upload = $this->upload->data();
				$upload['name'] = $data_upload['file_name'];
				$upload['size'] = $data_upload['file_size'];
				$upload['type'] = $data_upload['file_type'];
				$upload['url'] 	= base_url()."upliddir/".$data_upload['file_name'];
				$upload['thumbnailUrl'] = base_url()."upliddir/".$filename_thumb;
				$upload['deleteUrl'] = base_url()."operatorweb/page/delUploaddoc/?filename=".$filename."&".$this->security->get_csrf_token_name()."=".$this->security->get_csrf_hash();
				$upload['deleteType'] = "DELETE";
				
				$imgwidth = $data_upload['image_width'];
				$imgheight = $data_upload['image_height'];
				
				$config['image_library'] = 'gd2';
				$config['source_image']	= BASEPATH."../upliddir/".$filename;
				$config['maintain_ratio'] = true;
				$config['width'] = 1200;
				$config['height'] = 600;
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				
				if ( ! $this->image_lib->resize())
				{
					echo $this->image_lib->display_errors();
				}
				
				$path_parts = pathinfo(BASEPATH."../upliddir/".$filename);
				
				list($imgnewwidth,$imgnewheight,$imgtype) = getimagesize(BASEPATH."../upliddir/".$filename);
				$config['new_image']	 = BASEPATH."../upliddir/".$path_parts['filename'].'_thumb.'.$path_parts['extension'];
				$config['image_library'] = 'gd2';
				$config['source_image']	= BASEPATH."../upliddir/".$filename;
				$config['maintain_ratio'] = true;
				if($imgnewwidth>$imgnewheight){
					$imgnewwidth = ($imgnewwidth/$imgnewheight)*300;
					$imgnewheight = 300;
				}else{
					$imgnewwidth = 300;
					$imgnewheight = ($imgnewheight/$imgnewwidth)*300;
				}
				$config['width'] = $imgnewwidth;
				$config['height'] = $imgnewheight;
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				
				if ( ! $this->image_lib->resize())
				{
					echo $this->image_lib->display_errors();
				}
				
				list($imgnewwidth,$imgnewheight,$imgtype) = getimagesize(BASEPATH."../upliddir/".$path_parts['filename'].'_thumb.'.$path_parts['extension']);
				$config['image_library'] = 'gd2';
				$config['source_image']	= BASEPATH."../upliddir/".$path_parts['filename'].'_thumb.'.$path_parts['extension'];
				$config['x_axis'] = ($imgnewwidth-300)/2;
				$config['y_axis'] = ($imgnewheight-300)/2;
				$config['maintain_ratio'] = false;
				$config['width'] = 300;
				$config['height'] = 300;
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				
				if ( ! $this->image_lib->crop())
				{
					echo $this->image_lib->display_errors();
				}

			
				$data = array('files' => array($upload));
				
				echo json_encode($data);
			}	
		}else{
			$data = array('error'=>array("text"=>"File duplicate"));
			echo json_encode($data);
		}
	}
	
	function delUploaddoc(){
		$a1 = explode(".",(($this->input->post('filename')!='')?$this->input->post('filename'):$this->input->get('filename')));
		$filename = $a1[0].".".$a1[1];
		$filename_thumb = $a1[0]."_thumb.".$a1[1];
		if(file_exists(BASEPATH."../upliddir/".$filename)) unlink(BASEPATH."../upliddir/".$filename);
		if(file_exists(BASEPATH."../upliddir/".$filename_thumb)) unlink(BASEPATH."../upliddir/".$filename_thumb);
		echo "Item Deleted..";
	}
	
	
	function saveGalleryupload(){
		$ret['text'] = "Album berhasil disimpan";
		foreach($_POST as $key=>$value){
			if(is_array($value)){
				foreach($value as $key2=>$value2){
					$data[$key2][$key] = $value2;
				}
			}
		}
		
		foreach($data as $key=>$value){
			$data2 = $value;
			$data2['album'] = $this->input->post('album');
			$data2['iduser'] = $this->session->userdata('iduser');
			if(!$this->db->insert("section_gallery",$data2)){
				$ret['text'] = "terjadi kesalahan";
			}
		}
		echo json_encode($ret);
	}
	
	function delGalleryupload(){
		$rs = $this->db->query("select * from section_gallery where album='".$this->input->post('album')."'");
		foreach($rs->result() as $item){
			if(file_exists(BASEPATH.'../upliddir/'.$item->filename)){
				$fileparts = pathinfo(BASEPATH.'../upliddir/'.$item->filename);
				$file1 = BASEPATH.'../upliddir/'.$item->filename;
				$file2 = BASEPATH.'../upliddir/'.$fileparts['filename'].'_thumb.'.$fileparts['extension'];
				unlink($file1);
				unlink($file2);
			}
		}
		$data['album'] = $this->input->post('album');
		$this->db->delete("section_gallery",$data);
	}
	
	function delGalleryuploadphoto(){
		$rs = $this->db->query("select * from section_gallery where filenameid='".$this->input->post('filenameid')."'");
		foreach($rs->result() as $item){
			if(file_exists(BASEPATH.'../upliddir/'.$item->filename)){
				$fileparts = pathinfo(BASEPATH.'../upliddir/'.$item->filename);
				$file1 = BASEPATH.'../upliddir/'.$item->filename;
				$file2 = BASEPATH.'../upliddir/'.$fileparts['filename'].'_thumb.'.$fileparts['extension'];
				unlink($file1);
				unlink($file2);
			}
		}
		$data['filenameid'] = $this->input->post('filenameid');
		$this->db->delete("section_gallery",$data);
	}
	
	/*bayuuu*/
	function detailpertanyaan(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("utin_tanya",$data);
		echo json_encode($rs->row());
	}
	function sendmailpertanyaan(){
		error_reporting(0);
		$ret['error'] = false;
		$ret['text'] = "Data Terkirim";
		$this->db->query("update utin_tanya set jawaban='".$this->input->post('jawaban')."' , waktubalas='".date('Y-m-d h:i:s')."' , publish='".$this->input->post('publish')."' where id='".$this->input->post('id_edit')."'");
		$this->operatorweb_mod->kirimmail($this->input->post('registerreg'),'TANGGAPAN JAWABAN PERTANYAAN ANDA','Salam <b>'.$user->registernama.'</b><br>'.$this->input->post('jawaban').' ');
		

		echo json_encode($ret);

	}
	function saveAgendadinas(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id_agenda");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		//$data['agenda_seo'] = url_title($this->input->post('agenda_title'));
		$this->operatorweb_mod->save("tbl_agenda",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editAgendadinas(){
		$data['id_agenda'] = $this->input->post('id_agenda');
		$ret = $this->db->query("SELECT a.*,b.nama AS USER FROM tbl_agenda a 
	LEFT JOIN mast_user b ON a.iduser=b.iduser where a.id_agenda='".$data['id_agenda']."'")->row();
		echo json_encode($ret);
	}
	
	function delAgendadinas(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("tbl_agenda",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
}