<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.links_title like '%".$cari."%' 
	 or b.username like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select * from section_links a 
	left join mast_user b on a.iduser=b.iduser 
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operatorweb/page/data/links';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.username
		FROM section_links a
		left join mast_user b on a.iduser=b.iduser 
		 $where
		ORDER BY a.links_id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>JUDUL LINKS</th>
        <th>TYPE LINK</th>
        <th>THUMBNAIL</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
	if($item->links_thumb!=''){
		$xpath = new DOMXPath(@DOMDocument::loadHTML($item->links_thumb));
		$src = $xpath->evaluate("string(//img/@src)");	
	}
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><a href="<?=$item->links_target?>" target="_blank"><?=$item->links_title?></a></div>
        </td>
        <td>
            <div class="text-left"><?=(($item->links_type==0)?"Text":"Image")?></div>
        </td>
        <td>
            <div class="text-left">
			<?php
			if($item->links_thumb!=''){
			?>
			<img src="<?=$src?>" />
			<?php
			}
			?>
			</div>
        </td>
		<td>
            <div class="text-center"><?=$item->username?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->links_id?>"><i  class=" text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->links_id?>"><i class=" text-red fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>