<script>
$(document).ready(function(){

    $('#thn').datepicker({
		format:'yyyy',
		autoclose:true,
		viewMode: 2,
		minViewMode: 2
	}).on('changeDate',function(){
		refreshData();
	});
	$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					refreshData();
				break;
				case 1:
					
				break;
			}
	});
	
	refreshData();
	
    
});

function refreshData(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>auth/page/data/default',
		data:{ 'thn':$('#thn').val() },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});
}

</script>
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#statistik" data-toggle="tab"><i class="icon-pencil"></i> Statistik</a></li>
</ul>
<div class="tab-content" style="overflow:visible">
	<div class="tab-pane active" id="statistik">
		<input type="text" class="form-control"id="thn" name="thn" maxlength="4" style="width:50px" placeHolder="Tahun" value="<?=date('Y')?>" />
		<div id="result">
        <div class="row-fluid">
            <div align="center">
                <h4>DATA STATISTIK</h4>
            </div>
        </div>
       <div id="graph1" style="width:95%"></div>
		
		</div>
		</div>
	</div>
</div>