<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}
	function kepokmasFormatResult(data) {
        var markup= "<li>"+ data.title+"</li>";
        return markup;
    }
	
    function kepokmasFormatSelection(data) {
        return data.title;
    }
    $(document).ready(function(){
		
		var config_image = {
            toolbar : 'Image',
            height: '200',
            width: '100%'
        };
		
		var config_pengantar = {
            toolbar : 'MyToolbar',
            height: '350'
        };

		$('#ppid_tgl').datepicker({
			format:'yyyy-mm-dd',
			autoclose:true
		});

		$('#agenda_date').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		});
		
        $('#docplan_detail').ckeditor(config_pengantar);
		$('#art_thumb').ckeditor(config_image);
		
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup(function(){
			refreshData();
		});
		
		$("#sub_id").select2({
			id: function(e) { return e.id }, 
			placeholder: "Cari Bahan",
			minimumInputLength: 0,
			multiple: false,
			allowClear:true,
			ajax: { 
				url: "<?=base_url()?>operatorweb/ajaxfile/carimasterkepokmas",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 22, // page size
						page: page, // page number
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					};
				},
				results: function (data, page) { 
					var more = (page * 22) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: kepokmasFormatResult, 
			formatSelection: kepokmasFormatSelection
		});
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        $('#form1').trigger('reset');
                    });
            }else{
                $('#form1').trigger('reset');
            }
			$('#id_edit').val('');
			$('#sub_id').select2('data',null);
        });
		
	
        /* untuk reset */
     $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			CKupdate();
			$('#id_edit').val('');
			$('#sub_id').select2('data',null);
        });
		

		

		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		
		
		
		 $(document).on('click','a[title=Edit]', function(){
			
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>operatorweb/page/editKepokmas2',
				type: 'post',
				data: { 
					'docplan_id':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					for(attrname in ret){
						if(attrname=='docplan_id'){
							$('#id_edit').val(ret[attrname]);
						}else{
							$('#'+attrname).val(ret[attrname]);
						}
					}
					//$('#sub_id').select2('data',{'id':ret.sub_id,'name':ret.title});
					
				}
			});
        });
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>operatorweb/page/delKepokmas2',
					type: 'post',
					data: { 
						'docplan_id':$this.eq(index).attr('data')
						,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
	
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#docplan_title').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
		function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>operatorweb/page/data/kepokmas',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }
    });
    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
		  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Manajemen Harga Komoditas</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>operatorweb/page/saveKepokmas2">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<div class="col-xs-3">
						<p>Beras Premium</p>
						<input type="text" class="form-control" id="h1" name="h1" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Beras Medium</p>
						<input type="text" class="form-control" id="h2" name="h2" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Beras Termurah</p>
						<input type="text" class="form-control" id="h3" name="h3" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Kacang Tanah</p>
						<input type="text" class="form-control" id="h4" name="h4" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Jagung</p>
						<input type="text" class="form-control" id="h5" name="h5" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Kedelai</p>
						<input type="text" class="form-control" id="h6" name="h6" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Ubi Kayu</p>
						<input type="text" class="form-control" id="h7" name="h7" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Cabe Merah Besar</p>
						<input type="text" class="form-control" id="h8" name="h8" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Cabe Merah Keriting</p>
						<input type="text" class="form-control" id="h9" name="h9" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Cabe Rawit Merah</p>
						<input type="text" class="form-control" id="h10" name="h10" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Bawang Putih (Bonggol)</p>
						<input type="text" class="form-control" id="h11" name="h11" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Bawang Putih (Kating)</p>
						<input type="text" class="form-control" id="h12" name="h12" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Minyak Goreng</p>
						<input type="text" class="form-control" id="h13" name="h13" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Tepung Terigu</p>
						<input type="text" class="form-control" id="h14" name="h14" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Daging Sapi</p>
						<input type="text" class="form-control" id="h15" name="h15" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Ikan Asin Teri</p>
						<input type="text" class="form-control" id="h16" name="h16" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Ikan Asin Layur</p>
						<input type="text" class="form-control" id="h17" name="h17" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Daging Ayam Ras</p>
						<input type="text" class="form-control" id="h18" name="h18" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Telur Ayam Ras</p>
						<input type="text" class="form-control" id="h19" name="h19" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Gula Pasir (Lokal)</p>
						<input type="text" class="form-control" id="h20" name="h20" placeholder="Rp.">
						</div>
						<div class="col-xs-3">
						<p>Bawang Merah</p>
						<input type="text" class="form-control" id="h21" name="h21" placeholder="Rp.">
						</div>
						
						<div class="col-xs-3">
						<p>Tanggal</p>
							<input type="text" class="form-control" id="ppid_tgl" name="ppid_tgl" placeholder="yyyy" style="width:130px;" autocomplete=off />
						</div>
						
						
						<div class="col-xs-3">
						<p>Pasar</p>
							<select id="pasar" name="pasar" class="form-control" style="width:200px;">
								<option value="1">Pasar Johar</option>
								<option value="2">Pasar Karangayu</option>
								<option value="3">Pasar Gayamsari</option>
								<option value="4">Pasar Pedurungan</option>
								<option value="5">Pasar Rasamala</option>
								<option value="6">Pasar Jatingaleh</option>
								<option value="7">Pasar Dargo</option>
								<option value="8">Pasar Mangkang</option>
								<option value="9">Pasar Peterongan</option>	
								<option value="10">Pasar Ngaliyan</option>									
							</select>
						</div>
						
						<div class="col-xs-3">
						<p>Publish</p>
							<select id="flag" name="flag" class="form-control" style="width:200px;">
								<option value="1">Ya</option>
								<option value="0">Tidak</option>	
							</select>
						</div>
					</div>
					
					
						
					
					
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">List Kepokmas </h3>
					  <div class="box-tools">
						<div class="input-group pull-right" style="width: 150px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:200px" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
							
						  </div>
						  
						</div>
						<div class="btn-group">
						<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="5">5</option>
						<option value="20">20</option>
						<option value="40">40</option>
					</select>
					</div>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>