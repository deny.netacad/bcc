<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}
	
	
    $(document).ready(function(){
		var config_image = {
            toolbar : 'Image',
            height: '200',
            width: '100%'
        };
		
		var config_pengantar = {
            toolbar : 'MyToolbar',
            height: '350'
        };

		$('#art_date').datepicker({
			format:'yyyy-mm-dd',
			autoclose:true
		});
		
        $('#art_content,#art_intro').ckeditor(config_pengantar);
		$('#art_thumb').ckeditor(config_image);
		
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup(function(){
			refreshData();
		});
		
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        $('#form1').trigger('reset');
                    });
            }else{
                $('#form1').trigger('reset');
            }
			$('#id_edit').val('');
        });
		
		
        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			CKupdate();
			$('#id_edit').val('');
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
        });
		

		$(document).on('click','a.ajax-replace', function(){
			
			
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		
		
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>operatorweb/page/editArticle',
				type: 'post',
				data: { 
					'art_id':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					for(attrname in ret){
						if(attrname=='art_id'){
							$('#id_edit').val(ret[attrname]);
						}else{
							$('#'+attrname).val(ret[attrname]);
						}
					}
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>operatorweb/page/delArticle',
					type: 'post',
					data: { 
						'art_id':$this.eq(index).attr('data')
						,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
	
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#art_title').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
		function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>operatorweb/page/data/article',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }
    });
    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
		  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Manajemen Berita dan Artikel </h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>operatorweb/page/saveArticle">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="art_title" class="col-sm-2 control-label">Judul</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="art_title" name="art_title" placeholder="Judul article Timeline">
						</div>
					</div>
					<div class="form-group">
						<label for="art_date" class="col-sm-2 control-label">Tanggal</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="art_date" name="art_date" placeholder="yyyy-mm-dd" style="width:130px;" />
						</div>
					</div>
					<div class="form-group">
						<label for="art_thumb" class="col-sm-2 control-label">Thumbnail</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="art_thumb" name="art_thumb" placeholder="Thumbnail"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="art_intro" class="col-sm-2 control-label">Article Intro</label>
						<div class="col-sm-10">
							<textarea id="art_intro" name="art_intro" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="art_content" class="col-sm-2 control-label">Article Content</label>
						<div class="col-sm-10">
							<textarea id="art_content" name="art_content" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="position" class="col-sm-2 control-label">Posisi</label>
						<div class="col-sm-10">
							<select id="position" name="position" class="form-control" style="width:200px;">
								<option value="0">Berita</option>
								<option value="1">Event</option>
								<!--<option value="1">Artikel</option>-->								
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="flag" class="col-sm-2 control-label">Publish</label>
						<div class="col-sm-10">
							<select id="flag" name="flag" class="form-control" style="width:200px;">
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
							</select>
						</div>
					</div>
					
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">List Article</h3>
					  <div class="box-tools">
						<div class="input-group pull-right" style="width: 150px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:200px" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
							
						  </div>
						  
						</div>
						<div class="btn-group">
						<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="4">4</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					</div>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>