<script>
	
$(document).ready(function(){
	
	/* action save */
	$(document).on('submit','#form1',function(event) {
		event.preventDefault();
		var $form = $( this );
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});

		var url = $form.attr( 'action' );

		if(confirm('simpan data ..?')){
			$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					alert(ret.text);
				});
		}
	});

	/* untuk reset */
	$(document).on('reset','#form1',function(){
		$('input').each(function(index,item){
			$(item).attr('disabled',false);
		});
		$('select').each(function(index,item){
			$(item).attr('disabled',false);
		});
	});
	
	$('#myTab').on('shown',function(e){
		var index = $('#myTab a').index(e.target);
		switch(index){
			case 0:
				$('#user_pass').focus();
			break;
		}
	});

});
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Section - Update Password</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>main/saveChangepass">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<div class="box-body">
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Username</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value="<?=$this->session->userdata('user_name')?>" disabled />
						</div>
					</div>
					<div class="form-group">
						<label for="user_pass" class="col-sm-2 control-label">Password Baru</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="user_pass" name="user_pass" placeholder="Password Baru" required />
						</div>
					</div>
					<div class="form-group">
						<label for="user_pass2" class="col-sm-2 control-label">Ulangi Password Baru</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="user_pass2" name="user_pass2" placeholder="Ulangi Password Baru" required />
						</div>
					</div>
					
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>