<?php

?>

<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>

<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/locales-all.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/themes/fas/theme.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>
  $( document ).ready(function() {
    $('.ckeditor').each(function(e){
      $(this).ckeditor();
    });
  });

</script>

<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
        <form class="form-horizontal" action="<?php echo base_url('operatorweb/simpanProfile'); ?>" method="POST">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
          <div id="accordion">
    				<div class="box box-info">
      				<div class="box-header with-border" href="#col1" data-toggle="collapse" data-parent="#accordion">
                <h3 class="box-title">Profile Disnaker</h3>
      				</div><!-- /.box-header -->
              <div id="col1" class="box-body panel-collapse collapse in">
                <div class="form-group">
      						<div class="col-md-12">
                    <?php
                      $qProfile = $this->db->query("
                        SELECT content
                        FROM section_profile
                        WHERE name = 'profile'
                      ")->row()->content;
                    ?>
                    <textarea class="ckeditor" name="profile" rows="8" cols="80"><?php echo $qProfile; ?></textarea>
      						</div>
      					</div>
              </div>
    				</div>
    				<div class="box box-info">
      				<div class="box-header with-border" href="#col2" data-toggle="collapse" data-parent="#accordion">
                <h3 class="box-title">Visi dan Misi</h3>
      				</div><!-- /.box-header -->
              <div id="col2" class="box-body panel-collapse collapse">
                <div class="form-group">
      						<div class="col-md-12">
                    <?php
                      $qVisi = $this->db->query("
                        SELECT content
                        FROM section_profile
                        WHERE name = 'visimisi'
                      ")->row()->content;
                    ?>
                    <textarea class="ckeditor" name="visimisi" rows="8" cols="80"><?php echo $qVisi; ?></textarea>
      						</div>
      					</div>
              </div>
    				</div>
    				<div class="box box-info">
      				<div class="box-header with-border" href="#col21" data-toggle="collapse" data-parent="#accordion">
                <h3 class="box-title">Profile Kepala Dinas</h3>
      				</div><!-- /.box-header -->
              <div id="col21" class="box-body panel-collapse collapse">
                <div class="form-group">
      						<div class="col-md-12">
                    <?php
                      $qVisi = $this->db->query("
                        SELECT content
                        FROM section_profile
                        WHERE name = 'kadis'
                      ")->row()->content;
                    ?>
                    <textarea class="ckeditor" name="kadis" rows="8" cols="80"><?php echo $qVisi; ?></textarea>
      						</div>
      					</div>
              </div>
    				</div>
    				<div class="box box-info">
      				<div class="box-header with-border" href="#col3" data-toggle="collapse" data-parent="#accordion">
                <h3 class="box-title">Strategi</h3>
      				</div><!-- /.box-header -->
              <div id="col3" class="box-body panel-collapse collapse">
                <div class="form-group">
      						<div class="col-md-12">
                    <?php
                      $qStrategi = $this->db->query("
                        SELECT content
                        FROM section_profile
                        WHERE name = 'strategi'
                      ")->row()->content;
                    ?>
                    <textarea class="ckeditor" name="strategi" rows="8" cols="80"><?php echo $qStrategi; ?></textarea>
      						</div>
      					</div>
              </div>
    				</div>
    				<div class="box box-info">
      				<div class="box-header with-border" href="#col4" data-toggle="collapse" data-parent="#accordion">
                <h3 class="box-title">Tugas Pokok dan Fungsi</h3>
      				</div><!-- /.box-header -->
              <div id="col4" class="box-body panel-collapse collapse">
                <div class="form-group">
      						<div class="col-md-12">
                    <?php
                      $qTupoksi = $this->db->query("
                        SELECT content
                        FROM section_profile
                        WHERE name = 'tupoksi'
                      ")->row()->content;
                    ?>
                    <textarea class="ckeditor" name="tupoksi" rows="8" cols="80"><?php echo $qTupoksi; ?></textarea>
      						</div>
      					</div>
              </div>
    				</div>
    				<div class="box box-info">
      				<div class="box-header with-border" href="#col5" data-toggle="collapse" data-parent="#accordion">
                <h3 class="box-title">Struktur Organisasi</h3>
      				</div><!-- /.box-header -->
              <div id="col5" class="box-body panel-collapse collapse">
                <div class="form-group">
      						<div class="col-md-12">
                    <?php
                      $qStruktur = $this->db->query("
                        SELECT content
                        FROM section_profile
                        WHERE name = 'struktur'
                      ")->row()->content;
                    ?>
                    <textarea class="ckeditor" name="struktur" rows="8" cols="80"><?php echo $qStruktur; ?></textarea>
      						</div>
      					</div>
              </div>
    				</div>
            <div class="row">
              <div class="col-lg-12">
                <button type="submit" class="btn btn-md btn-primary pull-right"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </div>
          </div>
        </form>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>

</section>
