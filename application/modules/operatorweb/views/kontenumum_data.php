<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (b.title like '%".$cari."%' or a.content like '%".$cari."%'
	 or c.username like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select * from dokumen a 
	inner join section_category b on a.menu_id=b.id
	left join mast_user c on a.iduser=c.iduser
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operatorweb/page/data/kontenumum';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.title as menu_title,c.username
		FROM dokumen a
		inner join section_category b on a.menu_id=b.id
        left join mast_user c on a.iduser=c.iduser
     $where
        order by b.id
		 limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    <colgroup>
        <col class="con0" style="width:25px" />
		<col class="con0" style="width:200px" />
		<col class="con0" style="width:auto" />
		<col class="con0" style="width:100px" />
		<col class="con0" style="width:100px" />
		<col class="con0" style="width:100px" />
		<col class="con0" style="width:70px" />
    </colgroup>
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>MENU</th>
        <th>TITLE</th>
        <th>STATUS</th>
		<th>TIMESTAMP</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
        ?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->doc_id?></span>
            <div class="text-left"><?=$item->menu_title?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->title?></div>
			<div class="text-left"><small><?=substr(strip_tags($item->content,''),0,200)?> ...</small></div>
        </td>
        <td>
            <div class="text-left"><?=($item->flag==1)?'Publish':'Unpublish'?></div>
        </td>
		<td>
            <div class="text-center"><?=$item->tmin?></div>
        </td>
		<td>
            <div class="text-center"><?=$item->username?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->doc_id?>"><i class="text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->doc_id?>"><i class="text-red fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>