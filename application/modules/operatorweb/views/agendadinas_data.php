<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.agenda_title like '%".$cari."%' or a.agenda_date like '%".$cari."%'
	 or b.username like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "SELECT a.*,b.nama AS USER FROM tbl_agenda a 
	LEFT JOIN mast_user b ON a.iduser=b.iduser 
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operatorweb/page/data/agendadinas';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.username as user
		FROM tbl_agenda a
		left join mast_user b on a.iduser=b.iduser 
		 $where
		ORDER BY a.id_agenda desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>ISI AGENDA</th>
		<th>TEMPAT AGENDA</th>
        <th>TANGGAL</th>
        <th>KETERANGAN AGENDA</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->isi_agenda?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->tmpt_agenda?></div>
        </td>
		  <td>
            <div class="text-left"><?=$item->tgl_agenda?></div>
        </td>
        <td>
            <div class="text-left"><small><?=$item->ket_agenda?></small></div>
        </td>
		<td>
            <div class="text-center"><?=$item->nama?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->id_agenda?>"><i  class=" text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->id_agenda?>"><i class=" text-red fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>