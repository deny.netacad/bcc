<script>
	try{
		$('#fileupload').fileupload({
			url:'<?=base_url()?>operatorweb/page/do_uploadphoto',
			maxWidth:600,
			maxHeight:600,
			previewMaxWidth: 300,
			previewMaxHeight: 300,
			previewThumbnail:false,
			previewCrop: true,
			formData:{ 
				'username':'<?=$this->session->userdata('username')?>',
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
			complete: function (e, data) {
				$('#savegallery span').html('Simpan Gallery');
			}
		});
		}catch(e){ alert(e.message); }
	
	
	$(document).on('ready',function(){
		
		
	});
</script>
<?php
$readonly = "";
$albumvalue = "";
if($this->input->post('album')!=''){
	$readonly = " readonly";
	$albumvalue = $this->input->post('album');
}
?>

<form id="fileupload" action="<?=base_url()?>operatorweb/page/saveGalleryupload" method="POST" enctype="multipart/form-data">
<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
<div class="row">
	<div class="col-lg-12">
		<input type="text" class="form-control" name="album" id="album" value="<?=$albumvalue?>" placeHolder="Nama Album" style="margin-bottom:5px;" required <?=$readonly?> />
	</div>
</div>
<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
<div class="row fileupload-buttonbar">
	<div class="col-lg-7">
		<!-- The fileinput-button span is used to style the file input field as button -->
		<span class="btn btn-success fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Add files...</span>
			<input type="file" name="files" multiple>
		</span>
		<button type="submit" class="btn btn-primary start" id="savegallery">
			<i class="glyphicon glyphicon-upload"></i>
			<span>Start upload</span>
		</button>
		<button type="reset" class="btn btn-warning cancel">
			<i class="glyphicon glyphicon-ban-circle"></i>
			<span>Cancel upload</span>
		</button>
		<button type="button" class="btn btn-danger delete">
			<i class="glyphicon glyphicon-trash"></i>
			<span>Delete</span>
		</button>
		<input type="checkbox" class="toggle">
		<!-- The loading indicator is shown during file processing -->
		<span class="fileupload-loading"></span>
	</div>
	<!-- The global progress information -->
	<div class="col-lg-5 fileupload-progress fade">
		<!-- The global progress bar -->
		<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
			<div class="progress-bar progress-bar-success" style="width:0%;"></div>
		</div>
		<!-- The extended global progress information -->
		<div class="progress-extended">&nbsp;</div>
	</div>
</div>
<!-- The table listing the files available for upload/download -->
<!--<table role="presentation" class="row-fluid table table-striped"><tbody class="files"></tbody></table>-->
<div class="row-fluid files"></div>	
</form>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<div class="col-lg-3 template-upload fade">
		<span class="preview"></span>
		<p class="name">{%=file.name%}</p>
		{% if (file.error) { %}
			<div><span class="label label-danger">Error</span> {%=file.error%}</div>
		{% } %}
		<p class="size">{%=o.formatFileSize(file.size)%}</p>
		{% if (!o.files.error) { %}
			<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
		{% } %}
		{% if (!o.files.error && !i && !o.options.autoUpload) { %}
			<button class="btn btn-primary start">
				<i class="glyphicon glyphicon-upload"></i>
				<span>Start</span>
			</button>
		{% } %}
		{% if (!i) { %}
			<button class="btn btn-warning cancel">
				<i class="glyphicon glyphicon-ban-circle"></i>
				<span>Cancel</span>
			</button>
		{% } %}
</div>
{% } %}

</script>

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<div class="col-lg-3 col-md-3 col-sm-6 template-download fade">
	<div class="info-box hover ehover12">
		{% if (file.thumbnailUrl) { %}
			<img class="img-responsive" src="{%=file.thumbnailUrl%}">
			<div class="overlay">
				<h2>&nbsp;</h2>
				{% if (file.deleteUrl) { %}
					<button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
						<i class="glyphicon glyphicon-trash"></i>
						<span>Delete</span>
					</button>
					<input type="checkbox" name="delete" value="1" class="toggle">
				{% } else { %}
					<button class="btn btn-warning cancel">
						<i class="glyphicon glyphicon-ban-circle"></i>
						<span>Cancel</span>
					</button>
				{% } %}
			</div>		
		{% } %}
	</div>
	<p class="name">
		
	</p>
	{% if (file.error) { %}
		<div><span class="label label-danger">Error</span> {%=file.error%}</div>
	{% } %}
	<!--<span class="size">{%=o.formatFileSize(file.size)%}</span>-->
	<input type="hidden" name="filename[]" value="{%=file.name%}"  />
	<input type="hidden" name="filethumb[]" value="{%=file.thumbnailUrl%}"  />
	<input type="text" name="filedesc[]" style="width:90%" placeHolder="Add Caption"  />
	
</div>
{% } %}
</script>
