<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<?php
#print_r($this->session->userdata);
?>
<script>
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			CKEDITOR.instances[instance].updateElement();
		}
		CKEDITOR.instances[instance].setData('');
	}
	
	
	function loadData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>operator/page/loadAboutus',
            data:{ 
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                
            },
            success:function(response){
                var ret = $.parseJSON(response);
				for(attrname in ret){
					if(attrname=='doc_id'){
						$('#id_edit').val(ret[attrname]);
					}else{
						$('#'+attrname).val(ret[attrname]);
					}
				}
            }
        });
    }

	
    $(document).ready(function(){
        
		var config_pengantar = {
            toolbar : 'MyToolbar',
            height: '350'
        };

        $('#content').ckeditor(config_pengantar);

		
		$('#per_page').change(function(){
			loadData();
		});
		
		$('#cari').keyup( $.debounce(250,loadData));
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        loadData();
                    });
            }else{
                loadData();
            }
        });

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			CKupdate();
        });
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#title').focus();
				break;
			}
		});
		
        loadData();

    });

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
	 <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
			  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Section - Tentang Kami</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>operator/page/saveAboutus">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Judul</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="title" name="title" placeholder="Judul Konten">
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-2 control-label">Isi Konten</label>
						<div class="col-sm-10">
							<textarea id="content" name="content" placeholder="Isi Konten"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Status</label>
						<div class="col-sm-10">
							<select id="flag" name="flag" class="form-control" required="">
								<option value="1">Publish</option>
								<option value="0">Unpublish</option>
							</select>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>