<?php
  $def_video = $this->db->query("
    SELECT * FROM r_pengaturan
    WHERE parameter = 'video_files'
  ")->row();
  $def_video = $def_video->value;

  $profile_vid = $this->db->query("
    SELECT * FROM r_pengaturan
    WHERE parameter = 'video_disnaker'
  ")->row();
  $profile_vid = $profile_vid->value;

  $kiosk_vid = $this->db->query("
    SELECT * FROM r_pengaturan
    WHERE parameter = 'video_kiosk'
  ")->row();
  $kiosk_vid = $kiosk_vid->value;

  $allvid = array($def_video, $profile_vid, $kiosk_vid);

  $qSlider = $this->db->query("
    SELECT * FROM r_pengaturan
    WHERE parameter = 'front_slider'
    ORDER BY num ASC
  ")->result();
  $def_slider = array();
  foreach ($qSlider as $key => $value) {
    $def_slider[$value->num] = $value->value;
  }

  $qSlider = $this->db->query("
    SELECT * FROM r_pengaturan
    WHERE parameter = 'profile_slider'
    ORDER BY num ASC
  ")->result();
  $profile_slider = array();
  foreach ($qSlider as $key => $value) {
    $profile_slider[$value->num] = $value->value;
  }
?>

<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>

<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/locales-all.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/themes/fas/theme.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>
  $( document ).ready(function() {
    $('.datatable').DataTable();
    $("#upload-custom").fileinput({
      theme: "fas"
    });

    $("#btnUpload").on( "click", function() {
      $('#modalUpload').modal('show');
    });

  });

</script>

<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
  				<div class="box-header with-border">
            <h3 class="box-title">Extra Content</h3>
            <button id="btnUpload" class="btn btn-sm btn-info pull-right"><i class="fa fa-upload"></i> Upload Berkas</button>
  				</div><!-- /.box-header -->
          <div class="box-body">
            <form class="form-horizontal" action="<?php echo base_url('operatorweb/simpanConfig'); ?>" method="POST">
              <div class="form-group">
    						<label for="no_loket" class="col-md-2 control-label">Image Slider</label>
    						<div class="col-md-10">
                  <?php
                    $qFiles = $this->db->query("
                      SELECT * FROM h_extra_files
                      WHERE type = 'front_slider'
                    ")->result();
                  ?>
                  <div class="row" style="padding: 0px 0px;">
                    <?php foreach ($qFiles as $key => $value): ?>
                      <div class="col-lg-4 col-md-6 col-sm-12" style="margin-top: 10px;">
                        <img src="<?php echo base_url('/assets/img/banner/'.$value->filename); ?>" alt="" class="img img-responsive" width="100%" style="border: 1px solid black;">
                        <?php $test  = array_search($value->filename,$def_slider,true); ?>
                        <?php $test2 = array_search($value->filename,$profile_slider,true); ?>
                        Front Slider:
                        <div class="col-lg-12">
                          <?php foreach ($def_slider as $k => $v): ?>
                            <?php if ($v == $value->filename): ?>
                              <a href="<?php echo base_url('/operatorweb/enableSlide/'.$value->filename.'/'.$k); ?>" class="btn btn-sm btn-default pull-left disabled" style="margin: 2px;"><?php echo $k; ?></a>
                            <?php else: ?>
                              <a href="<?php echo base_url('/operatorweb/enableSlide/'.$value->filename.'/'.$k); ?>" class="btn btn-sm btn-success pull-left" style="margin: 2px;"><?php echo $k; ?></a>
                            <?php endif; ?>
                          <?php endforeach; ?>
                          <?php if (!$test && !$test2): ?>
                            <a href="<?php echo base_url('/operatorweb/deleteFile/'.$value->filename); ?>" class="btn btn-sm btn-danger pull-right" style="margin: 2px;" onclick="if(confirm('Hapus Berkas?')) return true; else return false;"><i class="fa fa-trash"></i> Delete</a>
                          <?php endif; ?>
                        </div>
                        <br>
                        Profile Slider:
                        <div class="col-lg-12">
                          <?php foreach ($profile_slider as $k2 => $v2): ?>
                            <?php if ($v2 == $value->filename): ?>
                              <a href="<?php echo base_url('/operatorweb/enableSlide2/'.$value->filename.'/'.$k2); ?>" class="btn btn-sm btn-default pull-left disabled" style="margin: 2px;"><?php echo $k2; ?></a>
                            <?php else: ?>
                              <a href="<?php echo base_url('/operatorweb/enableSlide2/'.$value->filename.'/'.$k2); ?>" class="btn btn-sm btn-success pull-left" style="margin: 2px;"><?php echo $k2; ?></a>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
    						</div>
    					</div>
              <div class="form-group">
    						<label for="no_loket" class="col-md-2 control-label">Default Video</label>
    						<div class="col-md-10">
                  <?php
                    $qFiles = $this->db->query("
                      SELECT * FROM h_extra_files
                      WHERE type = 'video_files'
                    ")->result();
                  ?>
                  <div class="row" style="padding: 0px 5px;">
                    <?php foreach ($qFiles as $key => $value): ?>
                      <div class="col-lg-4 col-md-6 col-sm-12" style="margin-top: 10px;">
                        <video width="100%" height="220" controls style="border: 1px solid black; object-fit: cover;">
                          <source src="<?php echo base_url('/assets/video/'.$value->filename) ?>">
                        </video>
                        <?php $testVid  = array_search($value->filename,$allvid,true); ?>
                        <div class="row">
                          <div class="col-lg-12">
                            <?php if ($def_video == $value->filename): ?>
                                <a type="button" class="btn btn-xs btn-primary pull-left disabled" style="margin: 2px;">Current Videowall</a>
                              <?php else: ?>
                                <a href="<?php echo base_url('/operatorweb/enableVid/'.$value->filename); ?>" class="btn btn-xs btn-success pull-left" style="margin: 2px;">Set Videowall</a>
                            <?php endif; ?>
                            <?php if ($testVid === false): ?>
                              <a href="<?php echo base_url('/operatorweb/deleteFile/'.$value->filename); ?>" class="btn btn-sm btn-danger pull-right" style="margin-top: -3px;" onclick="if(confirm('Hapus Berkas?')) return true; else return false;"><i class="fa fa-trash"></i> Delete</a>
                            <?php endif; ?>
                            <button onclick="copyUrl('<?php echo base_url('/assets/video/'.$value->filename); ?>')" type="button" class="btn btn-sm btn-info pull-right" style="margin-top: -3px; margin-right: 5px;" >Copy URL</button>
                          </div>
                          <div class="col-lg-12">
                            <?php if ($profile_vid == $value->filename): ?>
                              <a type="button" class="btn btn-xs btn-primary pull-left disabled" style="margin: 2px;">Current Profile Disnaker</a>
                            <?php else: ?>
                              <a href="<?php echo base_url('/operatorweb/enableVid2/'.$value->filename); ?>" class="btn btn-xs btn-success pull-left" style="margin: 2px;">Set Profile Disnaker</a>
                            <?php endif; ?>
                          </div>
                          <div class="col-lg-12">
                            <?php if ($kiosk_vid == $value->filename): ?>
                              <a type="button" class="btn btn-xs btn-primary pull-left disabled" style="margin: 2px;">Current Kiosk Antrian</a>
                            <?php else: ?>
                              <a href="<?php echo base_url('/operatorweb/enableVid3/'.$value->filename); ?>" class="btn btn-xs btn-success pull-left" style="margin: 2px;">Set Kiosk Antran</a>
                            <?php endif; ?>
                          </div>

                        </div>

                      </div>
                    <?php endforeach; ?>
                  </div>
    						</div>
    					</div>
              <div class="form-group">
    						<label for="no_loket" class="col-md-2 control-label">Running Text</label>
    						<div class="col-md-10">
                  <textarea name="running_text" rows="2" class="form-control" required><?php echo $this->db->query('SELECT value FROM r_pengaturan WHERE parameter = "running_text"')->row()->value; ?></textarea>
    						</div>
    					</div>
              <div class="form-group">
    						<div class="col-md-12">
                  <button type="submit" class="btn btn-md btn-primary pull-right"><i class="fa fa-save"></i> Simpan Running Text</button>
                </div>
    					</div>
            </form>
          </div>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>

  <!-- Modal -->
<div class="modal fade" id="modalUpload" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4><span class="glyphicon glyphicon-file"></span> Tambah Berkas</h4>
      </div>
      <form id="formUpload" class="form-horizontal" action="<?php echo base_url('operatorweb/uploadBerkas'); ?>" enctype="multipart/form-data" method="POST">
        <div class="modal-body">
          <div class="form-horizontal">
            <div class="form-group">
              <label for="bus_hour" class="col-sm-3 control-label">Pilih Berkas</label>
              <div class="col-sm-9">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                  <input id="upload-custom" accept="image/*, video/*" name="file" type="file" class="file" data-browse-on-zone-click="true" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button class="btn btn-danger pull-left" data-dismiss="modal">Batalkan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>

<script type="text/javascript">
  function copyUrl(url) {
    var el = document.createElement('textarea');

    el.value = url;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    alert("Copied the text: " + url);
  }
</script>
