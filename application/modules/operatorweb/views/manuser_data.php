<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?10:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.user_name like '%".$cari."%' or a.user_id like '%".$cari."%') ";

    $n = intval($this->uri->segment(5));
    $q = "select * from ref_user a";
	
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'main/page/data/manuser';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.* from ref_user a
		 $where
		ORDER BY a.user_id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>USERNAME</th>
        <th>NAMA LENGKAP</th>
        <th>EMAIL</th>
		<th><div class="text-center">ROLE</div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
    ?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->user_name?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->user_realname?></div>
        </td>
		<td>
            <div class="text-left"><?=$item->user_email?></div>
        </td>
        <td>
            <div class="text-center"><?=(($item->user_role==1)?"super admin":"admin konten")?></div>
        </td>
		<td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->user_id?>"><i class="fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->user_id?>"><i class="fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>