<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?10:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.prod_name like '%".$cari."%' or a.prod_price like '%".$cari."%'
	 or b.user_name like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select * from section_product a 
	left join ref_user b on a.iduser=b.user_id
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'main/page/data/manproduk';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.user_name
		FROM section_product a
		left join ref_user b on a.iduser=b.user_id
		 $where
		ORDER BY a.prod_id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>NAMA PRODUK</th>
        <th>HARGA PRODUK</th>
        <th>DESKRIPSI</th>
		<th>THUMBNAIL</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
    preg_match_all('/<img[^>]+>/i',$item->prod_thumb, $result);
	if(count($result)>0){
		$prod_thumb = (string) reset(simplexml_import_dom(DOMDocument::loadHTML($result[0][0]))->xpath("//img/@src"));
	}
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->prod_id?></span>
            <div class="text-left"><?=$item->prod_name?></div>
        </td>
        <td>
            <div class="text-left"><?=number_format($item->prod_price)?></div>
        </td>
        <td>
            <div class="text-left"><small><?=substr(strip_tags($item->prod_desc,''),0,200)?> ...</small></div>
        </td>
		<td>
            <div class="text-center"><a href="<?=str_replace('.thumbs/','',$prod_thumb)?>" rel="lightbox"><img src="<?=$prod_thumb?>" /></a></div>
        </td>
		<td>
            <div class="text-center"><?=$item->user_name?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->prod_id?>"><i class="fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->prod_id?>"><i class="fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>