<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?10:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.judul_video like '%".$cari."%' or a.link_video like '%".$cari."%'
	 or b.user_name like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select * from section_manvideo a 
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'main/page/data/manvideo';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*
		FROM section_manvideo a
	
		 $where
		ORDER BY a.id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>JUDUL VIDEO</th>
        <th>LINK VIDEO</th>
        <th>DESKRIPSI</th>
		<th>THUMBNAIL</th>
		
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
   
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->id?></span>
            <div class="text-left"><?=$item->judul_video?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->link_video?></div>
        </td>
        <td>
            <div class="text-left"><small><?=substr(strip_tags($item->desc_video,''),0,200)?> ...</small></div>
        </td>
		<td>
         <div class="text-left"><small><?=substr(strip_tags($item->desc_tumb,''),0,200)?> ...</small></div>  </td>
		
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->id?>"><i class="fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->id?>"><i class="fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>