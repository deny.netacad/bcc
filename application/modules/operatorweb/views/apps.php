<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}
	
	var xhr = $.ajax();
    $(document).ready(function(){
		var config_image = {
            toolbar : 'Image',
            height: '200',
            width: '100%'
        };
		
		var config_pengantar = {
            toolbar : 'MyToolbar',
            height: '350'
        };

		$('#agenda_date').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		});
		
        $('#apps_detail').ckeditor(config_pengantar);
		$('#apps_thumb').ckeditor(config_image);
		
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup(function(){
			refreshData();
		});
		
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        $('#form1').trigger('reset');
                    });
            }else{
                $('#form1').trigger('reset');
            }
			$('#id_edit').val('');
        });
		
		
        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			CKupdate();
			$('#id_edit').val('');
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
        });
		

		$(document).on('click','a.ajax-replace', function(){
			
			xhr.abort();
			xhr = $.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(), '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
         },
				beforeSend:function(){
					$('#spinner').html('<i class="icon-spinner icon-spin"></i>');
				},
				success:function(response){
					$('#spinner').html('');
					$('#result').html(response);
				}
			});
			return false;
		});
		
		
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>operatorweb/page/editApps',
				type: 'post',
				data: { 
					'apps_id':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					for(attrname in ret){
						if(attrname=='apps_id'){
							$('#id_edit').val(ret[attrname]);
						}else if(attrname=='apps_detail'){
							CKEDITOR.instances['apps_detail'].setData(ret.apps_detail);	
						}else if(attrname=='apps_thumb'){
							CKEDITOR.instances['apps_thumb'].setData(ret.apps_thumb);		
						}else{
							$('#'+attrname).val(ret[attrname]);
						}
					}
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>operatorweb/page/delApps',
					type: 'post',
					data: { 
						'apps_id':$this.eq(index).attr('data')
						,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });

		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#apps_title').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
		function refreshData(){
			$.ajax({
				type:'post',
				url:'<?=base_url()?>operatorweb/page/data/apps',
				data:{ 
					'per_page':$('#per_page').val(),'cari':$('#cari').val(),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
		}
    });
    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
		  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Manajemen Link Aplikasi
					<?php //print_r ($this->session->userdata('ses_kcfinder'))?>
				</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>operatorweb/page/saveApps">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="apps_title" class="col-sm-2 control-label">Judul Aplikasi</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="apps_title" name="apps_title" placeholder="Judul aplikasi">
						</div>
					</div>
					<div class="form-group">
						<label for="apps_link" class="col-sm-2 control-label">Link Aplikasi</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="apps_link" name="apps_link" placeholder="Link aplikasi">
						</div>
					</div>
					<div class="form-group">
						<label for="apps_link" class="col-sm-2 control-label">Position Promo Aplikasi</label>
						<div class="col-sm-10">
							<select name="position" id="position" class="form-control" style="width:200px;">
							<option value="1"> SLIDE LARGE </option>
							<option value="2"> SLIDE MEDIUM</option>
							<option value="4"> SLIDE SMALL </option>
							<option value="3"> SLIDE SMALL POTRAIT</option>
							<option value="5"> SLIDE SMALL LANDSCAPE</option>							
							<option value="6"> BOTTOM 4 PROMO </option>			
							<option value="7"> PARALAX FRONT PAGE</option>			
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="apps_thumb" class="col-sm-2 control-label">Image Aplikasi</label>
						<div class="col-sm-10">
							<textarea id="apps_thumb" name="apps_thumb" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="apps_detail" class="col-sm-2 control-label">Detail Aplikasi</label>
						<div class="col-sm-10">
							<textarea id="apps_detail" name="apps_detail" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="flag" class="col-sm-2 control-label">Publish</label>
						<div class="col-sm-10">
							<select id="flag" name="flag" class="form-control" style="width:200px;">
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
							</select>
						</div>
					</div>
					
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">List Article</h3>
					  <div class="box-tools">
						<div class="input-group pull-right" style="width: 150px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:200px" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
							
						  </div>
						  
						</div>
						<div class="btn-group">
						<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="4">4</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					</div>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>