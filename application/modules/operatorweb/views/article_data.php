<?php
    $where = "where a.position=0 ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " (a.art_title like '%".$cari."%' or a.art_date like '%".$cari."%'
	 or b.username like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "SELECT a.*,b.username
	FROM section_article a
	left join mast_user b on a.iduser=b.iduser
	
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operatorweb/page/data/article';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
	$data['posts'] = $this->db->query("SELECT a.*,b.username
	FROM section_article a
	left join mast_user b on a.iduser=b.iduser
	 $where
	ORDER BY a.art_id desc limit
	".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>JUDUL</th>
        <th>TANGGAL</th>
        <th>LINK</th>
		<th>THUMBNAIL</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
    $art_thumb ='';
	preg_match_all('/<img[^>]+>/i',$item->art_thumb, $result);
	if(count($result[0])>0){
		$art_thumb = (string) reset(simplexml_import_dom(DOMDocument::loadHTML($result[0][0]))->xpath("//img/@src"));
	}
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->art_title?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->art_date?></div>
        </td>
        <td>
            <div class="text-left"><small>/page/berita/<?=$item->art_seo?></small></div>
        </td>
		<td>
           <!-- <div class="text-center"><a href="<?=str_replace('content/','content/.thumbs/',$art_thumb)?>" rel="lightbox"><img src="<?=$art_thumb?>" /></a> </div>-->
        </td>
		<td>
            <div class="text-center"><?=$item->username?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->art_id?>"><i  class=" text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->art_id?>"><i class=" text-red fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>