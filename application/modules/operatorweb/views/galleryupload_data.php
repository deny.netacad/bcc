<?php
$rs = $this->db->query("select * from section_gallery a group by album");
foreach($rs->result() as $item){
?>
<div class="col-lg-3 col-md-3">
	<div class="info-box hover ehover1">
		<img class="img-responsive" src="<?=$item->filethumb?>" alt="">
		<div class="overlay">
			<h2><?=$item->album?></h2>
			<button class="info view-album" data="<?=$item->album?>">Lihat Album</button>
			<button class="info del-album" style="color:red" data="<?=$item->album?>">Hapus Album</button>
		</div>				
	</div>
</div>
<?php
}
?>
<div class="row-fluid">
<div class="col-lg-12">
	<span class="btn btn-success fileinput-button" id="add-album">
		<i class="glyphicon glyphicon-plus"></i>
		<span>Tambah Album</span>
	</span>
</div>
</div>