<script src="<?=$def_js?>sort.js"></script>
<script>
    $(document).ready(function(){
        //doEvent();
        $('#alert2').hide();

        $('table.sortable tbody').sortable({
            update: function(event,ui){
                var bannerOrder_ = $(this).sortable('toArray').toString();
                $.ajax({
                    type: 'post',
                    url:'<?=base_url()?>operator/page/saveSortSlide',
                    data: { bannerOrder: bannerOrder_,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
                    success: function(){
                        $('#per_page').trigger('change');
                    }
                });
            }
        }).disableSelection();
    });
</script>
<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.title_1 like '%".$cari."%' or a.title_2 like '%".$cari."%'
	 or b.mast_user like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select * from section_slide a 
	left join mast_user b on a.iduser=b.iduser
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operator/page/data/manslide';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.username
		FROM section_slide a
		left join mast_user b on a.iduser=b.iduser
		 $where
		ORDER BY a.menu_order asc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers " id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover sortable">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>HEADER 1</th>
        <th>HEADER 2</th>
        <th>LIST</th>
		<th>THUMBNAIL</th>
		<th>DESKRIPSI/LINK</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
    preg_match_all('/<img[^>]+>/i',$item->slide_thumb, $result);
	if(count($result[0])>0){
		$slide_thumb = (string) reset(simplexml_import_dom(DOMDocument::loadHTML($result[0][0]))->xpath("//img/@src"));
	}
	?>
    <tr id="<?=$item->menu_order."-".$item->slide_id?>">
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->slide_id?></span>
            <div class="text-left"><?=$item->title_1?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->title_2?></div>
        </td>
		<td>
            <div class="text-left">
			<ul>
			<?php
			$list = explode(',',$item->list_1);
			if(count($list)>0){
				foreach($list as $list_item){
					?>
					<li><?=$list_item?></li>
					<?php
				}
			}
			?>
			</ul>
			</div>
        </td>
        <td>
            <div class="text-center"><a href="<?=str_replace('.thumbs/','',$slide_thumb)?>" rel="lightbox"><img src="<?=$slide_thumb?>" /></a></div>
        </td>
		<td>
            <div class="text-left"><small><?=substr(strip_tags($item->short_p,''),0,200)?> ...</small></div>
        </td>
		
		<td>
            <div class="text-center"><?=$item->username?></div>
        </td>
        
		<td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->slide_id?>"><i class="text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->slide_id?>"><i class="text-red fa fa-trash"></i></a>
				 <i class="fa fa-arrows"></i>
				 <!--<img class="move" src="<?=base_url()?>assets/img/arrow-move.png" alt="Move" title="Move" />-->
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>