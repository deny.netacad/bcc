<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?2:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " and (a.video_title like '%".$cari."%' or a.event_cat like '%".$cari."%') ";

    $n = intval($this->uri->segment(5));
    $q = "select * from section_events_video a 
	left join mast_user b on a.iduser=b.iduser where a.idbidang='".$this->session->userdata('idbidang')."'
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operator/page/data/videogallery';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.username
		FROM section_events_video a
		left join mast_user b on a.iduser=b.iduser where a.idbidang='".$this->session->userdata('idbidang')."'
		 $where
		ORDER BY a.video_id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>JUDUL VIDEO</th>
		<th>PREVIEW</th>
		<th>KATEGORI</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->video_id?></span>
            <div class="text-left"><?=$item->video_title?></div>
        </td>
		<td>
            <div class="text-center">
			<iframe src="//www.youtube.com/embed/<?=$item->video_code?>?rel=0" frameborder="0" style="width:300px;height:200px;" allowfullscreen></iframe>	
			</div>
        </td>
		<td>
            <div class="text-center"><?=$item->video_cat?></div>
        </td>
		<td>
            <div class="text-center"><?=$item->username?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->video_id?>"><i class="fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->video_id?>"><i class="fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>