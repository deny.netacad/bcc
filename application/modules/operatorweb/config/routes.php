<?php
$route['(operatorweb)/apps'] 		= "page/load/apps";
$route['(operatorweb)/manmenu'] 		= "page/load/manmenu";
$route['(operatorweb)/kontenumum'] 	= "page/load/kontenumum";
$route['(operatorweb)/aboutus'] 		= "page/load/aboutus";
$route['(operatorweb)/package'] 		= "page/load/package";
$route['(operatorweb)/manproduk'] 		= "page/load/manproduk";
$route['(operatorweb)/manslide'] 		= "page/load/manslide";
$route['(operatorweb)/gallery'] 		= "page/load/gallery";
$route['(operatorweb)/galleryupload'] 		= "page/load/galleryupload";
$route['(operatorweb)/videogallery'] 	= "page/load/videogallery";
$route['(operatorweb)/video'] 			= "page/load/video";
$route['(operatorweb)/news'] 			= "page/load/news";
$route['(operatorweb)/article'] 		= "page/load/article";
$route['(operatorweb)/testimonial'] 	= "page/load/testimonial";
$route['(operatorweb)/kontak'] 		= "page/load/kontak";
$route['(operatorweb)/manvideo'] 		= "page/load/manvideo";
$route['(operatorweb)/manuser'] 		= "page/load/manuser";
$route['(operatorweb)/changepass'] 	= "page/load/changepass";
$route['(operatorweb)/pejabat'] 		= "page/load/pejabat";
$route['(operatorweb)/agenda'] 		= "page/load/agenda";
$route['(operatorweb)/links'] 		= "page/load/links";
$route['(operatorweb)/perencanaan'] 		= "page/load/perencanaan";

$route['(operatorweb)/kepokmas'] 		= "page/load/kepokmas";

$route['(operatorweb)/mkepokmas'] 		= "page/load/mkepokmas";
$route['(operatorweb)/jwbpertanyaan'] 		= "page/load/jwbpertanyaan";
$route['(operatorweb)/agendadinas'] 		= "page/load/agendadinas";
$route['(operatorweb)/konten_menu'] 		= "page/load/konten_menu";
$route['(operatorweb)/seo'] 		= "page/load/seo";

$route['(operatorweb)/extra'] 		= "page/load/extra";
$route['(operatorweb)/disnaker'] 		= "page/load/disnaker";
