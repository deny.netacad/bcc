<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portaldev extends MY_Controller {

	public function index()
	{
		$this->db->insert('ngi_webvisitor',array('ip' => $this->input->ip_address()));
		$this->load->view('index',$this->data);
	}
	
	
	function regSubscriber(){
	           	echo '<meta http-equiv="refresh" content="5; url=/">';	
           $data['email'] = $this->input->post('email');
           if(!$this->db->insert("section_subscriber",$data)){
               echo "Email sudah terdaftar berlangganan";
           }else{

               echo "Email berhasil didaftarkan";
           }
           
    }
	
	# jpod
	function resizeImage(){
		echo "Bismillah";
		$dir = BASEPATH."../content/images/";
		$files1 = scandir($dir);
		echo "<pre>";
		#print_r($files1);
		echo '<style> table th,table td{padding:5px;} table td{ vertical-align:top}</style>';
		echo "<table border='1' style='border-collapse:collapse'>";
		echo "<tr style='background-color:#FEFEFE'>";
		echo "<th>No.</th>";
		echo "<th>FILE</th>";
		echo "<th>ORIGINAL SIZE</th>";
		echo "<th>COMPRESSED</th>";
		echo "</tr>";
		$this->load->library('image_lib');
		$n = 0;
		foreach($files1 as $file){
			$ext = pathinfo($dir.$file, PATHINFO_EXTENSION);
			$filesize = $this->formatBytes(filesize($dir.$file),2);
			
			
			#list($width, $height, $type, $attr) = getimagesize($dir.$file);
			switch($ext){
				case "jpg":
					list($width_ori,$height_ori) = getimagesize($dir.$file);
					if($width_ori==1000){
						# set maximum width and height
						$width = 1000;
						$height = 1000;
						$ratio_orig = $width_ori/$height_ori;

						if ($width/$height > $ratio_orig) {
						   $width = $height*$ratio_orig;
						} else {
						   $height = $width/$ratio_orig;
						}
						/*
						$image_p = imagecreatetruecolor($width, $height);
						$image = imagecreatefromjpeg($dir.$file);
						imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_ori, $height_ori);
						imagejpeg($image_p,$dir.$file,98);
						*/
						/*
						# 1583
						$config['image_library'] = 'gd2';
						$config['source_image'] = $dir.$file;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']     = 1000;
						$config['height']   = 1000;
						
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						*/
						$n++;
						echo "<tr>";
						echo "<td>$n</td>";
						echo "<td><a href='http://unifitindonesia.com/beta/content/images/$file' target='_blank'>$file</a></d>";
						echo "<td>$ext<br/>$filesize<br/>$width_ori x $height_ori<br/>$width x $height</d>";
						echo "<td>$ext</d>";
						echo "</tr>";
					}
				break;
			}
		}
		echo "</table>";
		echo "</pre>";
	}
	
	function formatBytes($size, $precision = 2)
	{
		$base = log($size, 1024);
		$suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

		return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
	}
	function do_upload_scanfile(){
		$path_parts = pathinfo($_FILES["scanfile"]["name"]);
		$extension = $path_parts['extension'];
		
		$config['upload_path'] = BASEPATH.'../upliddir/';
		$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
		$config['max_size']	= '5000000';
		$config['file_name'] = $this->input->post('filename').".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('scanfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
		
	}



	function do_upload_scanfile_ktp(){
		$path_parts = pathinfo($_FILES["scanfile"]["name"]);
		$extension = $path_parts['extension'];
		
		$config['upload_path'] = BASEPATH.'../upliddir/ktp/';
		$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
		$config['max_size']	= '5000000';
		$config['file_name'] = $this->input->post('filename').".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('scanfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
		
	}
	
	
	function delUpload(){
		$full_path = $this->input->post('full_path');
		if(file_exists($full_path)){
			unlink($full_path);
			$ret['text'] = "file terhapus";
		}else{
			$ret['text'] = "file tidak ketemu";
		}
		echo json_encode($ret);
	}
	function do_upload(){
		$nama = $this->uri->segment(3);
		$namas = "".$nama."_file";
		$path_parts = pathinfo($_FILES["".$namas.""]["name"]);
		$extension = $path_parts['extension'];
		#$filename = str_replace("_"," ",$_FILES["dip_file"]["name"]);
		$filename = str_replace("_"," ",date('Ymd'));
	
		$filename = str_replace("-"," ",$filename);
		$filename = str_replace($extension,"",$filename);
		$config['upload_path'] = BASEPATH.'../upliddir/'.$nama.'/';
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['max_size']	= '50000000';
		$config['file_name'] = "".$nama."-".url_title($filename).".".$extension;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload(''.$namas.''))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode($data);
		}
	}

	function popularart(){
		$input = array(
			'art_id' => $_POST['art_id']
		);

		$this->db->insert('utin_popular_art',$input);
	}

	function sendemail(){
		$this->load->library('email');

		$mail_config['smtp_host'] = 'mail.bogorkab.go.id';
		$mail_config['smtp_port'] = '587';
		$mail_config['smtp_user'] = 'bogorcareercenter@bogorkab.go.id';
		$mail_config['_smtp_auth'] = TRUE;
		$mail_config['smtp_pass'] = 'career123';
		// $mail_config['smtp_crypto'] = 'tls';
		$mail_config['protocol'] = 'smtp';
		$mail_config['mailtype'] = 'html';
		$mail_config['send_multipart'] = FALSE;
		$mail_config['charset'] = 'utf-8';
		$mail_config['wordwrap'] = TRUE;
		$mail_config['newline'] = "\r\n";
		$this->email->initialize($mail_config);
		$this->email->from('bogorcareercenter@bogorkab.go.id', 'Identification');
		$this->email->to('salafudin.muhammad@gmail.com');
		$this->email->subject('testing');
		$this->email->message('The email send using codeigniter library');

		if($this->email->send()){
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}else{
            $this->session->set_flashdata("email_sent",$this->email->print_debugger());
		}

        redirect(base_url().'portal/login');
	}
	
}
