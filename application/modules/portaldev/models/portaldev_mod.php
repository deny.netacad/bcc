<?php
class Portaldev_mod extends MY_Model {
	
	var $modules = "portaldev";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function foo(){
		echo "est";
	}
	
	function &getChild($member_code,$side=1,$level=1,&$count){
		$rs = $this->db->get_where("ngi_member",array("member_upline"=>$member_code));
		$level++;
		$n = 0;
		foreach($rs->result() as $item){ 
			if($level==2) $n++;
			if($n==2) $side = 2;
			$count[$side][] = $item->member_code;
			$this->getChild($item->member_code,$side,$level,$count);
		}
		return $count;
	}
	
	function createTree($level=2,$limit=5,$start=0){
		$n = 2;
		if($n>0){
			$level++;
			echo '<ul>';
		}
		for($n=0;$n<2;$n++){
			echo '<li level="'.$level.'"><a class="member-tooltip" data-placement="top" title="no member"><div class="m-thumb no-photo"></div>';
			echo '<div class="tree-node">';
			echo '<div class="tree-code">&nbsp;</div>
			<div class="tree-name">&nbsp;</div>
			<div class="tree-feet">&nbsp;</div>';
			echo '</div>';
			echo '<div class="overlay">
						<div class="menu-member">
							<i class="fa fa-plus-circle"></i>
							<i class="fa fa-user"></i>
						</div>
					</div>';
			echo '</a>';
			if($level<$limit){
				$this->createTree($level,$limit,$start);
			}
			echo '</li>';
		}
		if($n>0){
			echo '</ul>';
		}
	}
	
	function showtree($id='',$level=1){
		$rs= $this->db->query("select member_code,member_name,date_format(member_joindate,'%b') as member_joinmonth,
		date_format(member_joindate,'%Y') as member_joinyear,pack_id,ifnull(member_sponsor,member_code) as member_sponsor
		from ngi_member where member_upline='".$id."'");
		if($rs->num_rows()>0){
			$level++;
			echo '<ul>';
		}
		foreach($rs->result() as $item){
			echo '<li level="'.$level.'"><a class="member-tooltip" id="'.$item->member_code.'" data-placement="top" data-sponsor="'.$item->member_sponsor.'" 
			title="'.$item->member_code.'
			<div>'.$item->member_name.'</div>"><div class="m-thumb no-photo"></div>';
			echo '<div class="tree-node pack-'.$item->pack_id.'">';
			echo '<div class="tree-code">'.$item->member_code.'</div>
			<div class="tree-name">'.$item->member_name.'</div>';
			echo '</div>';
			echo '</a>';
			if($level<5){
				$this->showtree($item->member_code,$level);
			}
			echo '</li>';
		}
		
		if($rs->num_rows()>0){
			echo '</ul>';
		}
	}
	
	function showtree2($id='',$level=1){
		$rs= $this->db->query("select member_code,member_name,date_format(member_joindate,'%b') as member_joinmonth,
		date_format(member_joindate,'%Y') as member_joinyear 
		from ngi_member where member_upline='".$id."'");
		if($rs->num_rows()>0){
			$level++;
			echo '<ul style="display:none">';
		}
		foreach($rs->result() as $item){
			echo '<li><a class="member-tooltip" data-placement="top" title="'.$item->member_code.'
			<div>'.$item->member_name.'</div>"><i class="fa fa-user"></i>';
			echo '<div class="tree-code">'.$item->member_code.'</div>
			<div class="tree-name">'.$item->member_name.'</div>
			<div class="tree-sponsor">Sponsor : '.$item->member_sponsor.'</div>';
			echo '</a>';
			#if($level<4){
				$this->showtree2($item->member_code,$level);
			#}
			echo '</li>';
		}
		if($rs->num_rows()>0){
			echo '</ul>';
		}
	}
	
	function &showtreedata($id='',$level=1,&$ret){
		$rs= $this->db->query("select member_code,member_name,date_format(member_joindate,'%b') as member_joinmonth,
		date_format(member_joindate,'%Y') as member_joinyear,pack_id,ifnull(member_sponsor,member_code) as member_sponsor
		,member_upline
		from ngi_member where member_upline='".$id."'");
		$level++;
		foreach($rs->result() as $item){
			$this->member_mod->getChild($item->member_code,1,1,$count);
			$item->kiri = count($count[1]);
			$item->kanan = count($count[2]);
			$ret[$level][$id][] = $item;
			if($level<5){
				$this->showtreedata($item->member_code,$level,$ret);
			}
		}
		return $ret;
	}
	
	
}