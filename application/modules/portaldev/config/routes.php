<?php 
$route['(portaldev)/berita']	= "page/load/berita";
$route['(portaldev)/berita/(:any)']	= "page/load/berita/$1";
$route['(portaldev)/all_berita']	= "page/load/all_berita";
$route['(portaldev)/jobs/(:any)'] = "page/load/jobs/$1";
$route['(portaldev)/profile/(:any)'] = "page/load/profile/$1";
$route['(portaldev)/company/(:any)'] = "page/load/company/$1";
$route['(portaldev)/register']	= "page/load/register";
$route['(portaldev)/caripekerjaan']	= "page/load/caripekerjaan";
$route['(portaldev)/pengaturan']	= "page/load/pengaturan";
$route['(portaldev)/lamaran']	= "page/load/lamaran";
$route['(portaldev)/subpage']	= "page/load/subpage";
$route['(portaldev)/register_perusahaan']	= "page/load/register_perusahaan";
$route['(portaldev)/login']	= "page/load/login";
$route['(portaldev)/profile'] = "page/load/profile";
$route['(portaldev)/all_berita/(:any)']	= "page/load/all_berita/$1";
$route['(portaldev)/all_berita']	= "page/load/all_berita/1";
$route['(portaldev)/pengumuman']	= "page/load/pengumuman";
$route['(portaldev)/lengkapilampiran']	= "page/load/lengkapilampiran";
$route['(portaldev)/absen']	= "page/load/absen";
$route['(portaldev)/verifikasi']	= "page/load/verifikasi";
$route['(portaldev)/lupapassword']	= "page/load/lupapassword";

$route['(portaldev)/cetakkartupeserta']	= "page/load/cetakkartupeserta";
