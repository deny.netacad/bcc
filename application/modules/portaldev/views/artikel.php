
 <style>
 .w3_whatsapp_btn {
    background-image: url('icon.png');
    border: 1px solid rgba(0, 0, 0, 0.1);
    display: inline-block !important;
    position: relative;
    font-family: Arial,sans-serif;
    letter-spacing: .4px;
    cursor: pointer;
    font-weight: 400;
    text-transform: none;
    color: #fff;
    border-radius: 2px;
    background-color: #5cbe4a;
    background-repeat: no-repeat;
    line-height: 1.2;
    text-decoration: none;
    text-align: left;
}

.w3_whatsapp_btn_small {
    font-size: 12px;
    background-size: 16px;
    background-position: 5px 2px;
    padding: 3px 6px 3px 25px;
}

.w3_whatsapp_btn_medium {
    font-size: 16px;
    background-size: 20px;
    background-position: 4px 2px;
    padding: 4px 6px 4px 30px;
}

.w3_whatsapp_btn_large {
    font-size: 16px;
    background-size: 20px;
    background-position: 5px 5px;
    padding: 8px 6px 8px 30px;
    color: #fff;
}   

a.whatsapp { color: #fff;}
    #image_div{
  
	}

.cover{
     object-fit: cover;
     height: 275px !important;
}
</style>
 <div class="main">
   <?php
                     $rs = $this->db->query("select a.*,c.nama,date_format(a.art_date,'%b %d, %Y') as tmstamp_ from section_article a
                     left join mast_user c on a.iduser=c.iduser
                     where a.art_seo='".$this->uri->segment(4)."'");
                     $item = $rs->row();
                     if($rs->num_rows()!=0){
                          if($item->art_thumb!=""){
                      $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                      $src = $xpath->evaluate("string(//img/@src)");
                      $src=str_replace('.thumbs/','',$src);
                     }
                       
                     ?>
                    
                <div class="page-header larger parallax custom" style="background-image:url(<?=base_url()?>template/simple/assets/images/backgrounds/index-restaurant/bg.jpg)">
                    <div class="container">
                        <h1>ARTIKEL</h1>
                        <ol class="breadcrumb">
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li><a href="#">Pages</a></li>
                            <li class="active">Artikel</li>
                        </ol>
                    </div><!-- End .container -->
                </div><!-- End .page-header -->

                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3">
                            <article class="entry">
							  <?php 
                            if($item->art_thumb!=""){
                            ?>
                                <div class="entry-media">
                                    <figure>
                                        <img src="<?=$src?>" alt="entry image">
                                    </figure>
                                </div><!-- End .enty-media -->
							<?php }?>
                               <h2 class="entry-title"><?=$item->art_title?></h2>
                                <div class="entry-meta">
                                    <span><i class="fa fa-calendar"></i>  <?=$item->tmstamp_?>, </span>
                                    <a href="#"><i class="fa fa-user"></i>  <?=$item->nama?></a>
                                </div><!-- End .entry-meta -->
								</br>
								<div style="width:100%; class="entry-content">
								<p>
                                  <?=$item->art_content?>
								</p>
                                </div><!-- End .entry-content -->
							<?php }?>
                                
                               <!-- begin wwww.htmlcommentbox.com -->
 <div id="HCB_comment_box"><a href="http://www.htmlcommentbox.com">Daftar Komentar</a> sedang memuat komentar...</div>
 <link rel="stylesheet" type="text/css" href="//www.htmlcommentbox.com/static/skins/bootstrap/twitter-bootstrap.css?v=0" />
 <script type="text/javascript" id="hcb"> /*<!--*/ if(!window.hcb_user){hcb_user={};} (function(){var s=document.createElement("script"), l=hcb_user.PAGE || (""+window.location).replace(/'/g,"%27"), h="//www.htmlcommentbox.com";s.setAttribute("type","text/javascript");s.setAttribute("src", h+"/jread?page="+encodeURIComponent(l).replace("+","%2B")+"&mod=%241%24wq1rdBcg%24aYVVAe3znKaMuqx%2Fgqppt."+"&opts=16862&num=10&ts=1539252576133");if (typeof s!="undefined") document.getElementsByTagName("head")[0].appendChild(s);})(); /*-->*/ </script>
<!-- end www.htmlcommentbox.com -->
                                    <div class="author-content">
                                        <div class="social-icons">
                                            <label>Find Us:</label>
                                            <a href="#" class="social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                                            <a href="#" class="social-icon" title="Twitter"><i class="fa fa-twitter"></i></a>
                                            <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                                            <a href="#" class="social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                                            <a href="#" class="social-icon" title="Tumblr"><i class="fa fa-tumblr"></i></a>
                                            <a href="#" class="social-icon" title="Flickr"><i class="fa fa-flickr"></i></a>
                                        </div><!-- End .social-icons -->
                                    </div><!-- end .author-content -->
                         
                            </article>

                             <div class="single-related-posts">
                                <h3 class="title mb25">Related Posts</h3>
                                <div class="blog-related-carousel owl-carousel">
                                      <?php
                       $rs = $this->db->query("select a.*,c.nama,date_format(a.art_date,'%b %d, %Y') as tmstamp_ from section_article a
left join mast_user c on a.iduser=c.iduser
where flag=1 and position= 2 and art_thumb !='' and art_intro !='' order by rand() limit 9");
                        foreach($rs->result() as $item){
                        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                        $src = $xpath->evaluate("string(//img/@src)");
                        $src=str_replace('.thumbs','',$src);
                        //  $url = (($item->apps_link!='')?$item->apps_link:base_url().'portal/aplikasi/'.$item->apps_seo);
                        ?>
                                    <article class="entry entry-grid">
                                        <div class="entry-media">
                                            <figure>
                                                <a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>"><img src="<?=$src?>" alt="Gambar <?=$item->art_title?>" width=414 height=275 class="cover" style="background-color: white"></a>
                                            </figure>
                                            <div class="entry-meta">
                                                <span><i class="fa fa-calendar"></i><?=$item->tmstamp_?></span>
                                                <a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" class="counting" name="<?=$item->art_id?>"><i class="fa fa-user"></i> <?=$item->nama?></a>
                                            </div><!-- End .entry-media -->
                                        </div><!-- End .entry-media -->
                                        <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" class="counting" name="<?=$item->art_id?>"><?=$item->art_title?></a></h2>
                                        <div class="entry-content">
                                            <p><?=($item->art_intro)?></p>
                                            <a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" class="readmore counting" name="<?=$item->art_id?>">Read more<i class="fa fa-angle-right"></i></a>
                                        </div><!-- End .entry-content -->
                                    </article>

                                   
                                     <?php } ?>
                                </div><!-- End .blog-related-carousel -->
                            </div><!-- End .post-related-posts -->

                        </div><!-- End .col-md-9 -->

                        <aside class="col-md-3 col-md-pull-9 sidebar">
                            <div class="widget search-widget">
                                <form action="<?=base_url()?>portal/page/cari_artikel/" method="post">
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
                                    <input type="search" class="form-control" name="keyword" placeholder="Cari artikel" autocomplete="off" required>
                                    <button type="submit" class="btn btn-link"><i class="fa fa-search"></i></button>
                                </form>
                            </div><!-- End .widget -->

                            <div class="widget">
                                <h3 class="widget-title">Paling Banyak Dilihat</h3>
                              
                                <ul class="posts-list">
                                      <?php
                      /*  $rs = $this->db->query("select a.*,c.nama,date_format(a.art_date,'%b %d, %Y') as tmstamp_ from section_article a
												left join mast_user c on a.iduser=c.iduser
												where flag=1 and position= 1 and art_thumb !='' and art_intro !='' order by art_date desc limit 3"); */
												$rs = $this->db->query("select a.*,c.nama,date_format(b.art_date,'%b %d, %Y') as tmstamp_, b.* from (SELECT *, count(art_id) as jml_klik FROM `utin_popular_art` group by art_id order by jml_klik desc) a inner join section_article b on a.art_id = b.art_id left join mast_user c on b.iduser=c.iduser where b.position=1 and b.flag=1 limit 3");
                        foreach($rs->result() as $item){
                        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                        $src = $xpath->evaluate("string(//img/@src)");
                        $src=str_replace('.thumbs','',$src);
                        //  $url = (($item->apps_link!='')?$item->apps_link:base_url().'portal/aplikasi/'.$item->apps_seo);
                        ?>
                                    <li>
                                        <figure>
                                            <a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" title="<?=$item->art_title?>" class="counting" name="<?=$item->art_id?>"><img src="<?=$src?>" class="cover2" alt="Post"></a>
                                        </figure>
                                        <h5 class="judul2"><a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" class="counting" name="<?=$item->art_id?>"><?=$item->art_title?></a></h5>
                                        <span><i class="fa fa-calendar"></i><?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($item->tmstamp)); ?> &nbsp <i class="fa fa-eye"></i> <?=$item->jml_klik?> kali</span>
										<a href="#"><i class="fa fa-user"></i> <?=$item->nama?></a>
                                    </li>
                                  <?php } ?>
                                </ul>
                               
                            </div>

                          <!-- <div class="widget flickr-widget">
                                <h3 class="widget-title">Instagram Disperindag</h3>
                                <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/03de240df6365240bf8a25e25a28bfad.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
                            </div> -->
							
							  <div class="widget flickr-widget">
								<div class="widget">

								<a class="twitter-timeline" data-width="370" data-height="280" href="https://twitter.com/ketahananpan9an">Tweets by Distapang Kota Semarang</a> 
								<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

								</div>                              
							  </div><!-- End .widget -->
                          
                        </aside>
                    </div><!-- end .row -->
                </div><!-- End .container -->
            </div><!-- End .main -->