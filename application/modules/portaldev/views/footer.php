<style>
.ss_download_wrapper_details h1 {
    font-size: 38px;
}
.jp_downlord_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: #184981;
}
</style>

    <div class="jp_counter_main_wrapper" style="margin-bottom: 30px;">
        <div class="container">
            <div class="gc_counter_cont_wrapper" style="background: #184981;">
                <div class="count-description">
                    <span class="timer"><?=
					$this->db->query('SELECT SUM(jml_pekerja) AS jml FROM ngi_joblist WHERE NOW() < DATE_ADD(date_end, INTERVAL 1 DAY)')->row()->jml
					?></span><!-- <i class="fa fa-plus"></i> -->
                    <h5 class="con1">Lowongan Tersedia</h5>
                </div>
            </div>
            <div class="gc_counter_cont_wrapper2" style="background: #1d579a;">
                <div class="count-description">
                    <span class="timer"><?=$this->db->query('select iduser from user_pelamar')->num_rows()?></span><!-- <i class="fa fa-plus"></i> -->
                    <h5 class="con2">Pencaker Terdaftar</h5>
                </div>
            </div>
            <div class="gc_counter_cont_wrapper3" style="background: #184981;">
                <div class="count-description">
                    <span class="timer"><?=$this->db->query('select id from ngi_joblist')->num_rows()?></span><!-- <i class="fa fa-plus"></i> -->
                    <h5 class="con3">Pekerjaan Dibuka</h5>
                </div>
            </div>
            <div class="gc_counter_cont_wrapper4" style="background: #1d579a;">
                <div class="count-description">
                    <span class="timer"><?=$this->db->query('select idperusahaan from ngi_perusahaan')->num_rows()?></span><!-- <i class="fa fa-plus"></i> -->
                    <h5 class="con4">Perusahaan Terdaftar</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="jp_downlord_main_wrapper">
        <div class="jp_downlord_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    <div class="jp_down_mob_img_wrapper">
                        <img src="<?=base_url()?>template/HTML/job_dark/images/content/mobail3.png" alt="mobail_img" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="ss_download_wrapper_details">
                        <h1><span>Bogor</span> Career Center</h1>
						<p>daftar kerja mudah cukup scan</p>
                        <!-- <p>Fast, Simple & Delightful.</p> -->
                        <!--<a href="#" class="ss_appstore"><span><i class="fa fa-apple" aria-hidden="true"></i></span> App Store</a>
                        <a href="#" class="ss_playstore"><span><i class="fa fa-android" aria-hidden="true"></i></span> Play Store</a>-->
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 visible-sm visible-xs">
                    <div class="jp_down_mob_img_wrapper">
                        <img src="<?=base_url()?>template/HTML/job_dark/images/content/mobail3.png" class="img-responsive" alt="mobail_img" />
                    </div>
                </div>
            </div>
        </div>
    </div>