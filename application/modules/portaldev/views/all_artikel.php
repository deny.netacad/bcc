  <?php
    $row = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.position= 2 and a.art_thumb !='' and a.art_intro !='' order by a.art_date desc")->num_rows();
    $maxrow = (int)($row/3) + 1;
    $page = $this->uri->segment(4);
    $range = ($page - 1)*3;

    if ($page == ''){
        header("Location: /v3/portal/page/all_artikel/1");
        exit;
    }

    if ($page > $maxrow){
        header("Location: /v3/portal/page/all_artikel/$maxrow");
        exit;
    }else if($page == $maxrow){
         $paging = ($range+1)." - ".($row);
    }else{
         $paging = ($range+1)." - ".($page*6);
    }
 ?>    

<div class="main">
    <div class="page-header larger parallax custom" style="background-image:url(<?=base_url()?>template/simple/assets/images/backgrounds/index-restaurant/bg.jpg)">
        <div class="container">
            <h1>ARTIKEL</h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url()?>">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active">Artikel</li>
            </ol>
        </div><!-- End .container -->
    </div><!-- End .page-header -->

    <div class="container">
        <div class="row">
            <div class="col-md-9">
				       <?php
        $rs = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.position= 2 and a.art_thumb !='' and a.art_intro !='' order by a.art_date desc limit $range, 3");
        foreach($rs->result() as $item){
        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
        $src = $xpath->evaluate("string(//img/@src)");
        $src=str_replace('.thumbs','',$src);
        //  $url = (($item->apps_link!='')?$item->apps_link:base_url().'portal/aplikasi/'.$item->apps_seo);
        ?>
                <article class="entry entry-list">
			
                    <div class="row">
                        <div class="col-md-6">
                            <div class="entry-media">
                                <figure>
                                    <a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>"><img src="<?=$src?>" alt="<?=$item->art_title?>" style="background-color: white;"></a>
                                </figure>
                                <div class="entry-meta">
                                    <span><i class="fa fa-calendar"></i><?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($item->tmstamp)); ?></span>
                                    <a href="#"><i class="fa fa-user"></i> <?=$item->nama?></a>
                                </div><!-- End .entry-media -->
                            </div><!-- End .entry-media -->
                        </div><!-- End .col-md-6 -->
                        <div class="col-md-6">
                            <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>"><?=$item->art_title?></a></h2>
                            <div class="entry-content">
                                <p><?=strtoupper($item->art_intro)?></p>
                                <a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                            </div><!-- End .entry-content -->
                        </div><!-- End .col-md-6 -->
                    </div><!-- End .row -->
                </article>

            <?php } ?>
                 <nav class="pagination-container">
                    <label>Showing: <?=$paging?> of <?php echo $row ?></label>
                    <?php if ($page == 1): ?>
                        <ul class="pagination">
                        <li class="active"><a href="<?=base_url()?>portal/page/all_artikel/1">1</a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/2">2</a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/3">3</a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/4">4</a></li>
                        <li>
                            <a href="<?=base_url()?>portal/page/all_artikel/<?=$page+1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                    </ul>
                    <?php elseif($page == 2): ?>
                        <ul class="pagination">
                        <li>
                            <a href="<?=base_url()?>portal/page/all_artikel/<?=$page-1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/1">1</a></li>
                        <li class="active"><a href="<?=base_url()?>portal/page/all_artikel/2">2</a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/3">3</a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/4">4</a></li>
                        <li>
                            <a href="<?=base_url()?>portal/page/all_artikel/<?=$page+1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                    </ul>
                <?php elseif($page == $maxrow): ?>
                        <ul class="pagination">
                        <li>
                            <a href="<?=base_url()?>portal/page/all_artikel/<?=$page-1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/<?=$page-3?>"><?=$page-3?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/<?=$page-2?>"><?=$page-2?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/<?=$page-1?>"><?=$page-1?></a></li>
                        <li class="active"><a href="<?=base_url()?>portal/page/all_artikel/<?=$page?>"><?=$page?></a></li>
                    </ul>
                    <?php else: ?>
                        <ul class="pagination">
                        <li>
                            <a href="<?=base_url()?>portal/page/all_artikel/<?=$page-1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/<?=$page-2?>"><?=$page-2?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/<?=$page-1?>"><?=$page-1?></a></li>
                        <li class="active"><a href="<?=base_url()?>portal/page/all_artikel/<?=$page?>"><?=$page?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_artikel/<?=$page+1?>"><?=$page+1?></a></li>
                        <li>
                            <a href="<?=base_url()?>portal/page/all_artikel/<?=$page+1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                    </ul>
                    <?php endif ?>
                </nav>
            </div><!-- End .col-md-9 -->

            <aside class="col-md-3 sidebar">
                <div class="widget search-widget">
                    <form action="<?=base_url()?>portal/page/cari_artikel/" method="post">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
                        <input type="search" class="form-control" name="keyword" placeholder="Cari artikel" autocomplete="off" required>
                        <button type="submit" class="btn btn-link"><i class="fa fa-search"></i></button>
                    </form>
                </div><!-- End .widget -->

              

                <div class="widget">
                                <h3 class="widget-title">Paling Banyak Dilihat</h3>
                              
                                <ul class="posts-list">
                                      <?php
                      /*  $rs = $this->db->query("select a.*,c.nama,date_format(a.art_date,'%b %d, %Y') as tmstamp_ from section_article a
												left join mast_user c on a.iduser=c.iduser
												where flag=1 and position= 1 and art_thumb !='' and art_intro !='' order by art_date desc limit 3"); */
												$rs = $this->db->query("select a.*,c.nama,date_format(b.art_date,'%b %d, %Y') as tmstamp_, b.* from (SELECT *, count(art_id) as jml_klik FROM `utin_popular_art` group by art_id order by jml_klik desc) a inner join section_article b on a.art_id = b.art_id left join mast_user c on b.iduser=c.iduser where b.position=1 and b.flag=1 limit 3");
                        foreach($rs->result() as $item){
                        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                        $src = $xpath->evaluate("string(//img/@src)");
                        $src=str_replace('.thumbs','',$src);
                        //  $url = (($item->apps_link!='')?$item->apps_link:base_url().'portal/aplikasi/'.$item->apps_seo);
                        ?>
                                    <li>
                                        <figure>
                                            <a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" title="<?=$item->art_title?>" class="counting" name="<?=$item->art_id?>"><img src="<?=$src?>" class="cover2" alt="Post"></a>
                                        </figure>
                                        <h5 class="judul2"><a href="<?=base_url()?>portal/page/artikel/<?=$item->art_seo?>" class="counting" name="<?=$item->art_id?>"><?=$item->art_title?></a></h5>
                                        <span><i class="fa fa-calendar"></i><?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($item->tmstamp)); ?> &nbsp <i class="fa fa-eye"></i> <?=$item->jml_klik?> kali</span>
										<a href="#"><i class="fa fa-user"></i> <?=$item->nama?></a>
                                    </li>
                                  <?php } ?>
                                </ul>
                               
                            </div>


               <div class="widget flickr-widget">
                    <h3 class="widget-title">Instagram Distapang Kota Semarang</h3>
                    <!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/a78c70ac66ad56c69959f08159d8afa6.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
					</div><!-- End .widget -->
				
				
				  <div class="widget flickr-widget">
								<div class="widget">

								<a class="twitter-timeline" data-width="370" data-height="280" href="https://twitter.com/ketahananpan9an">Tweets by Distapang Kota Semarang</a> 
								<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

								</div>                              
							  </div><!-- End .widget -->
              
            </aside>
        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End .main -->