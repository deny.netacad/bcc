<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/magnific-popup.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" /> 
<style>

 .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
} 
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: rgb(24, 73, 129);
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #184981;
    border-color: #184981;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    z-index: 2;
    color: #fff;
    border: 1px solid #184981;
    background-color: #184981;
    border-color: none;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_bottom_cont_wrapper {
    float: left;
    width: 100%;
    background: #f9f9f9;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-bottom: 1px solid #184981;
    border-top: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
.mainmenu ul li a {

    padding: 0px 7px 47px 10px;
    }
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_hiring_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_hiring_heading_wrapper h2:before{
    content:'';
    border:1px solid #184981;
    width:8px;
    position:absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
  
</style>

<script>
 $(".share-popup").click(function(){
    var window_size = "width=585,height=511";
    var url = this.href;
    var domain = url.split("/")[2];
    switch(domain) {
        case "www.facebook.com":
            window_size = "width=585,height=368";
            break;
        case "www.twitter.com":
            window_size = "width=585,height=261";
            break;
        case "plus.google.com":
            window_size = "width=517,height=511";
            break;
    }
    window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,' + window_size);
    return false;
});


</script>
<div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
			<?php
                     $rs = $this->db->query("SELECT a.*,b.username
					FROM section_article a
					LEFT JOIN mast_user b ON a.iduser=b.iduser
					WHERE a.menu=1");
                     $item = $rs->row();
                     if($rs->num_rows()!=0){
                          if($item->art_thumb!=""){
                      $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                      $src = $xpath->evaluate("string(//img/@src)");
                      $src=str_replace('.thumbs/','',$src);
                     }
                       
                     ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2><?=$item->art_title?></h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="#">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Tentang</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
				
					 <?php }?>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp blog_cate section Wrapper Start -->
    <div class="jp_blog_cate_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
					  <?php
                     $rs = $this->db->query("SELECT a.*,b.username
	FROM section_article a
	LEFT JOIN mast_user b ON a.iduser=b.iduser
	WHERE a.menu=1 ");
                     $item = $rs->row();
                     if($rs->num_rows()!=0){
                          if($item->art_thumb!=""){
                      $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                      $src = $xpath->evaluate("string(//img/@src)");
                      $src=str_replace('.thumbs/','',$src);
                     }
                       
                     ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_blog_cate_left_main_wrapper">
                                <div class="jp_first_blog_post_main_wrapper">
								<?php 
                            if($item->art_thumb!=""){
                            ?>
                                    <div class="jp_first_blog_post_img">
                                        <img src="<?=$src?>" class="img-responsive" alt="<?=$item->art_title?>" />
                                    </div>
									
							     <?php }?>

                                    <div class="jp_first_blog_post_cont_wrapper">
                                        <!--<ul>
                                            <li><a href="#"><i class="fa fa-calendar"></i> &nbsp;&nbsp;<?=$item->tmstamp_?></a></li>
                                            <li><a href="#"><i class="fa fa-user"></i> &nbsp;&nbsp;<?=$item->nama?></a></li>
                                        </ul>-->
                                        <h3><a href="#"><?=$item->art_title?></a></h3>
                                        <p><?=$item->art_content?></p>
                                    </div>
                                    
                                  
                               
                                </div>
                            </div>
                        </div>
						
					 <?php }?>
				
                    </div>
                </div>
              
            </div>
        </div>
    </div>
<script src="<?=base_url()?>template/HTML/job_dark/js/jquery.magnific-popup.js"></script>
<script src="<?=base_url()?>template/HTML/job_dark/js/custom_II.js"></script>