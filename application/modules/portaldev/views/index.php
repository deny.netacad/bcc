<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Bogor Career Center</title>
      <?php
    $rs = $this->db->query("select * from ref_seo");
    $seo = $rs->row();

    ?>
    <meta content="width=device-width, initial-scale=1.0" name="<?=$seo->title?>" />
    <meta name="description" content="<?=$seo->desc?>" />
    <meta name="keywords" content="<?=$seo->keyword?> " />
    <meta name="author" content="<?=$seo->title?>" />
    <meta name="MobileOptimized" content="320" />

    <!--srart theme style -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/owl.theme.default.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/flaticon.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive.css" />
	<!-- <link rel="stylesheet" href="<?=base_url()?>template/HTML/job_dark/js/jQuery-Plugin-For-Horizontal-Text-Scrolling-Simple-Marquee/css/marquee.css" />
	<link rel="stylesheet" href="<?=base_url()?>template/HTML/job_dark/js/jQuery-Plugin-For-Horizontal-Text-Scrolling-Simple-Marquee/css/example.css" />
 -->
<!-- 	<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" /> -->
	<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
	
    <link href="<?=base_url()?>assets/js/plugins/select2/select2.css" rel="stylesheet">

	
    <!-- favicon links -->
    <link rel="shortcut icon" type="<?=base_url()?>template/HTML/job_dark/image/png" href="<?=base_url()?>template/HTML/job_dark/images/header/favicon.ico" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_hiring_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_hiring_heading_wrapper h2:before{
    content:'';
    border:1px solid #184981;
    width:8px;
    position:absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
		text-transform: none !important;
		color: #000000;
		display: block;
		font-size: 14px;
		letter-spacing: 1px;
		padding: 0px 8px 47px 10px;
		text-transform: capitalize;
		font-family: 'Montserrat', sans-serif;
		-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
		position: relative;
		}
		.jp_footer_candidate ul li a {
		text-transform: none;
		}
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}
    </style>
	
</head>
<body class="telo">
       <div class="jp_top_header_img_wrapper" style="border-top: 3px solid #184981;">
        <!-- <div class="jp_slide_img_overlay" ></div> -->
        <div class="jp_slide_img_overlay" style="background: #184981eb;"></div>
        <div class="gc_main_menu_wrapper" style="background-color: white;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 hidden-xs hidden-sm full_width">
                        <div class="gc_header_wrapper">
                            <div class="gc_logo">
                                <a href="<?=base_url()?>"><img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:138px;height:75px;" title="Bogor Career Center" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>

                    <?php

                        if($this->session->userdata('is_login_pelamar')){
                            $grid = 'col-lg-9 col-md-10';

                            $profil = $this->db->query('select skill, ttg_anda, exp, photo from user_pelamar where iduser ="'.$this->session->userdata('id').'"')->result_array();
                            $last_edu = $this->db->query('select jurusan, tahun_lulus from user_pelamar where iduser ="'.$this->session->userdata('id').'"')->row();

                            $counter = 0;
                            foreach ($profil[0] as $key => $value) {
                                if($value == ""){
                                    $counter++;
                                }
                            }

                            if($last_edu->jurusan == "" || $last_edu->tahun_lulus == ""){
                                $counter++;
                            }
                            
                        }else{
                            $grid = 'col-lg-7 col-md-8';
                        }

                    ?>

                    <div class="<?=$grid?> col-sm-12 col-xs-12 center_responsive">
                        <div class="header-area hidden-menu-bar stick" id="sticker">
                            <!-- mainmenu start -->
                            <div class="mainmenu">
                                <!-- <div class="gc_right_menu">
                                    <ul>
                                        <li id="search_button">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_3" x="0px" y="0px" viewBox="0 0 451 451" style="enable-background:new 0 0 451 451;" xml:space="preserve"><g><path id="search" d="M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z" fill="#23c0e9"/></g></svg>
                                        </li>
                                        <li>
                                            <div id="search_open" class="gc_search_box">
                                                <input type="text" placeholder="Search here">
                                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                        </li>
                                    </ul>
                                </div> -->
                                <ul class="float_left">
                                    <li class="has-mega gc_main_navigation"><a href="<?=base_url()?>" class="gc_main_navigation">Beranda&nbsp;</a>
                                        <!-- mega menu start -->
                                       
                                    </li>
                                     <li class="has-mega gc_main_navigation"><a href="<?=base_url()?>portal/page/all_event" class="gc_main_navigation">Informasi Event&nbsp;</i></a>
                                        <!-- mega menu start -->
                                       
                                    </li>
                                    <li class="has-mega gc_main_navigation"><a href="<?=base_url()?>portal/page/all_berita" class="gc_main_navigation">Berita&nbsp;</i></a>
                                        <!-- mega menu start -->
                                       
                                    </li>

                                    <li class="parent gc_main_navigation"><a href="<?=base_url()?>portal/caripekerjaan" class="gc_main_navigation">Cari Lowongan Kerja&nbsp;</i></a>
                                        <!-- sub menu start -->
                                      
                                        <!-- sub menu end -->
                                    </li>
									<li class="has-mega gc_main_navigation"><a href="#" class="gc_main_navigation">  Informasi&nbsp;<i class="fa fa-angle-down"></i></a>
                                        <!-- mega menu start -->
                                        <ul>
                                            <li class="parent"><a href="<?=base_url()?>portal/page/tentang">Tentang</a></li>
                                            <li class="parent"><a href="<?=base_url()?>portal/page/kontak">Kontak Kami</a></li>
                                            <!-- <li class="parent"><a href="<?=base_url()?>portal/page/all_event">Informasi Event</a></li> -->
                                            <li class="parent"><a href="<?=base_url()?>../pelatihan" target="_blank">Informasi Pelatihan</a></li>
                                        </ul>
                                    </li>
                                    

                                    <!-- <li class="has-mega gc_main_navigation"><a href="<?=base_url()?>portal/page/kontak" class="gc_main_navigation">Kontak Kami&nbsp;</a> -->
                                        <!-- mega menu start -->
                                      
                                    <!-- </li> -->

                                    <?php if($this->session->userdata('is_login_pelamar')) { ?>
                                        <li class="has-mega gc_main_navigation"><a href="#" class="gc_main_navigation"> <?=$this->session->userdata('nama')?> &nbsp;<?=(($counter>0)? '<span class="badge btn-danger">Lengkapi profil</span>&nbsp;' : '')?> <i class="fa fa-angle-down"></i></a>
                                            <!-- mega menu start -->
                                            <ul>
                                                <li class="parent"><a href="<?=base_url()?>portal/profile">Profile</a></li>
                                                <li class="parent"><a href="<?=base_url()?>portal/lamaran">Lamaran</a></li>
                                                <li class="parent"><a href="<?=base_url()?>portal/pengaturan">Pengaturan&nbsp;<?=(($counter > 0)? '<span class="badge btn-danger">'.$counter.'</span>' : '')?></a></li>
                                                <li class="parent"><a href="<?=base_url()?>portal/page/logoutportal">Logout</a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>
                               
                            </div>
                            <!-- mainmenu end -->
                            <!-- mobile menu area start -->
                            <header class="mobail_menu">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="gc_logo">
                                                <a href="<?=base_url()?>"><img src="<?=base_url()?>assets/logo/logo_bcc_new.png" style="width:110px;height:55px;" class="center" alt="Logo" title="Grace Church"></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="cd-dropdown-wrapper">
                                                <a class="house_toggle" href="#0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.177 31.177" style="enable-background:new 0 0 31.177 31.177;" xml:space="preserve" width="25px" height="25px"><g><g><path class="menubar" d="M30.23,1.775H0.946c-0.489,0-0.887-0.398-0.887-0.888S0.457,0,0.946,0H30.23    c0.49,0,0.888,0.398,0.888,0.888S30.72,1.775,30.23,1.775z" fill="#184981"/></g><g><path class="menubar" d="M30.23,9.126H12.069c-0.49,0-0.888-0.398-0.888-0.888c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,8.729,30.72,9.126,30.23,9.126z" fill="#184981"/></g><g><path class="menubar" d="M30.23,16.477H0.946c-0.489,0-0.887-0.398-0.887-0.888c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,16.079,30.72,16.477,30.23,16.477z" fill="#184981"/></g><g><path class="menubar" d="M30.23,23.826H12.069c-0.49,0-0.888-0.396-0.888-0.887c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,23.43,30.72,23.826,30.23,23.826z" fill="#184981"/></g><g><path class="menubar" d="M30.23,31.177H0.946c-0.489,0-0.887-0.396-0.887-0.887c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.398,0.888,0.888C31.118,30.78,30.72,31.177,30.23,31.177z" fill="#184981"/></g></g></svg>
                                                    </a>
                                                <nav class="cd-dropdown">
                                                    <h2><a href="#">Bursa<span>Kerja</span></a></h2>
                                                    <a href="#0" class="cd-close">Close</a>
                                                    <ul class="cd-dropdown-content">
                                                        <!-- <li>
                                                            <form class="cd-search">
                                                                <input type="search" placeholder="Search...">
                                                            </form>
                                                        </li> -->
                                                        <li class="">
                                                            <a href="<?=base_url()?>">Beranda</a>

                                                            <!-- .cd-secondary-dropdown -->
                                                        </li>
                                                        <!-- .has-children -->
                                                        <li>
                                                            <a href="<?=base_url()?>portal/page/all_event">Informasi Event</a>
                                                        </li>

                                                        <!-- <li class="has-children"> -->
                                                        <li>
                                                            <a href="<?=base_url()?>portal/page/all_berita">Berita</a>

                                                            
                                                            <!-- .cd-secondary-dropdown -->
                                                        </li>
                                                        <!-- .has-children -->
                                                        <!-- <li class="has-children"> -->
                                                        <li>
                                                            <a href="<?=base_url()?>portal/caripekerjaan">Cari Lowongan Kerja</a>

                                                            
                                                            <!-- .cd-secondary-dropdown -->
                                                        </li>
														 <li class="has-children">
                                                            <a href="#">Informasi</a>

                                                            <ul class="cd-secondary-dropdown is-hidden">
                                                                <li class="go-back"><a href="#0">Back</a></li>
                                                                <li>
                                                                    <a href="<?=base_url()?>portal/page/tentang">Tentang</a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?=base_url()?>portal/page/kontak">Kontak Kami</a>
                                                                </li>
                                                                <!-- .has-children -->
                                                                <!--  <li>
                                                                    <a href="<?=base_url()?>portal/page/all_event">Informasi Event</a>
                                                                </li> -->
                                                                <li>
                                                                    <a href="<?=base_url()?>../pelatihan" target="_blank">Informasi Pelatihan</a>
                                                                </li>
																
                                                                <!-- .has-children -->

                                                            </ul>
                                                            <!-- .cd-secondary-dropdown -->
                                                        </li>
                                                        <!-- .has-children -->
                                                        <!-- <li class="has-children"> -->
                                                        <!-- <li>
                                                            <a href="<?=base_url()?>portal/page/kontak">Kontak Kami</a> -->

                                                            
                                                            <!-- .cd-secondary-dropdown -->
                                                        <!-- </li> -->

                                                        <?php if($this->session->userdata('is_login_pelamar')) { ?>
                                                           <li class="has-children">
                                                                <a href="#"><?=$this->session->userdata('nama')?> &nbsp;<?=(($counter>0)? '<span class="badge btn-danger">Lengkapi profil</span>&nbsp;' : '')?></a>

                                                                <ul class="cd-secondary-dropdown is-hidden">
                                                                    <li class="go-back"><a href="#0">Menu</a></li>
                                                                    <li><a href="<?=base_url()?>portal/profile">Profile</a></li>
                                                                    <li><a href="<?=base_url()?>portal/lamaran">Lamaran</a></li>
                                                                    <li><a href="<?=base_url()?>portal/pengaturan">Pengaturan&nbsp;<?=(($counter > 0)? '<span class="badge btn-danger">'.$counter.'</span>' : '')?></a></li>
                                                                    <li><a href="<?=base_url()?>portal/page/logoutportal">Logout</a></li>
                                                                    <!-- .has-children -->
                                                                </ul>
                                                                <!-- .cd-secondary-dropdown -->
                                                            </li>
                                                        <?php }else{ ?>
                                                              <li class="has-children">
                                                            <a href="#">Daftar</a>

                                                            <ul class="cd-secondary-dropdown is-hidden">
                                                                <li class="go-back"><a href="#0">Back</a></li>
                                                                <li>
                                                                    <a href="<?=base_url()?>portal/page/register">Daftar Pencaker</a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?=base_url()?>portal/page/register_perusahaan">Daftar Perusahaan</a>
                                                                </li>
                                                                <!-- .has-children -->
                                                                
                                                                <!-- .has-children -->

                                                            </ul>
                                                            <!-- .cd-secondary-dropdown -->
                                                        </li>
                                                            <li>
                                                                <a href="<?=base_url()?>portal/login">Login</a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <!-- .cd-dropdown-content -->



                                                </nav>
                                                <!-- .cd-dropdown -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- .cd-dropdown-wrapper -->
                            </header>
                        </div>
                    </div>
                    <!-- mobile menu area end -->
                    <?php if($this->session->userdata('is_login_pelamar')) { ?>
                       
                    <?php }else{ ?>

                         <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                            <div class="jp_navi_right_btn_wrapper">
                                <ul>

                                   <!--  <li><a href="<?=base_url()?>portal/register"><i class="fa fa-user"></i>&nbsp; DAFTAR</a></li> -->
                                    <li><a href="<?=base_url()?>portal/login"><i class="fa fa-sign-in"></i>&nbsp; LOGIN</a></li>
                                </ul>
                                <ul class="dropdown">
                                  <button class="dropbtn"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; DAFTAR</button>
                                  <div class="dropdown-content">
                                    <a href="<?=base_url()?>portal/page/register">Daftar Pencaker</a>
                                    <a href="<?=base_url()?>portal/page/register_perusahaan">Daftar Perusahaan</a>
                                  </div>
                                </ul>
                            </div>
                        </div>

                    <?php } ?>
                </div>
            </div>
        </div>

        <?php
            if(!isset($page) || $page == "caripekerjaan"){
                $this->load->view($this->modules.'/header');
            }
        ?>
    </div>

    <?php
        if(!isset($page)){
            $this->load->view($this->modules.'/body');
            $this->load->view($this->modules.'/default');
            $this->load->view($this->modules.'/footer');
        }else{
            $this->load->view($this->modules.'/'.$page);
        }
    ?>

    <!-- jp downlord Wrapper End -->
    <!-- jp Newsletter Wrapper Start -->
    <div class="jp_main_footer_img_wrapper">
        <div class="jp_newsletter_img_overlay_wrapper"></div>
        <!-- <div class="jp_newsletter_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="jp_newsletter_text">
                            <img src="<?=base_url()?>template/HTML/job_light/images/content/news_logo.png" class="img-responsive" alt="news_logo" />
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="jp_newsletter_field">
                            <i class="fa fa-envelope-o"></i><input type="text" placeholder="Enter Your Email"><button type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- jp Newsletter Wrapper End -->
        <!-- jp footer Wrapper Start -->
        <div class="jp_footer_main_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_footer_logo_wrapper">
                            <div class="jp_footer_logo">
                                <!-- <a href="#"><img src="<?=base_url()?>template/HTML/job_light/images/content/resume_logo.png" alt="footer_logo"/></a> -->
                            </div>
                        </div>
                    </div>
                    <div class="jp_footer_three_sec_main_wrapper">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_candidate">
                                <?php
									$rs = $this->db->query("select * from section_contact ");
									$item = $rs->row();

									?>
								<div class="jp_footer_first_cont">
									
									<h2><?=$item->office_name?></h2>
                                   <ul>
                                        <li><a href="#" style="font-size:14px;"><i class="fa fa-map-marker" aria-hidden="true"></i><?=$item->addr?>,<?=$item->city?>,<?=$item->state?></a></li>
                                        <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i><?=$item->phone?></a></li>
										<li><a href="#"><i class="fa fa-fax" aria-hidden="true"></i><?=$item->fax?></a></li>
										<li><a href="#" style="font-size:14px;"><i class="fa fa-envelope"  aria-hidden="true"></i><?=$item->email?></a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_candidate_wrapper jp_footer_candidate_wrapper2">
                                <div class="jp_footer_candidate">
                                    <h2>Pencaker</h2>
                                    <ul>
                                        <li><a href="<?=base_url()?>portal/register"><i class="fa fa-caret-right" aria-hidden="true"></i> Add a Resume</a></li>
                                        <li><a href="<?=base_url()?>portal/register"><i class="fa fa-caret-right" aria-hidden="true"></i> candidate Dashboard</a></li>
                                        <li><a href="<?=base_url()?>portal/login"><i class="fa fa-caret-right" aria-hidden="true"></i> My Account</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_candidate_wrapper jp_footer_candidate_wrapper3">
                                <div class="jp_footer_candidate">
                                    <h2>Perusahaan</h2>
                                    <ul>
                                        <li><a href="<?=base_url()?>portal/register_perusahaan"><i class="fa fa-caret-right" aria-hidden="true"></i> Employer Dashboard</a></li>
                                        <li><a href="<?=base_url()?>portal/register_perusahaan"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Job</a></li>
                                        <li><a href="<?=base_url()?>portal/register_perusahaan"><i class="fa fa-caret-right" aria-hidden="true"></i> Job Page</a></li>
                                        <li><a href="<?=base_url()?>portal/login"><i class="fa fa-caret-right" aria-hidden="true"></i> My Account</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="jp_footer_candidate_wrapper jp_footer_candidate_wrapper4">
                                <div class="jp_footer_candidate">
                                    <h2>Information</h2>
                                    <ul>
                                        <li><a href="<?=base_url()?>portal/page/tentang"><i class="fa fa-caret-right" aria-hidden="true"></i> About Us</a></li>
                                        <li><a href="<?=base_url()?>portal/page/kontak"><i class="fa fa-caret-right" aria-hidden="true"></i> Contact</a></li>
                                      <!--   <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Terms & Conditions</a></li>
                                        <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Lokasi Kami</a></li> -->
                                        
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_bottom_footer_Wrapper">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="jp_bottom_footer_left_cont">
                                        <p>© 2019 Dinas Tenaga Kerja Kabupaten Bogor. All Rights Reserved.</p>
                                    </div>
                                    <div class="jp_bottom_top_scrollbar_wrapper">
                                        <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="jp_bottom_footer_right_cont">
                                        <ul>
                                            <li><a href="<?=$item->fb?>"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="<?=$item->twitter?>"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="<?=$item->instagram?>"><i class="fa fa-instagram"></i></a></li>
                                           <!--  <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li class="hidden-xs"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="hidden-xs"><a href="#"><i class="fa fa-vimeo"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp footer Wrapper End -->
    <!--main js file start-->
    <script src="<?=base_url()?>template/HTML/job_light/js/jquery_min.js"></script>
    <script src="<?=base_url()?>template/HTML/job_light/js/bootstrap.js"></script>
    <script src="<?=base_url()?>template/HTML/job_light/js/jquery.menu-aim.js"></script>
    <script src="<?=base_url()?>template/HTML/job_light/js/jquery.countTo.js"></script>
    <script src="<?=base_url()?>template/HTML/job_light/js/jquery.inview.min.js"></script>
    <script src="<?=base_url()?>template/HTML/job_light/js/owl.carousel.js"></script>
    <script src="<?=base_url()?>template/HTML/job_light/js/modernizr.js"></script>
    <script src="<?=base_url()?>template/HTML/job_light/js/custom.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/plugins/select2/select2.js"></script>
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>template/HTML/job_dark/js/jQuery-Plugin-For-Horizontal-Text-Scrolling-Simple-Marquee/js/marquee.js"></script>
 -->
    <!--main js file end-->
</body>

</html>
