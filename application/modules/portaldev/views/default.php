<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<style type="text/css">
  .jp_job_post_keyword_wrapper li i {
    padding-right: 5px;
    color: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
@media only screen and (max-device-width : 375px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 100%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}
@media only screen and (max-device-width : 411px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 100%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}
@media only screen and (max-device-width : 414px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 100%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}
@media only screen and (max-device-width : 730px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 100%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
/*.jp_form_btn_wrapper li a {
    float: left;
    width: 300px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}*/
}
/*@media only screen and (max-device-width : 1024px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 15%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 114px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}*/

.jp_job_post_right_cont p {
    font-size: 16px;
    color: #797979;
    padding-top: 5px;
}
.jp_job_post_main_wrapper_cont:hover .jp_job_post_keyword_wrapper {
    border: 1px solid #184981;
    border-top: 0;
    background: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
    .jp_add_resume_wrapper {
    width: 100%;
    height: 100%;
    background-position: center 0;
    background-size: cover;
    background-repeat: no-repeat;
    position: relative;
    text-align: center;
    padding-left: 5px;
    padding-right: 5px;
}
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: #184981;
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #af2020;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.jp_add_resume_cont li a:hover{
    color: #184981;
    background-color: #23c0e9;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #ffffff;
    border: 1px solid #3b6392;
    background: #184981;
    -webkit-border-top-right-radius: 0;
    -moz-border-top-right-radius: 0;
    border-top-right-radius: 0;
    -webkit-border-top-left-radius: 0;
    -moz-border-top-left-radius: 0;
    border-top-left-radius: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.jp_job_post_right_cont li:first-child {
    color: #6f7e98;
}

.jp_hiring_content_wrapper img{
    width: 100px !important;
}

.company_img {
    width: 100px !important;
    height: 95px;
    object-fit: contain;
}

</style>
<!-- blog -->
<style type="text/css">
    .jp_blog_cate_main_wrapper{
    float:left;
    width:100%;
    background:#f9f9f9;
    padding-top:55px;
    padding-bottom:5px;
}
.jp_blog_cate_left_main_wrapper{
    float:left;
    width:100%;
}
.jp_first_blog_post_main_wrapper{
    float:left;
    width:100%;
}
.jp_first_blog_post_img{
    float:left;
    width:100%;
}
.jp_first_blog_post_cont_wrapper{
    float:left;
    width:100%;
    background:#ffffff;
    border-bottom: 1px solid #e9e9e9;
    border-left: 1px solid #e9e9e9;
    border-right: 1px solid #e9e9e9;
    padding-top:45px;
    padding-bottom:45px;
    padding-left:30px;
    padding-right:30px;
    border-top:0;
    margin-bottom: 25px;
}
.jp_first_blog_post_cont_wrapper ul{
    
}
.jp_first_blog_post_cont_wrapper li{
    float:left;
    margin-left:40px;
}
.jp_first_blog_post_cont_wrapper li a i{
    color:#af2020;
}
.jp_first_blog_post_cont_wrapper li:first-child{
    margin-left:0;
}
.jp_first_blog_post_cont_wrapper li a{
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_post_cont_wrapper li a:hover{
    color:#af2020;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_post_cont_wrapper h3{
    font-size:20px;
    color:#000000;
    float:left;
    width:100%;
    font-weight:bold;
    padding-top:30px;
    padding-bottom:25px;
}
.jp_first_blog_post_cont_wrapper h3 a{
    color:#000000;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_blog_cate_left_main_wrapper:hover .jp_first_blog_post_cont_wrapper h3 a{
    color:#af2020;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_blog_cate_left_main_wrapper:hover .jp_first_blog_bottom_cont_wrapper{
    border-bottom:1px solid #af2020;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_post_cont_wrapper p{
    float:left;
    width:100%;
}

.beritakecil{
    float:left;
    width:100%;
    background:#ffffff;
    border-bottom: 1px solid #e9e9e9;
    border-left: 1px solid #e9e9e9;
    border-right: 1px solid #e9e9e9;
    padding-top:10px;
    padding-bottom:15px;
    padding-left:15px;
    padding-right:15px;
    border-top:0;
    margin-bottom: 25px;
    height: 157px;


}

.bg_putih{
    float:left;
    width:100%;
    background:#ffffff;
    border-top: 1px solid #e9e9e9;
    border-left: 1px solid #e9e9e9;
    border-right: 1px solid #e9e9e9;
}

.beritakecil ul{
    
}
.beritakecil li{
    float:left;
    margin-left:40px;
    font-size: 11px;
}
.beritakecil li a i{
    color:#af2020;
}
.beritakecil li:first-child{
    margin-left:0;
}
.beritakecil li a{
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.beritakecil li a:hover{
    color:#af2020;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.beritakecil h3{
    font-size:16px;
    color:#000000;
    float:left;
    width:100%;
    font-weight:bold;
    padding-top:10px;
    padding-bottom:2px;
}
.beritakecil h3 a{
    font-size:14px;
    color:#000000;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_blog_cate_left_main_wrapper:hover .beritakecil h3 a{
    color:#af2020;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_blog_cate_left_main_wrapper:hover .jp_first_blog_bottom_cont_wrapper{
    border-bottom:1px solid #af2020;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.beritakecil p{
    float:left;
    width:100%;
    font-size: 14px;
    line-height: 18px;
    margin-top: 10px;
}

.jp_first_blog_bottom_cont_wrapper{
    float:left;
    width:100%;
    background:#f9f9f9;
    padding:20px;
    border:1px solid #e9e9e9;
    border-bottom:1px solid #184981;
    border-top:0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_post_cont_wrapper2{
    border-top:1px solid #e9e9e9;
}

.jp_blog_bottom_right_cont{
    float:left;
    width:75%;
}
.jp_blog_bottom_right_cont ul{
    float:right;
    margin-right: 15px;
}
.jp_blog_bottom_right_cont li{
    float:left;
    margin-left:10px;
}
.jp_blog_bottom_right_cont li:first-child{
    margin-left:0;
    font-weight:bold;
    color:#000000;
    margin-top: 7px;
}
.jp_blog_bottom_right_cont li a{
    float:left;
    width:25px;
    height:30px;
    margin-top: 3px;
    -webkit-border-radius:15px;
    -moz-border-radius:15px;
    border-radius:15px;
    background:transparent; 
    text-align:center;
    line-height:30px;
    color:#797979;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_blog_bottom_right_cont li a:hover{
    background:#184981; 
    color:#ffffff;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.jp_blog_bottom_right_cont p{
    margin:0;
}
.jp_blog_bottom_right_cont p a{
    font-size:20px;
    color:#af2020;
    float:right;
    margin-top: 8px;
}
.jp_blog_bottom_left_cont{
    float:left;
    width:25%;
}

.pager_wrapper{
    margin-top:20px;
    text-align:center;
}
.btnlainya {
    float: right;
    width: 100%;
    height: 30px;
    line-height: 30px;
    text-align: center;
    background: #174981;
    color: #ffffff;
    font-size: 12px;
    text-transform: uppercase;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
}

.blink_me {
  animation: blinker 1s linear infinite;
  color:white;

}


@keyframes blinker {
  50% {
    opacity: 0;
  }
}

/*@media screen and (min-width: 1200px) {
    .imgberitakecil {
        height: 150px;
    }
}
@media screen and (min-width: 992px) {
    .imgberitakecil {
        height: 130px;
    }
}
*/
/*responsive*/

</style>
<!-- end blog -->

<!-- Start Berita -->
<div class="jp_best_deals_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_best_deal_slider_main_wrapper">
                        <div class="jp_best_deal_heading_wrapper">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><h2>Berita</h2></div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <a href="<?=base_url()?>portal/page/all_berita/1" class="btnlainya">Lainya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jp_blog_cate_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <?php
                        $rs = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.position= 0 and a.art_thumb !='' and a.art_intro !='' order by a.tmstamp desc limit 1");
                        foreach($rs->result() as $item){
                        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                        $src = $xpath->evaluate("string(//img/@src)");
                        $src=str_replace('.thumbs','',$src);
                        ?>
                    <div class="jp_blog_cate_left_main_wrapper">
                        <div class="jp_first_blog_post_main_wrapper">
                            <div class="jp_first_blog_post_img bg_putih" align="center">
                                <img src="<?=$src?>" class="img-responsive imgberitabesar"  alt="<?=$item->art_title?>" />
                            </div>
                            <div class="jp_first_blog_post_cont_wrapper">
                                <ul>
                                    <li><a href="#"><i class="fa fa-calendar"></i> &nbsp;&nbsp;<?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($item->tmstamp)); ?></a></li>
                                    <li><a href="#"><i class="fa fa-user"></i> &nbsp;&nbsp;<?=$item->nama?></a></li>
                                </ul>
                                <h3><a href="<?=base_url()?>portal/page/berita/<?=$item->art_seo?>" name="<?=$item->art_id?>"><?=$item->art_title?></a></h3>
                                <p><?=($item->art_intro)?></p>
                            </div>
                        </div>
                    </div>
                      <?php $notlike= $item->art_id; } ?>
                </div>
                <div class="col-lg-3 col-md-3">
                    <?php
                        $rs = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.position= 0 and a.art_thumb !='' and a.art_intro !='' and a.art_id!=$notlike order by a.tmstamp desc limit 2");
                        foreach($rs->result() as $beritakcl){
                        $xpath = new DOMXPath(@DOMDocument::loadHTML($beritakcl->art_thumb));
                        $src = $xpath->evaluate("string(//img/@src)");
                        $src=str_replace('.thumbs','',$src);
                        ?>
                    <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> -->
                        <div class="jp_blog_cate_left_main_wrapper">
                            <div class="jp_first_blog_post_main_wrapper">
                                <div class="jp_first_blog_post_img bg_putih" align="center">
                                    <img src="<?=$src?>"  class="img-responsive imgberitakecil" alt="blog_img" />
                                </div>
                                <div class="beritakecil xs-none sm-none">
                                    <ul>
                                        <!-- <li><i class="fa fa-user"></i> &nbsp;&nbsp;<?=$item->nama?></li> -->
                                        <li><i class="fa fa-calendar"></i> <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($beritakcl->tmstamp)); ?></li>
                                    </ul>
                                    <h3><a href="<?=base_url()?>portal/page/berita/<?=$beritakcl->art_seo?>" name="<?=$beritakcl->art_id?>"><?=$beritakcl->art_title?></a></h3>
                                </div>
                                <div class="jp_first_blog_post_cont_wrapper lg-none md-none">
                                <ul>
                                    <li><a href="#"><i class="fa fa-calendar"></i> &nbsp;&nbsp;<?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($beritakcl->tmstamp)); ?></a></li>
                                    <li><a href="#"><i class="fa fa-user"></i> &nbsp;&nbsp;<?=$beritakcl->nama?></a></li>
                                </ul>
                                <h3><a href="<?=base_url()?>portal/page/berita/<?=$beritakcl->art_seo?>" name="<?=$beritakcl->art_id?>"><?=$beritakcl->art_title?></a></h3>
                                <p><?=($beritakcl->art_intro)?></p>
                            </div>
                            </div>
                        </div>
                    <!-- </div> -->
                      <?php } ?>
                </div>
            </div>
        </div>
    </div>
<!-- end Berita -->
    <!-- Start event -->
    <div class="jp_best_deals_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_best_deal_slider_main_wrapper">
                        <div class="jp_best_deal_heading_wrapper">
                            <h2>Event</h2>
                        </div>
                        <div class="jp_best_deal_slider_wrapper" style="margin-top: 25px">
                            <div class="owl-carousel owl-theme">
                                <div class="item">
                                    <div class="row">
                                         <?php
                                           $rs = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit 0,2 ");
                                            foreach($rs->result() as $event){
                                            $xpath = new DOMXPath(@DOMDocument::loadHTML($event->agenda_thumb));
                                            $src = $xpath->evaluate("string(//img/@src)");
                                            $src=str_replace('.thumbs','',$src);
                                        ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-top: 30px;">
                                            <div class="jp_job_post_main_wrapper_cont">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=$src?>" alt="post_img" />
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <a href="<?=base_url()?>portal/page/event/<?=$event->agenda_seo?>" name="<?=$event->art_id?>" class="counting"><h4><?=$event->agenda_title?></h4></a>
                                                                <P><i class="fa fa-calendar"></i> <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($event->tmstamp)); ?></P>
                                                                <?=($event->agenda_detail)?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i>&nbsp; <?=$event->nama?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php
                            $querylimit2 = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit 2,2");
                            if ($querylimit2->num_rows() != '0') {
                            
                             ?>
                                <div class="item">
                                    <div class="row">
                                         <?php
                                           $rs = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit 2,2 ");
                                            foreach($rs->result() as $event){
                                            $xpath = new DOMXPath(@DOMDocument::loadHTML($event->agenda_thumb));
                                            $src = $xpath->evaluate("string(//img/@src)");
                                            $src=str_replace('.thumbs','',$src);
                                        ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-top: 30px;">
                                            <div class="jp_job_post_main_wrapper_cont">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=$src?>" alt="post_img" />
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <a href="<?=base_url()?>portal/page/event/<?=$event->agenda_seo?>" name="<?=$event->art_id?>" class="counting"><h4><?=$event->agenda_title?></h4></a>
                                                                <P><i class="fa fa-calendar"></i> <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($event->tmstamp)); ?></P>
                                                                <?=($event->agenda_detail)?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i>&nbsp; <?=$event->nama?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php }
                            $querylimit4 = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit 2,2");
                            if ($querylimit4->num_rows() != '0') { ?>
                                <div class="item">
                                    <div class="row">
                                         <?php
                                           $rs = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit 4,2 ");
                                            foreach($rs->result() as $event){
                                            $xpath = new DOMXPath(@DOMDocument::loadHTML($event->agenda_thumb));
                                            $src = $xpath->evaluate("string(//img/@src)");
                                            $src=str_replace('.thumbs','',$src);
                                        ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-top: 30px;">
                                            <div class="jp_job_post_main_wrapper_cont">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=$src?>" alt="post_img" />
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <a href="<?=base_url()?>portal/page/event/<?=$event->agenda_seo?>" name="<?=$event->art_id?>" class="counting"><h4><?=$event->agenda_title?></h4></a>
                                                                <P><i class="fa fa-calendar"></i> <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($event->tmstamp)); ?></P>
                                                                <?=($event->agenda_detail)?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i>&nbsp; <?=$event->nama?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end event -->

    

<div class="jp_first_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_hiring_slider_main_wrapper">
                            <div class="jp_hiring_heading_wrapper">
                                <h2 style="font-size: 18px;">Perusahaan buka lowongan di kabupaten bogor terbaik </h2>
                            </div>
                            <div class="jp_hiring_slider_wrapper">
                                <div class="owl-carousel owl-theme">
                                    <?php 
                                        $q = $this->db->query('select a.profile_url, a.photo, a.alamat, a.nama_perusahaan, count(b.id) as jml  from ngi_perusahaan a 
                                        left join (select * from ngi_joblist where is_aktif = 1 
                                        and now() < date_add(date_end, interval 1 DAY)) b on a.`idperusahaan` = b.idperusahaan
                                        group by a.idperusahaan
                                        order by count(b.id) desc');
                                        $data = $q->result();

                                        foreach ($data as $item) {
                                    ?>
                                    <div class="item">
                                        <div class="jp_hiring_content_main_wrapper">
                                            <div class="jp_hiring_content_wrapper">
                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                <h4><?=((strlen($item->nama_perusahaan) > 10)? substr($item->nama_perusahaan,0,10)."..." : $item->nama_perusahaan)?></h4>
                                                <p>(<?=((strlen($item->alamat) > 15)? substr($item->alamat,0,15)."..." : $item->alamat)?>)</p>
                                                <ul>
                                                    <li><a href="<?=base_url()?>portal/company/<?=$item->profile_url?>"><?=$item->jml?> Opening</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="cc_featured_product_main_wrapper">
                            <div class="jp_hiring_heading_wrapper jp_job_post_heading_wrapper">
                                <h2 style="font-size: 18px;">Lowongan kerja di kabupaten bogor terbaru</h2>
                            </div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#best" aria-controls="best" role="tab" data-toggle="tab">Semuanya</a></li>
                                <li role="presentation"><a href="#trand" aria-controls="trand" role="tab" data-toggle="tab">Part Time</a></li>
                                <li role="presentation"><a href="#fulltime" aria-controls="fulltime" role="tab" data-toggle="tab">Full Time </a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="best">
                                <div class="ss_featured_products">
                                    <div class="">
                                        <div class="item" data-hash="zero">
                                            <?php 
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) ORDER BY log desc LIMIT 4");

                                                if($joblist->num_rows() > 0){
                                                foreach ($joblist->result() as $item) {

                                                $keyword = explode(',', $item->keyword);

                                            ?>
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?=$item->title?></h4>
                                                                <p><?=$item->nama_perusahaan?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-user"></i>&nbsp; <?=$item->nama_job?></li>
                                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                                </ul>
                                                                <ul>

                                                                </ul>
                                                                <ul>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                    <li><a href="#"> <?=$item->tipe?></a></li>
                                                                    <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <?php  foreach ($keyword as $kw) { ?>
                                                            <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>  
                                            <?php }}else{ ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_recent_resume_box_wrapper">
                                                    <h3 style="text-align: center; color: #184981;">Tidak ditemukan hasil</h3>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="trand">
                                <div class="ss_featured_products">
                                    <div class="owl-carousel owl-theme">
                                        <div class="item" data-hash="zero">
                                            <?php 
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) and a.tipe = 'part time' ORDER BY log desc LIMIT 5");

                                                if($joblist->num_rows() > 0){
                                                foreach ($joblist->result() as $item) {

                                                $keyword = explode(',', $item->keyword);

                                            ?>
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?=$item->title?></h4>
                                                                <p><?=$item->nama_perusahaan?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-user"></i>&nbsp; <?=$item->nama_job?></li>
                                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                                </ul>
                                                                <ul>

                                                                </ul>
                                                                <ul>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                    <li><a href="#"> <?=$item->tipe?></a></li>
                                                                    <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <?php  foreach ($keyword as $kw) { ?>
                                                            <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>  
                                            <?php }}else{ ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_recent_resume_box_wrapper">
                                                    <h3 style="text-align: center; color: #184981;">Tidak ditemukan hasil</h3>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="fulltime">
                                <div class="ss_featured_products">
                                    <div class="owl-carousel owl-theme">
                                        <div class="item" data-hash="zero">
                                            <?php 
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) and a.tipe = 'full time' ORDER BY log desc LIMIT 5");

                                                if($joblist->num_rows() > 0){
                                                foreach ($joblist->result() as $item) {

                                                $keyword = explode(',', $item->keyword);

                                            ?>
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?=$item->title?></h4>
                                                                <p><?=$item->nama_perusahaan?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-user"></i>&nbsp; <?=$item->nama_job?></li>
                                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                                </ul>
                                                                <ul>

                                                                </ul>
                                                                <ul>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                    <li><a href="#"> <?=$item->tipe?></a></li>
                                                                    <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <?php  foreach ($keyword as $kw) { ?>
                                                            <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>  
                                            <?php }}else{ ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_recent_resume_box_wrapper">
                                                    <h3 style="text-align: center; color: white;">Tidak ditemukan hasil</h3>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="jp_first_right_sidebar_main_wrapper">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont blink_me">
                                    <img src="<?=base_url()?>assets/logo/logo_bcc_white.png" alt="Logo Bogor Career Center" style="width:138px;height:75px;" title="Bogor Career Center" >
                                      <?php
                                        $rs = $this->db->query("select narasi from section_contact");
                                        $seo = $rs->row();

                                        ?>
                                    <h4 style="font-size:14px;text-align:center;color: #f4f4f4;">
                                        <?=$seo->narasi?>
                                    </h4>
                                    <ul>
                                        <li><a href="<?=base_url()?>portal/register_perusahaan" style="font-size:12px;"><i class="fa fa-plus-circle"></i> &nbsp;DAFTAR PERUSAHAAN</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_spotlight_main_wrapper">
                                <div class="spotlight_header_wrapper">
                                    <h4 style="font-size:12px;">Rekomendasi Lowongan <br>Kerja di Kabupaten Bogor </h4>
                                </div>
                                <div class="jp_spotlight_slider_wrapper">
                                    <div class="owl-carousel owl-theme">
                                    <?php 

                                            if($this->session->userdata('is_login_pelamar')){
                                                $person = $this->db->query('select skill from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();

                                                $skill = explode(',', $person->skill);

                                                $where = "";
                                                foreach ($skill as $s) {
                                                    $where .= " or a.skill like '%".$s."%'";
                                                }

                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ORDER BY RAND() LIMIT 5");
                                            }else{
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5");
                                            }

                                            if($joblist->num_rows() == 0 ){
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5");
                                            }

                                            foreach ($joblist ->result() as $item) {

                                            ?>
                                    
                                        <div class="item">
                                            <div class="jp_spotlight_slider_img_Wrapper">
                                                <img src="<?=base_url()?>template/HTML/job_light/images/content/spotlight.jpg" alt="spotlight_img" />
                                            </div>
                                            <div class="jp_spotlight_slider_cont_Wrapper">
                                                <h4><?=$item->nama_job?></h4>
                                                <p><?=$item->nama_perusahaan?></p>
                                                <ul>
                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->type?> <?=$item->city?>, <?=$item->province?></li>
                                                </ul>
                                            </div>
                                            <div class="jp_spotlight_slider_btn_wrapper">
                                                <div class="jp_spotlight_slider_btn">
                                                    <ul>
                                                        <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>"><i class="fa fa-plus-circle"></i> &nbsp;DETAIL</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }?>
                                    
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                    
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_spotlight_main_wrapper">
                                <div class="spotlight_header_wrapper">
                                    <h4 style="font-size:14px;">Informasi Pelatihan</h4>
                                </div>
                                <div class="jp_spotlight_slider_wrapper">
                                    <div class="owl-carousel owl-theme">
                                         <?php
                                        //atiati karo iki wa. mau mati kabeh sak halaman;
                               
                                        $this->load->database('default', FALSE);
                                        $this->db2 = $this->load->database('pelatihan', TRUE);
                                        
                                        $pelatihan = $this->db2->query("SELECT a.*,DATE_FORMAT(a.tgl_mulai,'%d %M') AS tglmulai,DATE_FORMAT(a.tgl_ahir,'%d %M %Y') AS tglakhir FROM ref_pelatihan a ");
                                        
                                        $this->load->database('pelatihan', FALSE);
                                        $this->load->database('default', TRUE);
                                        
                                       // echo("test");
                                        foreach ($pelatihan->result() as $items) {
                                        ?>
                                        <div class="item">
                                            <div class="jp_spotlight_slider_img_Wrapper">
                                                <img src="<?=base_url()?>template/HTML/job_dark/images/content/resume_bg_1.jpg" alt="spotlight_img" />
                                            </div>
                                            <div class="jp_spotlight_slider_cont_Wrapper">
                        
                                                <p><?=$items->judul?></p>
                                                <ul>
                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?=$items->tempat?></li>
                                                    <li><i class="fa fa-calendar-o"></i>&nbsp;  <?=$items->tglmulai?> s/d <?=$items->tglakhir?></li>
                                                </ul>
                                            </div>
                                            <div class="jp_spotlight_slider_btn_wrapper">
                                                <div class="jp_spotlight_slider_btn">
                                                    <ul>
                                                        <li><a href="<?=base_url()?>../pelatihan/portal/dip_detail/<?=$item->id?>"><i class="fa fa-plus-circle"></i> &nbsp;DETAIL</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }?>
                                    
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>