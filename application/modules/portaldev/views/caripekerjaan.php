  
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />
   <style type="text/css">
   .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
    .disabled{
        pointer-events:none; //This makes it not clickable
        opacity:0.6;  
    }

        .jp_add_resume_wrapper {
            width: 100%;
            height: 100%;
            background-position: center 0;
            background-size: contain;
            background-repeat: no-repeat;
            position: relative;
            text-align: center;
            padding-left: 30px;
            padding-right: 30px;
        }

    .jp_job_post_right_cont li:first-child {
        color: #6f7e98;
    }
     .jp_job_post_keyword_wrapper li i {
    padding-right: 5px;
    color: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_job_post_right_cont p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.jp_job_post_main_wrapper_cont:hover .jp_job_post_keyword_wrapper {
    border: 1px solid #184981;
    border-top: 0;
    background: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
    .jp_add_resume_wrapper {
    width: 100%;
    height: 100%;
    background-position: center 0;
    background-size: cover;
    background-repeat: no-repeat;
    position: relative;
    text-align: center;
    padding-left: 30px;
    padding-right: 30px;
}
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: #184981;
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #ffffff;
    border: 1px solid #3b6392;
    background: #184981;
    -webkit-border-top-right-radius: 0;
    -moz-border-top-right-radius: 0;
    border-top-right-radius: 0;
    -webkit-border-top-left-radius: 0;
    -moz-border-top-left-radius: 0;
    border-top-left-radius: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.jp_job_post_right_cont li:first-child {
    color: #6f7e98;
}

.jp_hiring_content_wrapper img{
    width: 100px !important;
}

.company_img {
    width: 100px !important;
    height: 95px;
    object-fit: contain;
}
	.mainmenu ul li a {

	padding: 0px 7px 47px 10px;
	}
    
    .jp_slide_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #12703b;
}   
 .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_hiring_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_hiring_heading_wrapper h2:before{
    content:'';
    border:1px solid #184981;
    width:8px;
    position:absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}
.jp_listing_heading_wrapper span {
    color: #184981;
}
.gc_causes_search_box p span {
    color: #184981;
}
.handyman_sec1_wrapper label span {
    color: #184981;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}
.jp_rightside_job_categories_heading {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
   </style>
   
    <div class="jp_listing_sidebar_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<?php 
					$where = "";
                    $wj = ""; 
                    $wc = ""; 
                    $ws = ""; 
                    $wt = "";
                    $sorting = ($_POST['sorting'] == '')? 'a.log desc' : $_POST['sorting'];

            		if ($_POST["city"] != ""){ 
                        $where .= " and d.city_id =  '".$_POST["city"]."'"; 
                    }else{
                        if($_POST["province"] != ""){
                             $where .= " and d.province_id =  '".$_POST["province"]."'"; 
                        }
                    }

            		if ($_POST["keyword"] != ""){ 
                        $where .= " and (a.keyword like  '%".$_POST["keyword"]."%' or c.nama_job like '%".$_POST["keyword"]."%' or a.skill like '%".$_POST["keyword"]."%' or a.title like '%".$_POST["keyword"]."%' )"; 
                        $this->db->insert('ngi_keyword',array('keyword' => $_POST['keyword']));
                    }

                    if ($_POST["jobs"] != ""){
                        $jobs = explode(",", $_POST["jobs"]);
                        $wj .= " and ( ";

                        foreach ($jobs as $item) {
                            $wj .= "a.posisi = '".trim($item,'j')."' or ";
                        }

                        $wj = substr($wj, 0, -3);
                        $wj .= " ) ";
                    }

                    if ($_POST["category"] != ""){
                        $ctgr = explode(",", $_POST["category"]);
                        $wc .= " and ( ";

                        foreach ($ctgr as $item) {
                            $wc .= "a.idcategory = '".trim($item,'c')."' or ";
                        }

                        $wc = substr($wc, 0, -3);
                        $wc .= " ) ";
                    }

                    if ($_POST["salary"] != ""){
                        $slr = explode(",", $_POST["salary"]);
                        $ws .= " and ( ";

                        foreach ($slr as $item) {
                            $ws .= " ( a.upah_kerja = ".trim($item,'s')." ) or ";
                        }

                        $ws = substr($ws, 0, -3);
                        $ws .= " ) ";
                    }


                    if ($_POST["type"] != ""){
                        $tp = explode(",", $_POST["type"]);
                        $wt .= " and ( ";

                        foreach ($tp as $item) {
                            $wt .= "a.tipe = '".$item."' or ";
                        }

                        $wt = substr($wt, 0, -3);
                        $wt .= " ) ";
                    }

                    $pages = ($_POST['page'] == '')? 1 : $_POST['page'];
                    $page = ($pages-1)*10;

            		// if ($_POST["location"] != ""){ $where .= " and idkecamatan =  '".$_POST["location"]."'"; }
            		$q1 = $this->db->query("select a.*, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.type, d.city, d.subdistrict_name, f.name as upah from ngi_joblist a 
            									left join ngi_perusahaan b on a.idperusahaan = b.idperusahaan
            									left join ngi_job c on a.posisi = c.id
            									left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id 
                                                left join ngi_jobcategory e on a.idcategory = e.id
												left join ngi_salary f on a.upah_kerja = f.id
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ".$wj." ".$wc." ".$ws." ".$wt."
                                                order by ".$sorting."
                                                limit ".$page.", 10");

                    $q2 = $this->db->query("select a.id from ngi_joblist a 
                                                left join ngi_perusahaan b on a.idperusahaan = b.idperusahaan
                                                left join ngi_job c on a.posisi = c.id
                                                left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id 
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ".$wj." ".$wc." ".$ws." ".$wt." ");

            		$joblist = $q1->result();
            		$jml = $q2->num_rows();
                    $max_page = ceil($jml/10);

                	 ?>
                    <div class="jp_listing_heading_wrapper">
                        <h2>Kami menemukan <span><?=$jml?></span> hasil untuk anda.</h2>
                    </div>
                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_listing_tabs_wrapper">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="gc_causes_select_box_wrapper">
                                        <div class="gc_causes_select_box">
                                            <select id="sorting">
												<option value="a.log desc">Urutkan terbaru</option>
												<option value="a.log asc">Urutkan terlama</option>
												<option value="c.nama_job asc">Urutkan alphabet</option>
											</select><i class="fa fa-angle-down"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 text-center" style="margin: 0 auto !important">
                                    <div class="gc_causes_search_box_wrapper gc_causes_search_box_wrapper2">
                                        <div class="gc_causes_search_box">
                                            <p>Anda melihat &nbsp;<span><?=($page+1)?> - <?=(($pages == $max_page)? $page+(($jml%10 == 0)? 10 : $jml%10) : $page+10)?> dari <?=$jml?> hasil</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tab-content">
                                <div id="grid" class="tab-pane fade in active">
                                    <div class="row">
                                    	<?php

                                            if($jml > 0){

                                    		foreach ($joblist as $item) {

                                    		$keyword = explode(',', $item->keyword);
                                    			
                                    	?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont">
                                                    <div class="jp_job_post_main_wrapper jp_job_post_grid_main_wrapper">
                                                        <div class="row">
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                <div class="jp_job_post_side_img">
                                                                    <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="tittle_img" style="width: 100px; height: 95px; object-fit: contain;" />
                                                                </div>
                                                                <div class="jp_job_post_right_cont" style="padding-top: 0px;">
                                                                    <h4><?=$item->title?></h4>
                                                                    <p><?=$item->nama_perusahaan?></p>
                                                                    <ul>
                                                                        <li><i class="fa fa-user"></i>&nbsp; <?=$item->nama_job?> &nbsp;  <span><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></span></li>
                                                                    </ul>
                                                                    <ul>
                                                                        
                                                                    </ul>
                                                                    <ul>
                                                                        <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                <div class="jp_job_post_right_btn_wrapper">
                                                                    <ul>
                                                                        <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                        <li><a href="#"> <?=$item->tipe?></a></li>
                                                                        <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="jp_job_post_keyword_wrapper">
                                                        <ul>
                                                            <li><i class="fa fa-tags"></i>Keywords :</li>
                                                            <?php  foreach ($keyword as $kw) { ?>
                                                                <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div> 
                                            </div>
                                        <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="pager_wrapper gc_blog_pagination">
                                                <ul class="pagination">
                                                    <?php if($_POST['page'] == '' || $_POST['page'] == '1') {?>
                                                        <li class="disabled"><a href="#">Priv</a></li>
                                                        <li class="active"><a href="#">1</a></li>
                                                        <?php for($i=2; $i<=min($max_page,3);$i++){ ?>
                                                            <li><a href="#" class="pages" id="<?=$i?>"><?=$i?></a></li>
                                                        <?php } if($max_page == '1'){?>
                                                            <li class="disabled"><a href="#">Next</a></li>
                                                        <?php }else {?>
                                                            <li><a href="#" class="pages" id="2">Next</a></li>
                                                        <?php }?>
                                                    <?php } else if($_POST['page'] == $max_page){ ?>
                                                        <li><a href="#" class="pages" id="<?=($max_page-1)?>">Priv</a></li>
                                                        <?php for($i=max($max_page-3,1); $i<=$max_page;$i++){ ?>
                                                            <li class="<?=(($i == $max_page)? "active" : "")?>"><a href="#" class="<?=(($i != $max_page)? "pages" : "")?>" id="<?=$i?>"><?=$i?></a></li>
                                                        <?php } ?>
                                                        <li class="disabled"><a href="#">Next</a></li>
                                                    <?php } else { ?>
                                                        <li><a href="#" class="pages" id="<?=($_POST['page']-1)?>">Priv</a></li>
                                                        <li><a href="#" class="pages" id="<?=($_POST['page']-1)?>"><?=($_POST['page']-1)?></a></li>
                                                        <li class="active"><a href="#" class="pages" id="<?=($_POST['page'])?>"><?=($_POST['page'])?></a></li>
                                                        <li><a href="#" class="pages" id="<?=($_POST['page']+1)?>"><?=($_POST['page']+1)?></a></li>
                                                        <li><a href="#" class="pages" id="<?=($_POST['page']+1)?>">Next</a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>

                                        <?php }else{?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="jp_recent_resume_box_wrapper">
                                                <h3 style="text-align: center; color: white;">Tidak ditemukan hasil</h3>
                                            </div>
                                        </div>

                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- category checked -->
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Pekerjaan</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <?php 
                                                    $jobs = $this->db->query("select a.*, count(b.id) as jml from ngi_job a
                                                                                left join (select a.*, d.* from ngi_joblist a
                                                                                left join ngi_job c on a.posisi = c.id
                                                                                left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                                                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where.") b on a.id = b.posisi
                                                                                group by a.id order by jml desc, a.nama_job asc")->result();

                                                    foreach ($jobs as $j ) {
                                                    $jobs_array = explode(",", $_POST["jobs"]);

                                                    if(in_array("j".$j->id, $jobs_array)){
                                                        $jobs_checked = "checked";
                                                    }else{
                                                        $jobs_checked = "";
                                                    }
                                                 ?>
                                                <p class="joblist">
                                                    <input type="checkbox" id="j<?=$j->id?>" name="jobs"  <?=$jobs_checked?> >
                                                    <label for="j<?=$j->id?>""><?=$j->nama_job?> <span>(<?=$j->jml?>)</span></label>
                                                </p>

                                                <?php } ?>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#" id="jobListBtn">Tampilkan lainnya</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Kategori</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <?php 
                                                    $category = $this->db->query("select a.*, count(b.id) as jml from ngi_jobcategory a
                                                                                left join (select a.*, d.* from ngi_joblist a
                                                                                left join ngi_job c on a.posisi = c.id
                                                                                left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                                                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where.") b on a.id = b.idcategory
                                                                                group by a.id order by jml desc")->result();

                                                    foreach ($category as $c ) {
                                                    $category_array = explode(",", $_POST["category"]);

                                                    if(in_array("c".$c->id, $category_array)){
                                                        $category_checked = "checked";
                                                    }else{
                                                        $category_checked = "";
                                                    }
                                                 ?>
                                                <p class="jobcategory">
                                                    <input type="checkbox" id="c<?=$c->id?>" name="category"  <?=$category_checked?> >
                                                    <label for="c<?=$c->id?>""><?=$c->category?> <span>(<?=$c->jml?>)</span></label>
                                                </p>

                                                <?php } ?>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#" id="jobCategoryBtn">Tampilkan lainnya</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Upah Kerja</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <?php 
                                                    $salary = $this->db->query("select a.*, count(b.id) as jml from ngi_salary a left join
                                                                                (select a.*, d.* from ngi_joblist a
                                                                                            left join ngi_job c on a.posisi = c.id
                                                                                            left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                                                                                            where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where.") b on b.upah_kerja = a.id
                                                                                group by a.range
                                                                                order by a.id")->result();

                                                    foreach ($salary as $s) {
                                                    $salary_array = explode(",", $_POST["salary"]);

                                                    if(in_array("s".$s->id, $salary_array)){
                                                        $salary_checked = "checked";
                                                    }else{
                                                        $salary_checked = "";
                                                    }
                                                 ?>
                                                <p class="jobsalary">
                                                    <input type="checkbox" id="s<?=$s->id?>" name="salary" <?=$salary_checked?> >
                                                    <label for="s<?=$s->id?>"><?=$s->range?><span> (<?=$s->jml?>)</span></label>
                                                </p>

                                                <?php } ?>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#" id="jobSalaryBtn">Tampilkan lainnya</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Tipe</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <?php 
                                                    $type = $this->db->query("select tipe, count(tipe) as jml from (select a.*, d.* from ngi_joblist a
                                                                                            left join ngi_job c on a.posisi = c.id
                                                                                            left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                                                                                            where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where.") a group by a.tipe")->result();

                                                    foreach ($type as $t){
                                                    $type_array = explode(",", $_POST["type"]);

                                                    if(in_array($t->tipe, $type_array)){
                                                        $type_checked = "checked";
                                                    }else{
                                                        $type_checked = "";
                                                    }
                                                 ?>
                                                <p>
                                                    <input type="checkbox" id="<?=$t->tipe?>" name="type" <?=$type_checked?> >
                                                    <label for="<?=$t->tipe?>"><?=$t->tipe?><span> (<?=$t->jml?>)</span></label>
                                                </p>

                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!-- <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">Tampilkan lainnya</a></li>
                                        </ul> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.redirect.js"></script>
    <script type="text/javascript">    
        var jobs = new Array();
        var category = new Array();
        var salary = new Array();
        var type = new Array();

        var currentJobListLength = 5;
        var currentJobCategoryLength = 5;
        var currentJobSalaryLength = 3;

        $(document).ready(function(){
            console.log('<?=json_encode($_POST)?>');

            $(document).on('change','input[name="jobs"]', function(){

                sendData('');
            });

            $(document).on('change','input[name="category"]', function(){

                sendData('');
            });

            $(document).on('change','input[name="salary"]', function(){

                sendData('');
            });

            $(document).on('change','input[name="type"]', function(){

                sendData('');
            });


            $(document).on('change','#sorting', function(){

                sendData('');
            });

            $('.fa-heart-o').hover( function(){
                $(this).attr('class', 'fa fa-heart fa-lg text-danger');
                $(this).parent().addClass('bg-red');
            },  function(){
                $(this).attr('class', 'fa fa-heart-o fa-lg text-danger');
                $(this).parent().removeClass('bg-red');
            });

            $('.pagination .disabled a, .pagination .active a').on('click', function(e) {
                e.preventDefault();
            });

            $(document).on('click','.pages', function(){
                var pages = $(this).prop('id');

                sendData(pages);
            }).on('click', '.trending', function(){
                var trend = $(this).prop('id');

                sendData('',trend);
            }).on('click', '#jobListBtn', function(e){
                e.preventDefault();
                showMoreJobList();
            }).on('click', '#jobCategoryBtn', function(e){
                e.preventDefault();
                showMoreJobCategory();
            }).on('click', '#jobSalaryBtn', function(e){
                e.preventDefault();
                showMoreJobSalary();
            })

            $('.joblist').hide();
            $('.joblist').slice(0,currentJobListLength).show();

            $('.jobcategory').hide();
            $('.jobcategory').slice(0,currentJobCategoryLength).show();

            $('.jobsalary').hide();
            $('.jobsalary').slice(0,currentJobSalaryLength).show();
        });

        function sendData(pages, trending = ''){
            $('input[name="jobs"]').each(function(){
                if($(this).is(':checked')){
                    jobs.push($(this).prop('id'));
                }
            });

            $('input[name="category"]').each(function(){
                if($(this).is(':checked')){
                    category.push($(this).prop('id'));
                }
            });


            $('input[name="salary"]').each(function(){
                if($(this).is(':checked')){
                    salary.push($(this).prop('id'));
                }
            });

            $('input[name="type"]').each(function(){
                if($(this).is(':checked')){
                    type.push($(this).prop('id'));
                }
            });

            post($('#jobsearch').attr('action'), {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    keyword : (trending == '')? $('input[name="keyword"]').val() : trending,
                    location : $('select[name="location"]').val(),
                    sorting : $('#sorting').val(),
                    // experience : $('select[name="experience"]').val(),
                    jobs: jobs,
                    category : category,
                    salary : salary,
                    type : type,
                    page : pages
                }, "post");
        }


        function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }

        function showMoreJobList(){
            currentJobListLength = currentJobListLength + 5;
            $('.joblist').slice(0,currentJobListLength).show();
        }

        function showMoreJobCategory(){
            currentJobCategoryLength = currentJobCategoryLength + 5;
            $('.jobcategory').slice(0,currentJobCategoryLength).show();
        }

        function showMoreJobSalary(){
            currentJobSalaryLength = currentJobSalaryLength + 5;
            $('.jobsalary').slice(0,currentJobSalaryLength).show();
        }
    </script>