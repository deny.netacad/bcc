<style>
.jp_tittle_name_wrapper {
    float: left;
    width: 15%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_tittle_side_cont_wrapper p {
    font-size: 14px;
    color: #184981;
}
.jp_banner_jobs_categories_wrapper2{
	float:left;
	width:100%;
	height:100%;
	background:#ffffff24;
	position:relative;
}
.jp_slide_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981eb;
}

.jp_banner_main_jobs_wrapper2{
	float:left;
	width:100%;
	margin-top:40px;
	padding-bottom:39px;
}
.jp_banner_main_jobs2 li{
	float:left;
	margin-left:20px;
}
.jp_banner_main_jobs2 li:first-child{
	margin-left:0;
	color:#ffffff;
}
.jp_banner_main_jobs2 li i{
	padding-right:5px;
	color:#23c0e9;
}
.jp_banner_main_jobs2 li a{
	color:#ffffffcf;
	-webkit-transition: all 0.5s;
	-o-transition: all 0.5s;
	-ms-transition: all 0.5s;
	-moz-transition: all 0.5s;
	transition: all 0.5s;
	
}
.jp_banner_main_jobs2 li a:hover{
	color:#23c0e9;
	-webkit-transition: all 0.5s;
	-o-transition: all 0.5s;
	-ms-transition: all 0.5s;
	-moz-transition: all 0.5s;
	transition: all 0.5s;
}
.modal-body {
    position: relative;
    padding: 60px;
}
.btn-primary {
    color: #fff;
    background-color: #2b77cd;
    border-color: #2e6da4;
    border-radius: 15px;
}

</style>          
<script>
	   

  $(function (){



    // $('.simple-marquee-container').SimpleMarquee();

    $(document).on('change','select[name="province"]',function(){
        $.ajax({
            url : '<?=base_url()?>portal/page/cariKab',
            type: 'post',
            data: {
              <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
              id : $(this).val()
            },
            success: function(result){
              $('select[name="city"]').html(result);
            }
        });
    }).on('click', 'a.link_category', function(){
      post('<?=base_url()?>portal/caripekerjaan', {
          <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
          category : $(this).prop('id')
      }, "post");
    }).on('click', 'a.trending', function(){
      post('<?=base_url()?>portal/caripekerjaan', {
          <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
          keyword : $(this).prop('id')
      }, "post");
    });
	
	

  });
 function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
	

		</script>
	
<div class="jp_banner_heading_cont_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_job_heading_wrapper">
                            <div class="jp_job_heading">
                                <h1>MAU CARI APA ?</h1>
                                <p>DISNAKER KABUPATEN BOGOR</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form action="<?=base_url()?>portal/caripekerjaan" method="POST" id="jobsearch">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
                            <div class="jp_header_form_wrapper">
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="keyword" placeholder="Nama pekerjaan, Ketrampilan, Kata Kunci, dll" value="<?=$_POST['keyword']?>">
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="jp_form_btn_wrapper">
                                        <ul style="width: 100%;">
                                            <li ><a onclick="document.getElementById('jobsearch').submit()" style="width: 100%;"><i class="fa fa-search"></i> Cari</a></li>
                                        </ul>
                                    </div>
                                </div>
								<?php 
								$this->load->library('user_agent');
								if($this->session->userdata('is_login_pelamar')) { ?>
								<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                    <div class="jp_form_btn_wrapper">
                                        <ul>
											<li><button id="<?=(($this->agent->platform() == "Linux")? 'scan' : 'not-avaliable')?>" type="button" class="btn btn-primary" style="padding: 14px 14px;"><i class="fa fa-qrcode"></i>
											SCAN 
											</button></li>
                                        </ul>
                                    </div>
                                </div>
								<?php }?>
								
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_banner_main_jobs_wrapper2">
                            <div class="jp_banner_main_jobs2">
                                <!-- <ul>
                                    <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                    <?php 
                                        $q = $this->db->query('select keyword, count(id) as jml from ngi_keyword where date(log) between current_date - interval 7 day and current_date group by keyword order by jml desc limit 6');

                                        $data = $q->result();

                                        $i = 0;
                                        $len = count($data);
                                        foreach ($data as $item) {
                                            if ($i == $len - 1) {
                                                echo '<li><a href="#" class="trending" id="'.$item->keyword.'">'.$item->keyword.'</a></li>';          
                                            } else {
                                                echo '<li><a href="#" class="trending" id="'.$item->keyword.'">'.$item->keyword.',</a></li>';          
                                            }

                                            $i++;
                                        }
                                    ?>
                                </ul> -->
                            </div>
							
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(!isset($page)){ ?>
            <div class="jp_banner_jobs_categories_wrapper">
                <div class="container">
                    <?php
                        $rs = $this->db->query('select SUM(jml_pekerja) as "jml", a.category, a.icon, a.id from ngi_jobcategory a 
                                                left join (select * from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day) ) b on a.id = b.`idcategory`
                                                WHERE b.id!=0
                                                group by a.id
                                                order by rand()
                                                limit 4')->result();

                        foreach ($rs as $item) {
                    ?>

                    <div class="jp_top_jobs_category_wrapper jp_job_cate_left_border jp_job_cate_left_border_bottom">
                        <div class="jp_top_jobs_category">
                            <i class="fa <?=$item->icon?>"></i>
                            <h3><a href="#" id="c<?=$item->id?>" class="link_category"><?=$item->category?></a></h3>
                            <p>(<?=$item->jml?> lowongan)</p>
                        </div>
                    </div>

                     <?php } 

                        $jml = $this->db->query('select SUM(jml_pekerja) as "jml" from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day)')->row();
                        $aktf = $this->db->query('select count(id) as "jml_aktif" from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day)')->row();
                    ?>

                    <div class="jp_top_jobs_category_wrapper">
                        <div class="jp_top_jobs_category">
                            <i class="fa fa-th-large"></i>
                            <h3><a href="<?=base_url()?>portal/caripekerjaan">Total Lowongan</a></h3>
                            <p>(<?=$jml->jml?> lowongan)</p>
                        </div>
                    </div>
                    

                    <div class="jp_top_jobs_category_wrapper">
                        <div class="jp_top_jobs_category">
                            <i class="fa fa-check-square-o"></i>
                            <h3><a href="<?=base_url()?>portal/caripekerjaan">Total Lowongan Aktif</a></h3>
                            <p>(<?=$aktf->jml_aktif?> lowongan)</p>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        <?php } ?>

		
		
		<!--<div class="container2">
			<div class="simple-marquee-container">
				<div class="marquee-sibling">
					<li>Pengumuman :</li>
				</div>
				<div class="marquee">
					<ul class="marquee-content-items">
					 <?php
                        $rs = $this->db->query('select * from ref_pengumuman where publish=1 limit 3')->result();

                        foreach ($rs as $item) {
                    ?>
						<li><?=$item->nama?></li>
						<?php }?>
					</ul>
				</div>
			</div>
		</div>-->
			<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">SCAN Untuk Mendaftar Pekerjaan</h5>
		<small style="font-size:10px;font-style:italic;">*Disarankan menggunakan browser chrome dengan versi terupdate</small>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
          <div class="col-lg-12">
            <div id="app">
                <div class="row">
                    <div class="col-lg-4">
                    <section class="cameras">
                      <ul>
                        <li v-if="cameras.length === 0" class="empty">No cameras found</li>
						<!--li v-for="camera in cameras">
                          <span v-if="camera.id == activeCameraId" :title="formatName(camera.name)" class="active">{{ formatName(camera.name) }}</span>
                          <span v-if="camera.id != activeCameraId" :title="formatName(camera.name)">
                            <a @click.stop="selectCamera(camera)">{{ formatName(camera.name) }}</a>
                          </span>
                        </li-->
                      </ul>
                    </section>
                    <div id="list">
                        
                    </div>
                  </div>
                    <div class="col-lg-8">
                        <video width="100%" height="50%" id="preview"></video>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
		<script>
	var qr = '';
	
	$(document).on('click', '#scan', function(){
		var app = new Vue({
				el: '#app',
				data: {
				scanner: null,
				activeCameraId: null,
				cameras: [],
				scans: []
			},

		  mounted: function () {
			var self = this;
			self.scanner = new Instascan.Scanner({ video: document.getElementById('preview'),mirror: false, scanPeriod: 3 ,});
			self.scanner.addListener('scan', function (content, image) {
			  self.scans.unshift({ date: +(Date.now()), content: content });
			  qr = content;
			  kirim(qr);
			});
			Instascan.Camera.getCameras().then(function (cameras) {
			  self.cameras = cameras;
			  if (cameras.length > 0) {
				// self.activeCameraId = cameras[1].id;
				self.scanner.start(cameras[1]);
			  }else {
				console.error('Your Devices Not Support to use');
			  }
			}).catch(function (e) {
			  console.error(e);
			});
		  },
		  methods: {
			formatName: function (name) {
			  return name || 'Camera Ready To Scan!';
			},
			selectCamera: function (camera) {
			  this.activeCameraId = camera.id;
			  this.scanner.start(camera);
			}
		  }
		});
		
		$('#showModal').modal('show');
	}).on('click', '#not-avaliable', function(){
		alert('This feature is only available on android device');
	})

function kirim(qr)
{	
   if(confirm('Apakah anda yakin akan mendaftar?')){
		$.ajax({
		   url: "<?php echo base_url('portal/page/readqr')?>/" + qr,
						type: 'post',
						data: {
							 <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
							 id : '<?=$this->session->userdata('id')?>',
							 id_job : qr

						},
						dataType: 'json',
						
			success: function(result){
				if(result.login){
					if(result.success){
						alert('Pendaftaran pekerjaan berhasil');
						$('#showModal').modal('hide'); // show bootstrap modal when complete loaded 
						window.location.href = "<?=base_url()?>portal/lamaran";
					}else{
						alert('Anda sudah melakukan pendaftaran pada pekerjaan tersebut');
						window.location.href = "<?=base_url()?>portal/lamaran";
					}
				}
			},
		   
		});
	}else{
		$('#showModal').on('hide.bs.modal', function(e){
		}).modal('hide');
	}
}



</script>