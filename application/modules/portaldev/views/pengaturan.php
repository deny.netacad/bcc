<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />

<style type="text/css">
.mainmenu ul li a {

	padding: 0px 7px 47px 10px;
	}
    .jp_recent_resume_cont_wrapper{
        padding-top: 0px !important;
        width: 100% !important;
    }

    .fa-icon{
        font-size: 15px;
        color: #f36969;
    }

    .mobile-btn {
        float: right;
        padding-top: 10px;
    }

    .accordion_wrapper .panel-group .panel-heading+.panel-collapse>.panel-body {
        background: #eee;
        padding: 25px 0px 30px 0px;
    }

    .ket {
        border: 1px solid #ddd;
        background: #e9e9e9;
        color: #6a7c98;
        font-size: 17px;
    }

    .cek {
        border: 1px solid #ddd;
        background: #e9e9e9;
        color: #6a7c98;
        font-size: 17px;
    }
    .gc_main_navigation {
            color: #f9f9f9!important;
            font-weight: bold;
        }

    .jp_adp_form_wrapper input, .jp_adp_form_wrapper select {
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .jp_adp_choose_resume_bottom_btn_post li button {
        width: 160px;
        height: 50px;
        text-align: center;
        color: #ffffff;
        font-weight: bold;
        background: #af2020;
        border: #f36969;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        -webkit-transition: all 0.5s;
        -o-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -moz-transition: all 0.5s;
        transition: all 0.5s;
    }

    .jp_adp_choose_resume_bottom_btn_post li button[disabled] {
        background: #f19d9d;
        border: #f19d9d;
    }

    #autocomplete, #autocomplete2 {
        background-color: #181d28;
        border-radius: 3px;
        border: 1px solid #212b2d;
        max-height: 250px;
        overflow-y: auto;
    }


    #autocomplete ul, #autocomplete2 ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
 
    #autocomplete ul li,  #autocomplete2 ul li  {
        padding: 10px 20px;
        border-bottom: 1px solid #212b2d;
    }

    #autocomplete ul li:focus,
    #autocomplete ul li:hover,
    #autocomplete2 ul li:focus,
    #autocomplete2 ul li:hover {
        background-color: #F0F0F0;
        cursor: pointer;
    }


    .tag {
      font-size: 14px;
      padding: .3em .4em .4em;
      margin: 0 .1em;
    }

    .tag a {
      color: #bbb;
      cursor: pointer;
      opacity: 0.6;
    }

    .tag a:hover {
      opacity: 1.0
    }

    .tag .remove {
      vertical-align: bottom;
      top: 0;
    }

    .tag a {
      margin: 0 0 0 .3em;
    }

    .tag a .glyphicon-white {
      color: #fff;
      margin-bottom: 2px;
    }

    .jp_adp_textarea_main_wrapper.no-topborder{
        border-top: none;
        padding-top: 0px;
    }
	
	#tag-area{
		overflow-x: auto;
		padding-bottom: 5px;
	}
    
 .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
} 
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: rgb(24, 73, 129);
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #184981;
    border-color: #184981;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    z-index: 2;
    color: #fff;
    border: 1px solid #184981;
    background-color: #184981;
    border-color: none;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_bottom_cont_wrapper {
    float: left;
    width: 100%;
    background: #f9f9f9;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-bottom: 1px solid #184981;
    border-top: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
.mainmenu ul li a {

    padding: 0px 7px 47px 10px;
    }
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_job_des h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_job_des h2:before {
    content: '';
    border: 1px solid #184980;
    width: 8px;
    position: absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
  
    .login_wrapper button.btn {
        color: #fff;
        width: 100%;
        height: 50px;
        padding: 6px 25px;
        line-height: 36px;
        margin-bottom: 20px;
        text-align: left;
        border-radius: 8px;
        background-color: #337AB8;
        font-size: 16px;
        border: 1px solid #f9f9f9;
        text-align: center;
        text-transform: uppercase;
    }

 .jp_cp_rd_wrapper li:first-child a {
    float: left;
    width: 100%;
    height: 50px;
    text-align: center;
    line-height: 50px;
    background: #184980;
    color: #ffffff;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    font-size: 18px;
    font-weight: 600;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -ms-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
}
.accordion_wrapper .panel .panel-heading a {
    display: block;
    background: #184980;
    padding: 12px 20px;
    color: #fff;
}
.abt_page_2_wrapper .panel .panel-heading a.collapsed {
    background: #184980 !important;
}
 
 
</style>

<?php
    
    if($this->session->userdata('is_login_pelamar')){
    
    $biodata = $this->db->query('select * from (select * from user_pelamar where iduser = "'.$this->session->userdata('id').'") a
                                    left join ngi_kecamatan b on a.kecamatan_id = b.subdistrict_id')->row();
?>

<div class="jp_listing_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar" style="padding-top: 20px">
                <div class="jp_job_des">
                    <h2>Pengaturan</h2>
                </div>
                <div class="row" style="padding-top: 80px">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="accordion_wrapper abt_page_2_wrapper">
                            <div class="panel-group" id="accordion_threeLeft">
                                <!-- /.panel-default -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree" aria-expanded="true">
                                                PROFIL <?=(($biodata->ttg_anda == "")? '&nbsp; <span class="label label-danger">Ceritakan tentang diri anda<span>' : '')?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree" class="panel-collapse collapse in" aria-expanded="true" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>portal/page/saveprofil" method="post" id="form">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->iduser?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Nama lengkap</span>
                                                                <input type="text" placeholder="Isikan nama lengkap sesuai dengan kartu tanda pengenal anda" name="nama" id="nama" required="" value="<?=$biodata->nama?>">
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">NIK</span>
                                                                <input type="text" placeholder="Isikan nomor induk kependudukan anda" name="no_ktp" id="no_ktp" required="" value="<?=$biodata->no_ktp?>"  pattern="[0-9]{16,16}" title="No ktp harus berupa angka, 16 digit">
                                                            </div>
                                                        </div>
                                                         <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Alamat lengkap</span>
                                                                <input type="text" placeholder="Alamat tempat tinggal sesuai dengan kartu tanda pengenal anda" name="alamat" id="alamat" required="" value="<?=$biodata->alamat?>">
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">No. telfon</span>
                                                                <input type="text" placeholder="Nomor telfon atau handphone yg bisa dihubungi" name="no_telp" id="no_telp" required="" value="<?=$biodata->no_telp?>">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Provinsi</span>
                                                                        <select name="province_id">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $q1 = $this->db->query('select province_id, province from ngi_kecamatan group by province_id')->result();

                                                                                foreach ($q1 as $province) {
                                                                                    if($province->province_id == $biodata->province_id){
                                                                                        echo '<option value="'.$province->province_id.'" selected>'.$province->province.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$province->province_id.'">'.$province->province.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Kab/Kota</span>
                                                                        <select name="kab_id">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $q2 = $this->db->query('select city_id, city, type from ngi_kecamatan where province_id = "'.$biodata->province_id.'" group by city_id')->result();

                                                                                foreach ($q2 as $city) {
                                                                                    if($city->city_id == $biodata->city_id){
                                                                                        echo '<option value="'.$city->city_id.'" selected>'.$city->type.' '.$city->city.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$city->city_id.'">'.$city->type.' '.$city->city.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Kecamatan</span>
                                                                        <select name="kecamatan_id">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $q3 = $this->db->query('select subdistrict_id, subdistrict_name from ngi_kecamatan where city_id = "'.$biodata->city_id.'"')->result();

                                                                                foreach ($q3 as $kec) {
                                                                                    if($kec->subdistrict_id == $biodata->subdistrict_id){
                                                                                        echo '<option value="'.$kec->subdistrict_id.'" selected>'.$kec->subdistrict_name.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$kec->subdistrict_id.'">'.$kec->subdistrict_name.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Tempat</span>
                                                                        <input type="text" placeholder="Kota tempat anda lahir" name="tmp_lahir" id="tmp_lahir" required="" value="<?=$biodata->tmp_lahir?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Tgl lahir</span>
                                                                        <input type="text" class="dt" placeholder="Tanggal anda lahir" name="tgl_lahir" id="tgl_lahir" required="" value="<?=$biodata->tgl_lahir?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Jenis kelamin</span>
                                                                        <select name="gender">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $jk = array('Laki-laki', 'Perempuan');

                                                                                foreach ($jk as $select1) {
                                                                                    if($select1 == $biodata->gender){
                                                                                        echo '<option value="'.$select1.'" selected>'.$select1.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$select1.'">'.$select1.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Agama</span>
                                                                        <select name="agama">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $agama = array('Budha','Hindu', 'Islam', 'Kristen', 'Kristen-Katolik', 'Kristen Protestan', 'Lainnya');

                                                                                 foreach ($agama as $select2) {
                                                                                    if($select2 == $biodata->agama){
                                                                                        echo '<option value="'.$select2.'" selected>'.$select2.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$select2.'">'.$select2.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Kewarganegaraan</span>
                                                                        <select name="wn">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $wn = array('WNI', 'WNA');

                                                                                 foreach ($wn as $select3) {
                                                                                    if($select3 == $biodata->wn){
                                                                                        echo '<option value="'.$select3.'" selected>'.$select3.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$select3.'">'.$select3.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Status</span>
                                                                        <select name="sts">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $sts = array('Lajang', 'Nikah', 'Cerai');

                                                                                 foreach ($sts as $select4) {
                                                                                    if($select4 == $biodata->sts){
                                                                                        echo '<option value="'.$select4.'" selected>'.$select4.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$select4.'">'.$select4.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px">
                                                        <div class="jp_adp_textarea_main_wrapper">
                                                            <div style="padding-bottom: 25px">
                                                                <textarea rows="7" placeholder="Tulis sesuatu tentang diri anda" name="ttg_anda" id="ttg_anda" required=""><?=str_replace("<br />","\n",$biodata->ttg_anda)?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree4" aria-expanded="false">
                                                AKUN <?=(($biodata->photo == "")? '&nbsp; <span class="label label-danger">Unggah foto anda<span>' : '')?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree4" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>portal/page/saveakun" method="post" id="form2" enctype="multipart/form-data">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->iduser?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <img src="<?=base_url()?>assets/img/profile/<?=(($biodata->photo == '')? 'default_user.png' : $biodata->photo )?>" style="width: 300px; height: 300px; object-fit: contain;">
                                                        <input type="file" name="scanfile" style="padding-top: 10px;">
                                                        <input type="hidden" name="filename" value="<?=$biodata->photo?>">
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Username</span>
                                                                <input type="text" name="username" id="username" value="<?=$biodata->username?>" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka, 3 - 10 karakter" required="">
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">URL Profil</span>
                                                                <input type="text" placeholder="<?=base_url()?>protal/profile/" name="profile_url" id="profile_url" required="" value="<?=$biodata->profile_url?>" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka, 3 - 10 karakter">
                                                                <span class="input-group-addon cek" id="url-change"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit2">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <form action="<?=base_url()?>portal/page/savepassword" method="post" id="form3">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->iduser?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Password Lama</span>
                                                                <input type="password" id="pass_lama">
                                                                <span class="input-group-addon cek" id="pass_lama-change"></span>
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Password baru</span>
                                                                <input type="password" name="pass_baru" id="pass_baru" required="">
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Konfirmasi Password baru</span>
                                                                <input type="password" required="" id="confirm_pass">
                                                                <span class="input-group-addon cek" id="confirm_pass-change"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit3">Ganti</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <form action="<?=base_url()?>portal/page/saveemail" method="post" id="form7">
	                                        	<div style="padding-top: 10px">
	                                                <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->iduser?>">
	                                                <input type="hidden" placeholder="Judul Lowongan" name="link_enkripsi" required="" value="<?=$biodata->link_enkripsi?>">
	                                                <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
	                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                                                    <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Email</span>
                                                                <input type="text" placeholder="Alamat email anda yang masih aktif" name="email" id="email" required="" value="<?=$biodata->email?>">
                                                                <span class="input-group-addon cek" id="email-change"></span>
                                                            </div>
                                                        </div>
	                                                </div>
	                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                                                    <div class="jp_adp_choose_resume_bottom_btn_post">
	                                                        <ul>
	                                                            <li><button type="submit" id="submit7">Ganti</button></li>
	                                                        </ul>
	                                                    </div>
	                                                </div>
	                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree5" aria-expanded="false">
                                                RIWAYAT PENDIDIKAN <?=(($biodata->education == "")? '&nbsp; <span class="label label-danger">Lengkapi riwayat pedidikan anda<span>' : '')?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree5" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>portal/page/saveeducationuser" method="post">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" name="id_edit" required="" value="<?=$biodata->iduser?>">
                                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <input type="hidden" id="all-education" name="education">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="tag-area-education">
                                                            <?php 
                                                                if($biodata->education != ""){
                                                                    $education = explode(',', $biodata->education);
                                                                    foreach ($education as $s) {
                                                                        echo '<span class="tag label label-default">
                                                                              <span class="education-name">'.$s.'</span>
                                                                              <a class="close-tag-education"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a> 
                                                                            </span>';
                                                                    } 
                                                                }
                                                            ?>    
                                                        
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                            <input type="text" id="education" placeholder="Cari nama sekolah anda">
                                                            <div id="autocomplete2"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree5" aria-expanded="false">
                                                PENDIDIKAN TERAKHIR<?=(($biodata->ins_pend == "" || $biodata->jurusan == "" || $biodata->tahun_lulus == "")? '&nbsp; <span class="label label-danger">Lengkapi pedidikan terakhir anda<span>' : '')?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree5" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>portal/page/savelasteducation" method="post">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" name="id_edit" required="" value="<?=$biodata->iduser?>">
                                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Pend. terakhir</span>
                                                                <select name="pendidikan" required="">
                                                                    <option value="">Pilih salah satu</option>
                                                                    <?php
                                                                        $pend = array('SD', 'SMP', 'SMA', 'SMK', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3');

                                                                         foreach ($pend as $select4) {
                                                                            if($select4 == $biodata->pendidikan){
                                                                                echo '<option value="'.$select4.'" selected>'.$select4.'</option>';
                                                                            }else{
                                                                                echo '<option value="'.$select4.'">'.$select4.'</option>';
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Nama Instansi</span>
                                                                <input type="text" placeholder="Nama instansi pendidikan terakhir anda" name="ins_pend" id="ins_pend" required="" value="<?=$biodata->ins_pend?>">
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Jurusan/prodi</span>
                                                                <input type="text" placeholder="Jurusan atau program studi pendidikan terakhir anda" name="jurusan" id="jurusan" required="" value="<?=$biodata->jurusan?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Tahun lulus</span>
                                                                <select name="tahun_lulus" required="">
                                                                    <option value="">Pilih salah satu</option>
                                                                    <?php
                                                                        $year = idate("Y");
                                                                         for ($i = $year; $i >= 1980; $i--){
                                                                            if($i == $biodata->tahun_lulus){
                                                                                echo '<option value="'.$i.'" selected>'.$i.'</option>';
                                                                            }else{
                                                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree2" aria-expanded="false">
                                                KEAHLIAN <?=(($biodata->skill == "")? '&nbsp; <span class="label label-danger">Lengkapi keahlian anda<span>' : '')?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree2" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>portal/page/saveskilluser" method="post" id="form4">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->iduser?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <input type="hidden" id="all-skill" name="skill">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="tag-area">
                                                            <?php 
                                                                if($biodata->skill != ""){
                                                                    $skill = explode(',', $biodata->skill);
                                                                    foreach ($skill as $s) {
                                                                        echo '<span class="tag label label-default">
                                                                              <span class="skill-name">'.$s.'</span>
                                                                              <a class="close-tag"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a> 
                                                                            </span>';
                                                                    } 
                                                                }
                                                            ?>    
                                                        
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                            <input type="text" id="skill" placeholder="Cari skill yang anda kuasai (maksimal panjang 50 karakter)">
                                                            <div id="autocomplete"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit4">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree3" aria-expanded="false">
                                                PENGALAMAN <?=(($biodata->exp == "")? '&nbsp; <span class="label label-danger">Ceritakan pengalaman anda<span>' : '')?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree3" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>portal/page/saveexpuser" method="post" id="form5">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->iduser?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_textarea_main_wrapper no-topborder">
                                                            <div style="">
                                                                <textarea rows="7" placeholder="Ceritakan pengalaman anda di dunia kerja" name="exp" id="exp" required=""><?=str_replace("<br />","\n",$biodata->exp)?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit5">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                            </div>
                            <!--end of /.panel-group-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } else{

    redirect(base_url().'portal/login');
}?>

<script type="text/javascript">
    var skill = new Array();
    var education = new Array();

    $(function(){
        gatherSkill();
        $('#all-skill').val(skill);

        gatherEducation();
        $('#all-education').val(education);

        $('.dt').datepicker({
            format:'yyyy-mm-dd',
            //format:'dd-mm-yyyy',
            minViewMode:0,
            viewMode:1
        }).on('changeDate',function(){
            $('.dt').datepicker('hide');
        });

        $(document).on('input', '#profile_url', function(e){
            var value = $(this).val();

            $('#url-change').html('<i class="fa fa-spinner fa-spin"></i>');
			
			var value = $(this).val();            
            var regexp = /[A-Za-z0-9]{3,}$/
			
			if(value.match(regexp)){
				$.ajax({
					url : '<?=base_url()?>portal/page/checkurlprofile',
					type: 'post',
					dataType: 'json',
					data : {
						'<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
						'url' : value
					},
					success: function(result){

						if(result.available){
							$('#url-change').html('<i class="fa fa-check"></i>');
							$('#submit2').attr('disabled',false);
						}else{
							$('#url-change').html('<i class="fa fa-times"></i>');
							$('#submit2').attr('disabled',true);
						}
					}
				});
			}else{
                $('#url-change').html('<i class="fa fa-times"></i>');
                $('#submit2').attr('disabled',true);
            }

            e.preventDefault();
        }).on('input', '#email', function(e){
            var value = $(this).val();

            $('#email-change').html('<i class="fa fa-spinner fa-spin"></i>');
			
			var value = $(this).val();            
            var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
			
			if(value.match(regexp)){
				$.ajax({
					url : '<?=base_url()?>portal/page/checkemail',
					type: 'post',
					dataType: 'json',
					data : {
						'<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
						'email' : value
					},
					success: function(result){

						if(result.available){
							$('#email-change').html('<i class="fa fa-check"></i>');
							$('#submit7').attr('disabled',false);
						}else{
							$('#email-change').html('<i class="fa fa-times"></i>');
							$('#submit7').attr('disabled',true);
						}
					}
				});
			}else{
                $('#email-change').html('<i class="fa fa-times"></i>');
                $('#submit7').attr('disabled',true);
            }

            e.preventDefault();
        }).on('input', '#pass_lama', function(){
            var value = $(this).val();

            $('#pass_lama-change').html('<i class="fa fa-spinner fa-spin"></i>');

            $.ajax({
                url : '<?=base_url()?>portal/page/checkpassword',
                type: 'post',
                dataType: 'json',
                data : {
                    '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                    'pass' : value
                },
                success: function(result){

                    if(result.available){
                        $('#pass_lama-change').html('<i class="fa fa-check"></i>');
                        $('#submit3').attr('disabled',false);
                    }else{
                        $('#pass_lama-change').html('<i class="fa fa-times"></i>');
                        $('#submit3').attr('disabled',true);
                    }
                }
            });
        }).on('input', '#confirm_pass', function(){
            var value = $(this).val();

            $('#confirm_pass-change').html('<i class="fa fa-spinner fa-spin"></i>');

            if($('#pass_baru').val() == value){
                $('#confirm_pass-change').html('<i class="fa fa-check"></i>');
                $('#submit3').attr('disabled',false);
            }else{
                $('#confirm_pass-change').html('<i class="fa fa-times"></i>');
                $('#submit3').attr('disabled',true);
            }
        }).on('input', '#skill', function(){
            var value = $(this).val();

            if(value == ""){
                $('#autocomplete').html('');
            }else{
                 $.ajax({
                    url : '<?=base_url()?>portal/page/searchskill',
                    type: 'post',
                    data : {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'skill' : value
                    },
                    success: function(result){
                        $('#autocomplete').html(result);
                    }
                });
            }
        }).on('click', '.select-skill', function(){
            var tagged = '<span class="tag label label-default"><span class="skill-name">'+$(this).attr('data')+'</span><a class="close-tag"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a></span>';
            var string = $(this).find('a').html();
            var boolean = string.includes('Tambahkan');

            if(!skill.includes($(this).attr('data').toLowerCase())){
                if(boolean){
                    $.ajax({
                        url : '<?=base_url()?>portal/page/savenewskill',
                        type: 'post',
                        data : {
                            '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                            'skill' : $(this).attr('data')
                        },
                        success: function(result){
                            if(result){
                                $('#tag-area').append(tagged);
                                $('#autocomplete').html('');
                                $('#skill').val('');
                            }else{
                                alert('Terjadi kesalahan. Silahkan ulangi');
                            }
                            
                        }
                    });
                }else{
                    $('#tag-area').append(tagged);
                    $('#autocomplete').html('');
                    $('#skill').val('');
                }

                skill = [];
                gatherSkill();
                $('#all-skill').val(skill);
            }else{
                $('#autocomplete').html('');
                $('#skill').val('');
            }

        }).on('click', '.close-tag', function(){
            $(this).closest('span.tag').remove();
            skill = [];
            gatherSkill();
            $('#all-skill').val(skill);
        }).on('change', 'select[name="province_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikab',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kab_id"]').html(result);
                }
            })
        }).on('change', 'select[name="kab_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikec',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kecamatan_id"]').html(result);
                }
            })
        }).on('input', '#education', function(){
            var value = $(this).val();

            if(value == ""){
                $('#autocomplete2').html('');
            }else{
                 $.ajax({
                    url : '<?=base_url()?>portal/page/searcheducation',
                    type: 'post',
                    data : {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'education' : value
                    },
                    success: function(result){
                        $('#autocomplete2').html(result);
                    }
                });
            }
        }).on('click', '.select-education', function(){
            var tagged = '<span class="tag label label-default"><span class="education-name">'+$(this).attr('data')+'</span><a class="close-tag-education"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a></span>';
            var string = $(this).find('a').html();
            var boolean = string.includes('Tambahkan');

            if(!education.includes($(this).attr('data').toLowerCase())){
                if(boolean){
                    $.ajax({
                        url : '<?=base_url()?>portal/page/saveneweducation',
                        type: 'post',
                        data : {
                            '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                            'education' : $(this).attr('data')
                        },
                        success: function(result){
                            if(result){
                                $('#tag-area-education').append(tagged);
                                $('#autocomplete2').html('');
                                $('#education').val('');
                            }else{
                                alert('Terjadi kesalahan. Silahkan ulangi');
                            }
                            
                        }
                    });
                }else{
                    $('#tag-area-education').append(tagged);
                    $('#autocomplete2').html('');
                    $('#education').val('');
                }

                education = [];
                gatherEducation();
                $('#all-education').val(education);
            }else{
                $('#autocomplete2').html('');
                $('#education').val('');
            }

        }).on('click', '.close-tag-education', function(){
            $(this).closest('span.tag').remove();
            education = [];
            gatherEducation();
            $('#all-education').val(skill);
        }).on('submit', 'form', function(){
			$(this).find('textarea').val($(this).find('textarea').val().replace(/\r\n|\r|\n/g,"<br />"));
		});
    });

 function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

 function gatherSkill(){
    $('.skill-name').each(function(){
        skill.push($(this).text().toLowerCase());
    });
 }

 function gatherEducation(){
    $('.education-name').each(function(){
        education.push($(this).text().toLowerCase());
    });
 }
</script>
