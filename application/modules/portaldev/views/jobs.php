    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />
    <style type="text/css">
    	a[disabled] {
		    pointer-events: none;
		}
        .gc_main_navigation {
            color: #f9f9f9!important;
            font-weight: bold;
        }
        .image {
            width: 70px;
            height: 70px;
            object-fit: contain;
        }

        .jp_contact_inputs4_wrapper textarea {
            margin-top: 0px;
            padding-left: 50px;
        }

        .jp_contact_inputs4_wrapper i {
            top : 21px;
        }

        .jp_contact_form_btn_wrapper ul{
            margin-top: 10px;
        }

        .jp_contact_form_btn_wrapper ul li{
            margin-bottom: 20px;
            float: right;
        }
        .jp_tittle_heading h2 {
            font-size: 36px;
            color: #333;
            font-weight: bold;
        }

        .komenarea{
            border: 1px solid #282d39!important; 
            border-radius: 10px; 
            padding: 10px;
            margin-top: 10px;            
        }

        .delborder{
            border-left: 1px solid #3e4148;
            padding-left: 20px;
        }
		.centerqr {
  display: block;
  margin-left: auto;
  margin-right: auto;
 
}
.mainmenu ul li a {

padding: 0px 7px 47px 10px;
}
.gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
    .disabled{
        pointer-events:none; //This makes it not clickable
        opacity:0.6;  
    }

        .jp_add_resume_wrapper {
            width: 100%;
            height: 100%;
            background-position: center 0;
            background-size: contain;
            background-repeat: no-repeat;
            position: relative;
            text-align: center;
            padding-left: 30px;
            padding-right: 30px;
        }

    .jp_job_post_right_cont li:first-child {
        color: #6f7e98;
    }
     .jp_job_post_keyword_wrapper li i {
    padding-right: 5px;
    color: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_job_post_right_cont p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.jp_job_post_main_wrapper_cont:hover .jp_job_post_keyword_wrapper {
    border: 1px solid #184981;
    border-top: 0;
    background: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
    .jp_add_resume_wrapper {
    width: 100%;
    height: 100%;
    background-position: center 0;
    background-size: cover;
    background-repeat: no-repeat;
    position: relative;
    text-align: center;
    padding-left: 30px;
    padding-right: 30px;
}
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: #184981;
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #ffffff;
    border: 1px solid #3b6392;
    background: #184981;
    -webkit-border-top-right-radius: 0;
    -moz-border-top-right-radius: 0;
    border-top-right-radius: 0;
    -webkit-border-top-left-radius: 0;
    -moz-border-top-left-radius: 0;
    border-top-left-radius: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.jp_job_post_right_cont li:first-child {
    color: #6f7e98;
}

.jp_hiring_content_wrapper img{
    width: 100px !important;
}

.company_img {
    width: 100px !important;
    height: 95px;
    object-fit: contain;
}
    .mainmenu ul li a {

    padding: 0px 7px 47px 10px;
    }
    
    .jp_slide_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #12703b;
}   
 .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_hiring_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_hiring_heading_wrapper h2:before{
    content:'';
    border:1px solid #184981;
    width:8px;
    position:absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}
.jp_listing_heading_wrapper span {
    color: #184981;
}
.gc_causes_search_box p span {
    color: #184981;
}
.handyman_sec1_wrapper label span {
    color: #184981;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

.mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}
.jp_rightside_job_categories_heading {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
.jp_listing_left_bottom_sidebar_key_wrapper li i {
    padding-right: 5px;
    color: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_job_res h2:after {
    content: '';
    border: 1px solid #184981;
    width: 30px;
    position: absolute;
    bottom: -15px;
    left: 11px;
}
.jp_job_res h2:before {
    content: '';
    border: 1px solid #184981;
    width: 8px;
    position: absolute;
    bottom: -15px;
    left: 0;
}
.jp_job_des h2:before {
    content: '';
    border: 1px solid #184981;
    width: 8px;
    position: absolute;
    bottom: -15px;
    left: 0;
}
.jp_job_des h2:after {
    content: '';
    border: 1px solid #184981;
    width: 30px;
    position: absolute;
    bottom: -15px;
    left: 11px;
}

    </style>

    <?php
        $seo = $this->uri->segment(3);
        $rs = $this->db->query('select a.*, f.name as upah, date_format(a.date_start,"%d %M %Y") as tgl_awal, date_format(a.date_end,"%d %M %Y") as tgl_akhir, date_end, date_format(a.log,"%d %M %Y") as tgl, b.nama_job, c.photo, c.nama_perusahaan, c.profile_url,
                                d.subdistrict_name, d.city, d.province, e.category from (select * from ngi_joblist where seo_url = "'.$seo.'" ) a
                                left join ngi_job b on a.posisi = b.id
                                left join ngi_perusahaan c on a.idperusahaan = c.idperusahaan
                                left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                                left join ngi_jobcategory e on a.idcategory = e.id
								left join ngi_salary f on a.upah_kerja = f.id');

        if($rs->num_rows() > 0){
            $data = $rs->row();
            $this->db->insert('ngi_jobtrend',array('id_joblist' => $data->id));
            if($this->session->userdata('is_login_pelamar')){
                $is_pelamar = $this->db->query('select id from ngi_jobapplied where id_job = "'.$data->id.'" and id_pelamar = "'.$this->session->userdata('id').'" and status is NULL')->num_rows();    
            }

            $keyword = explode(',', $data->keyword);
    ?>

    <div class="jp_listing_single_main_wrapper" style="padding-bottom:0px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2><?=$data->title?>&nbsp;</h2>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp listing Single cont Wrapper Start -->
    <div class="jp_listing_single_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_rightside_listing_single_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Gambaran umum</h4>
                                </div>
                                <div class="jp_jop_overview_img_wrapper">
                                    <div class="jp_jop_overview_img">
                                        <img src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_company.jpg')?>" alt="company_img" style="object-fit: contain; width: 100px; height: 95px;" />
                                    </div>
                                </div>
                                <div class="jp_job_listing_single_post_right_cont">
                                    <div class="jp_job_listing_single_post_right_cont_wrapper">
                                        <h4><?=$data->nama_job?></h4>
                                        <a style="color: #184981;padding-top: 10px; font-size: 16px" href="<?=base_url()?>portal/company/<?=$data->profile_url?>"><?=$data->nama_perusahaan?></a>
                                    </div>
                                </div>
                                <div class="jp_job_post_right_overview_btn_wrapper">
                                    <div class="jp_job_post_right_overview_btn">
                                        <ul>
                                            <!-- <li><a href="#"><i class="fa fa-heart-o"></i></a></li> -->
                                            <li><a href="#"><?=$data->tipe?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jp_listing_overview_list_outside_main_wrapper">
                                    <div class="jp_listing_overview_list_main_wrapper">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Tanggal posting</li>
                                                <li><?=$data->tgl?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Masa lowongan</li>
                                                <li><?=$data->tgl_awal?> ~ <?=$data->tgl_akhir?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Lokasi</li>
                                                <li><?=$data->subdistrict_name?>, <?=$data->city?>, <?=$data->province?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-info-circle"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Posisi</li>
                                                <li><?=$data->nama_job?></li>
                                            </ul>
                                        </div>
                                    </div>
                                     <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Min. pendidikan</li>
                                                <li><?=$data->min_pendidikan?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Upah kerja</li>
                                                <li>Rp. <?=($data->upah)?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-th-large"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Kategori</li>
                                                <li><?=$data->category?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-group"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Jumlah dibutuhkan</li>
                                                <li><?=$data->jml_pekerja?> orang</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-cogs"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Skill dibutuhkan</li>
                                                <li><?=$data->skill?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_right_bar_btn_wrapper">
                                        <div class="jp_listing_right_bar_btn">
                                            <ul>
                                                <!-- <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Apply With Facebook</a></li> -->
                                                <?php if($is_pelamar == 1) { ?>
                                                	<li><a class=""  disabled="disabled" tabIndex="-1">Menunggu konfirmasi</a></li>
                                                 <?php }else if($data->is_aktif == 0 || strtotime($data->date_end."23:59:59") < time()){?>
                                                    <li><a class=""  disabled="disabled" tabIndex="-1"><i class="fa fa-info-circle"></i> &nbsp;Lowongan tutup</a></li>
                                                <?php }else {?>
                                                	<li><a class="applynow"><i class="fa fa-plus-circle"></i> &nbsp;Lamar</a></li>
                                                <?php }?>
                                            </ul> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				
                    <div class="jp_listing_left_sidebar_wrapper">
					
                        <div class="jp_job_des">
                            <?php if($this->session->userdata('is_login_pelamar')) { ?>
						          <img src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=<?=base_url()?>portal/jobs/<?=$seo?>&choe=UTF-8" title="QRcode Untuk discan Pelamar" class="centerqr" />
                                  <div class="text-center" style="font-size: 12px; margin-top: -15px">Scan qr diatas untuk melamar pekerjaan</div></br></br>

                            <?php }?>
                            <h2>Deskripsi Pekerjaan</h2>
                            <p><?=(($data->deskripsi == '')? 'Tidak ada deskripsi untuk lowongan ini' : $data->deskripsi)?></p>
                        </div>
                        <div class="jp_job_res">
                            <h2>Tanggung Jawab</h2>
                            <p><?=(($data->responsibility == '')? 'Tidak ada keterangan tanggung jawab untuk lowongan ini' : $data->responsibility)?></p>
                        </div>
                        <div class="jp_job_res jp_job_qua">
                            <h2>Kualifikasi</h2>
                            <p><?=(($data->qualification == '')? 'Tidak ada keterangan kualifikasi untuk lowongan ini' : $data->qualification)?></p>
                        </div>
                        <div class="jp_job_res">
                            <h2>Lain-lain</h2>
                            <p><?=(($data->next_step == '')? 'Tidak ada keterangan lain-lain untuk lowongan ini' : $data->next_step)?></p>
                        </div>
                    </div>
                    <!-- <div class="jp_listing_left_bottom_sidebar_wrapper">
                        <div class="jp_listing_left_bottom_sidebar_social_wrapper">
                            <ul class="hidden-xs">
                                <li>SHARE :</li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li class="hidden-xs"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="hidden-xs"><a href="#"><i class="fa fa-vimeo"></i></a></li>
                            </ul>
                        </div>
                    </div> -->
                    <div class="jp_listing_left_bottom_sidebar_key_wrapper">
                        <ul>
                            <li><i class="fa fa-tags"></i>Keywords :</li>
                            <?php  foreach ($keyword as $kw) { ?>
                                <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_rightside_listing_single_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Gambaran umum</h4>
                                </div>
                                <div class="jp_jop_overview_img_wrapper">
                                    <div class="jp_jop_overview_img">
                                        <img src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_company.jpg')?>" alt="company_img" style="object-fit: contain; width: 100px; height: 95px;" />
                                    </div>
                                </div>
                                <div class="jp_job_listing_single_post_right_cont">
                                    <div class="jp_job_listing_single_post_right_cont_wrapper">
                                        <h4><?=$data->nama_job?></h4>
                                        <a style="color:#184981;padding-top: 10px; font-size: 16px" href="<?=base_url()?>portal/company/<?=$data->profile_url?>"><?=$data->nama_perusahaan?></a>
                                        <!-- <p><?=$data->nama_perusahaan?></p> -->
                                    </div>
                                </div>
                                <div class="jp_job_post_right_overview_btn_wrapper">
                                    <div class="jp_job_post_right_overview_btn">
                                        <ul>
                                            <!-- <li><a href="#"><i class="fa fa-heart-o"></i></a></li> -->
                                            <li><a href="#"><?=$data->tipe?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jp_listing_overview_list_outside_main_wrapper">
                                    <div class="jp_listing_overview_list_main_wrapper">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Tanggal posting</li>
                                                <li><?=$data->tgl?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Masa lowongan</li>
                                                <li><?=$data->tgl_awal?> ~ <?=$data->tgl_akhir?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Lokasi</li>
                                                <li><?=$data->subdistrict_name?>, <?=$data->city?>, <?=$data->province?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-info-circle"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Posisi</li>
                                                <li><?=$data->nama_job?></li>
                                            </ul>
                                        </div>
                                    </div>
                                     <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Min. pendidikan</li>
                                                <li><?=$data->min_pendidikan?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Upah kerja</li>
                                                <li>Rp. <?=($data->upah)?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-th-large"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Kategori</li>
                                                <li><?=$data->category?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-group"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Jumlah dibutuhkan</li>
                                                <li><?=$data->jml_pekerja?> orang</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-cogs"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Skill dibutuhkan</li>
                                                <li><?=$data->skill?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_right_bar_btn_wrapper">
                                        <div class="jp_listing_right_bar_btn">
                                            <ul>
                                                <!-- <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Apply With Facebook</a></li> -->
                                                <?php if($is_pelamar == 1) { ?>
                                                	<li><a class=""  disabled="disabled" tabIndex="-1">Menunggu konfirmasi</a></li>
                                                 <?php }else if($data->is_aktif == 0 || strtotime($data->date_end."23:59:59") < time()){?>
                                                    <li><a class=""  disabled="disabled" tabIndex="-1"><i class="fa fa-info-circle"></i> &nbsp;Lowongan tutup</a></li>
                                                <?php }else {?>
                                                	<li><a class="applynow"><i class="fa fa-plus-circle"></i> &nbsp;Lamar</a></li>
                                                <?php }?>
                                            </ul> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_blog_single_comment_main_wrapper">
                        <div class="jp_blog_single_comment_main">
                            <?php 
                            $type = "";
                            $jml = $this->db->query(" select * from ngi_jobcomment where is_aktif = 1 and id_job = '".$data->id."'")->num_rows(); 

                             if($this->session->userdata('is_login_pelamar')){
                                $type = "pelamar";
                                $profil = $this->db->query('select iduser as id, nama as nama from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();
                             }else if($this->session->userdata('is_login_perusahaan')){
                                $type = "perusahaan";
                                $profil = $this->db->query('select idperusahaan as id, nama_perusahaan as nama from ngi_perusahaan where idperusahaan = "'.$this->session->userdata('id').'"')->row();
                             }

                            ?>

                            <h2><?=$jml?> Komentar</h2>
                        </div>
                        <div class="jp_blog_single_comment_box_wrapper">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div style="padding-bottom: 10px">
                                        <?php 
                                            if($this->session->userdata('is_login_pelamar')){
                                                echo "Anda login sebagai ".$profil->nama;
                                             }else if($this->session->userdata('is_login_perusahaan')){
                                                echo "Anda login sebagai ".$profil->nama;
                                             }else{
                                                echo "Silahkan login untuk bisa berkomentar atau bertanya";
                                             }
                                        ?>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                                        <i class="fa fa-text-height"></i><textarea rows="6" placeholder="Tulis pertanyaan atau komentar anda" id="komentar"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="jp_contact_form_btn_wrapper">
                                        <ul>    
                                            <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                                <li><a class="komen"><i class="fa fa-plus-circle"></i>&nbsp; KOMENTAR</a></li>
                                            <?php }else{?>
                                                <li><a class="login"><i class="fa fa-sign-in"></i>&nbsp; LOGIN</a></li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php 

                                $result = $this->db->query("select a.* from (select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama from (select * from ngi_jobcomment where is_primary = 1 and is_aktif = 1 and id_job = '".$data->id."') a 
                                                            inner join user_pelamar b on a.id_user = b.iduser and a.tipe_user = 'pelamar'

                                                            union

                                                            select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s')as tgl, b.photo, b.nama_perusahaan from (select * from ngi_jobcomment where is_primary = 1 and is_aktif = 1 and id_job = '".$data->id."') a 
                                                            inner join ngi_perusahaan b on a.id_user = b.idperusahaan and a.tipe_user = 'perusahaan') a

                                                            order by a.log desc
                                                            ")->result();

                                foreach ($result as $komentar) {
                                    
                                ?>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 50px">
                                    <div class="jp_blog_comment_main_section_wrapper">
                                        <div class="jp_blog_sin_com_img_wrapper">
                                            <?php if($komentar->tipe_user == "pelamar"){?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($komentar->photo != '')? $komentar->photo : 'default_user.png')?>" alt="blog_img"/>
                                            <?php }else{?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($komentar->photo != '')? $komentar->photo : 'default_company.jpg')?>" alt="blog_img"/>
                                            <?php } ?>
                                        </div>
                                        <div class="jp_blog_sin_com_cont_wrapper">
                                            <ul>
                                                <li><i class="fa fa-user"></i>&nbsp;&nbsp; <a ><?=$komentar->nama?></a></li>
                                                <li><i class="fa fa-calendar"></i>&nbsp;&nbsp; <a><?=$komentar->tgl?></a></li>
                                                <li><i class="fa fa-reply"></i>&nbsp;&nbsp; <a href="#" class="replybtn">Balas</a></li>
                                                <?php if($komentar->id_user == $profil->id && $komentar->tipe_user == $type){ ?>
                                                    <li class="delborder"><i class="fa fa-trash"></i>&nbsp;&nbsp; <a href="#" onclick="hapus('<?=$komentar->id?>')" class="deletebtn">Hapus</a></li>
                                                <?php } ?>

                                            </ul>
                                            <p class="komenarea"><?=$komentar->komen?></p> 
                                            <div class="row reply" style="display: none;">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                                                        <i class="fa fa-text-height"></i><textarea rows="2" placeholder="Tulis pertanyaan atau komentar anda" class="komentar"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_form_btn_wrapper">
                                                        <ul>    
                                                            <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                                                <li><a onclick="balas('<?=$komentar->id?>', this)">KOMENTAR</a></li>
                                                            <?php }else{?>
                                                                <li><a id="login">LOGIN</a></li>
                                                            <?php }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php 

                                $result2 = $this->db->query("select a.* from (select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama from (select * from ngi_jobcomment where is_aktif = 1 and primary_comment = '".$komentar->id."') a 
                                                            inner join user_pelamar b on a.id_user = b.iduser and a.tipe_user = 'pelamar'

                                                            union

                                                            select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama_perusahaan from (select * from ngi_jobcomment where is_aktif = 1 and primary_comment = '".$komentar->id."') a 
                                                            inner join ngi_perusahaan b on a.id_user = b.idperusahaan and a.tipe_user = 'perusahaan') a

                                                            order by a.log asc
                                                            ")->result();

                                foreach ($result2 as $sub_komen) {

                                ?>

                                <div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="jp_blog_comment_main_section_wrapper jp_blog_comment_main_section_wrapper2 jp_blog_post_dots_wrapper">
                                        <div class="jp_blog_sin_com_img_wrapper">
                                            <?php if($sub_komen->tipe_user == "pelamar"){?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($sub_komen->photo != '')? $sub_komen->photo : 'default_user.png')?>" alt="blog_img"/>
                                            <?php }else{?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($sub_komen->photo != '')? $sub_komen->photo : 'default_company.jpg')?>" alt="blog_img"/>
                                            <?php } ?>
                                        </div>
                                        <div class="jp_blog_sin_com_cont_wrapper">
                                            <ul>
                                                <li><i class="fa fa-user"></i>&nbsp;&nbsp; <a href="#"><?=$sub_komen->nama?></a></li>
                                                <li><i class="fa fa-calendar"></i>&nbsp;&nbsp; <a href="#"><?=$sub_komen->tgl?></a></li>
                                                <li><i class="fa fa-reply"></i>&nbsp;&nbsp; <a href="#" class="replybtn">Balas</a></li>
                                                <?php if($sub_komen->id_user == $profil->id && $sub_komen->tipe_user == $type){ ?>
                                                    <li class="delborder"><i class="fa fa-trash"></i>&nbsp;&nbsp; <a href="" onclick="hapus('<?=$sub_komen->id?>')" class="deletebtn">Hapus</a></li>
                                                <?php } ?>
                                            </ul>
                                            <p  class="komenarea"><?=$sub_komen->komen?></p>
                                            <div class="row reply" style="display: none;">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                                                        <i class="fa fa-text-height"></i><textarea rows="2" placeholder="Tulis pertanyaan atau komentar anda" class="komentar"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_form_btn_wrapper">
                                                        <ul>    
                                                            <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                                                <li><a onclick="balas('<?=$komentar->id?>', this)">KOMENTAR</a></li>
                                                            <?php }else{?>
                                                                <li><a class="login"><i class="fa fa-sign-in"></i>&nbsp; LOGIN</a></li>
                                                            <?php }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="jp_blog_small_dots hidden-sm hidden-xs">
                                    </div>
                                    <div class="jp_blog_small_dots2 hidden-sm hidden-xs">
                                    </div>
                                </div>

                                <?php }
                                    echo "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='border-bottom:1px solid #282d39!important;padding-bottom:25px;'></div>";
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php } else { ?>

      <div class="error_page" style="padding-top: 200px !important; padding-bottom: 200px !important;">
        <div class="container">
            <div class="row">
             <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                    <div class="error_page_cntnt">
                        <h2>
                            <span>4</span>
                            <span>0</span>
                            <span>4</span>
                        </h2>
                        <h3>Sorry, This Page Isn't available :(</h3>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. A Aenean sollicitudin, lorem quis bibend Aenean sollicitudin, lorem . <a href="index.html">Home Page</a></p>
                       <!--  <div class="error_page_mail_wrapper">
                            <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a><input type="text" placeholder="Email Address *">
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <div class="modal fade" id="myModal" role="dialog" style="">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Login</h4>
	        </div>
	        <div class="modal-body">
	          <p>Silahkan login dahulu untuk bisa melamar pekerjaan</p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
	          <a type="button" class="btn btn-primary login">Login</a>
	        </div>
	      </div>
	    </div>
	  </div>

    <script type="text/javascript">
        $(function(){
            $(document).on('click','.trending', function(){
                var trend = $(this).prop('id');

                sendData(trend);
            }).on('click touchstart', '.applynow', function(e){
            	$(this).html('<i class="fa fa-circle-o-notch fa-spin"></i> &nbsp;Sedang proses');

            	$.ajax({
            		url: '<?=base_url()?>portal/page/lamar',
            		type: 'post',
            		data: {
            			 <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
            			 id : '<?=$this->session->userdata('id')?>',
            			 id_job : '<?=$data->id?>'

            		},
            		dataType: 'json',
            		success: function(result){
            			if(result.login){
            				if(result.success){
            					$('.applynow').html('Menunggu konfirmasi');
            					$('.applynow').attr('disabled',true);
            					$('.applynow').attr('tabIndex','-1');
            					$('.applynow').removeClass('applynow');
                                alert('Berhasil melamar pekerjaan. \nSilahkan lengkapi lampiran lamaran anda');
                                window.location = '<?=base_url()?>portal/lamaran';
            				}else{
            					alert('Terjadi kesalahan pada server. Silahkan coba lagi.');
            					$('.applynow').html('<i class="fa fa-plus-circle"></i> &nbsp;Lamar');
            				}
            			}else{
            				$('#myModal').modal('show');
            				$('.applynow').html('<i class="fa fa-plus-circle"></i> &nbsp;Lamar');
            			}
            		}

            	});

            	e.preventDefault();

            }).on('click', 'a', function(event){
            	if($(this).is("[disabled]")){
			        event.preventDefault();
			    }

            }).on('click touchstart', '.komen', function(){
                if($('#komentar').val() == ""){
                    $('#komentar').focus()
                }else{
                    post('<?=base_url()?>portal/page/savekomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id_job : '<?=$data->id?>',
                        id_user : '<?=$profil->id?>',
                        tipe_user: '<?=$type?>',
                        komen: $('#komentar').val(),
                        is_primary: 1,
                        url : window.location.href
                    }, "post");
                }

            }).on('click touchstart', '.login', function(){
                post('<?=base_url()?>portal/login', {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    url : window.location.href

                }, "post");

            }).on('click touchstart', '.replybtn', function(e){
                $('.reply').hide();
                $(this).closest('div').find('.reply').show();
                $(this).closest('div').find('.reply').find('textarea').focus();

                e.preventDefault();
            });

        });

        function sendData(trending = ''){
            post('<?=base_url()?>portal/caripekerjaan', {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    keyword : trending
                }, "post");
        }

        function hapus(id){
            if(confirm('Yakin ingin menghapus komen ini?')){
                post('<?=base_url()?>portal/page/hapuskomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id : id, 
                        url : window.location.href
                    }, "post");
            }
        }

        function balas(idkomen, obj){
            var reply = obj.closest('.reply').children[0].children[0].children[1];

            if(reply.value == ""){
                reply.focus();
            }else{
                post('<?=base_url()?>portal/page/savekomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id_job : '<?=$data->id?>',
                        id_user : '<?=$profil->id?>',
                        tipe_user: '<?=$type?>',
                        komen: reply.value,
                        primary_comment : idkomen,
                        is_primary: 0,
                        url : window.location.href
                    }, "post");
            }
        }


        function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }
    </script>
