<div style="widht:100vw; height:100vh">
	<div id="app">
		<div class="row">
			<div class="col-lg-4">
			<section class="cameras">
			  <ul>
				<li v-if="cameras.length === 0" class="empty">No cameras found</li>
				<!--li v-for="camera in cameras">
				  <span v-if="camera.id == activeCameraId" :title="formatName(camera.name)" class="active">{{ formatName(camera.name) }}</span>
				  <span v-if="camera.id != activeCameraId" :title="formatName(camera.name)">
					<a @click.stop="selectCamera(camera)">{{ formatName(camera.name) }}</a>
				  </span>
				</li-->
			  </ul>
			</section>
			<div id="list">
				
			</div>
		  </div>
			<div class="col-lg-8">
				<video width="100%" height="50%" id="preview"></video>
			</div>
	  </div>
	</div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
		<script>
	var qr = '';
	
	$(document).ready( function(){
		var app = new Vue({
					el: '#app',
					data: {
					scanner: null,
					activeCameraId: null,
					cameras: [],
					scans: []
				},

			  mounted: function () {
				var self = this;
				self.scanner = new Instascan.Scanner({ video: document.getElementById('preview'),mirror: false, scanPeriod: 3 ,});
				self.scanner.addListener('scan', function (content, image) {
				  self.scans.unshift({ date: +(Date.now()), content: content });
				  qr = content;
				  kirim(qr);
				});
				Instascan.Camera.getCameras().then(function (cameras) {
				  self.cameras = cameras;
				  if (cameras.length > 0) {
					// self.activeCameraId = cameras[1].id;
					self.scanner.start(cameras[1]);
				  }else {
					console.error('Your Devices Not Support to use');
				  }
				}).catch(function (e) {
				  console.error(e);
				});
			  },
			  methods: {
				formatName: function (name) {
				  return name || 'Camera Ready To Scan!';
				},
				selectCamera: function (camera) {
				  this.activeCameraId = camera.id;
				  this.scanner.start(camera);
				}
			  }
			});
	});

function kirim(qr)
{	
	$.ajax({
	   url: "<?php echo base_url('portal/page/absensi')?>/",
					type: 'post',
					data: {
						 <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
						 iduser : qr

					},
					dataType: 'json',
					
		success: function(result){
			if(result.success){
				alert('Berhasil mendata pengunjung event');
				window.location.href = "<?=base_url()?>portal/absen";
			}else{
				alert('Pencaker sudah tercatat dalam data pengunjung event');
				window.location.href = "<?=base_url()?>portal/absen";
			}
		}
	});
}



</script>