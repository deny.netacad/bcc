<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MY_Controller {
	var $modules = "perusahaan";

	public function index()
	{
		$this->load->view($this->modules.'/index',$this->data);
	}

	function subpage(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}
	/* function excelpencaker(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	} */

	function register(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function caripekerjaan(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function viewmore(){
		$output = '';
		$limit = $_POST['more'] + 3;
		$data = $this->db->query('select a.*, b.nama_job, c.subdistrict_name, c.city, c.province from (select id, upah_kerja, posisi, idkecamatan, title, seo_url, date_format(log, "%d %M %Y") as tgl, log from ngi_joblist where is_aktif = 1 and now() between date_start and date_add(date_end, interval 1 day) and idperusahaan = "'.$this->session->userdata('id').'" ) a
                                                left join ngi_job b on a.posisi = b.id
                                                left join ngi_kecamatan c on c.subdistrict_id = a.idkecamatan
																								order by a.log desc
                                                limit '.$limit)->result();

		foreach ($data as $item) {
			$jmlpelamar = $this->db->query('select id from ngi_jobapplied where id_job = '.$item->id)->num_rows();

			$output .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_cont_wrapper">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3>'.$item->title.'</h3></div>
                                <div style="padding-top: 25px">
                                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-user fa-icon"></i> &nbsp; '.$item->nama_job.'</div>
                                        <div><i class="fa fa-map-marker fa-icon"></i> &nbsp; '.$item->subdistrict_name.', '.$item->city.', '.$item->province.'</div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-money fa-icon"></i> &nbsp;Rp. '.number_format($item->upah_kerja).'</div>
                                        <div><i class="fa fa-calendar fa-icon"></i> &nbsp; '.$item->tgl.'</div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                        <a class="btn btn-warning edit" id="'.$item->id.'"><i class="fa fa-pencil"></i> &nbsp;Edit</a>
                                        <span class="dropdown">
                                          <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-times"></i> Hapus
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a class="delete" id="'.$item->id.'">Hapus Lowongan</a></li>
                                            <li><a class="closes" id="'.$item->id.'">Tutup Lowongan</a></li>
                                          </ul>
                                        </span>
                                        <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Informasi
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="'.base_url().'portal/jobs/'.$item->seo_url.'">Detail</a></li>
                                            <li><a href="'.base_url().'perusahaan/pelamar/'.$item->seo_url.'">Daftar Pelamar <span class="badge btn-danger">'.$jmlpelamar.'</span></a></li>
                                          </ul>
                                        </span>
                                    </div>
                                    <div class="hidden-lg hidden-md col-sm-12 col-xs-12 mobile-btn">
                                        <a class="btn btn-warning edit" id="'.$item->id.'"><i class="fa fa-pencil"></i> &nbsp;Edit</a>
                                        <span class="dropdown">
                                          <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-times"></i> Hapus
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a class="delete" id="'.$item->id.'">Hapus Lowongan</a></li>
                                            <li><a class="closes" id="'.$item->id.'">Tutup Lowongan</a></li>
                                          </ul>
                                        </span>
                                         <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Info
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="'.base_url().'portal/jobs/'.$item->seo_url.'">Detail</a></li>
                                            <li><a href="'.base_url().'perusahaan/pelamar/'.$item->seo_url.'">Daftar Pelamar <span class="badge btn-danger">'.$jmlpelamar.'</span></a></li>
                                          </ul>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
		}

		$numrows = $this->db->query('select a.*, b.nama_job, c.nama_kec from (select upah_kerja, posisi, idkecamatan, title, seo_url, date_format(log, "%d %M %Y") as tgl, log from ngi_joblist where is_aktif = 1 and now() between date_start and date_add(date_end, interval 1 day) and idperusahaan = "'.$this->session->userdata('id').'" ) a
	                                    left join ngi_job b on a.posisi = b.id
	                                    left join ref_kecamatan c on c.id = a.idkecamatan')->num_rows();

		if($numrows >= $limit){
			$output .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_adp_choose_resume_bottom_btn_post jp_adp_choose_resume_bottom_btn_post2">
                            <ul>
                                <li><a href="#" data="'.$limit.'" id="view">View More</a></li>
                            </ul>
                        </div>
                    </div>';
		}

		echo $output;

	}

		function viewmoreselesai(){
		$output = '';
		$limit = $_POST['more'] + 3;
		$data = $this->db->query('select a.*, b.nama_job, c.subdistrict_name, c.city, c.province from (select id, upah_kerja, posisi, idkecamatan, title, seo_url, date_format(log, "%d %M %Y") as tgl from ngi_joblist where is_aktif = 0 or not (now() between date_start and date_add(date_end, interval 1 day))
												and idperusahaan = "'.$this->session->userdata('id').'" ) a
                                                left join ngi_job b on a.posisi = b.id
                                                left join ngi_kecamatan c on c.subdistrict_id = a.idkecamatan
                                                order by a.log desc
                                                limit '.$limit)->result();

		foreach ($data as $item) {
			$jmlpelamar = $this->db->query('select id from ngi_jobapplied where id_job = '.$item->id)->num_rows();

			$output .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_cont_wrapper">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3>'.$item->title.'</h3></div>
                                <div style="padding-top: 25px">
                                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-user fa-icon"></i> &nbsp; '.$item->nama_job.'</div>
                                        <div><i class="fa fa-map-marker fa-icon"></i> &nbsp; '.$item->subdistrict_name.', '.$item->city.', '.$item->province.'</div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-money fa-icon"></i> &nbsp;Rp. '.number_format($item->upah_kerja).'</div>
                                        <div><i class="fa fa-calendar fa-icon"></i> &nbsp; '.$item->tgl.'</div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                        <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Informasi
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="'.base_url().'portal/jobs/'.$item->seo_url.'">Detail</a></li>
                                            <li><a href="'.base_url().'perusahaan/pelamar/'.$item->seo_url.'">Daftar Pelamar <span class="badge btn-danger">'.$jmlpelamar.'</span></a></li>
                                          </ul>
                                        </span>
                                    </div>
                                    <div class="hidden-lg hidden-md col-sm-12 col-xs-12 mobile-btn">
                                         <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Info
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="'.base_url().'portal/jobs/'.$item->seo_url.'">Detail</a></li>
                                            <li><a href="'.base_url().'perusahaan/pelamar/'.$item->seo_url.'">Daftar Pelamar <span class="badge btn-danger">'.$jmlpelamar.'</span></a></li>
                                          </ul>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
		}

		$numrows = $this->db->query('select a.*, b.nama_job, c.nama_kec from (select upah_kerja, posisi, idkecamatan, title, seo_url, date_format(log, "%d %M %Y") as tgl from ngi_joblist where is_aktif = 0 or not (now() between date_start and date_add(date_end, interval 1 day)) and idperusahaan = "'.$this->session->userdata('id').'" ) a
	                                    left join ngi_job b on a.posisi = b.id
	                                    left join ref_kecamatan c on c.id = a.idkecamatan')->num_rows();

		if($numrows >= $limit){
			$output .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_adp_choose_resume_bottom_btn_post jp_adp_choose_resume_bottom_btn_post2">
                            <ul>
                                <li><a href="#" data="'.$limit.'" id="view">View More</a></li>
                            </ul>
                        </div>
                    </div>';
		}

		echo $output;

	}

  function terimapelamar(){
    $data = $this->db->query('select email from user_pelamar where iduser = "'.$_POST['iduser'].'"')->row();
    $data2 = $this->db->query('select b.nama_perusahaan, c.nama_job from (select idperusahaan, posisi from ngi_joblist where id = "'.$_POST['idjob'].'") a
      left join ngi_perusahaan b on a.idperusahaan = b.idperusahaan
      left join ngi_job c on a.posisi = c.id')->row();

    if($this->db->where(array('id' => $_POST['id']))->update('ngi_jobapplied',array('status' => 1))){
        $from_email = "noreply@bogorcareercenter.bogorkab.go.id";
        $to_email = $data->email;

          $pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
         <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>Demystifying Email Design</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        <body style="margin: 0; padding: 0;">
          <table border="0" cellpadding="0" cellspacing="0" width="100%">
           <tr width="100%">
            <td width="0%"></td>
            <td width="100%">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
          <tr>
          <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
          <img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
          </td>
         </tr>
         <tr>
          <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
         <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
          <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
         <b >Pemberitahuan hasil lamaran</b>
        </td>
         </tr>
         <tr>
          <td style="padding: 20px 0 30px 0;" align="center">
           Lamaran pekerjaan anda pada <b>'.$data2->nama_perusahaan.'</b> sebagai <b>'.$data2->nama_job.'</b> dinyatakan <b style="color:green">DITERIMA</b></br>
           Silahkan login untuk melihat detail
          </td>
         </tr>
         <tr>
          <td align="center">
           <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/lamaran">Login</a>
          </td>
         </tr>
         </table>
        </td>
         </tr>
         <tr>
          <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
           <table border="0" cellpadding="0" cellspacing="0" width="100%">
         <tr>
         <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
         &reg; Bogor Career Center, 2019<br/>
        </td>
          <td align="right">
         
        </td>
         </tr>
        </table>
          </td>
         </tr>
         </table>
            </td>
            <td width="0%"></td>
           </tr>
        </table>
        </body>
        </html>';

        $this->load->library('email');
        $config = array();
        $config['protocol'] = 'smtp';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
        $config['smtp_user'] = 'superkarir@bcc.disnaker.bogorkab.go.id';
        $config['smtp_pass'] = 'Superkarir99';
        $config['smtp_port'] = 465;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        // $this->email->set_header("Content-Type", "text/html");
        $this->email->from($from_email, 'Bogor Career Center');
        $this->email->to($to_email);
        $this->email->subject('Bogor Career Center Pemberitahuan Lamaran');
        $this->email->message($pesan);
        #$result = $this->email->send();
        $this->email->send();

        echo "<script type='text/javascript'>alert('Berhasil melakukan aksi');top.location='".$_POST['url']."'</script>";
    }else{
        echo "<script type='text/javascript'>alert('Gagal melakukan aksi');top.location='".$_POST['url']."'</script>";
    }
  }


 function foox(){
         $from_email = "noreply@bogorkab.go.id";
        $to_email = 'dokumendody@gmail.com';

          $pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
         <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>Demystifying Email Design</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        <body style="margin: 0; padding: 0;">
          <table border="0" cellpadding="0" cellspacing="0" width="100%">
           <tr width="100%">
            <td width="0%"></td>
            <td width="100%">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
          <tr>
          <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
          <img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
          </td>
         </tr>
         <tr>
          <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
         <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
          <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
         <b >Pemberitahuan hasil lamaran</b>
        </td>
         </tr>
         <tr>
          <td style="padding: 20px 0 30px 0;" align="center">
           Lamaran pekerjaan anda pada <b> TEST </b> sebagai <b> TEST </b> dinyatakan <b style="color:green">DITERIMA</b></br>
           Silahkan login untuk melihat detail
          </td>
         </tr>
         <tr>
          <td align="center">
           <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/lamaran">Login</a>
          </td>
         </tr>
         </table>
        </td>
         </tr>
         <tr>
          <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
           <table border="0" cellpadding="0" cellspacing="0" width="100%">
         <tr>
         <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
         &reg; Bogor Career Center, 2019<br/>
        </td>
          <td align="right">
         
        </td>
         </tr>
        </table>
          </td>
         </tr>
         </table>
            </td>
            <td width="0%"></td>
           </tr>
        </table>
        </body>
        </html>';

        $this->load->library('email');
        $config = array();
        $config['protocol'] = 'smtp';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
        $config['smtp_user'] = 'superkarir@bcc.disnaker.bogorkab.go.id';
        $config['smtp_pass'] = 'Superkarir99';
        $config['smtp_port'] = 587;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        // $this->email->set_header("Content-Type", "text/html");
        $this->email->from($from_email, 'Bogor Career Center');
        $this->email->to($to_email);
        $this->email->subject('Bogor Career Center Pemberitahuan Lamaran');
        $this->email->message($pesan);
        #$result = $this->email->send();
        $this->email->send();

    
  }





  function tolakpelamar(){
      $data = $this->db->query('select email from user_pelamar where iduser = "'.$_POST['iduser'].'"')->row();
      $data2 = $this->db->query('select b.nama_perusahaan, c.nama_job from (select idperusahaan, posisi from ngi_joblist where id = "'.$_POST['idjob'].'") a
      left join ngi_perusahaan b on a.idperusahaan = b.idperusahaan
      left join ngi_job c on a.posisi = c.id')->row();

      if($this->db->where(array('id' => $_POST['id']))->update('ngi_jobapplied',array('status' => 0, 'reason' => $_POST['reason']))){
          $from_email = "noreply@bogorcareercenter.bogorkab.go.id";
          $to_email = $data->email;

            $pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml">
           <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Demystifying Email Design</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          </head>
          <body style="margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
             <tr width="100%">
              <td width="0%"></td>
              <td width="100%">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
            <tr>
            <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
            <img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
            </td>
           </tr>
           <tr>
            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
           <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
           <b >Pemberitahuan hasil lamaran</b>
          </td>
           </tr>
           <tr>
            <td style="padding: 20px 0 30px 0;" align="center">
             Lamaran pekerjaan anda pada <b>'.$data2->nama_perusahaan.'</b> sebagai <b>'.$data2->nama_job.'</b> dinyatakan <b style="color:red">DITOLAK</b></br>
             Silahkan login untuk melihat detail
            </td>
           </tr>
           <tr>
            <td align="center">
             <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/lamaran">Login</a>
            </td>
           </tr>
           </table>
          </td>
           </tr>
           <tr>
            <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
             <table border="0" cellpadding="0" cellspacing="0" width="100%">
           <tr>
           <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
           &reg; Bogor Career Center, 2019<br/>
          </td>
            <td align="right">
           
          </td>
           </tr>
          </table>
            </td>
           </tr>
           </table>
              </td>
              <td width="0%"></td>
             </tr>
          </table>
          </body>
          </html>';

          $this->load->library('email');
          $config = array();
          $config['protocol'] = 'smtp';
          $config['mailtype'] = 'html';
          $config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
          $config['smtp_user'] = 'superkarir@bcc.disnaker.bogorkab.go.id';
          $config['smtp_pass'] = 'Superkarir99';
          $config['smtp_port'] = 465;
          $this->email->initialize($config);
          $this->email->set_newline("\r\n");
          // $this->email->set_header("Content-Type", "text/html");
          $this->email->from($from_email, 'Bogor Career Center');
          $this->email->to($to_email);
          $this->email->subject('Bogor Career Center Pemberitahuan Lamaran');
          $this->email->message($pesan);
          #$result = $this->email->send();
          $this->email->send();

          echo "<script type='text/javascript'>alert('Berhasil melakukan aksi');top.location='".$_POST['url']."'</script>";
      }else{
          echo "<script type='text/javascript'>alert('Gagal melakukan aksi');top.location='".$_POST['url']."'</script>";
      }
  }


  	function bcemail() {
  		$posisi = $this->db->query('select nama_job from ngi_job where id = "'.$_GET['posisi'].'"' )->row()->nama_job;

  		$email = $this->db->query('select email from user_pelamar where subs_email = "1"')->result();

    if ($this->session->userdata('nama') != "PT Nusantara Global Inovasi"){
        foreach ($email as $item) {
          $from_email = "noreply@bogorcareercenter.bogorkab.go.id";
          $to_email = $item->email;

              $pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
             <head>
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              <title>Demystifying Email Design</title>
              <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            </head>
            <body style="margin: 0; padding: 0;">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
               <tr width="100%">
                <td width="0%"></td>
                <td width="100%">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
              <tr>
              <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
              <img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
              </td>
             </tr>
             <tr>
              <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
             <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
              <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
             <b >Informasi lowongan pekerjaan</b>
            </td>
             </tr>
             <tr>
              <td style="padding: 20px 0 30px 0;" align="center">
               Informasi lowongan baru pada <b>'.$this->session->userdata('nama').'</b> sebagai <b>'.$posisi.'</b> telah tersedia di web. Silahkan login untuk melihat detail lowongan pekerjaan.
              </td>
             </tr>
             <tr>
              <td align="center">
               <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/login">Login</a>
              </td>
             </tr>
             </table>
            </td>
             </tr>
             <tr>
              <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
               <table border="0" cellpadding="0" cellspacing="0" width="100%">
             <tr>
             <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
             &reg; Bogor Career Center, 2019<br/>
            </td>
              <td align="right">
             
            </td>
             </tr>
            </table>
              </td>
             </tr>
             </table>
                </td>
                <td width="0%"></td>
               </tr>
            </table>
            </body>
            </html>';

        $this->load->library('email');
        $config = array();
        $config['protocol'] = 'smtp';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
        $config['smtp_user'] = 'superkarir@bcc.disnaker.bogorkab.go.id';
        $config['smtp_pass'] = 'Superkarir99';
        $config['smtp_port'] = 465;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($from_email, 'Bogor Career Center');
        $this->email->to($to_email);
        $this->email->subject('Bogor Career Center Informasi Lowongan');
        $this->email->message($pesan);

        if($this->email->send()){

        }else{

        }
      }
    }
	}

	function savelowongan(){
		if($this->session->userdata('id') == ""){
			echo json_encode(array('login' => false));
		}else{
			$arrnot = ['id_edit','city_id','province_id','attachment'];

			foreach ($_POST as $key => $value) {
				if(!in_array($key, $arrnot)){
					$data[$key] = $value;
				}
			}


			//cara manual
			// $attach = "";
			// foreach ($_POST['attachment'] as $att) {
			//     $attach .= $att.",";
			// }
			// substr($attach, 0, -1);

			//lebih efektif
			$attach = implode(",", $_POST['attachment']);
			$data['attachment'] = $attach;
			$data['idperusahaan'] = $this->session->userdata('id');

			if($_POST['id_edit'] == ''){
				$data['seo_url'] = time();

				if($this->db->insert('ngi_joblist',$data)){
			        //kirim email
					// exec('bash -c "exec nohup setsid php-cli broadcastemail.php > /dev/null 2>&1 &"');
			        echo json_encode(array('login' => true, 'success' => true));
				}else{
					echo json_encode(array('login' => true, 'success' => false, 'error' => $this->db->_error_message()));
				}
			}else{
				
				if($this->db->where('id',$_POST['id_edit'])->update('ngi_joblist',$data)){
					echo json_encode(array('login' => true, 'success' => true));
				}else{
					echo json_encode(array('login' => true, 'success' => false, 'error' => $this->db->_error_message()));
				}
			}
		}
	}

	function hapuslowongan(){
		if($this->db->where('id',$_POST['id'])->delete('ngi_joblist')){
			echo "Berhasil menghapus lowongan";
		}else{
			echo "Gagal menghapus lowongan";
		}
	}

	function tutuplowongan(){
		if($this->db->where('id',$_POST['id'])->update('ngi_joblist',array('is_aktif' => 0))){
			echo "Berhasil menutup lowongan";
		}else{
			echo "Gagal menutup lowongan";
		}
	}

  function saveprofil(){
    foreach ($_POST as $key => $value) {
      if($key != "id_edit"){
        $input[$key] = $value;
      }
    }

    if($this->db->where('idperusahaan',$_POST['id_edit'])->update('ngi_perusahaan',$input)){
      echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
    }else{
      echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
    }
  }

  function saveakun(){
	  if($_FILES["scanfile"]["size"] != 0){
		$path_parts = pathinfo($_FILES["scanfile"]["name"]);
		$extension = $path_parts['extension'];
		$save_path = BASEPATH.'../assets/img/profile/';
		$filename = $_POST['profile_url']."_comp.".$extension;
		$config['upload_path'] = $save_path;
		$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
		$config['max_size'] = '5000000';
		$config['file_name'] = $filename;
		$this->load->library('upload', $config);

		if(file_exists($save_path.$filename)){
		  unlink($save_path.$filename);
		}
		if (!$this->upload->do_upload('scanfile')){
			echo "<script type='text/javascript'>alert('Gagal mengupload foto.".$this->upload->display_errors()."');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			if($this->db->where('idperusahaan',$_POST['id_edit'])->update('ngi_perusahaan',array('profile_url' => $_POST['profile_url'], 'photo' => $filename))){
			  echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
			}else{
				unlink($save_path.$filename);
			  echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
			}
		}
	  }else{
		if($this->db->where('idperusahaan',$_POST['id_edit'])->update('ngi_perusahaan',array('profile_url' => $_POST['profile_url']))){
		  echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
		}else{
		  echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
		}
	  }

  }

  function savepassword(){
    if($this->db->where('idperusahaan',$_POST['id_edit'])->update('ngi_perusahaan',array('pass' => md5($_POST['pass_baru'])))){
      echo "<script type='text/javascript'>alert('Berhasil mengubah password.');top.location='".base_url()."perusahaan/pengaturan'</script>";
    }else{
      echo "<script type='text/javascript'>alert('Gagal mengubah password');top.location='".base_url()."perusahaan/pengaturan'</script>";
    }
  }

  function checkusername(){
    $q = $this->db->query('select idperusahaan from ngi_perusahaan where username = "'.$_POST['username'].'"');

    if($q->num_rows() == 0){
      echo json_encode(array('available' => true));
    }else{
      echo json_encode(array('available' => false));
    }
  }

  function checkurlprofile(){
    $q = $this->db->query(' select idperusahaan from ngi_perusahaan where profile_url = "'.$_POST['url'].'" ');

    if($q->num_rows() == 0){
      echo json_encode(array('available' => true));
    }else{
      echo json_encode(array('available' => false));
    }
  }

  function checkpassword(){
    $q = $this->db->query('select idperusahaan from ngi_perusahaan where idperusahaan = "'.$this->session->userdata('id').'" and pass = md5("'.$_POST['pass'].'") ');

    if($q->num_rows() == 0){
      echo json_encode(array('available' => false));
    }else{
      echo json_encode(array('available' => true));
    }
  }

  function saveannounce(){
    if($this->db->where('idperusahaan',$_POST['id_edit'])->update('ngi_perusahaan', array('announce' => $_POST['announce']))){
      echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
    }else{
      echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."perusahaan/pengaturan'</script>";
    }
  }

	function cobalogin(){
		$this->session->set_userdata(array('is_login_perusahaan' => true));
	}

	function cobalogout(){
		$this->session->sess_destroy();
	}


	function cariKab(){
		$result = $this->db->query('select city_id, city, type from ngi_kecamatan where province_id = "'.$_POST['id'].'" group by city_id')->result();
		$output = '<option value="">-Pilih Kabupaten/Kota-</option>';

		foreach ($result as $data) {
			if($data->city_id == $_POST['ids']){
				$output .= '<option value="'.$data->city_id.'" selected>'.$data->type.' '.$data->city.'</option>';
			}else{
				$output .= '<option value="'.$data->city_id.'">'.$data->type.' '.$data->city.'</option>';
			}

		}

		echo $output;
	}

	function cariKec(){
		$result = $this->db->query('select subdistrict_id, subdistrict_name from ngi_kecamatan where city_id = "'.$_POST['id'].'" group by subdistrict_id')->result();
		$output = '<option value="">-Pilih Kecamatan-</option>';

		foreach ($result as $data) {
			if($data->subdistrict_id == $_POST['ids']){
				$output .= '<option value="'.$data->subdistrict_id.'" selected>'.$data->subdistrict_name.'</option>';
			}else{
				$output .= '<option value="'.$data->subdistrict_id.'">'.$data->subdistrict_name.'</option>';
			}

		}

		echo $output;
	}

	function searchResult(){
		setlocale(LC_ALL, "id_ID");
		$keyword = $_POST['keyword'];
		$range = $_POST['range'];

		$output = '';
		$rs = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.art_thumb !='' and a.art_intro !='' and (a.art_title like '%".$keyword."%' or a.art_content like '%".$keyword."%') order by a.art_date desc limit $range, 3");
		foreach($rs->result() as $item){
        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
        $src = $xpath->evaluate("string(//img/@src)");
        $src=str_replace('.thumbs','',$src);

        if($item->position == '0'){
        	$art = 'berita';
        	$pop = 'counting';
        }else{
        	$art = 'kepokmas';
        	$pop = ' ';
        }

        $output .= '<article class="entry entry-list">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="entry-media">
                                        <figure>
                                            <a href="'.base_url().'portal/page/'.$art.'/'.$item->art_seo.'" class="'.$pop.'" name="'.$item->art_id.'"><img src="'.$src.'" alt="'.$item->art_title.'" style="background-color: white;" class="cover"></a>
                                        </figure>
                                        <div class="entry-meta">
                                            <span><i class="fa fa-calendar"></i>'.strftime("%d %B %Y", strtotime($item->tmstamp)).'</span>
                                            <a href="#"><i class="fa fa-user"></i>'.$item->nama.'</a>
                                        </div><!-- End .entry-media -->
                                    </div><!-- End .entry-media -->
                                </div><!-- End .col-md-6 -->
                                <div class="col-md-6">
                                    <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="'.base_url().'portal/page/'.$art.'/'.$item->art_seo.'" class="'.$pop.'" name="'.$item->art_id.'">'.$item->art_title.'</a></h2>
                                    <div class="entry-content">
                                        <p>'.strtoupper($item->art_intro).'</p>
                                        <a href="'.base_url().'portal/page/'.$art.'/'.$item->art_seo.'" class="readmore '.$pop.'" name="'.$item->art_id.'">Read more<i class="fa fa-angle-right"></i></a>
                                    </div><!-- End .entry-content -->
                                </div><!-- End .col-md-6 -->
                            </div><!-- End .row -->
                        </article>';
        }

        echo $output;
	}




	/*function saveOrder(){
		if($this->input->post('prod_size')==""){
		$tes="No Size";
		}else{
		$tes=$this->input->post('prod_size');
		}
	$this->load->library('cart');

	$data = array(
               'id'      => $this->input->post('prod_id'),
               'qty'     => $this->input->post('qty'),
               'price'   => $this->input->post('prod_price'),
               'name'    => $this->input->post('prod_name'),
               'options' => array('sponsor' => $this->input->post('sponsor'), 'size' => $tes,'berat' => $this->input->post('prod_berat'))
            );

	$this->cart->insert($data);

	echo json_encode($ret);
	exit;

	}*/

	function saveOrderFront(){
		error_reporting(E_ALL);
		if($this->input->post('prod_size')==""){
		$size_temp="No Size";
		}else{
		$size_temp=$this->input->post('prod_size');
		}

	// Menghindari Inject Element by Dody
	$detailitem=$this->db->query("select * from barang where id='".$this->input->post('id')."'")->row();

	//	echo $detailitem->prod_price;
	$this->load->library('cart');

	$data = array(
               'id'    			  => $this->input->post('id'),
               'qty'   			  => $this->input->post('qty'),
               'price' 	 			=> $detailitem->selling_price,
               'name'  	  => $detailitem->product_name,
               'options' => array('normal_price' => $detailitem->normal_price,
               					 'promo_price' => $detailitem->promo_price,
               					 //'sponsor' => $this->session->userdata('member_user'),
               					 'size' => $size_temp,
               					 'paket_weight' => $detailitem->paket_weight,
               					 'foto_1' => $detailitem->foto_1
               					 )
            );


	if($this->cart->insert($data)){

		$ret['text']="Item berhasil di Input ke Cart";
	}else{
		$ret['log']=$this->db->last_query();
		$ret['text']="Gagal";

	}
error_reporting(E_ALL);
	echo json_encode($ret);
	exit;

	}

	function UpdateCart(){
		$this->load->library('cart');
		$data = array(
               'qty'     => $this->input->post('qty'),
               'rowid'   => $this->input->post('rowid')
              );
		$this->cart->update($data);
		echo json_encode($ret);
		exit;
		}

	function cekid(){
		$this->db->query("update section_calonseller set flag=1 where enkripsi_link='".$this->uri->segment('4')."'");
		echo "<script type='text/javascript'>top.location='".base_url()."'</script>";

	}

	function cek(){
		$this->db->query("update utin_tanya set flag=1 where enkripsi_link='".$this->uri->segment('4')."'");
		echo "<script type='text/javascript'>alert('Pertanyaan berhasil dikonfirmasi.');top.location='".base_url()."'</script>";

	}

	function cekmailmember(){
		$this->db->query("update ngi_calon_member set flag=1 where enkripsi_link='".$this->uri->segment('4')."'");
		echo "<script type='text/javascript'>alert('Silahkan Login Kembali');top.location='".base_url()."portal/page/checkout0'</script>";

	}


	function savePengaduan(){
		$date_s=date('Ymdhis');
		$rs=$this->db->query("select * from section_pengaduan where date(log_pengaduan)=date(now()) and ip='".$this->input->ip_address()."'")->num_rows();

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['register_captcha2'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			//$ret['err'] = 2;
			//$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}

		if($rs>3){

			$ret['text']="Anda dapat melakukan Permintaan Data Kembali Besok ";

		}else{
				$konfirmasi_email=$this->db->query("select * from section_pengaduan where email='".$this->input->post('email')."'")->num_rows();
				if($konfirmasi_email>=10){
				$ret['text'] = "Alamat Email Sudah Di gunakan";
				echo json_encode($ret);
				exit;

				}

				if($rs=$this->db->query("insert into section_pengaduan set log_pengaduan='".date('Y-m-d h:i:s')."',
					name_full='".$this->input->post('name_full')."',
					identity='".$this->input->post('identity')."',
					no_ktp='".$this->input->post('no_ktp')."',
					alamat='".$this->input->post('alamat')."',
					no_hp='".$this->input->post('no_hp')."',
					email='".$this->input->post('email')."',
					kronologis='".$this->input->post('kronologis')."',
					saksi='".$this->input->post('saksi')."',

					ya='".$this->input->post('ya')."',
					tidak='".$this->input->post('tidak')."',
					ip='".$this->input->ip_address()."',
					enkripsi_link='".md5(''.$this->input->ip_address().''.$date_s.''.$this->input->post('email').'')."'
					"))
				{
				$this->portal_mod->kirimmail($this->input->post('email'),'PENGADUAN PERILAKU APARAT SIPIL NEGARA disperindag JATENG ','Salam <b>'.$this->input->post('name_full').'</b><br>
				Pemberitahuan , Akun Anda Tercatat di System Kami dengan IP '.$_SERVER['REMOTE_ADDR'].'<br>
				Pengaduan Anda Sudah Kami Terima , Silahkan Klik Untuk Konfirmasi mail anda <a href="'.base_url().'portal/page/cekid/'.md5(''.$this->input->ip_address().''.$this->input->post('id').''.$date_s.''.$this->input->post('email').'').'"> KLIK DISNI UNTUK KONFIRMASI  </a>  <br>
				Untuk Segera Kami Proses .<br> ');




					$ret['text']="Pengajuan Anda Sudah Kami Terima \nSilahkan Buka email Yang diCantumkan Untuk Verifikasi jika dikontak masuk tidak ada cek di spam";


				}else{
					$ret['text']="Pengaduan Anda Ditolak Oleh System Kami";
				}
		}
		echo json_encode($ret);

	}
	///
	function savepermohonaninformasi(){
		error_reporting(1);
		$date_s=date('Ymdhis');
		$rs=$this->db->query("select * from section_calonseller where date(log_pengajuan)=date(now()) and ip='".$this->input->ip_address()."'")->num_rows();

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['register_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		 if ($row->count == 0)
			{
			$ret['err'] = 2;
			$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}

		if($rs>3){

			$ret['text']="Anda dapat melakukan Permintaan Data Kembali Besok ";

		}else{
				if($this->input->post("scanfile")==""){
				$ret['text'] = "Silahkan Lampirkan Scan KTP Anda Terlebih dahulu";
				echo json_encode($ret);
				exit;

				}


				//$konfirmasi_email=$this->db->query("select * from section_calonseller where registerreg='".$this->input->post('registerreg')."'")->num_rows();
				//if($konfirmasi_email>0){
				//$ret['text'] = "Alamat Email Sudah Di gunakan";
				//echo json_encode($ret);
				//exit;

				//}
				$kode=substr(md5(date('Y-m-d h:i:s')."".$_SERVER['REMOTE_ADDR']),0,5);
				if($rs=$this->db->query("insert into section_calonseller set log_pengajuan='".date('Y-m-d h:i:s')."',
					registername='".$this->input->post('registername')."',
					registeraddr='".$this->input->post('registeraddr')."',
					registertlp='".$this->input->post('registertlp')."',
					registerwant='".$this->input->post('registerwant')."',
					registerfor='".$this->input->post('registerfor')."',
					registerfrom_1='".$this->input->post('registerfrom_1')."',
					registerfrom_2='".$this->input->post('registerfrom_2')."',
					registerget_1='".$this->input->post('registerget_1')."',
					registerget_2='".$this->input->post('registerget_2')."',
					registerwork='".$this->input->post('registerwork')."',
					registerreg='".$this->input->post('registerreg')."',
					scanfile='".$this->input->post('scanfile')."',
					no_ktp='".$this->input->post('no_ktp')."',
					identity='".$this->input->post('identity')."',
					registerpassword='disperindagjateng2017',
					ip='".$this->input->ip_address()."',
					kode='".$kode."',
					enkripsi_link='".md5(''.$this->input->ip_address().''.$date_s.''.$this->input->post('registerreg').'')."'
					"))
				{
						$this->portal_mod->kirimmail($this->input->post('registerreg'),'Selamat Datang Di PPID DISPERINDAG JATENG Reg #'.$kode.'','Salam <b>'.$this->input->post('registernama').'</b><br>
				Kode Registrasi Anda '.$kode.'
				<br>Pemberitahuan , Akun Anda Tercatat di System Kami dengan IP '.$_SERVER['REMOTE_ADDR'].'<br>
				Proses Pengajuan Data Anda Sudah Kami Terima , Silahkan Klik Untuk Konfirmasi mail anda <a href="'.base_url().'portal/page/cekid/'.md5(''.$this->input->ip_address().''.$this->input->post('registerpassword').''.$date_s.''.$this->input->post('registerreg').'').'"> KLIK DISNI UNTUK KONFIRMASI  </a>  <br>
				Untuk Segera Kami Proses .<br> ');


					$ret['text']="\nSilahkan Cek Email Anda untuk Verifikasi , untuk melanjutkan Proses Permohonan data Anda";


				}
				else{
					$ret['text']="Pengajuan Anda Ditolak Oleh System Kami";
				}
		}
		//$ret['queri']=$this-db->last_query();
		echo json_encode($ret);

	}
function saveRegisterCalon(){
		$date_s=date('Ymdhis');
		$rs=$this->db->query("select * from section_calonseller where date(log_pengajuan)=date(now()) and ip='".$this->input->ip_address()."'")->num_rows();

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['register_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			//$ret['err'] = 2;
			//$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}

		if($rs>3){

			$ret['text']="Anda dapat melakukan Permintaan Data Kembali Besok ";

		}else{
				$konfirmasi_email=$this->db->query("select * from section_calonseller where registerreg='".$this->input->post('registerreg')."'")->num_rows();
				if($konfirmasi_email>0){
				$ret['text'] = "Alamat Email Sudah Di gunakan";
				echo json_encode($ret);
				exit;

				}

				if($rs=$this->db->query("insert into section_calonseller set log_pengajuan='".date('Y-m-d h:i:s')."',
					registername='".$this->input->post('registernama')."',
					registeraddr='".$this->input->post('registeraddr')."',
					registertlp='".$this->input->post('registertlp')."',
					registerwant='".$this->input->post('registerwant')."',
					registerfor='".$this->input->post('registerfor')."',
					registerfrom_1='".$this->input->post('registerfrom_1')."',
					registerfrom_2='".$this->input->post('registerfrom_2')."',
					registerget_1='".$this->input->post('registerget_1')."',
					registerget_2='".$this->input->post('registerget_2')."',
					registerwork='".$this->input->post('registerwork')."',
					registerreg='".$this->input->post('registerreg')."',
					registerpassword='disperindagjateng2017',
					ip='".$this->input->ip_address()."',
					enkripsi_link='".md5(''.$this->input->ip_address().''.$date_s.''.$this->input->post('registerreg').'')."'
					"))
				{
						$this->portal_mod->kirimmail($this->input->post('registerreg'),'Selamat Datang Di PPID disperindag JATENG ','Salam <b>'.$this->input->post('registernama').'</b><br>
				Pemberitahuan , Akun Anda Tercatat di System Kami dengan IP '.$_SERVER['REMOTE_ADDR'].'<br>
				Proses Pengajuan Data Anda Sudah Kami Terima , Silahkan Klik Untuk Konfirmasi mail anda <a href="'.base_url().'portal/page/cekid/'.md5(''.$this->input->ip_address().''.$this->input->post('registerpassword').''.$date_s.''.$this->input->post('registerreg').'').'"> KLIK DISNI UNTUK KONFIRMASI  </a>  <br>
				Untuk Segera Kami Proses .<br> ');


					$ret['text']="Pengajuan Anda Sudah Kami Terima \nOperator Kami Akan Segera Menghubungi Anda";


				}else{
					$ret['text']="Pengajuan Anda Ditolak Oleh System Kami";
				}
		}
		echo json_encode($ret);

	}



	function createakunmember(){
		$date_s=date('Ymdhis');

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['register_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			//$ret['err'] = 2;
			//$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}

			$konfirmasi_email=$this->db->query("select * from ngi_calon_member where email='".$this->input->post('InputEmail')."'")->num_rows();
			if($konfirmasi_email>0){
			$ret['text'] = "Alamat Email Sudah Di gunakan";
			echo json_encode($ret);
			exit;

			}

			if($rs=$this->db->query("insert into ngi_calon_member set log_='".date('Y-m-d h:i:s')."',name='".$this->input->post('InputName')."',email='".$this->input->post('InputEmail')."',password='".md5($this->input->post('InputPassword'))."', ip='".$this->input->ip_address()."',enkripsi_link='".md5(''.$this->input->ip_address().''.$this->input->post('InputPassword').''.$date_s.''.$this->input->post('InputEmail').'')."'"))
				{
						$this->portal_mod->kirimmail($this->input->post('InputEmail'),'Hi Shoppers! Selamat Datang Di UNIFIT E-Com ','Salam <b>'.$this->input->post('InputName').'</b><br>
				Pemberitahuan , Akun Anda Tercatat di System UNIFIT <br>
				Proses Pengajuan Anda Sudah Kami Terima , Silahkan Klik Untuk Konfirmasi mail anda <a href="'.base_url().'portal/page/cekmailmember/'.md5(''.$this->input->ip_address().''.$this->input->post('InputPassword').''.$date_s.''.$this->input->post('InputEmail').'').'"> KLIK DISNI UNTUK KONFIRMASI  </a>  <br>
				Untuk Segera Kami Proses .<br> ');


					$ret['text']="Cek Email Anda Untuk Konfirmasi Alamat Email ";


				}else{
					$ret['text']="Pengajuan Anda Ditolak Oleh System Kami";
				}

	echo json_encode($ret);
	}

	function sendemail2(){
	 $this->load->model("portal_mod");
#	echo $this->portal_mod->foo();
	  $this->portal_mod->kirimmail("agindanoe@gmail.com","test email bayu","pesan email test");
	}

	function sendemail(){
                $this->load->library('email');
                $config['protocol'] = 'mail';
                $config['mailpath'] = '/usr/bin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['mailtype'] = 'html';
                $config['wordwrap'] = TRUE;

                $this->email->initialize($config);
                $this->email->from('noreply@nginovasi.com', 'Contact distapang');
                $this->email->to('agindanoe@gmail.com');
                $cc = array("");
                $this->email->cc($cc);

                $html ='';

                $html='isi email';




                $this->email->subject('test email');
                $this->email->message($html);
                #$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

                $this->email->send();

                $this->email->print_debugger();
                $ret['text']="Berhasil terkirim";
                echo json_encode($ret);


        }


	function sendkontak(){
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailpath'] = '/usr/bin/mail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);
		$this->email->from(''.$this->input->post('sendmail').'', ''.$this->input->post('nama').' Contact UNIFIT');
		$this->email->to('ciptakreasimandiriperkasa@yahoo.com');
		$cc = array("");
		$this->email->cc($cc);

		$html ='';

		$html=''.$this->input->post('isi').'';




		$this->email->subject(''.$this->input->post('subjek').'');
		$this->email->message($html);
		#$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

		$this->email->send();

		$this->email->print_debugger();
		$ret['text']="Berhasil terkirim";
		echo json_encode($ret);


	}
	function saveRegister(){
		$arrnot 	= array("","id_edit","member_captcha");
		$keyedit 	= array("");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array("");
		$keynum		= array("");
		$keyout		= array("");
		$ret['text'] = "Berhasil Disimpan";

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['member_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			$ret['err'] = 2;
			$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}


		$data['member_sponsor']=$this->session->userdata('member_code');
		$data['kode_invoice']=md5($this->session->userdata('member_code').'-'.date('d/m/Y h:i:s'));
		$data['log']=date('Y-m-d h:i:s');
		#pembayaran
		$hargapaket=$this->db->query("select harga_jual from ngi_package a
		left join barang b on a.idbarang=b.id
		where a.pack_id='".$this->input->post('pack_id')."'");
		if($this->input->post('idkec')!='0'){
		$hargashipping=$this->db->query("select * from tb_shipping_rate where prov='".$this->input->post('idkec')."' order by price desc limit 1");
		}else if($this->input->post('idkec')=='0'){
		$hargashipping=$this->db->query("select * from tb_shipping_rate where prov='".$this->input->post('idkab')."' order by price desc limit 1");
		}
		$h1=$hargapaket->row();
		$h2=$hargashipping->row();
		$data['tagihan_invoice']=$h1->harga_jual+$h2->price;

		#end of pembayaran
		$this->portal_mod->save("ngi_member_pending",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,array(),array(),$keynum);



		$rs=$this->db->query("select * from ngi_member_pending where kode_invoice='".$data['kode_invoice']."'");
		$item=$rs->row();



		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailpath'] = '/usr/bin/mail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);
		$this->email->from('noreply@unifitindonesia.com', 'Unifit Indonesia');
		$this->email->to($this->input->post('member_email'));
		$cc = array("");
		$this->email->cc($cc);

		$html ='';

		$html='<center><table style="font-family:Verdana;font-size:13px;border-left-color:#0099FF;border-right-color:#0099FF;width:100%">
    <tr><td width="659" height="55" bgcolor="red" align="left" ><font color="#FFFFFF">&nbsp;&nbsp;INVOICE REGISTRASI MEMBER , <B>UNIFIT INDONESIA</B></font></td>
  </tr>
    <tr >
   <td style="background-color:#e7e9ea;"><div style="margin-left:10px"> <br> <p>Halo <b>Salam ,</b><br><br>Terimakasih atas kepercayaan anda kepada <b>UNIFIT INDONESIA</b>.<br> Pendaftaran anda telah kami terima dengan data sebagai berikut :<br /><p>Invoice No. <b>INV.UN.'.$item->id.'</b> </p><br></p>
    	<table width="98%" height="67" >
<tr>
					<td width="105" height="21">Nama</td>
		  <td width="18">:</td>
		  <td width="445">'.$item->member_name.'</td>
		  </tr>
				<tr>
					<td height="20">Alamat</td>
				  <td>:</td>
					<td>'.$item->member_bbm.'</td>
				</tr>
				<tr>
					<td height="16">Sponsor Anda</td>
				  <td>:</td>
					<td>'.$item->member_sponsor.'</td>
				</tr>
				</table>
<p><table><tr><td>Total Pembayaran</td><td>:</td><td>Rp. '.number_format($item->tagihan_invoice).'</td></tr></table></p>
<p>*tagihan sudah termasuk biaya kirim paket</p>
		<p>Silahkan melakukan pembayaran ke :<br />
			<p> Daftar Rekening:<br>

Unifit Indonesia<br>
BNI - Tangerang<br>
No: 0779908878<br>

Unifit Usaha Mandiri<br>
Bank mandiri - Tangerang Ki Samaun<br>
No: 155-00-1555888-8<br>

Unifit Bisnis Mandiri<br>
BCA - Supermall Lippo Karawaci<br>
No: 761-0076500<br></p>


			KONFIRMASI PEMBAYARAN PAKET JOIN<a href="http://unifitindonesia.com/portal/page/confirmpaymen/INV.UN.'.number_format($item->id).'">[klik disini]</a><br>
				</p>
	  <p style="font-size:9px">
	  Jika email ini ada dalam folder SPAM / BULK email anda, maka tandai alamat email dari kami ini bukan SPAM,<br>
selanjutnya kami mengirim email ke Anda, akan masuk ke INBOX email Anda.<br>

  Mohon jangan di replay email ini. Jika anda ada keluhan mengenai pelayanan kami, silahkan hubungi CS unifitindonesia
<p><small style="font-size:9px">Email ini dikirim oleh unifitindonesia[dot]com</small></p>
</div>
      </td>
    </tr>
	<tr><td bgcolor="#6abdf2"></td></tr>
	<tr height="55" bgcolor="#6abdf2">
      <td>
      <P style="font-size:7px"><center>
     <br>
      Email ini ditujukan untuk '.$item->member_name.', karena telah melakukan pendaftaran <BR>di unifitindonesia.com<br>
&copy;2015, <b>PT UNIFIT INDONESIA</b></center></P>


      </td>
    <tr>
</table></center>';




		$this->email->subject('Selamat Datang Calon Member UNIFIT INDONESIA');
		$this->email->message($html);
		#$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

		$this->email->send();

		$this->email->print_debugger();
		//$this->portal_mod->save("ngi_member_pending", $arrnot, $keyedit, $keyindex, $keydate, $keyin, $keyout, $data);

		//$this->session->destroy();
		//echo json_encode($ret);
	}

	function saveConfirm(){

	$arrnot 	= array("","id_edit","member_captcha");
		$keyedit 	= array("");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array("");
		$keynum		= array("");
		$keyout		= array("");
		$ret['text'] = "Berhasil Disimpan";

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['member_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			$ret['err'] = 2;
			$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}


		$data['ip']=$this->input->ip_address();
		$data['log']=date('Y-m-d h:i:s');

		$this->portal_mod->save("ngi_confirm",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,array(),array(),$keynum);

		$no_inv=explode('.',$this->input->post('no_inv'));
		$rs=$this->db->query("select * from ngi_member_pending a
left join ngi_member b on a.member_sponsor=b.member_code
where a.id='".$no_inv[2]."' ");
		$item=$rs->row();

		//if($rs->num()>0){
			$this->load->library('email');
			$config['protocol'] = 'mail';
			$config['mailpath'] = '/usr/bin/mail';
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);
			$this->email->from('noreply@unifitindonesia.com', 'UNIFIT INDONESIA');
			$this->email->to($item->member_mail);
			$cc = array("");
			$this->email->cc($cc);

		$html ='';

					$html='<center><table style="font-family:Verdana;font-size:13px;border-left-color:#0099FF;border-right-color:#0099FF">
				<tr><td width="659" height="55" bgcolor="red" align="left" ><font color="#FFFFFF">&nbsp;&nbsp;KONFIRMASI PEMBAYARAN [LANJUTAN] , <B>UNIFIT INDONESIA</B></font></td>
			  </tr>
				<tr >
			   <td style="background-color:#e7e9ea;"><div style="margin-left:10px"> <br> <p>Halo, <b>Salam ,</b><br>
			    Data Konfirmasi Anda sedang dalam proses, setelah pembayaran terverifikasi, kami akan memberikan email respon informasi mengenai PIN Pendaftaran anda telah kami kirimkan ke sponsor Anda, Berikut data Sponsor Anda :<br />
			     <p>Invoice No. <b>INV.UN.'.$item->id.'</b> </p>
				   </p>

					<table width="98%" height="148" >
<tr>
								<td width="105" height="18">Nama</td>
			  <td width="18">:</td>
					  <td width="445">'.$item->member_name.'</td>
					  </tr>
							<tr>
								<td height="20">Alamat</td>
							  <td>:</td>
								<td>'.$item->member_bbm.'</td>
							</tr>

							<tr>
								<td height="36" bgcolor="#CCCCCC" colspan="3" align="center">Detail Sponsor Anda</td>
						</tr>
							 <tr>
								<td height="16">No.Hp</td>
							  <td>:</td>
								<td>'.$item->member_hp.'</td>
							</tr>
							 <tr>
								<td height="18">Email</td>
							  <td>:</td>
								<td>'.$item->member_mail.'</td>
							</tr>


			     </table>
				<p> Daftar Rekening:<br>

Unifit Indonesia<br>
BNI - Tangerang<br>
No: 0779908878<br>

Unifit Usaha Mandiri<br>
Bank mandiri - Tangerang Ki Samaun<br>
No: 155-00-1555888-8<br>

Unifit Bisnis Mandiri<br>
BCA - Supermall Lippo Karawaci<br>
No: 761-0076500<br></p>

			<p>Harap hubungi sponsor di atas untuk proses registrasi Anda.  <br><p style="font-size:9px">Jika email ini ada dalam folder SPAM / BULK email anda, maka tandai alamat email dari kami ini bukan SPAM ,<br>
			selanjutnya kami mengirim email ke Anda, akan masuk ke INBOX email Anda.<br>

			  Mohon jangan di replay email ini. Jika anda ada keluhan mengenai pelayanan kami, silahkan hubungi CS unifitindonesia
			<p><small style="font-size:9px">Email ini dikirim oleh unifitindonesia[dot]com</small></p>
			   </div>
				  </td>
				</tr>
				<tr><td bgcolor="red"></td></tr>
					<tr height="55" bgcolor="red">
				  <td>
				  <P style="font-size:7px"><center>
				 <br>
				  Email ini ditujukan untuk Konfirmasi Detail Sponsor, karena telah melakukan pendaftaran <BR>di unifitindonesia.com<br>
			&copy;2015, <b>UNIFIT CORP</b></center></P>


				  </td>
				<tr>
			</table></center>';




		$this->email->subject('Selamat Datang Calon Member UNIFIT INDONESIA');
		$this->email->message($html);
		#$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

		$this->email->send();

		$this->email->print_debugger();
		//}


	}

	function saveConfirm2(){

	$arrnot 	= array("","id_edit","member_captcha");
		$keyedit 	= array("");
		$keyindex 	= array("","id");
		$keydate	= array("");
		$keyin		= array("");
		$keynum		= array("");
		$keyout		= array("");
		$ret['text'] = "Berhasil Disimpan";

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['member_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			$ret['err'] = 2;
			$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}


		$data['ip']=$this->input->ip_address();
		$data['log']=date('Y-m-d h:i:s');

		$this->portal_mod->save("ngi_confirm",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,array(),array(),$keynum);

		#$no_inv=explode('.',$this->input->post('no_inv'));
		$rs=$this->db->query("select * from ngi_orderonline_detail a
left join ngi_member b on a.member_code=b.member_code
where a.no_invoice='".$this->input->post('no_inv')."' limit 1");
		$item=$rs->row();

		//if($rs->num()>0){
			$this->load->library('email');
			$config['protocol'] = 'mail';
			$config['mailpath'] = '/usr/bin/mail';
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);
			$this->email->from('noreply@unifitindonesia.com', 'UNIFIT INDONESIA');
			$this->email->to($item->member_mail);
			$cc = array("");
			$this->email->cc($cc);

		$html ='';

					$html='DEAR MEMBER ,<BR>PEMBAYARAN TELAH DITERIMA SILAHKAN TUNGGU PROSES DARI KAMI.';




		$this->email->subject('CONFIRM UNIFIT INDONESIA');
		$this->email->message($html);
		#$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

		$this->email->send();

		$this->email->print_debugger();
		//}


	}

  function getUser($iduser){
  	$rs = $this->db->query("select * from ngi_member where member_user='".$iduser."' or member_mail='".$iduser."'");
    return $rs;
  }


 function getUsermember($email){
  	$rs = $this->db->query("select * from ngi_calon_member where email='".$email."' ");
    return $rs;
  }



  function savecicilan(){
	  $rs=mysql_query("update ngi_orderonline set cicilan='".$this->input->post('cicilan')."',cicilan_periode='".$this->input->post('cicilan_periode')."' where signature='".$_POST['signature']."'");


  }
  function cekDATA(){
	$datanya = $_POST['datanya'];
	$field=$_POST['fieldnya'];
	$table=$_POST['tablenya'];

	$rs = $this->db->query("select * from $table where $field =\"".$datanya."\" and cat=1");
	$item=$rs->row();
	if($rs->num_rows() > 0){
		$data = "0";
		$ret['price']=$item->price;
	}
	else if($datanya==""){
	$data = "0";
	}
	else{
		$data = "1";
		$ret['price']=$item->price;
	}
	echo json_encode($ret);

	//echo $data;
	}
	function delorder() {
	$this->load->library('cart');

    $data = array(
        'rowid'   => $this->input->post('rowid'),
        'qty'     => 0
    );

    $this->cart->update($data);
	$ret['text']="Berhasil Di hapus";
	echo json_encode($ret);
	}



  function login(){
	$username = $this->input->post("username");
	$rs = $this->getUser($username);

	if($rs->num_rows() > 0){
      $user = $rs->row();

      if(($username == $user->member_user || $username == $user->member_mail ) && (md5($this->input->post("userpass")) == $user->member_pass)){ // cek password

		$sesi = array(
		'username'=>$user->member_name
		,'member_code'=>$user->member_code
		,'iduser'=>$user->member_code
		,'is_login_lamarizkstore_member'=>1

		);


		$this->session->set_userdata($sesi);
		$this->portal_mod->updateMemberPrice();

		$ret['text']="Login Berhasil";
		echo json_encode($ret);
		exit;

      }else{
		$ret['text']="Kombinasi Data Salah";
		echo json_encode($ret);
		exit;
	  }
    }else{
		$ret['text']="Kombinasi Data Salah";
		echo json_encode($ret);
		exit;
	}
  }



	function saveAddres(){

	$ret['text']="Data Gagal Di Input";
	$ret['link']="checkout1";
	$sql="insert into cart_member set first_name='".$this->input->post('first_name')."' ,last_name='".$this->input->post('last_name')."',email='".$this->input->post('email')."',add_1='".$this->input->post('add_1')."',add_2='".$this->input->post('add_2')."',city='".$this->input->post('city')."',zip='".$this->input->post('zip')."',country='".$this->input->post('country')."',add_information='".$this->input->post('add_information')."',mobile_phone='".$this->input->post('mobile_phone')."'";

	if($this->db->query($sql)){
		$ret['text']="Berhasil Tercatat";
		$ret['link']="checkout2";


	}

		echo json_encode($ret);

		}





   function loginmember(){
	$email = $this->input->post("InputEmail");
	$rs = $this->getUsermember($email);

	if($rs->num_rows() > 0){
      $user = $rs->row();

      if($email == $user->email  && md5($this->input->post("InputPassword")) == $user->password){ // cek password

		$sesi = array(
		'username'=>$user->name
		,'email'=>$user->email
		,'iduser'=>$user->id
		,'is_login_unifit_portal'=>1

		);


		$this->session->set_userdata($sesi);
		//$this->portal_mod->updateMemberPrice();

		$ret['text']="Login Berhasil";
		echo json_encode($ret);
		exit;

      }else{
		$ret['text']="Kombinasi Data Salah";
		echo json_encode($ret);
		exit;
	  }
    }else{
		$ret['text']="Kombinasi Data Salah";
		echo json_encode($ret);
		exit;
	}
  }

function checkoutcart(){
	session_start();

		$sesi = array(

		'unik'=>md5(session_id()."".date('Y-m-d h:i:s')."".$this->input->ip_address())
		,'is_addr'=>1

		);

		$this->session->set_userdata($sesi);

	$data2['idprov']=$this->input->post("idprov");
	$data2['idkab']=$this->input->post("idkab");
	$data2['idkec']=$this->input->post("idkec");
	$data2['member_name']='';
	$data2['member_mail']='';
	$data2['member_bbm']='';
	$data2['member_hp']='';
	$data2['ip']=$this->input->ip_address();
	$data2['unik']=$this->session->userdata('unik');
	$data2['biaya_kg']=$this->input->post("ValTotalShipping");
	$data2['biaya_total']=$this->input->post("ValGrandTotal");
	$data2['transaction_date']=date('Y-m-d h:i:s');

	if($this->input->post("idprov")=='' or $this->input->post("idkab")=='' or $this->input->post("idkec")==''){
		$ret['text']="Proses menyimpan data Anda gagal Lengkapi data Shipping Anda ! ";
	}
	else if($this->db->insert("ngi_orderonline",$data2)){
		$ret['text']="Data Anda Sudah Kami Simpan Sementara , Silahkan Lanjutkan Proses Berikutnya";

		$this->load->library('cart');
		foreach($this->cart->contents() as $item){
		$data['unik']=$this->session->userdata('unik');
		$data['prod_id']=$item['id'];
		$data['qty']=$item['qty'];
		$data['prod_price']=$item['price'];
		$data['prod_name']=$item['name'];
		$data['sponsor']=$item['options']['sponsor'];
		$data['size']=$item['options']['size'];
		$data['berat']=$item['options']['berat'];
		$data['biaya_kg']=$this->input->post("ValTotalShipping");
		$this->db->insert("ngi_orderonline_detail",$data);

	}


	}else{
		$ret['text']="Proses menyimpan data Anda gagal Lengkapi data Shipping Anda! ";

	}


	$ret['idkab']=$this->input->post("idkab");
	$ret['idkec']=$this->input->post("idkec");

	echo json_encode($ret);

}



function checkoutcartupdate(){

$rs=$this->db->query("update ngi_orderonline set member_name='".$this->input->post('name')."',member_bbm='".$this->input->post('address')."',
member_mail='".$this->input->post('email')."',member_code='".$this->session->userdata('member_code')."',member_hp='".$this->input->post('mobilephone')."' where unik='".$this->session->userdata('unik')."'");

//==== MENGIRIM MAIL ====//

	$datamail2=$this->db->query("select *,date_format(log,'%d,%M %Y %h:%i:%s') as log from ngi_orderonline where unik='".$this->session->userdata('unik')."'")->row();

	$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailpath'] = '/usr/bin/mail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);
		$this->email->from('noreply@lamarizkstore.com', 'LAMARIZK STORE');
		$this->email->to($datamail2->member_mail);
		$cc = array("");
		$this->email->cc($cc);
		$html='
		<body>
		<div class="invoice-box" style="max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;
        color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;
        line-height:inherit;
        text-align:left;">
            <tr class="top">
                <td colspan="2" style="padding:5px;
        vertical-align:top;">
                    <table style="width:100%;
        line-height:inherit;
        text-align:left;">
                        <tr>
                            <td class="title" style="font-size:45px;
        line-height:45px;
        color:#333;">
                                <img src="http://lamarizk.com/lm_store.png" style="width:100%; max-width:300px;">
                            </td>

                            <td align=right>
                                Invoice #: '.$datamail2->no_invoice.'<br>
                                Created: '.$datamail2->log.'<br>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information" style=" padding-bottom:40px;">
                <td colspan="2">
                    <table style="width:100%;
        line-height:inherit;
        text-align:left;">
                        <tr>
                            <td>
                                Lamarizk Store .<br>
                                JL. RAYA PASAR JUMAT NO 45 J-K<br>
                                JAKARTA SELATAN
                            </td>

                            <td>
                               '.$datamail2->member_name.'.<br>
                                 '.$datamail2->member_bbm.'<br>
                                '.$datamail2->member_hp.'
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="heading" style="  background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;">
                <td style="height:50px;padding:20px" >
                    Payment Method
                </td>

                <td align="right" style="height:50px;padding:20px">

                </td>
            </tr>

            <tr class="details">
                <td style="padding-left:20px" >
                    DETAIL CART
                </td>

                <td align="right" style="padding-right:20px">

                </td>
            </tr>

            <tr class="heading" style="  background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;">
                <td style="    border-bottom:1px solid #eee;padding-left:20px">
                    Item
                </td>

                <td align="right" style="padding-right:20px">
                    Price
                </td>
            </tr>';


			$noinv2=$this->db->query("select * from ngi_orderonline_detail where no_invoice='".$datamail2->no_invoice."'");
			foreach($noinv2->result() as $item){
				$total[]=$item->prod_price;
				$grandtotal[]=$item->prod_price*$item->qty;
				$total_berat[]=$item->berat;
				$html.='
				<tr class="item" style="border-bottom:1px solid #eee;padding-left:20px">
                <td style="padding-left:20px">
                   '.$item->prod_name.' ('.$item->qty.')
                </td>

                <td align="right" style="padding-right:20px">
                   '.number_format($item->prod_price).'
                </td>
            </tr>';

			$no++;}
            $html.='





            <tr class="total" style=" border-top:2px solid #eee;
        font-weight:bold;">
                <td></td>

                <td align="right" style="padding-right:20px">
                   Total: '.number_format(array_sum($grandtotal)).'
                </td>
            </tr>
        </table>
    </div>
	</body>
	</html>';
		$this->email->subject('invoice lamarizkstore');
		$this->email->message($html);
		#$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

		$this->email->send();

		$this->email->print_debugger();
		$this->cart->destroy();
	//== MAIL SELESAI ==//

}
function SendmailInvoice(){

		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailpath'] = '/usr/bin/mail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);
		$this->email->from('noreply@lamarizkstore.com', 'LAMARIZK STORE');
		$this->email->to("dokumendody@gmail.com");
		$cc = array("");
		$this->email->cc($cc);
		$html='
		<body>
		<div class="invoice-box" style="max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;
        color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;
        line-height:inherit;
        text-align:left;">
            <tr class="top">
                <td colspan="2" style="padding:5px;
        vertical-align:top;">
                    <table style="width:100%;
        line-height:inherit;
        text-align:left;">
                        <tr>
                            <td class="title" style="font-size:45px;
        line-height:45px;
        color:#333;">
                                <img src="http://lamarizk.com/lm_store.png" style="width:100%; max-width:300px;">
                            </td>

                            <td align=right>
                                Invoice #: 123<br>
                                Created: January 1, 2015<br>
                                Due: February 1, 2015
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information" style=" padding-bottom:40px;">
                <td colspan="2">
                    <table style="width:100%;
        line-height:inherit;
        text-align:left;">
                        <tr>
                            <td>
                                Next Step Webs, Inc.<br>
                                12345 Sunny Road<br>
                                Sunnyville, TX 12345
                            </td>

                            <td>
                                Acme Corp.<br>
                                John Doe<br>
                                john@example.com
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="heading" style="  background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;">
                <td style="height:50px;padding:20px" >
                    Payment Method
                </td>

                <td align="right" style="height:50px;padding:20px">
                    Check #
                </td>
            </tr>

            <tr class="details">
                <td style="padding-left:20px" >
                    Check
                </td>

                <td align="right" style="padding-right:20px">
                    1000
                </td>
            </tr>

            <tr class="heading" style="  background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;">
                <td style="    border-bottom:1px solid #eee;">
                    Item
                </td>

                <td align="right">
                    Price
                </td>
            </tr>

            <tr class="item" style="    border-bottom:1px solid #eee;">
                <td>
                    Website design
                </td>

                <td align="right">
                    $300.00
                </td>
            </tr>



            <tr class="item last">
                <td>
                    Domain name (1 year)
                </td>

                <td align="right">
                    $10.00
                </td>
            </tr>

            <tr class="total" style=" border-top:2px solid #eee;
        font-weight:bold;">
                <td></td>

                <td align="right">
                   Total: $385.00
                </td>
            </tr>
        </table>
    </div>
</body>
</html>';
		$this->email->subject('invoice lamarizkstore');
		$this->email->message($html);
		//$this->email->attach('http://lamarizk.com/lm_store.png');

		$this->email->send();

		$this->email->print_debugger();
		echo "sip";

}


//selesai belanja
function saveFinish(){

		$expiration = time()-6000; // detikan
		/*$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['member_captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			$ret['err'] = 2;
			$ret['error'] = true;
			$ret['text'] = "Capcha Salah";
			echo json_encode($ret);
			exit;
			}

		*/

		$this->load->library('cart');
		$ret['text']="Data Tersimpan";
		$data2['idprov']=$this->input->post("idprov");
		$data2['idkab']=$this->input->post("idkab");
		$data2['idkec']=$this->input->post("idkec");
		$data2['member_name']=$this->input->post("member_name");
		$data2['member_mail']=$this->input->post("member_mail");
		$data2['member_bbm']=$this->input->post("member_bbm");
		$data2['member_hp']=$this->input->post("member_hp");
		$data2['ip']=$this->input->ip_address();
		$data2['unik']=$this->input->post("unik");
		$data2['transaction_date']=date('Y-m-d h:i:s');
		$this->db->insert("ngi_orderonline",$data2);
		//echo $this->db->last_query();

	foreach($this->cart->contents() as $item){
		$data['unik']=$this->input->post("unik");
		$data['prod_id']=$item['id'];
		$data['qty']=$item['qty'];
		$data['prod_price']=$item['price'];
		$data['prod_name']=$item['name'];
		$data['sponsor']=$item['options']['sponsor'];
		$data['size']=$item['options']['size'];
		$data['berat']=$item['options']['berat']/10;
		$data['biaya_kg']=$this->input->post("biaya_kg");
		$this->db->insert("ngi_orderonline_detail",$data);

	}

		$sesi = array(

		'unik'=>$this->input->post("unik")
		,'is_addr'=>1

		);

		$this->session->set_userdata($sesi);





		if($this->session->userdata('member_code')!=''){
			$rs=$this->db->query("select member_mail from ngi_member where member_code='".$this->session->userdata('member_code')."'");
			$data=$rs->row();
			$member_mail=$data->member_mail;
		}else{
			$member_mail=$this->input->post('member_mail');
		}

		$datanoinv=$this->db->query("select * from ngi_orderonline where unik='".$this->input->post("unik")."'")->row();


		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailpath'] = '/usr/bin/mail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);
		$this->email->from('noreply@unifitindonesia.com', 'Unifit Indonesia');
		$this->email->to($member_mail);
		$cc = array("");
		$this->email->cc($cc);



		//$html =' TEST MAIL "'.$datanoinv->no_invoice.'"/"'.$datanoinv->ip.'"/"'.$datanoinv->unik.'"/"'.$datanoinv->member_bbm.'"/"'.$datanoinv->total.'"';

		$html='
		<h3>Invoice Belanja <font bgcolor="red">UNIFIT</font></h3><br>
		No.INVOICE '.$datanoinv->no_invoice.'<br>
		<br>

		 <table style="width:100%" >
						<thead >
						<tr bgcolor=#D32F2F height=50px><td  align=center >No</td><td align=center>Product</td><td align=center>weigh</td><td align=center>Quantity</td><td align=center>Price</td></tr>
						</thead>
						<tbody >
						<tr height="10px"><td ></td><td></th><td></td></tr>';

			$no=1;
			$noinv2=$this->db->query("select * from ngi_orderonline_detail where no_invoice='".$datanoinv->no_invoice."'");
			foreach($noinv2->result() as $item){
				$total[]=$item->prod_price;
				$grandtotal[]=$item->prod_price*$item->qty;
				$total_berat[]=$item->berat;
				$html.='<tr ><td align=center>'.$no.'</td><td>'.$item->prod_name.'</td><td align=center><span style="">'.$item->berat.'</span></td><td  align=center style="background-color:;width:50px;border-radius:20">'.$item->qty.'</td><td  align=right>'.number_format($item->prod_price).'</td></tr>	';
			$no++;}


			$html.='
				<tr style="background-color:;height:80px"><td></td><td></td><td  align=left colspan=2>SUB TOTAL</td><td align=right style="font-size:20px"><small>IDR</small> '.number_format(array_sum($total)).'</td></tr>

					<input type="hidden" value="1019000" name="totalbelanja" id="totalbelanja"/>

				<tr style="background-color:;height:0px"><td></td><td></td><td align=left colspan=2>Berat Total (kilos)<br>
					Pembulatan berat (kilos)
					</td><td align=right style="font-size:20px"><small>'.array_sum($total_berat).'</small><div>'.ceil(array_sum($total_berat)).'</div></td></tr>

					<input type=hidden name="total_berat" id="total_berat" value="0"/>
				<tr style="background-color:;height:0px"><td></td><td></td><td align=left colspan=2>Delivery Home<br>
					Delivery In 3 to 5 working day
					</td><td align=right style="font-size:20px"><small><div id="info_perkg" value=""></div></small><div id="info"  value=""/></div>'.number_format($item->biaya_kg*ceil(array_sum($total_berat))).'</td></tr>
					<tr style="background-color:;height:80px"><td></td><td></td><td  align=left colspan=2>GRAND TOTAL</td><td align=right style="font-size:20px"><small>'.number_format(array_sum($grandtotal)+$item->biaya_kg*ceil(array_sum($total_berat))).' IDR</small><div id="grandtotal"></div></td></tr>


					<tr style="background-color:;height:0px"><td></td><td></td><td align=left colspan=2><br>

					</td><td align=right style="font-size:20px;height:80px;" valign="bottom">
					<form id="form1" name="form1" action="http://unifitindonesia.com/portal/page/saveFinish" class="form-horizontal" method="post" enctype="multipart/form-data">
					<input type="hidden" id="csrf_unifitp_name" name="csrf_unifitp_name" value="d7867b8e74736200bd1c2a87e9c7fe0c" />
					<input type="hidden" id="berat_total" name="berat_total" value="0" class="form-control" />
					<input type="hidden" id="biaya_kg" name="biaya_kg" value="" class="form-control" />
					<input type="hidden" id="biaya_total" name="biaya_total" value="" class="form-control" />
					<input type="hidden" id="unik" name="unik" value="e25808" class="form-control" />


					</td>

					</tr>
					</tbody>
					</table>
					<br>
						 <table style="width:100%" >
					<thead >
					<tr><td width=33% align=center bgcolor="#CFD8DC" height="50px">
				Unifit Indonesia<br>
				BNI - Tangerang<br>
				No: 0779908878<br></td>

				<td  width=33%  align=center bgcolor="#B0BEC5">Unifit Usaha Mandiri<br>
				Bank mandiri - Tangerang Ki Samaun<br>
				No: 155-00-1555888-8<br>
				</td><td  width=33%  align=center bgcolor="#CFD8DC">

				Unifit Bisnis Mandiri<br>
				BCA - Supermall Lippo Karawaci<br>
				No: 761-0076500<br></p></td></tr>
				</thead>
					</table>
				KONFIRMASI PEMBAYARAN <a href="http://unifitindonesia.com/portal/page/confirmpay/'.$datanoinv->no_invoice.'"> [KLIK DISINI]</a>

					<br><p style="font-size:9px">Jika email ini ada dalam folder SPAM / BULK email anda, maka tandai alamat email
					dari kami ini
					bukan SPAM,<br>
				selanjutnya kami mengirim email ke Anda, akan masuk ke INBOX email Anda.<br>

				Mohon jangan di replay email ini. Jika anda ada keluhan mengenai pelayanan kami, silahkan hubungi
				CS unifitindonesia
				<p><small style="font-size:9px">Email ini dikirim oleh unifitindonesia[dot]com</small></p>


				<br>
				<center>
				Email ini ditujukan untuk '.$datanoinv->member_name.', karena telah melakukan belanja <BR>di unifitindonesia.com<br>
				&copy;2015, <b>UNIFIT CORP</b></center>


		';




		$this->email->subject('Terima Kasih telah Berbelanja di UNIFIT INDONESIA');
		$this->email->message($html);
		#$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

		$this->email->send();

		$this->email->print_debugger();

		$this->cart->destroy();


		$ret['no_invoice']=$datanoinv->no_invoice;
		echo json_encode($ret);
}

	function savePertanyaan(){
		$date_s=date('Ymdhis');
		$rs=$this->db->query("select * from utin_tanya where date(log )=date(now()) and ip='".$this->input->ip_address()."'")->num_rows();

		$expiration = time()-6000; // detikan
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($this->input->post('captcha'), $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		if ($row->count == 0)
			{
			//$ret['err'] = 2;
			//$ret['error'] = true;
			$ret['text'] = "captcha salah";
			$ret['stts'] = 0;
			echo json_encode($ret);
			exit;
			}

		if($rs>3){

			$ret['text']="Anda dapat melakukan Permintaan Data Kembali Besok ";
			$ret['stts'] = 1;

		}else{
				$konfirmasi_email=$this->db->query("select * from utin_tanya where email='".$this->input->post('email')."'")->num_rows();
				if($konfirmasi_email>=10){
				$ret['text'] = "Alamat Email Sudah Di gunakan";
				echo json_encode($ret);
				exit;

				}

				if($rs=$this->db->query("insert into utin_tanya set log='".date('Y-m-d h:i:s')."',
					nama='".$this->input->post('nama')."',
					ktp='".$this->input->post('ktp')."',
					email='".$this->input->post('email')."',
					pertanyaan='".$this->input->post('pertanyaan')."',
					ip='".$this->input->ip_address()."',
					enkripsi_link='".md5(''.$this->input->ip_address().''.$date_s.''.$this->input->post('email').'')."'
					"))
				{
				$this->portal_mod->kirimmail($this->input->post('email'),'MARI BERTANYA disperindag JATENG ','Salam <b>'.$this->input->post('nama').'</b><br>
				Pemberitahuan , Akun Anda Tercatat di System Kami dengan IP '.$_SERVER['REMOTE_ADDR'].'<br>
				Pertanyaan Anda Sudah Kami Terima , Silahkan Klik Untuk Konfirmasi mail anda <a href="'.base_url().'portal/page/cek/'.md5(''.$this->input->ip_address().''.$date_s.''.$this->input->post('email').'').'"> KLIK DISNI UNTUK KONFIRMASI  </a>  <br>
				Untuk Segera Kami Proses .<br> ');




					$ret['text']="Pengajuan Anda Sudah Kami Terima \nSilahkan Buka email Yang diCantumkan Untuk Verifikasi jika dikontak masuk tidak ada cek di spam";
					$ret['stts'] = 1;


				}else{
					$ret['text']="Pengaduan Anda Ditolak Oleh System Kami";
					$ret['stts'] = 1;
				}
		}
		echo json_encode($ret);
	}

	function do_upload_scanfile_ktp(){		$path_parts = pathinfo($_FILES["scanfile"]["name"]);		$extension = $path_parts['extension'];				$config['upload_path'] = BASEPATH.'../upliddir/ktp/';		$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';		$config['max_size']	= '5000000';		$config['file_name'] = $this->input->post('filename').".".$extension;		$this->load->library('upload', $config);		if ( ! $this->upload->do_upload('scanfile'))		{			$error = array('error' => $this->upload->display_errors());			echo json_encode($error);		}		else		{			$data = array('upload_data' => $this->upload->data());			echo json_encode($data);		}			}

	function regAkun(){
		$input = array(
			'email' => $this->input->post('email',true),
			'password' => md5($this->input->post('password',true)),
			'password_ori' => $this->input->post('password',true),
			'nama' => $this->input->post('nama',true),
			'no_ktp' => $this->input->post('noktp',true),
			'link_enkripsi' => md5($this->input->ip_address().strtotime('now'))
		);

		$user = $this->db->query("select iduser from user where email = '".$this->input->post('email',true)."' ");
		if($user->num_rows() > 0){
				echo json_encode(array('success' => false, 'teks' => 'Gagal mendaftarkan akun. Email sudah digunakan.'));
		}else{
			if($this->db->insert('user',$input)){
				$pesan = '<div class="card text-center">
						  <div class="card-header">
						    Selamat datang di Web Dinas Ketahanan Pangan Kota Semarang.
						  </div>
						  <div class="card-body">
						    <p class="card-text">Untuk bisa menggunakan akun anda, silahkan verifikasi akun anda dengan tombol di bawah ini.</p>
						    <a href="'.base_url().'portal/page/verifemail/'.md5($this->input->ip_address().strtotime('now')).'" class="btn btn-primary">Verifikasi Akun!</a>
						  </div>
						  <div class="card-footer text-muted">
						  </div>
						</div>';

				$this->portal_mod->kirimmail($this->input->post('email'),'Verifikasi akun Dinas Ketahanan Pangan',$pesan);
				echo json_encode(array('success' => true, 'teks' => 'Berhasil mendaftarkan akun. Silahkan periksa email dan lakukan verifikasi akun.'));
			}else{
				echo json_encode(array('success' => false, 'teks' => 'Gagal mendaftarkan akun. Silahkan coba lagi.'));
			}
		}


	}

	function loginAkun(){
		$email = $this->input->post("email",true);
		$pass = md5($this->input->post("password",true));
		$rs = $this->db->query("select * from user where email='".$email."' and password = '".$pass."' ");

		if($rs->num_rows() > 0){
	    	$user = $rs->row();


	      if($email == $user->email && $pass == $user->password){ // cek password
			if($user->verified == '1'){
				$module = array();
				$module['sertifikasi']['module'] = '21';
				$module['sertifikasi']['module'] = 'sertifikasi';
				$module['sertifikasi']['menumodule'] = 'sertifikasi';
				$module['sertifikasi']['urlmodule'] = 'sertifikasi';
				$module['sertifikasi']['d'] = '1';
				$module['sertifikasi']['e'] = '1';
				$module['sertifikasi']['v'] = '1';
				$module['sertifikasi']['i'] = '1';

				$sesi = array(
				'username'=>$user->email
				,'iduser'=>$user->iduser
				,'bidang'=> ''
				,'idbidang'=> ''
				,'level'=> 'masyarakat'
				,'role'=> $module
				,'def_mod'=>'sertifikasi'
				,'nama'=>$user->nama
				,'tmstamp' =>date("Y-m-d H:i:s")
				,'ip' => $this->input->ip_address()
				,'is_login_portal_disnakerbogor'=>true
				,'ses_kcfinder'=>array('disabled'=>false,'uploadURL'=>base_url().'content/')
				);

				$this->session->set_userdata($sesi);


				$data['username']	= $this->session->userdata('username');
				$data['ket']		= 'login sebagai User';
				$this->db->insert("log_user",$data);

	      		echo json_encode(array('success' => true, 'teks' => ''));
			}else{
				echo json_encode(array('success' => false, 'teks' => 'Akun anda belum terverifikasi, cek email untuk lakukan verifikasi.'));
			}
		  }else{
		  	echo json_encode(array('success' => false, 'teks' => 'Email atau password yang anda masukkan salah. Silahkan coba lagi.'));
		  }

	    }else{
			echo json_encode(array('success' => false, 'teks' => 'Email atau password yang anda masukkan salah. Silahkan coba lagi.'));
		}
	}

	function verifemail(){
		$this->db->query("update ngi_perusahaan set verified=1 where link_enkripsi='".$this->uri->segment('4')."'");
		echo "<script type='text/javascript'>alert('Akun berhasil diverifikasi. Anda bisa login menggunakan akun ini.');top.location='".base_url()."portal/login'</script>";

	}
  function buatroom(){
   $this->db->query("update ngi_joblist set linkinterview='".$_POST['linkinterview']."' where id='".$_POST['idjob']."'");
   echo "<script type='text/javascript'>alert('Berhasil membuat room interview');top.location='".$_POST['urlnya']."'</script>";

    // if($this->db->query('update ngi_joblist set linkinterview='".$_POST['linkinterview']."' where id='".$_POST['idjob']."'')){
    //    echo "<script type='text/javascript'>alert('Berhasil membuat room interview');top.location='".$_POST['urlnya']."'</script>";
    // }else{
    //   echo "<script type='text/javascript'>alert('Gagal membuat room interview');top.location='".$_POST['urlnya']."'</script>";
    // }
   // exit();

  }
}
