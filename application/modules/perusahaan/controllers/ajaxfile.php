<?php

class Ajaxfile extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}


	function caripasar(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			nama like '%".strtolower($keyword)."%'

			)

		";

		$rs = $this->db->query("select * from ref_pasar where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_pasar where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}


	function cariuser(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			username like '%".strtolower($keyword)."%' or
			id like '%".$keyword."%' or
			nama_pegawai like '%".strtolower($keyword)."%'
			)
			and username!='ngi'
		";

		$rs = $this->db->query("select * from pegawai where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from pegawai where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function caripetugas(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			nama like '%".strtolower($keyword)."%' or
			username like '%".$keyword."%'
			)

		";

		$rs = $this->db->query("SELECT * FROM ref_pegawai where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_pegawai where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	/*
	function caripetugas(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where (
			nama like '%".strtolower($keyword)."%' or
			username like '%".strtolower($keyword)."%'
		)
		";
		$q = "SELECT * FROM ref_pegawai $where";
		$rs = $this->db->query($q);

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}*/
	function cariagama(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			agama like '%".$keyword."%' or
			idagama like '%".$keyword."%'
			)
		";

		$rs = $this->db->query("select * from ref_agama where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_agama where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function carijab(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			jab like '%".$keyword."%' or
			idjab like '%".$keyword."%'
			)
		";

		$rs = $this->db->query("select * from ref_jab2 where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_jab2 where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function caritkpend(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			tkpend like '%".$keyword."%' or
			idtkpend like '%".$keyword."%'
			)
		";

		$rs = $this->db->query("select * from ref_tkpend where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_tkpend where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function carijur(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			jur like '%".$keyword."%' or
			idtkpend like '%".$keyword."%'
			)
			and length(jur)>0 and (idtkpend='".$this->input->post('idtkpend')."' and jur not like '%DELETE%')
		";

		$rs = $this->db->query("select * from ref_jur where $where ");
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_jur where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function cariangkot(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			plat like '%".$keyword."%' 
			)
		";

		$rs = $this->db->query("select * from ref_angkot where flag_delete = 0 and $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_angkot where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function caripegawaibank(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			nama like '%".strtolower($keyword)."%'
			)

		";

		$rs = $this->db->query("select * from ref_pegawai where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_pegawai where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
}
?>
