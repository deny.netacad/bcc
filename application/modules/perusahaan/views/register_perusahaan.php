
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
<script src="<?=base_url()?>template/HTML/job_dark/js/custom_II.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
<style type="text/css">
    .select2-container .select2-choice {
    height: 34px;
    border-radius: 0px;
    margin-top: -5px;
    margin-left: -12px;
    height: 32px;
    border: none;
    box-shadow: none;
    margin-right: -13px;
    background: none;
    background-image: none;
}
.select2-container .select2-choice > .select2-chosen {
    margin-right: 26px;
    display: block;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
</style>
<script>
	function PropinsiFormatResult(data) {
			var markup= "<li>"+ data.province+"</li>";
			return markup;
		}

		function PropinsiFormatSelection(data) {
			return data.province;
		}
	function KabFormatResult(data) {
			var markup= "<li>"+ data.city_name+" ("+ data.types+")</li>";
			return markup;
		}
	function KabFormatSelection(data) {
		 var markup= ""+ data.city_name+" ("+ data.types+")";
			
			return markup;
		}
	function KecFormatResult(data) {
			var markup= "<li>"+ data.subdistrict_name+"</li>";
			return markup;
		}

	function KecFormatSelection(data) {
		 var markup= ""+ data.subdistrict_name+"";
			
			return markup;
		}


   
	$(document).ready(function(){

	$("#provinsiid").select2({
				id: function(e) { return e.id }, 
				placeholder: "PROVINSI",
				minimumInputLength: 0,
				multiple: false,
				ajax: { 
					url: "<?=base_url()?>portal/page/cariPropinsi",
					dataType: 'jsonp',
					type:'POST',
					quietMillis: 100,
					data: function (term, page) {
						return {
							keyword: term, //search term
							per_page: 10, // page size
							page: page, // page number
							'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
						};
					},
					results: function (data, page) { 
						var more = (page * 10) < data.total;
						return {results: data.rows, more: more};
					}
				},
				formatResult: PropinsiFormatResult, 
				formatSelection: PropinsiFormatSelection
			});

	$("#kabkotaid").select2({
			id: function(e) { return e.city_id }, 
			placeholder: "KABUPATEN / KOTA",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?>portal/page/cariKab",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
						provinsiid:$('#provinsiid').val(),
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			
					
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: KabFormatResult, 
			formatSelection: KabFormatSelection
		});

	$("#kec_id").select2({
			id: function(e) { return e.subdistrict_id }, 
			placeholder: "KECAMATAN",
			minimumInputLength: 0,
			multiple: false,
			ajax: { 
				url: "<?=base_url()?>portal/page/cariKec",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 5, // page size
						page: page, // page number
						provinsiid:$('#provinsiid').val(),
						city_id:$('#kabkotaid').val(),
						
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			
					
					};
				},
				results: function (data, page) { 
					var more = (page * 5) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: KecFormatResult, 
			formatSelection: KecFormatSelection
		});




	//alert('1');

	$('#provinsiid').select2('data',{'id':"<?=$this->session->userdata('idpropinsi')?>",'province':"<?=$this->session->userdata('province')?>"});
	$('#kabkotaid').select2('data',{'city_id':"<?=$this->session->userdata('idkab')?>",'city_name':"<?=$this->session->userdata('kab')?>",'types':"<?=$this->session->userdata('type_kab')?>"});
	$('#kec_id').select2('data',{'subdistrict_id':"<?=$this->session->userdata('idkec')?>",'subdistrict_name':"<?=$this->session->userdata('kec')?>"});



	});
</script>
 <div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Registrasi Perusahaan</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="<?=base_url()?>">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Registrasi Perusahaan</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp register wrapper start -->
    <div class="register_section">
        <!-- register_form_wrapper -->
        <div class="register_tab_wrapper">
            <div class="container">
                <div class="row">
				
                    <div class="col-md-10 col-md-offset-1">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_pricing_cont_heading">
                        <h2>Additional Information :</h2>
                    </div>
                    <div class="jp_pricing_cont_wrapper">
                        <p>1. Akun ini akan digunakan untuk keperluan anda memberikan informasi lowongan pekerjaan, penempatan kerja .<br>
						2. Setelah pendaftaran Akun selesai anda harus datang ke disnaker untuk verifikasi data dengan membawa kelengkapan sbb :
						<br> &nbsp; - Cap Perusahaan
						<br> &nbsp; - Surat Permohonan Pengajuan Kebutuhan Tenaga Kerja Dari Perusahaan Yang Ditujukan Kepada Disnaker Kabupaten Bogor
						<br> &nbsp; - KTP Penanggung Jawab Perusahaan
						<br> &nbsp; - Fotocopy SIUP
						<br>3. Setelah verifikasi data selesai maka administrator disnaker akan mengaktifkan akun anda.</p>
						<br>
                    </div>
				</div>
                        <div role="tabpanel">

                            <!-- Nav tabs -->
                            <ul id="tabOne" class="nav register-tabs">
                                <li class="active"><a href="#contentOne-1" data-toggle="tab">Perusahaan<br> <span>Form Pendaftaran Akun Perusahaan</span></a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active register_left_form" id="contentOne-1">
									
                                    <div class="jp_regiter_top_heading">
                                        <p><b>AKUN LOGIN PERUSAHAAN</b></p>
                                    </div>
									<form method="post" data-remote="true" action="<?=base_url()?>portal/page/regPerusahaan" accept-charset="UTF-8" id="RegPerusahaan">
                                    <div class="row">
                                        <!--Form Group-->
                                       <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="username" id="username" placeholder="Username*">
                                        </div> 
										<div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="text" name="email" id="email" placeholder="Email*">
                                        </div> 
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="password" name="pass" id="pass" placeholder=" password*">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="password" name="pass_ori" id="pass_ori" placeholder="Konfirmasi password*">
                                        </div>
                                       
                                    </div>
									<div class="jp_regiter_top_heading">
                                        <p><b>DATA PERUSAHAAN</b></p>
                                    </div>
                                    <div class="row">
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="nama_perusahaan" id="nama_perusahaan" placeholder="Nama Perushaan*">
                                        </div>
										<div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="telp" id="telp" placeholder="No. Telp*">
                                        </div>
                                        <!--Form Group-->
                                       
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
											<select name="lokasi" id="lokasi">
												<option value="-">Pusat/Cabang*</option>
												<option value="Pusat">Pusat</option>
												<option value="Cabang">Cabang</option>
											</select>
										
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
										<select>
											<option value="">Jenis Usaha*</option>
                                    <?php
                                        $rs = $this->db->query("select * from mast_jenisusaha")->result();

                                        foreach ($rs as $item) {
                                            if(isset($_POST)){
                                                if($_POST['jenisusaha'] == $item->id){
                                                    echo '<option value="'.$item->id.'" selected="true">'.$item->nama.'</option>';
                                                }else{
                                                    echo '<option value="'.$item->id.'">'.$item->nama.'</option>';    
                                                }
                                            }else{
                                                echo '<option value="'.$item->id.'">'.$item->nama.'</option>';
                                            }
                                        }
                                    ?>
										</select>
                                        </div>
                                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                        <select name="gender" id="gender">
                                            <option value="-">Pusat/Cabang*</option>
                                            <option value="1">Pusat</option>
                                            <option value="2">Cabang</option>
                                        </select>
                                    
                                    </div>
                                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" name="provinsiid" class="form-control" id="provinsiid" placeholder="Provinsi*"/>
									</div>
									<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" name="kabkotaid" class="form-control" id="kabkotaid" placeholder="Provinsi*"/>
									</div>
									<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" name="kec_id" class="form-control" id="kec_id" placeholder="Provinsi*"/>
									</div>
                                
						</div><!--end of row-->
						<div class="login_btn_wrapper register_btn_wrapper login_wrapper ">
                                        <a href="#" class="btn btn-primary login_btn"> Daftar </a>
                                    </div>
									
									</form>
                                    <div class="login_message">
                                        <p>Sudah mempunyai akun? <a href="#"> Login Disini </a> </p>
                                    </div>
                                </div>
								
                               

                            </div>
                            <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that you logout to prevent any un-authorized access to your account</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
