 <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />

<div class="jp_popular_category_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_popu_cate_heading_wrapper">
                        <h4>Dashboard</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="jp_popu_cate_box_main_wrapper jp_popu_cate_box_main_wrapper_second">
                        <div class="jp_top_jobs_category_wrapper">
                            <div class="jp_top_jobs_category">
                                <i class="fa fa-group"></i>
                                <h3><a href="#">Pelamar</a></h3>
                                <?php $pelamar = $this->db->query('select a.id from ngi_jobapplied a
                                                                    left join ngi_joblist b on a.id_job = b.id
                                                                    where b.idperusahaan = "'.$this->session->userdata('id').'" and a.status is null and b.is_aktif = 1 and now() < date_add(b.date_end, interval 1 day)')->num_rows(); ?>
                                <p><?=$pelamar?> pelamar belum ditanggapi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="jp_popu_cate_box_main_wrapper jp_popu_cate_box_main_wrapper_second">
                        <div class="jp_top_jobs_category_wrapper">
                            <div class="jp_top_jobs_category">
                                <i class="fa fa-clock-o"></i>
                                <h3><a href="<?=base_url()?>perusahaan/lowongan">Lowongan Aktif</a></h3>
                                <?php $job_aktif = $this->db->query('select id from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day) and idperusahaan = "'.$this->session->userdata('id').'" ')->num_rows(); ?>
                                <p><?=$job_aktif?> lowongan aktif</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="jp_popu_cate_box_main_wrapper jp_popu_cate_box_main_wrapper_second">
                        <div class="jp_top_jobs_category_wrapper">
                            <div class="jp_top_jobs_category">
                                <i class="fa fa-check-square-o"></i>
                                <h3><a href="<?=base_url()?>perusahaan/lowonganselesai">Lowongan Selesai</a></h3>
                                <?php $job_selesai = $this->db->query('select id from ngi_joblist where is_aktif = 0 or not (now() < date_add(date_end, interval 1 day)) and idperusahaan = "'.$this->session->userdata('id').'" ')->num_rows(); ?>
                                <p><?=$job_selesai?> lowongan selesai</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="jp_popu_cate_box_main_wrapper jp_popu_cate_box_main_wrapper_second">
                        <div class="jp_top_jobs_category_wrapper">
                            <div class="jp_top_jobs_category">
                                <i class="fa fa-info"></i>
                                <h3><a href="#">Informasi</a></h3>
                                <?php $job_will_close = $this->db->query('select id from ngi_joblist where date_end = now() and idperusahaan = "'.$this->session->userdata('id').'" ')->num_rows(); ?>
                                <p><?=$job_will_close?> lowongan akan tutup hari ini</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>