<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />
<style type="text/css">
	.jp_adp_choose_resume_bottom_btn_post li button {
	    float: left;
	    width: 170px;
	    height: 50px;
	    line-height: 50px;
	    color: #ffffff;
	    border: 1px solid #f36969;
	    background: #f36969;
	    text-align: center;
	    text-transform: uppercase;
	    -webkit-border-radius: 10px;
	    -moz-border-radius: 10px;
	    border-radius: 10px;
	    -webkit-transition: all 0.5s;
	    -o-transition: all 0.5s;
	    -ms-transition: all 0.5s;
	    -moz-transition: all 0.5s;
	    transition: all 0.5s;
	}

	#autocomplete {
        background-color: #181d28;
        border-radius: 3px;
        border: 1px solid #212b2d;
        max-height: 250px;
        overflow-y: auto;
    }


    #autocomplete ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    #autocomplete ul li {
        padding: 10px 20px;
        border-bottom: 1px solid #212b2d;
    }

    #autocomplete ul li:focus,
    #autocomplete ul li:hover {
        background-color: #F0F0F0;
        cursor: pointer;
    }


    .tag {
      font-size: 14px;
      padding: .3em .4em .4em;
      margin: 0 .1em;
    }

    .tag a {
      color: #bbb;
      cursor: pointer;
      opacity: 0.6;
    }

    .tag a:hover {
      opacity: 1.0
    }

    .tag .remove {
      vertical-align: bottom;
      top: 0;
    }

    .tag a {
      margin: 0 0 0 .3em;
    }

    .tag a .glyphicon-white {
      color: #fff;
      margin-bottom: 2px;
    }
	
	#tag-area{
		overflow-x: auto;
		padding-bottom: 5px;
	}

    .jp_adp_textarea_main_wrapper.no-topborder{
        border-top: none;
        padding-top: 0px;
    }
</style>
<script type="text/javascript">
	$(function(){

		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			//format:'dd-mm-yyyy',
			minViewMode:0,
			viewMode:1
		})


		$(document).on('change', '#province_id', function(){
			setKabupaten($(this).val());
		}).on('change', '#city_id', function(){
			setKecamatan($(this).val());
		}).on('submit', '#form', function(e){
			e.preventDefault();

			$('#deskripsi').val($('#deskripsi').val().replace(/\r\n|\r|\n/g,"<br />"));
			$('#responsibility').val($('#responsibility').val().replace(/\r\n|\r|\n/g,"<br />"));
			$('#qualification').val($('#qualification').val().replace(/\r\n|\r|\n/g,"<br />"));
			$('#next_step').val($('#next_step').val().replace(/\r\n|\r|\n/g,"<br />"));

			var posisi = $('#posisi').val();

			$.ajax({
				url : $(this).attr('action'),
				data : $(this).serialize(),
				type: 'post',
				dataType: 'json',
				success: function(result){
					if(result.login){
						if(result.success){
							if($('#id_edit').val() == ""){
								$.ajax({
								    url   : "<?=base_url()?>perusahaan/page/bcemail",
								    data : {
								    	'posisi' : posisi
								    },
								    async : true //change this to false if you hate your users and want them to wait 
								}).done(function() {
								    
								});

								alert('Berhasil menyimpan data');
								window.location = '<?=base_url()?>perusahaan/buatlowongan';
							}else{
								alert('Berhasil menyimpan data');
								window.location = '<?=base_url()?>perusahaan/lowongan';
							}
						}else{
							alert('Gagal menyimpan data. error = '+result.error+' \nPeriksa koneksi internet anda');
						}
					}else{
						alert('Session anda habis. Silahkan login kembali');
						window.location = '<?=base_url()?>portal/login';
					}
				},
				error: function(e, r, o){
					console.log(e,r,o);
				}
			})
		})
	})

	function setKabupaten(idprov, idkab){
		$.ajax({
			url: '<?=base_url()?>perusahaan/page/carikab',
			type: 'post',
			data: {
				<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
				id : idprov,
				ids : idkab
			},
			success: function(result){
				$('#city_id').html(result);
			}
		})
	}

	function setKecamatan(idkab, idkec){
		$.ajax({
			url: '<?=base_url()?>perusahaan/page/carikec',
			type: 'post',
			data: {
				<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
				id: idkab,
				ids: idkec
			},
			success: function(result){
				$('#idkecamatan').html(result);
			}
		})
	}

	function setWilayah(idprov,idkab,idkec){
		setKabupaten(idprov,idkab);
		setKecamatan(idkab,idkec);
	}

</script>
	<?php if($_POST['edit'] == '') {  ?>

	<div class="jp_adp_main_section_wrapper" style="padding-top: 20px !important">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar">
					<div class="jp_job_des">
                        <h2>Buat lowongan</h2>
                	</div>
				</div>
				<form action="<?=base_url()?>perusahaan/page/savelowongan" method="post" id="form">
					<div style="padding-top: 100px">
						<input type="hidden" placeholder="Judul Lowongan" name="id_edit" id="id_edit">
						<input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="jp_adp_form_wrapper">
								<input type="text" placeholder="Judul Lowongan" name="title" id="title" required="">
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="idcategory" id="idcategory" required="">
									<option value="">-Pilih Kategori Pekerjaan-</option>
									<?php

										$result = $this->db->query('select id, category from ngi_jobcategory')->result();

										foreach ($result as $data) {
											echo '<option value="'.$data->id.'">'.$data->category.'</option>';
										}


									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="posisi" id="posisi" required="">
									<option value="">-Pilih Posisi-</option>
									<?php

										$result = $this->db->query('select id, nama_job from ngi_job')->result();

										foreach ($result as $data) {
											echo '<option value="'.$data->id.'">'.$data->nama_job.'</option>';
										}


									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="tipe" id="tipe" required="">
									<option value="">-Pilih Tipe Pekerjaan-</option>
									<option value="full time">Full time</option>
									<option value="part time">Part time</option>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="min_pendidikan" id="min_pendidikan" required="">
									<option value="">-Minimal Pendidikan-</option>
									<?php
											$pend = array('SD', 'SMP', 'SMA', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3', 'Akta IV', 'Akuntan', 'Apoteker', 'Kedokteran', 'Keperawatan', 'Notaris', 'Ners');

	                                     	foreach ($pend as $select4) {
	                                        	echo '<option value="'.$select4.'">'.$select4.'</option>';
	                                    	}
									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="upah_kerja" id="upah_kerja" required="">
									<option value="">-Upah Kerja-</option>
									<?php
											$pend = $this->db->query('select name, id from ngi_salary')->result();

	                                     	foreach ($pend as $select4) {
	                                        	echo '<option value="'.$select4->id.'">'.$select4->name.'</option>';
	                                    	}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bottom_line_Wrapper">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="jp_adp_form_wrapper">
										<input type="text" class="dt" placeholder="Tanggal mulai pengajuan lamaran" name="date_start" id="date_start" required="">
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="jp_adp_form_wrapper">
										<input type="text" class="dt" placeholder="Tanggal akhir pengajuan lamaran" name="date_end" id="date_end" required="">
									</div>
								</div>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="province_id" id="province_id" required="">
									<option value="">-Pilih Provinsi-</option>
									<?php

										$result = $this->db->query('select province_id, province from ngi_kecamatan group by province_id')->result();

										foreach ($result as $data) {
											echo '<option value="'.$data->province_id.'">'.$data->province.'</option>';
										}


									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="city_id" id="city_id" required="">
									<option value="">-Pilih Kabupaten/Kota-</option>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="idkecamatan" id="idkecamatan" required="">
									<option value="">-Pilih Kecamatan-</option>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<input type="text" placeholder="Jumlah dibutuhkan" name="jml_pekerja" id="jml_pekerja" pattern="^\d*$" title="Hanya angka yang diperbolehkan" required="">
							</div>
							<div class="jp_adp_form_wrapper">
								<input type="text" placeholder="Keyword (pisahkan dengan koma)" name="keyword" id="keyword" required="">
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<div id="tag-area" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 25px; border-top: 2px solid rgba(255, 255, 255, 0.14);"></div>
							</div>
                            <div class="jp_adp_form_wrapper">
                            	<input type="hidden" id="all-skill" name="skill">
                                <input type="text" id="skill" placeholder="Skill yang harus dikuasai, contoh programming, ms. office, dll (diisi satu demi satu, langsung nama skillnya)" style="border-radius: 0px">
                                <div id="autocomplete"></div>
                            </div>
                        </div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px">
							<div class="jp_adp_textarea_main_wrapper">
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Deskripsi pekerjaan" name="deskripsi" id="deskripsi" required=""></textarea>
								</div>
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Penjelasan tanggung jawab" name="responsibility" id="responsibility" required=""></textarea>
								</div>
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Penjelasan kualifikasi" name="qualification" id="qualification" required=""></textarea>
								</div>
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Penjelasan tambahan, lain-lain" name="next_step" id="next_step" required=""></textarea>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;">
							<div class="jp_adp_textarea_main_wrapper" style="padding-top: 20px;  padding-left: 10px">
								<div style="padding-bottom: 10px">
									<h3 style="color: #6f7e98">Lampiran</h3>
								</div>
								<?php
									$q = $this->db->query('select * from ngi_attachment')->result();

									foreach ($q as $item) {
								?>
									<div style="padding-bottom: 10px">
										<input type="checkbox" name="attachment[]" value="<?=$item->id?>"> &nbsp; <?=$item->name?>
									</div>

								<?php } ?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="jp_adp_choose_resume_bottom_btn_post">
								<ul>
									<li><button type="submit" id="submit"><i class="fa fa-plus-circle"></i>&nbsp; Simpan</button></li>
								</ul>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php } else {

		$item= $this->db->query('select a.*, b.city_id, b.province_id from (select * from ngi_joblist where id = "'.$_POST['edit'].'" ) a
									left join ngi_kecamatan b on a.idkecamatan = b.subdistrict_id')->row();

		echo '<script>setWilayah('.$item->province_id.','.$item->city_id.','.$item->idkecamatan.')</script>';

	?>

	<div class="jp_adp_main_section_wrapper" style="padding-top: 20px !important">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar">
					<div class="jp_job_des">
                        <h2>Edit lowongan</h2>
                	</div>
				</div>
				<form action="<?=base_url()?>perusahaan/page/savelowongan" method="post" id="form">
					<div style="padding-top: 100px">
						<input type="hidden" placeholder="Judul Lowongan" name="id_edit" id="id_edit" value="<?=$item->id?>" required="">
						<input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="jp_adp_form_wrapper">
								<input type="text" placeholder="Judul Lowongan" name="title" id="title" value="<?=$item->title?>" required="">
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="idcategory" id="idcategory" required="">
									<option value="">-Pilih Kategori Pekerjaan-</option>
									<?php

										$result = $this->db->query('select id, category from ngi_jobcategory')->result();

										foreach ($result as $data) {
											if($item->idcategory == $data->id){
												echo '<option value="'.$data->id.'" selected>'.$data->category.'</option>';
											}else{
												echo '<option value="'.$data->id.'">'.$data->category.'</option>';
											}
										}


									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="posisi" id="posisi" required="">
									<option value="">-Pilih Posisi-</option>
									<?php

										$result = $this->db->query('select id, nama_job from ngi_job')->result();

										foreach ($result as $data) {
											if($item->posisi == $data->id){
												echo '<option value="'.$data->id.'" selected>'.$data->nama_job.'</option>';
											}else{
												echo '<option value="'.$data->id.'">'.$data->nama_job.'</option>';
											}
										}


									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="tipe" id="tipe" required="">
									<option value="">-Pilih Tipe Pekerjaan-</option>
									<option value="full time" <?=(($item->tipe == 'full time')? 'selected ': '')?> >Full time</option>
									<option value="part time" <?=(($item->tipe == 'part time')? 'selected ': '')?> >Part time</option>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="min_pendidikan" id="min_pendidikan" required="">
									<option value="">-Minimal Pendidikan-</option>
									<?php
										$pend = array('SD', 'SMP', 'SMA', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3', 'Akta IV', 'Akuntan', 'Apoteker', 'Kedokteran', 'Keperawatan', 'Notaris', 'Ners');

	                                     foreach ($pend as $select4) {
	                                        if($select4 == $item->min_pendidikan){
	                                            echo '<option value="'.$select4.'" selected>'.$select4.'</option>';
	                                        }else{
	                                            echo '<option value="'.$select4.'">'.$select4.'</option>';
	                                        }
	                                    }
									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="upah_kerja" id="upah_kerja" required="">
									<option value="">-Upah Kerja-</option>
									<?php
											$pend = $this->db->query('select name, id from ngi_salary')->result();

	                                     	foreach ($pend as $select4) {
												if($select4->id == $item->upah_kerja){
													echo '<option value="'.$select4->id.'" selected>'.$select4->name.'</option>';
												}else{
													echo '<option value="'.$select4->id.'">'.$select4->name.'</option>';
												}
	                                    	}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bottom_line_Wrapper">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="jp_adp_form_wrapper">
										<input type="text" class="dt" placeholder="Tanggal mulai" name="date_start" id="date_start" value="<?=$item->date_start?>" required="">
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="jp_adp_form_wrapper">
										<input type="text" class="dt" placeholder="Tanggal berakhir" name="date_end" id="date_end" value="<?=$item->date_end?>" required="">
									</div>
								</div>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="province_id" id="province_id" required="">
									<option value="">-Pilih Provinsi-</option>
									<?php

										$result = $this->db->query('select province_id, province from ngi_kecamatan group by province_id')->result();

										foreach ($result as $data) {
											if($item->province_id == $data->province_id){
												echo '<option value="'.$data->province_id.'" selected>'.$data->province.'</option>';
											}else{
												echo '<option value="'.$data->province_id.'">'.$data->province.'</option>';
											}

										}


									?>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="city_id" id="city_id" required="">
									<option value="">-Pilih Kabupaten/Kota-</option>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<select name="idkecamatan" id="idkecamatan" required="">
									<option value="">-Pilih Kecamatan-</option>
								</select>
							</div>
							<div class="jp_adp_form_wrapper">
								<input type="text" placeholder="Jumlah dibutuhkan" name="jml_pekerja" id="jml_pekerja" value="<?=$item->jml_pekerja?>" pattern="^\d*$" title="Hanya angka yang diperbolehkan" required="">
							</div>
							<div class="jp_adp_form_wrapper">
								<input type="text" placeholder="Keyword (pisahkan dengan koma)" name="keyword" id="keyword" value="<?=$item->keyword?>" required="">
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<div id="tag-area" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 25px; border-top: 2px solid rgba(255, 255, 255, 0.14);">
									 <?php
										if($item->skill != ""){
											$skill = explode(',', $item->skill);
											foreach ($skill as $s) {
												echo '<span class="tag label label-default">
													  <span class="skill-name">'.$s.'</span>
													  <a class="close-tag"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a>
													</span>';
											}
										}
									?>
								</div>
							</div>
                            <div class="jp_adp_form_wrapper">
                            	<input type="hidden" id="all-skill" name="skill">
                                <input type="text" id="skill" placeholder="Skill yang harus dikuasai" style="border-radius: 0px">
                                <div id="autocomplete"></div>
                            </div>
                        </div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px">
							<div class="jp_adp_textarea_main_wrapper">
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Deskripsi pekerjaan" name="deskripsi" id="deskripsi" value="" required=""><?=str_replace("<br />","\n",$item->deskripsi)?></textarea>
								</div>
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Penjelasan tanggung jawab" name="responsibility" id="responsibility" value="" required=""><?=str_replace("<br />","\n",$item->responsibility)?></textarea>
								</div>
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Penjelasan kualifikasi" name="qualification" id="qualification" value="" required=""><?=str_replace("<br />","\n",$item->qualification)?></textarea>
								</div>
								<div style="padding-bottom: 25px">
									<textarea rows="7" placeholder="Penjelasan langkah selanjutnya setelah diterima pada sistem ini" name="next_step" id="next_step" value="" required=""><?=str_replace("<br />","\n",$item->next_step)?></textarea>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;">
							<div class="jp_adp_textarea_main_wrapper" style="padding-top: 20px;  padding-left: 10px">
								<div style="padding-bottom: 10px">
									<h3 style="color: white">Lampiran</h3>
								</div>
								<?php
									$q = $this->db->query('select * from ngi_attachment')->result();
									$attach = explode(",", $item->attachment);

									foreach ($q as $attachment) {
										if(in_array($attachment->id, $attach)){
								?>
									<div style="padding-bottom: 10px">
										<input type="checkbox" name="attachment[]" value="<?=$attachment->id?>"  checked> &nbsp; <?=$attachment->name?>
									</div>

								<?php }else { ?>
									<div style="padding-bottom: 10px">
										<input type="checkbox" name="attachment[]" value="<?=$attachment->id?>"> &nbsp; <?=$attachment->name?>
									</div>
								<?php } } ?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="jp_adp_choose_resume_bottom_btn_post">
								<ul>
									<li><button type="submit" id="submit"><i class="fa fa-plus-circle"></i>&nbsp; Simpan</button></li>
								</ul>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php } ?>

	<script type="text/javascript">
		var skill = new Array();

		$(function(){
			gatherSkill();
	        $('#all-skill').val(skill);

			$(document).on('input', '#skill', function(){
	            var value = $(this).val();

	            if(value == ""){
	                $('#autocomplete').html('');
	            }else{
	                 $.ajax({
	                    url : '<?=base_url()?>portal/page/searchskill',
	                    type: 'post',
	                    data : {
	                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
	                        'skill' : value
	                    },
	                    success: function(result){
	                        $('#autocomplete').html(result);
	                    }
	                });
	            }
	        }).on('click touchstart ', '.select-skill', function(){
	            var tagged = '<span class="tag label label-default"><span class="skill-name">'+$(this).attr('data')+'</span><a class="close-tag"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a></span>';
	            var string = $(this).find('a').html();
	            var boolean = string.includes('Tambahkan');

	            if(!skill.includes($(this).attr('data').toLowerCase())){
	                if(boolean){
	                    $.ajax({
	                        url : '<?=base_url()?>portal/page/savenewskill',
	                        type: 'post',
	                        data : {
	                            '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
	                            'skill' : $(this).attr('data')
	                        },
	                        success: function(result){
	                            if(result){
	                                $('#tag-area').append(tagged);
	                                $('#autocomplete').html('');
	                                $('#skill').val('');
	                            }else{
	                                alert('Terjadi kesalahan. Silahkan ulangi');
	                            }

	                        }
	                    });
	                }else{
	                    $('#tag-area').append(tagged);
	                    $('#autocomplete').html('');
	                    $('#skill').val('');
	                }

	                skill = [];
	                gatherSkill();
	                $('#all-skill').val(skill);
	            }else{
	                $('#autocomplete').html('');
	                $('#skill').val('');
	            }

	        }).on('click touchstart ', '.close-tag', function(){
	            $(this).closest('span.tag').remove();
	            skill = [];
	            gatherSkill();
	            $('#all-skill').val(skill);
	        });
		});

		function gatherSkill(){
		    $('.skill-name').each(function(){
		        skill.push($(this).text().toLowerCase());
		    });
		}

	</script>
