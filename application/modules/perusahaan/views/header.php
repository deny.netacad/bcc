           <div class="jp_banner_heading_cont_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_job_heading_wrapper">
                            <div class="jp_job_heading">
                                <h1><span>3,000+</span> Bursa Kerja Online</h1>
                                <p>DISNAKER KABUPATEN BOGOR</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form action="<?=base_url()?>portal/caripekerjaan" method="POST" id="jobsearch">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
                            <div class="jp_header_form_wrapper">
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <input type="text" name="keyword" placeholder="Keyword e.g. (Job Title, Description, Tags)" value="<?=$_POST['keyword']?>">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="jp_form_location_wrapper">
                                        <i class="fa fa-dot-circle-o first_icon"></i>
                                <select name="location">
                                    <option value="">Select Location</option>
                                    <?php
                                        $rs = $this->db->query("select * from ref_kecamatan")->result();

                                        foreach ($rs as $item) {
                                            if(isset($_POST)){
                                                if($_POST['location'] == $item->id){
                                                    echo '<option value="'.$item->id.'" selected="true">'.$item->nama_kec.'</option>';
                                                }else{
                                                    echo '<option value="'.$item->id.'">'.$item->nama_kec.'</option>';    
                                                }
                                            }else{
                                                echo '<option value="'.$item->id.'">'.$item->nama_kec.'</option>';
                                            }
                                        }
                                    ?>
                                </select><i class="fa fa-angle-down second_icon"></i>
                                    </div>
                                </div>
                               <!--  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="jp_form_exper_wrapper">
                                        <i class="fa fa-dot-circle-o first_icon"></i>
                                <select name="experience">
                                    <option value="">-</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select><i class="fa fa-angle-down second_icon"></i>
                                    </div>
                                </div> -->
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="jp_form_btn_wrapper">
                                        <ul>
                                            <li><a onclick="document.getElementById('jobsearch').submit()"><i class="fa fa-search"></i> Search</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_banner_main_jobs_wrapper">
                            <div class="jp_banner_main_jobs">
                                <ul>
                                    <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                    <?php 
                                        $q = $this->db->query('select keyword, count(id) as jml from ngi_keyword where date(log) between current_date - interval 7 day and current_date group by keyword order by jml desc limit 6');

                                        $data = $q->result();

                                        $i = 0;
                                        $len = count($data);
                                        foreach ($data as $item) {
                                            if ($i == $len - 1) {
                                                echo '<li><a href="#" class="trending" id="'.$item->keyword.'">'.$item->keyword.'</a></li>';          
                                            } else {
                                                echo '<li><a href="#" class="trending" id="'.$item->keyword.'">'.$item->keyword.',</a></li>';          
                                            }

                                            $i++;
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(!isset($page)){ ?>
            <div class="jp_banner_jobs_categories_wrapper">
                <div class="container">
                    <?php
                        $rs = $this->db->query('select count(a.id) as jml, a.category, a.icon, a.id from ngi_jobcategory a 
                                                left join ngi_joblist b on a.id = b.`idcategory`
                                                group by a.id
                                                order by rand()
                                                limit 5')->result();

                        foreach ($rs as $item) {
                    ?>

                    <div class="jp_top_jobs_category_wrapper jp_job_cate_left_border jp_job_cate_left_border_bottom">
                        <div class="jp_top_jobs_category">
                            <i class="fa <?=$item->icon?>"></i>
                            <h3><a href="#" id="c<?=$item->id?>" class="link_category"><?=$item->category?></a></h3>
                            <p>(<?=$item->jml?> pekerjaan)</p>
                        </div>
                    </div>

                    <?php } 

                        $jml = $this->db->query('select id from ngi_joblist')->num_rows();
                    ?>

                    <div class="jp_top_jobs_category_wrapper">
                        <div class="jp_top_jobs_category">
                            <i class="fa fa-th-large"></i>
                            <h3><a href="<?=base_url()?>portal/caripekerjaan">Semua pekerjaan</a></h3>
                            <p>(<?=$jml?> pekerjaan)</p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>