<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />

<style type="text/css">
    .jp_recent_resume_cont_wrapper{
        padding-top: 0px !important;
        width: 100% !important;
    }

    .fa-icon{
        font-size: 15px;
        color: #f36969;
    }

    .mobile-btn {
        float: right;
        padding-top: 10px;
    }

    .photo{
        width: 70px;
        height: 80px;
        object-fit: contain;
        display: block;
        margin-left: auto;
        margin-right: auto;
        padding-bottom: 10px;
    }

    .td-w65{
        width: auto !important;
    }

    .badge{
        background-color: transparent;
    }

    li a.disabled {
        pointer-events: none;
        cursor: not-allowed;
    }
</style>

<?php
    $seo = $this->uri->segment(3);

    $rs = $this->db->query('select a.*, date_format(a.log,"%d %M %Y") as tgl, date_format(a.date_start,"%d %M %Y") as tgl_awal, date_format(a.date_end,"%d %M %Y") as tgl_akhir, b.nama_job, e.category, c.subdistrict_name, c.city, c.province from (select * from ngi_joblist where seo_url = "'.$seo.'" ) a 
                            left join ngi_job b on a.posisi = b.id
                            left join ngi_kecamatan c on a.idkecamatan = c.subdistrict_id
                            left join ngi_jobcategory e on a.idcategory = e.id');

    if($rs->num_rows() > 0){
        $job = $rs->row();
?>

<div class="jp_listing_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar" style="padding-top: 20px">
                <div class="jp_job_des">
                    <h2>Detail Lowongan</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_cp_right_side_inner_wrapper">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="td-w25">Judul Lowongan</td>
                                        <td class="td-w10">:</td>
                                        <td class="td-w65"><?=$job->title?></td>
                                    </tr>
                                    <!-- <tr>
                                        <td class="td-w25">Kategori Pekerjaan</td>
                                        <td class="td-w10">:</td>
                                        <td class="td-w65"><?=$job->category?></td>
                                    </tr> -->
                                    <tr>
                                        <td class="td-w25">Posisi</td>
                                        <td class="td-w10">:</td>
                                        <td class="td-w65"><?=$job->nama_job?></td>
                                    </tr>
                                    <!-- <tr>
                                        <td class="td-w25">Upah Kerja</td>
                                        <td class="td-w10">:</td>
                                        <td class="td-w65">Rp. <?=number_format($job->upah_kerja)?></td>
                                    </tr>
                                    <tr>
                                        <td class="td-w25">Lokasi</td>
                                        <td class="td-w10">:</td>
                                        <td class="td-w65"><?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></td>
                                    </tr>
                                    <tr>
                                        <td class="td-w25">Tanggal Posting</td>
                                        <td class="td-w10">:</td>
                                        <td class="td-w65"><?=$job->tgl?></td>
                                    </tr>
                                    <tr>
                                        <td class="td-w25">Masa Lowongan</td>
                                        <td class="td-w10">:</td>
                                        <td class="td-w65"><?=$job->tgl_awal?> ~ <?=$job->tgl_akhir?></td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="jp_job_des" style="padding-top: 60px; padding-bottom: 40px">
               
                  <!-- <a class="btn btn-success room">Buat Room Interview</a> -->
                <h2>Pelamar &nbsp; 
                </h2>
                <br>
                <br>
                 <!-- <h3 style="color: white;font-size: 16px;">
                     <?php
                    if($job->linkinterview == ""){
                         echo "<a class='btn btn-success room' title='Buat Link Interview'>Buat Room Interview</a>";

                    }else{
                        echo "Link Interview <a class='btn btn-success' href='https://meet.jit.si/$job->linkinterview' target='_blank' title='visit link interview'>$job->linkinterview</a>";
                    }
                ?>
                </h3> -->
					
                </div>
<!--                 <div>
                    <h3> Tahap Administrasi</h3>
                </div> -->
                <div class="row" id="tempat">
                    <?php 

                        $q = $this->db->query('SELECT a.*, date_format(a.log,"%d %M %Y %H:%i:%s") as tgl, b.nama,b.no_telp,b.email, b.no_ktp, b.profile_url, b.photo, c.subdistrict_name, c.city, c.province from (select * from ngi_jobapplied where id_job = "'.$job->id.'" ) a 
                                                left join user_pelamar b on a.id_pelamar = b.iduser 
                                                left join ngi_kecamatan c on c.subdistrict_id = b.kecamatan_id
                                                where b.no_ktp like "3301%" or b.no_ktp like "3372%"
                                                order by a.log desc' );

                         $q2 = $this->db->query('SELECT a.*, date_format(a.log,"%d %M %Y %H:%i:%s") as tgl, b.nama, b.email,b.no_telp,b.no_ktp, b.profile_url, b.photo, c.subdistrict_name, c.city, c.province from (select * from ngi_jobapplied where id_job = "'.$job->id.'" ) a 
                                                left join user_pelamar b on a.id_pelamar = b.iduser 
                                                left join ngi_kecamatan c on c.subdistrict_id = b.kecamatan_id
                                                where not (b.no_ktp like "3301%" or b.no_ktp like "3372%") 
                                                order by a.log desc');

                        if($q->num_rows() > 0 || $q2->num_rows() > 0) {

                            $data = $q->result();
                            $data2 = $q2->result();

                            foreach ($data as $item) {
                    ?>


                    <!-- warga bogor -->
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_cont_wrapper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                        <img class="photo" src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_user.png')?>" alt="resume_img">
                                    </div>
                                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h3>
                                                <?=$item->nama?> 
                                                <?php if($item->status == "1") { ?>

                                                <span class="btn-success badge"><i class="fa fa-check"></i> &nbsp;Diterima</span>
                                                <?php } else if($item->status == "0"){ ?>

                                                <span class="btn-danger badge"><i class="fa fa-cross"></i> &nbsp;Ditolak</span>
                                                <?php }  ?>
                                            </h3>
                                        </div>
                                        <div style="padding-top: 25px">
                                             <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div><i class="fa fa-id-card fa-icon"></i> &nbsp;<?=$item->no_ktp?></div>
                                                <div><i class="fa fa-map-marker fa-icon"></i> &nbsp;<?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div><i class="fa fa-envelope fa-icon"></i> &nbsp;<?=$item->email?></div>
                                                <div><i class="fa fa-calendar fa-icon"></i> &nbsp;<?=$item->tgl?></div>
                                            </div>
                                         <!--   <a href="https://api.whatsapp.com/send?phone=<?=$item->no_telp?>&text=&source=&data=test"><i class="fa fa-phone"></i> &nbsp;Interview</a> -->
                                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mobile-btn">
                                                <a class="btn btn-info" href="<?=base_url()?>portal/profile/<?=$item->profile_url?>"><i class="fa fa-eye"></i> &nbsp;Profile</a>
                                                <?php if($item->status == ""){?>

                                                <span class="dropdown">
                                                  <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-gear"></i> Aksi
                                                  <span class="caret"></span></button>
                                                  <ul class="dropdown-menu">
                                                    <li><a class="terima" id="<?=$item->id_pelamar?>">Terima</a></li>
                                                    <li><a class="tolak" id="<?=$item->id_pelamar?>">Tolak</a></li>
                                                  </ul>
                                                </span>

                                                <?php }  ?>
                                               <span class="dropdown">
                                                  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-file"></i> Lampiran
                                                  <span class="caret"></span></button>
                                                  <ul class="dropdown-menu">
                                                    <?php
                                                        $array = explode(",", $job->attachment);
                                                        foreach ($array as $att) {
                                                            $rs = $this->db->query('select * from ngi_attachment where id = "'.$att.'"')->row();
                                                            $rs2 = $this->db->query('select * from ngi_jobattachment where idjob = "'.$job->id.'" and idpelamar = "'.$item->id_pelamar.'" and idfile = "'.$att.'"')->row();

                                                            if($rs2->nama_file != ""){
                                                                echo '<li><a target="_blank" href="'.base_url().'assets/img/lampiran/'.$rs2->nama_file.'">'.$rs->name.'</a></li>';   
                                                            }else{
                                                                echo '<li><a class="no-file" onclick="alert(\'Pelamar belum mengupload file '.$rs->name.'\')">'.$rs->name.'</a></li>'; 
                                                            }
                                                        }
                                                    ?>
                                                  </ul>
                                                </span>
                                                <!-- <a class="btn btn-info" style="font-size: 13px;"href="https://api.whatsapp.com/send?phone=<?=preg_replace("/^0/", "62",$item->no_telp)?>&text=https://meet.jit.si/<?=$job->linkinterview?>" target="_new" title="share link Interview via whatsapp"><i class="fa fa-whatsapp"></i> &nbsp;Interview</a>  -->
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- non warga bogor -->
                    <?php } foreach ($data2 as $item) { ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_cont_wrapper">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <img class="photo" src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_user.png')?>" alt="resume_img">
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h3>    
                                                        <?=$item->nama?> 
                                                        <?php if($item->status == "1") { ?>

                                                        <span class="btn-success badge"><i class="fa fa-check"></i> &nbsp;Diterima</span>
                                                        <?php } else if($item->status == "0"){ ?>

                                                        <span class="btn-danger badge"><i class="fa fa-cross"></i> &nbsp;Ditolak</span>
                                                        <?php }  ?>
                                                    </h3>
                                                </div>
                                                <div style="padding-top: 25px">
                                                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <div><i class="fa fa-id-card fa-icon"></i> &nbsp;<?=$item->no_ktp?></div>
                                                        <div><i class="fa fa-map-marker fa-icon"></i> &nbsp;<?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <div><i class="fa fa-envelope fa-icon"></i> &nbsp;<?=$item->email?></div>
                                                        <div><i class="fa fa-calendar fa-icon"></i> &nbsp;<?=$item->tgl?></div>
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mobile-btn">
                                                        <a class="btn btn-info" href="<?=base_url()?>portal/profile/<?=$item->profile_url?>"><i class="fa fa-eye"></i> &nbsp;Profil</a>
                                                         
                                                        <?php if($item->status == ""){?>

                                                        <span class="dropdown">
                                                          <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-gear"></i> Aksi
                                                          <span class="caret"></span></button>
                                                          <ul class="dropdown-menu">
                                                            <li><a class="terima" id="<?=$item->id_pelamar?>" data="<?=$item->id?>">Terima</a></li>
                                                            <li><a class="tolak" id="<?=$item->id_pelamar?>" data="<?=$item->id?>">Tolak</a></li>
                                                          </ul>
                                                        </span>

                                                        <?php }?>
                                                         <span class="dropdown">
                                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-file"></i> Lampiran
                                                          <span class="caret"></span></button>
                                                          <ul class="dropdown-menu">
                                                            <?php
                                                                $array = explode(",", $job->attachment);
                                                                foreach ($array as $att) {
                                                                    $rs = $this->db->query('select * from ngi_attachment where id = "'.$att.'"')->row();
                                                                    $rs2 = $this->db->query('select * from ngi_jobattachment where idjob = "'.$job->id.'" and idpelamar = "'.$item->id_pelamar.'" and idfile = "'.$att.'"')->row();

                                                                    if($rs2->nama_file != ""){
                                                                        echo '<li><a target="_blank" href="'.base_url().'assets/img/lampiran/'.$rs2->nama_file.'">'.$rs->name.'</a></li>';   
                                                                    }else{
                                                                        echo '<li><a class="no-file" onclick="alert(\'Pelamar belum mengupload file '.$rs->name.'\')">'.$rs->name.'</a></li>'; 
                                                                    }
                                                                }
                                                            ?>

                                                          </ul>
                                                        </span>
                                                        <!-- <a class="btn btn-info" style="font-size: 13px;"href="https://api.whatsapp.com/send?phone=<?=preg_replace("/^0/", "62",$item->no_telp)?>&text=https://meet.jit.si/<?=$job->linkinterview?>" target="_new" title="share link Interview via whatsapp"><i class="fa fa-whatsapp"></i> &nbsp;Interview</a>  -->
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                    <?php }?>

                    <?php }else{?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <h3 style="text-align: center; color: white;">Tidak ada pelamar</h3>
                        </div>
                    </div>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } else { ?>

<!-- jp Tittle Wrapper End -->
<!-- jp 404 error wrapper start -->
  <div class="error_page" style="padding-top: 200px !important; padding-bottom: 200px !important;">
    <div class="container">
        <div class="row">
         <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                <div class="error_page_cntnt">
                    <h2>
                        <span>Perusahaan</span>
                    </h2>
                    <h3>Halaman untuk perusahaan</h3>
                    <p>Halaman ini untuk perusahaan <a href="<?=base_url()?>">Home Page</a></p>
                   <!--  <div class="error_page_mail_wrapper">
                        <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a><input type="text" placeholder="Email Address *">
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alasan ditolak</h4>
      </div>
      <form method="post" action="<?=base_url()?>perusahaan/page/tolakpelamar">
          <div class="modal-body">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
           <!--  <input type="hidden" name="iduser" id="iduser"> -->
            <input type="hidden" name="idjob" value="<?=$job->id?>">
            <input type="hidden" name="iduser" id="iduser">
            <input type="hidden" name="id" id="id">
            <input type="hidden" name="url" id="url">
            <?php $data = $this->db->query('select * from reason')->result()?>
            <select id="reason" name="reason">
                <?php
                    foreach ($data as $item) {
                        echo "<option value=".$item->id.">".$item->reason."</option>";
                    }
                ?>
            </select>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Tolak</button>
          </div>
      </form>
    </div>

  </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Buat Room Interview</h4>
      </div>
      <form method="post" action="<?=base_url()?>perusahaan/page/buatroom">
          <div class="modal-body">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
           <!--  <input type="hidden" name="iduser" id="iduser"> -->
            <input type="hidden" name="idjob" value="<?=$job->id?>">
            <input type="hidden" name="urlnya" id="urlnya">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Link Interview</span>
              </div>
              <input type="text" style="width: 154%;" class="form-control" placeholder="nama interview" name="linkinterview" id="linkinterview">
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Buat Room Interview</button>
          </div>
      </form>
    </div>

  </div>
</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click','.terima', function(e){
            var iduser = $(this).prop('id');
            var id = $(this).attr('data');

            post('<?=base_url()?>perusahaan/page/terimapelamar', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', iduser : iduser, id : id, idjob : '<?=$job->id?>', url : window.location.href}, 'post');
            e.preventDefult();
        });

        $(document).on('click','.tolak', function(e){
            $('#myModal').modal('show');
            var iduser = $(this).prop('id');
            var id = $(this).attr('data');

            $('#iduser').val(iduser);
            $('#id').val(id);
            $('#url').val(window.location.href);

            // post('<?=base_url()?>perusahaan/page/tolakpelamar', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', iduser : id, idjob : '<?=$job->id?>', url : window.location.href}, 'post');
            // e.preventDefult();
        });

        $(document).on('click','.room', function(e){
            $('#myModal1').modal('show');
            var id = $(this).prop('id');

            //$('#iduser').val(id);
           $('#urlnya').val(window.location.href);

            // post('<?=base_url()?>perusahaan/page/buatroom', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', idjob : '<?=$job->id?>', url : window.location.href}, 'post');
            // e.preventDefult();
        });


        // $(document).on('click touch','.no-file',function(e){
        // 	e.preventDefult();
        // }
    });
    // $(document).on('click','.room', function(){
    //         if(confirm("Apa anda yakin ingin membuat room interview ?")){
    //             var id = $(this).attr('data');

    //             $.ajax({
    //                 url  : '<?=base_url()?>perusahaan/page/buatroom',
    //                 type : 'post',
    //                 data : {
    //                     '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
    //                     idjob : '<?=$job->id?>',
    //                     linkinterview . val('linkinterview');
    //                     //table: 'ngi_perusahaan',
                        
    //                 },
    //                 success: function(rs){
    //                     alert(rs);
    //                     refreshData();
    //                 }
    //             })
    //         }
    //     })

    function post(path, params, method) {
        method = method || "post"; // Set method to post by default if not specified.

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }
</script>