 <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />

<style type="text/css">
    .jp_recent_resume_cont_wrapper{
        padding-top: 0px !important;
        width: 100% !important;
    }

    .fa-icon{
        font-size: 15px;
        color: #f36969;
    }

    .mobile-btn {
        float: right;
        padding-top: 10px;
    }
</style>

<div class="jp_listing_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar" style="padding-top: 20px">
                <div class="jp_job_des">
                        <h2>Informasi</h2>
                </div>
                <div class="row" style="padding-top: 40px" id="tempat">
                    <?php 

                        $q = $this->db->query('select a.*, b.nama_job, c.nama_kec from (select id, upah_kerja, posisi, idkecamatan, title, seo_url, date_format(log, "%d %M %Y") as tgl from ngi_joblist where is_aktif = 0 
                                                and idperusahaan = "'.$this->session->userdata('id').'") a
                                                left join ngi_job b on a.posisi = b.id
                                                left join ref_kecamatan c on c.id = a.idkecamatan
                                                limit 5');

                        if($q->num_rows() > 0) {

                            $data = $q->result();

                            foreach ($data as $item) {

                                $jmlpelamar = $this->db->query('select id from ngi_jobapplied where id_job = '.$item->id)->num_rows();
                    ?>

                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_cont_wrapper">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3><?=$item->title?></h3></div>
                                <div style="padding-top: 25px">
                                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-user fa-icon"></i> &nbsp;<?=$item->nama_job?></div>
                                        <div><i class="fa fa-map-marker fa-icon"></i> &nbsp;<?=$item->nama_kec?></div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-money fa-icon"></i> &nbsp;Rp. <?=number_format($item->upah_kerja)?></div>
                                        <div><i class="fa fa-calendar fa-icon"></i> &nbsp;<?=$item->tgl?></div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                        <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Informasi
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                            <li><a href="<?=base_url()?>perusahaan/pelamar/<?=$item->seo_url?>">Daftar Pelamar <span class="badge btn-danger"><?=$jmlpelamar?></span></a></li>
                                          </ul>
                                        </span>
                                    </div>
                                    <div class="hidden-lg hidden-md col-sm-12 col-xs-12 mobile-btn">
                                         <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Informasi
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                            <li><a href="<?=base_url()?>perusahaan/pelamar/<?=$item->seo_url?>">Daftar Pelamar <span class="badge btn-danger"><?=$jmlpelamar?></span></a></li>
                                          </ul>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="more">
                        <div class="jp_adp_choose_resume_bottom_btn_post jp_adp_choose_resume_bottom_btn_post2">
                            <ul>
                                <li><a href="#" data="5" id="view">View More</a></li>
                            </ul>
                        </div>
                    </div>

                    <?php }else{?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <h3 style="text-align: center; color: white;">Tidak ada lowongan pekerjaan selesai</h3>
                        </div>
                    </div>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click','#view',function(){
            $.ajax({
                type: 'post',
                url: '<?=base_url()?>perusahaan/page/viewmoreselesai',
                data: {
                    '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                    'more' : $(this).attr('data')
                },
                success:function(result){
                    $('#tempat').html(result);
                }
            })
        });
    });
</script>