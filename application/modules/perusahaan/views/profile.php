
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />

	<?php
        $rs = $this->db->query('select a.*, b.nama, c.province, c.type, c.city, c.subdistrict_name from (select * from ngi_perusahaan a where a.idperusahaan = "'.$this->session->userdata('id').'") a
        						left join mast_jenisusaha b on a.jenisusaha = b.id
        						left join ngi_kecamatan c on a.kec_id = c.subdistrict_id');

        if($rs->num_rows() > 0){
            $data = $rs->row();
            // $this->db->insert('ngi_jobtrend',array('id_joblist' => $data->id));
    ?>

    <style type="text/css">
    	.nav-tabs {
    		float: right;
		    padding-bottom: 10px;
		    border: 0;
    	}

    	.jp_recent_resume_cont_wrapper{
    		padding-left: 50px !important;
    		padding-top: 0px;
    	}
		
		td {
			display: inline-block;
			overflow-x: auto !important;
		}
		
		.td-w10{
			width: 5% !important;
			vertical-align: top;
		}

    	.td-w25{
    		width: 40% !important;
			vertical-align: top;
    	}
		
		.td-w65{
			width: 50% !important;
		}

    	.jp_recent_resume_cont_wrapper p {
    		padding-top: 0px;
    		margin: 0px;
    	}

    	.jp_recent_resume_cont_wrapper h3 {
    		padding-bottom: 15px;
    	}
		
		.panel-body{
			overflow-x: auto;
		}
    </style>
    <div class="jp_listing_single_main_wrapper" style="padding-bottom:0px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Profile</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp profile Wrapper Start -->
	<div class="jp_cp_profile_main_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="jp_cp_left_side_wrapper">
						<div class="jp_cp_left_pro_wallpaper">
							<img src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_company.jpg')?>" alt="profile_img" style="width: 50%; object-fit: contain; border: 2px solid gray;"  />
							<h2><?=$data->nama_perusahaan?></h2>
							<p>Kantor <?=$data->lokasi?></p>
							<!-- <ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
							</ul> -->
						</div>
						<div class="jp_cp_rd_wrapper">
							<ul>
								<!-- <li><a href="#"><i class="fa fa-download"></i> &nbsp;Download Resume</a></li> -->
								<li><a href="tel:<?=$data->telp?>"><i class="fa fa-phone"></i> &nbsp;Telpon perusahaan</a></li>
							</ul>
						</div>
					</div>
                    <!-- <div class="jp_add_resume_wrapper jp_job_location_wrapper jp_cp_left_ad_res">
                        <div class="jp_add_resume_img_overlay"></div>
                        <div class="jp_add_resume_cont">
                            <img src="images/content/resume_logo.png" alt="logo" />
                            <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;ADD RESUME</a></li>
                            </ul>
                        </div>
                    </div> -->
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:25px">
	                <div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="best">
							<div class="jp_cp_right_side_wrapper">
								<div class="jp_cp_right_side_inner_wrapper">
									<h2>detail perusahaan</h2>
									<table style="width:100%">
		                                <tbody>
		                                    <tr>
		                                        <td class="td-w25">Nama Direktur</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->nm_dir?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Alamat</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->alamat?> <?=$data->subdistrict_name?>, <?=$data->type?> <?=$data->city?>, <?=$data->province?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Jenis Usaha</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->nama?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Phone</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->telp?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Email</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->email?></td>
		                                    </tr>
		                                </tbody>
		                            </table>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
									<div class="jp_cp_accor_heading_wrapper">
										<h2>Tentang perusahaan</h2>
										<p><?=$data->ttg_anda?></p>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
									<div class="accordion_wrapper abt_page_2_wrapper">
										<div class="panel-group" id="accordion_threeLeft">


											<!-- /.panel-default -->
											<div class="panel panel-default">
												<div class="panel-heading bell">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree" aria-expanded="true">
												Pengumuman
											  </a>
													</h4>
												</div>
												<div id="collapseTwentyLeftThree" class="panel-collapse collapse in" aria-expanded="true" role="tablist">
													<div class="panel-body">
														<?=$data->announce?>
													</div>
												</div>
											</div>
											<!-- /.panel-default -->
										</div>
										<!--end of /.panel-group-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php } else {

		redirect(base_url());

	 }?>

    <script type="text/javascript">
    	$(function(){
    		$(document).on('click','.detail', function(){
    			var seo = $(this).prop('id');

    			window.location = '<?=base_url()?>portal/jobs/'+seo;
    		})
    	})

    </script>
