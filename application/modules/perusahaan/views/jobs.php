    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />
    <style type="text/css">
    	a[disabled] {
		    pointer-events: none;
		}

        .image {
            width: 70px;
            height: 70px;
            object-fit: contain;
        }

        .jp_contact_inputs4_wrapper textarea {
            margin-top: 0px;
            padding-left: 50px;
        }

        .jp_contact_inputs4_wrapper i {
            top : 21px;
        }

        .jp_contact_form_btn_wrapper ul{
            margin-top: 10px;
        }

        .jp_contact_form_btn_wrapper ul li{
            margin-bottom: 20px;
            float: right;
        }

        .komenarea{
            border: 1px solid #282d39!important; 
            border-radius: 10px; 
            padding: 10px;
            margin-top: 10px;            
        }

        .delborder{
            border-left: 1px solid #3e4148;
            padding-left: 20px;
        }
		.centerqr {
  display: block;
  margin-left: auto;
  margin-right: auto;
 
}
.mainmenu ul li a {

padding: 0px 7px 47px 10px;
}
    </style>
    <?php
        $seo = $this->uri->segment(3);

        $rs = $this->db->query('select a.*, f.name as upah, date_format(a.date_start,"%d %M %Y") as tgl_awal, date_format(a.date_end,"%d %M %Y") as tgl_akhir, date_end, date_format(a.log,"%d %M %Y") as tgl, b.nama_job, c.photo, c.nama_perusahaan, 
                                d.subdistrict_name, d.city, d.province, e.category from (select * from ngi_joblist where seo_url = "'.$seo.'" ) a
                                left join ngi_job b on a.posisi = b.id
                                left join ngi_perusahaan c on a.idperusahaan = c.idperusahaan
                                left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                                left join ngi_jobcategory e on a.idcategory = e.id
								left join ngi_salary f on a.upah_kerja = f.id');

        if($rs->num_rows() > 0){
            $data = $rs->row();
            $this->db->insert('ngi_jobtrend',array('id_joblist' => $data->id));
            if($this->session->userdata('is_login_pelamar')){
                $is_pelamar = $this->db->query('select id from ngi_jobapplied where id_job = "'.$data->id.'" and id_pelamar = "'.$this->session->userdata('id').'"')->num_rows();    
            }

            $keyword = explode(',', $data->keyword);
    ?>

    <div class="jp_listing_single_main_wrapper" style="padding-bottom:0px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2><?=$data->title?>&nbsp;</h2>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp listing Single cont Wrapper Start -->
    <div class="jp_listing_single_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_rightside_listing_single_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Gambaran umum</h4>
                                </div>
                                <div class="jp_jop_overview_img_wrapper">
                                    <div class="jp_jop_overview_img">
                                        <img src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_company.jpg')?>" alt="company_img" style="object-fit: contain; width: 100px; height: 95px;" />
                                    </div>
                                </div>
                                <div class="jp_job_listing_single_post_right_cont">
                                    <div class="jp_job_listing_single_post_right_cont_wrapper">
                                        <h4><?=$data->nama_job?></h4>
                                        <p><?=$data->nama_perusahaan?></p>
                                    </div>
                                </div>
                                <div class="jp_job_post_right_overview_btn_wrapper">
                                    <div class="jp_job_post_right_overview_btn">
                                        <ul>
                                            <!-- <li><a href="#"><i class="fa fa-heart-o"></i></a></li> -->
                                            <li><a href="#"><?=$data->tipe?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jp_listing_overview_list_outside_main_wrapper">
                                    <div class="jp_listing_overview_list_main_wrapper">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Tanggal posting</li>
                                                <li><?=$data->tgl?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Masa lowongan</li>
                                                <li><?=$data->tgl_awal?> ~ <?=$data->tgl_akhir?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Lokasi</li>
                                                <li><?=$data->subdistrict_name?>, <?=$data->city?>, <?=$data->province?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-info-circle"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Posisi</li>
                                                <li><?=$data->nama_job?></li>
                                            </ul>
                                        </div>
                                    </div>
                                     <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Min. pendidikan</li>
                                                <li><?=$data->min_pendidikan?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Upah kerja</li>
                                                <li>Rp. <?=($data->upah)?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-th-large"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Kategori</li>
                                                <li><?=$data->category?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-group"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Jumlah dibutuhkan</li>
                                                <li><?=$data->jml_pekerja?> orang</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-cogs"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Skill dibutuhkan</li>
                                                <li><?=$data->skill?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_right_bar_btn_wrapper">
                                        <div class="jp_listing_right_bar_btn">
                                            <ul>
                                                <!-- <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Apply With Facebook</a></li> -->
                                                <?php if($is_pelamar == 1) { ?>
                                                	<li><a class=""  disabled="disabled" tabIndex="-1"><i class="fa fa-check"></i> &nbsp;Sudah dilamar</a></li>
                                                 <?php }else if($data->is_aktif == 0 || strtotime($data->date_end."23:59:59") < time()){?>
                                                    <li><a class=""  disabled="disabled" tabIndex="-1"><i class="fa fa-info-circle"></i> &nbsp;Lowongan tutup</a></li>
                                                <?php }else {?>
                                                	<li><a class="applynow"><i class="fa fa-plus-circle"></i> &nbsp;Lamar</a></li>
                                                <?php }?>
                                            </ul> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				
                    <div class="jp_listing_left_sidebar_wrapper">
					
                        <div class="jp_job_des">
						<img src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=<?=$data->id?>&choe=UTF-8" title="QRcode Untuk discan Pelamar" class="centerqr" /></br>
                            <h2>Deskripsi Pekerjaan</h2>
                            <p><?=(($data->deskripsi == '')? 'Tidak ada deskripsi untuk lowongan ini' : $data->deskripsi)?></p>
                        </div>
                        <div class="jp_job_res">
                            <h2>Tanggung Jawab</h2>
                            <p><?=(($data->responsibility == '')? 'Tidak ada keterangan responsibility untuk lowongan ini' : $data->responsibility)?></p>
                        </div>
                        <div class="jp_job_res jp_job_qua">
                            <h2>Kualifikasi</h2>
                            <p><?=(($data->qualification == '')? 'Tidak ada keterangan kualifikasi untuk lowongan ini' : $data->qualification)?></p>
                        </div>
                        <div class="jp_job_apply">
                            <h2>Lain-lain</h2>
                            <p><?=(($data->next_step == '')? 'Tidak ada keterangan lain-lain untuk lowongan ini' : $data->next_step)?></p>
                        </div>
                    </div>
                    <!-- <div class="jp_listing_left_bottom_sidebar_wrapper">
                        <div class="jp_listing_left_bottom_sidebar_social_wrapper">
                            <ul class="hidden-xs">
                                <li>SHARE :</li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li class="hidden-xs"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="hidden-xs"><a href="#"><i class="fa fa-vimeo"></i></a></li>
                            </ul>
                        </div>
                    </div> -->
                    <div class="jp_listing_left_bottom_sidebar_key_wrapper">
                        <ul>
                            <li><i class="fa fa-tags"></i>Keywords :</li>
                            <?php  foreach ($keyword as $kw) { ?>
                                <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_rightside_listing_single_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Gambaran umum</h4>
                                </div>
                                <div class="jp_jop_overview_img_wrapper">
                                    <div class="jp_jop_overview_img">
                                        <img src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_company.jpg')?>" alt="company_img" style="object-fit: contain; width: 100px; height: 95px;" />
                                    </div>
                                </div>
                                <div class="jp_job_listing_single_post_right_cont">
                                    <div class="jp_job_listing_single_post_right_cont_wrapper">
                                        <h4><?=$data->nama_job?></h4>
                                        <p><?=$data->nama_perusahaan?></p>
                                    </div>
                                </div>
                                <div class="jp_job_post_right_overview_btn_wrapper">
                                    <div class="jp_job_post_right_overview_btn">
                                        <ul>
                                            <!-- <li><a href="#"><i class="fa fa-heart-o"></i></a></li> -->
                                            <li><a href="#"><?=$data->tipe?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jp_listing_overview_list_outside_main_wrapper">
                                    <div class="jp_listing_overview_list_main_wrapper">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Tanggal posting</li>
                                                <li><?=$data->tgl?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Masa lowongan</li>
                                                <li><?=$data->tgl_awal?> ~ <?=$data->tgl_akhir?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Lokasi</li>
                                                <li><?=$data->subdistrict_name?>, <?=$data->city?>, <?=$data->province?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-info-circle"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Posisi</li>
                                                <li><?=$data->nama_job?></li>
                                            </ul>
                                        </div>
                                    </div>
                                     <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Min. pendidikan</li>
                                                <li><?=$data->min_pendidikan?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Upah kerja</li>
                                                <li>Rp. <?=(($data->upah == "")? $data->upah_kerja : $data->upah)?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-th-large"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Kategori</li>
                                                <li><?=$data->category?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-group"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Jumlah dibutuhkan</li>
                                                <li><?=$data->jml_pekerja?> orang</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jp_listing_overview_list_main_wrapper2">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-cogs"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>Skill dibutuhkan</li>
                                                <li><?=$data->skill?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_right_bar_btn_wrapper">
                                        <div class="jp_listing_right_bar_btn">
                                            <ul>
                                                <!-- <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;Apply With Facebook</a></li> -->
                                                <?php if($is_pelamar == 1) { ?>
                                                	<li><a class=""  disabled="disabled" tabIndex="-1"><i class="fa fa-check"></i> &nbsp;Sudah dilamar</a></li>
                                                 <?php }else if($data->is_aktif == 0 || strtotime($data->date_end."23:59:59") < time()){?>
                                                    <li><a class=""  disabled="disabled" tabIndex="-1"><i class="fa fa-info-circle"></i> &nbsp;Lowongan tutup</a></li>
                                                <?php }else {?>
                                                	<li><a class="applynow"><i class="fa fa-plus-circle"></i> &nbsp;Lamar</a></li>
                                                <?php }?>
                                            </ul> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_blog_single_comment_main_wrapper">
                        <div class="jp_blog_single_comment_main">
                            <?php 
                            $type = "";
                            $jml = $this->db->query(" select * from ngi_jobcomment where is_aktif = 1 and id_job = '".$data->id."'")->num_rows(); 

                             if($this->session->userdata('is_login_pelamar')){
                                $type = "pelamar";
                                $profil = $this->db->query('select iduser as id, nama as nama from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();
                             }else if($this->session->userdata('is_login_perusahaan')){
                                $type = "perusahaan";
                                $profil = $this->db->query('select idperusahaan as id, nama_perusahaan as nama from ngi_perusahaan where idperusahaan = "'.$this->session->userdata('id').'"')->row();
                             }

                            ?>

                            <h2><?=$jml?> Komentar</h2>
                        </div>
                        <div class="jp_blog_single_comment_box_wrapper">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div style="padding-bottom: 10px">
                                        <?php 
                                            if($this->session->userdata('is_login_pelamar')){
                                                echo "Anda login sebagai ".$profil->nama;
                                             }else if($this->session->userdata('is_login_perusahaan')){
                                                echo "Anda login sebagai ".$profil->nama;
                                             }else{
                                                echo "Silahkan login untuk bisa berkomentar atau bertanya";
                                             }
                                        ?>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                                        <i class="fa fa-text-height"></i><textarea rows="6" placeholder="Tulis pertanyaan atau komentar anda" id="komentar"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="jp_contact_form_btn_wrapper">
                                        <ul>    
                                            <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                                <li><a class="komen"><i class="fa fa-plus-circle"></i>&nbsp; KOMENTAR</a></li>
                                            <?php }else{?>
                                                <li><a class="login"><i class="fa fa-sign-in"></i>&nbsp; LOGIN</a></li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php 

                                $result = $this->db->query("select a.* from (select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama from (select * from ngi_jobcomment where is_primary = 1 and is_aktif = 1 and id_job = '".$data->id."') a 
                                                            inner join user_pelamar b on a.id_user = b.iduser and a.tipe_user = 'pelamar'

                                                            union

                                                            select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s')as tgl, b.photo, b.nama_perusahaan from (select * from ngi_jobcomment where is_primary = 1 and is_aktif = 1 and id_job = '".$data->id."') a 
                                                            inner join ngi_perusahaan b on a.id_user = b.idperusahaan and a.tipe_user = 'perusahaan') a

                                                            order by a.log desc
                                                            ")->result();

                                foreach ($result as $komentar) {
                                    
                                ?>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 50px">
                                    <div class="jp_blog_comment_main_section_wrapper">
                                        <div class="jp_blog_sin_com_img_wrapper">
                                            <?php if($komentar->tipe_user == "pelamar"){?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($komentar->photo != '')? $komentar->photo : 'default_user.png')?>" alt="blog_img"/>
                                            <?php }else{?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($komentar->photo != '')? $komentar->photo : 'default_company.jpg')?>" alt="blog_img"/>
                                            <?php } ?>
                                        </div>
                                        <div class="jp_blog_sin_com_cont_wrapper">
                                            <ul>
                                                <li><i class="fa fa-user"></i>&nbsp;&nbsp; <a ><?=$komentar->nama?></a></li>
                                                <li><i class="fa fa-calendar"></i>&nbsp;&nbsp; <a><?=$komentar->tgl?></a></li>
                                                <li><i class="fa fa-reply"></i>&nbsp;&nbsp; <a href="#" class="replybtn">Balas</a></li>
                                                <?php if($komentar->id_user == $profil->id && $komentar->tipe_user == $type){ ?>
                                                    <li class="delborder"><i class="fa fa-trash"></i>&nbsp;&nbsp; <a href="#" onclick="hapus('<?=$komentar->id?>')" class="deletebtn">Hapus</a></li>
                                                <?php } ?>

                                            </ul>
                                            <p class="komenarea"><?=$komentar->komen?></p> 
                                            <div class="row reply" style="display: none;">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                                                        <i class="fa fa-text-height"></i><textarea rows="2" placeholder="Tulis pertanyaan atau komentar anda" class="komentar"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_form_btn_wrapper">
                                                        <ul>    
                                                            <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                                                <li><a onclick="balas('<?=$komentar->id?>', this)">KOMENTAR</a></li>
                                                            <?php }else{?>
                                                                <li><a id="login">LOGIN</a></li>
                                                            <?php }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php 

                                $result2 = $this->db->query("select a.* from (select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama from (select * from ngi_jobcomment where is_aktif = 1 and primary_comment = '".$komentar->id."') a 
                                                            inner join user_pelamar b on a.id_user = b.iduser and a.tipe_user = 'pelamar'

                                                            union

                                                            select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama_perusahaan from (select * from ngi_jobcomment where is_aktif = 1 and primary_comment = '".$komentar->id."') a 
                                                            inner join ngi_perusahaan b on a.id_user = b.idperusahaan and a.tipe_user = 'perusahaan') a

                                                            order by a.log asc
                                                            ")->result();

                                foreach ($result2 as $sub_komen) {

                                ?>

                                <div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="jp_blog_comment_main_section_wrapper jp_blog_comment_main_section_wrapper2 jp_blog_post_dots_wrapper">
                                        <div class="jp_blog_sin_com_img_wrapper">
                                            <?php if($sub_komen->tipe_user == "pelamar"){?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($sub_komen->photo != '')? $sub_komen->photo : 'default_user.png')?>" alt="blog_img"/>
                                            <?php }else{?>
                                                <img class="image" src="<?=base_url()?>assets/img/profile/<?=(($sub_komen->photo != '')? $sub_komen->photo : 'default_company.jpg')?>" alt="blog_img"/>
                                            <?php } ?>
                                        </div>
                                        <div class="jp_blog_sin_com_cont_wrapper">
                                            <ul>
                                                <li><i class="fa fa-user"></i>&nbsp;&nbsp; <a href="#"><?=$sub_komen->nama?></a></li>
                                                <li><i class="fa fa-calendar"></i>&nbsp;&nbsp; <a href="#"><?=$sub_komen->tgl?></a></li>
                                                <li><i class="fa fa-reply"></i>&nbsp;&nbsp; <a href="#" class="replybtn">Balas</a></li>
                                                <?php if($sub_komen->id_user == $profil->id && $sub_komen->tipe_user == $type){ ?>
                                                    <li class="delborder"><i class="fa fa-trash"></i>&nbsp;&nbsp; <a href="" onclick="hapus('<?=$sub_komen->id?>')" class="deletebtn">Hapus</a></li>
                                                <?php } ?>
                                            </ul>
                                            <p  class="komenarea"><?=$sub_komen->komen?></p>
                                            <div class="row reply" style="display: none;">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                                                        <i class="fa fa-text-height"></i><textarea rows="2" placeholder="Tulis pertanyaan atau komentar anda" class="komentar"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="jp_contact_form_btn_wrapper">
                                                        <ul>    
                                                            <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                                                <li><a onclick="balas('<?=$komentar->id?>', this)">KOMENTAR</a></li>
                                                            <?php }else{?>
                                                                <li><a class="login"><i class="fa fa-sign-in">LOGIN</a></li>
                                                            <?php }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="jp_blog_small_dots hidden-sm hidden-xs">
                                    </div>
                                    <div class="jp_blog_small_dots2 hidden-sm hidden-xs">
                                    </div>
                                </div>

                                <?php }
                                    echo "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='border-bottom:1px solid #282d39!important;padding-bottom:25px;'></div>";
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php } else { ?>

      <div class="error_page" style="padding-top: 200px !important; padding-bottom: 200px !important;">
        <div class="container">
            <div class="row">
             <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                    <div class="error_page_cntnt">
                        <h2>
                            <span>4</span>
                            <span>0</span>
                            <span>4</span>
                        </h2>
                        <h3>Sorry, This Page Isn't available :(</h3>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. A Aenean sollicitudin, lorem quis bibend Aenean sollicitudin, lorem . <a href="index.html">Home Page</a></p>
                       <!--  <div class="error_page_mail_wrapper">
                            <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a><input type="text" placeholder="Email Address *">
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <div class="modal fade" id="myModal" role="dialog" style="">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Login</h4>
	        </div>
	        <div class="modal-body">
	          <p>Silahkan login sebagai pencaker dahulu untuk bisa melamar pekerjaan</p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
	          <a type="button" class="btn btn-primary login">Login</a>
	        </div>
	      </div>
	    </div>
	  </div>

    <script type="text/javascript">
        $(function(){
            $(document).on('click','.trending', function(){
                var trend = $(this).prop('id');

                sendData(trend);
            }).on('click touchstart', '.applynow', function(e){
            	$(this).html('<i class="fa fa-circle-o-notch fa-spin"></i> &nbsp;Sedang proses');

            	$.ajax({
            		url: '<?=base_url()?>portal/page/lamar',
            		type: 'post',
            		data: {
            			 <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
            			 id : '<?=$this->session->userdata('id')?>',
            			 id_job : '<?=$data->id?>'

            		},
            		dataType: 'json',
            		success: function(result){
            			if(result.login){
            				if(result.success){
            					$('.applynow').html('<i class="fa fa-check"></i> &nbsp;Sudah dilamar');
            					$('.applynow').attr('disabled',true);
            					$('.applynow').attr('tabIndex','-1');
            					$('.applynow').removeClass('applynow');
                                alert('Berhasil melamar pekerjaan. \nSilahkan lengkapi lampiran lamaran anda');
                                window.location = '<?=base_url()?>portal/lamaran';
            				}else{
            					alert('Terjadi kesalahan pada server. Silahkan coba lagi.');
            					$('.applynow').html('<i class="fa fa-plus-circle"></i> &nbsp;Lamar');
            				}
            			}else{
            				$('#myModal').modal('show');
            				$('.applynow').html('<i class="fa fa-plus-circle"></i> &nbsp;Lamar');
            			}
            		}

            	});

            	e.preventDefault();

            }).on('click', 'a', function(event){
            	if($(this).is("[disabled]")){
			        event.preventDefault();
			    }

            }).on('click touchstart', '.komen', function(){
                if($('#komentar').val() == ""){
                    $('#komentar').focus()
                }else{
                    post('<?=base_url()?>portal/page/savekomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id_job : '<?=$data->id?>',
                        id_user : '<?=$profil->id?>',
                        tipe_user: '<?=$type?>',
                        komen: $('#komentar').val(),
                        is_primary: 1,
                        url : window.location.href
                    }, "post");
                }

            }).on('click touchstart', '.login', function(){
                post('<?=base_url()?>portal/login', {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    url : window.location.href

                }, "post");

            }).on('click touchstart', '.replybtn', function(e){
                $('.reply').hide();
                $(this).closest('div').find('.reply').show();
                $(this).closest('div').find('.reply').find('textarea').focus();

                e.preventDefault();
            });

        });

        function sendData(trending = ''){
            post('<?=base_url()?>portal/caripekerjaan', {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    keyword : trending
                }, "post");
        }

        function hapus(id){
            if(confirm('Yakin ingin menghapus komen ini?')){
                post('<?=base_url()?>portal/page/hapuskomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id : id, 
                        url : window.location.href
                    }, "post");
            }
        }

        function balas(idkomen, obj){
            var reply = obj.closest('.reply').children[0].children[0].children[1];

            if(reply.value == ""){
                reply.focus();
            }else{
                post('<?=base_url()?>portal/page/savekomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id_job : '<?=$data->id?>',
                        id_user : '<?=$profil->id?>',
                        tipe_user: '<?=$type?>',
                        komen: reply.value,
                        primary_comment : idkomen,
                        is_primary: 0,
                        url : window.location.href
                    }, "post");
            }
        }


        function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }
    </script>
