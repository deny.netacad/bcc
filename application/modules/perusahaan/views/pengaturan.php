 <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />

<style type="text/css">
    .jp_recent_resume_cont_wrapper{
        padding-top: 0px !important;
        width: 100% !important;
    }

    .fa-icon{
        font-size: 15px;
        color: #f36969;
    }

    .mobile-btn {
        float: right;
        padding-top: 10px;
    }

    .accordion_wrapper .panel-group .panel-heading+.panel-collapse>.panel-body {
        background: #12171f;
        padding: 25px 0px 30px 0px;
    }

    .ket {
        border: 1px solid #1a1e27;
        background: #282d39;
        color: #6a7c98;
        font-size: 17px;
    }

    .cek {
        border: 1px solid #1a1e27;
        background: #181d28;
        color: #6a7c98;
        font-size: 17px;
    }

    .jp_adp_form_wrapper input, .jp_adp_form_wrapper select {
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .jp_adp_choose_resume_bottom_btn_post li button {
        width: 160px;
        height: 50px;
        text-align: center;
        color: #ffffff;
        font-weight: bold;
        background: #f36969;
        border: #f36969;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        -webkit-transition: all 0.5s;
        -o-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -moz-transition: all 0.5s;
        transition: all 0.5s;
    }

    .jp_adp_choose_resume_bottom_btn_post li button[disabled] {
        background: #f19d9d;
        border: #f19d9d;
    }

    #autocomplete {
        background-color: #181d28;
        border-radius: 3px;
        border: 1px solid #212b2d;
        max-height: 250px;
        overflow-y: auto;
    }


    #autocomplete ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
 
    #autocomplete ul li {
        padding: 10px 20px;
        border-bottom: 1px solid #212b2d;
    }

    #autocomplete ul li:focus,
    #autocomplete ul li:hover {
        background-color: #F0F0F0;
        cursor: pointer;
    }


    .tag {
      font-size: 14px;
      padding: .3em .4em .4em;
      margin: 0 .1em;
    }

    .tag a {
      color: #bbb;
      cursor: pointer;
      opacity: 0.6;
    }

    .tag a:hover {
      opacity: 1.0
    }

    .tag .remove {
      vertical-align: bottom;
      top: 0;
    }

    .tag a {
      margin: 0 0 0 .3em;
    }

    .tag a .glyphicon-white {
      color: #fff;
      margin-bottom: 2px;
    }

    .jp_adp_textarea_main_wrapper.no-topborder{
        border-top: none;
        padding-top: 0px;
    }
 
</style>

<?php
    
    if($this->session->userdata('is_login_perusahaan')){
    
    $biodata = $this->db->query('select * from ngi_perusahaan where idperusahaan = "'.$this->session->userdata('id').'"')->row();
?>

<div class="jp_listing_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar" style="padding-top: 20px">
                <div class="jp_job_des">
                    <h2>Pengaturan</h2>
                </div>
                <div class="row" style="padding-top: 80px">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="accordion_wrapper abt_page_2_wrapper">
                            <div class="panel-group" id="accordion_threeLeft">
                                <!-- /.panel-default -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree" aria-expanded="true">
                                                PROFIL
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree" class="panel-collapse collapse in" aria-expanded="true" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>perusahaan/page/saveprofil" method="post" id="form">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->idperusahaan?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Nama lengkap</span>
                                                                <input type="text" placeholder="Nama lengkap perusahaan tidak boleh disingkat" name="nama_perusahaan" id="nama_perusahaan" required="" value="<?=$biodata->nama_perusahaan?>">
                                                            </div>
                                                        </div>
                                                         <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Alamat</span>
                                                                <input type="text" placeholder="Alamat lengkap perusahaan" name="alamat" id="alamat" required="" value="<?=$biodata->alamat?>">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Provinsi</span>
                                                                        <select name="province_id">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $q1 = $this->db->query('select province_id, province from ngi_kecamatan group by province_id')->result();

                                                                                foreach ($q1 as $province) {
                                                                                    if($province->province_id == $biodata->province_id){
                                                                                        echo '<option value="'.$province->province_id.'" selected>'.$province->province.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$province->province_id.'">'.$province->province.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Kab/Kota</span>
                                                                        <select name="kab_id">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $q2 = $this->db->query('select city_id, city, type from ngi_kecamatan where province_id = "'.$biodata->province_id.'" group by city_id')->result();

                                                                                foreach ($q2 as $city) {
                                                                                    if($city->city_id == $biodata->kab_id){
                                                                                        echo '<option value="'.$city->city_id.'" selected>'.$city->type.' '.$city->city.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$city->city_id.'">'.$city->type.' '.$city->city.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Kecamatan</span>
                                                                        <select name="kec_id">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $q3 = $this->db->query('select subdistrict_id, subdistrict_name from ngi_kecamatan where city_id = "'.$biodata->kab_id.'"')->result();

                                                                                foreach ($q3 as $kec) {
                                                                                    if($kec->subdistrict_id == $biodata->kec_id){
                                                                                        echo '<option value="'.$kec->subdistrict_id.'" selected>'.$kec->subdistrict_name.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$kec->subdistrict_id.'">'.$kec->subdistrict_name.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Nama Direktur</span>
                                                                        <input type="text" placeholder="Nama lengkap direktur perusahaan" name="nm_dir" id="nm_dir" required="" value="<?=$biodata->nm_dir?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">NIK Direktur</span>
                                                                        <input type="text" placeholder="Nomor induk kependudukan direktur perusahaan" name="noktpdir" id="noktpdir" required="" value="<?=$biodata->noktpdir?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Email</span>
                                                                        <input type="text" placeholder="Alamat email perusahaan yang masih aktif" name="email" id="email" required="" value="<?=$biodata->email?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">No. telfon</span>
                                                                        <input type="text" placeholder="Nomor telfon atau handphone yg bisa dihubungi" name="telp" id="telp" required="" value="<?=$biodata->telp?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Lokasi</span>
                                                                        <select name="lokasi">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $jk = array('Pusat', 'Cabang');

                                                                                foreach ($jk as $select1) {
                                                                                    if($select1 == $biodata->lokasi){
                                                                                        echo '<option value="'.$select1.'" selected>'.$select1.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$select1.'">'.$select1.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Jenis Usaha</span>
                                                                        <select name=jenisusaha>
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                 $q4 = $this->db->query('select * from mast_jenisusaha')->result();

                                                                                foreach ($q4 as $jenisusaha) {
                                                                                    if($jenisusaha->id == $biodata->jenisusaha){
                                                                                        echo '<option value="'.$jenisusaha->id.'" selected>'.$jenisusaha->nama.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$jenisusaha->id.'">'.$jenisusaha->nama.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Kewarganegaraan</span>
                                                                        <select name="wn">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $wn = array('WNI', 'WNA');

                                                                                 foreach ($wn as $select3) {
                                                                                    if($select3 == $biodata->wn){
                                                                                        echo '<option value="'.$select3.'" selected>'.$select3.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$select3.'">'.$select3.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="jp_adp_form_wrapper">
                                                                     <div class="input-group">
                                                                        <span class="input-group-addon ket">Pend. terakhir</span>
                                                                        <select name="pendidikan">
                                                                            <option value="">Pilih salah satu</option>
                                                                            <?php
                                                                                $pend = array('SD', 'SMP', 'SMA', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3', 'Akta IV', 'Akuntan', 'Apoteker', 'Kedokteran', 'Keperawatan', 'Notaris', 'Ners');

                                                                                 foreach ($pend as $select4) {
                                                                                    if($select4 == $biodata->pendidikan){
                                                                                        echo '<option value="'.$select4.'" selected>'.$select4.'</option>';
                                                                                    }else{
                                                                                        echo '<option value="'.$select4.'">'.$select4.'</option>';
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div -->
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px">
                                                        <div class="jp_adp_textarea_main_wrapper">
                                                            <div style="padding-bottom: 25px">
                                                                <textarea rows="7" placeholder="Tulis sesuatu tentang perusahaan ini" name="ttg_anda" id="ttg_anda" required=""><?=str_replace("<br />","\n",$biodata->ttg_anda)?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree4" aria-expanded="false">
                                                AKUN
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree4" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>perusahaan/page/saveakun" method="post" id="form2" enctype="multipart/form-data">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->idperusahaan?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <img src="<?=base_url()?>assets/img/profile/<?=(($biodata->photo == '')? 'default_company.jpg' : $biodata->photo )?>" style="width: 300px; height: 300px; object-fit: contain;">
                                                        <input type="file" name="scanfile" style="padding-top: 10px;">
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Username</span>
                                                                <input type="text" name="username" id="username" value="<?=$biodata->username?>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">URL Profil</span>
                                                                <input type="text" placeholder="<?=base_url()?>protal/profile/" name="profile_url" id="profile_url" required="" value="<?=$biodata->profile_url?>" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka, 3 - 10 karakter">
                                                                <span class="input-group-addon cek" id="url-change"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit2">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <form action="<?=base_url()?>perusahaan/page/savepassword" method="post" id="form3">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->idperusahaan?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Password Lama</span>
                                                                <input type="password" id="pass_lama">
                                                                <span class="input-group-addon cek" id="pass_lama-change"></span>
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Password baru</span>
                                                                <input type="password" name="pass_baru" id="pass_baru" required="">
                                                            </div>
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                             <div class="input-group">
                                                                <span class="input-group-addon ket">Konfirmasi Password baru</span>
                                                                <input type="password" required="" id="confirm_pass">
                                                                <span class="input-group-addon cek" id="confirm_pass-change"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit3">Ganti</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree2" aria-expanded="false">
                                                KEAHLIAN
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree2" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>portal/page/saveskilluser" method="post" id="form4">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->idperusahaan?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <input type="hidden" id="all-skill" name="skill">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="tag-area">
                                                            <?php 
                                                                if($biodata->skill != ""){
                                                                    $skill = explode(',', $biodata->skill);
                                                                    foreach ($skill as $s) {
                                                                        echo '<span class="tag label label-default">
                                                                              <span class="skill-name">'.$s.'</span>
                                                                              <a class="close-tag"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a> 
                                                                            </span>';
                                                                    } 
                                                                }
                                                            ?>    
                                                        
                                                        </div>
                                                        <div class="jp_adp_form_wrapper">
                                                            <input type="text" id="skill" placeholder="Cari skill yang anda kuasai">
                                                            <div id="autocomplete"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit4">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree3" aria-expanded="false">
                                                PENGUMUMAN
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwentyLeftThree3" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                        <div class="panel-body">
                                            <form action="<?=base_url()?>perusahaan/page/saveannounce" method="post" id="form5">
                                                <div style="padding-top: 10px">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="id_edit" required="" value="<?=$biodata->idperusahaan?>">
                                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_textarea_main_wrapper no-topborder">
                                                            <div style="">
                                                                <textarea rows="7" placeholder="Tulis pengumuman yang akan dibagikan pada profil perusahaan" name="announce" id="announce" required=""><?=str_replace("<br />","\n",$biodata->announce)?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="jp_adp_choose_resume_bottom_btn_post">
                                                            <ul>
                                                                <li><button type="submit" id="submit5">Simpan</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                            </div>
                            <!--end of /.panel-group-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } else{

    redirect(base_url().'portal/login');
}?>

<script type="text/javascript">
    var skill = new Array();

    $(function(){
        $('.dt').datepicker({
            format:'yyyy-mm-dd',
            //format:'dd-mm-yyyy',
            minViewMode:0,
            viewMode:1
        }).on('changeDate',function(){
            $('.dt').datepicker('hide');
        });

        $(document).on('click','#view',function(){
            $.ajax({
                type: 'post',
                url: '<?=base_url()?>perusahaan/page/viewmore',
                data: {
                    '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                    'more' : $(this).attr('data')
                },
                success:function(result){
                    $('#tempat').html(result);
                }
            })
        }).on('click', '.closes', function(){
            if(confirm('Anda yakin ingin menutup lowongan ini?')){
                $.ajax({
                    type: 'post',
                    url: '<?=base_url()?>perusahaan/page/tutuplowongan',
                    data: {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'id' : $(this).prop('id')
                    },
                    success:function(result){
                        alert(result);
                        location.reload();
                    }
                })
            }
        }).on('click', '.delete', function(){
              if(confirm('Anda yakin ingin menghapus lowongan ini?')){
                $.ajax({
                    type: 'post',
                    url: '<?=base_url()?>perusahaan/page/hapuslowongan',
                    data: {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'id' : $(this).prop('id')
                    },
                    success:function(result){
                        alert(result);
                        location.reload();
                    }
                })
            }
        }).on('click', '.edit', function(){
            post('<?=base_url()?>perusahaan/buatlowongan', 
                {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', edit : $(this).prop('id')}, 
                "post");

        }).on('input', '#profile_url', function(e){
            var value = $(this).val();

            $('#url-change').html('<i class="fa fa-spinner fa-spin"></i>');
			
			var value = $(this).val();            
            var regexp = /[A-Za-z0-9]{3,}$/
			
			if(value.match(regexp)){
				$.ajax({
					url : '<?=base_url()?>perusahaan/page/checkurlprofile',
					type: 'post',
					dataType: 'json',
					data : {
						'<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
						'url' : value
					},
					success: function(result){

						if(result.available){
							$('#url-change').html('<i class="fa fa-check"></i>');
							$('#submit2').attr('disabled',false);
						}else{
							$('#url-change').html('<i class="fa fa-times"></i>');
							$('#submit2').attr('disabled',true);
						}
					}
				});
			}else{
                $('#url-change').html('<i class="fa fa-times"></i>');
                $('#submit2').attr('disabled',true);
            }

            e.preventDefault();
        }).on('input', '#pass_lama', function(){
            var value = $(this).val();

            $('#pass_lama-change').html('<i class="fa fa-spinner fa-spin"></i>');

            $.ajax({
                url : '<?=base_url()?>perusahaan/page/checkpassword',
                type: 'post',
                dataType: 'json',
                data : {
                    '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                    'pass' : value
                },
                success: function(result){

                    if(result.available){
                        $('#pass_lama-change').html('<i class="fa fa-check"></i>');
                        $('#submit3').attr('disabled',false);
                    }else{
                        $('#pass_lama-change').html('<i class="fa fa-times"></i>');
                        $('#submit3').attr('disabled',true);
                    }
                }
            });
        }).on('input', '#confirm_pass', function(){
            var value = $(this).val();

            $('#confirm_pass-change').html('<i class="fa fa-spinner fa-spin"></i>');

            if($('#pass_baru').val() == value){
                $('#confirm_pass-change').html('<i class="fa fa-check"></i>');
                $('#submit3').attr('disabled',false);
            }else{
                $('#confirm_pass-change').html('<i class="fa fa-times"></i>');
                $('#submit3').attr('disabled',true);
            }
        }).on('change', 'select[name="province_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikab',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kab_id"]').html(result);
                }
            })
        }).on('change', 'select[name="kab_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikec',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kec_id"]').html(result);
                }
            })
        }).on('submit', 'form', function(){
			$(this).find('textarea').val($(this).find('textarea').val().replace(/\r\n|\r|\n/g,"<br />"));
		});
    });

 function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

 function gatherSkill(){
    $('.skill-name').each(function(){
        skill.push($(this).text().toLowerCase());
    });
 }
</script>
