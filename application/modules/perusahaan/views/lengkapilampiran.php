<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />
<style type="text/css">
	.jp_adp_choose_resume_bottom_btn_post li button {
	    float: left;
	    width: 170px;
	    height: 50px;
	    line-height: 50px;
	    color: #ffffff;
	    border: 1px solid #f36969;
	    background: #f36969;
	    text-align: center;
	    text-transform: uppercase;
	    -webkit-border-radius: 10px;
	    -moz-border-radius: 10px;
	    border-radius: 10px;
	    -webkit-transition: all 0.5s;
	    -o-transition: all 0.5s;
	    -ms-transition: all 0.5s;
	    -moz-transition: all 0.5s;
	    transition: all 0.5s;
	}

	#autocomplete {
        background-color: #181d28;
        border-radius: 3px;
        border: 1px solid #212b2d;
        max-height: 250px;
        overflow-y: auto;
    }


    #autocomplete ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
 
    #autocomplete ul li {
        padding: 10px 20px;
        border-bottom: 1px solid #212b2d;
    }

    #autocomplete ul li:focus,
    #autocomplete ul li:hover {
        background-color: #F0F0F0;
        cursor: pointer;
    }


    .tag {
      font-size: 14px;
      padding: .3em .4em .4em;
      margin: 0 .1em;
    }

    .tag a {
      color: #bbb;
      cursor: pointer;
      opacity: 0.6;
    }

    .tag a:hover {
      opacity: 1.0
    }

    .tag .remove {
      vertical-align: bottom;
      top: 0;
    }

    .tag a {
      margin: 0 0 0 .3em;
    }

    .tag a .glyphicon-white {
      color: #fff;
      margin-bottom: 2px;
    }

    .jp_adp_textarea_main_wrapper.no-topborder{
        border-top: none;
        padding-top: 0px;
    }
</style>
<script type="text/javascript">
	$(function(){

		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			//format:'dd-mm-yyyy',
			minViewMode:0,
			viewMode:1
		})


		$(document).on('change', '#province_id', function(){
			setKabupaten($(this).val());
		}).on('change', '#city_id', function(){
			setKecamatan($(this).val());
		}).on('submit', '#form', function(e){
			e.preventDefault();

			$.ajax({
				url : $(this).attr('action'),
				data : $(this).serialize(),
				type: 'post',
				dataType: 'json',
				success: function(result){
					if(result.success){
						alert('Berhasil menyimpan data');
						if($('#id_edit').val() == ""){
							window.location = '<?=base_url()?>perusahaan/buatlowongan';
						}else{
							window.location = '<?=base_url()?>perusahaan/lowongan';
						}
					}else{
						alert('Gagal menyimpan data');
					}
				},
				error: function(e, r, o){
					console.log(e,r,o);
				}
			})
		})
	})

	function setKabupaten(idprov, idkab){
		$.ajax({
			url: '<?=base_url()?>perusahaan/page/carikab',
			type: 'post',
			data: {
				<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
				id : idprov,
				ids : idkab
			},
			success: function(result){
				$('#city_id').html(result);
			}
		})
	}

	function setKecamatan(idkab, idkec){
		$.ajax({
			url: '<?=base_url()?>perusahaan/page/carikec',
			type: 'post',
			data: {
				<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
				id: idkab,
				ids: idkec
			},
			success: function(result){
				$('#idkecamatan').html(result);
			}
		})
	}

	function setWilayah(idprov,idkab,idkec){
		setKabupaten(idprov,idkab);
		setKecamatan(idkab,idkec);
	}

</script>
	<?php if($_POST['idjob'] != '') {  

		$data = $this->db->query("select id, attachment from ngi_joblist where id = '".$_POST['idjob']."'")->rows();
	?>
	<div class="jp_adp_main_section_wrapper" style="padding-top: 20px !important">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar">
					<div class="jp_job_des">
                        <h2>Lengkapi lampiran</h2>
                	</div>
				</div>
				<form action="<?=base_url()?>perusahaan/page/savelowongan" method="post" id="form">
					<div style="padding-top: 100px">
						<input type="hidden" placeholder="Judul Lowongan" name="id_edit" id="id_edit" required="">
						<input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php
							$array = explode(",", $data->attachment);
							foreach ($array as $att) {
						?>
							<div class="jp_adp_form_wrapper">
								<input type="file" name="files">
								<input type="hidden" name="idfile" value="<?=$att?>">
							</div>

						<?php } ?>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="jp_adp_choose_resume_bottom_btn_post">
								<ul>
									<li><button type="submit" id="submit"><i class="fa fa-plus-circle"></i>&nbsp; Simpan</button></li>
								</ul>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php } else { ?>

	 <div class="error_page" style="padding-top: 200px !important; padding-bottom: 200px !important;">
        <div class="container">
            <div class="row">
             <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                    <div class="error_page_cntnt">
                        <h2>
                            <span>4</span>
                            <span>0</span>
                            <span>4</span>
                        </h2>
                        <h3>Sorry, This Page Isn't available :(</h3>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. A Aenean sollicitudin, lorem quis bibend Aenean sollicitudin, lorem . <a href="index.html">Home Page</a></p>
                       <!--  <div class="error_page_mail_wrapper">
                            <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a><input type="text" placeholder="Email Address *">
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

	<?php } ?>