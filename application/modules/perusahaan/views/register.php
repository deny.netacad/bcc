
<script>
$(document).ready(function(){
	
		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			//format:'dd-mm-yyyy',
			minViewMode:0,
			viewMode:1
		}).on('changeDate',function(){
			$('.dt').datepicker('hide');
			loadDataKomoditas();
		});
		
	});
	

</script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">

 <div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Registrasi</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="<?=base_url()?>">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Registrasi</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp register wrapper start -->
    <div class="register_section">
        <!-- register_form_wrapper -->
        <div class="register_tab_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div role="tabpanel">

                            <!-- Nav tabs -->
                            <ul id="tabOne" class="nav register-tabs">
                                <li class="active"><a href="#contentOne-1" data-toggle="tab">Pelamar<br> <span>Saya sedang mencari pekerjaan</span></a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active register_left_form" id="contentOne-1">
									
                                    <div class="jp_regiter_top_heading">
                                        <p><b>AKUN LOGIN PENCARI KERJA</b></p>
                                    </div>
									<form method="post" data-remote="true" action="<?=base_url()?>portal/page/regPendaftar" accept-charset="UTF-8">
                                    <div class="row">
                                        <!--Form Group-->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="username" id="username" placeholder="Username*">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="password" name="password" id="password" placeholder=" password*">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="password" name="password_ori" id="password_ori" placeholder="Konfirmasi password*">
                                        </div>
                                       
                                    </div>
									<div class="jp_regiter_top_heading">
                                        <p><b>BIODATA</b></p>
                                    </div>
								
                                    <div class="row">
                                        <!--Form Group-->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="name" id="name" placeholder="Nama Lengkap*">
                                        </div>
										 <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="no_ktp" id="no_ktp" placeholder="Nomor Induk Kependudukan*">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">

                                            <input type="text" name="tmp_lahir" id="tmp_lahir" placeholder="Tempat Lahir*">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-2 col-sm-2 col-xs-12">

                                            <input type="text" class="dt" name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir*">
                                        </div>
										<div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="text" name="telp" id="telp" placeholder="Telp*">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="text" name="email" id="email" placeholder="Email*">
                                        </div> 
										<div class="form-group col-md-3 col-sm-3 col-xs-12">
											<select name="gender" id="gender">
												<option value="-">Jenis Kelamin*</option>
												<option value="Laki-laki">Laki-laki</option>
												<option value="Perempuan">Perempuan</option>
											</select>
										
                                        </div>
										<div class="form-group col-md-3 col-sm-3 col-xs-12">
											<select name="agama" id="agama">
												<option value="-">Agama*</option>
												<option value="Budha">Budha</option>
												<option value="Hindu">Hindu</option>
												<option value="Islam">Islam</option>
												<option value="Kristen">Kristen</option>
												<option value="Kristen-Katolik">Kristen-Katolik</option>
												<option value="Kristen Protestan">Kristen Protestan</option>
												<option value="Lainnya">Lainnya</option>
											</select>
										
                                        </div>
										<div class="form-group col-md-3 col-sm-3 col-xs-12">
											<select name="wn" id="wn">
												<option value="-">Kewarganegaraan*</option>
												<option value="WNI">WNI</option>
												<option value="WNA">WNA</option>
											</select>
										
                                        </div>
										<div class="form-group col-md-3 col-sm-3 col-xs-12">
											<select name="pendidikan" id="pendidikan">
												<option value="-">Pendidikan Terakhir*</option>
												<option value="SD">SD</option>
												<option value="SMP">SMP</option>
												<option value="SMA">SMA</option>
												<option value="D1">D1</option>
												<option value="D2">D2</option>
												<option value="D3">D3</option>
												<option value="D4">D4</option>
												<option value="S1">S1</option>
												<option value="S2">S2</option>
												<option value="S3">S3</option>
												<option value="Akta IV">Akta IV</option>
												<option value="Akuntan">Akuntan</option>
												<option value="Apoteker">Apoteker</option>
												<option value="Kedokteran">Kedokteran</option>
												<option value="Keperawatan">Keperawatan</option>
												<option value="Notaris">Notaris</option>
												<option value="Ners">Ners</option>
											</select>
										
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
										<select>
											<option value="">Kecamatan / Kelurahan</option>
                                    <?php
                                        $rs = $this->db->query("select * from ref_kecamatan")->result();

                                        foreach ($rs as $item) {
                                            if(isset($_POST)){
                                                if($_POST['location'] == $item->id){
                                                    echo '<option value="'.$item->id.'" selected="true">'.$item->nama_kec.'</option>';
                                                }else{
                                                    echo '<option value="'.$item->id.'">'.$item->nama_kec.'</option>';    
                                                }
                                            }else{
                                                echo '<option value="'.$item->id.'">'.$item->nama_kec.'</option>';
                                            }
                                        }
                                    ?>
										</select>
                                        </div>
                                        <!--Form Group-->
                                     <div class="form-group col-md-4 col-sm-4 col-xs-12">
											<select name="sts" id="sts">
												<option value="-">Status*</option>
												<option value="Lajang">Lajang</option>
												<option value="Nikah">Nikah</option>
												<option value="Cerai">Cerai</option>
											</select>
										
                                        </div>
                                       
                                    </div>
                                    
									
									 <div class="login_btn_wrapper register_btn_wrapper login_wrapper ">
                                        <a href="#" class="btn btn-primary login_btn"> Daftar </a>
                                    </div>
									
									</form>
                                    <div class="login_message">
                                        <p>Sudah Mempunyai Akun? <a href="#"> Login Disini </a> </p>
                                    </div>
                                </div>
								
                                

                            </div>
                            <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that you logout to prevent any un-authorized access to your account</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?=base_url()?>template/HTML/job_dark/js/custom_II.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>