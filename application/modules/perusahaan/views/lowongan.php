 <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_dark/css/responsive2.css" />

<style type="text/css">
    .jp_recent_resume_cont_wrapper{
        padding-top: 0px !important;
        width: 100% !important;
    }

    .fa-icon{
        font-size: 15px;
        color: #f36969;
    }

    .mobile-btn {
        float: right;
        padding-top: 10px;
    }
</style>

<div class="jp_listing_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar" style="padding-top: 20px">
                <div class="jp_job_des">
                    <h2>Lowongan Aktif</h2>
                </div>
                <div class="row" style="padding-top: 40px" id="tempat">
                    <?php

                        $q = $this->db->query('select a.*, b.nama_job, c.subdistrict_name, c.province, c.city, d.name as upah from (select id, upah_kerja, posisi, idkecamatan, title, seo_url, date_format(log, "%d %M %Y") as tgl, 
                                                date_format(date_start, "%d %M %Y") as tgl_mulai, date_format(date_end, "%d %M %Y") as tgl_akhir,
                                                log from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day) and idperusahaan = "'.$this->session->userdata('id').'") a
                                                left join ngi_job b on a.posisi = b.id
                                                left join ngi_kecamatan c on c.subdistrict_id = a.idkecamatan
												left join ngi_salary d on a.upah_kerja = d.id
                                                order by a.log desc');

                        if($q->num_rows() > 0) {

                            $data = $q->result();

                            foreach ($data as $item) {

                                $jmlpelamar = $this->db->query('select id from ngi_jobapplied where id_job = '.$item->id)->num_rows();
                                $jmlkomen = $this->db->query('select id from ngi_jobcomment where id_job = '.$item->id.' and not (id_user = "'.$this->session->userdata('id').'" and tipe_user = "perusahaan") and is_aktif = 1')->num_rows();
                    ?>

                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_cont_wrapper">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3><?=$item->title?> &nbsp;<span class="label label-danger"><?=$item->tgl_mulai?> ~ <?=$item->tgl_akhir?></span></h3></div>
                                <div style="padding-top: 25px">
                                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-user fa-icon"></i> &nbsp;<?=$item->nama_job?></div>
                                        <div><i class="fa fa-map-marker fa-icon"></i> &nbsp;<?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-money fa-icon"></i>&nbsp;Rp. <?=($item->upah)?></div>
                                        <div><i class="fa fa-calendar fa-icon"></i> &nbsp;<?=$item->tgl?></div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                        <a class="btn btn-warning edit" id="<?=$item->id?>"><i class="fa fa-pencil"></i> &nbsp;Edit</a>
                                        <span class="dropdown">
                                          <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-times"></i> Hapus
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a class="delete" id="<?=$item->id?>">Hapus Lowongan</a></li>
                                            <li><a class="closes" id="<?=$item->id?>">Tutup Lowongan</a></li>
                                          </ul>
                                        </span>
                                        <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Informasi
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="<?=base_url()?>perusahaan/jobs/<?=$item->seo_url?>">Detail <span class="badge btn-danger"><?=$jmlkomen?> Komentar</span></a></a></li>
                                            <li><a href="<?=base_url()?>perusahaan/pelamar/<?=$item->seo_url?>">Daftar Pelamar <span class="badge btn-danger"><?=$jmlpelamar?></span></a></li>
                                            
                                          </ul>
                                        </span>
                                    </div>
                                    <div class="hidden-lg hidden-md col-sm-12 col-xs-12 mobile-btn">
                                        <a class="btn btn-warning edit" id="<?=$item->id?>"><i class="fa fa-pencil"></i> &nbsp;Edit</a>
                                        <span class="dropdown">
                                          <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-times"></i> Hapus
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a class="delete" id="<?=$item->id?>">Hapus Lowongan</a></li>
                                            <li><a class="closes" id="<?=$item->id?>">Tutup Lowongan</a></li>
                                          </ul>
                                        </span>
                                         <span class="dropdown">
                                          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-eye"></i> Info
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li><a href="<?=base_url()?>perusahaan/jobs/<?=$item->seo_url?>">Detail <span class="badge btn-danger"><?=$jmlkomen?> Komentar</span></a></a></li>
                                            <li><a href="<?=base_url()?>perusahaan/pelamar/<?=$item->seo_url?>">Daftar Pelamar <span class="badge btn-danger"><?=$jmlpelamar?></span></a></li>
                                          </ul>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>

                    <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="more">
                        <div class="jp_adp_choose_resume_bottom_btn_post jp_adp_choose_resume_bottom_btn_post2">
                            <ul>
                                <li><a href="#" data="5" id="view">View More</a></li>
                            </ul>
                        </div>
                    </div> -->

                    <?php }else{?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <h3 style="text-align: center; color: white;">Tidak ada lowongan pekerjaan aktif</h3>
                        </div>
                    </div>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click','#view',function(){
            $.ajax({
                type: 'post',
                url: '<?=base_url()?>perusahaan/page/viewmore',
                data: {
                    '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                    'more' : $(this).attr('data')
                },
                success:function(result){
                    $('#tempat').html(result);
                }
            })
        }).on('click', '.closes', function(){
            if(confirm('Anda yakin ingin menutup lowongan ini?')){
                $.ajax({
                    type: 'post',
                    url: '<?=base_url()?>perusahaan/page/tutuplowongan',
                    data: {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'id' : $(this).prop('id')
                    },
                    success:function(result){
                        alert(result);
                        location.reload();
                    }
                })
            }
        }).on('click', '.delete', function(){
              if(confirm('Anda yakin ingin menghapus lowongan ini?')){
                $.ajax({
                    type: 'post',
                    url: '<?=base_url()?>perusahaan/page/hapuslowongan',
                    data: {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'id' : $(this).prop('id')
                    },
                    success:function(result){
                        alert(result);
                        location.reload();
                    }
                })
            }
        }).on('click', '.edit', function(){
            post('<?=base_url()?>perusahaan/buatlowongan', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', edit : $(this).prop('id')}, "post");
        });

        function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }
    });
</script>
