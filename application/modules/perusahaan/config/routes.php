<?php 
$route['(perusahaan)/lowongan']	= "page/load/lowongan";
$route['(perusahaan)/buatlowongan']	= "page/load/buatlowongan";
$route['(perusahaan)/lowonganselesai']	= "page/load/lowonganselesai";
$route['(perusahaan)/informasi']	= "page/load/informasi";
$route['(perusahaan)/pengaturan']	= "page/load/pengaturan";
$route['(perusahaan)/login']	= "page/load/login";
$route['(perusahaan)/profile']	= "page/load/profile";
$route['(perusahaan)/pelamar/(:any)']	= "page/load/pelamar/$1";
$route['(perusahaan)/jobs/(:any)']	= "page/load/jobs/$1";
//$route['(perusahaan)/excelpencaker'] = "page/load/excelpencaker";