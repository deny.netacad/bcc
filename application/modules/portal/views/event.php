<style>
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
}
</style>
<script>
 $(".share-popup").click(function(){
    var window_size = "width=585,height=511";
    var url = this.href;
    var domain = url.split("/")[2];
    switch(domain) {
        case "www.facebook.com":
            window_size = "width=585,height=368";
            break;
        case "www.twitter.com":
            window_size = "width=585,height=261";
            break;
        case "plus.google.com":
            window_size = "width=517,height=511";
            break;
    }
    window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,' + window_size);
    return false;
});


</script>
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5d8b04c57b26a80012429756&product=inline-share-buttons" async="async"></script>

<div class="jp_tittle_main_wrapper">
  <div class="jp_tittle_img_overlay"></div>
  <div class="container">
      <div class="row">
        <?php
           $rs = $this->db->query("select a.*,c.nama,date_format(a.agenda_date,'%b %d, %Y') as tmstamp_ from section_agenda a
           left join mast_user c on a.iduser=c.iduser
           where a.agenda_seo='".$this->uri->segment(4)."'");
           $item = $rs->row();
           if($rs->num_rows()!=0){
              if($item->agenda_thumb!=""){
                $xpath = new DOMXPath(@DOMDocument::loadHTML($item->agenda_thumb));
                $src = $xpath->evaluate("string(//img/@src)");
                $src=str_replace('.thumbs/','',$src);
             }
         ?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="jp_tittle_heading_wrapper" style="padding-bottom: 250px;">
              <div class="jp_tittle_heading">
                <h2><?php echo $item->agenda_title; ?> </h2>
              </div>
            </div>
          </div>
        <?php }?>
      </div>
  </div>
</div>

<div class="jp_blog_cate_main_wrapper" style="padding-top: 0px;">
  <div class="row">
    <?php
       $rs = $this->db->query("select a.*,c.nama,date_format(a.agenda_date,'%b %d, %Y') as tmstamp_ from section_agenda a
       left join mast_user c on a.iduser=c.iduser
       where a.agenda_seo='".$this->uri->segment(4)."'");
       $item = $rs->row();
       if($rs->num_rows()!=0){
          if($item->agenda_thumb!=""){
            $xpath = new DOMXPath(@DOMDocument::loadHTML($item->agenda_thumb));
            $src = $xpath->evaluate("string(//img/@src)");
            $src=str_replace('.thumbs/','',$src);
         }
     ?>
        <?php if($item->agenda_thumb!=""){ ?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
              <img style="border-radius: 50px; margin-top: -200px; width: 100%;" src="<?=$src?>" class="img img-responsive" alt="<?=$item->agenda_title?>" />
            </div>
          </div>
        <?php }?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="container">
            <div class="sharethis-inline-share-buttons" style="padding: 20px 20px;"></div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding: 10px; background: #184981; border-radius: 75px 75px 0px 0px;">
          <div class="container">
            <div class="row" style="padding: 40px 0px;">
              <div class="col-lg-12">
                <p style="color: white;" class="pull-left">
                  <i class="fa fa-calendar"></i> &nbsp;&nbsp;<?=$item->tmstamp_?>
                  &nbsp; &nbsp; &nbsp;
                  <i class="fa fa-user"></i> &nbsp;&nbsp;<?=$item->nama?>
                </p>
              </div>
              <div class="col-lg-12">
                <p style="color: white; font-size: 2.2em; line-height: 1.3em; margin: 30px 0px; font-weight: bold;">
                  <?=$item->agenda_title?>
                </p>
                <style media="screen">
                  #content p {
                    color: white !important;
                  }
                  #content span {
                    color: white !important;
                  }
                  #content a {
                    color: #ADD8E6 !important;
                  }
                </style>
                <div id="content">
                  <?=$item->agenda_content?>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p style="font-size: 20px; color: white; margin-top: 50px; font-weight: bold;">Event Lainnya</p>
                <div class="row">
                  <?php
                   $rs = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit 2")->result();
                  ?>
                  <?php foreach ($rs as $key => $value): ?>
                    <?php
                      $xpath = new DOMXPath(@DOMDocument::loadHTML($value->agenda_thumb));
                      $src = $xpath->evaluate("string(//img/@src)");
                      $src=str_replace('.thumbs','',$src);
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top: 20px;">
                      <div style="background: #FFFFFF; box-shadow: 0px 0px 10px 4px rgba(0, 0, 0, 0.2); border-radius: 15px; padding-bottom: 10px;">
                        <img style="border-radius: 15px 15px 0px 0px; width:100%; height: 350px;" class="img img-responsive" src="<?=$src?>" class="img-responsive" alt="<?=$value->agenda_title?>" />
                        <div style="padding: 10px 15px; height: 150px; position: relative;">
                          <p style="color: #003672;">
                            <i class="fa fa-calendar"></i> &nbsp; <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($value->tmstamp)); ?>
                          </p>
                          <p style="font-size: 1.5em; font-weight: bold; margin-top: 15px; color: #003672; font-weight: 600; font-size: 24px; line-height: 28px;"><?=$value->agenda_title?></p>
                        </div>
                        <a href="<?=base_url()?>portal/page/event/<?=$value->agenda_seo?>" style="position: absolute; bottom: 0px; left:50%; transform: translate(-50%, -50%); background: #00AA58; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 10px; color: white; padding: 5px 20px;">Lihat Event</a>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="jp_blog_single_form_main_wrapper">
                  <div class="jp_blog_single_form_heading"></div>
                  <style>
                    #HCB_comment_box h3 {
                      color: white;
                    }
                    #hcb_form_name, #hcb_form_content {
                      background: #FFFDFD !important;
                      border-radius: 10px !important;
                      padding: 10px !important;
                    }
                    #hcb_submit {
                      background: #00AA58 !important;
                      border-radius: 10px !important;
                    }
                    #hcb_file_label a {
                      background: #013076 !important;
                      border-radius: 10px !important;
                      color: white !important;
                    }
                    .home-desc {
                      display: none;
                    }
                    #comments_list p, b, span {
                      color: white !important;
                    }
                    .admin-link {
                      display: none;
                    }
                    #hcb_msg {
                      color: white !important;
                    }
                  </style>
                  <div id="HCB_comment_box"><a href="http://www.htmlcommentbox.com">Widget</a> is loading comments...</div>
                  <link rel="stylesheet" type="text/css" href="https://www.htmlcommentbox.com/static/skins/bootstrap/twitter-bootstrap.css?v=0" />
                  <script type="text/javascript" id="hcb"> /*<!--*/ if(!window.hcb_user){hcb_user={};} (function(){var s=document.createElement("script"), l=hcb_user.PAGE || (""+window.location).replace(/'/g,"%27"), h="https://www.htmlcommentbox.com";s.setAttribute("type","text/javascript");s.setAttribute("src", h+"/jread?page="+encodeURIComponent(l).replace("+","%2B")+"&mod=%241%24wq1rdBcg%24aYVVAe3znKaMuqx%2Fgqppt."+"&opts=16862&num=10&ts=1552293477585");if (typeof s!="undefined") document.getElementsByTagName("head")[0].appendChild(s);})(); /*-->*/ </script>
                  <!-- end www.htmlcommentbox.com -->
                </div>
              </div>
            </div>
          </div>
        </div>
    <?php }?>
  </div>
</div>
<script src="<?=base_url()?>template/HTML/job_dark/js/jquery.magnific-popup.js"></script>
