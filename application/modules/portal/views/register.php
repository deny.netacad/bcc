
<script>
$(document).ready(function(){

		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			//format:'dd-mm-yyyy',
			minViewMode:0,
			viewMode:1
		}).on('changeDate',function(){
			$('.dt').datepicker('hide');
			loadDataKomoditas();
		});

	});


</script>

<style type="text/css">
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
}
    .login_wrapper button.btn {
        color: #fff;
        width: 100%;
        height: 50px;
        padding: 6px 25px;
        line-height: 36px;
        margin-bottom: 20px;
        text-align: left;
        border-radius: 8px;
        background-color: #337AB8;
        font-size: 16px;
        border: 1px solid #f9f9f9;
        text-align: center;
        text-transform: uppercase;
    }

    .cek {
        border: 1px solid #f9f9f9;
        background: #f9f9f9;
        color: #6a7c98;
        font-size: 17px;
		vertical-alling: middle;
    }
    .cek2 {
        border: 1px solid #f9f9f9;
        background: #f9f9f9;
        color: #6a7c98;
        font-size: 17px;
        vertical-alling: left;
    }

    .register_left_form input[type="text"], .register_left_form input[type="email"],  .register_left_form input[type="no_ktp"], .register_left_form input[type="password"], .register_left_form input[type="tel"], .register_left_form input[type="number"], .register_left_form input[type="url"], .register_left_form select, .register_left_form textarea, .register_left_form input {
        text-transform: none;
    }
    .login_wrapper button.btn {
    color: #fff;
    width: 100%;
    height: 50px;
    padding: 6px 25px;
    line-height: 36px;
    margin-bottom: 20px;
    text-align: left;
    border-radius: 8px;
    background-color: #184981;
    font-size: 16px;
    border: 1px solid #f9f9f9;
    text-align: center;
    text-transform: uppercase;
}
.register_tab_wrapper .register-tabs>li.active::after {
    content: "";
    position: absolute;
    left: 50%;
    bottom: -5px;
    margin-left: -10px;
    border-top: 5px solid #184981;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
}
.register_tab_wrapper .register-tabs>li.active>a, .register_tab_wrapper .register-tabs>li.active>a:focus, .register_tab_wrapper .register-tabs>li.active>a:hover {
    color: #fff;
    background-color: #184981;
    border: 0;
}


.register_left_form input[type="text"] .has-error,
.register_left_form input[type="email"] .has-error,
.register_left_form input[type="no_ktp"] .has-error,
.register_left_form input[type="password"] .has-error,
.register_left_form input[type="tel"] .has-error,
.register_left_form input[type="number"] .has-error,
.register_left_form input[type="url"] .has-error,
.register_left_form select .has-error,
.register_left_form textarea .has-error,
.register_left_form input .has-error{
    border:1px solid red;
}



</style>
 <div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Registrasi</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp register wrapper start -->
    <div class="register_section">
        <!-- register_form_wrapper -->
        <div class="register_tab_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div role="tabpanel">

                            <!-- Nav tabs -->
                            <ul id="tabOne" class="nav register-tabs">
                                <li class="active"><a href="#contentOne-1" data-toggle="tab">Pelamar<br> <span>Saya sedang mencari pekerjaan</span></a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active register_left_form" id="contentOne-1">

                                    <div class="jp_regiter_top_heading">
                                        <p><b>AKUN LOGIN PENCARI KERJA</b></p>
                                    </div>
									<form method="post" data-remote="true" action="<?=base_url()?>portal/page/regPendaftar" accept-charset="UTF-8" autocomplete="off">
                                    <input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
                                    <div class="row">
                                        <!--Form Group-->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <div class="input-group">
                                                <input type="text" name="username" id="username" placeholder="Username*" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka tanpa spasi, 3 - 10 karakter">
                                                <span class="input-group-addon cek" id="username_check"></span>
                                            </div>
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="password" name="password" id="password" placeholder=" password*">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
											<div class="input-group">
                                                <input type="password" name="password_ori" id="password_ori" placeholder="Konfirmasi password*">
                                                <span class="input-group-addon cek" id="password_check"></span>
                                            </div>
                                        </div>

                                    </div>
									<div class="jp_regiter_top_heading">
                                        <p><b>BIODATA</b></p>
                                    </div>

                                    <div class="row">
                                        <!--Form Group-->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="nama" id="nama" placeholder="Nama Lengkap*" required="">
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                 <input type="text" name="no_ktp" id="no_ktp"  pattern="[0-9]{16,16}" title="No ktp harus berupa angka, 16 digit" placeholder="Nomor Induk Kependudukan*" required="" maxlength="16">
                                                <span class="input-group-addon cek" id="check_nik"></span>
                                            </div>
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">

                                            <input type="text" name="tmp_lahir" id="tmp_lahir" placeholder="Tempat Lahir*" required="">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-2 col-sm-2 col-xs-12">

                                            <input type="text" class="dt" name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir*" required="" >
                                        </div>
										<div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="text" name="no_telp" id="no_telp" placeholder="Telp*" required="">
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <input type="text" name="email" id="email" placeholder="Email*" required="">
                                                <span class="input-group-addon cek" id="email_check"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="alamat" id="alamat" placeholder="Alamat*" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
											<select name="gender" id="gender" required="">
												<option value="Laki-laki">Laki-laki</option>
												<option value="Perempuan">Perempuan</option>
											</select>

                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
											<select name="agama" id="agama" required="">
												<option value="-">Agama*</option>
												<option value="Budha">Budha</option>
												<option value="Hindu">Hindu</option>
												<option value="Islam">Islam</option>
												<option value="Kristen">Kristen</option>
												<option value="Kristen-Katolik">Kristen-Katolik</option>
												<option value="Kristen Protestan">Kristen Protestan</option>
												<option value="Lainnya">Lainnya</option>
											</select>

                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
											<select name="wn" id="wn" required="">
												<option value="-">Kewarganegaraan*</option>
												<option value="WNI">WNI</option>
												<option value="WNA">WNA</option>
											</select>

                                        </div>
										<div class="form-group col-md-12 col-sm-12 col-xs-12">
											<select name="pendidikan" id="pendidikan" required="">
												<option value="-">Pendidikan Terakhir*</option>
												<option value="SD">SD</option>
												<option value="SMP">SMP</option>
												<option value="SMA">SMA</option>
                                                <option value="SMK">SMK</option>
												<option value="D1">D1</option>
												<option value="D2">D2</option>
												<option value="D3">D3</option>
												<option value="D4">D4</option>
												<option value="S1">S1</option>
												<option value="S2">S2</option>
												<option value="S3">S3</option>
											</select>

                                        </div>
                                        <!-- <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" placeholder="Nama instansi pendidikan terakhir anda" name="ins_pend" id="ins_pend" required="">
                                        </div> -->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" placeholder="Jurusan atau program studi pendidikan terakhir anda" name="jurusan" id="jurusan" required="" value="<?=$biodata->jurusan?>">
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <select name="tahun_lulus" required="">
                                                <option value="">Tahun Lulus</option>
                                                <?php
                                                    $year = idate("Y");
                                                     for ($i = $year; $i >= 1980; $i--){
                                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
    										<select name="province_id" required="">
    											<option value="">Provinsi</option>
                                                <?php
                                                    $q = $this->db->query('select province, province_id from ngi_kecamatan group by province_id')->result();

                                                    foreach ($q as $i) {
                                                        echo '<option value="'.$i->province_id.'">'.$i->province.'</option>';
                                                    }

                                                ?>
    										</select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <select name="kab_id" required="">
                                                <option value="">Kabupaten / Kota</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <select name="kecamatan_id" required="">
                                                <option value="">Kecamatan / Kelurahan</option>
                                            </select>
                                        </div>
                                        <!--Form Group-->
                                     <div class="form-group col-md-12 col-sm-12 col-xs-12">
											<select name="sts" id="sts" required="">
												<option value="-">Status*</option>
												<option value="Lajang">Lajang</option>
												<option value="Nikah">Nikah</option>
												<option value="Cerai">Cerai</option>
											</select>

                                        </div>

                                    </div>


									 <div class="login_btn_wrapper register_btn_wrapper login_wrapper ">
                                        <button type="submit" class="btn btn-primary login_btn" id="submit_btn" disabled=""> Daftar </button>
                                    </div>

									</form>
                                    <div class="login_message">
                                        <p>Sudah Mempunyai Akun? <a href="<?=base_url()?>portal/login" style="color: #164980;"> Login Disini </a> </p>
                                    </div>
                                </div>



                            </div>
                            <p class="btm_txt_register_form">Jika berhasil, maka akan langsung diarahkan ke halaman login. Jika gagal, kembali ke halaman ini.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- <script src="<?=base_url()?>template/HTML/job_dark/js/custom_II.js"></script> -->
<!-- <script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script> -->
<!-- <script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script> -->
<script type="text/javascript">
    $(function(){
        $(document).on('change', 'select[name="province_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikab',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kab_id"]').html(result);
                }
            })
        }).on('change', 'select[name="kab_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikec',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kecamatan_id"]').html(result);
                }
            })
        }).on('input', '#password_ori', function(){
			$('#password_check').html('<i class="fa fa-spinner fa-spin"></i>');

            if($(this).val() == $('#password').val()){
				$('#password_check').html('<i class="fa fa-check"></i>');
                $('#submit_btn').attr('disabled',false);
            }else{
				$('#password_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        }).on('input', '#username', function(){
            $('#username_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();
            var regexp = /[A-Za-z0-9]{3,}$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkusername',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        username: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#username_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                            $('#username').removeClass("has-error");
                        }else{
                            $('#username_check').html('<i class="fa fa-times text-red"></i>');
                            $('#username').addClass("has-error");
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#username_check').html('<i class="fa fa-times"></i>');
                $('#regBtn').attr('disabled',true);
            }
        }).on('input', '#no_ktp', function(e){

           // var val = $(this).val();
//[0-9]{16,16}
 var value = $(this).val();
            var regexp = /[0-9]/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/ceknik',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        no_ktp: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#check_nik').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#check_nik').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#check_nik').html('<i class="fa fa-times"></i>');
                $('#regBtn').attr('disabled',true);
            }

        	// var val = $(this).val();

        	// if(val.length == '16'){
        	// 	 $.ajax({
         //            url: '<?=base_url()?>portal/page/ceknik',
         //            type: 'post',
         //            dataType: 'json',
         //            data: {
         //                <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
         //                no_ktp: value
         //            },
         //            success: function(result){
         //                if(result.available){
         //                    $('#check_nik').html('<i class="fa fa-check"></i>');
         //                    $('#submit_btn').attr('disabled',false);
         //                }else{
         //                    $('#check_nik').html('<i class="fa fa-times"></i>');
         //                    $('#submit_btn').attr('disabled',true);
         //                }
         //            }
         //        });
        	// }else{
        	// 	$('#submit_btn').attr('disabled',true);
        	// }
        }).on('input', '#email', function(){
            $('#email_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();
            var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkemail',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        email: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#email_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#email_check').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#email_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        });
    })
</script>
