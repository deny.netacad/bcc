<div class="container-fluid" style="background: #FFF; margin-top: 120px;">
  <div class="text-center">
    <div style="background: #D0E8FF; border-radius: 15px; width: 100%; height: 25vh;"></div>
    <img class="img-circle" src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_user.png')?>" alt="profile_img" style="width: 15%; object-fit: contain; border: 1px solid gray; margin-top: -7vw;" />
    <p style="font-weight: bold; font-size: 42px; line-height: 54px; letter-spacing: -0.01em; color: #3B3B3B;"> <?php echo $nama; ?> </p>
    <p style="font-size: 24px; line-height: 24px; letter-spacing: -0.01em; color: #3B3B3B;"> <?php echo $subdistrict_name; ?>, <?php echo $type." ".$city; ?></p>
    <p style="font-size: 24px; line-height: 24px; letter-spacing: -0.01em; color: #3B3B3B;"> <?php echo $province; ?></p>
    <br>
  </div>
</div>
