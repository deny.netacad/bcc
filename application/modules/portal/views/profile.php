	<?php
        $url = $this->uri->segment(3);
        $rs = $this->db->query('select a.*, date_format(a.tgl_lahir, "%d %M %Y") as tgl_lhr, b.province, b.city, b.type, b.subdistrict_name from (select * from user_pelamar a where a.profile_url = "'.$url.'") a
        							left join ngi_kecamatan b on a.kecamatan_id = b.subdistrict_id');
        $rs2 = $this->db->query('select a.*, date_format(a.tgl_lahir, "%d %M %Y") as tgl_lhr, b.province, b.city, b.type, b.subdistrict_name from (select * from user_pelamar a where a.iduser = "'.$this->session->userdata('id').'") a
        							left join ngi_kecamatan b on a.kecamatan_id = b.subdistrict_id');

        if($rs->num_rows() > 0 || $rs2->num_rows() > 0){
        	if($rs->num_rows() > 0){
        		$data = $rs->row();
        	}else{
        		$data = $rs2->row();
        	}
    ?>

	<div id="Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Hasil Generate Barcode <?=$data->nama?></h4>
  </div>

  <div class="modal-body">
  <form id="form_print" name="form_print" action="<?=base_url()?>portal/page/pdfkartupeserta/cetakkartupeserta" method="post" target="_blank" enctype="multipart/form-data">
	<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
			<div>

						<input type="hidden" name="iduser" id="iduser" value="<?=$data->iduser?>" size="60" style="min-width:250px"></br></br>

						</div>
		<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?=$data->iduser?>&choe=UTF-8" title="QRcode id Pelamar"  class="centerqr" style=" text-align:center; width: 45%; object-fit: contain; border: 2px solid gray;" /></br>

	 <input type="submit" class="btn btn-info" value="Print">
	 </form>
  </div>
  <!--
  <div class="modal-footer">
    <button type="button" class="btn btn-default" onclick="window.location.href='https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=<?=$data->iduser?>&choe=UTF-8'">Print</button>
  </div>
  !-->
</div>

  </div>
</div>
    <div class="jp_listing_single_main_wrapper" style="padding-bottom:0px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Profile
							</h2>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp profile Wrapper Start -->
	<div class="jp_cp_profile_main_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="jp_cp_left_side_wrapper">
						<div class="jp_cp_left_pro_wallpaper">
							<img src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_user.png')?>" alt="profile_img" style="width: 45%; object-fit: contain; border: 2px solid gray;" />
							<h2><?=$data->nama?></h2>

							<!-- <p>UI/UX Designer in Dewas</p>
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
							</ul> -->
						</div>
						<div class="jp_cp_rd_wrapper">
							<ul>
								<!-- <li><a href="#"><i class="fa fa-download"></i> &nbsp;Download Resume</a></li> -->
								<li><a href="tel:<?=$data->no_telp?>"><i class="fa fa-phone"></i> &nbsp;Telfon <?=$data->nama?></a></li>
                                <!-- <li><a href="https://api.whatsapp.com/send?phone=6281242341957&text=&source=&data=test"><i class="fa fa-phone"></i> &nbsp;Whatsap <?=$data->nama?></a></li> -->

								<li><a href="#Modal"><i class="fa fa-qrcode"></i> &nbsp;Generate QR <?=$data->nama?></a></li>
							</ul>
						</div>
					</div>
                    <!-- <div class="jp_add_resume_wrapper jp_job_location_wrapper jp_cp_left_ad_res">
                        <div class="jp_add_resume_img_overlay"></div>
                        <div class="jp_add_resume_cont">
                            <img src="images/content/resume_logo.png" alt="logo" />
                            <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;ADD RESUME</a></li>
                            </ul>
                        </div>
                    </div> -->
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:30px">
	                <div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="best">
							<div class="jp_cp_right_side_wrapper">
								<div class="jp_cp_right_side_inner_wrapper">
									<h2>detail personal</h2>
									<table style="width:100%">
		                                <tbody>
		                                    <tr>
		                                        <td class="td-w25">Nama lengkap</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->nama?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Tempat tgl lahir</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->tmp_lahir?>, <?=$data->tgl_lhr?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Alamat</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->alamat?> <?=$data->subdistrict_name?>, <?=$data->type?> <?=$data->city?>, <?=$data->province?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Jenis Kelamin</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->gender?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">No. telfon</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->no_telp?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Email</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->email?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Status</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->sts?></td>
		                                    </tr>
		                                    <tr>
		                                        <td class="td-w25">Kewarganegaraan</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->wn?></td>
		                                    </tr>
		                                     <tr>
		                                        <td class="td-w25">Pend. Terakhir</td>
		                                        <td class="td-w10">:</td>
		                                        <td class="td-w65"><?=$data->pendidikan?></td>
		                                    </tr>
		                                </tbody>
		                            </table>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
									<div class="jp_cp_accor_heading_wrapper">
										<h2>Tentang saya</h2>
										<p><?=$data->ttg_anda?></p>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
									<div class="accordion_wrapper abt_page_2_wrapper">
										<div class="panel-group" id="accordion_threeLeft">


											<!-- /.panel-default -->
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree" aria-expanded="false">
												RIWAYAT PENDIDIKAN
											  </a>
													</h4>
												</div>
												<div id="collapseTwentyLeftThree" class="panel-collapse collapse" aria-expanded="false" role="tablist">
													<div class="panel-body">
														<?php
                                                            if($data->education != ""){
                                                                $edu = explode(',', $data->education);
                                                                foreach ($edu as $s) {
                                                                    echo '<span class="tag label label-default">
                                                                          <span class="skill-name">'.$s.'</span>

                                                                        </span>';
                                                                }
                                                            }
                                                        ?>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree2" aria-expanded="false">
												KEAHLIAN
											  </a>
													</h4>
												</div>
												<div id="collapseTwentyLeftThree2" class="panel-collapse collapse" aria-expanded="false" role="tablist">
													<div class="panel-body">
														<?php
                                                            if($data->skill != ""){
                                                                $skill = explode(',', $data->skill);
                                                                foreach ($skill as $s) {
                                                                    echo '<span class="tag label label-default">
                                                                          <span class="skill-name">'.$s.'</span>

                                                                        </span>';
                                                                }
                                                            }
                                                        ?>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a class="collapsed" data-toggle="collapse" data-parent="#accordion_threeLeft" href="#collapseTwentyLeftThree3" aria-expanded="false">
												PENGALAMAN
											  </a>
													</h4>
												</div>
												<div id="collapseTwentyLeftThree3" class="panel-collapse collapse" aria-expanded="false" role="tablist">
													<div class="panel-body">
														<?=$data->exp?>
													</div>
												</div>
											</div>
											<!-- /.panel-default -->
										</div>
										<!--end of /.panel-group-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php } else { ?>

    <div class="error_page" style="padding-top: 200px !important; padding-bottom: 200px !important;">
      <div class="container">
          <div class="row">
           <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                  <div class="error_page_cntnt">
                      <h2>
                          <span>4</span>
                          <span>0</span>
                          <span>4</span>
                      </h2>
                      <h3>Sorry, This Page Isn't available :(</h3>
                      <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. A Aenean sollicitudin, lorem quis bibend Aenean sollicitudin, lorem . <a href="index.html">Home Page</a></p>
                     <!--  <div class="error_page_mail_wrapper">
                          <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a><input type="text" placeholder="Email Address *">
                      </div> -->
                  </div>
              </div>
          </div>
      </div>
  </div>

    <?php } ?>

    <script type="text/javascript">
    	$(function(){
    		$(document).on('click','.detail', function(){
    			var seo = $(this).prop('id');

    			window.location = '<?=base_url()?>portal/jobs/'+seo;
    		})
			$('a[href$="#Modal"]').on( "click", function() {
			$('#Modal').modal('show');
			});



    	})

		function myFunction() {
		window.print();
		}


    </script>
 <div id="placeholder-div"></div>
  <script type="text/javascript">
    window.___gcfg = {
      lang: 'id-ID'
    };
    (function() {
      var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
      po.src = 'https://apis.google.com/js/platform.js?onload=renderButtons';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    function renderButtons(){
      gapi.hangout.render('placeholder-div', {
          'render': 'createhangout',
        });
    }
  </script>
