<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="<?=$def_ass?>bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$def_ass?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?=$def_ass?>css/ionicons.min.css">
<link rel="stylesheet" href="<?=$def_ass?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<?=$def_ass?>dist/css/AdminLTE.css">
<link rel="stylesheet" href="<?=$def_ass?>dist/css/skins/_all-skins.min.css">

<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
<link href="<?=$def_css?>datetimepicker.css" rel="stylesheet">
<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?=$def_css?>custom.css" rel="stylesheet">
<link href="<?=$def_css?>icomoon/style.css" rel="stylesheet">
<link href="<?=$def_js?>plugins/imgeffects/css/effects.min.css" rel="stylesheet">

<!-- jQuery 2.1.4 -->
<script src="<?=$def_ass?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?=$def_ass?>bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=$def_ass?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=$def_ass?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=$def_ass?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=$def_ass?>dist/js/demo.js"></script>

<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.min.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!--<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/modules/exporting.js"></script>-->
<script type="text/javascript" src="<?=$def_js?>plugins/mask/jquery.mask.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/jpodtable/jquery.jpodtable.js"></script>
<script src="<?=$def_js?>holder.js"></script>
<script src="<?=$def_js?>enterform.js"></script>
<script src="<?=$def_js?>plugins/isloading/jquery.isloading.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>

<?php

?>

<style media="screen">
  .text-small {
    font-size: 200% !important;
  }
  .text-medium {
    font-size: 300% !important;
  }
  .text-large {
    font-size: 800% !important;
  }
  .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: white;
    height: 75px;
    color: white;
    text-align: center;
  }
  .date-video {
    position: absolute;
    right: 0;
    font-weight: bold;
    padding: 10px 40px;
    font-size: 3em;
    background: #00AA58;
    border-radius: 0px 30px;
    color: white;
    z-index: 99;
  }
  .corner {
    background: url("<?php echo base_url(); ?>assets/img/corner_right_bottom.png") no-repeat right center;
    background-repeat: no-repeat;
    background-size: 300px 150px;
    background-position: right 0px bottom 0px;
    border-radius: 30px;
  }
</style>

<div class="wrapper" style="padding: 15px 15px; background-color: #013076;">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-8">
        <div class="row">
          <div class="pull-right date-video">
            <?php echo date('d-M-Y'); ?>
          </div>
          <div class="panel panel-info" style="background: #FFFFFF; border-radius: 30px;">
            <div class="panel-body">
              <?php
                $qVideo = $this->db->query("
                  SELECT * FROM r_pengaturan
                  WHERE parameter = 'video_kiosk'
                ")->row();
              ?>
              <video width="100%" height="55%" autoplay loop muted style="border-radius: 30px; object-fit: cover;">
                <source src="<?php echo base_url('/assets/video/'.$qVideo->value) ?>">
              </video>
            </div>
          </div>
          <div class="panel panel-info" style="background: #FFFFFF; border-radius: 30px;">
            <div class="panel-body corner">
              <img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:210px; margin:30px;" title="Bogor Career Center" >
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-info" style="background: transparent; border: transparent;">
          <div class="panel-body" style="padding-top: 0px;">
            <?php foreach ([1,2,3,4,5] as $value): ?>
              <div class="row" style="background: #FFFFFF; border-radius: 30px; margin-bottom: 10px; padding: 10px;">
                <div class="col-lg-3 text-center" style="background: #00AA58; border-radius: 30px; color: white; font-weight: bold;">
                  <div style="font-size: 1.5em; background: #098950; border-radius: 30px 30px 0px 0px; min-width: 100%;">
                    LOKET
                  </div>
                  <div style="font-size: 4.5em;">
                    <?php echo $value; ?>
                  </div>
                </div>
                <div class="col-lg-9 text-center">
                  <div style="font-size: 1.5em; background: #032456; border-radius: 30px 30px 0px 0px; min-width: 100%; color: white; ">
                    NOMOR ANTRIAN
                  </div>
                  <div id="containerLoket-<?php echo $value; ?>" style="font-size: 4.5em; background: #013076; border-radius: 0px 0px 30px 30px; color: white; font-weight: bold;">
                    <span id="loket-<?php echo $value; ?>">0000</span>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>

    <!-- <div class="row">
      <div class="col-lg-4">
        <div class="panel panel-info">
          <div class="panel-body">
            <div class="jumbotron bg-blue" style="margin: 0px">
              <p class="text-medium">Nomor Antrian</p>
              <h1 id="callNomor" class="text-large">0000</h1>
              <p id="callLoket" class="text-medium">Ke Loket X</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-body">
              <div class="jumbotron bg-yellow" style="margin: 0px">
                <p class="text-medium">Belum Dilayani</p>
                <h1 id="sisaAntrian" class="text-large">0000</h1>
                <p class="text-medium">Antrian</p>
              </div>
            </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-danger">
            <div class="panel-body">
              <div class="jumbotron bg-red" style="margin: 0px">
                <p class="text-medium">Kuota Layanan</p>
                <h1 id="kuotaHarian" class="text-large">0000</h1>
                <p class="text-medium">Tersisa</p>
              </div>
            </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-body">
              <div class="jumbotron bg-green" style="margin: 0px">
                <p class="text-small">Sedang Dilayani</p>
                <div id="sedangDilayani" class="row text-center">
                  <div class="col-lg-3">
                    <h1 class="text-medium">Loket X : 000</h1>
                  </div>
                  <div class="col-lg-3">
                    <h1 class="text-medium">Loket X : 000</h1>
                  </div>
                  <div class="col-lg-3">
                    <h1 class="text-medium">Loket X : 000</h1>
                  </div>
                  <div class="col-lg-3">
                    <h1 class="text-medium">Loket X : 000</h1>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div> -->

    <div class="footer">
      <div class="pull-left" style="font-weight: bold; padding: 15px; font-size: 2.5em; background: #00AA58; border-radius: 0px 50px 0px 0px; color: white;">
        <span id="digital-clock">00:00:00</span> WIB &nbsp;
      </div>
      <marquee class="pull-left" width="83%" direction="left" scrollamount="8" height="75px" style="padding-top: 7px; color: black; font-size: 3em; font-weight: bold;">
        <?php
          $qRunText = $this->db->query("
            SELECT * FROM r_pengaturan
            WHERE parameter = 'running_text'
          ")->row();
        ?>
        <?php echo $qRunText->value; ?>
      </marquee>
    </div>
  </div>
</div>

<div class="audio-library">
  <audio id="nomor">
    <source src="<?php echo base_url('/assets/voices/nomor.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="antrian">
    <source src="<?php echo base_url('/assets/voices/antrian.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="loket">
    <source src="<?php echo base_url('/assets/voices/loket.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="a">
    <source src="<?php echo base_url('/assets/voices/a.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="b">
    <source src="<?php echo base_url('/assets/voices/b.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="c">
    <source src="<?php echo base_url('/assets/voices/c.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="d">
    <source src="<?php echo base_url('/assets/voices/d.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="kosong">
    <source src="<?php echo base_url('/assets/voices/kosong.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="satu">
    <source src="<?php echo base_url('/assets/voices/satu.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="dua">
    <source src="<?php echo base_url('/assets/voices/dua.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="tiga">
    <source src="<?php echo base_url('/assets/voices/tiga.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="empat">
    <source src="<?php echo base_url('/assets/voices/empat.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="lima">
    <source src="<?php echo base_url('/assets/voices/lima.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="enam">
    <source src="<?php echo base_url('/assets/voices/enam.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="tujuh">
    <source src="<?php echo base_url('/assets/voices/tujuh.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="delapan">
    <source src="<?php echo base_url('/assets/voices/delapan.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="sembilan">
    <source src="<?php echo base_url('/assets/voices/sembilan.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="sepuluh">
    <source src="<?php echo base_url('/assets/voices/sepuluh.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="sebelas">
    <source src="<?php echo base_url('/assets/voices/sebelas.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="seratus">
    <source src="<?php echo base_url('/assets/voices/seratus.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="belas">
    <source src="<?php echo base_url('/assets/voices/belas.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="puluh">
    <source src="<?php echo base_url('/assets/voices/puluh.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="ratus">
    <source src="<?php echo base_url('/assets/voices/ratus.mp3'); ?>" type="audio/mpeg">
  </audio>

</div>

<script type="text/javascript">
  $( document ).ready(function() {
    setInterval(clockUpdate, 1000);
    setInterval(function(){
      cekPanggilan();
      // cekSisa();
      cekLayanan();
      // cekKuota();
    }, 2000);
  });

  function clockUpdate() {
    var date = new Date();

    function addZero(x) {
      if (x < 10) {
        return x = '0' + x;
      } else {
        return x;
      }
    }

    function twelveHour(x) {
      if (x > 12) {
        return x = x - 12;
      } else if (x == 0) {
        return x = 12;
      } else {
        return x;
      }
    }

    var h = addZero(date.getHours());
    var m = addZero(date.getMinutes());
    var s = addZero(date.getSeconds());

    $('#digital-clock').html(h + ':' + m + ':' + s)
  }

  function audioPlayer(string, index) {
    var strings = string.split(" ");
    var audio = document.getElementById(strings[index]);
    audio.play();
    audio.onended = function() {
      if(index < strings.length-1){
        audioPlayer(string, index+1);
      }
    };
  }

  function cekSisa() {
    $.get("<?php echo base_url('loket/ajaxfile/hitungTunggu'); ?>", function( data ) {
      $('#sisaAntrian').html(data.jumlah);
    });
  }

  function cekKuota() {
    $.get("<?php echo base_url('loket/ajaxfile/kuotaHarian'); ?>", function( data ) {
      $('#kuotaHarian').html(data);
    });
  }

  function cekLayanan() {
    $.get("<?php echo base_url('loket/ajaxfile/sedangDilayani'); ?>", function( data ) {
      var stringHtml = "";
      $.each( data, function( key, value ) {
        // stringHtml += '' +
        //   '<div class="col-lg-3">' +
        //     '<h1 class="text-medium">Loket '+ value.loket +' : '+ value.no_antrian +'</h1>' +
        //   '</div>';
        $('#loket-'+value.loket).html(value.no_antrian);
      });
      if(data.length == 0){
        for (var i = 1; i < 5; i++) {
          $('#loket-'+i).html("0000");
        }
      }
      // if(data.length > 0) $('#sedangDilayani').html(stringHtml);
      // else $('#sedangDilayani').html("");
    });
  }

  function cekPanggilan() {
    $.get("<?php echo base_url('loket/ajaxfile/cekPanggilan'); ?>", function( data ) {
      $.each( data, function( key, value ) {
        var kode  = value.no_antrian.substring(0,1).toLowerCase();
        var antrian = value.no_antrian.substring(1);
        var antrian_tb = digit3(antrian);
        var loket_tb = digit3(value.loket);
        var string_play = $.trim(("antrian " + kode + " " + antrian_tb + " loket "+ loket_tb).replace(/  +/g, ' '));

        setTimeout(function(){
          $('#callNomor').html(value.no_antrian);
          $('#callLoket').html("ke Loket " + value.loket);
          audioPlayer(string_play, 0);
          if(data.length == key + 1) cekPanggilan();
        }, key * string_play.split(" ").length * 1800);
      });
    });
  }
</script>
