<style>
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
    padding-bottom: 400px;
}
</style>
 <?php
    $perPage = 4;

    $row = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc")->num_rows();
    $maxrow = (int)(ceil($row / $perPage));
    $page = $this->uri->segment(4);
    $range = ($page - 1)*$perPage;

    if ($page == ''){
      header("Location: /portal/page/all_event/1");
      exit;
    }

    if ($page > $maxrow){
        header("Location: /portal/page/all_event/$maxrow");
        exit;
    }else if($page == $maxrow){
         $paging = ($range+1)." - ".($row);
    }else{
         $paging = ($range+1)." - ".($page*$perPage);
    }
 ?>
<div class="jp_tittle_main_wrapper">
  <div class="jp_tittle_img_overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="jp_tittle_heading_wrapper">
          <div class="jp_tittle_heading">
            <h2>Event Bogor Career Center</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="jp_blog_cate_main_wrapper" style="margin-top: -170px; background: #FFF;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <?php
            $rs = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit $range,$perPage");
            foreach($rs->result() as $item){
              $xpath = new DOMXPath(@DOMDocument::loadHTML($item->agenda_thumb));
              $src = $xpath->evaluate("string(//img/@src)");
              $src=str_replace('.thumbs','',$src);
          ?>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top: 30px;">
              <div style="min-height: 460px; background: #FFFFFF; box-shadow: 0px 0px 10px 4px rgba(0, 0, 0, 0.15); border-radius: 20px; padding-bottom: 10px;">
                <img style="width: 100%; height: 320px; border-radius: 20px 20px 0px 0px;" src="<?=$src?>" class="img-responsive" alt="<?=$item->agenda_title?>">
                <div style="padding: 20px;">
                  <div style="margin-bottom: 15px;">
                    <p class="pull-left"><i class="fa fa-calendar"></i> &nbsp; <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($item->tmstamp)); ?></p>
                    <p class="pull-right"><i class="fa fa-user"></i> &nbsp; <?=$item->nama?></p>
                  </div>
                  <br>
                  <h3><a style="color: #184981; font-weight: 900;" href="<?=base_url()?>portal/page/event/<?=$item->agenda_seo?>" name="<?=$item->agenda_id?>" class="counting"><?=$item->agenda_title?></a></h3>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
        <div class="pager_wrapper gc_blog_pagination">
          <?php if ($page <= $maxrow): ?>
            <ul class="pagination">
              <?php if ($page == 1): ?>
                <li class="hidden"><a href="#">Prev</a></li>
              <?php else: ?>
                <li><a href="<?=base_url()?>portal/page/all_event/<?=$page-1?>">Prev</a></li>
              <?php endif; ?>

              <?php if ($maxrow >= 3 && $page >= 3): ?>
                <?php foreach (range($page-1, $page+1) as $value): ?>
                  <li class="<?php echo ($page == $value)?"active":""; ?> <?php echo ($value > $maxrow)?"hidden":""; ?>"><a href="<?=base_url()?>portal/page/all_event/<?php echo $value ?>"><?php echo $value ?></a></li>
                <?php endforeach; ?>
              <?php else: ?>
                <?php foreach (range(1, 3) as $value): ?>
                  <li class="<?php echo ($page == $value)?"active":""; ?> <?php echo ($value > $maxrow)?"hidden":""; ?>"><a href="<?=base_url()?>portal/page/all_event/<?php echo $value ?>"><?php echo $value ?></a></li>
                <?php endforeach; ?>
              <?php endif; ?>

              <?php if ($page != $maxrow): ?>
                <li><a href="<?=base_url()?>portal/page/all_event/<?=$page+1?>">Next</a></li>
              <?php else: ?>
                <li class="hidden"><a href="<?=base_url()?>portal/page/all_event/<?=$page+1?>">Next</a></li>
              <?php endif; ?>
            </ul>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="row" style="margin-top: 20px;">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="jp_spotlight_main_wrapper" style="background: #EEF6FF; box-shadow: 0px 0px 15px 5px rgba(0, 0, 0, 0.1); border-radius: 15px;">
          <div class="spotlight_header_wrapper" style="background: transparent !important">
            <h4 style="font-weight: bold; font-size: 24px; line-height: 29px; color: #003672;">Rekomendasi Lowongan</h4>
          </div>
          <div class="jp_spotlight_slider_wrapper" style="background: #EEF6FF; border-radius: 15px; min-height:275px;">
            <div class="owl-carousel owl-theme">
              <?php if($this->session->userdata('is_login_pelamar')){
                $person = $this->db->query('select skill from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();
                $skill = explode(',', $person->skill);

                $where = "";
                foreach ($skill as $s) {
                  $where .= " or a.skill like '%".$s."%'";
                }

                $joblist = $this->db->query("
                  SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                  LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                  LEFT JOIN ngi_job c ON a.posisi = c.id
                  LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                  left join ngi_salary e on e.id = a.upah_kerja
                  WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ORDER BY RAND() LIMIT 5
                ");
              }
              else
              {
                $joblist = $this->db->query("
                  SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                  LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                  LEFT JOIN ngi_job c ON a.posisi = c.id
                  LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                  left join ngi_salary e on e.id = a.upah_kerja
                  WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5
                ");
              }

              if($joblist->num_rows() == 0 ){
                $joblist = $this->db->query("
                  SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                  LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                  LEFT JOIN ngi_job c ON a.posisi = c.id
                  LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                  left join ngi_salary e on e.id = a.upah_kerja
                  WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5
                ");
              }
              ?>
              <?php foreach ($joblist ->result() as $key => $item): ?>
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <img clas src="<?=base_url()?>template/HTML/job_light/images/content/spotlight.jpg" alt="spotlight_img" />
                    </div>
                    <div class="col-lg-8 col-sm-12">
                      <h4><?=$item->nama_job?></h4>
                      <p><?=$item->nama_perusahaan?></p>
                      <ul>
                        <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                        <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->type?> <?=$item->city?>, <?=$item->province?></li>
                      </ul>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px; margin-top: 10px;" class="pull-right" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Lihat Detil</a>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="jp_spotlight_main_wrapper" style="background: #EEF6FF; box-shadow: 0px 0px 15px 5px rgba(0, 0, 0, 0.1); border-radius: 15px;">
          <div class="spotlight_header_wrapper" style="background: transparent !important">
            <h4 style="font-weight: bold; font-size: 24px; line-height: 29px; color: #003672;">Informasi Pelatihan</h4>
          </div>
          <div class="jp_spotlight_slider_wrapper" style="background: #EEF6FF; border-radius: 15px; min-height:275px;">
            <div class="owl-carousel owl-theme">
            <?php
              $pelatihan = $this->db->query("
                SELECT a.*,DATE_FORMAT(a.tgl_mulai,'%d %M') AS tglmulai,DATE_FORMAT(a.tgl_ahir,'%d %M %Y') AS tglakhir FROM ref_pelatihan a
              ");

              foreach ($pelatihan->result() as $items) {
            ?>
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <img src="<?=base_url()?>template/HTML/job_dark/images/content/resume_bg_1.jpg" alt="spotlight_img" />
                    </div>
                    <div class="col-lg-8 col-sm-12">
                      <p style="font-family: Montserrat; font-style: normal; font-weight: 600; font-size: 20px; line-height: 29px; color: #676767;"><?=$items->judul?></p>
                      <ul>
                        <li style="font-family: Montserrat; font-style: normal; font-weight: normal; font-size: 16px; line-height: 22px; color: #5B5B5B;">
                          <i class="fa fa-map-marker"></i>&nbsp; <?=$items->tempat?>
                        </li>
                        <li style="font-family: Montserrat; font-style: normal; font-weight: normal; font-size: 16px; line-height: 22px; color: #5B5B5B;"><i class="fa fa-calendar-o"></i>&nbsp;  <?=$items->tglmulai?> s/d <?=$items->tglakhir?></li>
                      </ul>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px; margin-top: 10px;" class="pull-right" href="<?=base_url()?>../pelatihan/portal/dip_detail/<?=$item->id?>">Lihat Detil</a>
                    </div>

                  </div>
                </div>
            <?php  } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-sm-12" style="background: white; border-radius: 15px; margin-top: 50px; padding: 35px; box-shadow: 0px 0px 10px 4px rgba(0, 0, 0, 0.1);">
      <?php
        $rs = $this->db->query("select narasi from section_contact");
        $seo = $rs->row();
      ?>
      <div class="row">
        <div class="col-lg-3 col-sm-12">
          <img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:100%;" title="Bogor Career Center" >
        </div>
        <div class="col-lg-9 col-sm-12">
          <p style="font-weight: normal; font-size: 20px; line-height: 24px; text-align: center; letter-spacing: 0.15em; color: #676767;">
            <?= $seo->narasi ?>
          </p>
          <br>
          <center>
            <a href="<?=base_url()?>portal/register_perusahaan" style="padding: 15px; color: white; background: #00A85A; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px;"><i class="fa fa-plus-circle"></i> &nbsp;DAFTAR PERUSAHAAN</a>
          </center>
        </div>
      </div>
    </div>
  </div>
</div>
