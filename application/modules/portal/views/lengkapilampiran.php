<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />
<style type="text/css">
	.jp_adp_choose_resume_bottom_btn_post li button {
	    float: left;
	    width: 170px;
	    height: 50px;
	    line-height: 50px;
	    color: #ffffff;
	    border: 1px solid #f36969;
	    background: #f36969;
	    text-align: center;
	    text-transform: uppercase;
	    -webkit-border-radius: 10px;
	    -moz-border-radius: 10px;
	    border-radius: 10px;
	    -webkit-transition: all 0.5s;
	    -o-transition: all 0.5s;
	    -ms-transition: all 0.5s;
	    -moz-transition: all 0.5s;
	    transition: all 0.5s;
	}
	.gc_main_navigation {
            color: #f9f9f9!important;
            font-weight: bold;
        }

	#autocomplete {
        background-color: #181d28;
        border-radius: 3px;
        border: 1px solid #212b2d;
        max-height: 250px;
        overflow-y: auto;
    }


    #autocomplete ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
 
    #autocomplete ul li {
        padding: 10px 20px;
        border-bottom: 1px solid #212b2d;
    }

    #autocomplete ul li:focus,
    #autocomplete ul li:hover {
        background-color: #F0F0F0;
        cursor: pointer;
    }


    .tag {
      font-size: 14px;
      padding: .3em .4em .4em;
      margin: 0 .1em;
    }

    .tag a {
      color: #bbb;
      cursor: pointer;
      opacity: 0.6;
    }

    .tag a:hover {
      opacity: 1.0
    }

    .tag .remove {
      vertical-align: bottom;
      top: 0;
    }

    .tag a {
      margin: 0 0 0 .3em;
    }

    .tag a .glyphicon-white {
      color: #fff;
      margin-bottom: 2px;
    }

    .jp_adp_textarea_main_wrapper.no-topborder{
        border-top: none;
        padding-top: 0px;
    }
</style>
<script type="text/javascript">
	$(function(){
		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			//format:'dd-mm-yyyy',
			minViewMode:0,
			viewMode:1
		})


		$(document).on('change', '#province_id', function(){
			setKabupaten($(this).val());
		}).on('change', '#city_id', function(){
			setKecamatan($(this).val());
		}).on('submit', '#form', function(e){
			$(this).find('.submit').html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Loading');
			e.preventDefault();

			var formData = new FormData(this);

			$.ajax({
				url : $(this).attr('action'),
				data : formData,
				type: 'post',
				dataType: 'json',
				contentType: false,
                processData: false,
				success: function(result){
					alert(result.teks);
					post('<?=base_url()?>portal/lengkapilampiran/', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', idjob : '<?=$_POST['idjob']?>'}, "post");
				},
				error: function(e, r, o){
					console.log(e,r,o);
					post('<?=base_url()?>portal/lengkapilampiran/', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', idjob :  '<?=$_POST['idjob']?>'}, "post");
				}
			})
		})
	})

	function setKabupaten(idprov, idkab){
		$.ajax({
			url: '<?=base_url()?>perusahaan/page/carikab',
			type: 'post',
			data: {
				<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
				id : idprov,
				ids : idkab
			},
			success: function(result){
				$('#city_id').html(result);
			}
		})
	}

	function setKecamatan(idkab, idkec){
		$.ajax({
			url: '<?=base_url()?>perusahaan/page/carikec',
			type: 'post',
			data: {
				<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
				id: idkab,
				ids: idkec
			},
			success: function(result){
				$('#idkecamatan').html(result);
			}
		})
	}

	function setWilayah(idprov,idkab,idkec){
		setKabupaten(idprov,idkab);
		setKecamatan(idkab,idkec);
	}

	function post(path, params, method) {
        method = method || "post"; // Set method to post by default if not specified.

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }

</script>
	<?php if($_POST['idjob'] != '') {  

		$data = $this->db->query("select id, attachment from ngi_joblist where id = '".$_POST['idjob']."'")->row();
	?>
	<div class="jp_adp_main_section_wrapper" style="padding-top: 20px !important">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar">
					<div class="jp_job_des">
                        <h2>Lengkapi lampiran</h2>
                	</div>
				</div>
				<div style="padding-top: 100px">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
						if($data->attachment != ""){
						$array = explode(",", $data->attachment);

						foreach ($array as $att) {
							$rs = $this->db->query('select * from ngi_attachment where id = "'.$att.'"')->row();
							$rs2 = $this->db->query('select * from ngi_jobattachment where idjob = "'.$_POST['idjob'].'" and idpelamar = "'.$this->session->userdata('id').'" and idfile = "'.$att.'"')->row();
					?>
						<div class="row" style="padding-bottom: 10px">
							<form action="<?=base_url()?>portal/page/uploadlampiran" method="post" id="form" enctype="multipart/form-data">
								<div class="col-xs-4">
									<?=$rs->name?>
								</div>
								<div class="col-xs-4">
									<input type="file" name="files" required="">
									<input type="hidden" name="idfile" value="<?=$att?>">
									<input type="hidden" name="idjob" value="<?=$_POST['idjob']?>">
									<input type="hidden" placeholder="Judul Lowongan" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" required="">
								</div>
								<div class="col-xs-4">
									<button type="submit" class="btn btn-sm btn-primary submit" id="">Upload</button>
									<button class="btn btn-sm btn-default" onclick="preview('<?=$rs2->nama_file?>')" <?=(($rs2->nama_file == "")? 'disabled' : '')?>>Preview</button>
									<?=(($rs2->nama_file != "")? '&nbsp; <i class="fa fa-check fa-lg" style="color:green"></i>' : '')?>
								</div>
							</form>
						</div>

					<?php }}else{ ?>
					<div class="jp_recent_resume_box_wrapper">
                        <h3 style="text-align: center; color: white;">Tidak lampiran yang harus diunggah</h3>
                    </div>
                    <?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php } else { ?>

	 <div class="error_page" style="padding-top: 200px !important; padding-bottom: 200px !important;">
        <div class="container">
            <div class="row">
             <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                    <div class="error_page_cntnt">
                        <h2>
                            <span>4</span>
                            <span>0</span>
                            <span>4</span>
                        </h2>
                        <h3>Sorry, This Page Isn't available :(</h3>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. A Aenean sollicitudin, lorem quis bibend Aenean sollicitudin, lorem . <a href="index.html">Home Page</a></p>
                       <!--  <div class="error_page_mail_wrapper">
                            <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a><input type="text" placeholder="Email Address *">
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

	<?php } ?>

	<script type="text/javascript">
		function preview(filename){
			window.open('<?=base_url()?>assets/img/lampiran/'+filename,'_blank');
		}
	</script>