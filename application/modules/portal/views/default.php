<style type="text/css">
.company_img {
    width: 100px !important;
    height: 95px;
    object-fit: contain;
}
.perusahaan-slider {
    float: left;
    width: 100%;
    background: #ffffff;
    border: 1px solid #e9e9e9;
    padding-left: 35px;
    padding-right: 35px;
    padding-top: 40px;
    padding-bottom: 40px;
}
.nav-tabs > li {
  background: #2B77CD !important;
  border: 0px !important;
  border-radius: 15px 15px 0px 0px !important;
  margin-right: 2px !important;
}
.nav-tabs > .active {
  background: #FFFFFF !important;
}
.nav-tabs > .active > .tab-control {
  color: #003672 !important;
}
.tab-control {
  font-weight: 600 !important;
  background: transparent !important;
  text-align: center !important;
  border: 0px !important;
  border-radius: 15px 15px 0px 0px !important;
  color: #FFFFFF !important;
}
</style>

<script src="<?=base_url()?>template/HTML/job_light/js/jquery_min.js"></script>
<div class="jp_first_sidebar_main_wrapper" style="background: #FFFFFF; padding-bottom: 40px;">
  <div class="container" style="margin-bottom: 20px;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 half-background" style="background: #EEF6FF; border-radius: 20px; margin-top: 40px;">
        <center>
          <i class="fa fa-search" style="font-size: 6em; color: #184981; margin: 20px 0px;"></i>
          <h3 style="color: #003672; font-weight: bold;">PERUSAHAAN TERBAIK DI KABUPATEN BOGOR</h3>
        </center>
        <div class="jp_hiring_slider_wrapper" style="margin-top: 10px;">
          <div class="owl-carousel owl-theme">
            <?php
              $q = $this->db->query('select a.profile_url, a.photo, a.alamat, a.nama_perusahaan, count(b.id) as jml  from ngi_perusahaan a
              left join (select * from ngi_joblist where is_aktif = 1
              and now() < date_add(date_end, interval 1 DAY)) b on a.`idperusahaan` = b.idperusahaan
              group by a.idperusahaan
              order by count(b.id) desc');
              $data = $q->result();
            ?>
            <?php foreach ($data as $key => $item): ?>
              <div class="item">
                <div class="jp_hiring_content_main_wrapper text-center" style="background: transparent; border: 0px; padding: 0px;">
                  <div class="jp_hiring_content_wrapper text-left" style="width:95%;">
                    <h4 style="font-weight: 500; font-size: 24px; color: #3D3C3C; margin-bottom: 10px;"><?= $item->nama_perusahaan ?></h4>
                    <div class="row">
                      <div class="col-lg-5 text-center">
                        <a href="<?php echo base_url('portal/company').'/'.$item->profile_url; ?>" target="_blank">
                          <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="img img-responsive" style="min-width:160px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); border-radius: 15px;" />
                        </a>
                      </div>
                      <div class="col-lg-7">
                        <p style="font-weight: 500; font-size: 20px;"><?= $item->alamat ?></p>
                        <a style="font-weight: 600; font-size: 20px; color: #003672;" class="link" href="#" onclick="$('#form-keyword').val('<?php echo $item->nama_perusahaan; ?>'); $('#form-cari').click()"><?=$item->jml?> Lowongan</a>
                      </div>
                      <div class="col-lg-12 text-right">
                        <a class="btn" style="width: 25%; color: white; background: #003672; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px;" href="<?php echo base_url('portal/company').'/'.$item->profile_url; ?>" onclick="$('#form-keyword').val('<?php echo $item->nama_perusahaan; ?>'); $('#form-cari').click()">
                          Lihat
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="jp_first_sidebar_main_wrapper" style="background: #003672; margin-top: 0px; padding-bottom: 20px;">
  <div class="container" style="margin-bottom: 20px; padding: 10px 0px;">
    <div class="row">
      <div class="col-lg-12 col-sm-12">
        <div class="cc_featured_product_main_wrapper" style="padding-top: 30px">
          <div class="row">
            <div class="col-lg-3 col-sm-12 pull-right">
              <div style="background: #FFFFFF; border-radius: 0px 0px 30px 30px; margin-top: -40px; padding: 10px;" class="text-center">
                <img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:150px;height:80px;" title="Bogor Career Center" >
              </div>
            </div>
            <div class="col-lg-9 col-sm-12">
              <div class="jp_hiring_heading_wrapper jp_job_post_heading_wrapper">
                <h2 style="font-size: 24px; color: #FFFFFF; font-weight: bold; margin-top: 30px;">Lowongan Kerja di Kabupaten Bogor</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-9 col-sm-12">
          <ul class="nav nav-tabs" role="tablist" style="border: 0px; margin-top: 40px;">
            <li role="presentation" class="active">
              <a class="tab-control" href="#best" aria-controls="best" role="tab" data-toggle="tab">Semuanya</a>
            </li>
            <li role="presentation">
              <a class="tab-control" href="#fulltimes" aria-controls="fulltimes" role="tab" data-toggle="tab">Full Time </a>
            </li>
            <li role="presentation">
              <a class="tab-control" href="#trend" aria-controls="trand" role="tab" data-toggle="tab">Part Time</a>
            </li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="best">
              <div class="ss_featured_products" style="padding-bottom: 0px; padding-top: 0px; margin-top: 0px; background: #FFFFFF; border-radius: 0px 15px 15px 15px;">
                <div class="">
                  <div class="item" data-hash="zero">
                    <?php
                      $joblist = $this->db->query("
                        SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a
                        LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                        LEFT JOIN ngi_job c ON a.posisi = c.id
                        LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                        LEFT JOIN ngi_salary e on e.id = a.upah_kerja
                        WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) ORDER BY log desc LIMIT 5
                      ")->result();
                    ?>
                    <?php if (count($joblist) > 0): ?>
                      <?php foreach ($joblist as $key => $item): ?>
                        <?php $keyword = explode(',', $item->keyword); ?>
                        <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2" style="margin-top: 0px;">
                          <div class="jp_job_post_main_wrapper" style="margin-top: 0px; background: transparent !important; border: 0px !important;">
                            <div class="row">
                              <div class="col-lg-3 col-sm-12">
                                <center>
                                  <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="img img-responsive" style="background: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); border-radius: 15px; min-width: 100px;"/>
                                </center>
                              </div>
                              <div class="col-lg-9 col-sm-12">
                                <a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">
                                  <h4 style="font-weight: 600; font-size: 24px; color: #3D3C3C; margin-top: 15px;"><?=$item->title?></h4>
                                </a>
                                <p style="font-weight: 500; font-size: 18px; color: #2B77CD;">
                                  <?=$item->nama_perusahaan?>
                                  <a style="background: #FFFFFF; border: 1px solid #2B77CD; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px;" class="pull-right"> <?= ucwords($item->tipe) ?></a>
                                </p>
                                <div class="row">
                                  <div class="col-lg-3 col-sm-12">
                                    <i class="fa fa-user" style="color: #2B77CD;"></i>&nbsp; <?=$item->nama_job?>
                                  </div>
                                  <div class="col-lg-8 col-sm-12">
                                    <i class="fa fa-money" style="color: #2B77CD;"></i>&nbsp; Rp. <?=($item->upah)?>
                                  </div>
                                </div>
                                <p style="font-size: 18px;">
                                  <i class="fa fa-map-marker" style="color: #2B77CD;"></i> &nbsp; <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?>
                                </p>
                                <p>
                                  <i class="fa fa-tags" style="color: #184981;"></i>&nbsp;Keywords :
                                  <?php foreach ($keyword as $key => $kw): ?>
                                    <a href="#" class="trending" id="<?=$kw?>"><?=$kw?></a>
                                    <?php if (($key+1) < count($keyword)): ?>,&nbsp;<?php endif; ?>
                                  <?php endforeach; ?>
                                  <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px;" class="pull-right" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Lihat Detil</a>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php if (($key+1) < count($joblist)): ?>
                          <hr style="border: 1px solid #C4C4C4 !important;">
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                          <h3 style="text-align: center; color: #184981;">Tidak ditemukan hasil</h3>
                        </div>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
                <a href="<?php echo base_url('portal/page/caripekerjaan'); ?>">
                  <button class="btn btn-block btn-primary" style="font-weight: bold; padding: 10px; margin: 0px; border-radius: 0px 0px 15px 15px;">LIHAT SEMUA</button>
                </a>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="trend">
              <div class="ss_featured_products" style="padding-bottom: 0px; padding-top: 0px; margin-top: 0px; background: #FFFFFF; border-radius: 0px 15px 15px 15px;">
                <div class="">
                  <div class="item" data-hash="zero">
                    <?php
                      $joblist = $this->db->query("
                        SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a
                        LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                        LEFT JOIN ngi_job c ON a.posisi = c.id
                        LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                        left join ngi_salary e on e.id = a.upah_kerja
                        where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) and a.tipe = 'part time' ORDER BY log desc LIMIT 5
                      ")->result();
                    ?>
                    <?php if (count($joblist) > 0): ?>
                      <?php foreach ($joblist as $key => $item): ?>
                        <?php $keyword = explode(',', $item->keyword); ?>
                        <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2" style="margin-top: 0px;">
                          <div class="jp_job_post_main_wrapper" style="margin-top: 0px; background: transparent !important; border: 0px !important;">
                            <div class="row">
                              <div class="col-lg-3 col-sm-12">
                                <center>
                                  <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="img img-responsive" style="background: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); border-radius: 15px; min-width: 100px;"/>
                                </center>
                              </div>
                              <div class="col-lg-9 col-sm-12">
                                <a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">
                                  <h4 style="font-weight: 600; font-size: 24px; color: #3D3C3C; margin-top: 15px;"><?=$item->title?></h4>
                                </a>
                                <p style="font-weight: 500; font-size: 18px; color: #2B77CD;">
                                  <?=$item->nama_perusahaan?>
                                  <a style="background: #FFFFFF; border: 1px solid #2B77CD; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px;" class="pull-right"> <?= ucwords($item->tipe) ?></a>
                                </p>
                                <div class="row">
                                  <div class="col-lg-3 col-sm-12">
                                    <i class="fa fa-user" style="color: #2B77CD;"></i>&nbsp; <?=$item->nama_job?>
                                  </div>
                                  <div class="col-lg-8 col-sm-12">
                                    <i class="fa fa-money" style="color: #2B77CD;"></i>&nbsp; Rp. <?=($item->upah)?>
                                  </div>
                                </div>
                                <p style="font-size: 18px;">
                                  <i class="fa fa-map-marker" style="color: #2B77CD;"></i> &nbsp; <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?>
                                </p>
                                <p>
                                  <i class="fa fa-tags" style="color: #184981;"></i>&nbsp;Keywords :
                                  <?php foreach ($keyword as $key => $kw): ?>
                                    <a href="#" class="trending" id="<?=$kw?>"><?=$kw?></a>
                                    <?php if (($key+1) < count($keyword)): ?>,&nbsp;<?php endif; ?>
                                  <?php endforeach; ?>
                                  <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px;" class="pull-right" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Lihat Detil</a>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php if (($key+1) < count($joblist)): ?>
                          <hr style="border: 1px solid #C4C4C4 !important;">
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                          <h3 style="text-align: center; color: #184981;">Tidak ditemukan hasil</h3>
                        </div>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
                <a href="<?php echo base_url('portal/page/caripekerjaan'); ?>">
                  <button class="btn btn-block btn-primary" style="font-weight: bold; padding: 10px; margin: 0px; border-radius: 0px 0px 15px 15px;">LIHAT SEMUA</button>
                </a>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="fulltimes">
              <div class="ss_featured_products" style="padding-bottom: 0px; padding-top: 0px; margin-top: 0px; background: #FFFFFF; border-radius: 0px 15px 15px 15px;">
                <div class="">
                  <div class="item" data-hash="zero">
                    <?php
                      $joblist = $this->db->query("
                        SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a
                        LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                        LEFT JOIN ngi_job c ON a.posisi = c.id
                        LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                        left join ngi_salary e on e.id = a.upah_kerja
                        where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) and a.tipe = 'full time' ORDER BY log desc LIMIT 5
                      ")->result();
                    ?>
                    <?php if (count($joblist) > 0): ?>
                      <?php foreach ($joblist as $key => $item): ?>
                        <?php $keyword = explode(',', $item->keyword); ?>
                        <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2" style="margin-top: 0px;">
                          <div class="jp_job_post_main_wrapper" style="margin-top: 0px; background: transparent !important; border: 0px !important;">
                            <div class="row">
                              <div class="col-lg-3 col-sm-12">
                                <center>
                                  <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="img img-responsive" style="background: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); border-radius: 15px; min-width: 100px;"/>
                                </center>
                              </div>
                              <div class="col-lg-9 col-sm-12">
                                <a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">
                                  <h4 style="font-weight: 600; font-size: 24px; color: #3D3C3C; margin-top: 15px;"><?=$item->title?></h4>
                                </a>
                                <p style="font-weight: 500; font-size: 18px; color: #2B77CD;">
                                  <?=$item->nama_perusahaan?>
                                  <a style="background: #FFFFFF; border: 1px solid #2B77CD; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px;" class="pull-right"> <?= ucwords($item->tipe) ?></a>
                                </p>
                                <div class="row">
                                  <div class="col-lg-3 col-sm-12">
                                    <i class="fa fa-user" style="color: #2B77CD;"></i>&nbsp; <?=$item->nama_job?>
                                  </div>
                                  <div class="col-lg-8 col-sm-12">
                                    <i class="fa fa-money" style="color: #2B77CD;"></i>&nbsp; Rp. <?=($item->upah)?>
                                  </div>
                                </div>
                                <p style="font-size: 18px;">
                                  <i class="fa fa-map-marker" style="color: #2B77CD;"></i> &nbsp; <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?>
                                </p>
                                <p>
                                  <i class="fa fa-tags" style="color: #184981;"></i>&nbsp;Keywords :
                                  <?php foreach ($keyword as $key => $kw): ?>
                                    <a href="#" class="trending" id="<?=$kw?>"><?=$kw?></a>
                                    <?php if (($key+1) < count($keyword)): ?>,&nbsp;<?php endif; ?>
                                  <?php endforeach; ?>
                                  <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px;" class="pull-right" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Lihat Detil</a>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php if (($key+1) < count($joblist)): ?>
                          <hr style="border: 1px solid #C4C4C4 !important;">
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                          <h3 style="text-align: center; color: #184981;">Tidak ditemukan hasil</h3>
                        </div>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
                <a href="<?php echo base_url('portal/page/caripekerjaan'); ?>">
                  <button class="btn btn-block btn-primary" style="font-weight: bold; padding: 10px; margin: 0px; border-radius: 0px 0px 15px 15px;">LIHAT SEMUA</button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-12 text-center">
          <?php
            $rs = $this->db->query("select narasi from section_contact");
            $seo = $rs->row();
          ?>
          <p style="font-weight: normal; font-size: 20px; line-height: 30px; text-align: center; letter-spacing: 0.15em; color: #FFFFFF; margin-top: 100px;">
            <?= $seo->narasi ?>
          </p>
          <br>
          <a href="<?=base_url()?>portal/register_perusahaan" style="padding: 15px; color: white; background: #00A85A; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px;"><i class="fa fa-plus-circle"></i> &nbsp;DAFTAR PERUSAHAAN</a>
        </div>
      </div>

    </div>
  </div>
</div>
<div class="jp_first_sidebar_main_wrapper" style="background: #ffffff; margin-top: 0px;">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="jp_spotlight_main_wrapper" style="background: #EEF6FF; box-shadow: 0px 0px 15px 5px rgba(0, 0, 0, 0.15); border-radius: 15px;">
          <div class="spotlight_header_wrapper" style="background: transparent !important">
            <h4 style="font-weight: bold; font-size: 24px; line-height: 29px; color: #003672;">Rekomendasi Lowongan</h4>
          </div>
          <div class="jp_spotlight_slider_wrapper" style="background: #EEF6FF; border-radius: 15px; padding-bottom: 100px; min-height:275px;">
            <?php if($this->session->userdata('is_login_pelamar')){
              $person = $this->db->query('select skill from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();
              $skill = explode(',', $person->skill);

              $where = "";
              foreach ($skill as $s) {
                $where .= " or a.skill like '%".$s."%'";
              }

              $joblist = $this->db->query("
                SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                LEFT JOIN ngi_job c ON a.posisi = c.id
                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                left join ngi_salary e on e.id = a.upah_kerja
                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ORDER BY RAND() LIMIT 5
              ");
            }
            else
            {
              $joblist = $this->db->query("
                SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                LEFT JOIN ngi_job c ON a.posisi = c.id
                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                left join ngi_salary e on e.id = a.upah_kerja
                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5
              ");
            }

            if($joblist->num_rows() == 0 ){
              $joblist = $this->db->query("
                SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                LEFT JOIN ngi_job c ON a.posisi = c.id
                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                left join ngi_salary e on e.id = a.upah_kerja
                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5
              ")->result();
            }
            ?>
            <?php if (count($joblist) <= 1): ?>
              <style media="screen">
                .owl-job .owl-nav {
                  display: none !important;
                }
              </style>
            <?php endif; ?>
            <div class="owl-carousel owl-theme" owl-job>
              <?php foreach ($joblist as $key => $item): ?>
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <img clas src="<?=base_url()?>template/HTML/job_light/images/content/spotlight.jpg" alt="spotlight_img" />
                    </div>
                    <div class="col-lg-8 col-sm-12">
                      <h4><?=$item->nama_job?></h4>
                      <p><?=$item->nama_perusahaan?></p>
                      <ul>
                        <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                        <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->type?> <?=$item->city?>, <?=$item->province?></li>
                      </ul>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px; margin-top: 10px;" class="pull-right" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Lihat Detil</a>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="jp_spotlight_main_wrapper" style="background: #EEF6FF; box-shadow: 0px 0px 15px 5px rgba(0, 0, 0, 0.15); border-radius: 15px;">
          <div class="spotlight_header_wrapper" style="background: transparent !important">
            <h4 style="font-weight: bold; font-size: 24px; line-height: 29px; color: #003672;">Informasi Pelatihan</h4>
          </div>
          <div class="jp_spotlight_slider_wrapper" style="background: #EEF6FF; border-radius: 15px; padding-bottom: 100px; min-height:275px;">
            <?php
              $pelatihan = $this->db->query("
                SELECT a.*,DATE_FORMAT(a.tgl_mulai,'%d %M') AS tglmulai,DATE_FORMAT(a.tgl_ahir,'%d %M %Y') AS tglakhir FROM ref_pelatihan a
              ")->result();
            ?>
            <?php if (count($pelatihan) <= 1): ?>
              <style media="screen">
                .owl-pelatihan .owl-nav {
                  display: none !important;
                }
              </style>
            <?php endif; ?>
            <div class="owl-carousel owl-theme owl-pelatihan">
            <?php
              foreach ($pelatihan as $items) {
            ?>
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <img src="<?=base_url()?>template/HTML/job_dark/images/content/resume_bg_1.jpg" alt="spotlight_img" />
                    </div>
                    <div class="col-lg-8 col-sm-12">
                      <p style="font-family: Montserrat; font-style: normal; font-weight: 600; font-size: 20px; line-height: 29px; color: #676767;"><?=$items->judul?></p>
                      <ul>
                        <li style="font-family: Montserrat; font-style: normal; font-weight: normal; font-size: 16px; line-height: 22px; color: #5B5B5B;">
                          <i class="fa fa-map-marker"></i>&nbsp; <?=$items->tempat?>
                        </li>
                        <li style="font-family: Montserrat; font-style: normal; font-weight: normal; font-size: 16px; line-height: 22px; color: #5B5B5B;"><i class="fa fa-calendar-o"></i>&nbsp;  <?=$items->tglmulai?> s/d <?=$items->tglakhir?></li>
                      </ul>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px; margin-top: 10px;" class="pull-right" href="<?=base_url()?>../pelatihan/portal/dip_detail/<?=$item->id?>">Lihat Detil</a>
                    </div>

                  </div>
                </div>
            <?php  } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
