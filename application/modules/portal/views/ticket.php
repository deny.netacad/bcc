<?php
  $ref = $this->db->query('
    SELECT *
    FROM h_antrian_online
    WHERE no_ref = '.$this->db->escape($_GET['no_ref']).'
  ')->row();

  $newAntrian = array(
    'dtm_register' => date('Y-m-d H:i:s'),
    'dtm_panggil' => date('Y-m-d H:i:s'),
    'dtm_selesai' => date('Y-m-d H:i:s'),
    'mast_user_id' => 0,
    'user_id' => $ref->user_id,
    'no_ref' => $ref->no_ref,
    'no_antrian' => $_GET['no'],
    'keterangan' => null,
    'loket' => null,
    'panggil' => 'belum',
  );

  $this->db->insert('h_antrian', $newAntrian);

  require_once(APPPATH.'libraries/tcpdf/tcpdf.php');

  $pdf = new TCPDF('P', 'mm', [80, 297], true, 'UTF-8', false);

  $pdf->setPrintHeader(false);
  $pdf->setPrintFooter(false);
  $pdf->SetMargins(5,5,5,5);
  $pdf->SetHeaderMargin(0);
  $pdf->SetFooterMargin(0);

  $pdf->AddPage();

  ob_start();
?>

<style>
  .tiny {
    font-size: 7pt;
  }
  .small {
    font-size: 10pt;
  }
  .medium {
    font-size: 12pt;
  }
  .huge {
    font-size: 42pt;
  }
</style>

<table border="0">
  <tr>
    <td align="center" colspan="3" style="border-bottom: 1px solid black;">
      <img src="<?php echo FCPATH.'assets/logo/logo_bcc_new.png'; ?>" style="width: 75px;">
    </td>
  </tr>
  <tr>
    <td align="left" colspan="2">
      <span class="small"><?php echo date('d-M-Y'); ?></span>
    </td>
    <td align="right">
      <span class="small"><?php echo date('H:i:s'); ?></span>&nbsp;&nbsp;
    </td>
  </tr>
  <tr>
    <td align="center" class="small" colspan="3"> <br><br> Hai <?php echo $_GET['name']; ?>, Nomor Antrian Anda:</td>
  </tr>
  <tr>
    <td align="center" class="huge" colspan="3"><?php echo $_GET['no']; ?></td>
  </tr>
  <tr>
    <td align="center" class="small" colspan="3">Sedang Menunggu <?php echo $_GET['wait']; ?> Antrian Lagi <br> </td>
  </tr>
  <tr>
    <td align="center"><img src="<?php echo FCPATH.'assets/img/logo-bogor.jpg'; ?>" style="width: 30px;"></td>
    <td align="left" class="medium" colspan="2">Dinas Tenaga Kerja Kabupaten Bogor</td>
  </tr>
  <tr>
    <td align="center" class="tiny" colspan="3">Jl. Bersih No.2, Tengah, Cibinong, Bogor, Jawa Barat 16914</td>
  </tr>
  <hr>
</table>

<?php
  $content = ob_get_contents();
  ob_end_clean();
  $pdf->writeHTML($content, true, false, true, false, '');
  $pdf->Output(FCPATH.'assets/ticket/example_001.pdf', 'I');
?>
