<style type="text/css">
  .jp_tittle_img_overlay {
     position: absolute;
     top: 0%;
     right: 0%;
     left: 0%;
     bottom: 0%;
     background: #184981;
     padding-bottom: 420px;
  }

  a[disabled] {
    pointer-events: none;
  }
  .image {
      width: 70px;
      height: 70px;
      object-fit: contain;
  }

  .jp_contact_inputs4_wrapper textarea {
      margin-top: 0px;
      padding-left: 50px;
  }

  .jp_contact_inputs4_wrapper i {
      top : 21px;
  }

  .jp_contact_form_btn_wrapper ul{
      margin-top: 10px;
  }

  .jp_contact_form_btn_wrapper ul li{
      margin-bottom: 20px;
      float: right;
  }
  .jp_tittle_heading h2 {
      font-size: 36px;
      color: #333;
      font-weight: bold;
  }

  .job-panel {
    text-align: center;
    padding: 15px 10px;
  }
  .job-panel-title {
    color: #2B77CD;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 25px;
    line-height: 32px;
    padding-bottom: 0px;
  }
  .job-panel-separator {
    margin: 0px;
    padding: 1px;
    margin-bottom: 5px;
    margin-left: 20%;
    margin-right: 20%;
    text-align: center;
    background: #013076;
  }
  .job-panel-value {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 600;
    font-size: 22px;
    line-height: 32px;
  }

  .job-item {
    margin-bottom: 50px;
  }
  .job-item h2 {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 600;
    font-size: 24px;
    line-height: 32px;
    color: #013076;
  }
  .job-item p {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 30px;
    color: #000000;
    text-transform: none !important;
    margin-top: 10px;
    margin-left: 20px;
  }

  .komen {
    border: none !important;
    background: #00AA58 !important;
    border-radius: 10px !important;
    padding: 5px 10px;
    height: auto !important;
    width: auto !important;
    cursor: pointer;
  }
  .komen:hover {
    background: #098950 !important;
  }

  .img-komentar {
    height: 100px;
    width: 100px;
    float: right;
    object-fit: cover;
    border-radius: 10px;
    box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25);
  }

  .komentar-control {
    font-size: 1.2em !important;
    padding: 25px 0px 10px 0px;
  }
  .komentar-control a {
    padding: 5px;
    color: #184981;
    margin-right: 10px;
    margin-left: 10px;
  }
  .komenarea {
    margin-top: 5px;
    margin-left: 10px;
    font-size: 1.2em !important;
    border-radius: 10px;
    padding: 10px 15px;
    background: #FFFDFD;
    box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
  }

  .reply {
    background: #FFFDFD;

  }
  .komentar {
    margin-top: 5px !important;
    margin-left: 10px;
    padding-left: 45px !important;
    padding-top: 10px !important;
    width: 99% !important;
    box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25) !important;
    border-radius: 15px;
    border: none !important;
    outline: none !important;
  }

  .login {
    border: none !important;
    background: #013076 !important;
    border-radius: 10px !important;
    padding: 5px 30px;
    height: auto !important;
    width: auto !important;
    cursor: pointer;
  }
  .login:hover {
    background: #184981 !important;
  }
</style>

    <?php
        $seo = $this->uri->segment(3);
        $rs = $this->db->query('select a.*, f.name as upah, date_format(a.date_start,"%d %M %Y") as tgl_awal, date_format(a.date_end,"%d %M %Y") as tgl_akhir, date_end, date_format(a.log,"%d %M %Y") as tgl, b.nama_job, c.photo, c.nama_perusahaan, c.profile_url,
                                d.subdistrict_name, d.type as type_adm, d.city, d.province, e.category from (select * from ngi_joblist where seo_url = "'.$seo.'" ) a
                                left join ngi_job b on a.posisi = b.id
                                left join ngi_perusahaan c on a.idperusahaan = c.idperusahaan
                                left join ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                                left join ngi_jobcategory e on a.idcategory = e.id
								left join ngi_salary f on a.upah_kerja = f.id');

        if($rs->num_rows() > 0){
            $data = $rs->row();
            $this->db->insert('ngi_jobtrend',array('id_joblist' => $data->id));
            if($this->session->userdata('is_login_pelamar')){
                $is_pelamar = $this->db->query('select id from ngi_jobapplied where id_job = "'.$data->id.'" and id_pelamar = "'.$this->session->userdata('id').'" and status is NULL')->num_rows();
            }

            $keyword = explode(',', $data->keyword);
    ?>
    <div class="jp_tittle_main_wrapper" style="padding: 80px 0px;">
      <div class="jp_tittle_img_overlay"></div>
      <div class="container" style="padding: 5px 20px;">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <img src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_company.jpg')?>" alt="company_img" class="img img-responsive" style="box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); border-radius: 15px;" />
          </div>
          <?php
            $gridQR = "col-lg-9 col-md-9";
            if ($this->session->userdata('is_login_pelamar')) {
              $gridQR = "col-lg-7 col-md-7";
            }
          ?>
          <div class="<?php echo $gridQR; ?> col-sm-12 col-xs-12">
            <p style="margin: 10px 0px; font-family: 'Poppins'; font-style: normal; font-weight: bold; font-size: 48px; line-height: 54px; text-transform: uppercase; color: #FFFFFF;"><?php echo $data->title ?></p>
            <a style="color: #FFF; font-size: 18px" href="<?=base_url()?>portal/company/<?=$data->profile_url?>"><?=$data->nama_perusahaan?> &#9679; <?php echo $data->subdistrict_name.", ".$data->type_adm." ".$data->city.", ".$data->province; ?></a>
            <p style="margin: 10px 0px; font-family: 'Poppins'; font-style: normal; font-size: 18px; color: #FFFFFF;">Diposting <?php echo $data->tgl; ?></p>
            <p style="margin-top: 15px;">
              <?php if ($is_pelamar == 1): ?>
                <a class=""  disabled="disabled" tabIndex="-1">Menunggu konfirmasi</a>
              <?php elseif ($data->is_aktif == 0 || strtotime($data->date_end."23:59:59") < time()): ?>
                <a class=""  disabled="disabled" tabIndex="-1"><i class="fa fa-info-circle"></i> &nbsp;Lowongan tutup</a>
              <?php else: ?>
                <a href="#" class="applynow" style="display: inline-block; background: #00AA58; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 10px; padding: 10px 20px; color: white;">Lamar Sekarang</a>
              <?php endif; ?>
            </p>
          </div>
          <?php if ($this->session->userdata('is_login_pelamar')): ?>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <img src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=<?=base_url()?>portal/jobs/<?=$seo?>&choe=UTF-8" title="QRcode Untuk discan Pelamar" class="centerqr" style="padding-top: 15px;" />
              <p style="font-size: 0.8em; margin-top: 10px; text-align: center; color: white !important;">Scan QR Code di atas melalui apikasi BCC untuk melamar pekerjaan ini</p>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: #FFFDFD; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25); border-radius: 20px; margin-top: -50px; padding: 40px 0px;">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row">
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-money"></i>&nbsp; Upah Kerja </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;">Rp. <?php echo $data->upah; ?> </p>
              </div>
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-users"></i>&nbsp; Dibutuhkan </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;"><?php echo $data->jml_pekerja; ?> Orang</p>
              </div>
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-th-large"></i>&nbsp; Kategori </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;"><?php echo $data->category; ?> </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="border-left: 4px solid #013076; border-right: 4px solid #013076;">
            <div class="row">
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-clock-o"></i>&nbsp; Masa Lowongan </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;"><?php echo $data->tgl_awal." - ".$data->tgl_akhir; ?> </p>
              </div>
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-map-marker"></i>&nbsp; Lokasi </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;"><?php echo $data->subdistrict_name.", ".$data->type_adm." ".$data->city; ?></p>
              </div>
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-user"></i>&nbsp; Posisi </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;"><?php echo $data->nama_job; ?> </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row">
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-graduation-cap"></i>&nbsp; Min. Pendidikan </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;"><?php echo $data->min_pendidikan; ?> </p>
              </div>
              <div class="job-panel">
                <p class="job-panel-title"> <i class="fa fa-cogs"></i>&nbsp; Skill Dibutuhkan </p>
                <hr class="job-panel-separator">
                <p class="job-panel-value" style="font-size: 20px;"><?php echo $data->skill; ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
          <div class="job-item">
              <h2>Deskripsi Pekerjaan :</h2>
              <p><?=(($data->deskripsi == '')? 'Tidak ada deskripsi untuk lowongan ini' : $data->deskripsi)?></p>
          </div>
          <div class="job-item">
              <h2>Tanggung Jawab :</h2>
              <p><?=(($data->responsibility == '')? 'Tidak ada keterangan tanggung jawab untuk lowongan ini' : $data->responsibility)?></p>
          </div>
          <div class="job-item">
              <h2>Kualifikasi :</h2>
              <p><?=(($data->qualification == '')? 'Tidak ada keterangan kualifikasi untuk lowongan ini' : $data->qualification)?></p>
          </div>
          <div class="job-item">
              <h2>Lain-lain :</h2>
              <p><?=(($data->next_step == '')? 'Tidak ada keterangan lain-lain untuk lowongan ini' : $data->next_step)?></p>
          </div>
          <!-- <div class="jp_listing_left_bottom_sidebar_key_wrapper">
              <ul>
                  <li><i class="fa fa-tags"></i>Keywords :</li>
                  <?php  foreach ($keyword as $kw) { ?>
                      <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                  <?php } ?>
              </ul>
          </div> -->
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="jp_blog_single_comment_main_wrapper">
            <div class="jp_blog_single_comment_main">
              <?php
                $type = "";
                $jml = $this->db->query(" select * from ngi_jobcomment where is_aktif = 1 and id_job = '".$data->id."'")->num_rows();
                // echo "<pre>";
                // print_r($jml);
                // die();

                 if($this->session->userdata('is_login_pelamar')){
                    $type = "pelamar";
                    $profil = $this->db->query('select iduser as id, nama as nama from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();
                 }else if($this->session->userdata('is_login_perusahaan')){
                    $type = "perusahaan";
                    $profil = $this->db->query('select idperusahaan as id, nama_perusahaan as nama from ngi_perusahaan where idperusahaan = "'.$this->session->userdata('id').'"')->row();
                 }
              ?>
              <p style="font-weight: 600; font-size: 24px; line-height: 32px; color: #013076;"><?=$jml?> KOMENTAR</p>
            </div>
            <div class="jp_blog_single_comment_box_wrapper" style="padding: 5px; border: none; margin: 10px 0px;">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div style="display: inline-block; padding: 10px 15px; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25); border-radius: 10px; margin: 10px 0px;">
                    <?php
                      if($this->session->userdata('is_login_pelamar')){
                          echo "Anda login sebagai ".$profil->nama;
                       }else if($this->session->userdata('is_login_perusahaan')){
                          echo "Anda login sebagai ".$profil->nama;
                       }else{
                          echo "Silahkan login untuk dapat berkomentar atau bertanya";
                       }
                    ?>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper" style="background: #FFFDFD; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25); border-radius: 15px; margin: 10px 0px;">
                    <i class="fa fa-text-height" style="padding-top: 5px;"></i><textarea style="border: none; outline: none;" rows="4" placeholder="Tulis pertanyaan atau komentar anda" id="komentar"></textarea>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="jp_contact_form_btn_wrapper">
                    <ul>
                      <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                        <li><a class="komen">Kirim Komentar</a></li>
                      <?php }else{?>
                        <li><a class="login">LOGIN</a></li>
                      <?php }?>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
                <?php
                  $result = $this->db->query("select a.* from (select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama from (select * from ngi_jobcomment where is_primary = 1 and is_aktif = 1 and id_job = '".$data->id."') a
                                              inner join user_pelamar b on a.id_user = b.iduser and a.tipe_user = 'pelamar'
                                              union
                                              select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s')as tgl, b.photo, b.nama_perusahaan from (select * from ngi_jobcomment where is_primary = 1 and is_aktif = 1 and id_job = '".$data->id."') a
                                              inner join ngi_perusahaan b on a.id_user = b.idperusahaan and a.tipe_user = 'perusahaan') a
                                              order by a.log desc
                                              ")->result();
                ?>
                <?php foreach ($result as $komentar): ?>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 30px">
                    <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <div style="padding: 20px;">
                          <?php if($komentar->tipe_user == "pelamar"){ ?>
                            <img class="img img-responsive img-komentar" src="<?=base_url()?>assets/img/profile/<?=(($komentar->photo != '')? $komentar->photo : 'default_user.png')?>" alt="blog_img"/>
                          <?php }else{ ?>
                            <img class="img img-responsive img-komentar" src="<?=base_url()?>assets/img/profile/<?=(($komentar->photo != '')? $komentar->photo : 'default_company.jpg')?>" alt="blog_img"/>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="komentar-control">
                          <a><i class="fa fa-user"></i>&nbsp; <?=$komentar->nama?></a>
                          | <a><i class="fa fa-calendar"></i>&nbsp; <?=$komentar->tgl?></a>
                          | <a href="#" class="replybtn"><i class="fa fa-reply"></i>&nbsp; Balas</a>
                          <?php if ($komentar->id_user == $profil->id && $komentar->tipe_user == $type): ?>
                            |<a href="#" onclick="hapus('<?=$komentar->id?>')" class="deletebtn"><i class="fa fa-trash"></i>&nbsp; Hapus</a>
                          <?php endif; ?>
                        </div>
                        <p class="komenarea"><?=$komentar->komen?></p>
                        <div class="row reply" style="display: none;">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                              <i class="fa fa-text-height"></i><textarea rows="3" placeholder="Tulis pertanyaan atau komentar anda" class="komentar"></textarea>
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_contact_form_btn_wrapper">
                              <ul>
                                  <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                    <li><a class="komen" onclick="balas('<?=$komentar->id?>', this)">KIRIM KOMENTAR</a></li>
                                  <?php }else{?>
                                    <li><a id="login" class="login">LOGIN</a></li>
                                  <?php }?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php
                  $result2 = $this->db->query("select a.* from (select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama from (select * from ngi_jobcomment where is_aktif = 1 and primary_comment = '".$komentar->id."') a
                                              inner join user_pelamar b on a.id_user = b.iduser and a.tipe_user = 'pelamar'

                                              union

                                              select a.*, date_format(a.log, '%d/%m/%Y %H:%i:%s') as tgl, b.photo, b.nama_perusahaan from (select * from ngi_jobcomment where is_aktif = 1 and primary_comment = '".$komentar->id."') a
                                              inner join ngi_perusahaan b on a.id_user = b.idperusahaan and a.tipe_user = 'perusahaan') a

                                              order by a.log asc
                                              ")->result();
                ?>
                <?php foreach ($result2 as $keySub => $sub_komen): ?>
                  <div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12" style="padding-top: 10px">
                    <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <div style="padding: 20px;">
                          <?php if($sub_komen->tipe_user == "pelamar"){ ?>
                            <img class="img img-responsive img-komentar" src="<?=base_url()?>assets/img/profile/<?=(($sub_komen->photo != '')? $sub_komen->photo : 'default_user.png')?>" alt="blog_img"/>
                          <?php }else{ ?>
                            <img class="img img-responsive img-komentar" src="<?=base_url()?>assets/img/profile/<?=(($sub_komen->photo != '')? $sub_komen->photo : 'default_company.jpg')?>" alt="blog_img"/>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="komentar-control">
                          <a><i class="fa fa-user"></i>&nbsp; <?=$sub_komen->nama?></a>
                          |<a><i class="fa fa-calendar"></i>&nbsp; <?=$sub_komen->tgl?></a>
                          <?php if (count($result2)-1 == $keySub): ?>
                            |<a href="#" class="replybtn"><i class="fa fa-reply"></i>&nbsp; Balas</a>
                          <?php endif; ?>
                          <?php if ($sub_komen->id_user == $profil->id && $sub_komen->tipe_user == $type): ?>
                            |<a href="#" onclick="hapus('<?=$sub_komen->id?>')" class="deletebtn"><i class="fa fa-trash"></i>&nbsp; Hapus</a>
                          <?php endif; ?>
                        </div>
                        <p class="komenarea"><?=$sub_komen->komen?></p>
                        <div class="row reply" style="display: none;">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                              <i class="fa fa-text-height"></i><textarea rows="3" placeholder="Tulis pertanyaan atau komentar anda" class="komentar"></textarea>
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_contact_form_btn_wrapper">
                              <ul>
                                  <?php if($this->session->userdata('is_login_pelamar') || $this->session->userdata('is_login_perusahaan')){ ?>
                                    <li><a class="komen" onclick="balas('<?=$komentar->id?>', this)">KIRIM KOMENTAR</a></li>
                                  <?php }else{?>
                                    <li><a id="login" class="login">LOGIN</a></li>
                                  <?php }?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>
                    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='padding-bottom:15px;'></div>
                  <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php } else {
      redirect(base_url('/portal/page/error'));
    } ?>

    <div class="modal fade" id="myModal" role="dialog" style="">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Login</h4>
	        </div>
	        <div class="modal-body">
	          <p>Silahkan login dahulu untuk bisa melamar pekerjaan</p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
	          <a type="button" class="btn btn-primary">Login</a>
	        </div>
	      </div>
	    </div>
	  </div>

    <script type="text/javascript">
        $(function(){
            $(document).on('click','.trending', function(){
                var trend = $(this).prop('id');

                sendData(trend);
            }).on('click touchstart', '.applynow', function(e){
            	$(this).html('Sedang Proses');

            	$.ajax({
            		url: '<?=base_url()?>portal/page/lamar',
            		type: 'post',
            		data: {
            			 <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
            			 id : '<?=$this->session->userdata('id')?>',
            			 id_job : '<?=$data->id?>'

            		},
            		dataType: 'json',
            		success: function(result){
            			if(result.login){
            				if(result.success){
            					$('.applynow').html('Menunggu konfirmasi');
            					$('.applynow').attr('disabled',true);
            					$('.applynow').attr('tabIndex','-1');
            					$('.applynow').removeClass('applynow');
                                alert('Berhasil melamar pekerjaan. \nSilahkan lengkapi lampiran lamaran anda');
                                window.location = '<?=base_url()?>portal/lamaran';
            				}else{
            					alert('Terjadi kesalahan pada server. Silahkan coba lagi.');
            					$('.applynow').html('Lamar Sekarang');
            				}
            			}else{
            				$('#myModal').modal('show');
            				$('.applynow').html('Lamar Sekarang');
            			}
            		}

            	});

            	e.preventDefault();

            }).on('click', 'a', function(event){
            	if($(this).is("[disabled]")){
			        event.preventDefault();
			    }

            }).on('click touchstart', '.komen', function(){
                if($('#komentar').val() == ""){
                    $('#komentar').focus()
                }else{
                    post('<?=base_url()?>portal/page/savekomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id_job : '<?=$data->id?>',
                        id_user : '<?=$profil->id?>',
                        tipe_user: '<?=$type?>',
                        komen: $('#komentar').val(),
                        is_primary: 1,
                        url : window.location.href
                    }, "post");
                }

            }).on('click touchstart', '.login', function(){
                post('<?=base_url()?>portal/login', {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    url : window.location.href

                }, "post");

            }).on('click touchstart', '.replybtn', function(e){
                $('.reply').hide();
                $(this).closest('div').siblings('.reply').show();
                $(this).closest('div').siblings('.reply').find('textarea').focus();

                e.preventDefault();
            });

        });

        function sendData(trending = ''){
            post('<?=base_url()?>portal/caripekerjaan', {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    keyword : trending
                }, "post");
        }

        function hapus(id){
            if(confirm('Yakin ingin menghapus komen ini?')){
                post('<?=base_url()?>portal/page/hapuskomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id : id,
                        url : window.location.href
                    }, "post");
            }
        }

        function balas(idkomen, obj){
            var reply = obj.closest('.reply').children[0].children[0].children[1];

            if(reply.value == ""){
                reply.focus();
            }else{
                post('<?=base_url()?>portal/page/savekomentar', {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        id_job : '<?=$data->id?>',
                        id_user : '<?=$profil->id?>',
                        tipe_user: '<?=$type?>',
                        komen: reply.value,
                        primary_comment : idkomen,
                        is_primary: 0,
                        url : window.location.href
                    }, "post");
            }
        }


        function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }
    </script>

<br>
<br>
