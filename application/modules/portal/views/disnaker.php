<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/magnific-popup.css" />
<style>
.corner-profile {
  background: url("<?php echo base_url(); ?>assets/img/profile_corner.png") no-repeat right center;
  background-repeat: no-repeat;
  background-size: 375px 275px;
  background-position: right 20px bottom 20px;
}
.corner-visimisi {
  background: url("<?php echo base_url(); ?>assets/img/visimisi_corner.png") no-repeat right center;
  background-repeat: no-repeat;
  background-size: 250px 200px;
  background-position: left 0px bottom 10px;
}
.corner-strategi {
  background: url("<?php echo base_url(); ?>assets/img/strategi_corner.png") no-repeat right center;
  background-repeat: no-repeat;
  background-size: 400px 200px;
  background-position: left 75px bottom 10px;
}
.corner-tupoksi {
  background: url("<?php echo base_url(); ?>assets/img/tupoksi_corner.png") no-repeat right center;
  background-repeat: no-repeat;
  background-size: 300px 300px;
  background-position: right 20px bottom 10px;
}
</style>

<div class="jp_banner_heading_cont_wrapper" style="padding-top: 20px; background-color: white;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="jp_job_heading_wrapper">
          <div class="jp_job_heading text-center">
            <center>
              <table style="margin-bottom: 20px;" border="0">
                <tr>
                  <td rowspan="2">
                    <img src="<?php echo base_url('/assets/img/logo-bogor.jpg'); ?>" class="pull-right" style="width: 110px; margin-right: 10px">
                  </td>
                  <td>
                    <p style="font-weight: 900; font-size: 2em; color: black;">DINAS TENAGA KERJA</p>
                    <p style="font-weight: 900; font-size: 2em; color: black;">KABUPATEN BOGOR </p>
                  </td>
                </tr>
                <tr style="border-top: 2px solid black;">
                  <td>
                    <p style="color: black; font-size: 1.2em;">Jl. Bersih No. 2 Komplek Pemda Kabupaten Bogor<br>Telp. (021) 8757668 – 8760273, Cibinong 16914</p>
                  </td>
                </tr>
              </table>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div id="view-ad-image">
      <div class="owl-carousel owl-theme">
        <?php
          $qSlider = $this->db->query("
            SELECT * FROM r_pengaturan
            WHERE parameter = 'profile_slider'
            ORDER BY num ASC
          ")->result();
        ?>
        <?php foreach ($qSlider as $key => $value): ?>
          <div class="item"> <img class="img-owl" src="<?php echo base_url(); ?>/assets/img/banner/<?php echo $value->value; ?>" alt=""> </div>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="container">
      <div class="row" style="background: #FFFFFF; box-shadow: 0px 5px 13px rgba(0, 0, 0, 0.25); border-radius: 28px; margin-bottom: 50px; margin-top: -50px; position: relative; z-index: 99;">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 20px;">
          <p style="color: #013076; font-weight: 800; font-size: 1.2em;">BOGOR CAREER CENTER</p>
          <p style="text-align: justify;">implementasi karsa bogor maju untuk lulusan terbaik yang sedang mencari pekerjaan di kabupaten bogor</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 20px;">
          <p style="color: #013076; font-weight: 800; font-size: 1.2em;">BOGOR CAREER CENTER</p>
          <p style="text-align: justify;">implementasi karsa bogor maju untuk lulusan terbaik yang sedang mencari pekerjaan di kabupaten bogor</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 20px;">
          <p style="color: #013076; font-weight: 800; font-size: 1.2em;">BOGOR CAREER CENTER</p>
          <p style="text-align: justify;">implementasi karsa bogor maju untuk lulusan terbaik yang sedang mencari pekerjaan di kabupaten bogor</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="jp_blog_cate_main_wrapper" style="margin-top: -80px; padding-bottom: 0px; background-color: white; font-family: 'Poppins'; font-style: normal; font-weight: normal; font-size: 24px; line-height: 36px;">
  <div id="profil" class="row" style="background-color: #184981; color: white; margin-top: 10px; padding: 30px; font-size: 18px; font-weight: normal; line-height:28px;">
    <div class="container corner-profile">
      <?php
        $qTemp = $this->db->select('content')->from('section_profile')->where('name','profile')->get()->row();
        echo $qTemp->content;
      ?>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <?php
          $qVideo = $this->db->query("
            SELECT * FROM r_pengaturan
            WHERE parameter = 'video_disnaker'
          ")->row();
        ?>
        <video width="100%" height="55%" autoplay loop controls style="border-radius: 10px; border: 2px solid white; object-fit: cover;">
          <source src="<?php echo base_url('/assets/video/'.$qVideo->value) ?>">
        </video>
      </div>
    </div>
  </div>
  <br>
  <div id="kadis" class="row">
    <div class="container">
      <span class="pull-right" style="color: #013076; font-weight: bold; font-size: 3em; margin-right: 30px; text-transform: uppercase;">| KEPALA DINAS</span>
    </div>
    <?php
      $qTemp = $this->db->select('content')->from('section_profile')->where('name','kadis')->get()->row();
      echo $qTemp->content;
    ?>
  </div>
  <br>
  <div class="container">
    <span id="visimisi" style="color: #013076; font-weight: bold; font-size: 3em; margin: 20px; margin-top: 50px; margin-bottom: 40px;" class="pull-right">VISI & MISI |</span>
    <div class="row" style="color: #184981; background-color: white; margin-top: 0px; padding: 30px;">
      <?php
        $qTemp = $this->db->select('content')->from('section_profile')->where('name','visimisi')->get()->row();
        echo $qTemp->content;
      ?>
    </div>
  </div>
  <br>
  <br>
  <br>

  <div class="container">
    <span id="strategi" style="color: #013076; font-weight: bold; font-size: 3em; margin: 20px;">| STRATEGI</span>
  </div>
  <div class="row" style="background-color: #184981; color: white; margin-top: 10px; padding: 20px 45px;">
    <div class="container corner-strategi">
      <?php
        $qTemp = $this->db->select('content')->from('section_profile')->where('name','strategi')->get()->row();
        echo $qTemp->content;
      ?>
    </div>
  </div>
  <br>
  <div class="container">
    <span id="tupoksi" style="width: 100%; color: #013076; font-weight: bold; font-size: 3em; margin: 20px; margin-top: 50px; margin-bottom: 40px;" class="text-right pull-right">TUGAS POKOK |</span>
    <div  id="accordion" class="row corner-tupoksi" style="color: #184981; background-color: white; margin-top: 0px; padding: 30px; font-size: 18px; font-weight: normal; line-height:32px;">
      <?php
      $qTemp = $this->db->select('content')->from('section_profile')->where('name','tupoksi')->get()->row();
      echo $qTemp->content;
      ?>
    </div>
  </div>
  <br>
  <br>
  <div class="container">
    <span id="struktur" style="width: 100%; color: #013076; font-weight: bold; font-size: 3em;" class="text-right pull-right">STRUKTUR ORGANISASI |</span>
  </div>
  <div class="row" style="background-color: #184981; color: white; margin-top: 70px; padding: 60px; font-size: 18px; font-weight: normal; line-height:32px;">
    <div class="container">
      <?php
      $qTemp = $this->db->select('content')->from('section_profile')->where('name','struktur')->get()->row();
      echo $qTemp->content;
      ?>
    </div>
  </div>
</div>
  <br>
<div style="background: #fff;">
  <div class="container"style="background: #fff;">
    <span style="width: 100%; color: #013076; font-weight: bold; font-size: 3em; margin: 20px; margin-top: 50px; margin-bottom: 40px;" class="pull-left">| DOKUMEN PUBLIKASI</span>
    <div class="row" style="color: #184981; margin-top: 0px; padding: 30px; font-size: 18px; font-weight: normal; line-height:32px;">
      <table id="publikasi" class="table table-bordered">
        <thead>
          <tr style="background: #184981; color: white; font-size: 20px;">
            <th>No</th>
            <th>Nama Dokumen</th>
            <th>Kategori Dokumen</th>
            <th>Tanggal</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr style="background: #F1F1F1; font-weight: bold;">
            <td>1</td>
            <td>Dummy 01</td>
            <td>Dummy Kategory</td>
            <td>12-10-2021</td>
            <td>
              <button class="btn btn-info btn-sm btn-block"> <i class="fa fa-download"></i> Unduh</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<style media="screen">
  .owl-nav, .owl-dots {
    display: none;
  }
</style>

<script type="text/javascript">

$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop: true,
    nav: true,
    dots: true,
    navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
      0: {
        items: 1,
      }
    },
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true
  });
  $('#publikasi').DataTable({

  });
});
</script>
