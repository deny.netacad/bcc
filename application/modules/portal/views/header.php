<style>
.modal-body {
    position: relative;
    padding: 60px;
}
.btn-primary {
    color: #fff;
    background-color: #2b77cd;
    border-color: #2e6da4;
    border-radius: 15px;
}

#view-ad-image {
  width: 100%;
  overflow: hidden;
  background: white;
  position: relative;
}

.owl-carousel .owl-stage-outer {
  margin-bottom:-70px !important;
}

.img-btn{
  cursor: pointer;
}
.img-btn:hover{
  transform: scale(1.05);
}

.jp_top_jobs_category_wrapper_new {
    float: left;
    width: 100%;
    margin: 10px 0px;
    color: #2B77CD;
    padding: 60px 30px;
    background: #FFFFFF;
    box-shadow: 0px 0px 10px 4px rgba(0, 0, 0, 0.1);
    border-radius: 15px;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1);
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1);
    transition: all 200ms ease-in;
    transform: scale(1);
}

.select2-container {
  width: 100% !important;
  height: 70px !important;
  padding-top: 10px !important;
  margin-top: -10px !important;
  border: none !important;
  outline: none !important;
}
.select2-container-active {
  border: none !important;
  outline: none !important;
}

.select2-container .select2-choice {
  height: 50px !important;
  background: none !important;
  border: 1px solid #2B77CD !important;
  outline: none !important;
  box-sizing: border-box !important;
  border-radius: 37.5px !important;
  width: 100% !important;
  padding-left: 40px;
}
.select2-container .select2-choice .select2-arrow {
  visibility: hidden;
  padding-left: 50px;
  z-index: 3;
}
.select2-container .select2-choice .select2-chosen {
  padding-top: 10px !important;
  height: 50px !important;
}

.select2-drop-active {
  border-top: 1px solid #2B77CD;
  padding-top: 7px !important;
}
.select2-drop.select2-drop-above.select2-drop-active {
  border-bottom: 1px solid #2B77CD;
}

</style>

<script>
  $(function (){
    $(document).on('change','select[name="province"]',function(){
        $.ajax({
            url : '<?=base_url()?>portal/page/cariKab',
            type: 'post',
            data: {
              <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
              id : $(this).val()
            },
            success: function(result){
              $('select[name="city"]').html(result);
            }
        });
    }).on('click', 'a.link_category', function(){
      post('<?=base_url()?>portal/page/caripekerjaan', {
          <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
          category : $(this).prop('id')
      }, "post");
    }).on('click', 'a.trending', function(){
      post('<?=base_url()?>portal/page/caripekerjaan', {
          <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
          keyword : $(this).prop('id')
      }, "post");
    });
  });

  function post(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");

    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
      if(params.hasOwnProperty(key)) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);
        form.appendChild(hiddenField);
      }
    }
    document.body.appendChild(form);
    form.submit();
  }
</script>

<div class="jp_banner_heading_cont_wrapper" style="padding-top: 10px">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div id="view-ad-image">
        <div class="owl-carousel owl-theme">
          <?php
            $qSlider = $this->db->query("
              SELECT * FROM r_pengaturan
              WHERE parameter = 'front_slider'
              ORDER BY num ASC
            ")->result();
          ?>
          <?php foreach ($qSlider as $key => $value): ?>
            <div class="item"> <img class="img-owl" src="<?php echo base_url(); ?>/assets/img/banner/<?php echo $value->value; ?>" alt=""> </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row" style="margin-top: 10px">
          <div class="col-lg-3 col-sm-6 col-xs-6" style="padding: 5px 10px">
            <a href="<?php echo base_url('/portal/kartukuning') ?>">
              <img src="<?php echo base_url('/assets/img/button/kartu_kuning.png'); ?>" class="img-btn img-responsive" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 col-xs-6" style="padding: 5px 10px">
            <img src="<?php echo base_url('/assets/img/button/balai.png'); ?>" class="img-btn img-responsive" alt="">
          </div>
          <div class="col-lg-3 col-sm-6 col-xs-6" style="padding: 5px 10px">
            <img src="<?php echo base_url('/assets/img/button/sertifikasi.png'); ?>" class="img-btn img-responsive" alt="">
          </div>
          <div class="col-lg-3 col-sm-6 col-xs-6" style="padding: 5px 10px">
            <img src="<?php echo base_url('/assets/img/button/antrian.png'); ?>" class="img-btn img-responsive" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
              <div class="jp_banner_heading_cont_wrapper" style="background-color: #184981; padding: 100px 10px !important; margin-top:20px">
                <div class="container" style="margin-top: -100px">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form action="<?=base_url()?>portal/page/caripekerjaan" method="POST" id="jobsearch">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
                            <div class="jp_header_form_wrapper" style="border-radius: 15px 15px 0px 0px; background: #F4F4F4;">
                              <h4 style="color: #3D3C3C; margin-bottom: 10px;">Hallo</h4>
                              <h2 style="color: #3D3C3C; font-weight: 500;">Mau Cari Lowongan Pekerjaan?</h2>
                            </div>
                            <div class="jp_header_form_wrapper row" style="margin: 0px !important; background: #FFFFFF; border-radius: 0px 0px 15px 15px;">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <input style="border: 1px solid #2B77CD; box-sizing: border-box; border-radius: 37.5px;" type="text" id="form-keyword" name="keyword" placeholder="Posisi atau Perusahaan" value="<?=(isset($_POST['keyword'])?$_POST['keyword']:"");?>">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="jp_form_location_wrapper">
                                        <i class="fa fa-dot-circle-o first_icon"></i>
                                <select name="province" class="select2">
                                    <option value="">Semua Provinsi</option>
                                    <?php
                                        $rs = $this->db->query("select province_id, province from ngi_kecamatan group by province_id")->result();

                                        foreach ($rs as $item) {
                                            if(isset($_POST)){
                                                if($_POST['province'] == $item->province_id){
                                                    echo '<option value="'.$item->province_id.'" selected="true">'.$item->province.'</option>';
                                                }else{
                                                    echo '<option value="'.$item->province_id.'">'.$item->province.'</option>';
                                                }
                                            }else{
                                                echo '<option value="'.$item->province_id.'">'.$item->province.'</option>';
                                            }
                                        }
                                    ?>
                                </select><i class="fa fa-angle-down second_icon"></i>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="jp_form_location_wrapper">
                                        <i class="fa fa-dot-circle-o first_icon"></i>
                                <select name="city" class="select2">
                                    <option value="">Semua Kab/Kota</option>
                                    <?php
                                        if(isset($_POST)){
                                          if($_POST['province']){
                                            if($_POST['city']){
                                              $rs = $this->db->query("select city, type, city_id from ngi_kecamatan where province_id = '".$_POST['province']."' group by city_id")->result();

                                              foreach ($rs as $item) {
                                                  if($_POST['city'] == $item->city_id){
                                                      echo '<option value="'.$item->city_id.'" selected="true">'.$item->type.' '.$item->city.'</option>';
                                                  }else{
                                                      echo '<option value="'.$item->city_id.'">'.$item->type.' '.$item->city.'</option>';
                                                  }
                                              }
                                            }else{
                                              $rs = $this->db->query("select city, type, city_id from ngi_kecamatan where province_id = '".$_POST['province']."' group by city_id")->result();

                                              foreach ($rs as $item) {
                                                  echo '<option value="'.$item->city_id.'">'.$item->type.' '.$item->city.'</option>';
                                              }
                                            }
                                          }
                                        }
                                    ?>
                                </select><i class="fa fa-angle-down second_icon"></i>
                                    </div>
                                </div>
                                <?php
                								$this->load->library('user_agent');
                								if($this->session->userdata('is_login_pelamar')) { ?>
                                  <div class="row">

                								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-left" style="margin-top: 30px;">
                                  <div class="jp_form_btn_wrapper">
                                    <ul>
                                      <li><a style="background: #2B77CD; border-radius: 37.5px; padding:0px !important;" id="<?=(($this->agent->platform() == "Linux")? 'scan' : 'not-avaliable')?>" type="button" class="btn btn-primary"><i class="fa fa-qrcode"></i>SCAN</a></li>
                                    </ul>
                                  </div>
                                </div>
                								<?php }?>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right" style="margin-top: 30px;">
                                    <div class="jp_form_btn_wrapper">
                                        <ul>
                                            <li><a style="background: #2B77CD; border-radius: 37.5px;" id="form-cari" onclick="document.getElementById('jobsearch').submit()"><i class="fa fa-search"></i> Cari</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>


                            </div>
                        </form>
                    </div>
                </div>
        </div>
      </div>
      <?php if(!isset($page)){ ?>
        <div class="jp_banner_main_jobs_wrapper" style="padding-bottom: 25px; margin-top: -60px;">
          <div class="container">
            <div class="row">
              <?php
                $rs = $this->db->query('select SUM(jml_pekerja) as "jml", a.category, a.icon, a.id from ngi_jobcategory a
                                        left join (select * from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day) ) b on a.id = b.`idcategory`
                                        WHERE b.id!=0
                                        group by a.id
                                        order by rand()
                                        limit 4')->result();
              ?>
              <?php foreach ($rs as $item) { ?>
                <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="jp_top_jobs_category_wrapper_new">
                    <div class="row">
                      <div class="col-xs-5 text-center">
                        <i class="fa <?=$item->icon?>" style="font-size: 7em"></i>
                      </div>
                      <div class="col-xs-7 text-left" style="margin-top: 10px">
                        <h2><a href="#" id="c<?=$item->id?>" class="link_category" style="font-weight: 500; color: #2B77CD;"><?=$item->category?></a></h2>
                        <p><?=$item->jml?> Posisi</p>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>

              <?php
                $jml = $this->db->query('select SUM(jml_pekerja) as "jml" from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day)')->row();
               ?>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="jp_top_jobs_category_wrapper_new">
                  <div class="row">
                    <div class="col-xs-5 text-center">
                      <i class="fa fa-th-large" style="font-size: 7em"></i>
                    </div>
                    <div class="col-xs-7 text-left" style="margin-top: 10px">
                      <h2><a href="<?=base_url()?>portal/page/caripekerjaan" style="font-weight: 500; color: #2B77CD;">Total Lowongan</a></h2>
                      <p><?=$jml->jml?> Posisi</p>
                    </div>
                  </div>
                </div>
              </div>

              <?php
                $aktf = $this->db->query('select count(id) as "jml_aktif" from ngi_joblist where is_aktif = 1 and now() < date_add(date_end, interval 1 day)')->row();
              ?>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="jp_top_jobs_category_wrapper_new ">
                  <div class="row">
                    <div class="col-xs-5 text-center">
                      <i class="fa fa-check-square-o" style="font-size: 7em"></i>
                    </div>
                    <div class="col-xs-7 text-left" style="margin-top: 10px">
                      <h2><a href="<?=base_url()?>portal/page/caripekerjaan" style="font-weight: 500; color: #2B77CD;">Lowongan Aktif</a></h2>
                      <p><?=$aktf->jml_aktif?> Lowongan</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>

			<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">SCAN Untuk Mendaftar Pekerjaan</h5>
		<small style="font-size:10px;font-style:italic;">*Disarankan menggunakan browser chrome dengan versi terupdate</small>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
          <div class="col-lg-12">
            <div id="app">
                <div class="row">
                    <div class="col-lg-4">
                    <section class="cameras">
                      <ul>
                        <li v-if="cameras.length === 0" class="empty">No cameras found</li>
						<!--li v-for="camera in cameras">
                          <span v-if="camera.id == activeCameraId" :title="formatName(camera.name)" class="active">{{ formatName(camera.name) }}</span>
                          <span v-if="camera.id != activeCameraId" :title="formatName(camera.name)">
                            <a @click.stop="selectCamera(camera)">{{ formatName(camera.name) }}</a>
                          </span>
                        </li-->
                      </ul>
                    </section>
                    <div id="list">

                    </div>
                  </div>
                    <div class="col-lg-8">
                        <video width="100%" height="50%" id="preview"></video>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
		<script>
	var qr = '';

  $(document).ready(function(){
    $('.select2')
      .select2()
      .on("select2-close", function () {
        setTimeout(function() {
          $('.select2-container-active').removeClass('select2-container-active');
        }, 1);
      });

    $('.owl-carousel').owlCarousel({
      loop: true,
      nav: true,
      dots: true,
      navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      responsive: {
        0: {
          items: 1,
        }
      },
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:true
    });

    $('#sliderPerusahaan .owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      autoplay:true,
      autoWidth:true,
      responsiveClass: true,
      smartSpeed: 1200,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 2,
          nav: true
        }
      }
    });
  });

	$(document).on('click', '#scan', function(){
		var app = new Vue({
				el: '#app',
				data: {
				scanner: null,
				activeCameraId: null,
				cameras: [],
				scans: []
			},

		  mounted: function () {
			var self = this;
			self.scanner = new Instascan.Scanner({ video: document.getElementById('preview'),mirror: false, scanPeriod: 3 ,});
			self.scanner.addListener('scan', function (content, image) {
			  self.scans.unshift({ date: +(Date.now()), content: content });
			  qr = content;
			  kirim(qr);
			});
			Instascan.Camera.getCameras().then(function (cameras) {
			  self.cameras = cameras;
			  if (cameras.length > 0) {
				// self.activeCameraId = cameras[1].id;
				self.scanner.start(cameras[1]);
			  }else {
				console.error('Your Devices Not Support to use');
			  }
			}).catch(function (e) {
			  console.error(e);
			});
		  },
		  methods: {
			formatName: function (name) {
			  return name || 'Camera Ready To Scan!';
			},
			selectCamera: function (camera) {
			  this.activeCameraId = camera.id;
			  this.scanner.start(camera);
			}
		  }
		});

		$('#showModal').modal('show');
	}).on('click', '#not-avaliable', function(){
		alert('This feature is only available on android device');
	})

function kirim(qr)
{
   if(confirm('Apakah anda yakin akan mendaftar?')){
		$.ajax({
		   url: "<?php echo base_url('portal/page/readqr')?>/" + qr,
						type: 'post',
						data: {
							 <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
							 id : '<?=$this->session->userdata('id')?>',
							 id_job : qr

						},
						dataType: 'json',

			success: function(result){
				if(result.login){
					if(result.success){
						alert('Pendaftaran pekerjaan berhasil');
						$('#showModal').modal('hide'); // show bootstrap modal when complete loaded
						window.location.href = "<?=base_url()?>portal/lamaran";
					}else{
						alert('Anda sudah melakukan pendaftaran pada pekerjaan tersebut');
						window.location.href = "<?=base_url()?>portal/lamaran";
					}
				}
			},

		});
	}else{
		$('#showModal').on('hide.bs.modal', function(e){
		}).modal('hide');
	}
}



</script>
