<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />

<!--<link rel="stylesheet" href="<?=base_url()?>instascan-master/docs/style.css">-->

  <style type="text/css">
  .jp_recent_resume_cont_wrapper {
    width: 100% !important;
  }
  
 .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
} 
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: rgb(24, 73, 129);
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #184981;
    border-color: #184981;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    z-index: 2;
    color: #fff;
    border: 1px solid #184981;
    background-color: #184981;
    border-color: none;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_bottom_cont_wrapper {
    float: left;
    width: 100%;
    background: #f9f9f9;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-bottom: 1px solid #184981;
    border-top: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
.mainmenu ul li a {

    padding: 0px 7px 47px 10px;
    }
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_job_des h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_job_des h2:before {
    content: '';
    border: 1px solid #184980;
    width: 8px;
    position: absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
  
    .login_wrapper button.btn {
        color: #fff;
        width: 100%;
        height: 50px;
        padding: 6px 25px;
        line-height: 36px;
        margin-bottom: 20px;
        text-align: left;
        border-radius: 8px;
        background-color: #337AB8;
        font-size: 16px;
        border: 1px solid #f9f9f9;
        text-align: center;
        text-transform: uppercase;
    }

 .jp_cp_rd_wrapper li:first-child a {
    float: left;
    width: 100%;
    height: 50px;
    text-align: center;
    line-height: 50px;
    background: #184980;
    color: #ffffff;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    font-size: 18px;
    font-weight: 600;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -ms-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
}
.accordion_wrapper .panel .panel-heading a {
    display: block;
    background: #184980;
    padding: 12px 20px;
    color: #fff;
}
.abt_page_2_wrapper .panel .panel-heading a.collapsed {
    background: #184980 !important;
}
.btn-info {
    color: #fff;
    background-color: #174981;
    border-color: #2c748a;
}

.btn-info:hover{
    background-color:#0f3054;
}
</style>
<?php if($this->session->userdata('is_login_pelamar')){?>
<div class="jp_listing_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 jp_cl_right_bar" style="padding-top: 20px">
                <div class="jp_job_des">
                    <h2>Daftar Lamaran 
					</h2>
					
                </div>
				
                <div class="row" style="padding-top: 40px" id="tempat">
                    <?php

                        $q = $this->db->query('select ifnull(g.reason,"Tenggat waktu habis") as reason, f.name as upah, date_format(a.log, "%d %M %Y") as tgl, a.status, c.nama_perusahaan, c.photo, d.nama_job, b.*, e.province, e.city, e.subdistrict_name  from (select * from ngi_jobapplied where id_pelamar = "'.$this->session->userdata('id').'") a
                                        left join ngi_joblist b on a.id_job = b.id
                                        left join ngi_perusahaan c on b.idperusahaan = c.idperusahaan
                                        left join ngi_job d on d.id = b.posisi
                                        left join ngi_kecamatan e on b.idkecamatan = e.subdistrict_id
										left join ngi_salary f on b.upah_kerja = f.id
                                        left join reason g on a.reason = g.id
										 WHERE d.nama_job IS NOT NULL
                                        order by a.log desc');

                        if($q->num_rows() > 0) {
                            $data = $q->result();
                            foreach ($data as $item) {
                                // $attachment = explode(",", $item->attachment);

                                // $array = [];
                                // foreach ($attachment as $att) {
                                //     $bool = $this->db->query("select nama_file from ngi_jobattachment where idjob = '".$item->id."' and idpelamar = '".$this->session->userdata('id')."' and idfile = '".$att."'")->num_rows();
                                //     array_push($array, $bool);
                                // }

                    ?>

                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <div class="jp_recent_resume_cont_wrapper">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px">
                                    <h3>
                                        <?=$item->title?> ( <?=$item->nama_perusahaan?> ) 
                                        <button type="button" class="btn btn-success btn-sm" onclick="lengkapilampiran('<?=$item->id?>')"> Upload lampiran </button>
                                    </h3>
                                </div>
                                <div style="padding-top: 25px">
                                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-user fa-icon"></i> &nbsp;<?=$item->nama_job?></div>
                                        <div><i class="fa fa-map-marker fa-icon"></i> &nbsp;<?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div><i class="fa fa-money fa-icon"></i> &nbsp;Rp. <?=($item->upah)?></div>
                                        <div><i class="fa fa-calendar fa-icon"></i> &nbsp;<?=$item->tgl?></div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                        <a class="btn btn-info" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>"><i class="fa fa-eye"></i> &nbsp;Detail</a>
                                        <?php if($item->status == "0" || strtotime($item->date_end."23:59:59") < time()) {
                                            echo ' <button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Alasan : '.$item->reason.'"><i class="fa fa-info-circle"></i> &nbsp;Ditolak</button>';
                                        }else if($item->status == "1") {
                                            echo ' <button class="btn btn-success" disabled><i class="fa fa-info-circle"></i> &nbsp;Diterima</button>';
                                        }else{
                                            echo ' <button class="btn btn-default" disabled><i class="fa fa-info-circle"></i> &nbsp;Belum dikonfirmasi</button>';
                                        }?>                                       
                                    </div>
                                    <div class="hidden-lg hidden-md col-sm-12 col-xs-12 mobile-btn">
                                        <a class="btn btn-info" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>"><i class="fa fa-pencil"></i> &nbsp;Detail</a>
                                        <?php if($item->status == "0" || strtotime($item->date_end."23:59:59") < time()) {
                                            echo ' <button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Alasan : '.$item->reason.'"><i class="fa fa-info-circle"></i> &nbsp;Ditolak</button>';
                                        }else if($item->status == "1") {
                                            echo ' <button class="btn btn-success" disabled><i class="fa fa-info-circle"></i> &nbsp;Diterima</button>';
                                        }else{
                                            echo ' <button class="btn btn-default" disabled><i class="fa fa-info-circle"></i> &nbsp;Belum dikonfirmasi</button>';
                                        }?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>

                    <?php }else{?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_recent_resume_box_wrapper">
                            <h3 style="text-align: center; color: #184981;">Tidak ada riwayat lamaran</h3>
                        </div>
                    </div>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php }else{ ?>
<script type="text/javascript">
    $(function(){
        post('<?=base_url()?>portal/login/', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', url : '<?=base_url()?>portal/lamaran/'}, "post");
    })
</script>
    <!-- redirect(base_url()); -->
<?php }?>


<script type="text/javascript">
    $(function(){
        $('[data-toggle="tooltip"]').tooltip(); 

        $(document).on('click','#view',function(){
            $.ajax({
                type: 'post',
                url: '<?=base_url()?>perusahaan/page/viewmore',
                data: {
                    '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                    'more' : $(this).attr('data')
                },
                success:function(result){
                    $('#tempat').html(result);
                }
            })
        }).on('click', '.closes', function(){
            if(confirm('Anda yakin ingin menutup lowongan ini?')){
                $.ajax({
                    type: 'post',
                    url: '<?=base_url()?>perusahaan/page/tutuplowongan',
                    data: {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'id' : $(this).prop('id')
                    },
                    success:function(result){
                        alert(result);
                        location.reload();
                    }
                })
            }
        }).on('click', '.delete', function(){
              if(confirm('Anda yakin ingin menghapus lowongan ini?')){
                $.ajax({
                    type: 'post',
                    url: '<?=base_url()?>perusahaan/page/hapuslowongan',
                    data: {
                        '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                        'id' : $(this).prop('id')
                    },
                    success:function(result){
                        alert(result);
                        location.reload();
                    }
                })
            }
        }).on('click', '.edit', function(){
            post('<?=base_url()?>perusahaan/buatlowongan', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', edit : $(this).prop('id')}, "post");
        });
    });

    function lengkapilampiran(id){
        post('<?=base_url()?>portal/lengkapilampiran/', {<?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>', idjob : id}, "post")
    }

    function post(path, params, method) {
        method = method || "post"; // Set method to post by default if not specified.

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }
</script>
<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">SCAN Untuk Mendaftar Pekerjaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
          <div class="col-lg-12">
            <div id="app">
                <div class="row">
                    <div class="col-lg-4">
                    <section class="cameras">
                      <ul>
                        <li v-if="cameras.length === 0" class="empty">No cameras found</li>
                      <!-- <li v-for="camera in cameras">
                          <span v-if="camera.id == activeCameraId" :title="formatName(camera.name)" class="active">{{ formatName(camera.name) }}</span>
                          <span v-if="camera.id != activeCameraId" :title="formatName(camera.name)">
                            <a @click.stop="selectCamera(camera)">{{ formatName(camera.name) }}</a>
                          </span>
                        </li>-->
                      </ul>
                    </section>
                    <div id="list">
                        
                    </div>
                  </div>
                    <div class="col-lg-8">
                        <video width="100%" height="50%" id="preview"></video>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script>
var qr = '';
  // var app = new Vue({
  // el: '#app',
  // data: {
    // scanner: null,
    // activeCameraId: null,
    // cameras: [],
    // scans: []
  // },

  // mounted: function () {
    // var self = this;
    // self.scanner = new Instascan.Scanner({ video: document.getElementById('preview'),mirror: false, scanPeriod: 3 ,});
    // self.scanner.addListener('scan', function (content, image) {
      // self.scans.unshift({ date: +(Date.now()), content: content });
      // qr = content;
      // kirim(qr);
    // });
    // Instascan.Camera.getCameras().then(function (cameras) {
      // self.cameras = cameras;
      // if (cameras.length > 0) {
       // self.activeCameraId = cameras[0].id;
        // self.scanner.start(cameras[1]);
      // }else {
        // console.error('Your Devices Not Support to use');
      // }
    // }).catch(function (e) {
      // console.error(e);
    // });
  // },
  // methods: {
    // formatName: function (name) {
      // return name || 'Camera Ready To Scan!';
    // },
    // selectCamera: function (camera) {
      // this.activeCameraId = camera.id;
      // this.scanner.start(camera);
    // }
  // }
// });

function kirim(qr)
{
   alert('Apakah anda yakin akan mendaftar?');
    $.ajax({
       url: "<?php echo base_url('portal/page/readqr')?>/" + qr,
            		type: 'post',
            		data: {
            			 <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
            			 id : '<?=$this->session->userdata('id')?>',
            			 id_job : qr

            		},
            		dataType: 'json',
					
		success: function(result){
			if(result.login){
            				if(result.success){
								alert('Pendaftaran pekerjaan berhasil');
								$('#showModal').modal('hide'); // show bootstrap modal when complete loaded 
								window.location.href = "<?=base_url()?>portal/lamaran";
            				}else{
            					alert('Anda sudah melakukan pendaftaran pada pekerjaan tersebut');
								window.location.href = "<?=base_url()?>portal/lamaran";
            				}
            			}
		},
       
    });
}

</script>
