<hr style="margin: 0px 0px 20px 0px; border: 1px solid #184981">
<div class="container" style="padding: 50px;">
  <center>
    <img src="<?php echo base_url('/assets/img/404.png') ?>" class="img img-responsive" width="350px" alt="">
    <p style="font-weight: 600; font-size: 2.5em; margin-top: 25px; text-align: center; color: #184981;">Waduhhh yang Kamu Cari Tidak Ada</p>
    <p style="font-weight: 400; font-size: 1.5em; margin-top: 20px; text-align: center; color: #919191;">Mungkin Kamu Salah Jalan atau Alamat. Coba Cari Lagi...</p>
  </center>

</div>
