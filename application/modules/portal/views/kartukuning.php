
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />

   <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/locales-all.min.js"></script>
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.css">

   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/id.min.js" integrity="sha512-he8U4ic6kf3kustvJfiERUpojM8barHoz0WYpAUDWQVn61efpm3aVAD8RWL8OloaDDzMZ1gZiubF9OSdYBqHfQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

	<?php
        $url = $this->uri->segment(3);
        $rs = $this->db->query('select a.*, date_format(a.tgl_lahir, "%d %M %Y") as tgl_lhr, b.province, b.city, b.type, b.subdistrict_name from (select * from user_pelamar a where a.profile_url = "'.$url.'") a
        							left join ngi_kecamatan b on a.kecamatan_id = b.subdistrict_id');
        $rs2 = $this->db->query('select a.*, date_format(a.tgl_lahir, "%d %M %Y") as tgl_lhr, b.province, b.city, b.type, b.subdistrict_name from (select * from user_pelamar a where a.iduser = "'.$this->session->userdata('id').'") a
        							left join ngi_kecamatan b on a.kecamatan_id = b.subdistrict_id');

        if($rs->num_rows() > 0 || $rs2->num_rows() > 0){
        	if($rs->num_rows() > 0){
        		$data = $rs->row();
        	}else{
        		$data = $rs2->row();
        	}

        $item = $this->db->query("
          SELECT
            a.*,DATE_FORMAT(a.tgl_lahir,'%Y-%m-%d') AS tgl_lhr,
            DATE(curdate() + INTERVAL 2 YEAR) as kadaluarsa,
            DATE_FORMAT(a.register,'%m') as bln,
            DATE_FORMAT(a.register,'%Y') as thn,
            b.*
          FROM user_pelamar a
          LEFT JOIN ngi_kecamatan b ON a.kecamatan_id=b.subdistrict_id
          WHERE a.iduser='".$this->session->userdata('id')."'
        ")->row();

        $default_quota = $this->db->query("
          SELECT value FROM r_pengaturan WHERE parameter = 'kuota_default'
        ")->row()->value;
        $daysForward = 30;

        $current = $this->db->query("
          SELECT
            DATE(dtm) as date,
            COUNT(DATE(dtm)) as jumlah
          FROM h_antrian_online
          WHERE DATE(dtm) BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d', strtotime('now +'.$daysForward.' days'))."'
          GROUP BY DATE(dtm)
        ")->result();
        $current2 = array();
        foreach ($current as $key => $value) {
          $current2[$value->date] = $value->jumlah;
        }

        $quota = $this->db->query("
          SELECT tanggal, kuota
          FROM r_kuota_harian
          WHERE tanggal BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d', strtotime('now +'.$daysForward.' days'))."'
        ")->result();
        $quota2 = array();
        foreach ($quota as $key => $value) {
          $quota2[$value->tanggal] = $value->kuota;
        }

        $date_arr = array();
        for ($i=1; $i < $daysForward; $i++) {
          $date_now  = date('Y-m-d', strtotime('now +'.$i.' days'));
          $date_week = date('w', strtotime('now +'.$i.' days'));

          if ($date_week > 0 && $date_week < 6) {
            if ($quota2[$date_now] != -1) {
              if(isset($current2[$date_now])){
                if(isset($quota2[$date_now])) $date_arr[$date_now] = $quota2[$date_now] - $current2[$date_now];
                else $date_arr[$date_now] = $default_quota - $current2[$date_now];
              } else {
                if(isset($quota2[$date_now])) $date_arr[$date_now] = $quota2[$date_now];
                else $date_arr[$date_now] = $default_quota;
              }
            }
          }
        }

        $user_id = $this->session->userdata['id'];
        $current_request = $this->db->query("
          SELECT *
          FROM h_antrian_online
          WHERE user_id = '".$user_id."'
          AND dtm > '".date('Y-m-d')."'
        ")->row();

    ?>

    <style type="text/css">


 .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
}
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: rgb(24, 73, 129);
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #184981;
    border-color: #184981;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    z-index: 2;
    color: #fff;
    border: 1px solid #184981;
    background-color: #184981;
    border-color: none;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_bottom_cont_wrapper {
    float: left;
    width: 100%;
    background: #f9f9f9;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-bottom: 1px solid #184981;
    border-top: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
.mainmenu ul li a {

    padding: 0px 7px 47px 10px;
    }
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_cp_accor_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_cp_accor_heading_wrapper h2:before {
    content: '';
    border: 1px solid #184980;
    width: 8px;
    position: absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }

.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}

    .login_wrapper button.btn {
        color: #fff;
        width: 100%;
        height: 50px;
        padding: 6px 25px;
        line-height: 36px;
        margin-bottom: 20px;
        text-align: left;
        border-radius: 8px;
        background-color: #337AB8;
        font-size: 16px;
        border: 1px solid #f9f9f9;
        text-align: center;
        text-transform: uppercase;
    }

 .jp_cp_rd_wrapper li:first-child a {
    float: left;
    width: 100%;
    height: 50px;
    text-align: center;
    line-height: 50px;
    background: #184980;
    color: #ffffff;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    font-size: 18px;
    font-weight: 600;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -ms-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
}
.accordion_wrapper .panel .panel-heading a {
    display: block;
    background: #184980;
    padding: 12px 20px;
    color: #fff;
}
.abt_page_2_wrapper .panel .panel-heading a.collapsed {
    background: #184980 !important;
}

#tabelwarna1 { width: 567px; height:300px;}
td {
    font: bold 11px Trebuchet MS;
    border-right: 0px solid #C1DAD7;
    border-bottom: 0px solid #C1DAD7;
    border-left: 0px solid #C1DAD7;
    padding: 1px 1px 1px 1px ;
}
.style1 {
    font-size: 10px;
    font-weight: bold;
}

tr.size9 td{
    font-size: 9pt;
}

#customers {
    font: bold 11px Trebuchet MS;
    border-collapse: collapse;
    border: 1px solid #000;
}
#customers td, #customers tr {
    font-size: 1em;
    border: 1px solid #000;
    padding: 3px 7px 3px 7px;
}
.style3 {
    font-size: smaller;
    font-style: italic;
}

table.size9 tr td {
    font-size: 9pt;
}
</style>

<script type="text/javascript">
  var calendar;
  var slotAvailable;
  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    calendar = new FullCalendar.Calendar(calendarEl, {
      height: 400,
      width: "95%",
      initialView: 'dayGridMonth',
      selectable: true,
      selectAllow: function(info) {
        var now = new Date();
        now.setDate(now.getDate() + 1);
        var dd = String(now.getDate()).padStart(2, '0');
        var mm = String(now.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = now.getFullYear();
        now = yyyy + '-' + mm + '-' + dd;

        var later = new Date();
        later.setDate(later.getDate() + <?php echo $daysForward-1; ?>);
        dd = String(later.getDate()).padStart(2, '0');
        mm = String(later.getMonth() + 1).padStart(2, '0'); //January is 0!
        yyyy = later.getFullYear();
        later = yyyy + '-' + mm + '-' + dd;

        if (info.startStr < now || info.startStr > later) return false;
        else return true;
      },
      select: function(info) {
        var now = new Date(info.startStr);
        var dd = String(now.getDate()).padStart(2, '0');
        var mm = String(now.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = now.getFullYear();
        now = dd + '-' + mm + '-' + yyyy;

        if (confirm("Ajukan antrian untuk tanggal "+now+"?")) {

          var postData = {
            <?php echo $this->security->get_csrf_token_name(); ?> : "<?php echo $this->security->get_csrf_hash(); ?>",
            dtm : info.startStr
          };

          $.post("<?php echo base_url('portal/ajaxfile/setAntrianKuning'); ?>", postData, function( data ) {
            location.reload();
          });

        }
      },
      events: [
        <?php
          foreach ($date_arr as $key => $value) {
            echo "{
              title: 'Quota ".$value." Orang',
              start: '".$key."'
            },";
          }
        ?>
      ]
    });
  });

  function showRequest() {
    $('#modalRequest').modal('show');
    setTimeout(function(){ calendar.render(); }, 250);
  }
</script>

    <!-- Modal -->
    <div class="modal fade" id="modalRequest" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4><span class="glyphicon glyphicon-user"></span> Daftar Antrian Online</h4>
          </div>
          <div class="modal-body">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-lg-2">

                </div>
                <div class="col-lg-9 text-center">
                  <p>Silahkan pilih tanggal kedatangan ke Loket Disnaker. Sesuaikan dengan kuota yang tercantum. Loket tidak melayani pengunjung pada hari Sabtu dan Minggu. </p>
                </div>
              </div>
              <div class="form-group">
                <label for="bus_hour" class="col-lg-2 control-label">Pilih Tanggal</label>
                <div class="col-lg-9">
                  <div id='calendar'></div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button onclick="submitSelesai()" class="btn btn-primary">Simpan</button>
            <button class="btn btn-danger pull-left" data-dismiss="modal">Batalkan</button>
          </div>
        </div>
      </div>
    </div>

    <div class="jp_listing_single_main_wrapper" style="padding-bottom:0px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <div class="col-lg-6">
                              <h2 style="color:black !important;">Draft AK-1</h2>
                            </div>
                            <div class="col-lg-6">
                              <?php if (empty($current_request)): ?>
                                <button type="button" class="btn btn-primary btn-md pull-right" onclick="showRequest()">Request Pencetakan AK-1</button>
                              <?php else: ?>
                                <p class="text-center" style="font-size: 1.1em">
                                  Silahkan Datang ke Loket Disnaker pada <span class="label label-success"><?php echo date('d-M-Y', strtotime($current_request->dtm)); ?></span> lalu tuliskan nomor referensi <span class="label label-primary"><?php echo $current_request->no_ref; ?></span> pada monitor antrian
                                </p>
                              <?php endif; ?>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h4 style="color:black !important; padding-bottom:10px;">Halaman Depan</h4>
                        </div>
                        <div style="width:101%; overflow:auto;">
                          <table width="200" border="0" cellspacing="1" cellpadding="1" id="tabelwarna1" style="border:1px solid #000;">
                            <tr class="size9">
                              <td valign="top">
                                <table width="100%" cellpadding="1" cellspacing="2">
                                  <tr class="size9">
                                    <td colspan="3"><strong><u>PENDIDIKAN FORMAL</u></strong></td>
                                  </tr>
                                  <tr class="size9">
                                    <td width="120" valign="top"><?=strtoupper($item->pendidikan)?>/SEDERAJAT</td>
                                    <td width="200" valign="top"><?=strtoupper($item->jurusan)?></td><!--ini :-->
                                    <td width="100" valign="top" align="right">Th. <?=$item->tahun_lulus?></td><!--jurusannya :-->
                                  </tr>
                                  <tr>
                                    <td colspan="3" height="10px">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" style="vertical-align:middle;"><hr size="1" width="385" align="left" /></td>
                                  </tr>
                                  <tr class="size9">
                                    <td colspan="3"><strong><u>KETERAMPILAN</u></strong></td>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <?php
                                          if($item->skill != ""){
                                              $edu = explode(',', $item->skill);
                                              for ($i=0; $i < count($edu); $i++) {
                                                  echo '<p style="font-size=8px;">'.($i+1).'. '.strtoupper($edu[$i]).'</p>';
                                              }
                                          }else{
                                              for ($i=1; $i < 4; $i++) {
                                                  echo '<p style="font-size=8px;">'.$i.'. </p>';
                                              }
                                          }
                                      ?>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td>
                                <table width="150" cellpadding="1" cellspacing="2" border="0">
                                  <tr>
                                    <td width="142">
                                      <br /><br /><br />
                                      <br /><br />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:center; font-weight:bold;">
                                      <span style="text-align:center; font-weight:bold;font-size:8pt;">Penandatangan</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <br /><br /><br /><br /><br /><br />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:center;"><span style="text-align:center;font-size:7.5pt">Pejabat</span></td>
                                  </tr>
                                  <tr>
                                    <td style="border-top:1px solid #000;">
                                      <div align="center">
                                        <span style="text-align:center;font-size:7.5pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP. 0000 000 00000</span>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td>&nbsp;&nbsp;</td>
                              <td>
                                <table width="100%" cellpadding="1" cellspacing="1" id="tabelwarna1" border="0">
                                  <tr>
                                    <td colspan="23" align="right">
                                      <span class="style1">Kartu AK/I</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td rowspan="3">
                                      <img src="<?php echo base_url(); ?>/assets/img/logo-bogor.jpg" height="75" width="60" style="object-fit: contain;" />
                                    </td>
                                    <td colspan="22" style="text-align:center; font-weight:bold;font-size:11pt;vertical-align:middle;">
                                      PEMERINTAH <b>KABUPATEN BOGOR</b>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="22" style="text-align:center; font-weight:bold;font-size:10pt;vertical-align:middle;">
                                      <b>DINAS TENAGA KERJA</b>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="22" style="text-align:center; font-weight:bold;font-size:10pt;vertical-align:middle;">
                                      <b>Jl. Bersih No.02 Kompl. Pemda Cibinong , Kabupaten Bogor, Provinsi Jawa Barat 16913 Telp : 8757668</b>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="23" style="border:1px solid #333;text-align:center; font-weight:bold; height:20px; font-size:9pt;vertical-align:middle;">
                                      KARTU TANDA BUKTI PENDAFTARAN PENCARI KERJA
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="2" style="font-size:9pt">No. Pendaftaran Pencari Kerja </td>
                                    <td style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;" width="19">3</td>
                                    <td style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;" width="19">2</td>
                                    <td style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;" width="19">0</td>
                                    <td style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;" width="19">1</td>
                                    <td width="19">&nbsp;</td>
                                    <?php
                                      if($item->iduser != ""){
                                        $iduser = str_split($item->iduser);
                                        $jml = 7 - count($iduser);
                                        for ($i=0; $i < $jml; $i++) {
                                          echo '<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;">0</td>';
                                        }
                                        foreach ($iduser as $s) {
                                          echo '<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;">'.$s.'</td>';
                                        }
                                      }
                                    ?>
                                    <td width="19">&nbsp;</td>
                                    <?php
                                      if($item->bln != ""){
                                        $bln = str_split($item->bln);
                                        foreach ($bln as $s) {
                                          echo '<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;">'.$s.'</td>';
                                        }
                                      }
                                    ?>
                                    <?php
                                      if($item->thn != ""){
                                        $thn = str_split($item->thn);
                                        foreach ($thn as $s) {
                                          echo '<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;">'.$s.'</td>';
                                        }
                                      }
                                    ?>
                                  </tr>
                                  <tr>
                                    <td colspan="2" style="font-size:9pt">No. Induk Kependudukan</td>
                                    <?php
                                      $bulan = array (1 =>   'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
                                        'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
                                      );
                                      function tanggal_indo($tanggal)
                                      { $split = explode('-', $tanggal);
                                        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                                      }
                                      $counter = 0;
                                      if($item->no_ktp != ""){
                                        $no_ktp = str_split($item->no_ktp);
                                        foreach ($no_ktp as $s) {
                                          if($counter == 1 || $counter == 5 || $counter == 11){
                                            echo '<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;">'.$s.'</td><td width="19">&nbsp;</td>';
                                          }else{
                                            echo '<td width="19" style="border:1px solid #333;text-align:center; font-weight:bold;font-size:9pt;">'.$s.'</td>';
                                          }
                                          $counter++;
                                        }
                                      }
                                    ?>
                                  </tr>
                                  <tr class="size9">
                                    <td rowspan="5" width="92" style="border:1px dashed #333;"><img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_user.png')?>" width="100%" height="110" /></td>
                                    <td>&nbsp;Nama Lengkap</td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td colspan="18"><?=strtoupper($item->nama)?></td>
                                  </tr>
                                  <tr class="size9">
                                    <td>&nbsp;Tempat/Tgl.Lahir</td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td colspan="18"><?=strtoupper($item->tmp_lahir)?> / <?php
                                    echo strtoupper(tanggal_indo($item->tgl_lhr));
                                    ?></td>
                                  </tr>
                                  <tr class="size9">
                                    <td>&nbsp;Jenis Kelamin</td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td colspan="18"><?=strtoupper($item->gender)?></td>
                                  </tr>
                                  <tr class="size9">
                                    <td style="border-bottom:1px solid #fff;">&nbsp;Status</td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td colspan="18"><?=strtoupper($item->sts)?></td>
                                  </tr>
                                  <tr  class="size9">
                                    <td height="21">&nbsp;Agama</td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td colspan="18"><?=strtoupper($item->agama)?></td>
                                  </tr>
                                  <tr class="size9">
                                    <td rowspan="3" style="font-size:6pt;text-align:center; font-weight:bold;vertical-align:middle;">Tanda Tangan<br />Pencari Kerja</td>
                                    <td width="79">&nbsp;Alamat</td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td rowspan="2" colspan="18" valign="top"><?=strtoupper($item->alamat)?>,KECAMATAN <?=strtoupper($item->subdistrict_name)?>, <?=strtoupper($item->type)?> <?=strtoupper($item->city)?>, <?=strtoupper($item->province)?></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                  </tr>
                                  <tr class="size9">
                                    <td>&nbsp;Telp/Hp</td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td colspan="18"> <?=strtoupper($item->no_telp)?></td>
                                  </tr>
                                  <tr class="size9">
                                    <td rowspan="2"></td>
                                    <td width="79">&nbsp;Berlaku s.d </td>
                                    <td>&nbsp;&nbsp;:</td>
                                    <td colspan="18"><?php echo strtoupper(tanggal_indo($item->kadaluarsa)); ?></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h4 style="color:black !important; padding-bottom:10px;">Halaman Belakang</h4>
                        </div>
                        <div style="width:101%; overflow:auto;">
                          <table width="98%" style="border:1px solid #000;">
                            <tr>
                              <td width="1%">&nbsp;</td>
                              <td width="46%" valign="top">
                                <table width="100%" id="customers" class="size9">
                                  <tr>
                                    <td height="34" colspan="2" style="text-align:top; font-weight:bold;font-size:10pt;"><u>KETENTUAN :</u></td>
                                  </tr>
                                  <tr>
                                    <td width="5%" height="28" valign="top" style="text-align:top; font-weight:bold;font-size:10pt;">1.</td>
                                    <td width="95%" valign="top">BERLAKU NASIONAL</td>
                                  </tr>
                                  <tr>
                                    <td height="40" valign="top">2.</td>
                                    <td valign="top">BILA ADA PERUBAHAN DATA/KETERANGAN LAINNYA ATAU TELAH MENDAPAT PEKERJAAN HARAP SEGERA MELAPOR</td>
                                  </tr>
                                  <tr>
                                    <td height="53" valign="top">3.</td>
                                    <td valign="top">APABILA PENCARI KERJA YANG BERSANGKUTAN TELAH DITERIMA BEKERJA MAKA INSTANSI/PERUSAHAAN YANG MENERIMA AGAR MENGEMBALIKAN KARTU AK.I INI.</td>
                                  </tr>
                                  <tr>
                                    <td height="47" valign="top">4.</td>
                                    <td valign="top">KARTU INI BERLAKU SELAMA 2 TAHUN DENGAN KEHARUSAN KETENTUAN MELAPOR SETIAP 6 BULAN SEKALI BAGI PENCARI KERJA YANG BELUM MENDAPATKAN PEKERJAAN</td>
                                  </tr>
                                </table>
                              </td>
                              <td width="2%" valign="top"><br></td>
                              <td width="45%" valign="top">
                                <table width="100%" id="customers">
                                  <tr>
                                    <td width="18%" style="text-align:center;font-weight:bold;font-size:10pt;">LAPORAN<br></td>
                                    <td width="32%" style="text-align:center;font-weight:bold;font-size:10pt;">TGL - BULAN - TAHUN<br></td>
                                    <td width="50%" style="text-align:center;font-weight:bold;font-size:10pt;">Tanda Tangan Pengantar Kerja/<br> Pertugas Pendaftar <br> <span class="style3">(Cantumkan NIP)</span></td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:center;font-size:9pt;">PERTAMA</td>
                                    <td style="text-align:center;"><?=$item->bln1?></td>
                                    <td style="text-align:center;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:center;font-size:9pt;">KEDUA</td>
                                    <td style="text-align:center;"><?=$item->bln2?></td>
                                    <td style="text-align:center;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:center;font-size:9pt;">KETIGA</td>
                                    <td style="text-align:center;"><?=$item->bln3?></td>
                                    <td style="text-align:center;">&nbsp;</td>
                                  </tr>
                                </table>
                                <br>
                                <table width="100%" id="customers">
                                  <tr>
                                    <td width="50%" style="font-size:9pt;">DITERIMA DI</td>
                                    <td width="50%">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td style="font-size:9pt;">TERHITUNG MULAI TANGGAL</td>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp profile Wrapper Start -->
	<div class="jp_cp_profile_main_wrapper">
		<div class="container">
			<div class="row">

			</div>
		</div>
	</div>

	<?php } else { ?>

    <div class="error_page" style="padding-top: 200px !important; padding-bottom: 200px !important;">
      <div class="container">
          <div class="row">
           <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                  <div class="error_page_cntnt">
                      <h2>
                          <span>4</span>
                          <span>0</span>
                          <span>4</span>
                      </h2>
                      <h3>Sorry, This Page Isn't available :(</h3>
                      <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. A Aenean sollicitudin, lorem quis bibend Aenean sollicitudin, lorem . <a href="index.html">Home Page</a></p>
                     <!--  <div class="error_page_mail_wrapper">
                          <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a><input type="text" placeholder="Email Address *">
                      </div> -->
                  </div>
              </div>
          </div>
      </div>
  </div>

    <?php } ?>

    <script type="text/javascript">
    	$(function(){
    		$(document).on('click','.detail', function(){
    			var seo = $(this).prop('id');

    			window.location = '<?=base_url()?>portal/jobs/'+seo;
    		})
			$('a[href$="#Modal"]').on( "click", function() {
			$('#Modal').modal('show');
			});



    	})

		function myFunction() {
		window.print();
		}


    </script>
 <div id="placeholder-div"></div>
  <script type="text/javascript">
    window.___gcfg = {
      lang: 'id-ID'
    };
    (function() {
      var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
      po.src = 'https://apis.google.com/js/platform.js?onload=renderButtons';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    function renderButtons(){
      gapi.hangout.render('placeholder-div', {
          'render': 'createhangout',
        });
    }
  </script>
