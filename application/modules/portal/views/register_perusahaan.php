<style type="text/css">
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
}
    .login_wrapper button.btn {
        color: #fff;
        width: 100%;
        height: 50px;
        padding: 6px 25px;
        line-height: 36px;
        margin-bottom: 20px;
        text-align: left;
        border-radius: 8px;
        background-color: #337AB8;
        font-size: 16px;
        border: 1px solid #f9f9f9;
        text-align: center;
        text-transform: uppercase;
    }


    .login_wrapper button.btn {
    color: #fff;
    width: 100%;
    height: 50px;
    padding: 6px 25px;
    line-height: 36px;
    margin-bottom: 20px;
    text-align: left;
    border-radius: 8px;
    background-color: #184981;
    font-size: 16px;
    border: 1px solid #f9f9f9;
    text-align: center;
    text-transform: uppercase;
}
.register_tab_wrapper .register-tabs>li.active::after {
    content: "";
    position: absolute;
    left: 50%;
    bottom: -5px;
    margin-left: -10px;
    border-top: 5px solid #184981;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
}
.register_tab_wrapper .register-tabs>li.active>a, .register_tab_wrapper .register-tabs>li.active>a:focus, .register_tab_wrapper .register-tabs>li.active>a:hover {
    color: #fff;
    background-color: #184981;
    border: 0;
}
  .cek {
        border: 1px solid #f9f9f9;
        background: #f9f9f9;
        color: #6a7c98;
        font-size: 17px;
		vertical-alling: middle;
    }


.register_left_form input[type="text"], .register_left_form input[type="email"], .register_left_form input[type="password"], .register_left_form input[type="tel"], .register_left_form input[type="number"], .register_left_form input[type="url"], .register_left_form input, .register_left_form select, .register_left_form textarea {
	text-transform: none;
}

    .jp_pricing_cont_heading h2:after {
    content: '';
    border: 1px solid #184981;
    width: 30px;
    position: absolute;
    bottom: -15px;
    left: 11px;
}
.jp_pricing_cont_heading h2:before {
    content: '';
    border: 1px solid #184981;
    width: 8px;
    position: absolute;
    bottom: -15px;
    left: 0;
}
</style>
<script>


$(document).ready(function(){


  $(document).on('change', 'select[name="province_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikab',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kab_id"]').html(result);
                }
            })
        }).on('change', 'select[name="kab_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikec',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kecamatan_id"]').html(result);
                }
            })
        }).on('input', '#username', function(){
            $('#username_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();
            var regexp = /[A-Za-z0-9]{3,}$/

            if(value.match(regexp)){
            	$.ajax({
	                url: '<?=base_url()?>portal/page/checkusername_perusahaan',
	                type: 'post',
	                dataType: 'json',
	                data: {
	                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
	                    username: value
	                },
	                success: function(result){
	                    if(result.available){
	                        $('#username_check').html('<i class="fa fa-check"></i>');
	                        $('#regBtn').attr('disabled',false);
	                    }else{
	                        $('#username_check').html('<i class="fa fa-times"></i>');
	                        $('#regBtn').attr('disabled',true);
	                    }
	                }
	            });
            }else{
				$('#username_check').html('<i class="fa fa-times"></i>');
	            $('#regBtn').attr('disabled',true);
            }
        }).on('input', '#password_ori', function(){
			$('#password_check').html('<i class="fa fa-spinner fa-spin"></i>');

            if($(this).val() == $('#pass').val()){
				$('#password_check').html('<i class="fa fa-check"></i>');
                $('#regBtn').attr('disabled',false);
            }else{
				$('#password_check').html('<i class="fa fa-times"></i>');
                $('#regBtn').attr('disabled',true);
            }
        }).on('input', '#email', function(){
            $('#email_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();
            var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkemail',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        email: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#email_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#email_check').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#email_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        });

//alert('1');

});



</script>
 <div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Registrasi Perusahaan</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp register wrapper start -->
    <div class="register_section">
        <!-- register_form_wrapper -->
        <div class="register_tab_wrapper">
            <div class="container">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_pricing_cont_heading">
                        <h2>Additional Information :</h2>
                    </div>
                    <div class="jp_pricing_cont_wrapper">
                        <p>1. Akun ini akan digunakan untuk keperluan anda memberikan informasi lowongan pekerjaan, penempatan kerja .<br>
						2. Setelah pendaftaran Akun selesai anda harus datang ke disnaker untuk verifikasi data dengan membawa kelengkapan sbb :
						<br> &nbsp; - Cap Perusahaan
						<br> &nbsp; - Surat Permohonan Pengajuan Kebutuhan Tenaga Kerja Dari Perusahaan Yang Ditujukan Kepada Disnaker Kabupaten Bogor
						<br> &nbsp; - KTP Penanggung Jawab Perusahaan
						<br> &nbsp; - Fotocopy SIUP
						<br>3. Setelah verifikasi data selesai maka administrator disnaker akan mengaktifkan akun anda.</p>
						<br>
                    </div>
				</div>
                        <div role="tabpanel">

                            <!-- Nav tabs -->
                            <ul id="tabOne" class="nav register-tabs">
                                <li class="active"><a href="#contentOne-1" data-toggle="tab">Perusahaan<br> <span>Form Pendaftaran Akun Perusahaan</span></a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active register_left_form" id="contentOne-1">

                                    <div class="jp_regiter_top_heading">
                                        <p><b>AKUN LOGIN PERUSAHAAN</b></p>
                                    </div>
								 <!--<form action="<?=base_url()?>portal/page/regPerusahaan" method="POST" id="daftarperusahaan">-->
								 <form id="formperusahaan" action="<?=base_url()?>portal/page/regPerusahaan" method="post">
									<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
									<div class="row">
                                        <!--Form Group-->
										<div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <input type="text" name="email" id="email" placeholder="Email*" required="">
                                                <span class="input-group-addon cek" id="email_check"></span>
                                            </div>
                                        </div>
                                       <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <input type="text" name="username" id="username" placeholder="Username*" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka tanpa spasi, 3 - 10 karakter" required="">
                                                <span class="input-group-addon cek" id="username_check"></span>
                                            </div>
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="password" name="pass" id="pass" placeholder=" password*" required="">
                                        </div>
										 <div class="form-group col-md-6 col-sm-6 col-xs-12">
											<div class="input-group">
                                                <input type="password" name="password_ori" id="password_ori" placeholder="Konfirmasi password*" required="">
                                                <span class="input-group-addon cek" id="password_check"></span>
                                            </div>
                                        </div>
                                    </div>
									<div class="jp_regiter_top_heading">
                                        <p><b>DATA PERUSAHAAN</b></p>
                                    </div>
                                    <div class="row">
                                        <!--Form Group-->
										<div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="nama_perusahaan" id="nama_perusahaan" placeholder="Nama Perusahaan*" required="">
                                        </div>
										<div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="alamat" id="alamat" placeholder="Alamat Perusahaan*" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="telp" id="telp" placeholder="No. Telp*" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="noktpdir" id="noktpdir" placeholder="No. Ktp Penanggung Jawab*"  pattern="[0-9]{16,16}" title="No ktp harus berupa angka, 16 digit" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="nm_dir" id="nm_dir" placeholder="Nama Lengkap Penanggung Jawab*" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="no_hp" id="no_hp" placeholder="No Hp Penanggung Jawab*" required="">
                                        </div>
                                        <!--Form Group-->

										<div class="form-group col-md-4 col-sm-4 col-xs-12">
											<select name="lokasi" id="lokasi" required="">
												<option value="-">Pusat/Cabang*</option>
												<option value="Pusat">Pusat</option>
												<option value="Cabang">Cabang</option>
											</select>

                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
										<select name="jenisusaha" required="">
											<option value="">Jenis Usaha*</option>
                                    <?php
                                        $rs = $this->db->query("select * from mast_jenisusaha")->result();

                                        foreach ($rs as $item) {
                                            if(isset($_POST)){
                                                if($_POST['jenisusaha'] == $item->id){
                                                    echo '<option value="'.$item->id.'" selected="true">'.$item->nama.'</option>';
                                                }else{
                                                    echo '<option value="'.$item->id.'">'.$item->nama.'</option>';
                                                }
                                            }else{
                                                echo '<option value="'.$item->id.'">'.$item->nama.'</option>';
                                            }
                                        }
                                    ?>
										</select>
                                        </div>

                                  <div class="form-group col-md-4 col-sm-4 col-xs-12">
    										<select name="province_id" required="">
    											<option value="">Provinsi</option>
                                                <?php
                                                    $q = $this->db->query('select province, province_id from ngi_kecamatan group by province_id')->result();

                                                    foreach ($q as $i) {
                                                        echo '<option value="'.$i->province_id.'">'.$i->province.'</option>';
                                                    }

                                                ?>
    										</select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <select name="kab_id" required="">
                                                <option value="">Kabupaten / Kota</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <select name="kecamatan_id" required="">
                                                <option value="">Kecamatan / Kelurahan</option>
                                            </select>
                                        </div>

						</div><!--end of row-->
							</br>
									<div class="login_btn_wrapper register_btn_wrapper login_wrapper">
										<button class="btn btn-primary btn-lg btn-block" type="submit" id="regBtn">Buat akun</button>
									</div>
									</form>

                                    <div class="login_message">
                                        <p>Sudah Mempunyai Akun? <a href="<?=base_url()?>portal/login" style="color: #164980;"> Login Disini </a> </p>
                                    </div>

                                </div>



                            </div>
                            <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that you logout to prevent any un-authorized access to your account</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
