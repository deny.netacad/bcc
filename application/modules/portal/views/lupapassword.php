   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />
   <style type="text/css">
   	.gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
   </style>

<?php if($this->session->userdata('is_login_pelamar')){
	redirect(base_url().'portal');
}else if($this->session->userdata('is_login_perusahaan')){
	redirect(base_url().'perusahaan');
}else{ ?>


<div class="login_section">
		<!-- login_form_wrapper -->
		<div class="login_form_wrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<!-- login_wrapper -->
						<h1>Reset password</h1>
							<form id="formlogin" action="<?=base_url()?>portal/page/resetpassword" method="post">
								<p>Masukkan username/email akun yang akan di reset<p>
								<div class="login_wrapper">
									<p color="red" id="message"></p>
									<!-- <div class="row">
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
											<a href="#" class="btn btn-primary"> <span>Login with Facebook</span> <i class="fa fa-facebook"></i> </a>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
											<a href="#" class="btn btn-primary google-plus"> Login  with Google <i class="fa fa-google-plus"></i> </a>
										</div>
									</div>
									<h2>or</h2> -->
									<div class="formsix-pos">
										<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
										<div class="form-group i-email">
											<input type="email" class="form-control" name="username" id="email2" placeholder="Email/Username*" required />
										</div>
									</div>
									
									<div class="login_btn_wrapper">
										<a class="btn btn-primary login_btn" id="login"> Reset</a>
									</div>
									<div class="login_message">
										<p>Sudah ingat passwordnya ? <a href="<?=base_url()?>portal/login"> Login disini </a> </p>
									</div>
								</div>
							</form>
						<!-- /.login_wrapper-->
					</div>
				</div>
			</div>
		</div>
		<!-- /.login_form_wrapper-->
	</div>

	<script type="text/javascript">
		$(function(){
     		$('#email2').focus();

			$('#login').click(function(){
				$('#login').attr('disabled',true);
				$('#login').html('Loading....');
				$('#formlogin').submit();
			});

			$(document).on('submit','#formlogin', function(e){
				$.ajax({
					url : $(this).prop('action'),
					data : $(this).serialize(),
					dataType: 'json',
					method : 'post',
					success: function(rs){
						if(rs.success){
							alert("Password berhasil direset, password baru telah dikirim ke email "+rs.email+"");
							window.location = '<?=base_url()?>portal/login';
						}else{
							alert(rs.teks);
							$('#login').attr('disabled',false);
							$('#login').html('login');
						}
					},
					error: function(e){
						alert('Terjadi error pada server. Silahkan coba lagi');
						$('#login').attr('disabled',false);
						$('#login').html('login');
					}
				});

				e.preventDefault();
				// $('#login').attr('disable',false);
				// $('#login').html('Loading....');
			})
		})

		function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }

	</script>

<?php } ?>