<style media="screen">
  .jp_tittle_img_overlay {
     position: absolute;
     top: 0%;
     right: 0%;
     left: 0%;
     bottom: 0%;
     background: #184981;
     padding-bottom: 420px;
  }
</style>

<script>
 $(".share-popup").click(function(){
    var window_size = "width=585,height=511";
    var url = this.href;
    var domain = url.split("/")[2];
    switch(domain) {
      case "www.facebook.com":
          window_size = "width=585,height=368";
          break;
      case "www.twitter.com":
          window_size = "width=585,height=261";
          break;
      case "plus.google.com":
          window_size = "width=517,height=511";
          break;
    }
    window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,' + window_size);
    return false;
});


</script>
<div class="jp_tittle_main_wrapper">
  <div class="jp_tittle_img_overlay"></div>
  <div class="container">
    <div class="row">
			<?php
                     $rs = $this->db->query("SELECT a.*,b.username
					FROM section_article a
					LEFT JOIN mast_user b ON a.iduser=b.iduser
					WHERE a.menu=1");
                     $item = $rs->row();
                     if($rs->num_rows()!=0){
                          if($item->art_thumb!=""){
                      $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                      $src = $xpath->evaluate("string(//img/@src)");
                      $src=str_replace('.thumbs/','',$src);
                     }

                     ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2><?=$item->art_title?></h2>
                        </div>
                    </div>
                </div>

					 <?php }?>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp blog_cate section Wrapper Start -->
    <div class="jp_blog_cate_main_wrapper" style="margin-top: -130px; background: #FFF;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
					  <?php
                     $rs = $this->db->query("SELECT a.*,b.username
	FROM section_article a
	LEFT JOIN mast_user b ON a.iduser=b.iduser
	WHERE a.menu=1 ");
                     $item = $rs->row();
                     if($rs->num_rows()!=0){
                          if($item->art_thumb!=""){
                      $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                      $src = $xpath->evaluate("string(//img/@src)");
                      $src=str_replace('.thumbs/','',$src);
                     }

                     ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_blog_cate_left_main_wrapper">
                                <div class="jp_first_blog_post_main_wrapper">
								<?php
                            if($item->art_thumb!=""){
                            ?>
                                    <div class="jp_first_blog_post_img">
                                        <img src="<?=$src?>" class="img-responsive" alt="<?=$item->art_title?>" />
                                    </div>

							     <?php }?>

                                    <div class="jp_first_blog_post_cont_wrapper" style="border-radius: 25px; box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.15);">
                                        <!--<ul>
                                            <li><a href="#"><i class="fa fa-calendar"></i> &nbsp;&nbsp;<?=$item->tmstamp_?></a></li>
                                            <li><a href="#"><i class="fa fa-user"></i> &nbsp;&nbsp;<?=$item->nama?></a></li>
                                        </ul>-->
                                        <h3><a href="#"><?=$item->art_title?></a></h3>
                                        <p><?=$item->art_content?></p>
                                    </div>



                                </div>
                            </div>
                        </div>

					 <?php }?>

                    </div>
                </div>

            </div>
        </div>
    </div>
<script src="<?=base_url()?>template/HTML/job_dark/js/jquery.magnific-popup.js"></script>
<script src="<?=base_url()?>template/HTML/job_dark/js/custom_II.js"></script>
