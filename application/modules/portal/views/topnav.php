<style media="screen">

  .gc_main_menu_wrapper {
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    background-color: white;
    box-shadow: 0px 2px 10px rgba(24, 73, 129, 0.3);
  }

  .main-nav {
    color: #184981 !important;
    background: #D8D8D8 !important;
    border-radius: 5px;
    font-family: 'Poppins', sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 21px;
    transition: 0.5s !important;
  } .main-nav:hover {
    color: white !important;
    background: #184981 !important;
  }

  .mainmenu {
    position: inherit !important;
    float: right !important;
    margin: 0px !important;
  	padding: 0px !important;
	}
  .mainmenu ul li a {
  	padding: 5px 15px;
	}
  .main-nav i {
    padding-top: 3px;
    float: right;
    font-weight: bold;
  }

  .parent-nav {
    width: 190px !important;
    box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25);
    background: #FFFDFD !important;
    border-radius: 7px !important;
    border-top: none !important;
  }
  .child-nav-a {
    color: black !important;
    padding: 5px !important;
    margin: 0px 0px;
    border-radius: 5px !important;
    border-bottom: 1px dashed #919191 !important;
  }

  .dropbtn {
    background: #FFFFFF;
    border: 1px solid #184981;
    box-sizing: border-box;
    border-radius: 0px 0px 20px 20px;
    width: 120px;
    height: 31px;
    transition: 0.4s;
    margin-right: 10px;
  }

  .dropdown-content {
    visibility: hidden;
    position: absolute;
    background-color: #FFF;
    border-radius: 10px;
    width: 120px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    margin-top: 20px;
    opacity: 0;
    transition: 0.4s !important;
  } .dropdown:hover .dropdown-content {
    visibility: visible;
    opacity: 1;
    margin-top: 0px;
  }
  .dropdown-content a {
    border-radius: 10px;
    color: black;
    padding: 5px 10px;
    text-decoration: none;
    display: block;
  } .dropdown-content a:hover {
    background-color: #ddd;
  }

  #login-btn {
    background: #2C77CD;
    border-radius: 0px 0px 20px 20px;
    border: none;
    color: white;
    height: 30px;
    width: 100px;
    transition: 0.5s;
    z-index: 13;
  } #login-btn:hover {
    padding-bottom: 30px;
    padding-top: 25px;
  }

  #profile-icon {
    background: #2C77CD;
    border-radius: 0px 0px 15px 15px;
    border: none;
    color: white;
    height: 30px;
    padding: 0px 15px;
    font-size: 25px;
    z-index: 13;
    margin-right: 10px;
  }

  #profile-btn {
    border: none;
    color: white;
    height: 30px;
    transition: 0.5s;
    z-index: 13;
  }

  .profile-btn-content {
    visibility: collapse;
    position: absolute;
    right: 25px;
    width: 170px;
    background-color: #FFF;
    border-radius: 10px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    margin-top: 55px;
    opacity: 0;
    transition: 0.4s !important;
  } #profile-btn:hover .profile-btn-content {
    visibility: visible;
    opacity: 1;
    margin-top: 32px;
  }

  #profile-on-menu:hover .profile-btn-content {
    visibility: visible;
    opacity: 1;
    margin-top: 0px;
  }

  .profile-btn-content li a {
    padding: 10px;
    display: block;
    color: #2C77CD;
    border-bottom: 1px dashed black;
    border-radius: 10px;
    transition: 0.5s;
  } .profile-btn-content li a:hover {
    background: #EAEAEA;
  }

  .logout-btn {
    background: #2C77CD !important;
    color: white !important;
    border-radius: 15px !important;
    border: none !important;
    text-align: center !important;
    padding: 5px 15px !important;
    display: inline-block !important;
    margin: 5px 0px !important;
    transition: 0.4s !important;
  } .logout-btn:hover {
    padding: 5px 25px !important;
  }

  #img-logo {
    height: 40px;
    margin-top: 15px;
    margin-bottom: 15px;
    transition: 0.2s;
  }
  #btn-right {
    padding-top: 35px;
    height: 20px;
    transition: 0.2s;
  }
  #menu-top {
    padding-top: 20px;
    transition: 0.2s;
  }

  #menu-fixed {
    position: fixed;
    top: 35px;
    width: 100%;
    transition: 0.2s;
    background-color: white;
    z-index: 11;
  }
  #menu-fixed-top {
    position: fixed;
    top: 0;
    width: 100%;
    transition: 0.2s;
    height: 35px;
    background-color: #184981;
    z-index: 12;
  }
</style>

<script>
  window.onscroll = function() {scrollFunction()};

  function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
      document.getElementById("menu-fixed-top").style.top  = "-35px";
      document.getElementById("menu-fixed").style.top  = "0px";
      document.getElementById("profile-on-menu").style.visibility  = "visible";
      document.getElementById("profile-on-menu").style.opacity  = "1";
    } else {
      document.getElementById("menu-fixed-top").style.top  = "0px";
      document.getElementById("menu-fixed").style.top  = "35px";
      document.getElementById("profile-on-menu").style.visibility  = "collapse";
      document.getElementById("profile-on-menu").style.opacity  = "0";
    }
  }
</script>
<div id="menu-fixed-top">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php if (!$this->session->userdata('is_login_pelamar')): ?>
          <a href="<?=base_url()?>portal/login" id="login-btn" class="pull-right text-center">
            <span> Login </span>
          </a>
          <ul class="dropdown pull-right">
            <button class="dropbtn">Daftar <i class="fa fa-angle-down"></i></button>
            <div class="dropdown-content">
              <a href="<?=base_url()?>portal/page/register">Pencaker</a>
              <a href="<?=base_url()?>portal/page/register_perusahaan">Perusahaan</a>
            </div>
          </ul>
        <?php else: ?>
          <ul id="profile-btn" class="pull-right">
            <a href="<?=base_url()?>portal/login" class="pull-right text-center">
              <span id="profile-icon" class="text-center">
                <span class="fa fa-user"></span>
              </span>
              <span style="color: white; margin-right: 10px; padding-top:2px;" class="pull-right"> <?=$this->session->userdata('nama')?> </span>
            </a>
            <ul class="profile-btn-content">
              <li>
                <a href="<?=base_url()?>portal/profile" style="background: #D0E8FF; border-radius: 10px; border: none;">
                  <i style="padding: 6px; background: #2C77CD; color: white; border-radius: 5px;" class="fa fa-user"></i>
                  <span style="color: #2C77CD;"><?=$this->session->userdata('nama')?></span>
                </a>
              </li>
              <li><a href="<?=base_url()?>portal/profile"><i class="fa fa-user" style="color: #00AA58;"></i> &nbsp;&nbsp;Profil Saya</a></li>
              <li><a href="<?=base_url()?>portal/pengaturan"><i class="fa fa-cog" style="color: #00AA58;"></i> &nbsp;Pengaturan</a></li>
              <li style="text-align: center;">
                <a class="logout-btn" href="<?=base_url()?>portal/page/logoutportal">Keluar</a>
              </li>
            </ul>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div id="menu-fixed">
  <div class="gc_main_menu_wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <div id="img-logo">
            <img src="<?php echo base_url('/assets/img/logo-bogor.jpg'); ?>" class="pull-left hidden-md hidden-sm hidden-xs" style="height: 100%; padding-right: 10px; border-right: 3px solid #184981;">
            <a href="<?=base_url()?>"><img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="padding-left: 5px; height:95%;" title="Bogor Career Center" class="img-responsive"></a>
          </div>
        </div>

        <?php
          if($this->session->userdata('is_login_pelamar')){
            $grid = 'col-lg-8 col-md-8';

            $profil = $this->db->query('select skill, ttg_anda, exp, photo from user_pelamar where iduser ="'.$this->session->userdata('id').'"')->result_array();
            $last_edu = $this->db->query('select jurusan, tahun_lulus from user_pelamar where iduser ="'.$this->session->userdata('id').'"')->row();

            $counter = 0;
            foreach ($profil[0] as $key => $value) {
              if($value == ""){
                $counter++;
              }
            }
            if($last_edu->jurusan == "" || $last_edu->tahun_lulus == ""){
              $counter++;
            }
          }else{
            $grid = 'col-lg-8 col-md-8';
          }
        ?>

        <!-- MAIN MENU -->
        <div class="<?=$grid?> col-sm-12 col-xs-12">
          <div id="menu-top" class="header-area hidden-menu-bar stick">
            <!-- mainmenu start -->
            <div class="mainmenu">
              <ul class="pull-left">
                <li><a href="<?=base_url()?>" class="main-nav">Beranda&nbsp;</a></li>
                <li><a href="<?=base_url()?>portal/page/caripekerjaan" class="main-nav">Lowongan Kerja&nbsp;</i></a></li>
                <li><a href="<?=base_url()?>../pelatihan" class="main-nav">Informasi Pelatihan &nbsp; </i></a></li>
                <li><a href="<?=base_url()?>portal/page/informasi" class="main-nav">Berita & Event &nbsp; </i></a></li>
                <li><a href="#" class="main-nav" style="width: 190px;"> Tentang Disnaker&nbsp;<i class="fa fa-angle-down"></i></a>
                  <ul class="parent-nav">
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/disnaker">Profil Disnaker</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/disnaker#kadis">Profil Kepala Dinas</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/disnaker#visimisi">Visi dan Misi</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/disnaker#strategi">Strategi</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/disnaker#tupoksi">Tupoksi</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/disnaker#struktur">Struktur Organisasi</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/disnaker#publikasi">Dokumen Publikasi</a></li>
                  </ul>
                </li>
                <!-- <li><a href="#" class="main-nav hidden-md hidden-sm hidden-xs" style="width: 190px;"> Informasi Website&nbsp;<i class="fa fa-angle-down"></i></a>
                  <ul class="parent-nav">
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/tentang">Tentang</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>portal/page/kontak">Kontak Kami</a></li>
                    <li class="child-nav"><a class="child-nav-a" href="<?=base_url()?>../pelatihan" target="_blank">Informasi Pelatihan</a></li>
                  </ul>
                </li> -->
              </ul>
            </div>
            <!-- mainmenu end -->

            <!-- mobile menu area start -->
            <header class="mobail_menu">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-xs-6 col-sm-6">
                    <div class="gc_logo">
                      <a href="<?=base_url()?>"><img src="<?=base_url()?>assets/logo/logo_bcc_new.png" style="width:110px;height:55px;" class="center" alt="Logo" title="Grace Church"></a>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6">
                    <div class="cd-dropdown-wrapper">
                      <a class="house_toggle" href="#0">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.177 31.177" style="enable-background:new 0 0 31.177 31.177;" xml:space="preserve" width="25px" height="25px">
                          <g> <path class="menubar" d="M30.23,1.775H0.946c-0.489,0-0.887-0.398-0.887-0.888S0.457,0,0.946,0H30.23    c0.49,0,0.888,0.398,0.888,0.888S30.72,1.775,30.23,1.775z" fill="#184981"/> </g>
                          <g> <path class="menubar" d="M30.23,9.126H12.069c-0.49,0-0.888-0.398-0.888-0.888c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,8.729,30.72,9.126,30.23,9.126z" fill="#184981"/> </g>
                          <g> <path class="menubar" d="M30.23,16.477H0.946c-0.489,0-0.887-0.398-0.887-0.888c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,16.079,30.72,16.477,30.23,16.477z" fill="#184981"/> </g>
                          <g> <path class="menubar" d="M30.23,23.826H12.069c-0.49,0-0.888-0.396-0.888-0.887c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,23.43,30.72,23.826,30.23,23.826z" fill="#184981"/> </g>
                          <g> <path class="menubar" d="M30.23,31.177H0.946c-0.489,0-0.887-0.396-0.887-0.887c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.398,0.888,0.888C31.118,30.78,30.72,31.177,30.23,31.177z" fill="#184981"/> </g>
                        </svg>
                      </a>
                      <nav class="cd-dropdown">
                        <h2><a href="#">BCC</a></h2>
                        <a href="#0" class="cd-close">Close</a>
                        <ul class="cd-dropdown-content">
                          <li class="">
                            <a href="<?=base_url()?>">Beranda</a>
                          </li>
                          <li class="has-children">
                            <a href="#">Disnaker</a>
                            <ul class="cd-secondary-dropdown is-hidden">
                              <li class="go-back"><a href="#0">Back</a></li>
                              <li class=""><a href="<?=base_url()?>portal/page/disnaker">Profil</a></li>
                              <li class=""><a href="<?=base_url()?>portal/page/disnaker#visimisi">Visi dan Misi</a></li>
                              <li class=""><a href="<?=base_url()?>portal/page/disnaker#strategi">Strategi</a></li>
                              <li class=""><a href="<?=base_url()?>portal/page/disnaker#tupoksi">Tupoksi</a></li>
                              <li class=""><a href="<?=base_url()?>portal/page/disnaker#struktur">Struktur Organisasi</a></li>
                              <li class=""><a href="<?=base_url()?>portal/page/disnaker#publikasi">Dokumen Publikasi</a></li>
                            </ul>
                          </li>
                          <li>
                            <a href="<?=base_url()?>portal/page/informasi">Berita & Event</a>
                          </li>
                          <li>
                            <a href="<?=base_url()?>portal/page/caripekerjaan">Cari Lowongan Kerja</a>
                          </li>
                          <li class="has-children">
                            <a href="#">Informasi</a>
                            <ul class="cd-secondary-dropdown is-hidden">
                              <li class="go-back"><a href="#0">Back</a></li>
                              <li> <a href="<?=base_url()?>portal/page/tentang">Tentang</a> </li>
                              <li> <a href="<?=base_url()?>portal/page/kontak">Kontak Kami</a> </li>
                              <li> <a href="<?=base_url()?>../pelatihan" target="_blank">Informasi Pelatihan</a> </li>
                            </ul>
                          </li>
                        <?php if ($this->session->userdata('is_login_pelamar')): ?>
                          <li class="has-children">
                            <a href="#"><?=$this->session->userdata('nama')?> &nbsp;<?=(($counter>0)? '' : '')?></a>
                            <ul class="cd-secondary-dropdown is-hidden">
                              <li class="go-back"><a href="#0">Menu</a></li>
                              <li><a href="<?=base_url()?>portal/profile">Profile</a></li>
                              <li><a href="<?=base_url()?>portal/lamaran">Lamaran</a></li>
                              <li><a href="<?=base_url()?>portal/kartukuning">Draft Kartu AK-1</a></li>
                              <li><a href="<?=base_url()?>portal/pengaturan">Pengaturan&nbsp;<?=(($counter > 0)? '<span class="badge btn-danger">'.$counter.'</span>' : '')?></a></li>
                              <li><a href="<?=base_url()?>portal/page/logoutportal">Logout</a></li>
                            </ul>
                          </li>
                        <?php else: ?>
                          <li class="has-children">
                            <a href="#">DAFTAR</a>
                            <ul class="cd-secondary-dropdown is-hidden">
                              <li class="go-back"><a href="#0">Back</a></li>
                              <li><a href="<?=base_url()?>portal/page/register">Daftar Pencaker</a></li>
                              <li><a href="<?=base_url()?>portal/page/register_perusahaan">Daftar Perusahaan</a></li>
                            </ul>
                          </li>
                          <li> <a href="<?=base_url()?>portal/login">LOGIN</a> </li>
                        <?php endif; ?>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </header>
          </div>
        </div>
        <!-- mobile menu area end -->
        <?php if ($this->session->userdata('is_login_pelamar')): ?>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
            <ul id="profile-on-menu" style="margin-top: 18px; transition: 0.4s !important; visibility: hidden; opacity: 0;">
              <a href="#" style="display: block; background: #184981; border-radius: 5px; color: white; padding: 5px;">
                <span><?=$this->session->userdata('nama')?> &nbsp; <i class="fa fa-angle-down" style="color: white;"></i></span>
              </a>
              <ul class="profile-btn-content text-left">
                <li>
                  <a href="<?=base_url()?>portal/profile" style="background: #D0E8FF; border-radius: 10px; border: none;">
                    <i style="padding: 6px; background: #2C77CD; color: white; border-radius: 5px;" class="fa fa-user"></i>
                    <span style="color: #2C77CD;"><?=$this->session->userdata('nama')?></span>
                  </a>
                </li>
                <li><a href="<?=base_url()?>portal/profile"><i class="fa fa-user" style="color: #00AA58;"></i> &nbsp;&nbsp;Profil Saya</a></li>
                <li><a href="<?=base_url()?>portal/pengaturan"><i class="fa fa-cog" style="color: #00AA58;"></i> &nbsp;Pengaturan</a></li>
                <li style="text-align: center;">
                  <a class="logout-btn" href="<?=base_url()?>portal/page/logoutportal">Keluar</a>
                </li>
              </ul>
            </ul>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div style="margin-top: 100px"></div>
