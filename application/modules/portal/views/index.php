<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Bogor Career Center</title>
  <?php
    $rs = $this->db->query("select * from ref_seo");
    $seo = $rs->row();
  ?>
  <meta content="width=device-width, initial-scale=1.0" name="<?=$seo->title?>" />
  <meta name="description" content="<?=$seo->desc?>" />
  <meta name="keywords" content="<?=$seo->keyword?> " />
  <meta name="author" content="<?=$seo->title?>" />
  <meta name="MobileOptimized" content="320" />

  <link rel="shortcut icon" type="<?=base_url()?>template/HTML/job_dark/image/png" href="<?=base_url()?>template/HTML/job_dark/images/header/favicon.ico" />

  <!-- Jquery First -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!--srart theme style -->
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/animate.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/font-awesome.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/fonts.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/reset.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/owl.carousel.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/owl.theme.default.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/flaticon.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive.css" />
	<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/js/plugins/select2/select2.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/datatables/dataTables.bootstrap.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/magnific-popup.css" />


  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

</head>
<body class="telo">
  <div class="jp_top_header_img_wrapper" style="border-top: 3px solid #184981;">
    <?php
      $this->load->view($this->modules.'/topnav');
    ?>
    <?php
      if(!isset($page) || $page == "caripekerjaan"){
        $this->load->view($this->modules.'/header');
      }
    ?>
  </div>
    <?php
      if(!isset($page)){
        $this->load->view($this->modules.'/default');
        $this->load->view($this->modules.'/footer');
      }else{
        $this->load->view($this->modules.'/'.$page);
      }
    ?>

  <div>
    <img src="<?php echo base_url(); ?>assets/img/tugu_pancakarsa_upd.png" alt="" style="width:100%; margin-bottom: -5px;">
  </div>
  <div class="jp_main_footer_img_wrapper" style="background: url('<?php echo base_url('/assets/img/footer_1.png'); ?>'); background-size: 100% 100%; background-repeat: no-repeat;">
    <div class="jp_newsletter_img_overlay_wrapper" style="background: #0e1427bf"></div>
    <div class="jp_footer_main_wrapper" style="padding: 50px 0px;">
      <div class="container">
        <?php
					$rs = $this->db->query("select * from section_contact ");
					$item = $rs->row();
				?>
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 20px;">
            <center>
              <table border="0">
                <tr>
                  <td>
                    <img src="<?php echo base_url('/assets/img/logo_bogor.png'); ?>" class="pull-right" style="width: 70px; margin-right: 10px">
                  </td>
                  <td>
                    <p style="font-weight: 600; font-size: 1.5em; color: white; text-transform: uppercase;"><?php echo $item->office_name; ?></p>
                    <p style="font-weight: 600; font-size: 1.5em; color: white; text-transform: uppercase;"><?php echo $item->city; ?></p>
                  </td>
                </tr>
              </table>
            </center>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="color: white; font-size: 1.2em; margin-top: 20px; border-left: 2px solid white; border-right: 2px solid white;">
            <table border="0">
              <tr>
                <td></td>
                <td style="padding: 5px; font-weight: bold;">Alamat Kami</td>
              </tr>
              <tr valign="top">
                <td style="padding: 5px;"><i class="fa fa-map-marker"></i></td>
                <td style="padding: 5px;"><?php echo $item->addr.', '.$item->postcode; ?><br><br></td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="color: white; font-size: 1.2em; margin-top: 20px;">
            <table border="0">
              <tr>
                <td></td>
                <td style="padding: 5px; font-weight: bold;">Kontak Kami</td>
              </tr>
              <tr valign="top" style="font-size: 0.9em;">
                <td style="padding: 2px;"><i class="fa fa-phone"></i></td>
                <td style="padding: 2px;"><?php echo $item->phone; ?></td>
              </tr>
              <tr valign="top" style="font-size: 0.9em;">
                <td style="padding: 2px;"><i class="fa fa-phone"></i></td>
                <td style="padding: 2px;"><?php echo $item->fax; ?></td>
              </tr>
              <tr valign="top" style="font-size: 0.9em;">
                <td style="padding: 2px;"><i class="fa fa-envelope"></i></td>
                <td style="padding: 2px;"><?php echo $item->email; ?></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="row" style="margin-top: 40px;">
          <div class="col-lg-2 col-md-2"> &nbsp; </div>
          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4" style="height: 70px; display: flex; justify-content: center;">
            <a href="<?=$item->twitter?>" style="margin-top: 8px; position: absolute; padding:15px 18px; background: white; border-radius: 50%;"><i class="fa fa-twitter" style="font-size: 1.2em;"></i></a>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center" style="height: 70px; display: flex; justify-content: center;">
            <a href="<?=$item->instagram?>" style="margin-top: 8px; position: absolute; padding:15px 18px; background: white; border-radius: 50%;"><i class="fa fa-instagram" style="font-size: 1.2em;"></i></a>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center" style="height: 70px; display: flex; justify-content: center;">
            <a href="<?=$item->fb?>" style="margin-top: 8px; position: absolute; padding:15px 20px; background: white; border-radius: 50%;"><i class="fa fa-facebook" style="font-size: 1.2em;"></i></a>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center" style="height: 70px; ">
            <a href="<?=$item->playstore?>"><img style="height: 70px;" src="<?php echo base_url('/assets/img/google-play-badge 1.png'); ?>" alt=""> </a>
          </div>
          <div class="col-lg-2 col-md-2"> &nbsp; </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
            <div class="jp_bottom_footer_left_cont">
              <center>
                <p style="color: white;">© <?php echo date('Y'); ?> <?php echo $item->office_name.' '.$item->city; ?>. All Rights Reserved.</p>
              </center>
            </div>
            <div class="jp_bottom_top_scrollbar_wrapper pull-right">
              <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- jp footer Wrapper End -->

  <!--main js file start-->
  <script src="<?=base_url()?>template/HTML/job_light/js/bootstrap.js"></script>
  <script src="<?=base_url()?>template/HTML/job_light/js/jquery.menu-aim.js"></script>
  <script src="<?=base_url()?>template/HTML/job_light/js/jquery.countTo.js"></script>
  <script src="<?=base_url()?>template/HTML/job_light/js/jquery.inview.min.js"></script>
  <script src="<?=base_url()?>template/HTML/job_light/js/owl.carousel.js"></script>
  <script src="<?=base_url()?>template/HTML/job_light/js/modernizr.js"></script>
  <script src="<?=base_url()?>template/HTML/job_light/js/custom.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/plugins/select2/select2.js"></script>
  <script src="<?php echo base_url('/assets/js/plugins/datatables/jquery.dataTables.js') ?>" charset="utf-8"></script>
  <!--main js file end-->
</body>
</html>
