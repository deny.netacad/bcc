<style type="text/css">
  .owl-nav {
    display: none;
  }
  .owl-dots {
    margin-top: 10px !important;
  }
</style>

<div class="jp_listing_sidebar_main_wrapper" style="background: #FFF">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      	<?php
          $where = "";
          $wj = "";
          $wc = "";
          $ws = "";
          $wt = "";
          $sorting = ($_POST['sorting'] == '')? 'a.log desc' : $_POST['sorting'];

      		if ($_POST["city"] != ""){
            $where .= " and d.city_id =  '".$_POST["city"]."'";
          }else{
            if($_POST["province"] != ""){
              $where .= " and d.province_id =  '".$_POST["province"]."'";
            }
          }

      		if ($_POST["keyword"] != ""){
            $where .= " and (a.keyword like  '%".$_POST["keyword"]."%' or c.nama_job like '%".$_POST["keyword"]."%' or a.skill like '%".$_POST["keyword"]."%' or a.title like '%".$_POST["keyword"]."%' )";
            $this->db->insert('ngi_keyword',array('keyword' => $_POST['keyword']));
          }

          if ($_POST["jobs"] != ""){
            $jobs = explode(",", $_POST["jobs"]);
            $wj .= " and ( ";
              foreach ($jobs as $item) {
                $wj .= "a.posisi = '".trim($item,'j')."' or ";
              }
            $wj = substr($wj, 0, -3);
            $wj .= " ) ";
          }

          if ($_POST["category"] != ""){
            $ctgr = explode(",", $_POST["category"]);
            $wc .= " and ( ";
              foreach ($ctgr as $item) {
                $wc .= "a.idcategory = '".trim($item,'c')."' or ";
              }
            $wc = substr($wc, 0, -3);
            $wc .= " ) ";
          }

          if ($_POST["salary"] != ""){
            $slr = explode(",", $_POST["salary"]);
            $ws .= " and ( ";
              foreach ($slr as $item) {
                $ws .= " ( a.upah_kerja = ".trim($item,'s')." ) or ";
              }
            $ws = substr($ws, 0, -3);
            $ws .= " ) ";
          }

          if ($_POST["type"] != ""){
            $tp = explode(",", $_POST["type"]);
            $wt .= " and ( ";
              foreach ($tp as $item) {
                $wt .= "a.tipe = '".$item."' or ";
              }
            $wt = substr($wt, 0, -3);
            $wt .= " ) ";
          }

          $pages = ($_POST['page'] == '')? 1 : $_POST['page'];
          $page = ($pages-1)*10;

      		$q1 = $this->db->query("
            SELECT
              a.*,
              b.nama_perusahaan,
              b.photo,
              c.nama_job,
              d.province, d.type, d.city, d.subdistrict_name,
              f.name as upah
            FROM ngi_joblist a
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_job c on a.posisi = c.id
              LEFT JOIN ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
              LEFT JOIN ngi_jobcategory e on a.idcategory = e.id
              LEFT JOIN ngi_salary f on a.upah_kerja = f.id
            WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ".$wj." ".$wc." ".$ws." ".$wt."
            ORDER BY ".$sorting." LIMIT ".$page.", 10
          ");

          $q2 = $this->db->query("
            SELECT a.id
            FROM ngi_joblist a
              LEFT JOIN ngi_perusahaan b on a.idperusahaan = b.idperusahaan
              LEFT JOIN ngi_job c on a.posisi = c.id
              LEFT JOIN ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
            WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ".$wj." ".$wc." ".$ws." ".$wt."
          ");

      		$joblist = $q1->result();
      		$jml = $q2->num_rows();
          $max_page = ceil($jml/10);
      	?>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -70px; padding: 25px; background: white; border-radius: 15px; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25);">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 10px 0px;">
            <p style="font-size: 1.2em; font-weight: bold;">Filter Pencarian</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel" style="border-radius: 10px; border: 1px solid #184981;">
              <div class="panel-heading" style="background: #184981; color: white; border-radius: 10px;" data-toggle="collapse" data-target="#pekerjaan">Pekerjaan <i class="fa fa-chevron-down pull-right" style="margin-top: 5px;"></i> </div>
              <div id="pekerjaan" class="collapse panel-body" style="max-height: 120px; overflow-y: auto;">
                <?php
                  $jobs = $this->db->query("
                    SELECT a.*, count(b.id) as jml from ngi_job a
                      LEFT JOIN (
                        SELECT a.*, d.*
                        FROM ngi_joblist a
                          LEFT JOIN ngi_job c on a.posisi = c.id
                          LEFT JOIN ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                        WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where."
                      ) b on a.id = b.posisi
                    GROUP BY a.id
                    ORDER BY jml desc, a.nama_job asc
                  ")->result();
                ?>
                <?php foreach ($jobs as $j): ?>
                  <?php
                    $jobs_array = explode(",", $_POST["jobs"]);

                    if(in_array("j".$j->id, $jobs_array)){
                      $jobs_checked = "checked";
                    }else{
                      $jobs_checked = "";
                    }
                  ?>
                  <?php if ($j->jml > 0): ?>
                    <p class="">
                      <input class="hidden" type="checkbox" id="j<?=$j->id?>" name="jobs"  <?=$jobs_checked?> >
                      <label for="j<?=$j->id?>"> <?=$j->nama_job?> <span>(<?=$j->jml?>)</span> </label>
                    </p>
                  <?php endif; ?>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel" style="border-radius: 10px; border: 1px solid #184981;">
              <div class="panel-heading" style="background: #184981; color: white; border-radius: 10px;" data-toggle="collapse" data-target="#kategori">Kategori <i class="fa fa-chevron-down pull-right" style="margin-top: 5px;"></i> </div>
              <div id="kategori" class="collapse panel-body" style="max-height: 120px; overflow-y: auto;">
                <?php
                    $category = $this->db->query("
                      SELECT a.*, count(b.id) as jml
                      FROM ngi_jobcategory a
                        LEFT JOIN (
                          SELECT a.*, d.*
                          FROM ngi_joblist a
                            LEFT JOIN ngi_job c on a.posisi = c.id
                            LEFT JOIN ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                          WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where."
                        ) b on a.id = b.idcategory
                      GROUP BY a.id
                      ORDER BY jml desc
                    ")->result();
                 ?>
                 <?php foreach ($category as $c): ?>
                   <?php
                     $category_array = explode(",", $_POST["category"]);

                     if(in_array("c".$c->id, $category_array)){
                         $category_checked = "checked";
                     }else{
                         $category_checked = "";
                     }
                   ?>
                   <?php if ($c->jml > 0): ?>
                     <p class="">
                       <input class="hidden" type="checkbox" id="c<?=$c->id?>" name="category"  <?=$category_checked?> >
                       <label for="c<?=$c->id?>"><?=$c->category?> <span>(<?=$c->jml?>)</span></label>
                     </p>
                   <?php endif; ?>
                 <?php endforeach; ?>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel" style="border-radius: 10px; border: 1px solid #184981;">
              <div class="panel-heading" style="background: #184981; color: white; border-radius: 10px;" data-toggle="collapse" data-target="#upah">Upah Kerja <i class="fa fa-chevron-down pull-right" style="margin-top: 5px;"></i> </div>
              <div id="upah" class="collapse panel-body" style="max-height: 120px; overflow-y: auto;">
                <?php
                  $salary = $this->db->query("
                    SELECT a.*, count(b.id) as jml
                    FROM ngi_salary a
                      LEFT JOIN (
                        SELECT a.*, d.*
                        FROM ngi_joblist a
                          LEFT JOIN ngi_job c on a.posisi = c.id
                          LEFT JOIN ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                        WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where."
                      ) b on b.upah_kerja = a.id
                    GROUP BY a.range
                    ORDER BY a.id
                  ")->result();
                ?>
                <?php foreach ($salary as $s): ?>
                  <?php
                    $salary_array = explode(",", $_POST["salary"]);

                    if(in_array("s".$s->id, $salary_array)){
                        $salary_checked = "checked";
                    }else{
                        $salary_checked = "";
                    }
                  ?>
                  <?php if ($s->jml > 0): ?>
                    <p class="">
                      <input class="hidden" type="checkbox" id="s<?=$s->id?>" name="salary" <?=$salary_checked?> >
                      <label for="s<?=$s->id?>"><?=$s->range?><span> (<?=$s->jml?>)</span></label>
                    </p>
                  <?php endif; ?>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel" style="border-radius: 10px; border: 1px solid #184981;">
              <div class="panel-heading" style="background: #184981; color: white; border-radius: 10px;" data-toggle="collapse" data-target="#waktu">Waktu Kerja <i class="fa fa-chevron-down pull-right" style="margin-top: 5px;"></i> </div>
              <div id="waktu" class="collapse panel-body" style="max-height: 120px; overflow-y: auto;">
                <?php
                  $type = $this->db->query("
                    SELECT tipe, count(tipe) as jml
                    FROM (
                      SELECT a.*, d.*
                      FROM ngi_joblist a
                        LEFT JOIN ngi_job c on a.posisi = c.id
                        LEFT JOIN ngi_kecamatan d on a.idkecamatan = d.subdistrict_id
                      WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where."
                    ) a
                    GROUP BY a.tipe"
                  )->result();
                ?>
                <?php foreach ($type as $t): ?>
                  <?php
                    $type_array = explode(",", $_POST["type"]);

                    if(in_array($t->tipe, $type_array)){
                        $type_checked = "checked";
                    }else{
                        $type_checked = "";
                    }
                  ?>
                  <p>
                      <input class="hidden" type="checkbox" id="<?=$t->tipe?>" name="type" <?=$type_checked?> >
                      <label for="<?=$t->tipe?>"><?=$t->tipe?><span> (<?=$t->jml?>)</span></label>
                  </p>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="gc_causes_select_box_wrapper">
              <div class="gc_causes_select_box">
                <select id="sorting">
                  <option value="a.log desc">Urutkan terbaru</option>
                  <option value="a.log asc">Urutkan terlama</option>
                  <option value="c.nama_job asc">Urutkan alphabet</option>
                </select><i class="fa fa-angle-down"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <p class="pull-right" style="margin-top: 15px; font-size: 1.1em; font-weight: bold;">Anda melihat &nbsp;<span><?=($page+1)?> - <?=(($pages == $max_page)? $page+(($jml%10 == 0)? 10 : $jml%10) : $page+10)?> dari total <?=$jml?> hasil</span></p>
          </div>
        </div>
      </div>

      <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 20px 0px;">
        <p style="font-size: 1.2em; font-weight: bold;">Kami menemukan <span><?=$jml?></span> hasil untuk anda</p>
      </div> -->

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 35px;">
        <div class="row">
          <?php if ($jml > 0): ?>
            <?php foreach ($joblist as $item): ?>
              <?php
                $keyword = explode(',', $item->keyword);
              ?>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 25px;">
                <div class="row" style="border-left: 12px solid #184981; background: #FFF; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.25); border-radius: 20px; padding: 15px;">
                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-left: -8px;">
                    <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="tittle_img" class="img img-responsive" style="box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); border-radius: 15px;" />
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="padding: 5px 5px;">
                    <p style="font-size: 1.5em; font-weight: bold; color: black;"><?=$item->title?></p>
                    <ul class="pull-right">
                      <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                      <li><a href="#"> <?=$item->tipe?></a></li>
                      <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                    </ul>
                    <p style="font-size: 1.1em; font-weight: normal; color: black;">
                      <?php echo $item->nama_perusahaan; ?>
                      &#9679;
                      <?php echo $item->subdistrict_name.', '.$item->type.' '.$item->city.', '.$item->province; ?>
                    </p>
                    <p style="margin-top: 15px; color: #5B5B5B; font-size: 1.1em;">
                      <i class="fa fa-user" style="color: #2B77CD; font-size: 1.3em;"></i>
                      &nbsp;
                      <?php echo $item->nama_job; ?>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <i class="fa fa-money" style="color: #2B77CD; font-size: 1.3em;"></i>
                      &nbsp;
                      Rp. <?php echo $item->upah; ?>
                    </p>
                    <p style="margin-top: 15px; color: #5B5B5B; font-size: 1.1em;">
                      <i class="fa fa-map-marker" style="color: #2B77CD; font-size: 1.3em;"></i>
                      &nbsp;
                      <?php echo $item->subdistrict_name.', '.$item->type.' '.$item->city.', '.$item->province; ?>
                    </p>
                    <p style="margin-top: 5px; color: #909090; font-size: 1.1em;" class="pull-left">
                      Diposting pada <?php echo date('d-F-Y', strtotime($item->log)); ?>
                    </p>
                    <p style="margin-top: 5px; color: #5B5B5B; font-size: 1.1em;" class="pull-right">
                      <i class="fa fa-tags" style="color: #184981;"></i>
                      &nbsp;
                      <span style="color: #184981;">Keyword : </span>
                      <?php foreach ($keyword as $idx => $kw): ?>
                        <a href="#"  class="trending" id="<?php echo $kw ?>"><?php echo $kw; ?></a>
                        <?php if ($idx+1 < count($keyword)): ?>
                          <?php echo ","; ?>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </p>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0px; margin-top: -50px">
                <div class="pager_wrapper gc_blog_pagination">
                    <ul class="pagination">
                        <?php if($_POST['page'] == '' || $_POST['page'] == '1') {?>
                            <li class="disabled"><a href="#">Prev</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <?php for($i=2; $i<=min($max_page,3);$i++){ ?>
                                <li><a href="#" class="pages" id="<?=$i?>"><?=$i?></a></li>
                            <?php } if($max_page == '1'){?>
                                <li class="disabled"><a href="#">Next</a></li>
                            <?php }else {?>
                                <li><a href="#" class="pages" id="2">Next</a></li>
                            <?php }?>
                        <?php } else if($_POST['page'] == $max_page){ ?>
                            <li><a href="#" class="pages" id="<?=($max_page-1)?>">Prev</a></li>
                            <?php for($i=max($max_page-3,1); $i<=$max_page;$i++){ ?>
                                <li class="<?=(($i == $max_page)? "active" : "")?>"><a href="#" class="<?=(($i != $max_page)? "pages" : "")?>" id="<?=$i?>"><?=$i?></a></li>
                            <?php } ?>
                            <li class="disabled"><a href="#">Next</a></li>
                        <?php } else { ?>
                            <li><a href="#" class="pages" id="<?=($_POST['page']-1)?>">Prev</a></li>
                            <li><a href="#" class="pages" id="<?=($_POST['page']-1)?>"><?=($_POST['page']-1)?></a></li>
                            <li class="active"><a href="#" class="pages" id="<?=($_POST['page'])?>"><?=($_POST['page'])?></a></li>
                            <li><a href="#" class="pages" id="<?=($_POST['page']+1)?>"><?=($_POST['page']+1)?></a></li>
                            <li><a href="#" class="pages" id="<?=($_POST['page']+1)?>">Next</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
          <?php else: ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="jp_recent_resume_box_wrapper">
                    <h3 style="text-align: center; color: white;">Tidak ditemukan hasil</h3>
                </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.redirect.js"></script>
    <script type="text/javascript">
        var jobs = new Array();
        var category = new Array();
        var salary = new Array();
        var type = new Array();

        var currentJobListLength = 5;
        var currentJobCategoryLength = 5;
        var currentJobSalaryLength = 3;

        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
              margin:10,
              loop:true,
              autoplay:true,
              autoplayTimeout:4000,
              autoplayHoverPause:true,
              autoWidth:true
            });

            console.log('<?=json_encode($_POST)?>');

            $(document).on('change','input[name="jobs"]', function(){

                sendData('');
            });

            $(document).on('change','input[name="category"]', function(){

                sendData('');
            });

            $(document).on('change','input[name="salary"]', function(){

                sendData('');
            });

            $(document).on('change','input[name="type"]', function(){

                sendData('');
            });


            $(document).on('change','#sorting', function(){

                sendData('');
            });

            $('.fa-heart-o').hover( function(){
                $(this).attr('class', 'fa fa-heart fa-lg text-danger');
                $(this).parent().addClass('bg-red');
            },  function(){
                $(this).attr('class', 'fa fa-heart-o fa-lg text-danger');
                $(this).parent().removeClass('bg-red');
            });

            $('.pagination .disabled a, .pagination .active a').on('click', function(e) {
                e.preventDefault();
            });

            $(document).on('click','.pages', function(){
                var pages = $(this).prop('id');

                sendData(pages);
            }).on('click', '.trending', function(){
                var trend = $(this).prop('id');

                sendData('',trend);
            }).on('click', '#jobListBtn', function(e){
                e.preventDefault();
                showMoreJobList();
            }).on('click', '#jobCategoryBtn', function(e){
                e.preventDefault();
                showMoreJobCategory();
            }).on('click', '#jobSalaryBtn', function(e){
                e.preventDefault();
                showMoreJobSalary();
            })

            $('.joblist').hide();
            $('.joblist').slice(0,currentJobListLength).show();

            $('.jobcategory').hide();
            $('.jobcategory').slice(0,currentJobCategoryLength).show();

            $('.jobsalary').hide();
            $('.jobsalary').slice(0,currentJobSalaryLength).show();
        });

        function sendData(pages, trending = ''){
            $('input[name="jobs"]').each(function(){
                if($(this).is(':checked')){
                    jobs.push($(this).prop('id'));
                }
            });

            $('input[name="category"]').each(function(){
                if($(this).is(':checked')){
                    category.push($(this).prop('id'));
                }
            });


            $('input[name="salary"]').each(function(){
                if($(this).is(':checked')){
                    salary.push($(this).prop('id'));
                }
            });

            $('input[name="type"]').each(function(){
                if($(this).is(':checked')){
                    type.push($(this).prop('id'));
                }
            });

            post($('#jobsearch').attr('action'), {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    keyword : (trending == '')? $('input[name="keyword"]').val() : trending,
                    location : $('select[name="location"]').val(),
                    sorting : $('#sorting').val(),
                    // experience : $('select[name="experience"]').val(),
                    jobs: jobs,
                    category : category,
                    salary : salary,
                    type : type,
                    page : pages
                }, "post");
        }


        function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }

        function showMoreJobList(){
            currentJobListLength = currentJobListLength + 5;
            $('.joblist').slice(0,currentJobListLength).show();
        }

        function showMoreJobCategory(){
            currentJobCategoryLength = currentJobCategoryLength + 5;
            $('.jobcategory').slice(0,currentJobCategoryLength).show();
        }

        function showMoreJobSalary(){
            currentJobSalaryLength = currentJobSalaryLength + 5;
            $('.jobsalary').slice(0,currentJobSalaryLength).show();
        }
    </script>
