<?php if($this->session->userdata('is_login_pelamar')){
	redirect(base_url().'portal');
}else if($this->session->userdata('is_login_perusahaan')){
	redirect(base_url().'perusahaan');
}else{ ?>


<div class="login_section">
		<!-- login_form_wrapper -->
		<div class="login_form_wrapper">
			<div class="container">
				<div class="row">
					<?php echo $this->session->flashdata('email_sent'); ?>
					<div class="col-md-8 col-md-offset-2">
						<!-- login_wrapper -->
						<h1>Masuk ke akun anda</h1>
							<form id="formlogin" action="<?=base_url()?>portal/page/loginportal" method="post">
								<div class="login_wrapper" style="border-radius: 25px;">
									<div class="formsix-pos">
										<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
										<input type="hidden" name="refer" value="<?php echo $this->agent->referrer(); ?>">
										<div class="form-group i-email">
											<input type="email" class="form-control" name="username" id="email2" placeholder="Email/Username*" required />
										</div>
									</div>
									<div class="formsix-e">
										<div class="form-group i-password">
											<input type="password" class="form-control" name="password" id="password2" placeholder="Password *" required />
										</div>
									</div>
									<div class="login_remember_box" style="margin-top: 0px !important; margin-bottom: 50px !important;">
										<!-- <label class="control control--checkbox">Ingat saya
											<input type="checkbox">
											<span class="control__indicator"></span>
										</label> -->
										<a href="<?=base_url()?>portal/lupapassword" class="forget_password">
											Lupa password
										</a>
									</div>
									<div class="login_btn_wrapper">
										<a class="btn btn-primary login_btn" id="login"> Login </a>
									</div>
									<div class="login_message">
										<p>Tidak punya akun ? <a href="<?=base_url()?>portal/register" style="color: #164980;"> Daftar sekarang </a> </p>
									</div>
								</div>
							</form>
						<!-- /.login_wrapper-->
					</div>
				</div>
			</div>
		</div>
		<!-- /.login_form_wrapper-->
	</div>

	<script type="text/javascript">
		$(function(){
      $('#email2').focus();

			$('#login').click(function(){
				$('#login').attr('disabled',true);
				$('#login').html('Loading....');
				$('#formlogin').submit();
			});

			$(document).on('submit','#formlogin', function(e){
				$.ajax({
					url : $(this).prop('action'),
					data : $(this).serialize(),
					dataType: 'json',
					method : 'post',
					success: function(rs){
						if(rs.success){
							if(rs.tipe == 'pelamar'){
								window.location = '<?=(($redirect == "")? base_url() : $redirect)?>';
							}else{
								window.location = '<?=(($_POST['url'] == "")? base_url()."perusahaan" : $_POST['url'])?>';
							}
						}else{
							if(rs.tipe == undefined) {
								alert('Email/username atau password salah. Silahkan coba lagi.');
								$('#login').attr('disabled',false);
								$('#login').html('login');
							}else{
								if(rs.email == undefined) {
									alert('Silahkan lakukan verifikasi ke kantor disnaker dengan membawa berkas-berkas yang berkaitan.');
									$('#login').attr('disabled',false);
									$('#login').html('login');
								}else{
								 	post('<?=base_url()?>portal/verifikasi', {
					                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
					                    email : rs.email,
					                    tipe : rs.tipe
					                }, "post");
								}
							}
						}
					},
					error: function(e){
						alert('Terjadi error pada server. Silahkan coba lagi');
						$('#login').attr('disabled',false);
						$('#login').html('login');
					}
				});

				e.preventDefault();
				// $('#login').attr('disable',false);
				// $('#login').html('Loading....');
			})
		})

		function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }

	</script>

<?php } ?>
