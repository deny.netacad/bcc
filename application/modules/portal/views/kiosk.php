<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="<?=$def_ass?>bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$def_ass?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?=$def_ass?>css/ionicons.min.css">
<link rel="stylesheet" href="<?=$def_ass?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<?=$def_ass?>dist/css/AdminLTE.css">
<link rel="stylesheet" href="<?=$def_ass?>dist/css/skins/_all-skins.min.css">

<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
<link href="<?=$def_css?>datetimepicker.css" rel="stylesheet">
<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?=$def_css?>custom.css" rel="stylesheet">
<link href="<?=$def_css?>icomoon/style.css" rel="stylesheet">
<link href="<?=$def_js?>plugins/imgeffects/css/effects.min.css" rel="stylesheet">

<!-- jQuery 2.1.4 -->
<script src="<?=$def_ass?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?=$def_ass?>bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=$def_ass?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=$def_ass?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=$def_ass?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=$def_ass?>dist/js/demo.js"></script>

<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.min.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/mask/jquery.mask.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/jpodtable/jquery.jpodtable.js"></script>
<script src="<?=$def_js?>holder.js"></script>
<script src="<?=$def_js?>enterform.js"></script>
<script src="<?=$def_js?>plugins/isloading/jquery.isloading.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>

<link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.16/dist/sweetalert2.all.min.js" integrity="sha256-551HBsteMvKOSqjUXSmRy/EOF0bBNbWB96b5L3Deh8A=" crossorigin="anonymous"></script>

<script src="https://printjs-4de6.kxcdn.com/print.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">

<style media="screen">
  .text-small {
    font-size: 200% !important;
  }
  .text-medium {
    font-size: 300% !important;
  }
  .text-large {
    font-size: 800% !important;
  }
  .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: white;
    height: 75px;
    color: white;
    text-align: center;
  }
  .table-detail td {
    padding: 10px;
    font-size: 1em;
    font-weight: bold;
    border-bottom: 1px solid #B0B0B0;
  }
  .antrian {
    background: url("<?php echo base_url(); ?>assets/img/corner_right_bottom.png") no-repeat right center;
    background-repeat: no-repeat;
    background-size: 300px 150px;
    background-position: right 0px bottom 0px;
    border-radius: 30px;
  }
</style>

<div class="wrapper" style="padding: 15px 0px; background-color: #013076; overflow: hidden;">
  <div class="container-fluid">
    <div class="row" style="margin-bottom: 60px;">
      <div class="col-lg-6">
        <div class="panel" style="background: #FFFFFF; border-radius: 30px;">
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-4 text-center">
                <img id="tiketPhoto" src="<?php echo base_url('assets/img/profile/default_user.png') ?>" class="img" style="width:85%; height: 200px; border-radius: 15px;">
              </div>
              <div class="col-lg-8">
                <table style="width: 100%; font-size: 1.5em; margin-top: 10px;" class="table-detail">
                  <tr>
                    <td style="width: 1%; white-space: nowrap;">Nama</td>
                    <td width='5%'>:</td>
                    <td id="tiketNama"></td>
                  </tr>
                  <tr>
                    <td style="width: 1%; white-space: nowrap;">Tanggal</td>
                    <td width='5%'>:</td>
                    <td id="tiketTanggal"></td>
                  </tr>
                  <tr>
                    <td style="width: 1%; white-space: nowrap;">Layanan</td>
                    <td width='5%'>:</td>
                    <td id="tiketLayanan"></td>
                  </tr>
                  <tr>
                    <td style="width: 1%; white-space: nowrap;">No Referensi</td>
                    <td width='5%'>:</td>
                    <td id="tiketReferensi"></td>
                  </tr>
                </table>
                <button id="btnCetak" class="btn btn-lg btn-primary pull-right disabled" style="font-size: 2.5em; margin-top: 75px; background: #013076; border-radius: 15px;"><i class="fa fa-print"></i>&nbsp;Cetak Tiket</button>
              </div>
            </div>
          </div>
        </div>
        <div class="panel" style="background: #FFFFFF; border-radius: 31px;">
          <div class="panel-body antrian" style="padding-top: 0px;">
            <div class="row">
              <span class="pull-left" style="font-size: 4em; font-weight: bold; margin-left: 20px;">Tiket Antrian</span>
              <div class="pull-right" style="font-weight: bold; padding: 10px; font-size: 3em; background: #00AA58; border-radius: 0px 30px; color: white;">
                <?php echo date('d-M-Y'); ?>
              </div>
              <div class="col-lg-8">
                <div class="text-center" style="margin: 0px;">
                  <span id="tiketNomor" style="font-size: 10em; font-family: 'Orbitron', sans-serif; letter-spacing: 0px;">0000</span>
                </div>
                <div class="text-center" style="font-size: 2em">
                  Mohon Menunggu <span id="tiketTunggu">0</span> Antrian Lagi
                </div>
              </div>
              <div class="col-lg-4 text-center">
                <img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:150px;height:80px; margin-top:30px;" title="Bogor Career Center" >
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="panel" style="background: #FFFFFF; border-radius: 30px;">
          <div class="panel-body">
            <?php $button = array(
              '1' => 3,'2' => 3,'3' => 3,'A' => 3,
              '4' => 3,'5' => 3,'6' => 3,'B' => 3,
              '7' => 3,'8' => 3,'9' => 3,'C' => 3,
              '0' => 9,'D' => 3,
              'DELETE' => 6,'ENTER' => 6,
            ); ?>
            <div style="height: 9em; margin-bottom: 20px; background: #E1E3E2; border: 5px solid #013076; box-sizing: border-box; border-radius: 18px;" class="text-center">
              <span id="kolomNomor" style="font-size: 6em; font-family: 'Orbitron', sans-serif; letter-spacing: 8px;">00000000</span>
            </div>
            <div id="row">
            <?php foreach ($button as $key => $value): ?>
              <center>
                <div class="col-lg-<?php echo $value; ?> col-sm-<?php echo $value; ?>">
                  <input type="button" style="<?php echo ($value==3)?"width: 95%;":"width: 100%;"; ?> margin-bottom: 10px; border-radius: 30px; padding: 25px 0px; font-size: 2.5em; font-weight: bold; background-color: #00AA58; color: white;" value="<?php echo $key ?>" id="pin-<?php echo $key ?>" class="btn btn-block btn-default"/>
                </div>
              </center>
            <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="pull-left" style="font-weight: bold; padding: 15px; font-size: 2.5em; background: #00AA58; border-radius: 0px 50px 0px 0px; color: white;">
        <span id="digital-clock">00:00:00</span> WIB &nbsp;
      </div>
      <marquee class="pull-left" width="83%" direction="left" scrollamount="8" height="75px" style="padding-top: 7px; color: black; font-size: 3em; font-weight: bold;">
        <?php
          $qRunText = $this->db->query("
            SELECT * FROM r_pengaturan
            WHERE parameter = 'running_text'
          ")->row();
        ?>
        <?php echo $qRunText->value; ?>
      </marquee>
    </div>
  </div>
</div>

<style media="screen">
.swal2-popup {
  font-size: 2rem !important;
}
</style>

<script type="text/javascript">
  $(document).ready(function() {
    clockUpdate();
    setInterval(clockUpdate, 1000);
  });

  function clockUpdate() {
    var date = new Date();

    function addZero(x) {
      if (x < 10) {
        return x = '0' + x;
      } else {
        return x;
      }
    }

    function twelveHour(x) {
      if (x > 12) {
        return x = x - 12;
      } else if (x == 0) {
        return x = 12;
      } else {
        return x;
      }
    }

    var h = addZero(date.getHours());
    var m = addZero(date.getMinutes());
    var s = addZero(date.getSeconds());

    $('#digital-clock').html(h + ':' + m + ':' + s)
  }

  var textWriten = "";
  var isIdentity = false;
  var stringGet = "";

  <?php foreach ($button as $key => $value): ?>
    <?php if (strlen($key) == 1): ?>
      $('#pin-<?php echo $key ?>').on( "click", function() {
        if (textWriten.length < 8) {
          textWriten += "<?php echo $key ?>";
        }
        $('#kolomNomor').html(textWriten);
      });
    <?php endif; ?>
  <?php endforeach; ?>

  $('#pin-DELETE').on( "click", function() {
    if (textWriten.length <= 1 || isIdentity == true) {
      resetIdentitas();
      location.reload();
    } else {
      textWriten = textWriten.slice(0,-1);
      $('#kolomNomor').html(textWriten);
    }
  });
  $('#pin-ENTER').on( "click", function() {
    var postData = {
      <?php echo $this->security->get_csrf_token_name(); ?> : "<?php echo $this->security->get_csrf_hash(); ?>",
      no_ref: textWriten
    };

    $.post("<?php echo base_url('portal/ajaxfile/cekReferensi'); ?>", postData, function( data ) {
      if (data.length == 0) {
        resetIdentitas();
        swalError();
      }
      else {
        isIdentity = true;
        $('#tiketNama').html(data.nama);
        $('#tiketTanggal').html(data.dtm);
        $('#tiketLayanan').html(data.nama_layanan);
        $('#tiketReferensi').html(data.no_ref);
        $('#tiketNomor').html(data.no_tiket);
        $('#tiketTunggu').html(data.tunggu);
        if (data.photo.length > 0) {
          $("#tiketPhoto").attr("src", "<?php echo base_url('assets/img/profile/') ?>/"+ data.photo + "?v=" + `${new Date().getTime()}`);
        }
        $('#btnCetak').removeClass("disabled");

        stringGet +='?name='+data.nama+'&no='+data.no_tiket+'&wait='+data.tunggu+'&no_ref='+data.no_ref;

        setTimeout(function(){ if(isIdentity) resetIdentitas(); }, 15000);
      }
    });
  });

  $('#btnCetak').on( "click", function() {
    if(isIdentity){
      printJS('portal/ticket'+stringGet);
      setTimeout(function(){ location.reload(); }, 4000);
    }
    resetIdentitas();
  });

  function swalError() {
    Swal.fire({
      allowOutsideClick: false,
      icon: 'error',
      html: '<h1>Nomor Referensi <span class="text-danger">Tidak Ditemukan</span> atau <span class="text-primary">Kadaluarsa</span>!</h1>'
    }).then((result) => {
      if (result.isConfirmed) {
        resetIdentitas();
      }
    });
  }

  function resetIdentitas() {
    isIdentity = false;
    stringGet = "";
    textWriten = '';
    $('#kolomNomor').html('00000000');

    $('#tiketNama').html("");
    $('#tiketTanggal').html("");
    $('#tiketLayanan').html("");
    $('#tiketReferensi').html("");
    $('#tiketNomor').html("0000");
    $('#tiketTunggu').html("0");
    $("#tiketPhoto").attr("src", "<?php echo base_url('assets/img/profile/default_user.png') ?>" + "?v=" + `${new Date().getTime()}`);
    $('#btnCetak').addClass("disabled");
  }
</script>
