<?php
if(isset($_SESSION['email'])){
	$_POST['email'] = $_SESSION['email'];
	unset($_SESSION['email']);
}

if($this->session->userdata('is_login_pelamar')){
	redirect(base_url().'portal');
}else if($this->session->userdata('is_login_perusahaan')){
	redirect(base_url().'perusahaan');
}else if($_POST['email'] == ""){
	redirect(base_url().'portal/login');
}else{

?>


<div class="login_section">
		<!-- login_form_wrapper -->
		<div class="login_form_wrapper">
			<div class="container">
				<div class="row">
					<?php echo $this->session->flashdata('email_sent'); ?>
					<div class="col-md-8 col-md-offset-2">
						<!-- login_wrapper -->
						<h1>Verifikasi email anda</h1>
							<form id="formlogin" action="<?=base_url()?>portal/page/kirimulangemail" method="post">
								<p id="keterangan" style="font-size: 1.2em; margin-bottom: 40px; line-height: 1.4em;">Buka <b>email yang Anda daftarkan</b> pada sistem kami, kemudian <b>klik tautan</b> yang sudah kami kirimkan. Jika Anda tidak mendapatkan email apapun, Anda bisa mengirim ulang email dengan menggunakan tombol dibawah ini.</p>
								<div class="login_wrapper">
									<div class="formsix-pos">
										<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
										<input type="hidden" name="email" value="<?=$_POST['email']?>">
										<input type="hidden" name="tipe" value="<?=$_POST['tipe']?>">
									</div>
									<div class="login_btn_wrapper">
										<a class="btn btn-primary login_btn" id="login"> Kirim ulang </a>
									</div>
									<div class="login_message">
										<p>Sudah verifikasi email ? <a href="<?=base_url()?>portal/login"> Login disini </a> </p>
									</div>
								</div>
							</form>
						<!-- /.login_wrapper-->
					</div>
				</div>
			</div>
		</div>
		<!-- /.login_form_wrapper-->
	</div>

	<script type="text/javascript">
		$(function(){
      $('#email2').focus();

			$('#login').click(function(){
				$('#login').attr('disabled',true);
				$('#login').html('Loading....');
				$('#formlogin').submit();
			});

			$(document).on('submit','#formlogin', function(e){
				$.ajax({
					url : $(this).prop('action'),
					data : $(this).serialize(),
					dataType: 'json',
					method : 'post',
					success: function(rs){
            console.log(rs);
						if(rs.success){
							$('#keterangan').html('Email tautan verifikasi telah dikirimkan ulang ke alamat email <?=$_POST['email']?>. Silahkan cek email dan lakukan verifikasi.');
							$('#login').attr('disabled',false);
							$('#login').html('Kirim ulang');
						}else{
							$('#keterangan').html('Email tautan verifikasi gagal dikirimkan ke alamat email <?=$_POST['email']?>. Alamat email tidak valid.');
							$('#login').attr('disabled',false);
							$('#login').html('Kirim ulang');
						}
					},
					error: function(e){
						alert('Terjadi error pada server. Silahkan coba lagi');
						$('#login').attr('disabled',false);
						$('#login').html('Kirim ulang');
					}
				});

				e.preventDefault();
				// $('#login').attr('disable',false);
				// $('#login').html('Loading....');
			})
		})

	</script>

<?php } ?>
