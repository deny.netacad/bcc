<style>
  .container-highlight {
    margin-top: -140px;
    padding: 0px 30px;
    position: relative;
  }
  .container-highlight-judul {
    width: calc(100% - 60px);
    background-color:rgba(1,1,1,0.4);
    position: absolute;
    bottom: 0px;
    left: 0px;
    margin: 0px 30px;
    padding: 20px 30px;
    border-radius: 0px 0px 50px 50px;

    -webkit-transition: background .5s ease-out;
    -moz-transition: background .5s ease-out;
    -o-transition: background .5s ease-out;
    transition: background .5s ease-out;
  }
  .container-highlight-judul:hover {
    background-color:rgba(1,1,1,0.9);
  }
</style>

<hr style="margin: 0px 0px 0px 0px; border: 1px solid #184981">
<div class="row">
  <div style="background: #184981; padding-top: 75px; padding-bottom: 200px;">
    <div class="container" style="padding: 0 25px;">
      <span style="font-size: 2.5em; color: white;">Berita dan Event Bogor Career Center</span>
    </div>
  </div>
  <?php
     $rs = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.position= 0 and a.art_thumb !='' and a.art_intro !='' order by a.art_date desc limit 3")->result();
  ?>
  <?php
    $xpath = new DOMXPath(@DOMDocument::loadHTML($rs[0]->art_thumb));
    $src = $xpath->evaluate("string(//img/@src)");
    $src=str_replace('.thumbs','',$src);
  ?>
  <div class="container text-left container-highlight">
    <img style="border-radius: 50px 50px 50px 50px; width:100%" class="img img-responsive" src="<?=$src?>" class="img-responsive" alt="<?=$rs[0]->art_title?>" />
    <div class="container-highlight-judul">
      <p style="font-size: 1.5em; font-weight: bold;"><a class="link-judul" href="<?=base_url()?>portal/page/berita/<?=$rs[0]->art_seo?>" name="<?=$rs[0]->art_id?>" style="color: white;"><?=$rs[0]->art_title?></a></p>
      <p style="color: white;" class="pull-right">
        <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($rs[0]->tmstamp)); ?> &nbsp;
        <i class="fa fa-calendar"></i>
      </p>
    </div>
  </div>
  <div class="container" style="margin-top: 40px; padding: 0 25px;">
    <p style="color: #184981; font-size: 1.5em; font-weight: bold;">Berita Terkini</p>
    <div class="row">
      <?php foreach ($rs as $key => $value): ?>
        <?php
          $xpath = new DOMXPath(@DOMDocument::loadHTML($value->art_thumb));
          $src = $xpath->evaluate("string(//img/@src)");
          $src=str_replace('.thumbs','',$src);
        ?>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 20px;">
          <div style="background: #FFFFFF; box-shadow: 0px 0px 10px 4px rgba(0, 0, 0, 0.2); border-radius: 15px; padding-bottom: 10px;">
            <img style="border-radius: 15px 15px 0px 0px; width:100%; height: 200px;" class="img img-responsive" src="<?=$src?>" class="img-responsive" alt="<?=$value->art_title?>" />
            <div style="padding: 10px 15px; height: 330px; position: relative;">
              <p style="color: #003672;">
                <i class="fa fa-calendar"></i> &nbsp; <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($value->tmstamp)); ?>
              </p>
              <p style="font-size: 1.5em; font-weight: bold; margin-top: 15px; color: #003672; font-weight: 600; font-size: 24px; line-height: 28px;"><?=$value->art_title?></p>
              <p style="font-size: 1em; font-weight: bold; margin-top: 15px; color: #5B5B5B; line-height: 22px;"><?=$value->art_intro?></p>
            </div>
            <a href="<?=base_url()?>portal/page/berita/<?=$value->art_seo?>" style="position: absolute; bottom: 0px; left:50%; transform: translate(-50%, -50%); background: #00AA58; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 10px; color: white; padding: 5px 20px;">Baca Berita</a>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <br>
    <center>
      <a href="<?=base_url()?>portal/page/all_berita" style="color:white; font-weight: bold;">Lihat Semua Berita</a>
    </center>
  </div>
</div>
<div style="background: #184981; margin-top: -200px; padding-top: 250px; padding-bottom: 50px; border-radius: 100px 100px 0px 0px;">
  <div class="container">
    <p style="color: white; font-size: 2em; font-weight: bold;">Event Terbaru</p>
    <?php
     $rs = $this->db->query("select a.*, b.nama from section_agenda a inner join mast_user b on a.iduser = b.iduser where a.flag=1  and a.agenda_thumb !='' and a.agenda_title !='' order by a.agenda_id desc limit 2")->result();
    ?>
    <div class="row">
      <?php foreach ($rs as $key => $value): ?>
        <?php
          $xpath = new DOMXPath(@DOMDocument::loadHTML($value->agenda_thumb));
          $src = $xpath->evaluate("string(//img/@src)");
          $src=str_replace('.thumbs','',$src);
        ?>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top: 20px;">
          <div style="background: #FFFFFF; box-shadow: 0px 0px 10px 4px rgba(0, 0, 0, 0.2); border-radius: 15px; padding-bottom: 10px;">
            <img style="border-radius: 15px 15px 0px 0px; width:100%; height: 350px;" class="img img-responsive" src="<?=$src?>" class="img-responsive" alt="<?=$value->agenda_title?>" />
            <div style="padding: 10px 15px; height: 150px; position: relative;">
              <p style="color: #003672;">
                <i class="fa fa-calendar"></i> &nbsp; <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($value->tmstamp)); ?>
              </p>
              <p style="font-size: 1.5em; font-weight: bold; margin-top: 15px; color: #003672; font-weight: 600; font-size: 24px; line-height: 28px;"><?=$value->agenda_title?></p>
            </div>
            <a href="<?=base_url()?>portal/page/event/<?=$value->agenda_seo?>" style="position: absolute; bottom: 0px; left:50%; transform: translate(-50%, -50%); background: #00AA58; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 10px; color: white; padding: 5px 20px;">Lihat Event</a>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <br>
    <center>
      <a href="<?=base_url()?>portal/page/all_event" style="color:white; font-weight: bold;">Lihat Semua Event</a>
    </center>
    <div class="row" style="margin-top: 20px;">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="jp_spotlight_main_wrapper" style="background: #EEF6FF; box-shadow: 0px 0px 15px 5px rgba(0, 0, 0, 0.3); border-radius: 15px;">
          <div class="spotlight_header_wrapper" style="background: transparent !important">
            <h4 style="font-weight: bold; font-size: 24px; line-height: 29px; color: #003672;">Rekomendasi Lowongan</h4>
          </div>
          <div class="jp_spotlight_slider_wrapper" style="background: #EEF6FF; border-radius: 15px; min-height:275px;">
            <div class="owl-carousel owl-theme">
              <?php if($this->session->userdata('is_login_pelamar')){
                $person = $this->db->query('select skill from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();
                $skill = explode(',', $person->skill);

                $where = "";
                foreach ($skill as $s) {
                  $where .= " or a.skill like '%".$s."%'";
                }

                $joblist = $this->db->query("
                  SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                  LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                  LEFT JOIN ngi_job c ON a.posisi = c.id
                  LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                  left join ngi_salary e on e.id = a.upah_kerja
                  WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ORDER BY RAND() LIMIT 5
                ");
              }
              else
              {
                $joblist = $this->db->query("
                  SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                  LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                  LEFT JOIN ngi_job c ON a.posisi = c.id
                  LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                  left join ngi_salary e on e.id = a.upah_kerja
                  WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5
                ");
              }

              if($joblist->num_rows() == 0 ){
                $joblist = $this->db->query("
                  SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a
                  LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                  LEFT JOIN ngi_job c ON a.posisi = c.id
                  LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                  left join ngi_salary e on e.id = a.upah_kerja
                  WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5
                ");
              }
              ?>
              <?php foreach ($joblist ->result() as $key => $item): ?>
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <img clas src="<?=base_url()?>template/HTML/job_light/images/content/spotlight.jpg" alt="spotlight_img" />
                    </div>
                    <div class="col-lg-8 col-sm-12">
                      <h4><?=$item->nama_job?></h4>
                      <p><?=$item->nama_perusahaan?></p>
                      <ul>
                        <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                        <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->type?> <?=$item->city?>, <?=$item->province?></li>
                      </ul>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px; margin-top: 10px;" class="pull-right" href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Lihat Detil</a>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="jp_spotlight_main_wrapper" style="background: #EEF6FF; box-shadow: 0px 0px 15px 5px rgba(0, 0, 0, 0.3); border-radius: 15px;">
          <div class="spotlight_header_wrapper" style="background: transparent !important">
            <h4 style="font-weight: bold; font-size: 24px; line-height: 29px; color: #003672;">Informasi Pelatihan</h4>
          </div>
          <div class="jp_spotlight_slider_wrapper" style="background: #EEF6FF; border-radius: 15px; min-height:275px;">
            <div class="owl-carousel owl-theme">
            <?php
              $pelatihan = $this->db->query("
                SELECT a.*,DATE_FORMAT(a.tgl_mulai,'%d %M') AS tglmulai,DATE_FORMAT(a.tgl_ahir,'%d %M %Y') AS tglakhir FROM ref_pelatihan a
              ");

              foreach ($pelatihan->result() as $items) {
            ?>
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <img src="<?=base_url()?>template/HTML/job_dark/images/content/resume_bg_1.jpg" alt="spotlight_img" />
                    </div>
                    <div class="col-lg-8 col-sm-12">
                      <p style="font-family: Montserrat; font-style: normal; font-weight: 600; font-size: 20px; line-height: 29px; color: #676767;"><?=$items->judul?></p>
                      <ul>
                        <li style="font-family: Montserrat; font-style: normal; font-weight: normal; font-size: 16px; line-height: 22px; color: #5B5B5B;">
                          <i class="fa fa-map-marker"></i>&nbsp; <?=$items->tempat?>
                        </li>
                        <li style="font-family: Montserrat; font-style: normal; font-weight: normal; font-size: 16px; line-height: 22px; color: #5B5B5B;"><i class="fa fa-calendar-o"></i>&nbsp;  <?=$items->tglmulai?> s/d <?=$items->tglakhir?></li>
                      </ul>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <a style="background: #003672; color: #FFFFFF; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px; padding: 5px 20px; margin-top: 10px;" class="pull-right" href="<?=base_url()?>../pelatihan/portal/dip_detail/<?=$item->id?>">Lihat Detil</a>
                    </div>

                  </div>
                </div>
            <?php  } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-sm-12" style="background: white; border-radius: 15px; margin-top: 50px; padding: 35px;">
      <?php
        $rs = $this->db->query("select narasi from section_contact");
        $seo = $rs->row();
      ?>
      <div class="row">
        <div class="col-lg-3 col-sm-12">
          <img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:100%;" title="Bogor Career Center" >
        </div>
        <div class="col-lg-9 col-sm-12">
          <p style="font-weight: normal; font-size: 20px; line-height: 24px; text-align: center; letter-spacing: 0.15em; color: #676767;">
            <?= $seo->narasi ?>
          </p>
          <br>
          <center>
            <a href="<?=base_url()?>portal/register_perusahaan" style="padding: 15px; color: white; background: #00A85A; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); border-radius: 23.5px;"><i class="fa fa-plus-circle"></i> &nbsp;DAFTAR PERUSAHAAN</a>
          </center>
        </div>
      </div>
    </div>
  </div>
</div>

<br>
<br>
