<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="<?=$def_ass?>bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$def_ass?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?=$def_ass?>css/ionicons.min.css">
<link rel="stylesheet" href="<?=$def_ass?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<?=$def_ass?>dist/css/AdminLTE.css">
<link rel="stylesheet" href="<?=$def_ass?>dist/css/skins/_all-skins.min.css">

<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?=$def_css?>custom.css" rel="stylesheet">
<link href="<?=$def_css?>icomoon/style.css" rel="stylesheet">
<link href="<?=$def_js?>plugins/imgeffects/css/effects.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/owl.theme.default.css" />

<!-- jQuery 2.1.4 -->
<script src="<?=$def_ass?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?=$def_ass?>bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=$def_ass?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=$def_ass?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=$def_ass?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=$def_ass?>dist/js/demo.js"></script>

<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.min.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!--<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/modules/exporting.js"></script>-->
<script type="text/javascript" src="<?=$def_js?>plugins/mask/jquery.mask.js"></script>
<script type="text/javascript" src="<?=$def_js?>plugins/jpodtable/jquery.jpodtable.js"></script>
<script src="<?=$def_js?>holder.js"></script>
<script src="<?=$def_js?>enterform.js"></script>
<script src="<?=$def_js?>plugins/isloading/jquery.isloading.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>

<script src="<?=base_url()?>template/HTML/job_light/js/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.3.2/chart.min.js" integrity="sha512-VCHVc5miKoln972iJPvkQrUYYq7XpxXzvqNfiul1H4aZDwGBGC0lq373KNleaB2LpnC2a/iNfE5zoRYmB4TRDQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels" charset="utf-8"></script> -->

<?php
  $qPendidikan = $this->db->query("
    SELECT min_pendidikan as pendidikan, COUNT(min_pendidikan) as jumlah
    FROM ngi_joblist
    WHERE CHAR_LENGTH(min_pendidikan) < 4 AND is_aktif = 1
    GROUP BY min_pendidikan
    ORDER BY min_pendidikan
  ")->result();
  $arrPendidikan = array();
  foreach ($qPendidikan as $key => $value) {
    $arrPendidikan[$value->pendidikan] = $value->jumlah;
  }
  $arrPendCompiled = array(
    'pendidikan' => array('SMA Sederajat', 'Diploma', 'Sarjana', 'Magister'),
    'jumlah' => array($arrPendidikan['SMA'], $arrPendidikan['D3'], $arrPendidikan['S1'], $arrPendidikan['S2'])
  );
?>

<?php
  $qLamar = $this->db->query("
    SELECT SUM( CASE WHEN ngi_jobapplied.status = 1 THEN 1 ELSE 0 END ) AS Diterima, SUM( CASE WHEN ngi_jobapplied.status = 0 THEN 1 ELSE 0 END ) AS Ditolak, SUM( CASE WHEN ngi_jobapplied.status is NULL THEN 1 ELSE 0 END ) AS Belum, YEAR(ngi_jobapplied.log) AS tahun, MONTH(ngi_jobapplied.log) AS bulan FROM ngi_joblist LEFT JOIN ngi_jobapplied ON ngi_joblist.id = ngi_jobapplied.id_job WHERE MONTH(ngi_jobapplied.log) IS NOT NULL AND YEAR(ngi_jobapplied.log) = '".date('Y')."' GROUP BY YEAR(ngi_jobapplied.log), MONTH(ngi_jobapplied.log)
  ")->result();
  $arrStatus = array('Ditolak','Diterima','Belum');
  $arrLamarCompiled = array(
    'Ditolak' => array(0,0,0,0,0,0,0,0,0,0,0,0),
    'Diterima' => array(0,0,0,0,0,0,0,0,0,0,0,0),
    'Belum' => array(0,0,0,0,0,0,0,0,0,0,0,0)
  );
  foreach ($qLamar as $value) {
    foreach ($arrStatus as $val) {
      $arrLamarCompiled[$val][$value->bulan - 1] = $value->{$val};
    }
  }
?>

<?php
  $qBelum = $this->db->query("
    SELECT SUM( CASE WHEN ngi_joblist.min_pendidikan = 'SMA' THEN 1 ELSE 0 END ) AS SMA, SUM( CASE WHEN ngi_joblist.min_pendidikan = 'D3' THEN 1 ELSE 0 END ) AS D3, SUM( CASE WHEN ngi_joblist.min_pendidikan = 'S1' THEN 1 ELSE 0 END ) AS S1, SUM( CASE WHEN ngi_joblist.min_pendidikan = 'S2' THEN 1 ELSE 0 END ) AS S2, YEAR(ngi_jobapplied.log) AS tahun, MONTH(ngi_jobapplied.log) AS bulan FROM ngi_joblist LEFT JOIN ngi_jobapplied ON ngi_joblist.id = ngi_jobapplied.id_job WHERE MONTH(ngi_jobapplied.log) IS NOT NULL AND ngi_jobapplied.status IS NULL AND YEAR(ngi_jobapplied.log) = '".date('Y')."' GROUP BY YEAR(ngi_jobapplied.log), MONTH(ngi_jobapplied.log)
  ")->result();
  $arrJenjang = array('SMA','D3','S1','S2');
  $arrBelumCompiled = array(
    'SMA' => array(0,0,0,0,0,0,0,0,0,0,0,0),
    'D3' => array(0,0,0,0,0,0,0,0,0,0,0,0),
    'S1' => array(0,0,0,0,0,0,0,0,0,0,0,0),
    'S2' => array(0,0,0,0,0,0,0,0,0,0,0,0),
  );
  foreach ($qBelum as $value) {
    foreach ($arrJenjang as $val) {
      $arrBelumCompiled[$val][$value->bulan - 1] = $value->{$val};
    }
  }
?>

<?php
  $qLamaran = $this->db->query("
    SELECT
      COUNT(idcategory) AS jumlah,
      MONTH(ngi_joblist.date_start) as bulan,
      ngi_jobcategory.category
    FROM
      ngi_joblist
    RIGHT JOIN
      ngi_jobcategory
    ON
      ngi_jobcategory.id = ngi_joblist.idcategory
    WHERE YEAR(ngi_joblist.date_start) = '".date('Y')."'
    GROUP BY
      idcategory,
      YEAR(ngi_joblist.date_start),
      MONTH(ngi_joblist.date_start)
  ")->result();

  $arrLamaran = array();
  foreach ($qLamaran as $key => $value) {
    $arrLamaran[$value->category] = array(0,0,0,0,0,0,0,0,0,0,0,0);
  }
  foreach ($qLamaran as $key => $value) {
    $arrLamaran[$value->category][$value->bulan - 1] = $value->jumlah;
  }

  $arrLamaranCompiled = array();
  $index = 0;
  foreach ($arrLamaran as $key => $value) {
    $arrLamaranCompiled[$index]['name'] = $key;
    $arrLamaranCompiled[$index]['jumlah'] = $value;
    $index++;
  }

  $colors = array(
    "#F44336",
    "#FB8C00",
    "#1976D2",
    "#43A047",
    "#9966ff",
    "#36a2eb",
    "#8BC34A",
    "#FFCA28",
    "#c1c1c1",
    "#000000");
?>

<style media="screen">
  .text-small {
    font-size: 200% !important;
  }
  .text-medium {
    font-size: 300% !important;
  }
  .text-large {
    font-size: 800% !important;
  }
  .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: white;
    height: 75px;
    color: white;
    text-align: center;
  }
  .date-video {
    position: absolute;
    right: 0.5vw;
    font-weight: bold;
    padding: 10px 40px;
    font-size: 5vh;
    background: #00AA58;
    border-radius: 0px 30px;
    color: white;
    z-index: 99;
  }
  .corner {
    background: url("<?php echo base_url(); ?>assets/img/corner_right_bottom.png") no-repeat right center;
    background-repeat: no-repeat;
    background-size: 10vw 12vh;
    background-position: right 0px bottom 0px;
    border-radius: 30px;
  }
  .img-owl {
    border-radius: 30px;
  }
  .owl-nav, .owl-dots {
    display: none;
  }
</style>

<div class="wrapper" style="padding: 15px 0px 0px 15px; background-color: #164996;">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6" style="padding-right: 1vw">
        <div class="row">
          <div class="pull-right date-video">
            <?php echo date('d-M-Y'); ?>
          </div>
          <div class="panel panel-info" style="background: #FFFFFF; border-radius: 30px;">
            <div class="panel-body" style="padding: 5px;">
              <?php
                $qVideo = $this->db->query("
                  SELECT * FROM r_pengaturan
                  WHERE parameter = 'video_files'
                ")->row();
              ?>
              <video autoplay loop style="
                border-radius: 30px;
                object-fit: cover;
                height: 49vh;
                width: 100%;
              ">
                <source src="<?php echo base_url('/assets/video/'.$qVideo->value) ?>">
              </video>
            </div>
          </div>
          <div style="
            border: none;
            height: 2vh;
            margin: 0px;
            padding: 0px;
          "></div>
          <div class="row">
            <div class="col-lg-5">
              <div class="panel panel-info" style="
                width: 100%;
                height: 40vh;
                background: #FFFFFF;
                border-radius: 3em;
              ">
                <div class="panel-body corner" style="
                  padding: 5%;
                  height: 100%;
                ">
                  <canvas id="chartLowongan" style="width: 98%; height: 98%;"></canvas>
                </div>
              </div>
            </div>
            <div class="col-lg-7">
              <div class="panel panel-info" style="background: #3586FF; border-radius: 18px;">
                <div class="panel-body text-center" style="padding: 0px;">
                  <div style="font-weight: bold; font-size: 3.5vh; background: #013076; border-radius: 18px; min-width: 100%; color: white; padding: 10px 0px;">
                    Jumlah Perusahaan Terdaftar
                  </div>
                  <div style="font-size: 10vh; background: #3586FF; border-radius: 0px 0px 18px 18px; color: white; font-weight: bold;">
                    <span><?php echo number_format($this->db->query('select idperusahaan from ngi_perusahaan')->num_rows(),0,",","."); ?></span>
                  </div>
                </div>
              </div>
              <div class="panel panel-info" style="background: #00AA58; border-radius: 18px;">
                <div class="panel-body text-center" style="padding: 0px;">
                  <div style="font-weight: bold; font-size: 3.5vh; background: #098950; border-radius: 18px; min-width: 100%; color: white; padding: 10px 0px;">
                    Jumlah Pencari Kerja Terdaftar
                  </div>
                  <div style="font-size: 9.5vh; background: #00AA58; border-radius: 0px 0px 18px 18px; color: white; font-weight: bold;">
                    <span><?php echo number_format($this->db->query('select iduser from user_pelamar')->num_rows(),0,",","."); ?></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6" style="padding-left: 1vw">
        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-info" style="background: #FFFFFF; border-radius: 30px;">
              <div class="panel-body" style="padding: 5px;">
                <div id="view-ad-image">
                  <div class="owl-carousel owl-theme">
                    <?php
                      $qSlider = $this->db->query("
                        SELECT * FROM r_pengaturan
                        WHERE parameter = 'front_slider'
                        ORDER BY num ASC
                      ")->result();
                    ?>
                    <?php foreach ($qSlider as $key => $value): ?>
                      <div class="item"> <img class="img-owl" src="<?php echo base_url(); ?>/assets/img/banner/<?php echo $value->value; ?>" alt=""> </div>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="panel panel-info" style="background: #FFFFFF; border-radius: 30px; height: 26vh">
              <div class="panel-body" style="padding: 5px;">
                <center>
                  <canvas id="chartLamaran" style="width: calc(98%); height: calc(98%);"></canvas>
                </center>
              </div>
            </div>
          </div>
          <div class="col-lg-12" style="
            border: none;
            height: 2vh;
            margin: 0px;
            padding: 0px;
          "></div>
          <div class="col-lg-12">
            <div class="panel panel-info" style="background: #FFFFFF; border-radius: 30px; height: 20vh">
              <div class="panel-body" style="padding: 5px;">
                <center>
                  <canvas id="chartBelumPendidikan" style="width: calc(98%); height: calc(98%);"></canvas>
                </center>
              </div>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="panel panel-info" style="background: #FFFFFF; border-radius: 30px; height: 20vh">
              <div class="panel-body" style="padding: 5px;">
                <center>
                  <canvas id="chartBelum" style="width: calc(98%); height: calc(98%);"></canvas>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer" style="height: 5vh;">
      <div class="pull-left" style="
        position: absolute;
        bottom: 0; left: 0;
        width: 18vw;
        font-weight: bold;
        font-size: 4vh;
        background: #00AA58;
        border-radius: 0px 80px 0px 0px;
        color: white;
        z-index: 1;
      "><span id="digital-clock">00:00:00</span> WIB &nbsp;
      </div>
      <marquee class="pull-left" direction="left" scrollamount="20" style="
        position: absolute;
        bottom: 0;
        right: 0;
        color: black;
        width: 82vw;
        font-size: 4vh;
        font-weight: bold;
      ">
        <?php
          $qRunText = $this->db->query("
            SELECT * FROM r_pengaturan
            WHERE parameter = 'running_text'
          ")->row();
        ?>
        <?php echo $qRunText->value; ?>
      </marquee>
    </div>
  </div>
</div>

<div class="audio-library">
  <audio id="nomor">
    <source src="<?php echo base_url('/assets/voices/nomor.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="antrian">
    <source src="<?php echo base_url('/assets/voices/antrian.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="loket">
    <source src="<?php echo base_url('/assets/voices/loket.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="a">
    <source src="<?php echo base_url('/assets/voices/a.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="b">
    <source src="<?php echo base_url('/assets/voices/b.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="c">
    <source src="<?php echo base_url('/assets/voices/c.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="d">
    <source src="<?php echo base_url('/assets/voices/d.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="kosong">
    <source src="<?php echo base_url('/assets/voices/kosong.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="satu">
    <source src="<?php echo base_url('/assets/voices/satu.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="dua">
    <source src="<?php echo base_url('/assets/voices/dua.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="tiga">
    <source src="<?php echo base_url('/assets/voices/tiga.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="empat">
    <source src="<?php echo base_url('/assets/voices/empat.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="lima">
    <source src="<?php echo base_url('/assets/voices/lima.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="enam">
    <source src="<?php echo base_url('/assets/voices/enam.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="tujuh">
    <source src="<?php echo base_url('/assets/voices/tujuh.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="delapan">
    <source src="<?php echo base_url('/assets/voices/delapan.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="sembilan">
    <source src="<?php echo base_url('/assets/voices/sembilan.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="sepuluh">
    <source src="<?php echo base_url('/assets/voices/sepuluh.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="sebelas">
    <source src="<?php echo base_url('/assets/voices/sebelas.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="seratus">
    <source src="<?php echo base_url('/assets/voices/seratus.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="belas">
    <source src="<?php echo base_url('/assets/voices/belas.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="puluh">
    <source src="<?php echo base_url('/assets/voices/puluh.mp3'); ?>" type="audio/mpeg">
  </audio>
  <audio id="ratus">
    <source src="<?php echo base_url('/assets/voices/ratus.mp3'); ?>" type="audio/mpeg">
  </audio>

</div>

<script type="text/javascript">
  $( document ).ready(function() {
    $('.owl-carousel').owlCarousel({
      loop: true,
      nav: true,
      dots: true,
      navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      responsive: {
        0: {
          items: 1,
        }
      },
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:true
    });
    setInterval(clockUpdate, 1000);
    setInterval(function(){
      cekPanggilan();
      // cekSisa();
      cekLayanan();
      // cekKuota();
    }, 2000);

    var chartLowongan = document.getElementById('chartLowongan').getContext('2d');
    new Chart(chartLowongan, {
        type: 'doughnut',
        data: {
            labels: <?php echo json_encode($arrPendCompiled['pendidikan']); ?>,
            datasets: [{
                label: '# of Votes',
                data: <?php echo json_encode($arrPendCompiled['jumlah']); ?>,
                backgroundColor: [
                    '#3586FF',
                    '#013076',
                    '#00AA58',
                    '#098950',
                ]
            }]
        },
        options: {
          responsive: false,
          plugins: {
            legend: {
              position: 'bottom',
              align: 'start',
              maxWidth: 50,
              labels: {
                font: {
                  size: 50
                }
              }
            },
            title: {
              display: true,
              text: 'Lowongan Pekerjaan',
              color: '#000000',
              font: {
                size: 64,
              }
            },
            datalabels: {
              color: '#000000',
              textAlign: 'center',
              formatter: function(value, ctx) {
                  return value;
              }
            }
          }
        }
    });

    var chartLamaran = document.getElementById('chartLamaran').getContext('2d');
    new Chart(chartLamaran, {
        type: 'bar',
        data: {
            labels: [
      				'Jan', 'Feb', 'Mar',
      				'Apr', 'May', 'Jun',
      				'Jul', 'Aug', 'Sep',
      				'Oct', 'Nov', 'Dec'
      			],
            datasets: [
              <?php foreach ($arrLamaranCompiled as $key => $value): ?>
                <?php echo "{"; ?>
                  <?php echo "label: '".$value['name']."',"; ?>
                  <?php echo "data: ".json_encode($value['jumlah']).","; ?>
                  <?php echo "backgroundColor: ['".$colors[$key]."']"; ?>
                <?php echo "},"; ?>
              <?php endforeach; ?>
            ]
        },
        options: {
          responsive: false,
          scales: {
              y: {
                title : {
                  display: true,
                  text: "Jumlah Lowongan",
                  font: {
                    size: 35
                  }
                },
                beginAtZero: true
              }
          },
          plugins: {
            legend: {
              position: 'bottom',
              labels: {
                font: {
                  size: 45
                }
              }
            },
            title: {
              display: true,
              text: 'Grafik Terending Lowongan (<?php echo date('Y'); ?>)',
              color: '#000000',
              font: {
                size: 50,
              }
            },
          }
        }
    });

    var chartBelumPendidikan = document.getElementById('chartBelumPendidikan').getContext('2d');
    new Chart(chartBelumPendidikan, {
        type: 'bar',
        data: {
            labels: [
      				'Jan', 'Feb', 'Mar',
      				'Apr', 'May', 'Jun',
      				'Jul', 'Aug', 'Sep',
      				'Oct', 'Nov', 'Dec'
      			],
            datasets: [
              {
                label: 'SMA Sederajat',
                data: <?php echo json_encode($arrBelumCompiled['SMA']) ?>,
                backgroundColor: [
                    '#3586FF'
                ]
              },
              {
                label: 'Diploma',
                data: <?php echo json_encode($arrBelumCompiled['D3']) ?>,
                backgroundColor: [
                    '#013076'
                ]
              },
              {
                label: 'Sarjana',
                data: <?php echo json_encode($arrBelumCompiled['S1']) ?>,
                backgroundColor: [
                    '#00AA58'
                ]
              },
              {
                label: 'Magister',
                data: <?php echo json_encode($arrBelumCompiled['S2']) ?>,
                backgroundColor: [
                    '#098950'
                ]
              },
            ]
        },
        options: {
          responsive: false,
          scales: {
              y: {
                title : {
                  display: true,
                  text: "Jumlah Pelamar (orang)",
                  font: {
                    size: 35
                  }
                },
                beginAtZero: true
              }
          },
          plugins: {
            legend: {
              position: 'bottom',
              labels: {
                font: {
                  size: 45
                }
              }
            },
            title: {
              display: true,
              text: 'Tingkat Pendidikan Pelamar yang Belum Diterima (<?php echo date('Y'); ?>)',
              color: '#000000',
              font: {
                size: 50,
              }
            },
          }
        }
    });

    var chartBelum = document.getElementById('chartBelum').getContext('2d');
    new Chart(chartBelum, {
        type: 'bar',
        data: {
            labels: [
      				'Jan', 'Feb', 'Mar',
      				'Apr', 'May', 'Jun',
      				'Jul', 'Aug', 'Sep',
      				'Oct', 'Nov', 'Dec'
      			],
            datasets: [
              {
                label: 'Diterima',
                data: <?php echo json_encode($arrLamarCompiled['Diterima']) ?>,
                backgroundColor: [
                    '#00AA58'
                ]
              },
              {
                label: 'Ditolak',
                data: <?php echo json_encode($arrLamarCompiled['Ditolak']) ?>,
                backgroundColor: [
                    '#013076'
                ]
              },
              {
                label: 'Belum Diperiksa',
                data: <?php echo json_encode($arrLamarCompiled['Belum']) ?>,
                backgroundColor: [
                    '#098950'
                ]
              },
            ]
        },
        options: {
          responsive: false,
          scales: {
              y: {
                title : {
                  display: true,
                  text: "Jumlah Pelamar (orang)",
                  font: {
                    size: 35
                  }
                },
                beginAtZero: true
              }
          },
          plugins: {
            legend: {
              position: 'bottom',
              labels: {
                font: {
                  size: 45
                }
              }
            },
            title: {
              display: true,
              text: 'Grafik Pelamar Diterima & Belum Diterima (<?php echo date('Y'); ?>)',
              color: '#000000',
              font: {
                size: 50,
              }
            },
          }
        }
    });
  });

  function clockUpdate() {
    var date = new Date();

    function addZero(x) {
      if (x < 10) {
        return x = '0' + x;
      } else {
        return x;
      }
    }

    function twelveHour(x) {
      if (x > 12) {
        return x = x - 12;
      } else if (x == 0) {
        return x = 12;
      } else {
        return x;
      }
    }

    var h = addZero(date.getHours());
    var m = addZero(date.getMinutes());
    var s = addZero(date.getSeconds());

    $('#digital-clock').html(h + ':' + m + ':' + s)
  }

  function audioPlayer(string, index) {
    var strings = string.split(" ");
    var audio = document.getElementById(strings[index]);
    audio.play();
    audio.onended = function() {
      if(index < strings.length-1){
        audioPlayer(string, index+1);
      }
    };
  }

  function cekSisa() {
    $.get("<?php echo base_url('loket/ajaxfile/hitungTunggu'); ?>", function( data ) {
      $('#sisaAntrian').html(data.jumlah);
    });
  }

  function cekKuota() {
    $.get("<?php echo base_url('loket/ajaxfile/kuotaHarian'); ?>", function( data ) {
      $('#kuotaHarian').html(data);
    });
  }

  function cekLayanan() {
    $.get("<?php echo base_url('loket/ajaxfile/sedangDilayani'); ?>", function( data ) {
      var stringHtml = "";
      $.each( data, function( key, value ) {
        // stringHtml += '' +
        //   '<div class="col-lg-3">' +
        //     '<h1 class="text-medium">Loket '+ value.loket +' : '+ value.no_antrian +'</h1>' +
        //   '</div>';
        $('#loket-'+value.loket).html(value.no_antrian);
      });
      if(data.length == 0){
        for (var i = 1; i < 5; i++) {
          $('#loket-'+i).html("0000");
        }
      }
      // if(data.length > 0) $('#sedangDilayani').html(stringHtml);
      // else $('#sedangDilayani').html("");
    });
  }

  function cekPanggilan() {
    $.get("<?php echo base_url('loket/ajaxfile/cekPanggilan'); ?>", function( data ) {
      $.each( data, function( key, value ) {
        var kode  = value.no_antrian.substring(0,1).toLowerCase();
        var antrian = value.no_antrian.substring(1);
        var antrian_tb = digit3(antrian);
        var loket_tb = digit3(value.loket);
        var string_play = $.trim(("antrian " + kode + " " + antrian_tb + " loket "+ loket_tb).replace(/  +/g, ' '));

        setTimeout(function(){
          $('#callNomor').html(value.no_antrian);
          $('#callLoket').html("ke Loket " + value.loket);
          audioPlayer(string_play, 0);
          if(data.length == key + 1) cekPanggilan();
        }, key * string_play.split(" ").length * 1800);
      });
    });
  }
</script>
