<?php

class Ajaxfile extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}


	function caripasar(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			nama like '%".strtolower($keyword)."%'

			)

		";

		$rs = $this->db->query("select * from ref_pasar where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_pasar where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function carisport(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			name_sport like '%".strtolower($keyword)."%'

			)

		";

		$rs = $this->db->query("select * from sport where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from sport where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}


	function cariuser(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			username like '%".strtolower($keyword)."%' or
			id like '%".$keyword."%' or
			nama_pegawai like '%".strtolower($keyword)."%'
			)
			and username!='ngi'
		";

		$rs = $this->db->query("select * from pegawai where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from pegawai where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function caripetugas(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			nama like '%".strtolower($keyword)."%' or
			username like '%".$keyword."%'
			)

		";

		$rs = $this->db->query("SELECT * FROM ref_pegawai where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_pegawai where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	/*
	function caripetugas(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where (
			nama like '%".strtolower($keyword)."%' or
			username like '%".strtolower($keyword)."%'
		)
		";
		$q = "SELECT * FROM ref_pegawai $where";
		$rs = $this->db->query($q);

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}*/
	function cariagama(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			agama like '%".$keyword."%' or
			idagama like '%".$keyword."%'
			)
		";

		$rs = $this->db->query("select * from ref_agama where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_agama where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function carijab(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			jab like '%".$keyword."%' or
			idjab like '%".$keyword."%'
			)
		";

		$rs = $this->db->query("select * from ref_jab2 where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_jab2 where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function caritkpend(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			tkpend like '%".$keyword."%' or
			idtkpend like '%".$keyword."%'
			)
		";

		$rs = $this->db->query("select * from ref_tkpend where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_tkpend where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function carijur(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			jur like '%".$keyword."%' or
			idtkpend like '%".$keyword."%'
			)
			and length(jur)>0 and (idtkpend='".$this->input->post('idtkpend')."' and jur not like '%DELETE%')
		";

		$rs = $this->db->query("select * from ref_jur where $where ");
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_jur where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function cariangkot(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			plat like '%".$keyword."%'
			)
		";

		$rs = $this->db->query("select * from ref_angkot where flag_delete = 0 and $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_angkot where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function caripegawaibank(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = "
			(
			nama like '%".strtolower($keyword)."%'
			)

		";

		$rs = $this->db->query("select * from ref_pegawai where $where ");

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("select * from ref_pegawai where $where limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $this->db->_error_message();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	public function setAntrianKuning()
	{
		$permitted_chars = '0123456789ABCD';

		$input   = $this->input->post();
		$user_id = $this->session->userdata['id'];

		$input['user_id']      = $user_id;
		$input['r_layanan_id'] = 2;
		$input['no_ref'] 			 = substr(str_shuffle($permitted_chars), 0, 8);

		$this->db->insert('h_antrian_online', $input);
	}

	public function cekReferensi()
	{
		$input = $this->input->post('no_ref');
		$cek = $this->db->query("
			SELECT
				h_antrian_online.dtm,
				h_antrian_online.no_ref,
				r_layanan.nama_layanan,
				r_layanan.kode_antrian,
				user_pelamar.nama,
				user_pelamar.photo
			FROM h_antrian_online
			INNER JOIN r_layanan ON r_layanan.id = r_layanan_id
			INNER JOIN user_pelamar ON user_pelamar.iduser = user_id
			LEFT JOIN h_antrian ON h_antrian.no_ref = h_antrian_online.no_ref
			WHERE h_antrian_online.no_ref = '".$input."'
			AND dtm = '".date('Y-m-d')."'
			AND h_antrian.no_ref IS NULL
		")->row();

		if(!empty($cek)){
			$cek->dtm = date('d-M-Y', strtotime($cek->dtm));

			$cekTiket = $this->db->query("
				SELECT id
				FROM h_antrian
				WHERE dtm_register LIKE '".date('Y-m-d')."%'
			")->num_rows();

			$cek->tunggu = $this->db->query("
				SELECT id
				FROM h_antrian
				WHERE dtm_register LIKE '".date('Y-m-d')."%'
				AND panggil = 'belum'
			")->num_rows();

			$cek->no_tiket = $cek->kode_antrian.($cekTiket + 1);
		}

		header('Content-Type: application/json');
		echo json_encode($cek);
	}
}
?>
