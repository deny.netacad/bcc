<?php
$route['(portal)/berita']	= "page/load/berita";
$route['(portal)/berita/(:any)']	= "page/load/berita/$1";
$route['(portal)/all_berita']	= "page/load/all_berita";
$route['(portal)/jobs/(:any)'] = "page/load/jobs/$1";
$route['(portal)/profile/(:any)'] = "page/load/profile/$1";
$route['(portal)/company/(:any)'] = "page/load/company/$1";
$route['(portal)/register']	= "page/load/register";
$route['(portal)/caripekerjaan']	= "page/load/caripekerjaan";
$route['(portal)/pengaturan']	= "page/load/pengaturan";
$route['(portal)/lamaran']	= "page/load/lamaran";
$route['(portal)/subpage']	= "page/load/subpage";
$route['(portal)/register_perusahaan']	= "page/load/register_perusahaan";
$route['(portal)/login']	= "page/load/login";
$route['(portal)/profile'] = "page/load/profile";
$route['(portal)/all_berita/(:any)']	= "page/load/all_berita/$1";
$route['(portal)/all_berita']	= "page/load/all_berita/1";
$route['(portal)/pengumuman']	= "page/load/pengumuman";
$route['(portal)/lengkapilampiran']	= "page/load/lengkapilampiran";
$route['(portal)/absen']	= "page/load/absen";
$route['(portal)/verifikasi']	= "page/load/verifikasi";
$route['(portal)/lupapassword']	= "page/load/lupapassword";

$route['(portal)/cetakkartupeserta']	= "page/load/cetakkartupeserta";

$route['(portal)/kiosk']	= "page/kiosk";
$route['(portal)/ticket']	= "page/printTicket";
$route['(portal)/monitor'] = "page/monitor";
$route['(portal)/videowall'] = "page/videowall";
$route['(portal)/kartukuning'] = "page/load/kartukuning";

$route['(portal)/informasi'] = "page/load/informasi";
$route['(portal)/error'] = "page/load/error404";
