<?php
class User extends CI_Model {
  function __construct()
    {
        parent::__construct();
    }

	
function foo(){
		echo "foo";
	}
		
  function getUser($iduser){
  	$rs = $this->db->query("select a.*,b.d,b.e,b.v,b.i,c.*,e.urlmodule as def_mod,c.urlmodule as module,c.module as menumodule
	from mast_user a
	inner join mast_usermodule b on a.iduser=b.iduser
	inner join mast_module c on b.idmodule=c.idmodule
	inner join mast_default_module d on a.iduser=d.iduser
	left join mast_module e on d.idmodule=e.idmodule
	where a.username='".$iduser."'");
    return $rs;
  } 
    
  function login(){
	
    
	$username = $this->input->post("username");
	$rs = $this->getUser($username);
	
	if($rs->num_rows() > 0){
      $user = $rs->row();
	  
      if(($username == $user->username) && (md5($this->input->post("userpass")) == $user->userpass)){ // cek password
		$module = array();
        foreach($rs->result() as $item){
			$module[$item->module]['module'] = $item->idmodule;
			$module[$item->module]['module'] = $item->module;
			$module[$item->module]['menumodule'] = $item->menumodule;
			$module[$item->module]['urlmodule'] = $item->urlmodule;
			$module[$item->module]['d'] = $item->d;
			$module[$item->module]['e'] = $item->e;
			$module[$item->module]['v'] = $item->v;
			$module[$item->module]['i'] = $item->i;
		}
		$sesi = array(
		'username'=>$user->username
		,'iduser'=>$user->iduser
		,'level'=>$user->role
		,'role'=>$module
		,'def_mod'=>$user->def_mod
		,'nama'=>$user->nama
		,'tmstamp'=>date("Y-m-d H:i:s")
		,'ip'=>$user->ip
		,'is_login_portal_sampang'=>true
		);
		$this->session->set_userdata($sesi);        
		$data['ip']			= $_SERVER['REMOTE_ADDR'];
		$data['tmstamp']	= $this->session->userdata('tmstamp');
		$data['useragent']	= $this->session->userdata('user_agent');
		$dt['iduser']		= $user->iduser;
        $this->db->update("mast_user",$data,$dt);
		$data['username']	= $this->session->userdata('username');
		$data['ket']		= 'login sebagai '.$user->role;
		$this->db->insert("log_user",$data);
		
		$this->session->unset_userdata('login_attempt');
		
		return true;
      }else{
	  	$this->session->set_userdata('login_attempt', $this->session->userdata('login_attempt') + 1);
	  	return false;
	  }
    }else{
		$this->session->set_userdata('login_attempt', $this->session->userdata('login_attempt') + 1);
    	return false;
	}
	
  }
  
  function logout(){
	$data['ip']			= $_SERVER['REMOTE_ADDR'];
	$data['username']	= $this->session->userdata('username');
	$data['useragent']	= $this->session->userdata('user_agent');
	$data['tmstamp']	= $this->session->userdata('tmstamp');
	$data['ket']		= 'logout ';
	$dt['iduser']		= $this->session->userdata('iduser');
	$this->db->insert("log_user",$data);
    $this->db->query("update mast_user set lastlogin = NOW() 
	where iduser = '".$this->session->userdata('iduser')."'");  
  }
  
}
?>