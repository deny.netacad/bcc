<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $modules = "sdm";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('sdm/user');
		$this->load->model('sdm/sdm_list');
		
	}

	function index(){
		if ($this->session->userdata('is_login_portal_disnakerbogor'))
		{
			if($this->cekRole()==false){
				redirect('auth');
			}else{
				$this->load->view($this->modules.'/main',$this->data);
			}
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}
	
	//========== new ===========//
	function saveGantipass(){
		
		$data['iduser'] = $this->session->userdata('user_id');
		$data['userpass'] = md5($this->input->post('userpass'));
		$rs = $this->db->get_where("mast_user",$data);
		if($rs->num_rows()>0){
			if($this->input->post('userpass2')==$this->input->post('userpass3')){
				$dt['userpass'] = md5($this->input->post('userpass2'));
				if(!$this->db->update("mast_user",$dt,$data)){
					$ret['text'] = "Terjadi Kesalahan\n".$this->db->_error_message();
				}else{
					$ret['text'] = "Password telah diganti";
				}
			}else{
				$ret['text'] = "Password Perulangan salah";
			}
		}else{
			$ret['text'] = "Password lama salah";
		}
		echo json_encode($ret);
	}
	
	function saveManmodul(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","idmodule");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->sdm_mod->save("mast_module",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}
	
	function editManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_module",$data);
		echo json_encode($rs->row());
	}
	
	function delManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("mast_module",$data)){
			echo "Terjadi Kesalahan";
		}else{
			echo "Data Terhapus";
		}
	}

	function setlolos(){
		if($this->db->query('update survei_data set lolos = "'.$_POST['val'].'" where id = "'.$_POST['id'].'"')){
			echo 'Berhasil mengeset status';
		}else{
			echo 'Gagal mengeset status';
		}
	}

	function setverif(){
		$table = 'sertifikasi_'.$_POST['table'];

		if($this->db->query('update '.$table.' set verified = "'.$_POST['val'].'" where id = "'.$_POST['id'].'"')){
			echo 'Berhasil mengeset status';
		}else{
			echo 'Gagal mengeset status';
		}
	}
	
	function savedatasurvei(){
		$arrtgl = ["tgl_survei"];
		$arrnot = ["check", "check_id","boraks", "formalin", "methanil", "rhodamin", "pestisida", "id_edit"];
		$arruji = ["boraks", "formalin", "methanil", "rhodamin", "pestisida"];

		$code = md5($this->input->ip_address().strtotime('now'));

		foreach ($_POST as $key => $value) {
			if(!in_array($key,$arrnot)){
				$data[$key] = $value;
			}
		}

		foreach ($data as $key => $value) {
			if(in_array($key, $arrtgl)){
				$temp = explode('-',$value);

				$data[$key] = $temp[2].'-'.$temp[1].'-'.$temp[0];
			}
		}

		$data['lolos'] = '1';
		foreach ($_POST['check'] as $val) {
			if($val == 'Tidak'){
				$data['lolos'] = '0';
				break;
			}
		}

		if($data['lolos'] != '0'){
			foreach ($arruji as $val) {
				if($_POST[$val] == '1'){
					$data['lolos'] = '0';
					break;
				}
			}
		}


		$insert = array();
		if($_POST["id_edit"] == ""){

			//insert
			$data['random_code'] = $code;
			$data['perkiraan'] = "1";
			if($this->db->insert('survei_data',$data)){
				$result = $this->db->query('select id from survei_data where random_code ="'.$code.'"')->row();
				$check = count($_POST["check"]);
				for ($i=0; $i < $check ; $i++) { 
					$input = array(
						'id_survei_data' => $result->id,
						'id_hasilpengawasan' => $_POST['check_id'][$i],
						'value' => $_POST['check'][$i]
					);

					$insert[$i] = $input;
				}

				$this->db->insert_batch('survei_hasilpengawasan',$insert);

				$record = array(
					'id_survei_data' => $result->id,
					'formalin' => $_POST["formalin"],
					'boraks' => $_POST["boraks"],
					'rhodamin' => $_POST["rhodamin"],
					'methanil' => $_POST["methanil"],
					'pestisida' => $_POST["pestisida"]
				);

				$this->db->insert('survei_ujilab',$record);

				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan data'));
			}else{
				echo json_encode(array('success' => true, 'text' => 'Gagal menyimpan data'));
			}
		}else{

			//edit
			if($this->db->set($data)->where('id',$_POST['id_edit'])->update('survei_data')){
				$this->db->query('delete from survei_hasilpengawasan where id_survei_data ="'.$_POST['id_edit'].'"');

				$check = count($_POST["check"]);
				for ($i=0; $i < $check ; $i++) { 
					$input = array( 
						'id_survei_data' => $_POST['id_edit'],
						'id_hasilpengawasan' => $_POST['check_id'][$i],
						'value' => $_POST['check'][$i]
					);

					$insert[$i] = $input;
				}

				$this->db->insert_batch('survei_hasilpengawasan',$insert);

				$record = array(
					'id_survei_data' => $_POST['id_edit'],
					'formalin' => $_POST["formalin"],
					'boraks' => $_POST["boraks"],
					'rhodamin' => $_POST["rhodamin"],
					'methanil' => $_POST["methanil"],
					'pestisida' => $_POST["pestisida"]
				);

				if($this->db->query('select id_survei_ujilab from survei_ujilab where id_survei_data ="'.$_POST['id_edit'].'" ')->num_rows() > 0){
					$this->db->set($record)->where('id_survei_data',$_POST['id_edit'])->update('survei_ujilab');
				}else{
					$this->db->insert('survei_ujilab',$record);
				}

				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan data'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan data'));
			}
		}
	}

	function editdatasurvei(){
		$q = $this->db->query("select a.id, a.rantai_pangan, a.jenis_pangan, a.lokasi, a.lokasi2, a.jenis_lokasi, a.kecamatan, a.kelurahan, a.kantin, a.alamat, a.nama_pedagang, a.keterangan, a.perkiraan, a.pangan, date_format(a.tgl_survei,'%d-%m-%Y') as tgl_survei, a.tempat, a.status_kep, a.jml_pekerja, b.formalin, b.boraks, b.rhodamin, b.methanil, b.pestisida from survei_data a
								left join survei_ujilab b on a.id = b.id_survei_data where a.id = '".$_POST['id']."'")->row();
		
		$q2 = $this->db->query("select b.* from survei_data a
								left join survei_hasilpengawasan b on a.id = b.id_survei_data where a.id = '".$_POST['id']."'")->result();

		echo json_encode(array('data1' => $q, 'data2' => $q2));
	}

	function deldatasurvei(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->set('is_delete','1')->where($data)->update("survei_data")){
			echo "Terjadi Kesalahan";
		}else{
			echo "Data Terhapus";
		}
	}

	function detaildatasurvei(){
		$q2 = $this->db->query("select a.*, b.nama as rantai_pgn, d.nama as jenis_pgn, e.nama as pasar2, f.nama_pasar, g.kecamatan as kec, h.kelurahan as kel, date_format(a.tgl_survei,'%d/%m/%Y') as tgl, c.* from survei_data a
								left join ref_rantaipangan b on b.id = a.rantai_pangan
								left join survei_ujilab c on a.id = c.id_survei_data
								left join ref_jenispangan d on d.id = a.jenis_pangan
								left join ref_pasar e on e.id = a.lokasi
								left join ref_pasar_sub f on f.id = a.lokasi2

								left join ref_kecamatan g on g.id  = a.kecamatan
								left join ref_kelurahan h on h.id = a.kelurahan
								where a.id = '".$_POST['id']."'")->row();

		
		$output = '  <div class="modal-body">
				       	<div class="container-fluid">
						    <div class="row">
						     <label class="col-xs-6 col-sm-4 control-label">Nama Pedagang</label>
						      <div class="col-xs-6 col-sm-8">'.$q2->nama_pedagang.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Tempat</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->tempat.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Rantai Pangan</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->rantai_pgn.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Jenis Pangan</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->jenis_pangan == '1' || $q2->jenis_pangan == '2' || $q2->jenis_pangan == '3')? $q2->jenis_pgn.' ('.$q2->pangan.')'  : $q2->jenis_pgn).'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Lokasi</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->jenis_lokasi == '0' || $q2->rantai_pangan == '11')? $q2->pasar2.' ('.$q2->nama_pasar.')' : (($q2->jenis_lokasi == '1' || $q2->rantai_pangan == '12')?  $q2->pasar2.' ('.$q2->kel.','.$q2->kec.')' :
						       													$q2->kel.','.$q2->kec  )).'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Alamat</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->alamat.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Tanggal Survei</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->tgl.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Keterangan</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->keterangan.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Status</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->lolos == '1')? 'Memenuhi Syarat' : (($q2->lolos == '0')? "Tidak Memenuhi Syarat" : "Belum Ditentukan")).'</div>
						    </div>
					 	</div>
				      </div>';

		$q = $this->db->query("select b.value, c.deskripsi from survei_data a
								left join survei_hasilpengawasan b on b.id_survei_data = a.id
								left join ref_hasilpengawasan c on c.id = b.id_hasilpengawasan
								where a.id = '".$_POST['id']."'")->result();

		$output .= '<div class="modal-footer" style="text-align: left">';
		foreach ($q as $i) {
			$output .= '
					       	<div class="container-fluid">
							    <div class="row">
							     <label class="col-xs-6 col-sm-4 control-label">'.$i->deskripsi.'</label>
							      <div class="col-xs-6 col-sm-8">'.$i->value.'</div>
							    </div>
						 	</div>';
		}
		$output .= '</div>';


		if($q2->perkiraan == '1'){
			$output .= '<div class="modal-footer" style="text-align: left">
				       	<div class="container-fluid">
						    <div class="row">
						     <label class="col-xs-6 col-sm-4 control-label">Formalin</label>
						      <div class="col-xs-6 col-sm-8">'.(($q2->formalin == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Boraks</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->boraks == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Rhodamin B</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->rhodamin == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Methanil Yellow</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->methanil == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Pestisida</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->pestisida == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
					 	</div>
				      </div>';
		}

		echo $output;		      

	}

		function detaildatasurvei2(){
		$q2 = $this->db->query("select a.*, b.nama as rantai_pgn, e.nama as pasar2, e.nama as pasar2, f.nama_pasar, g.kecamatan as kec, h.kelurahan as kel, i.kantin as kan, date_format(a.tgl_survei,'%d/%m/%Y') as tgl, c.* from survei_data a
								left join ref_rantaipangan b on b.id = a.rantai_pangan
								left join survei_ujilab c on a.id = c.id_survei_data
								left join ref_pasar e on e.id = a.lokasi
								left join ref_pasar_sub f on f.id = a.lokasi2
								left join ref_kecamatan g on g.id  = a.kecamatan
								left join ref_kelurahan h on h.id = a.kelurahan
								left join ref_kantin i on i.id = a.kantin
								where a.id = '".$_POST['id']."'")->row();

		
		$output = '  <div class="modal-body">
				       	<div class="container-fluid">
						    <div class="row">
						     <label class="col-xs-6 col-sm-4 control-label">Nama Pedagang</label>
						      <div class="col-xs-6 col-sm-8">'.$q2->nama_pedagang.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Tempat</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->tempat.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Jenis Lokasi</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->rantai_pgn.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Lokasi</label>
						       <div class="col-xs-6  col-sm-8">'.(($q2->rantai_pangan == '5' )? 
						       											$q2->pasar2.' ('.$q2->nama_pasar.')' : 
						       											(($q2->rantai_pangan == '4' )? 
						       												$q2->kan.' ('.$q2->kel.','.$q2->kec.')' : 
						       												(($q2->rantai_pangan == '6' )? 
						       													$q2->pasar2.' ('.$q2->kel.','.$q2->kec.')' :
						       													$q2->kel.','.$q2->kec
						       													))).'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Alamat</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->alamat.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Tanggal Survei</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->tgl.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Keterangan</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->keterangan.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Status</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->lolos == '1')? 'Memenuhi Syarat' : (($q2->lolos == '0')? "Tidak Memenuhi Syarat" : "Belum Ditentukan")).'</div>
						    </div>
					 	</div>
				      </div>';

		$q = $this->db->query("select b.value, c.deskripsi from survei_data a
								left join survei_hasilpengawasan b on b.id_survei_data = a.id
								left join ref_hasilpengawasan2 c on c.id = b.id_hasilpengawasan
								where a.id = '".$_POST['id']."'")->result();

		$output .= '<div class="modal-footer" style="text-align: left">';
		foreach ($q as $i) {
			$output .= '
					       	<div class="container-fluid">
							    <div class="row">
							     <label class="col-xs-6 col-sm-4 control-label">'.$i->deskripsi.'</label>
							      <div class="col-xs-6 col-sm-8">'.$i->value.'</div>
							    </div>
						 	</div>';
		}
		$output .= '</div>';


		if($q2->perkiraan == '1'){
			$output .= '<div class="modal-footer" style="text-align: left">
				       	<div class="container-fluid">
						    <div class="row">
						     <label class="col-xs-6 col-sm-4 control-label">Formalin</label>
						      <div class="col-xs-6 col-sm-8">'.(($q2->formalin == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Boraks</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->boraks == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Rhodamin B</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->rhodamin == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Methanil Yellow</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->methanil == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Pestisida</label>
						      <div class="col-xs-6  col-sm-8">'.(($q2->pestisida == "1")? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>").'</div>
						    </div>
					 	</div>
				      </div>';
		}

		echo $output;		      

	}

	function detailpirt(){
		$q2 = $this->db->query("select a.*, b.nama as pemohon, b.no_ktp, c.jenis_pangan as jns_pangan ,d.nama_kemasan, date_format(a.register,'%d/%m/%Y') as tgl from sertifikasi_pirt a
								left join user b on a.id_user = b.iduser
								left join ref_jenispangan_user c on c.idjenispangan = a.jenis_pangan
								left join ref_jeniskemasan d on d.idjeniskemasan = a.jenis_kemasan
								where a.id = '".$_POST['id']."'")->row();

		$output = '  <div class="modal-body">
				       	<div class="container-fluid">
						    <div class="row">
						     <label class="col-xs-6 col-sm-4 control-label">Nama pemohon</label>
						      <div class="col-xs-6 col-sm-8">'.$q2->pemohon.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">No. KTP</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->no_ktp.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Tgl pengajuan</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->tgl.'</div>
						    </div>
						    <hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Pedagang</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Nama pedagang</td>
						 			<td class="col-xs-6  col-sm-8">'.$q2->nama.'</td>
						 		</tr>
						 		<tr>
						 			<td>Alamat pedagang</td>
						 			<td>'.$q2->alamat.'</td>
						 		</tr>
						 		<tr>
						 			<td>No.HP</td>
						 			<td>'.$q2->nohp.'</td>
						 		</tr>
						 	</table>

						 	<hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Perusahaan</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Nama perusahaan</td>
						 			<td class="col-xs-6  col-sm-8">'.$q2->nama_perusahaan.'</td>
						 		</tr>
						 		<tr>
						 			<td>Alamat perusahaan</td>
						 			<td>'.$q2->alamat_usaha.', '.$q2->kodepos.', telp: '.$q2->notelp.'</td>
						 		</tr>
						 		<tr>
						 			<td>Bulan tahun berdiri</td>
						 			<td> Bulan '.$q2->bulan.'/ Tahun '.$q2->tahun.'</td>
						 		</tr>
						 	</table>

						 	<hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Produk</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Merek</td>
						 			<td class="col-xs-6  col-sm-8">'.$q2->merek.'</td>
						 		</tr>
						 		<tr>
						 			<td>Jenis pangan</td>
						 			<td>'.$q2->jns_pangan.'</td>
						 		</tr>
						 		<tr>
						 			<td>Netto (berat bersih)</td>
						 			<td>'.$q2->netto.'/ '.$q2->nama_kemasan.'</td>
						 		</tr>
						 		<tr>
						 			<td>Kode produksi</td>
						 			<td>'.$q2->kode_produksi.'</td>
						 		</tr>
						 		<tr>
						 			<td>Proses produksi</td>
						 			<td>'.$q2->proses.'</td>
						 		</tr>
						 		<tr>
						 			<td>Kadaluarsa</td>
						 			<td>'.$q2->expired.'</td>
						 		</tr>
						 		<tr>
						 			<td>Komposisi produk</td>
						 			<td>'.$q2->komposisi.'</td>
						 		</tr>
						 		<tr>
						 			<td>Cara produksi</td>
						 			<td>'.$q2->cara_produksi.'</td>
						 		</tr>
						 		<tr>
						 			<td>Lain-lain</td>
						 			<td>'.$q2->lain_lain.'</td>
						 		</tr>
						 	</table>
					 	</div>
				      </div>';

		echo $output;				      
	}

	function detailpss(){
		$q2 = $this->db->query("select a.*, b.nama as pemohon, b.no_ktp, c.nama as nm, date_format(a.register,'%d/%m/%Y') as tgl from sertifikasi_pss a
								left join user b on a.id_user = b.iduser
								left join ref_nama_usaha c on a.nama_usaha = c.id
								where a.id = '".$_POST['id']."'")->row();

		$output = '  <div class="modal-body">
				       	<div class="container-fluid">
						    <div class="row">
						     <label class="col-xs-6 col-sm-4 control-label">Nama pemohon</label>
						      <div class="col-xs-6 col-sm-8">'.$q2->pemohon.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">No. KTP</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->no_ktp.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Tgl pengajuan</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->tgl.'</div>
						    </div>
						    <hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Pedagang</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Nama pedagang</td>
						 			<td class="col-xs-6  col-sm-8">'.$q2->nama.'</td>
						 		</tr>
						 		<tr>
						 			<td>Alamat pedagang</td>
						 			<td>'.$q2->alamat.'</td>
						 		</tr>
						 		<tr>
						 			<td>No.HP</td>
						 			<td>'.$q2->nohp.'</td>
						 		</tr>
						 	</table>

						 	<hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Usaha</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Jenis usaha</td>
						 			<td class="col-xs-6  col-sm-8">'.(($q2->nama_usaha == '11')? $q2->lain_lain : $q2->nm).'</td>
						 		</tr>
						 		<tr>
						 			<td>Alamat usaha</td>
						 			<td>'.$q2->alamat_usaha.'</td>
						 		</tr>
						 	</table>
					 	</div>
				      </div>';

		echo $output;				      
	}

	function detailpsat(){
		$q2 = $this->db->query("select a.*, b.nama as pemohon, b.no_ktp, d.`nama` as rantai, c.`nama` as jenis, date_format(a.register,'%d/%m/%Y') as tgl from sertifikasi_psat a
								left join user b on a.id_user = b.iduser
								left join `ref_rantaipangan` d on a.rantai_pangan = d.`id`
								left join `ref_jenispangan` c on a.jenis_pangan = c.`id`
								where a.id = '".$_POST['id']."'")->row();

		$output = '  <div class="modal-body">
				       	<div class="container-fluid">
						    <div class="row">
						     <label class="col-xs-6 col-sm-4 control-label">Nama pemohon</label>
						      <div class="col-xs-6 col-sm-8">'.$q2->pemohon.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">No. KTP</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->no_ktp.'</div>
						    </div>
						    <div class="row">
						     <label class="col-xs-6  col-sm-4  control-label">Tgl pengajuan</label>
						      <div class="col-xs-6  col-sm-8">'.$q2->tgl.'</div>
						    </div>
						    <hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Pedagang</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Nama pedagang</td>
						 			<td class="col-xs-6  col-sm-8">'.$q2->nama.'</td>
						 		</tr>
						 		<tr>
						 			<td>Alamat pedagang</td>
						 			<td>'.$q2->alamat.'</td>
						 		</tr>
						 		<tr>
						 			<td>No.HP</td>
						 			<td>'.$q2->nohp.'</td>
						 		</tr>
						 	</table>

						 	<hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Usaha</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Nama dagang</td>
						 			<td class="col-xs-6  col-sm-8">'.$q2->nama_dagang.'</td>
						 		</tr>
						 		<tr>
						 			<td>Alamat usaha</td>
						 			<td>'.$q2->alamat_usaha.'</td>
						 		</tr>
						 	</table>

						 	<hr class="divider">
						    <div class="row">
						     <p class="col-xs-12 col-sm-12">Informasi Usaha</p>
						    </div>
						 	<table class="table table-striped">
						 		<tr>
						 			<td class="col-xs-6 col-sm-4">Merk dagang</td>
						 			<td class="col-xs-6  col-sm-8">'.$q2->merk_dagang.'</td>
						 		</tr>
						 		<tr>
						 			<td>Rantai pangan</td>
						 			<td>'.$q2->rantai.'</td>
						 		</tr>
						 		<tr>
						 			<td>Jenis pangan</td>
						 			<td>'.$q2->jenis.'</td>
						 		</tr>
						 		<tr>
						 			<td>Nama pangan</td>
						 			<td>'.$q2->nama_pangan.'</td>
						 		</tr>
						 		<tr>
						 			<td>Jenis kemasan</td>
						 			<td>'.$q2->jenis_kemasan.'</td>
						 		</tr>
						 	</table>

					 	</div>
				      </div>';

		echo $output;				      
	}
}