<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sdm extends MY_Controller {
	var $modules = "sdm";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('sdm_mod');
	}
	
	function index(){
		if ($this->session->userdata('is_login_portal_disnakerbogor'))
		{
			if($this->cekRole()==false){
				redirect('auth');
			}else{
				$this->load->view($this->modules.'/main',$this->data);
			}
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}

	function login(){	
		$this->sdm->login();
		if($this->session->userdata('is_login_portal_disnakerbogor')){
			redirect('main');
		}else{
			redirect('/login');
		}
		
	}

	function logout(){
		$this->sdm->logout();
		$this->session->destroy();		
		redirect('index.php/login');
	}
	
	function graphStatistik(){
		$rs = $this->db->query("
			
				SELECT * from tb_pasien where year(log_daftar)='".$this->input->post('thn')."'
		");
		
		$jml= $rs->num_rows();
		$rs = $this->db->query("
			
				SELECT * from tb_periksa where year(log)='".$this->input->post('thn')."'
		");
		
		$jml2= $rs->num_rows();
		
		echo "Kategori,PASIEN BARU,PEMERIKSAAN\n";
		echo "ITEM,".$jml.",".$jml2."\n";
	}
	
	function graph(){
	$tgl_1 = explode("-",$this->input->post('thn'));
	$tglawal = $tgl_1[0];
	$blnawal = $tgl_1[1];
	$thnawal = $tgl_1[2];

	$tgl_2 = explode("-",$this->input->post('thn2'));
	$tglawal2 = $tgl_2[0];
	$blnawal2 = $tgl_2[1];
	$thnawal2 = $tgl_2[2];

		$rs = $this->db->query("select SUM(a.jml) as jml,b.nama_barang from barang_keluar_detail a 
		left join barang b on a.idbarang=b.id
		left join barang_keluar c on a.no_keluar=c.no_keluar
		where c.tgl_keluar between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."'
		and b.idjenis='".$this->input->post('idjenis')."' group by a.idbarang order by SUM(a.jml) desc limit 20");
		
		$tanggal="'Nama Barang',";
		$harga_beli_faktur="Jumlah Penjualan,";
		foreach($rs->result() as $item){
			$harga_beli_faktur .= "".$item->jml.",";
			$tanggal .= "".$item->nama_barang.",";
			
		}
		
		$tanggal = substr($tanggal,0,strlen($str)-1);
		$harga_beli_faktur = substr($harga_beli_faktur,0,strlen($str)-1);
		echo $tanggal."\n";
		echo $harga_beli_faktur."\n";
		//echo $harga_jual."\n";
		
		}
	
	function graph2(){
	$tgl_1 = explode("-",$this->input->post('thn'));
	$tglawal = $tgl_1[0];
	$blnawal = $tgl_1[1];
	$thnawal = $tgl_1[2];

	$tgl_2 = explode("-",$this->input->post('thn2'));
	$tglawal2 = $tgl_2[0];
	$blnawal2 = $tgl_2[1];
	$thnawal2 = $tgl_2[2];

	
		$rs = $this->db->query("select SUM(a.jml) as jml,b.nama_barang from barang_keluar_detail a
		left join barang b on a.idbarang=b.id 
		left join barang_keluar c on a.no_keluar=c.no_keluar
		where c.tgl_keluar between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."'
		group by a.idbarang order by SUM(a.jml) desc limit 20");
		$tanggal="'Nama Barang',";
		$harga_beli_faktur="Jumlah Penjualan,";
		foreach($rs->result() as $item){
			$harga_beli_faktur .= "".$item->jml.",";
			$tanggal .= "".$item->nama_barang.",";
			
		}
		
		$tanggal = substr($tanggal,0,strlen($str)-1);
		$harga_beli_faktur = substr($harga_beli_faktur,0,strlen($str)-1);
		echo $tanggal."\n";
		echo $harga_beli_faktur."\n";
		//echo $harga_jual."\n";
		
		}
	function graph3(){
	$tgl_1 = explode("-",$this->input->post('thn'));
	$tglawal = $tgl_1[0];
	$blnawal = $tgl_1[1];
	$thnawal = $tgl_1[2];

	$tgl_2 = explode("-",$this->input->post('thn2'));
	$tglawal2 = $tgl_2[0];
	$blnawal2 = $tgl_2[1];
	$thnawal2 = $tgl_2[2];

		$rs = $this->db->query("SELECT SUM(b.jml) as jmlh from barang_masuk a left join barang_masuk_detail b on a.no_masuk=b.no_masuk where a.tgl_masuk between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."'");
		$jml= $rs->row();
		$rs1 = $this->db->query("SELECT SUM(b.jml) as jmlh from barang_keluar a left join barang_keluar_detail b on a.no_keluar=b.no_keluar where a.tgl_keluar between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."' ");
		$jml2= $rs1->row();
		
		echo "Kategori,BARANG MASUK,BARANG KELUAR\n";
		echo "ITEM,".$jml->jmlh.",".$jml2->jmlh."\n";
	}
	
	function graph4(){
	$tgl_1 = explode("-",$this->input->post('thn'));
	$tglawal = $tgl_1[0];
	$blnawal = $tgl_1[1];
	$thnawal = $tgl_1[2];

	$tgl_2 = explode("-",$this->input->post('thn2'));
	$tglawal2 = $tgl_2[0];
	$blnawal2 = $tgl_2[1];
	$thnawal2 = $tgl_2[2];

	

		$rs = $this->db->query("SELECT SUM(b.jml) as jmlh from barang_masuk a 
		left join barang_masuk_detail b on a.no_masuk=b.no_masuk 
		left join barang c on b.idbarang=c.id 
		where a.tgl_masuk between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."'
		and c.id='".$this->input->post('idjenis')."' ");
		$jml= $rs->row();
		$rs1 = $this->db->query("SELECT SUM(b.jml) as jmlh from barang_keluar a
		left join barang_keluar_detail b on a.no_keluar=b.no_keluar
		left join barang c on b.idbarang=c.id
		where a.tgl_keluar between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."'
		and c.id='".$this->input->post('idjenis')."'  ");
		$jml2= $rs1->row();
		
		echo "Kategori,BARANG MASUK,BARANG KELUAR\n";
		echo "ITEM,".$jml->jmlh.",".$jml2->jmlh."\n";
	}
	
	function graph5(){
	$tgl_1 = explode("-",$this->input->post('thn'));
	$tglawal = $tgl_1[0];
	$blnawal = $tgl_1[1];
	$thnawal = $tgl_1[2];

	$tgl_2 = explode("-",$this->input->post('thn2'));
	$tglawal2 = $tgl_2[0];
	$blnawal2 = $tgl_2[1];
	$thnawal2 = $tgl_2[2];

	

		$rs = $this->db->query("select c.username,SUM(b.subtotal) as jml from barang_keluar a
		left join barang_keluar_detail b on a.no_keluar=b.no_keluar
		left join mast_user c  on c.iduser=a.user_id
		where a.tgl_keluar between '".$tgl_1[2]."-".$tgl_1[1]."-".$tgl_1[0]."' and '".$tgl_2[2]."-".$tgl_2[1]."-".$tgl_2[0]."'
		group by a.user_id order by SUM(b.subtotal) desc limit 20");
		$tanggal="'Pelanggan',";
		$harga_beli_faktur="Rp,";
		foreach($rs->result() as $item){
			$harga_beli_faktur .= "".$item->jml.",";
			$tanggal .= "".$item->username.",";
			
		}
		
		$tanggal = substr($tanggal,0,strlen($str)-1);
		$harga_beli_faktur = substr($harga_beli_faktur,0,strlen($str)-1);
		echo $tanggal."\n";
		echo $harga_beli_faktur."\n";
		//echo $harga_jual."\n";
		
		}
	

}