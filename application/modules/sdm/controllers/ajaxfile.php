<?php

class Ajaxfile extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	
	function caribidang(){
		$keyword 	= $this->input->post('keyword');
		
		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where ( bidang like '%".$keyword."%' )";
		$q = "select * from mast_bidang $where";
		$rs = $this->db->query($q);
		
		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	function getjenispangan(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_jenispangan where id_rantaipangan = "'.$id.'"')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->nama.'</option>';
		}

		echo $output;
	}

	function getjenispangan2(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_jenispangan where id_rantaipangan like "%'.$id.'%"')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->nama.'</option>';
		}

		echo $output;
	}

	function gethasilpengawasan(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_hasilpengawasan where id_jenispangan = "'.$id.'"');
		$this->outputhasilpengawasan($rs);
	}

	function gethasilpengawasan2(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_hasilpengawasan2 where id_rantaipangan = "'.$id.'"');
		$this->outputhasilpengawasan($rs);
	}

	function gethasilpengawasanextend(){
		$id = $this->input->post('id');

		switch ($id) {
			case 'Beras Kemasan':
				$id = "2a";
				break;
			case 'Beras Curah':
				$id = "2b";
				break;
			case 'Daging Sapi':
				$id = "3a";
				break;
			case 'Daging Kerbau':
				$id = "3b";
				break;
			case 'Daging Unggas':
				$id = "3c";
				break;
			default:
				# code...
				break;
		}

		$rs = $this->db->query('select * from ref_hasilpengawasan where id_jenispangan = "'.$id.'"');
		$this->outputhasilpengawasan($rs);
	}

	function outputhasilpengawasan($rs){
		$row = $rs->num_rows();
		$section1 = round($row/2);

		$counter = 1;
		$output = '<div class="col-sm-12">';
		foreach ($rs->result() as $i) {
			$output .= '<div class="form-group">
							<label for="links_title" class="col-sm-4 control-label">'.$i->deskripsi.'</label>
							<input type="hidden" name="check_id[]" value="'.$i->id.'">
							<div class="col-sm-8">
								<select class="form-control" name="check[]"required>
									<option value="">-Silahkan Pilih-</option>
									<option value="Ya">Ya</option>
									<option value="Tidak">Tidak</option>
								</select>
							</div>
						</div>';

			if($counter == $section1){
				$output .= '</div><div class="col-sm-12">';
			}

			$counter++;
		}				

		$output .= '</div>';

		echo $output;
	}

	function getjenislokasi(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_pasar where modern = "'.$id.'" and bend = 1')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->nama.'</option>';
		}

		echo $output;
	}

	function getPasar(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_pasar_sub where id_uptd = "'.$id.'"')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->nama_pasar.'</option>';
		}

		echo $output;
	}

	function getKelurahan(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_kelurahan where idkec = "'.$id.'"')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->kelurahan.'</option>';
		}

		echo $output;
	}

	function getKantin(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_kantin where idkel = "'.$id.'"')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->kantin.'</option>';
		}

		echo $output;
	}

	function getPangan(){
		$id = $this->input->post('id');
		switch ($id) {
			case '1':
				$data1 = array('Bayam','Kangkung','Cabe','Kacang Panjang','Kobis');

				$output = '<option value="">-Silahkan Pilih-</option><optgroup label="Sayur">';
				foreach ($data1 as $item) {
					$output .=  '<option value="'.$item.'">'.$item.'</option>';
				}

				$data2 = array('Jeruk','Apel','Anggur','Blimbling','Jambu Air');
				$output .= '</optgroup><optgroup label="Buah">';
				foreach ($data2 as $item) { 
					$output .=  '<option value="'.$item.'">'.$item.'</option>';
				}

				$output .= '</optgroup>';

				break;

			case '2':
				$data = array('Beras Kemasan','Beras Curah');

				$output = '<option value="">-Silahkan Pilih-</option>';
				foreach ($data as $item) {
					$output .=  '<option value="'.$item.'">'.$item.'</option>';
				}
				break;

			case '3':
				$data = array('Daging Sapi','Daging Kerbau', 'Daging Unggas');

				$output = '<option value="">-Silahkan Pilih-</option>';
				foreach ($data as $item) {
					$output .=  '<option value="'.$item.'">'.$item.'</option>';
				}
				break;

			case '4':
				$data = array('Telur Unggas');

				$output = '<option value="">-Silahkan Pilih-</option>';
				foreach ($data as $item) {
					$output .=  '<option value="'.$item.'">'.$item.'</option>';
				}
				break;

			default:

				$output = '<option value="">-Tidak ada pilihan-</option>';
				break;
		}

		echo $output;
	}
	
}
?>