<script>
function loadGraphByJenis(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>sdm/page/data/lappelamar',
		data:{ 'thn':$('#thn').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});
}

$(document).ready(function(){
	try{
		
		$(document).on('click','a.exp-pdf',function(){
			if(confirm('Export PDF Data Transaksi..?')){
				$('#htmltoprint').val($('#result_data').html());
				$('#form_print').submit();
			}
			return false;
		});
		
		loadGraphByJenis();
		$('.yr').mask('9999');
		$('.yr').datepicker({
			format:'yyyy',
			minViewMode:2,
			autoclose:true
		}).on('changeDate',function(){
			$('.yr').datepicker('hide');
			loadGraphByJenis();
		});
	}catch(e){
		alert(e);
	}
	loadGraphByJenis();
});
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<section class="content">
	<div class="nav-tabs-custom">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Grafik Pelamar Diterima dan Tidak</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<div class="col-md-3">
							<div class="control-group">
								<label for="thn">Tahun</label>
								<div class="input-block">
									<input type="text" id="thn" name="thn" class='yr' value="<?=date('Y')?>"/> <span id="wait"></span>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" id="result">
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>
