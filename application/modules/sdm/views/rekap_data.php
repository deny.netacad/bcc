<script>
    $(document).ready(function(){

    });
</script>

<?php
       
    $jl = $_POST['jl'];

    if($jl == '0'){
        $q = "
            select b.nama_pasar as nama, a.lokasi2, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak' 
from (select lokasi2, lolos, count(id) as jml from survei_data WHERE (rantai_pangan = '5' or rantai_pangan = '11' or jenis_lokasi = '0') group by lokasi2, lolos) a
left join ref_pasar_sub b on a.lokasi2 = b.id
group by a.lokasi2
        ";
    }else if($jl == '1'){
        $q = "
        select b.nama, a.lokasi, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak' 
from (select lokasi, lolos, count(id) as jml from survei_data WHERE (rantai_pangan = '6' or rantai_pangan = '12' or jenis_lokasi = '1') group by lokasi, lolos) a
left join ref_pasar b on a.lokasi = b.id
group by a.lokasi
        ";
    }else if($jl == '2'){
        $q = "
        select concat_ws(' ',d.kantin, '<br>( ',c.kelurahan,', ', b.kecamatan, ' )') as nama, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak' 
from (select kecamatan, kelurahan, kantin, lolos, count(id) as jml from survei_data where rantai_pangan = '4' group by kantin, lolos) a 
left join ref_kecamatan b on a.kecamatan = b.id
left join ref_kelurahan c on a.kelurahan = c.id
left join ref_kantin d on a.kantin = d.id
group by a.kantin

        ";
    }else if($jl == '3'){
        $q = "
        select concat_ws('',b.kelurahan, ', ',c.kecamatan) as nama, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak'  
from (select kecamatan, kelurahan, lolos, count(id) as jml from survei_data where (rantai_pangan = '7') group by kelurahan, kecamatan, lolos) a
left join ref_kelurahan b on b.id = a.kelurahan
left join ref_kecamatan c on c.id = a.kecamatan
group by a.kelurahan, a.kecamatan


        ";
    }else if($jl == '4'){
        $q = "
select concat_ws('',b.kelurahan, ', ',c.kecamatan) as nama, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak'  
from (select kecamatan, kelurahan, lolos, count(id) as jml from survei_data where (rantai_pangan = '8') group by kelurahan, kecamatan, lolos) a
left join ref_kelurahan b on b.id = a.kelurahan
left join ref_kecamatan c on c.id = a.kecamatan
group by a.kelurahan, a.kecamatan
        

        ";
    }else if($jl == '5'){
        $q = "
select concat_ws('',b.kelurahan, ', ',c.kecamatan) as nama, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak'  
from (select kecamatan, kelurahan, lolos, count(id) as jml from survei_data where (rantai_pangan = '9') group by kelurahan, kecamatan, lolos) a
left join ref_kelurahan b on b.id = a.kelurahan
left join ref_kecamatan c on c.id = a.kecamatan
group by a.kelurahan, a.kecamatan
            

        ";
    }
    
?>

<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th align="center">#</th>
        <th>LOKASI</th>
        <th>JUMLAH</th>
        <th>MEMENUHI SYARAT</th>
         <th>TIDAK MEMENUHI SYARAT</th>
        <th>KESIMPULAN</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($this->db->query($q)->result() as $item){ $n++;
        $tms = $item->tidak/($item->lolos+$item->tidak)*100;
        $ms = $item->lolos/($item->lolos+$item->tidak)*100;

        if($ms >= 80){
            $ket = '<font color="green">Aman pangan</font>';
        }else{
            $ket = '<font color="red">Tidak aman pangan</font>';
        }
    ?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->nama?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->lolos+$item->tidak?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->lolos?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->tidak?></div>
        </td>
        <td>
            <div class="text-left">
                <div>TMS = <?=$item->tidak?>/<?=$item->lolos+$item->tidak?> = <?=$tms?>%</div>
                <div>MS = <?=$item->lolos?>/<?=$item->lolos+$item->tidak?> = <?=$ms?>%</div>
                <div><?=$ket?></div>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>