<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
		
	$(document).on('change','#jenis_lokasi', function(){
		// var id = $(this).val(); 
		// $('#lokasi').html('<option value=""> Loading ... </option>');
		// getJenisLokasi(id,'','');

		refreshData($(this).val());
		
	}).on('change','#lokasi', function(){
		var id = $(this).val(); 
		$('#lokasi2').html('<option value=""> Loading ... </option>');

		if($('#jenis_lokasi').val() == '0'){
			getPasar(id,'');	
		}else{
			refreshData('1',$(this).val());
		}
	}).on('change','#lokasi2', function(){
		refreshData('0',$(this).val());
	});

	function getJenisLokasi(id,idlokasi,idlokasi2){
		if(id == '1'){
			$('#pasar').hide();
			$('#region').show();
		}else{
			$('#pasar').show();
			$('#region').hide();
		}

    	$('#simpan').html('Loading data....');
		$('#simpan').attr('disabled',true);

    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/getjenislokasi',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				$('#lokasi').html(rs);

				if(idlokasi != ''){
					$('#lokasi').val(idlokasi);

					if(id == '0'){
						getPasar(idlokasi,idlokasi2);
					}
				}

				$('#simpan').html('Simpan');
				$('#simpan').attr('disabled',false);
			}
		});
    }

    function getPasar(id,array){
    	$('#simpan').html('Loading data....');
		$('#simpan').attr('disabled',true);

    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/getPasar',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				$('#lokasi2').html(rs);

				if(array != ''){
					$('#lokasi2').val(array);
				}

				$('#simpan').html('Simpan');
				$('#simpan').attr('disabled',false);
			}
		});
    }
	

	function refreshData(jenis_lokasi){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>pengawasan/page/data/rekap',
            data:{ 
				jl : jenis_lokasi, 
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Rekap</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Rekap Data Survei</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1">
				<div class="box-body">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="links_title" class="col-sm-2 control-label">Jenis Lokasi</label>
							<div class="col-sm-10">
								<select class="form-control" id="jenis_lokasi" name="jenis_lokasi" required="true">
									<option value="">-Silahkan Pilih-</option>
									<option value="0">Pasar Tradisional</option>
									<option value="1">Pasar Modern</option>
									<option value="2">Kantin</option>
									<option value="3">Pedagang Kaki Lima</option>
									<option value="4">Pedagang Keliling</option>
									<option value="5">Industri Rumah Tangga</option>
								</select>
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="links_target" class="col-sm-2 control-label">Lokasi</label>
							<div class="col-sm-10">
								<select class="form-control" id="lokasi" name="lokasi" required="true">
									<option value="">-Pilih Jenis Lokasi Dahulu-</option>
								</select>
							</div>
						</div>
						<div class="form-group" style="display:none;" id="pasar">
							<label for="links_target" class="col-sm-2 control-label">Nama Pasar</label>
							<div class="col-sm-10">
								<select class="form-control" id="lokasi2" name="lokasi2" >
									<option value="">-Pilih Lokasi Dahulu-</option>
								</select>
							</div>
						</div> -->
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>