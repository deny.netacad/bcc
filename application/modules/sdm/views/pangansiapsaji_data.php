<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " and (a.nama like '%".$cari."%' 
	 or b.nama like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select a.*,b.nama as nm from sertifikasi_pss a
    left join ref_nama_usaha b on a.nama_usaha = b.id
    where a.deleted = 0 ";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'pengawasan/page/data/pangansiapsaji';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("select a.*,b.nama as nm, date_format(a.register, '%d/%m/%Y') as tgl from sertifikasi_pss a
    left join ref_nama_usaha b on a.nama_usaha = b.id
    where a.deleted = 0   
		 $where limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th align="center">#</th>
        <th>NAMA PEDAGANG</th>
        <th>ALAMAT PEDAGANG</th>
        <th>NAMA USAHA</th>
        <th>ALAMAT USAHA</th>
        <th>TGL PENGAJUAN</th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;

	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->nama?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->alamat?></div>
        </td>
        <td>
            <div class="text-left">
                <?php if ($item->nama_usaha != '11'): ?>
                    <?=$item->nm?>
                <?php else: ?>
                    <?=$item->lain_lain?>
                <?php endif ?>
            </div>
        </td>
        <td>
            <div class="text-left"><?=$item->alamat_usaha?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->tgl?>  &nbsp;<a href="#" title="Detail" data="<?=$item->id?>"><i class=" text-black fa fa-eye"></i></a></div>
        </td>
        <td>
            <div class="text-left">
                <?php
                    if($item->verified == "0"){
                        echo "<font color='red'>Permohonan sertifikasi ditolak</font>";
                    }elseif ($item->verified == "1") {
                        echo "<font color='green'>Permohonan sertifikasi diterima</font>";
                    }else{
                        echo "<button class='btn btn-success btn-sm acc' data='".$item->id."'>Terima</button><button class='btn btn-danger btn-sm dec' style='margin-left:10px' data='".$item->id."'>Tolak</button>";
                    }
                ?>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>