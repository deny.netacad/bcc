<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-users"></i> <span>&nbsp;Report Pelamar</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>sdm/lappelamar"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Pelamar Berhasil dan Gagal</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-newspaper"></i> <span>&nbsp;Trend Lowongan Kerja</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>sdm/resumetrend"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Resume Trend Lowongan Kerja</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-address-book"></i> <span>&nbsp;Rekomendasi Pelatihan</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>sdm/rekomenpelatihan"><i class="text-orange icomoon-address-book"></i>&nbsp;Sistem Pelatihan Untuk Pelamar</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-calendar"></i> <span>&nbsp;Kelola Event Pelatihan</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>sdm/eventpelatihan"><i class="text-orange icomoon-calendar"></i>&nbsp;Event Pelatihan</a></li>
	</ul>
</li>