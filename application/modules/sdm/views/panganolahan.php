<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}
	
	
    $(document).ready(function(){	
    	refreshData();

		var config_image = {
            toolbar : 'Image',
            height: '200',
            width: '100%'
        };
		
		var field = $('#ujilab').html();
		var input = '<div class="form-group">'+
							'<label for="lain_lain" class="col-sm-2 control-label">Nama usaha lainnya</label>'+
							'<div class="col-sm-10">'+
								'<input type="text" class="form-control" id="lain_lain" name="lain_lain" placeholder="Nama usaha" required="true">'+
							'</div>'+
						'</div>';

		$('#tgl_survei').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		});
		
        $('#links_thumb').ckeditor(config_image);
		
		$('#per_page').change(function(){
				refreshData();
		});
		
		$('#cari').keyup(function(){
				refreshData();
		});
		
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
        	$('#simpan').html('Loading....');
			$('#simpan').attr('disabled',true);

            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                       	if(ret.success){
                        	$('#form1').trigger('reset');
                       	}
                        alert(ret.text);
                        console.log(ret.text);


                        $('#simpan').html('Simpan');
						$('#simpan').attr('disabled',false);
                    });
            }
			$('#id_edit').val('');
        });
		
		
        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			CKupdate();
			$('#id_edit').val('');
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
        });
		

		$(document).on('click','a.ajax-replace', function(){
			
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),  '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
         },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		
		
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>sertifikasi/page/editpss',
				type: 'post',
				data: { 
					'id':$this.eq(index).attr('data'), 
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					// console.log(response);
					var ret = $.parseJSON(response);

					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					$('#id_edit').val(ret.id);

					if(ret.nama_usaha == '11'){
						$('#lain-lain').html(input);
					}else{
						$('#lain-lain').html('');
					}

					for(attrname in ret){
						$('#'+attrname).val(ret[attrname]);
					}
				}
			});
        });
		
		$(document).on('click','a[title=Detail]', function(){
			var $this = $('a[title=Detail]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>pengawasan/page/detailpirt',
				type: 'post',
				data: { 
					'id':$this.eq(index).attr('data')
					,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					$('#output').html(response);
					$('#details').modal({
						 backdrop: 'static',
		    			keyboard: false
					});
				}
			});


        }).on('click','.acc', function(){
			if(confirm("Apa anda yakin ingin menerima permohonan sertifikasi ini?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>pengawasan/page/setverif',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						table: 'pirt',
						val : '1'
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		}).on('click','.dec', function(){
			if(confirm("Apa anda yakin ingin menolak permohonan sertifikasi ini?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>pengawasan/page/setverif',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						table:'pirt',
						val : '0'
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		})
	
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					refreshData();
				break;
				case 1:
					refreshData();
				break;
			}
		});

		$(document).on('change','#nama_usaha', function(){
			var id = $(this).val(); 

			if(id == '11'){
				$('#lain-lain').html(input);
			}else{
				$('#lain-lain').html('');
			}
			
		});

	});

function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>pengawasan/page/data/panganolahan',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab"><!-- 
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li> -->
		  <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Tambah Sertifikasi PSS (Pangan Siap Saji)</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>sertifikasi/page/savepss">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="nama" class="col-sm-2 control-label">Nama lengkap</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap sesuai dengan KTP" required="true">
							</div>
						</div>
						<div class="form-group">
							<label for="alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat lengkap sesuai dengan KTP" required="true">
							</div>
						</div>
						<div class="form-group">
							<label for="nohp" class="col-sm-2 control-label">No. HP</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="nohp" name="nohp" placeholder="No. Handphone yg masih aktif" required="true">
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div id="ujilab">
					<div class="box-header with-border" style="padding-top: 20px">
						<h3 class="box-title">Informasi Usaha</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="nama_usaha" class="col-sm-2 control-label">Nama usaha</label>
								<div class="col-sm-10">
									<select class="form-control" id="nama_usaha" name="nama_usaha" required="true">
										<option value="">-Silahkan Pilih-</option>
										<?php
											$option = $this->db->query('select * from ref_nama_usaha')->result();
											foreach ($option as $opt) {
												echo '<option value="'.$opt->id.'">'.$opt->nama.'</option>';
											}
										?>
									</select>
								</div>
							</div>
							<div id="lain-lain">
								
							</div>
							<div class="form-group">
								<label for="alamat_usaha" class="col-sm-2 control-label">Alamat usaha</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="alamat_usaha" name="alamat_usaha" placeholder="Alamat lengkap tempat usaha" required="true">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right" id="simpan">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane active" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Daftar Permohonan Sertifikasi PIRT(Pangan Olahan)</h3>
					  <div class="box-tools">
					  	<div class="input-group pull-right" style="width: 120px;margin-left: 10px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:100px;" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						  </div>
						</div>
						<div class="btn-group pull-right">
							<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:50px">
								<option value="">-</option>
								<option value="4">4</option>
								<option value="25">25</option>
								<option value="50">50</option>
							</select>
						</div>
					</div><!-- /.box-header -->
					
					</div>
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>


<!-- Modal -->
<div class="modal fade" id="details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Detail Permohonan Sertifikasi PIRT (Pangan Olahan)</h4>
      </div>
      <div id="output">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>