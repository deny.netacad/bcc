<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');
    $pasar = $this->input->post('pasar');

    if($cari!="") $where.= " and (a.nama_pedagang like '%".$cari."%' 
	 or a.jenis_pangan like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select a.id,a.nama_pedagang,a.lolos,b.*,c.nama from survei_data a 
         left join survei_ujilab b on a.id = b.id_survei_data
         left join ref_jenispangan c on c.id = a.jenis_pangan where a.is_delete = '0' 
         and a.jenis_data = 'prodsegar'  
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'pengawasan/page/data/prodsegar';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("select a.id,a.nama_pedagang,a.jenis_pangan,a.lolos,a.pangan,b.*,c.nama from survei_data a 
         left join survei_ujilab b on a.id = b.id_survei_data
         left join ref_jenispangan c on c.id = a.jenis_pangan
         where a.is_delete = '0' and a.jenis_data = 'prodsegar' 
		 $where
		ORDER BY a.id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th align="center">#</th>
        <th>NAMA PEDAGANG/ PENANGGUNG JAWAB</th>
        <th>JENIS PANGAN</th>
        <th>FORMALIN</th>
        <th>BORAKS</th>
        <th>RHODAMIN B</th>
        <th>METHANIL YELLOW</th>
        <th>PESTISIDA</th>
		<th>STATUS</th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;

	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->nama_pedagang?></div>
        </td>
        <td>
            <div class="text-left"><?=(($item->jenis_pangan == '1' || $item->jenis_pangan == '2' || $item->jenis_pangan == '3')?  $item->pangan.'<br><i>('.$item->nama.')</i>' : $item->nama)?></div>
        </td>
         <td>
            <div class="text-left"><?=(($item->formalin == "" || $item->formalin == "1" )? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>" )?></div>
        </td>
        <td>
            <div class="text-left"><?=(($item->boraks == "" || $item->boraks == "1" )? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>" )?></div>
        </td>
        <td>
            <div class="text-left"><?=(($item->rhodamin == "" || $item->rhodamin == "1" )? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>" )?></div>
        </td>
        <td>
            <div class="text-left"><?=(($item->methanil == "" || $item->methanil == "1" )? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>" )?></div>
        </td>
         <td>
            <div class="text-left"><?=(($item->methanil == "" || $item->pestisida == "1" )? "<i class='fa fa-close' style='color:red'></i>" : "<i class='fa fa-check' style='color:green'></i>" )?></div>
        </td>
        <td>
            <div class="text-left">
                <?php
                    if($item->lolos == "0"){
                        echo "<font color='red'>Tidak Memenuhi Syarat</font>";
                    }elseif ($item->lolos == "1") {
                        echo "<font color='green'>Memenuhi Syarat</font>";
                    }else{
                        echo "<button class='btn btn-success btn-sm acc' data='".$item->id."'>Lolos</button><button class='btn btn-danger btn-sm dec' style='margin-left:10px' data='".$item->id."'>Tidak Lolos</button>";
                    }
                ?>
            </div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->id?>"><i  class=" text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->id?>"><i class=" text-red fa fa-trash"></i></a>
                <a href="#" title="Detail" data="<?=$item->id?>"><i class=" text-black fa fa-eye"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>