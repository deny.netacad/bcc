<?php
$thn = $this->input->post('thn');
$rs = $this->db->query("
SELECT a.thn,a.sts,CASE 
    WHEN a.sts = 0
     THEN 'Ditolak'
     WHEN a.sts = 1
     THEN 'Diterima'
     ELSE 'Diproses' END as statusnya
,SUM(IF(a.bln=1,a.jmlpelamar,0)) AS jan
,SUM(IF(a.bln=2,a.jmlpelamar,0)) AS feb
,SUM(IF(a.bln=3,a.jmlpelamar,0)) AS mar
,SUM(IF(a.bln=4,a.jmlpelamar,0)) AS apr
,SUM(IF(a.bln=5,a.jmlpelamar,0)) AS mei
,SUM(IF(a.bln=6,a.jmlpelamar,0)) AS jun
,SUM(IF(a.bln=7,a.jmlpelamar,0)) AS jul
,SUM(IF(a.bln=8,a.jmlpelamar,0)) AS agu
,SUM(IF(a.bln=9,a.jmlpelamar,0)) AS sep
,SUM(IF(a.bln=10,a.jmlpelamar,0)) AS okt
,SUM(IF(a.bln=11,a.jmlpelamar,0)) AS nov
,SUM(IF(a.bln=12,a.jmlpelamar,0)) AS des
FROM(
SELECT a.status AS sts,YEAR(a.log) AS thn,MONTH(a.log) AS bln,COUNT(a.id_pelamar) AS jmlpelamar FROM ngi_jobapplied a WHERE YEAR(a.log)='".$thn."'
GROUP BY a.status,YEAR(a.log),MONTH(a.log)
ORDER BY a.status
)a 
GROUP BY a.sts desc
");
$data = '';
foreach($rs->result() as $item){
	$data.= '{	name: \''.$item->statusnya.'\',	data: ['.$item->jan.', '.$item->feb.', '.$item->mar.', '.$item->apr.', '.$item->mei.', '.$item->jun.', '.$item->jul.', '.$item->agu.', '.$item->sep.', '.$item->okt.', '.$item->nov.', '.$item->des.'] },';
}
?>

<script>
$(document).ready(function(){
	Highcharts.setOptions({
		lang: {
		  decimalPoint: '.',
		  thousandsSep: ','
		}
	});

	Highcharts.chart('container', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'GRAFIK PELAMAR DITERIMA DAN TIDAK'
		},
		subtitle: {
			text: 'Source: disnakertrans kab.bogor'
		},
		xAxis: {
			categories: [
				'Jan',
				'Feb',
				'Mar',
				'Apr',
				'May',
				'Jun',
				'Jul',
				'Aug',
				'Sep',
				'Oct',
				'Nov',
				'Dec'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah Pelamar (Orang)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [ <?=substr($data,0,-1)?>]
	});
});
</script>
<div id="container"></div>
<form id="form_print" name="form_print" action="<?=base_url()?>sdm/page/pdf/lappelamar?o=L" method="post" target="_blank" enctype="multipart/form-data">
	<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
	<textarea id="htmltoprint" name="htmltoprint" style="display:none"></textarea>
</form>
<div class="box-body">
	<div class="form-group">
		<div class="col-md-12">
			<a href="#" class="exp-pdf btn btn-info"> Cetak PDF Laporan <i class="text-white icomoon-file-pdf"></i></a>
		</div>
	</div>
</div>

<div class="col-md-12" id="result_data" style="padding-left:20px;padding-right:20px;overflow:auto">
<h3>Statistik Pelamar Diterima dan Tidak <?=$thn?></h3>
<table class="table table-bordered" style="width:100%">
	<thead>
		<tr style="background:#81D4FA;">
			<th style="width:30px;color:white">NO</th>
			<th style="color:white">STATUS</th>
			<th style="text-align:center;width:90px;color:white">JAN</th>
			<th style="text-align:center;width:90px;color:white">FEB</th>
			<th style="text-align:center;width:90px;color:white">MAR</th>
			<th style="text-align:center;width:90px;color:white">APR</th>
			<th style="text-align:center;width:90px;color:white">MEI</th>
			<th style="text-align:center;width:90px;color:white">JUN</th>
			<th style="text-align:center;width:90px;color:white">JUL</th>
			<th style="text-align:center;width:90px;color:white">AGU</th>
			<th style="text-align:center;width:90px;color:white">SEP</th>
			<th style="text-align:center;width:90px;color:white">OKT</th>
			<th style="text-align:center;width:90px;color:white">NOV</th>
			<th style="text-align:center;width:90px;color:white">DES</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$n = 0;
		foreach($rs->result() as $item){ $n++;
		$tot1[] = $item->jan;
		$tot2[] = $item->feb;
		$tot3[] = $item->mar;
		$tot4[] = $item->apr;
		$tot5[] = $item->mei;
		$tot6[] = $item->jun;
		$tot7[] = $item->jul;
		$tot8[] = $item->agu;
		$tot9[] = $item->sep;
		$tot10[] = $item->okt;
		$tot11[] = $item->nov;
		$tot12[] = $item->des;
		?>
		<tr>
			<td><?=$n?>.</td>
			<td> <?php
                    if($item->sts == "0"){
                        echo "<font color='red'>Ditolak</font>";
                    }elseif ($item->sts == "1") {
                        echo "<font color='green'>Diterima</font>";
                    }else{
                         echo "<font color='black'>Diproses</font>";
                    }
                ?></td>
			<td align="right"><?=number_format($item->jan)?></td>
			<td align="right"><?=number_format($item->feb)?></td>
			<td align="right"><?=number_format($item->mar)?></td>
			<td align="right"><?=number_format($item->apr)?></td>
			<td align="right"><?=number_format($item->mei)?></td>
			<td align="right"><?=number_format($item->jun)?></td>
			<td align="right"><?=number_format($item->jul)?></td>
			<td align="right"><?=number_format($item->agu)?></td>
			<td align="right"><?=number_format($item->sep)?></td>
			<td align="right"><?=number_format($item->okt)?></td>
			<td align="right"><?=number_format($item->nov)?></td>
			<td align="right"><?=number_format($item->des)?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
	<tfoot>
		<tr style="background:#4FC3F7">
			<th colspan="2" style="text-align:right;color:white">Total</th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot1))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot2))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot3))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot4))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot5))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot6))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot7))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot8))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot9))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot10))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot11))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot12))?></th>
		</tr>
	</tfoot>
</table>
</div>