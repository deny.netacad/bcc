<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $modules = "bcc";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('bcc/user');
		$this->load->model('bcc/bcc_list');
		
	}

	function index(){
		if ($this->session->userdata('is_login_portal_disnakerbogor'))
		{
			if($this->cekRole()==false){
				redirect('auth');
			}else{
				$this->load->view($this->modules.'/main',$this->data);
			}
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}
	
	//========== new ===========//
	function saveGantipass(){
		
		$data['iduser'] = $this->session->userdata('user_id');
		$data['userpass'] = md5($this->input->post('userpass'));
		$rs = $this->db->get_where("mast_user",$data);
		if($rs->num_rows()>0){
			if($this->input->post('userpass2')==$this->input->post('userpass3')){
				$dt['userpass'] = md5($this->input->post('userpass2'));
				if(!$this->db->update("mast_user",$dt,$data)){
					$ret['text'] = "Terjadi Kesalahan\n".$this->db->_error_message();
				}else{
					$ret['text'] = "Password telah diganti";
				}
			}else{
				$ret['text'] = "Password Perulangan salah";
			}
		}else{
			$ret['text'] = "Password lama salah";
		}
		echo json_encode($ret);
	}
	
	function saveManmodul(){
		$arrnot 	= array("","id_edit");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","idmodule");
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		$this->pengawasan_mod->save("mast_module",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout);
	}
	
	function editManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("mast_module",$data);
		echo json_encode($rs->row());
	}
	
	function delManmodul(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("mast_module",$data)){
			echo "Terjadi Kesalahan";
		}else{
			echo "Data Terhapus";
		}
	}

	


	
	
	function saveKategori(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		$this->bcc_mod->save("section_category",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	

	function editKategori(){
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		$rs = $this->db->get_where("section_category",$data);
		echo json_encode($rs->row());
	}
	
	function delKategori(){
		$ret['text'] = 'Item terhapus';
		foreach($_POST as $key=>$value){
			$data[$key] = $value;
		}
		if(!$this->db->delete("section_category",$data)){
			$ret['text'] = "Terjadi kesalahan";
		}else{
			$ret['text'] = "Item terhapus";
		}
		echo json_encode($ret);
	}
	function saveEventVideo(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","video_id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		$data['iduser'] = $this->session->userdata('iduser');
		$data['idbidang'] = $this->session->userdata('idbidang');
		
		$this->bcc_mod->save("section_events_video",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	
	function editEventVideo(){
		$data['video_id'] = $this->input->post('video_id');
		$ret = $this->db->get_where('section_events_video', $data)->row();
		echo json_encode($ret);
	}
	
	function delEventVideo(){
		foreach($_POST as $key=>$value){
			$dt[$key] = $value;
		}
		if(!$this->db->delete("section_events_video",$dt)){
			$ret['text'] = 'Terjadi Kesalahan';
		}else{
			$ret['text'] = 'Item terhapus';
		}
		echo json_encode($ret);
	}
	
	function saveMenu(){
		$arrnot 	= array("","id_edit","ci_csrf_token");
		$keyedit 	= array("","id_edit");
		$keyindex 	= array("","id");
		$keydate	= array();
		$keyin		= array();
		$keynum		= array();
		$keyout		= array();
		
		
		$this->bcc_mod->save("menu",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data,null,$keynum);
	}
	

	function editManmenu(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('menu', $data)->row();
		echo json_encode($ret);
	}

	function delManmenu(){
		$data['id'] = $this->input->post('id');
		$dt['sub_id'] = $this->input->post('id');
		if(!$this->db->delete("menu",$data)){
			$ret['err']		= true;
			$ret['text']	= "Terjadi Kesalahan";
		}else{
			$this->db->delete("dokumen", $dt);
			$ret['err']		= false;
			$ret['text']	= "Data Tersimpan";
		}

		echo json_encode($ret);
	}

	function saveLowongankerja(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ngi_job',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ngi_job',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editmanlowongankerja(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ngi_job', $data)->row();
		echo json_encode($ret);
	}

	function delmanlowongankerja(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ngi_job",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	function saveKategorilowongan(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ngi_jobcategory',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ngi_jobcategory',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editmankategorilowongan(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ngi_jobcategory', $data)->row();
		echo json_encode($ret);
	}

	function delmankategorilowongan(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ngi_jobcategory",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}


	function saveEvent(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('mast_event',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('mast_event',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editmanevent(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('mast_event', $data)->row();
		echo json_encode($ret);
	}

	function delmanevent(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("mast_event",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	
	function saveJenisusaha(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('mast_jenisusaha',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('mast_lowongankerja',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editjenisusaha(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('mast_jenisusaha', $data)->row();
		echo json_encode($ret);
	}

	function deljenisusaha(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("mast_jenisusaha",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	
	
	
	function savePasar(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ref_pasar_sub',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_pasar_sub',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editPasar(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_pasar_sub', $data)->row();
		echo json_encode($ret);
	}

	function delPasar(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_pasar_sub",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	
	
	function savePasarModern(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			$data['bend'] = '1';
			$data['fend'] = '0';
			$data['modern'] = '1';

			if($this->db->insert('ref_pasar',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_pasar',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editPasarModern(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_pasar', $data)->row();
		echo json_encode($ret);
	}

	function delPasarModern(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_pasar",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

	function saveKantin(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ref_kantin',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_kantin',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editKantin(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_kantin', $data)->row();
		echo json_encode($ret);
	}

	function delKantin(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_kantin",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

	function getKelurahan(){
		$id = $this->input->post('id');
		$rs = $this->db->query('select * from ref_kelurahan where idkec = "'.$id.'"')->result();

		$output = '<option value="">-Silahkan Pilih-</option>';
		foreach ($rs as $item) {
			$output .=  '<option value="'.$item->id.'">'.$item->kelurahan.'</option>';
		}

		echo $output;
	}

	function saveKel(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('ref_kelurahan',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id',$_POST['id_edit'])->update('ref_kelurahan',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editKel(){
		$data['id'] = $this->input->post('id');
		$ret = $this->db->get_where('ref_kelurahan', $data)->row();
		echo json_encode($ret);
	}

	function delKel(){
		$data['id'] = $this->input->post('id');
		if($this->db->delete("ref_kelurahan",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	
	function detailpelamar(){
		$q2 = $this->db->query("SELECT DATE_FORMAT(a.log,'%d %M,%Y') AS tgl_daftar,b.*,d.nama_job AS nm_job FROM ngi_jobapplied a
LEFT JOIN user_pelamar b ON a.id_pelamar=b.iduser
LEFT JOIN ngi_joblist c ON a.id_job=c.id
LEFT JOIN ngi_job d ON c.posisi=d.id WHERE a.iduser = '".$_POST['iduser']."' ")->row();

		
		$output = 'sdsd';
		echo $output;		      

	}
	function setaktiv(){
		//$table = $_POST['table'];

		if($this->db->query('update ngi_perusahaan set verified_disnaker = "'.$_POST['val'].'",verified = "'.$_POST['val'].'" where idperusahaan = "'.$_POST['id'].'"')){
			echo 'Berhasil mengeset status';
		}else{
			echo 'Gagal mengeset status';
		}
	}

	function savepejabat(){
		foreach ($_POST as $key => $value) {
			if($key != 'id_edit'){
				$data[$key] = $value;
			}
		}

		if($_POST['id_edit'] == ""){
			if($this->db->insert('section_pejabat',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
			}
		}else{
			if($this->db->where('id_pejabat',$_POST['id_edit'])->update('section_pejabat',$data)){
				echo json_encode(array('success' => true, 'text' => 'Berhasil mengedit'));
			}else{
				echo json_encode(array('success' => false, 'text' => 'Gagal mengedit'));
			}
		}
	}
	

	function editpejabat(){
		$data['id_pejabat'] = $this->input->post('id_pejabat');
		$ret = $this->db->get_where('section_pejabat', $data)->row();
		echo json_encode($ret);
	}

	function delpejabat(){
		$data['id_pejabat'] = $this->input->post('id_pejabat');
		if($this->db->delete("section_pejabat",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	
	
	function regPendaftar(){
		$username = str_replace(" ","",$this->input->post('username',true));

		$input = array(
			'username' => $username,
			'email' => $this->input->post('email',true),
			'password' => md5($this->input->post('password',true)),
			'password_ori' => $this->input->post('password_ori',true),
			'nama' => $this->input->post('nama',true),
			'no_ktp' => $this->input->post('no_ktp',true),
			'tmp_lahir' => $this->input->post('tmp_lahir',true),
			'tgl_lahir' => $this->input->post('tgl_lahir',true),
			'gender' => $this->input->post('gender',true),
			'agama' => $this->input->post('agama',true),
			'wn' => $this->input->post('wn',true),
			'sts' => $this->input->post('sts',true),
			'pendidikan' => $this->input->post('pendidikan',true),
			// 'ins_pend' => $this->input->post('ins_pend',true),
			'jurusan' => $this->input->post('jurusan',true),
			'tahun_lulus' => $this->input->post('tahun_lulus',true),
			'kecamatan_id' => $this->input->post('kecamatan_id',true),
			'link_enkripsi' => md5($this->input->ip_address().strtotime('now')),
      		'profile_url' => time(),
      		'alamat' => $this->input->post('alamat',true),
      		'no_telp' => $this->input->post('no_telp',true)
		);

		if($this->db->insert('user_pelamar',$input)){
			$from_email = "noreply@bogorcareercenter.bogorkab.go.id";
        	$to_email = $_POST['email'];

			$pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			 <head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title>Demystifying Email Design</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body style="margin: 0; padding: 0;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				 <tr width="100%">
				 	<td width="0%"></td>
				  <td width="100%">
				   	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
			  <tr>
			  <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
			 	<img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
				</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
			 <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
			 <b >Verifikasi alamat email</b>
			</td>
			 </tr>
			 <tr>
			  <td style="padding: 20px 0 30px 0;" align="center">
			   Silahkan verifikasi alamat email anda dengan menekan tombol dibawah. Jika tidak, anda bisa mengunjungi url berikut '.base_url().'portal/page/verifikasiemailpelamar/'.md5($this->input->ip_address().strtotime('now')).'
			  </td>
			 </tr>
			 <tr>
			  <td align="center">
			   <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/page/verifikasiemailpelamar/'.md5($this->input->ip_address().strtotime('now')).'">Verifikasi!</a>
			  </td>
			 </tr>
			 </table>
			</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
			   <table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <tr>
			 <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
			 &reg; Bogor Career Center, 2019<br/>
			</td>
			  <td align="right">
			 
			</td>
			 </tr>
			</table>
			  </td>
			 </tr>
			 </table>
				  </td>
				  <td width="0%"></td>
				 </tr>
			</table>
			</body>
			</html>';

			$this->load->library('email');
			$config = array();
			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
			$config['smtp_user'] = 'superkarir@bcc.disnaker.bogorkab.go.id';
			$config['smtp_pass'] = 'Superkarir99';
			$config['smtp_port'] = 465;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $this->email->set_header("Content-Type", "text/html");
			$this->email->from($from_email, 'Bogor Career Center');
	        $this->email->to($to_email);
	        $this->email->subject('Bogor Career Center Verifikasi Email');
	        $this->email->message($pesan);
			#$result = $this->email->send();
			if($this->email->send()){
	   //          $email 	= $_POST['email'];
				// $tipe 	= 'pelamar';

				// $dokuform="<form name=\"param_pass\" id=\"param_pass\" method=\"post\" action=\"".base_url()."portal/verifikasi\">\n"; //example redirect link

				// $dokuform.="<input name=\"email\" type=\"hidden\" id=\"order_number\" value=\"$email\">\n";
				// $dokuform.="<input name=\"tipe\" type=\"hidden\" id=\"order_number\" value=\"$tipe\">\n";
				// $dokuform.="<input name=\"".$this->security->get_csrf_token_name()."\" type=\"hidden\" id=\"order_number\" value=\"".$this->security->get_csrf_hash()."\">\n";

				// $dokuform.="</form>\n";
				// $dokuform.="<script language=\"JavaScript\" type=\"text/javascript\">";
				// $dokuform.="document.getElementById('param_pass').submit();";
				// $dokuform.="</script>";

				// //FIREFOX FIX
				// $redirect_url = str_replace('&amp;', '&', $redirect_url);

				// printf($dokuform);
				echo json_encode(array('success' => true, 'text' => 'Berhasil menyimpan'));

	        }else{
	        	$this->db->query('delete from user_pelamar where link_enkripsi = '.md5($this->input->ip_address().strtotime('now')));
	            echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan alamat email tidak valid'));
			}
		}else{
    		echo json_encode(array('success' => false, 'text' => 'Gagal menyimpan'));
		}
	}
	
	function savepencaker(){
		$a = $this->input->post();

	if ($a['id_edit']=="") {
		unset($a['id_edit']);
		// echo json_encode($a);
		$mydate= $this->input->post('tgl_lahir');
		$date = DateTime::createFromFormat('d/m/Y', $mydate);
		$dateformat = $date->format('Y-m-d');
		$a['tgl_lahir'] = $dateformat;
		$a['password'] = md5($this->input->post('password'));
		$a['password_ori'] = $this->input->post('password');
		$a['link_enkripsi'] = md5($this->input->ip_address().strtotime('now'));
		$a['profile_url'] = time();
		$rs = $this->db->insert("user_pelamar",$a);

	}else{
		$rs = $this->db->query("select * from user_pelamar where iduser='".$this->input->post('id_edit')."'");
			$item = $rs->row();
			if($item->password!=$this->input->post('password')){
				$a['password'] = md5($this->input->post('password'));
				$a['password_ori'] = $this->input->post('password');
			}
		$this->db->where('iduser', $a['id_edit']);
		unset($a['id_edit']);
		$mydate= $this->input->post('tgl_lahir');
		$date = DateTime::createFromFormat('d/m/Y', $mydate);
		$dateformat = $date->format('Y-m-d');
		$a['tgl_lahir'] = $dateformat;
		$rs = $this->db->update("user_pelamar",$a);	
		

	}
		echo json_encode($a);
		// unset($a['id_edit']);
		// // echo json_encode($a);
		// $rs = $this->db->insert("user_pelamar_copy",$a);

		// echo json_encode($rs);

		// $arrnot 	= array("","id_edit","register","link_enkripsi","password"); # inputan yang tidak ada di table
		// $keyedit 	= array("","id_edit"); # index untuk keperluan edit data
		// $keyindex 	= array(); 
		// $keydate	= array();
		// $keyin		= array();
		// $keyout		= array();
		// if($this->input->post('id_edit')!=''){
		// 	$rs = $this->db->query("select * from user_pelamar where iduser='".$this->input->post('id_edit')."'");
		// 	$item = $rs->row();
		// 	if($item->password!=$this->input->post('password')){
		// 		$data['password'] = md5($this->input->post('password'));
		// 		$data['password_ori'] = $this->input->post('password');
		// 	}
		// }else{
		// 	$data['password'] = md5($this->input->post('password'));
		// 	$data['password_ori'] = $this->input->post('password');
		// 	//$data['password_ori'] = $this->input->post('password_ori');
		// 	$data['link_enkripsi'] = md5($this->input->ip_address().strtotime('now'));
  // 	 		$data['profile_url'] = time();
		// }
		
		// $this->bcc_mod->save("user_pelamar_copy",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data);
		// $str = $this->db->last_query();
		// print_r($str);
		// echo $this->session->userdata('iduser');
	}

	function delpencaker(){
		$data['iduser'] = $this->input->post('iduser');
		if($this->db->delete("user_pelamar",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}
	function editpencaker(){
		//  $data['iduser'] = $this->input->post('iduser');
		// $ret = $this->db->query('SELECT a.*,b.* from user_pelamar a left join ngi_kecamatan b on a.id_kecamatan=b.subdistrict_id where iduser="'..'" ')->row();
		// echo json_encode($ret);
			$data = $this->db->query("select * from user_pelamar where iduser='".$this->input->post('iduser')."'");
			$item = $data->row();
			$jml = $data->num_rows();
			if($jml > 0){
				foreach ($data->result() as $user) {
					if($user->province_id != NULL){
					
						$rs = $this->db->query("SELECT DATE_FORMAT(a.tgl_lahir,'%d/%m/%Y') AS tgl_lahir,a.pendidikan,a.alamat,a.`iduser`,a.`username`,a.`email`,a.`password`,a.`password_ori`,a.`nama`
						,a.`no_ktp`,a.`no_telp`,a.`tmp_lahir`,a.`gender`,a.`agama`,a.`wn`,a.`sts`,a.`jurusan`,
						a.`tahun_lulus`,a.`province_id`,a.`city_id`,a.`kecamatan_id`,a.`verified`,b.subdistrict_id AS kecamatan_id,b.subdistrict_name,
						b.`province_id`,b.`province`,b.`city_id`,b.`city` AS city_name,b.`type` FROM user_pelamar a 
						INNER JOIN ngi_kecamatan b ON a.kecamatan_id=b.subdistrict_id AND a.province_id=b.province_id
						AND a.`city_id`=b.`city_id` WHERE a.iduser='".$this->input->post('iduser')."'");

						echo json_encode($rs->row());
					}else{
						
						$rs = $this->db->query("SELECT DATE_FORMAT(a.tgl_lahir,'%d/%m/%Y') AS tgl_lahir,a.pendidikan,a.alamat,a.`iduser`,a.`username`,a.`email`,a.`password`,a.`password_ori`,a.`nama`
						,a.`no_ktp`,a.`no_telp`,a.`tmp_lahir`,a.`gender`,a.`agama`,a.`wn`,a.`sts`,a.`jurusan`,
						a.`tahun_lulus`,a.`province_id`,a.`city_id`,a.`kecamatan_id`,a.`verified` FROM user_pelamar a 
						 WHERE a.iduser='".$this->input->post('iduser')."' ");
						echo json_encode($rs->row());
					}
				}
			}else{
				echo json_encode("gagal mengedit");
			}
		
		
	}
	
	function saveperusahaan(){
		$arrnot 	= array("","id_edit","password","password_ori","link_enkripsi","fasilitas"); # inputan yang tidak ada di table
		$keyedit 	= array("","id_edit"); # index untuk keperluan edit data
		$keyindex 	= array("","idperusahaan"); 
		$keydate	= array();
		$keyin		= array();
		$keyout		= array();
		# custom value

		$data["fasilitas"] = implode(",", $this->input->post('fasilitas'));
		if($this->input->post('id_edit')!=''){
			$rs = $this->db->query("select * from ngi_perusahaan where idperusahaan='".$this->input->post('id_edit')."'");
			$item = $rs->row();
				$data['pass'] = $item->pass;
				$data['pass_ori'] = $item->pass_ori;
		}else{
			$data['pass'] = md5($this->input->post('pass'));
			$data['pass_ori'] = $this->input->post('pass_ori');
			$data['link_enkripsi'] = md5($this->input->ip_address().strtotime('now'));
      		$data['profile_url'] = time();
		}
		$this->bcc_mod->save("ngi_perusahaan",$arrnot,$keyedit,$keyindex,$keydate,$keyin,$keyout,$data);
	}

	function editPerusahaan(){
		//  $data['iduser'] = $this->input->post('iduser');
		// $ret = $this->db->query('SELECT a.*,b.* from user_pelamar a left join ngi_kecamatan b on a.id_kecamatan=b.subdistrict_id where iduser="'..'" ')->row();
		// echo json_encode($ret);
		$rs = $this->db->query("SELECT a.* from ngi_perusahaan a where a.idperusahaan = '".$this->input->post('idperusahaan')."' ");
		echo json_encode($rs->row());
	}
	

	function delPerusahaan(){
		$data['idperusahaan'] = $this->input->post('idperusahaan');
		if($this->db->delete("ngi_perusahaan",$data)){
			echo json_encode(array('success' => true, 'text' => 'Data terhapus'));
		}else{
			echo json_encode(array('success' => false, 'text' => 'Terjadi Kesalahan'));
		}
	}

	function cariKab(){
		$result = $this->db->query('select city_id, city, type from ngi_kecamatan where province_id = "'.$_POST['id'].'" group by city_id')->result();
		$output = '<option value="">Semua Kabupaten/Kota</option>';

		foreach ($result as $data) {
			if($data->city_id == $_POST['ids']){
				$output .= '<option value="'.$data->city_id.'" selected>'.$data->type.' '.$data->city.'</option>';
			}else{
				$output .= '<option value="'.$data->city_id.'">'.$data->type.' '.$data->city.'</option>';
			}

		}

		echo $output;
	}

	function cariKec(){
		$result = $this->db->query('select subdistrict_id, subdistrict_name from ngi_kecamatan where city_id = "'.$_POST['id'].'" group by subdistrict_id')->result();
		$output = '<option value="">Semua Kecamatan</option>';

		foreach ($result as $data) {
			if($data->subdistrict_id == $_POST['ids']){
				$output .= '<option value="'.$data->subdistrict_id.'" selected>'.$data->subdistrict_name.'</option>';
			}else{
				$output .= '<option value="'.$data->subdistrict_id.'">'.$data->subdistrict_name.'</option>';
			}

		}

		echo $output;
	}
	function uploadPencaker(){
		$filename = time().$_FILES['file']['name'];
		//$config['upload_path'] = '/home/brtsmg/public_html/v2/bayu/excel/';
		$config['upload_path'] = './assets/img/credit/';
		$config['allowed_types'] = 'xls';
		$config['max_size']	= '15000000';
		$config['file_name'] = $filename;
		if(!file_exists($config['upload_path'].$filename)){
			$this->load->library('upload',$config);
			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => true, 'teks' => $this->upload->display_errors());
				echo json_encode($error);
			}
			else
			{
				error_reporting(1);
				$data_upload = $this->upload->data();
				$inputFileName = './assets/img/credit/'.$filename;
			
				$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
				try {
						$inputFileType = IOFactory::identify($inputFileName);
						$objReader = IOFactory::createReader($inputFileType);
						$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
						die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
            
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
				for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
						$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);

						//Sesuaikan sama nama kolom tabel di database
						  $data = array(
							'username' => $rowData[0][1],
							'email' => $rowData[0][2],
							'no_ktp' => $rowData[0][3],
							'nama' => $rowData[0][4],
							'tgl_lahir' => $rowData[0][5],
							'gender' => $rowData[0][6],
							'sts' => $rowData[0][7],
							'pendidikan' => $rowData[0][8],
							'jurusan' => $rowData[0][9],
							'alamat' => $rowData[0][10],
							'no_telp' => $rowData[0][11],
							'profile_url' =>  md5($this->input->post('no_ktp')),
							'password'	=> md5($this->input->post('123')),
						);
						$this->db->insert("user_pelamar_coba",$data);
    			}
    			delete_files($data_upload['file_path']);
    			//echo $this->db->last_query();
    	        echo json_encode(array('error' => false, 'teks' => 'Berhasil mengunggah data'));
	    		//echo $this->db->last_query();
	    	}
    	}
	}
	
	
}