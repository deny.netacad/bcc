<script>

	function perusahaanFormatResult(data) {

		var markup= "<li>"+ data.nama_perusahaan+"</small</li>";

		return markup;

	}

	function perusahaanFormatSelection(data) {

		return data.nama_perusahaan;

	}
	$(document).ready(function(){
		$(document).on('change', 'select[name="prov"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>bcc/page/carikab',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kab"]').html(result);
                }
            })
        }).on('change', 'select[name="kab"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>bcc/page/carikec',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kec"]').html(result);
                }
            })
        })
	
		
		
		$(document).on('click','a.exp-pdf',function(){
			if(confirm('Export PDF Data ..?')){
				var input = $("<input>").attr("type", "hidden").attr("name", "pend").val($('#pend').val());
				$('#form_print').append(input);
				$('#form_print').submit();
				input.remove();
			}
			return false;
		});

		$(document).on('click','a.exp-excel',function(){
			if(confirm('Export Excel Data ..?')){
				var input = $("<input>").attr("type", "hidden").attr("name", "pend").val($('#pend').val());
				$('#form_print_excel').append(input);
				$('#form_print_excel').submit();
				input.remove();
			}
			return false;
		});

		$(document).on('click','a.exp-pdf-a',function(){
			if(confirm('Export PDF Data ..?')){
				$('#form1').attr('action','<?=base_url()?>bcc/page/pdflap/lappelamar');
				$('#form1').submit();
			}
			return false;
		});

		$(document).on('click','a.exp-excel-a',function(){
			if(confirm('Export Excel Data ..?')){
				$('#form1').attr('action','<?=base_url()?>bcc/page/view/lappelamar_excel');
				$('#form1').submit();
			}
			return false;
		});

		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			minViewMode:0,
			viewMode:1
		}).on('changeDate',function(){
			$('.dt').datepicker('hide');
			loadDataReturtgl();
		});

		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#tanggal_mulai').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
		
		$('#tanggal').focus();
	});





	/*
function refreshData(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>auth/page/data/pemeriksaandahak',
		data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});
}	
	*/
	

</script>
<section class="content">
	<div class="nav-tabs-custom">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Cetak Laporan Pelamar</h3>
					</div><!-- /.box-header -->

					<!-- form start -->
					<div class="box-body">
						<form style="padding-left: 15px;margin-bottom: 30px" id="form1" method="post" target="_blank" enctype="multipart/form-data">
							<div class="form-group col-md-12 col-sm-12 col-xs-12">
								<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
								<select name="pend" style="margin-right: 10px" id="pend">
									<option value="">Pilih Semua Pendidikan Terakhir</option>
	                                <option value="SD">SD/Sederajat</option>
									<option value="SMP">SMP/Sederajat</option>
									<option value="SMA">SMA/Sederajat</option>
                                    <option value="SMK">SMK/Sederajat</option>
									<option value="D1">D1/Sederajat</option>
									<option value="D2">D2/Sederajat</option>
									<option value="D3">D3/Sederajat</option>
									<option value="D4">D4/Sederajat</option>
									<option value="S1">S1/Sederajat</option>
									<option value="S2">S2/Sederajat</option>
									<option value="S3">S3/Sederajat</option>
								</select>
	                        </div>
							<div class="form-group col-md-12 col-sm-12 col-xs-12">
								<select name="prov" required="" style="margin-right: 10px">
									<option value="">Pilih Semua Daerah</option>
	                                <?php
	                                    $q = $this->db->query('select province, province_id from ngi_kecamatan group by province_id')->result();

	                                    foreach ($q as $i) {
	                                        echo '<option value="'.$i->province_id.'">'.$i->province.'</option>';
	                                    }

	                                ?>
								</select>

								<select name="kab" required="" style="margin-right: 10px">
	                                <option value="">Kabupaten / Kota</option>
	                            </select>

	                            <select name="kec" required="">
	                                <option value="">Kecamatan / Kelurahan</option>
	                            </select>
	                        </div>
	                        <a class="exp-pdf-a btn btn-info"> Cetak Laporan Pelamar (pdf)</a>
							<a class="exp-excel-a btn btn-info"> Cetak Laporan Pelamar (excel)</a>
						</form>
						<form id="form_print" name="form_print" action="<?=base_url()?>bcc/page/pdflap/lappelamar" method="post" target="_blank" enctype="multipart/form-data"></form>
						<form id="form_print_excel" name="form_print" action="<?=base_url()?>bcc/page/view/lappelamar_excel" method="post" target="_blank" enctype="multipart/form-data"></form>
					</div>
				</div>
			</div>
		</div><!-- /.tab-content -->
	</div>
</section>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 id="myModalLabel"><a id="btn-print2" title="Retur Detail"><i class="icomoon-image2"></i></a> Retur Detail <span id="item-desc"></span></h4>
  </div>
  <div class="modal-body">
    <div id="div-detail">
	
	</div>
  </div>
</div>
