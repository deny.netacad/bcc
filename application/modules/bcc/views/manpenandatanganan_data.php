<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
    $cari = $this->input->post('cari');
    
    if($cari!="") $where.= " where (nama like '%".$cari."%' or nip like '%".$cari."%')";
    
    $n = intval($this->uri->segment(5));
    $q = "select * from section_pejabat";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'bcc/page/data/manpenandatanganan';

    $config['total_rows']           = $totrows;
    $config['per_page']             = $per_page;
    $config['uri_segment']          = 5;
    $config['is_ajax_paging']       = TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("$q $where order by id_pejabat limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
    ");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
<?=$data['pagination']?>
</div>
<img id="noimage" style="display:none" />
<table class="table table-hover">
  <colgroup>
    <col class="con0" style="width:auto" />
    <col class="con0" style="width:auto" />
    <col class="con0" style="width:auto" />
    <col class="con0" style="width:auto" />
    <col class="con0" style="width:auto" />
    <col class="con0" style="width:auto" />
  </colgroup>
  <thead class="breadcrumb">
    <tr>
      <th>#</th>
      <th>NAMA</th>
      <th>NIP</th>
      <th>JABATAN</th>
      <th><div class="text-center">PROSES</div></th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
    ?>
    <tr>
      <td align="center"><?=$n?>.</td>
      <td>
      <span class="ed1" style="display:none"><?=$item->id?></span>
      <div class="text-left"><?=$item->nama?></div>
      </td>
      <td>
      <div class="text-left"><?=$item->nip?></div>
      </td>
        <td>
      <div class="text-left"><?=$item->keterangan?></div>
      </td>
      <td>
        <div class="text-center">
            <a href="#" title="Edit" data="<?=$item->id_pejabat?>"><i class="icomoon-pencil2"></i></a>
            <a href="#" title="Delete" data="<?=$item->id_pejabat?>"><i class="icomoon-remove red"></i></a>
        </div>
      </td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>
<span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
