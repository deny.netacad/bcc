<script>
$(document).ready(function(){
	$(document).on('click','a.exp-pdf',function(){
		if(confirm('Export PDF Data ..?')){
			$('#form_print').submit();
		}
		return false;
	});

	$(document).on('click','a.exp-excel',function(){
		if(confirm('Export Excel Data ..?')){
			$('#form_print_excel').submit();
		}
		return false;
	});
});
</script>
<section class="content">
    <div class="nav-tabs-custom">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Laporan Penyedia Lowongan</h3>
                    </div>
                    <div class="box-body">
                        <form id="form_print" name="form_print" action="<?=base_url()?>bcc/page/pdflap/lappenyedialowongan" method="post" target="_blank" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                                <div class="col-sm-12">
                                    </br>
                                    <a href="#" class="exp-pdf btn btn-info"> Cetak Laporan Penyedia Lowongan (pdf)</a>
                                </div>
                        </form>
                        <form id="form_print_excel" name="form_print" action="<?=base_url()?>bcc/page/view/lappenyedialowongan_excel" method="post" target="_blank" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                            </div>
                            <div class="col-sm-12">
                                </br>
                                <a href="#" class="exp-excel btn btn-info"> Cetak Laporan Penyedia Lowongan (excel)</a>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div class="row-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>