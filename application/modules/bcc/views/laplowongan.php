<script>

	function perusahaanFormatResult(data) {

		var markup= "<li>"+ data.nama_perusahaan+"</small</li>";

		return markup;

	}

	function perusahaanFormatSelection(data) {

		return data.nama_perusahaan;

	}
	$(document).ready(function(){
		$(".idperusahaan").select2({
			id: function(e) { return e.idperusahaan }, 
			placeholder: "Pilih Perusahaan",
			minimumInputLength: 0,
			multiple: false,
			allowClear:true,
			ajax: { 
				url: "<?=base_url()?>auth/ajaxfile/cariperusahaan",
				dataType: 'jsonp',
				type:'POST',
				quietMillis: 100,
				data: function (term, page) {
					return {
						keyword: term, //search term
						per_page: 200, // page size
						page: page, // page number
	
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					};
				},
				results: function (data, page) { 
					var more = (page * 100) < data.total;
					return {results: data.rows, more: more};
				}
			},
			formatResult: perusahaanFormatResult, 
			formatSelection: perusahaanFormatSelection
		});	
	
		
		
		$(document).on('click','a.exp-pdf',function(){
			if(confirm('Export PDF Data ..?')){
				$('#form_print').submit();
			}
			return false;
		});

		$(document).on('click','a.exp-excel',function(){
			if(confirm('Export Excel Data ..?')){
				$('#form_print_excel').submit();
			}
			return false;
		});

		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			minViewMode:0,
			viewMode:1
		}).on('changeDate',function(){
			$('.dt').datepicker('hide');
			loadDataReturtgl();
		});

		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#tanggal_mulai').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});
		
		$('#tanggal').focus();
	});





	/*
function refreshData(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>auth/page/data/pemeriksaandahak',
		data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val() },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});
}	
	*/
	

</script>
<section class="content">
	<div class="nav-tabs-custom">
		
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Cetak Laporan Lowongan Per Perusahaan</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				
				
				<div class="box-body">
				<form id="form_print" name="form_print" action="<?=base_url()?>bcc/page/pdfperusahaan/laplowongan" method="post" target="_blank" enctype="multipart/form-data">
						
					<div class="form-group">
						
						<div class="col-sm-12">
						<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
						
						<!--<input type="text" class="form-control dt"  id="tanggal_mulai" name="tanggal_mulai" placeholder="Tanggal Mulai" />
						<br>
						<input type="text"  id="tanggal_ahir" name="tanggal_ahir" placeholder="Hingga Tanggal" class='dt form-control' />-->
						
				
				</br>
						</div>
						<div class="form-group col-md-12">
						
						<label for="idperusahaan">Perusahaan</label>

						<div>

						<input type="text" name="idperusahaan" class="idperusahaan" size="60" style="min-width:250px"></br></br>

						</div>

					</div> 
						<div class="col-sm-12">
				</br>
				<a href="#" class="exp-pdf btn btn-info"> Cetak Laporan (PDF)</a>
						</div>
					</form>

<hr>
							<form id="form_print_excel" name="form_print_excel" action="<?=base_url()?>bcc/page/view/laplowongan_excel" method="post" target="_blank" enctype="multipart/form-data">
						
					<div class="form-group">
						
						<div class="col-sm-12">
						<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				
				</br>
						</div>
						<div class="form-group col-md-12">
						
						<label for="idperusahaan">Perusahaan</label>

						<div>

						<input type="text" name="idperusahaan" class="idperusahaan" size="60" style="min-width:250px"></br></br>

						</div>

					</div> 
						<div class="col-sm-12">
				</br>
				<a href="#" class="exp-excel btn btn-info"> Cetak Laporan (excel) </a>
						</div>
					</form>


					</div>
				

					
				</div>
				<div class="row-fluid">
				</div>
			
			</div>
		
				</div>
			</div><!-- /.tab-pane -->
			
		</div><!-- /.tab-content -->
	</div>
</section>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 id="myModalLabel"><a id="btn-print2" title="Retur Detail"><i class="icomoon-image2"></i></a> Retur Detail <span id="item-desc"></span></h4>
  </div>
  <div class="modal-body">
    <div id="div-detail">
	
	</div>
  </div>
</div>
