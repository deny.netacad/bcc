<?php
//$thn = $this->input->post('thn');
$rs = $this->db->query("
SELECT a.id AS pengunjung, SUM(IF(a.`log` = '2019-04-04', a.jml, 0)) AS hr1,SUM(IF(a.`log` = '2019-04-05', a.jml, 0)) AS hr2,
SUM(IF(a.`log` = '2019-04-06', a.jml, 0)) AS hr3,SUM(IF(a.`log` = '2019-04-07', a.jml, 0)) AS hr4 
FROM (SELECT COUNT(*) AS jml, `log`, 'pengunjung' AS id FROM ngi_pengunjungevent WHERE idevent = 1
GROUP BY `log`) a GROUP BY a.id
");
$data = '';
foreach($rs->result() as $item){
	$data.= '{	name: \''.$item->pengunjung.'\',	data: ['.$item->hr1.', '.$item->hr2.', '.$item->hr3.', '.$item->hr4.'] },';
}
?>

<script>
$(document).ready(function(){
	Highcharts.setOptions({
		lang: {
		  decimalPoint: '.',
		  thousandsSep: ','
		}
	});

	Highcharts.chart('container', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'GRAFIK PELAMAR KUNJUNGAN EVENT'
		},
		subtitle: {
			text: 'Source: disnakertrans kab.bogor'
		},
		xAxis: {
			categories: [
				'4 April 2019',
				'5 April 2019',
				'6 April 2019',
				'7 April 2019'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah Pelamar (Orang)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [ <?=substr($data,0,-1)?>]
	});
});
</script>
<div id="container"></div>
<form id="form_print" name="form_print" action="<?=base_url()?>bcc/page/pdf/lappelamarevent?o=L" method="post" target="_blank" enctype="multipart/form-data">
	<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
	<textarea id="htmltoprint" name="htmltoprint" style="display:none"></textarea>
</form>
<div class="box-body">
	<div class="form-group">
		<div class="col-md-12">
			<a href="#" class="exp-pdf btn btn-info"> Cetak PDF Laporan <i class="text-white icomoon-file-pdf"></i></a>
		</div>
	</div>
</div>

<div class="col-md-12" id="result_data" style="padding-left:20px;padding-right:20px;overflow:auto">
	<div class="row">
					<div class="col-md-12" id="result">
					</div>
				</div>
<h3>Statistik Pencaker pengunjung event</h3>
<table class="table table-bordered" style="width:100%">
	<thead>
		<tr style="background:#81D4FA;">
			<th style="width:30px;color:white">NO</th>
			<th style="color:white">STATUS</th>
			<th style="text-align:center;width:90px;color:white">4 April 2019</th>
			<th style="text-align:center;width:90px;color:white">5 April 2019</th>
			<th style="text-align:center;width:90px;color:white">6 April 2019</th>
			<th style="text-align:center;width:90px;color:white">7 April 2019</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$n = 0;
		foreach($rs->result() as $item){ $n++;
		$tot1[] = $item->hr1;
		$tot2[] = $item->hr2;
		$tot3[] = $item->hr3;
		$tot4[] = $item->hr4;
		?>
		<tr>
			<td><?=$n?>.</td>
			<td><?=$item->pengunjung?></td>
			<td align="right"><?=number_format($item->hr1)?></td>
			<td align="right"><?=number_format($item->hr2)?></td>
			<td align="right"><?=number_format($item->hr3)?></td>
			<td align="right"><?=number_format($item->hr4)?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
	<tfoot>
		<tr style="background:#4FC3F7">
			<th colspan="2" style="text-align:right;color:white">Total</th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot1))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot2))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot3))?></th>
			<th style="text-align:right;color:white"><?=number_format(array_sum($tot4))?></th>
		</tr>
	</tfoot>
</table>
</div>