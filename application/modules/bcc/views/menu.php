<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-settings"></i> <span>&nbsp;Manajemen BCC</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>bcc/manpenandatanganan"><i class="text-orange icomoon-user"></i>&nbsp;Mast Penandatanganan AK-1</a></li>
		<li><a href="<?=base_url()?>bcc/manpenyedialowongan"><i class="text-orange icomoon-office"></i>&nbsp;Manajemen Penyedia Lowongan</a></li>
		<li><a href="<?=base_url()?>bcc/mantrackingpelamar"><i class="text-orange icomoon-eye"></i>&nbsp;Manajemen Tracking & Kelola Pelamar</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-users"></i> <span>&nbsp;Report Pelamar</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>bcc/lappelamar"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Pelamar</a></li>
		<li><a href="<?=base_url()?>bcc/lappelamarevent"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Pelamar Event</a></li>
		<li><a href="<?=base_url()?>bcc/cetakak1"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Cetak AK-1</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-office"></i> <span>&nbsp;Report Penyedia Lowongan</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>bcc/lappenyedialowongan"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Penyedia Lowongan</a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-briefcase"></i> <span>&nbsp;Report Lowongan</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>bcc/laplowongan"><i class="text-orange icomoon-file-pdf"></i>&nbsp;Report Lowongan Per Perusahaan</span></a></li>
	</ul>
</li>
<li class="treeview ">
	<a href="#"><i class="text-blue icomoon-file"></i> <span>&nbsp;Pelatihan</span> <i class="fa fa-angle-left pull-right"></i></a>
	 <ul class="treeview-menu">
		<li><a href="<?=base_url()?>bcc/pelatihan"><i class="text-orange icomoon-office"></i>&nbsp;Posting Pelatihan</a></li>
		<li><a href="<?=base_url()?>bcc/pelatihan"><i class="text-orange icomoon-office"></i>&nbsp;Pendaftar Pelatihan</a></li>
	</ul>
</li>