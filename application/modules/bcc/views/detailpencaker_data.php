<script type="text/javascript">
  
  function pejabatFormatResult(data) {

    var markup= "<li><b>"+ data.nama+"</b></br><i>"+ data.keterangan+"</i></li>";

    return markup;

  }

  function pejabatFormatSelection(data) {

    return data.nama;

  }
    $(document).ready(function(){
      $("#idpejabat").select2({
      id: function(e) { return e.id_pejabat }, 
      placeholder: "Pilih Pejabat Penandatanganan",
      minimumInputLength: 0,
      multiple: false,
      allowClear:true,
      ajax: { 
        url: "<?=base_url()?>bcc/ajaxfile/caripejabat",
        dataType: 'jsonp',
        type:'POST',
        quietMillis: 100,
        data: function (term, page) {
          return {
            keyword: term, //search term
            per_page: 200, // page size
            page: page, // page number
            '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
          };
        },
        results: function (data, page) { 
          var more = (page * 100) < data.total;
          return {results: data.rows, more: more};
        }
      },
      formatResult: pejabatFormatResult, 
      formatSelection: pejabatFormatSelection
    }); 

    $(document).on('click','.depan',function(){
        var $this = $('.depan');
        var index = $this.index($(this));
        var vdata = $this.eq(index).attr('data').split('|');
        $('.sertifikat2').html(' <embed src="'+vdata[1]+'" width="100%" style="min-height:350px" />');
        $('#myModal2').modal('show');
    });
    });
    $(document).on('click','a.exp-pdf',function(){
      if(confirm('Cetak Bagian Depan Kartu AK-1 ?')){
        $('#form_print2').submit();
      }
      return false;
    });
   
</script>

<?php
 $rs = $this->db->query("SELECT a.*,DATE_FORMAT(a.tgl_lahir,'%d %M %Y') AS tgl_lhr,b.subdistrict_name AS nm_kecamatan FROM user_pelamar a
LEFT JOIN ngi_kecamatan b ON a.kecamatan_id=b.subdistrict_id WHERE a.iduser='".$this->input->post('idpencaker')."' ");



?>
<h4>Detail Pencari Kerja</h4>
 
<div id="myModal2" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">KARTU AK-1</h4>
      </div>
  <div class="modal-body sertifikat2">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


   <section class="content">

      <div class="row">
        <div class="col-md-12">
  <?php
    foreach($rs->result() as $data){
      $n++;
      error_reporting(1);
      
    ?>
          <!-- Profile Image -->
        
          <div class="box box-primary">
              <form id="form_print2" name="form_print2" action="<?=base_url()?>bcc/page/pdfak1/cetakak1_depan" method="post" target="_blank" enctype="multipart/form-data">
              <input type="hidden" name="iduser" id="iduser" value="<?=$data->iduser?>" size="60" style="min-width:250px"></br></br>
              <input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?=base_url()?>assets/img/profile/<?=(($data->photo != '')? $data->photo : 'default_user.png')?>" alt="User profile picture">
 
              <h3 class="profile-username text-center"><?=$data->nama?></h3>

              <p class="text-muted text-center"><?=$data->nik?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <div class="box-body">
              <strong><i class="fa fa-address-card margin-r-5"></i> Detail Personal</strong>

 <div class="jp_cp_right_side_wrapper">
                <div class="jp_cp_right_side_inner_wrapper">
                  <table style="width:100vw">
                                    <tbody>

                                        <tr>
                                            <td class="td-w25">Nama lengkap</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->nama?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-w25">Tempat tgl lahir</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->tmp_lahir?>, <?=$data->tgl_lhr?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-w25">Alamat</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->alamat?> <?=$data->subdistrict_name?>, <?=$data->type?> <?=$data->city?>, <?=$data->province?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-w25">Jenis Kelamin</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->gender?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-w25">No. telfon</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->no_telp?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-w25">Email</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->email?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-w25">Status</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->sts?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-w25">Kewarganegaraan</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->wn?></td>
                                        </tr>
                                         <tr>
                                            <td class="td-w25">Pend. Terakhir</td>
                                            <td class="td-w10">:</td>
                                            <td class="td-w65"><?=$data->pendidikan?></td>
                                        </tr>
                                    </tbody>
                                </table>
                </div>
              </div>
                </li>
                 <li class="list-group-item">
                  <div class="box-body">
              <strong><i class="fa fa-user margin-r-5"></i> Tentang <?=$data->nama?></strong>

              <div class="jp_cp_right_side_wrapper">
                <div class="jp_cp_right_side_inner_wrapper">
                  <table style="width:100vw">
                                    <tbody>
                                        <tr>
                                            <td class="td-w25"><?=$data->ttg_anda?></td>
                                          
                                        </tr>
                                      
                                    </tbody>
                                </table>
                </div>
              </div>
                </li>
              <li class="list-group-item">
                  <div class="box-body">
              <strong><i class="fa fa-graduation-cap margin-r-5"></i> Riwayat Pendidikan <?=$data->nama?></strong>

              <div class="jp_cp_right_side_wrapper">
                <div class="jp_cp_right_side_inner_wrapper">
                  <table style="width:100vw">
                                    <tbody>
                                        <tr>
                                            <td class="td-w25"> <?php 
                                                            if($data->education != ""){
                                                                $edu = explode(',', $data->education);
                                                                foreach ($edu as $s) {
                                                                    echo '
                                                                          <span class="label label-info" style="font-size:12px;">'.$s.'</span>
                                                                       ';
                                                                } 
                                                            }
                                                        ?>  </td>
                                          
                                        </tr>
                                      
                                    </tbody>
                                </table>
                </div>
              </div>
                </li>

               <li class="list-group-item">
                  <div class="box-body">
              <strong><i class="fa fa-male margin-r-5"></i> Keahlian <?=$data->nama?></strong>

              <div class="jp_cp_right_side_wrapper">
                <div class="jp_cp_right_side_inner_wrapper">
                  <table style="width:100vw">
                                    <tbody>
                                        <tr>
                                            <td class="td-w25"> <?php 
                                                            if($data->skill != ""){
                                                                $skill = explode(',', $data->skill);
                                                                foreach ($skill as $s) {
                                                                    echo '
                                                                          <span class="label label-info" style="font-size:12px;">'.$s.'</span>
                                                                       ';
                                                                } 
                                                            }
                                                        ?>  </td>
                                          
                                        </tr>
                                      
                                    </tbody>
                                </table>
                </div>
              </div>
                </li>
                 <li class="list-group-item">
                  <div class="box-body">
              <strong><i class="fa fa-briefcase margin-r-5"></i> Pengalaman <?=$data->nama?></strong>

              <div class="jp_cp_right_side_wrapper">
                <div class="jp_cp_right_side_inner_wrapper">
                  <table style="width:100vw">
                                    <tbody>
                                        <tr>
                                            <td class="td-w25"><?=$data->exp?></td>
                                          
                                        </tr>
                                      
                                    </tbody>
                                </table>
                </div>
              </div>
                </li>


              </ul>
              <p class="text-muted text-center"><input type="text" name="idpejabat" id="idpejabat" size="60" style="min-width:250px" required></p>
               <p class="text-muted text-center">
                <a href="#" class="exp-pdf btn btn-info">Cetak Bagian Depan</a>
                <a href="#" class="exp-pdf2 btn btn-info">Cetak Bagian Belakang</a>

               

                 <!--  <a href="#" data="pdf|<?=base_url()?>bcc/page/pdfak1/cetakak1_depan?&o=L&id=<?=$data->iduser?>" class="depan"> <i class="fa fa-print"></i> Cetak Depan</a>&nbsp;&nbsp; -->
                 <!--  <a href="#" data="pdf|<?=base_url()?>bcc/page/pdfak1/cetakak1_belakang?&o=L&id=<?=$data->iduser?>" class="lmrzknews2"> <i class="fa fa-print"></i> Cetak Belakang</a></p> -->
              
            </div>
            <!-- /.box-body -->
          </form>

          </div>
      

           <?php
    }
    ?>
        </div>
      </div>
    </section>