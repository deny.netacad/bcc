 <style type="text/css">
 	.cek {
        border: 1px solid #f9f9f9;
        background: #f9f9f9;
        color: #6a7c98;
        font-size: 17px;
		vertical-alling: middle;
    }

    .register_left_form input[type="text"], .register_left_form input[type="email"], .register_left_form input[type="password"], .register_left_form input[type="tel"], .register_left_form input[type="number"], .register_left_form input[type="url"], .register_left_form select, .register_left_form textarea, .register_left_form input {
        text-transform: none;
    }
 </style>
 <script>
 	function PropinsiFormatResult(data) {
        var markup= "<li>"+ data.province+"</li>";
        return markup;
    }

    function PropinsiFormatSelection(data) {
        return data.province;
    }


	function KabFormatResult(data) {
        var markup= "<li>"+ data.city_name+" ("+ data.types+")</li>";
        return markup;
    }

    function KabFormatSelection(data) {
     var markup= ""+ data.city_name+" ("+ data.types+")";
        
        return markup;
    }
	function KecFormatResult(data) {
        var markup= "<li>"+ data.subdistrict_name+"</li>";
        return markup;
    }

    function KecFormatSelection(data) {
     var markup= ""+ data.subdistrict_name+"";
        
        return markup;
    }

	
    $(document).ready(function(){
        
		$('#per_page').change(function(){
			refreshData();
		});
		$('.datemask').mask('00/00/0000');
		$('.dt').datepicker({
			format:'yyyy-mm-dd',
			//format:'dd-mm-yyyy',
			minViewMode:0,
			viewMode:1
		}).on('changeDate',function(){
			$('.dt').datepicker('hide');
			loadDataKomoditas();
		});
		
		$('#cari').keyup( $.debounce(250,refreshData));
		$.ajax({
            type:'post',
            url:'<?=base_url()?>bcc/page/data/mantrackingpelamar',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
        
		$("#province_id").select2({
		    id: function(e) { return e.id }, 
		    placeholder: "PROVINSI",
		    minimumInputLength: 0,
		    multiple: false,
		    allowClear:true,
		    ajax: { 
		        url: "<?=base_url()?>bcc/ajaxfile/cariPropinsi",
		        dataType: 'jsonp',
		        type:'POST',
		        quietMillis: 100,
		        data: function (term, page) {
		            return {
		                keyword: term, //search term
		                per_page: 5, // page size
		                page: page, // page number
		                '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
		            };
		        },
		        results: function (data, page) { 
		            var more = (page * 5) < data.total;
		            return {results: data.rows, more: more};
		        }
		    },
		    formatResult: PropinsiFormatResult, 
		    formatSelection: PropinsiFormatSelection
		});


		$("#city_id").select2({
		        id: function(e) { return e.city_id }, 
		        placeholder: "KABUPATEN / KOTA",
		        minimumInputLength: 0,
		        multiple: false,
		        allowClear:true,
		        ajax: { 
		            url: "<?=base_url()?>bcc/ajaxfile/cariKab",
		            dataType: 'jsonp',
		            type:'POST',
		            quietMillis: 100,
		            data: function (term, page) {
		                return {
		                    keyword: term, //search term
		                    per_page: 5, // page size
		                    page: page, // page number
		                    province_id:$('#province_id').val(),
		                    '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
		        
		                
		                };
		            },
		            results: function (data, page) { 
		                var more = (page * 5) < data.total;
		                return {results: data.rows, more: more};
		            }
		        },
		        formatResult: KabFormatResult, 
		        formatSelection: KabFormatSelection
		    });
			
        $("#kecamatan_id").select2({
        id: function(e) { return e.subdistrict_id }, 
        placeholder: "KECAMATAN",
        minimumInputLength: 0,
        multiple: false,
        allowClear:true,
        ajax: { 
            url: "<?=base_url()?>bcc/ajaxfile/cariKec",
            dataType: 'jsonp',
            type:'POST',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    keyword: term, //search term
                    per_page: 5, // page size
                    page: page, // page number
                    province_id:$('#province_id').val(),
                    city_id:$('#city_id').val(),
                    
                    '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
        
                
                };
            },
            results: function (data, page) { 
                var more = (page * 5) < data.total;
                return {results: data.rows, more: more};
            }
        },
        formatResult: KecFormatResult, 
        formatSelection: KecFormatSelection
    });
        
        $('#alert1').hide();
		/* action save */
        $("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					$('#alert1').html(ret.text);
					$('#alert1').fadeIn(1000);
					$('#alert1').fadeOut(1000);
					$('#form1').trigger('reset');
				});		
			}else{
				//$('#form1').trigger('reset');
				// $('#province_id').select2('data',null);
				// $('#city_id').select2('data',null);
				// $('#kecamatan_id').select2('data',null);
			}
	//		$('#form1').trigger('reset');
			$('#province_id').select2('data',null);
			$('#city_id').select2('data',null);
			$('#kecamatan_id').select2('data',null);
		});

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
			$('#id_edit').val('');
			//$('#form1').trigger('reset');
			$('#province_id').select2('data',null);
			$('#city_id').select2('data',null);
			$('#kecamatan_id').select2('data',null);

        });
		
		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>bcc/page/editpencaker',
				type: 'post',
				data: { 
					'iduser':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					$('#id_edit').val(ret['iduser']);
					for(attrname in ret){
						if(attrname=='iduser'){
							$('#id_edit').val(ret[attrname]);
						}else{
							$('#'+attrname).val(ret[attrname]);
						}
					}

					$('#kecamatan_id').select2('data',{'subdistrict_id':ret.kecamatan_id,'subdistrict_name':ret.subdistrict_name});
					$('#province_id').select2('data',{'id':ret.province_id,'province':ret.province});
					$('#city_id').select2('data',{'city_id':ret.city_id,'city_name':ret.city_name,'types':ret.type});
					//$('#tahun_lulus').select2('data',{'tahun_lulus':ret.tahun_lulus});
			
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>bcc/page/delpencaker',
					type: 'post',
					data: { 
						'iduser':$this.eq(index).attr('data'),
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
		$(document).on('click','a[title="Detail"]',function(){
			var $this = $('a[title="Detail"]');
			var index = $this.index($(this));
			
			$.ajax({
				url:'<?=base_url()?>bcc/page/view/userpelamar',
				type:'post',
				data:{ 'iduser':$this.eq(index).attr('data'),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){},
				success:function(response){
					$('#div-detail').html(response);
					$('#myModal').modal('show',function(){
						keyboard:true
					});
					return false;
				}
			});
		
		});
	
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#title').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});

    });
	
	function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>bcc/page/data/mantrackingpelamar',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }
    
    
    $(function(){
        $(document).on('input', '#password_ori', function(){
			$('#password_check').html('<i class="fa fa-spinner fa-spin"></i>');
			
            if($(this).val() == $('#password').val()){
				$('#password_check').html('<i class="fa fa-check"></i>');
                $('#submit_btn').attr('disabled',false);
            }else{
				$('#password_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        }).on('input', '#username', function(){
            $('#username_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();            
            var regexp = /[A-Za-z0-9]{3,}$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkusername',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        username: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#username_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#username_check').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#username_check').html('<i class="fa fa-times"></i>');
                $('#regBtn').attr('disabled',true);
            }
        }).on('input', '#no_ktp', function(e){
        	var val = $(this).val();

        	if(val.length == '16'){
        		$.ajax({
        			url : 'https://ayokitakerja.kemnaker.go.id/tools/check_nik/' + val,
        			type: 'get',
					contentType: 'text/plain',
					xhrFields: {
						withCredentials: false
					},
					header : {
						'Access-Control-Allow-Origin' : '*'
					},
        			success: function(rs){
        				console.log(rs)
        			}
        		})
        	}else{
        		$('#submit_btn').attr('disabled',true);
        	}
        }).on('input', '#email', function(){
            $('#email_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();            
            var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkemail',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        email: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#email_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#email_check').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#email_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        });
    })

 
 $(document).on('submit','#formaplut', function(e) {
    alert('Upload Data Pencaker?');
    e.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    var fileinput = ((document.getElementById('excel').files[0].size)/(1024*1024));
    var size = fileinput.toFixed(2);
    $.ajax({
        xhr: function()
          {
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function(evt){
              if (evt.lengthComputable) {
                var percentComplete = parseInt((evt.loaded / evt.total)*100);
                var percentSize = (size*percentComplete/100).toFixed(2);
                //Do something with upload progress
                $('#kirim').hide();
                $('#bar').html('<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">Mengunggah '+percentSize+' MB dari '+size+' MB ('+percentComplete+' %)</div></div>');
              }
            }, false);
            return xhr;
          },
        url: url,
        type: 'POST',
        data: formData,
        dataType:'json',
        complete: function(){
             $('#kirim').show();
             $('#bar').html('');
          },
        success: function (data) {
            if(data.error){
                alert(data.teks);
            }else{
                alert(data.teks);
                $('#list').click();
            }
            $('#formaplut').trigger('reset');
        },
        cache: false,
        contentType: false,
        processData: false
    });
    refreshData();
}); 
	

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
		  <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-upload"></i> Import Jadwal</a></li>
		  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		 </ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Manajemen Data Pencaker</h3>
				</div><!-- /.box-header -->
				<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>bcc/page/savepencaker">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value=""  />
				<!-- <input type="hidden" id="id_edit" name="iduser" value="iduser" /> -->
				<div class="box-body">
					<label class="text-muted">AKUN LOGIN PENCARI KERJA</label>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Username</label>
						<div class="input-group">
							<input type="text" class="form-control" name="username" id="username" placeholder="Username*" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka tanpa spasi, 3 - 10 karakter">
                           	<span class="input-group-addon cek" id="username_check"></span>
                                                
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Password</label>
						<div class="input-group">
							<input type="password" class="form-control" name="password" id="password" placeholder=" password*">
                            <span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Konfirmasi Password</label>
						<div class="input-group">
							<input type="password" class="form-control" name="password_ori" id="password_ori" placeholder="Konfirmasi password*">
                            <span class="input-group-addon cek" id="password_check"></span>
                                                
						</div>
					</div>
					<label class="text-muted">BIODATA</label>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Nama Lengkap</label>
						<div class="input-group">
							<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                      
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">NIK</label>
						<div class="input-group">
							<input type="text" class="form-control" name="no_ktp" id="no_ktp"  pattern="[0-9]{16,16}" title="No ktp harus berupa angka, 16 digit" placeholder="Nomor Induk Kependudukan*" maxlength="16" required=""> 
							<span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Tempat Lahir</label>
						<div class="input-group">
							<input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir" placeholder="Tempat Lahir*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Tanggal Lahir</label>
						<div class="input-group">
							 <input type="text" class="form-control datemask" name="tgl_lahir" id="tgl_lahir" placeholder="dd/mm/yyyy" required="" autocomplete="off">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">No Telp</label>
						<div class="input-group">
							<input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Telp*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Email</label>
						<div class="input-group">
							<input type="text" class="form-control" name="email" id="email" placeholder="Email*" required="">
							<span class="input-group-addon cek" id="email_check"></span>                  
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Alamat</label>
						<div class="input-group">
							<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Jenis Kelamin</label>
						<div class="input-group">
							<select id="gender" name="gender" class="form-control" required="">
								<option value="Laki-laki">Laki-laki</option>
								<option value="Perempuan">Perempuan</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Agama</label>
						<div class="input-group">
							<select name="agama" id="agama" class="form-control" required="">
												<option value="-">Agama*</option>
												<option value="Budha">Budha</option>
												<option value="Hindu">Hindu</option>
												<option value="Islam">Islam</option>
												<option value="Kristen">Kristen</option>
												<option value="Kristen-Katolik">Kristen-Katolik</option>
												<option value="Kristen Protestan">Kristen Protestan</option>
												<option value="Lainnya">Lainnya</option>
											</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Jenis Kelamin</label>
						<div class="input-group">
							<select name="wn" id="wn" class="form-control" required="">
								<option value="-">Kewarganegaraan*</option>
								<option value="WNI">WNI</option>
								<option value="WNA">WNA</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Pendidikan Terakhir</label>
						<div class="input-group">
							<select name="pendidikan" id="pendidikan" class="form-control" required="">
								<option value="-">Pendidikan Terakhir*</option>
								<option value="SD">SD</option>
								<option value="SMP">SMP</option>
								<option value="SMA">SMA</option>
                                <option value="SMK">SMK</option>
                                <option value="Paket C">Paket C</option>
								<option value="D1">D1</option>
								<option value="D2">D2</option>
								<option value="D3">D3</option>
								<option value="D4">D4</option>
								<option value="S1">S1</option>
								<option value="S2">S2</option>
								<option value="S3">S3</option>
								<option value="Akta IV">Akta IV</option>
								<option value="Akuntan">Akuntan</option>
								<option value="Apoteker">Apoteker</option>
								<option value="Kedokteran">Kedokteran</option>
								<option value="Keperawatan">Keperawatan</option>
								<option value="Notaris">Notaris</option>
								<option value="Ners">Ners</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Jurusan</label>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Jurusan atau program studi pendidikan terakhir anda" name="jurusan" id="jurusan" required="" value="<?=$biodata->jurusan?>">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Tahun Lulus</label>
						<div class="input-group">
							<input type="text" class="form-control" name="tahun_lulus" id="tahun_lulus" placeholder="Tahun Lulus*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label" for="province_id">Provinsi</label>
						<div class="input-group">
							<input type="hidden" class="" placeholder="Provinsi" name="province_id" id="province_id" required="" style="width: 100%;">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label" for="city_id">Kabupaten / Kota</label>
						<div class="input-group">
							<input type="hidden" class="" placeholder="Kabupaten / Kota" name="city_id" id="city_id" required="" style="width: 100%;">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label" for="kecamatan_id">Kecamatan / Kelurahan</label>
						<div class="input-group">
							<input type="hidden" class="" placeholder="Kecamatan / Kelurahan" name="kecamatan_id" id="kecamatan_id" required="" style="width: 100%;">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					
					
					
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Status</label>
						<div class="input-group">
							<select name="sts" id="sts" class="form-control" required="">
								<option value="-">Status*</option>
								<option value="Lajang">Lajang</option>
								<option value="Nikah">Nikah</option>
								<option value="Cerai">Cerai</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
				</div>
					
					
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right" >Simpan</button>
				</div>
				<!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane" id="tab_3">
			    <div class="box">
			        <div class="box-header with-border">
    			    	<h3 class="box-title">Import Data Pelamar</h3>
			    	</div><!-- /.box-header -->
			    	<div class="box-body">
    			    	<form id="formaplut" action="<?=base_url()?>bcc/page/uploadPencaker" method="post" enctype="multipart/form-data">
    			    	    <input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        			    	 <div class="col-md-12">
    			    	             Ketentuan unggah Data :<br>
    								1. File yang diunggah harus menggunakan file  <a href="<?=base_url()?>formatatlit.xls" target="_blank" class="btn btn-info">Sample</a> ini<br>
    								2. Unggah data bersifat menambah data bukan menggantikan data yang ada.<br>
    								3. Pastikan file yang akan diupload sesuai yang didownload yaitu <font color=red><b>formatpelamarbcc.xls.</b></font><br><br>
    						</div>
    		               <input type="file" id="excel" name="file" style="padding-bottom:20px;padding-left:10px"/>
    		               <div id="bar"></div>
                		    <div class="box-footer">
                		        <button type="reset" class="btn btn-default">Batal</button>
            		            <button type="submit" id="kirim" class="btn btn-info pull-right">Upload File</button>
                		    </div>
                		</form>
			    	</div>
			    </div>
			</div>
			<div class="tab-pane" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Daftar Data Pelamar</h3>
					  <div class="box-tools">
						<div class="input-group pull-right" style="width: 150px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:200px" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						  </div>
						</div>
						<div class="btn-group">
						<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="4">4</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					</div>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
		
		
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modal-title2" style="text-transform:capitalize">DETAIL PELAMAR</h4>
          </div>
          <div class="modal-body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                             <div id="div-detail">
	
	</div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
