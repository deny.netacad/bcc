<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
    $cari = $this->input->post('cari');
    
    if($cari!="") $where.= " where (e.nama_job like '%".$cari."%' or b.nama_perusahaan '%".$cari."%')";
    
    $n = intval($this->uri->segment(5));
    $q = "SELECT a.*,DATE_FORMAT(a.date_start,'%d %M %Y') AS dt_start,DATE_FORMAT(a.date_end, '%d %M %Y') AS dt_end,b.nama_perusahaan AS nm_perusahaan,c.subdistrict_name AS nm_kecmatan,d.category AS nm_category,e.nama_job AS nm_job FROM ngi_joblist a
		LEFT JOIN ngi_perusahaan b ON a.idperusahaan=b.idperusahaan
		LEFT JOIN ngi_kecamatan c ON a.idkecamatan=c.subdistrict_id
		LEFT JOIN ngi_jobcategory d ON a.idcategory=d.id
		LEFT JOIN ngi_job e ON a.posisi=e.id ";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'auth/page/data/manpostinglowongan';

    $config['total_rows']           = $totrows;
    $config['per_page']             = $per_page;
    $config['uri_segment']          = 5;
    $config['is_ajax_paging']       = TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("$q $where order by nama_perusahaan limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
    ");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th align="center">#</th>
        <th>NAMA PERUSAHAAN</th>
        <th>PEKERJAAN</th>
        <th>KATEGORI</th>
        <th>KECAMATAN</th>
		<th>TGL LOWONGAN</th>
        <th>GAJI</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;

	?>
   <tr>
		<td>
			<?=$n?>.
		</td>
		<td>
			<div class="text-left"><?=$item->nm_perusahaan?></div>
		</td>
		<td>
			<div class="text-left"><?=$item->nm_job?></div>
		</td>
		<td><div class="text-left"><?=$item->nm_category?></div></td>
		<td>
			<div class="text-left"><?=$item->nm_kecmatan?></div>
		</td>
		<td><div class="text-center"><?=$item->dt_start?> s/d <?=$item->dt_end?></div></td>
		<td>
			<div class="text-left">Rp. <?=number_format($item->upah_kerja)?></div>
		</td>
		<!--<td><div class="text-center"><?=($item->flag==1 && $item->scanfile!='')?'<a title="sendmail" data="'.$item->id.'"><button>Aktivasi</button></a>':''?></div></td>-->
	</tr>
        <?php
    }
    ?>
    </tbody>
</table>