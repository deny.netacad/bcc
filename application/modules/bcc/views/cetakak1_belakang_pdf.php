<style>
            #customers {
                font: bold 11px Trebuchet MS;
                border-collapse: collapse;
                border: 1px solid #000;
            }
            #customers td, #customers tr {
                font-size: 1em;
                border: 1px solid #000;
                padding: 3px 7px 3px 7px;
            }
            .style3 {
                font-size: smaller;
                font-style: italic;
            }

            table.size9 tr td {
                font-size: 9pt;
            }
</style>

<?php
$rs = $this->db->query("SELECT date_format(DATE(curdate() + INTERVAL 6 MONTH),'%d-%m-%Y') as bln1, date_format(DATE(curdate() + INTERVAL 12 MONTH),'%d-%m-%Y') as bln2, date_format(DATE(curdate() + INTERVAL 18 MONTH),'%d-%m-%Y') as bln3 FROM user_pelamar a
WHERE a.iduser='".$this->input->post('idpencaker')."' ");
 


foreach($rs->result() as $item){
    
?>
<table width="100%">
<tr>
<td width="1%">&nbsp;</td>
<td width="46%" valign="top">
    <table width="100%" id="customers" class="size9">
        <tr>
        <td height="34" colspan="2" style="text-align:top; font-weight:bold;font-size:10pt;"><u>KETENTUAN :</u></td>
        </tr>
        <tr>
        <td width="5%" height="28" valign="top" style="text-align:top; font-weight:bold;font-size:10pt;">1.</td>
        <td width="95%" valign="top">BERLAKU NASIONAL</td>
        </tr>
        <tr>
        <td height="40" valign="top">2.</td>
        <td valign="top">BILA ADA PERUBAHAN DATA/KETERANGAN LAINNYA ATAU TELAH MENDAPAT PEKERJAAN HARAP SEGERA MELAPOR</td>
        </tr>
        <tr>
        <td height="53" valign="top">3.</td>
        <td valign="top">APABILA PENCARI KERJA YANG BERSANGKUTAN TELAH DITERIMA BEKERJA MAKA INSTANSI/PERUSAHAAN YANG MENERIMA AGAR MENGEMBALIKAN
        KARTU AK.I INI.</td>
        </tr>
        <tr>
        <td height="47" valign="top">4.</td>
        <td valign="top">KARTU INI BERLAKU SELAMA 2 TAHUN DENGAN KEHARUSAN KETENTUAN MELAPOR SETIAP 6 BULAN SEKALI BAGI PENCARI
        KERJA YANG BELUM MENDAPATKAN PEKERJAAN</td>
        </tr>
    </table>
</td>
<td width="4%" valign="top"><br></td>
<td width="49%" valign="top">
    <table width="100%" id="customers">
        <tr>
            <td width="18%" style="text-align:center;font-weight:bold;font-size:10pt;">LAPORAN<br></td>
            <td width="32%" style="text-align:center;font-weight:bold;font-size:10pt;">TGL - BULAN - TAHUN<br></td>
            <td width="50%" style="text-align:center;font-weight:bold;font-size:10pt;">Tanda Tangan Pengantar Kerja/<br>
            Pertugas Pendaftar <br>
            <span class="style3">(Cantumkan NIP)</span></td>
        </tr>
        <tr>
            <td style="text-align:center;font-size:9pt;">PERTAMA</td>
            <td style="text-align:center;"><?=$item->bln1?></td>
            <td style="text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;font-size:9pt;">KEDUA</td>
            <td style="text-align:center;"><?=$item->bln2?></td>
            <td style="text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;font-size:9pt;">KETIGA</td>
            <td style="text-align:center;"><?=$item->bln3?></td>
            <td style="text-align:center;">&nbsp;</td>
        </tr>
    </table>
<br>
<table width="100%" id="customers">
<tr>
<td width="50%" style="font-size:9pt;">DITERIMA DI</td>
<td width="50%">&nbsp;</td>
</tr>
<tr>
<td style="font-size:9pt;">TERHITUNG MULAI TANGGAL</td>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
<?php 
}?>
<br><br>
