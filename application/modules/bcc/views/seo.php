<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	
	
	function loadData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>bcc/page/loadSeo',
            data:{ 
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                
            },
            success:function(response){
                var ret = $.parseJSON(response);
				for(attrname in ret){
					if(attrname=='contact_id'){
						$('#id_edit').val(ret[attrname]);
					}else{
						$('#'+attrname).val(ret[attrname]);
					}
				}
            }
        });
    }

	
    $(document).ready(function(){
        
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        loadData();
                    });
            }else{
                loadData();
            }
        });

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
        });
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#title').focus();
				break;
			}
		});
		
        loadData();

    });

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Section - Kontak</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>operatorweb/page/saveKontak">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="addr" class="col-sm-2 control-label">Keyword</label>
						<div class="col-sm-10">
							<input type="text" id="keyword" name="keyword" class="form-control" placeholder="Keyword pisahkan dengan koma" />
						</div>
					</div>
					<div class="form-group">
						<label for="bus_hour" class="col-sm-2 control-label">Deskripsi</label>
						<div class="col-sm-10">
							<textarea id="desc" name="desc" class="form-control" placeholder="Alamat"></textarea>
						</div>
					</div>

					
				
					
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>