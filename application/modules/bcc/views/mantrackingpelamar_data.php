<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
    $cari = $this->input->post('cari');
    
    if($cari!="") $where.= " where (nama like '%".$cari."%' or no_ktp like '%".$cari."%')";
    
    $n = intval($this->uri->segment(5));
    $q = "SELECT a.*,DATE_FORMAT(a.register,'%d %M %Y') AS tgl_daftar,DATE_FORMAT(a.tgl_lahir,'%d %M %Y') AS tgl_lhr,b.subdistrict_name AS nm_kecamatan FROM user_pelamar a
LEFT JOIN ngi_kecamatan b ON a.kecamatan_id=b.subdistrict_id";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'bcc/page/data/mantrackingpelamar';

    $config['total_rows']           = $totrows;
    $config['per_page']             = $per_page;
    $config['uri_segment']          = 5;
    $config['is_ajax_paging']       = TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("$q $where order by nama limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
    ");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
<?=$data['pagination']?>
</div>
<img id="noimage" style="display:none" />
<table class="table table-hover">
  <colgroup>
    <col class="con0" style="width: 25px" />
    <col class="con0" style="width:auto" />
    <col class="con0" style="width:200px" />
    <col class="con0" style="width:50px" />
  </colgroup>
  <thead class="breadcrumb">
    <tr>
      <th>#</th>
      <th>NAMA</th>
      <th>NIK</th>
	  <th>EMAIL</th>
    <th>PENDIDIKAN & JURUSAN</th>
	  <th>TTL</th>
	  <th>ALAMAT</th>
      <th><div class="text-center">PROSES</div></th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
    ?>
    <tr>
      <td align="center"><?=$n?>.</td>
      <td>
      <span class="ed1" style="display:none"><?=$item->iduser?></span>
      <div class="text-left"><?=strtoupper($item->nama)?></div>
      </td>
	    <td>
      <div class="text-left"><?=$item->no_ktp?></div>
      </td>
      <td>
      <div class="text-left"><?=$item->email?></div>
      </td>
      <td>
      <div class="text-left"><?=$item->pendidikan?>, <?=$item->jurusan?></div>
      </td>
	  <td>
      <div class="text-left"><?=$item->tmp_lahir?>, <?=$item->tgl_lhr?></div>
      </td>
	  <td>
      <div class="text-left"><?=$item->alamat?>, <?=$item->nm_kecamatan?></div>
      </td>
	
	
      <td>
        <div class="text-center">
            <a href="#" title="Edit" data="<?=$item->iduser?>"><i class="icomoon-pencil2"></i></a>
            <a href="#" title="Detail" data="<?=$item->iduser?>"><i class="icomoon-eye"></i></a>
            <a href="#" title="Delete" data="<?=$item->iduser?>"><i class="icomoon-remove red"></i></a>
        </div>
      </td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>
<span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
