
<style>
body{
  font-family: verdana;
  font-size:11px;
}
table, td, th {
    border: 1px solid black;
	padding:5px;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    height: 30px;
}
#identitas{

  font-size: 11px;
}
<?php 


if ($this->session->userdata('is_login_portal_disnakerbogor')){
      

$where = "";
$isall = true;

if ($_POST['prov'] != ""){
  $where = " where b.province_id = '".$_POST['prov']."' ";
  $isall = false;
}

if($_POST['kab'] != ""){
  $where = " where b.city_id = '".$_POST['kab']."' ";
  $isall = false;
}

if($_POST['kec'] != ""){
  $where = " where b.subdistrict_id = '".$_POST['kec']."' ";
  $isall = false;
}

$q2 = $this->db->query("SELECT b.subdistrict_name AS nm_kecamatan, b.city as nm_kota, b.province as nm_provinsi from ngi_kecamatan b ".$where." ");
$rs = $q2->result();

if($_POST["pend"] != ""){
	$where .= ($isall == true) ? " where a.pendidikan = '".$_POST["pend"]."' " : " and a.pendidikan = '".$_POST["pend"]."' ";
}


$q = $this->db->query("SELECT a.*, DATE_FORMAT(a.register,'%d %M %Y') AS tgl_daftar,DATE_FORMAT(a.tgl_lahir,'%d %M %Y') AS tgl_lhr,b.subdistrict_name AS nm_kecamatan, b.city as nm_kota, b.province as nm_provinsi FROM user_pelamar a
LEFT JOIN ngi_kecamatan b ON a.kecamatan_id=b.subdistrict_id  
".$where." 
ORDER BY a.nama");

?>
</style>

<h2 style="text-align:left;">DAFTAR <?=(($where == "") ? "SEMUA " : "")?>PELAMAR <?=(($_POST['kec'] != "") ? strtoupper($rs[0]->nm_kecamatan)."," : "")?> <?=(($_POST['kab'] != "") ? strtoupper($rs[0]->nm_kota)."," : "")?> <?=(($_POST['prov'] != "") ? strtoupper($rs[0]->nm_provinsi) : "")?></h2>

<table class="table table-bordered">
    
  <thead style="background:#d5f2fc">
    <tr>
      <th>NO</th>
      <th  style="vertical-align:middle"><div class="text-center">NAMA</div></th>
      <th ><div class="text-center">NIK</div></th>
      <th ><div class="text-center">NO HP</div></th>
      <th ><div class="text-center">EMAIL</div></th>
      <th ><div class="text-center">TTL</div></th>
       <th ><div class="text-center">ALAMAT</div></th>
       <th ><div class="text-center">PENDIDIKAN TERAKHIR</div></th>
        <th ><div class="text-center">TGL DAFTAR</div></th>
      </tr>
  </thead>
  <tbody>
    <?php
    $n=1;
    foreach($q->result() as $item){
      error_reporting(1);
     
    ?>
    <tr>
      <td align="center"><?=$n?></td>
      <td><div class="text-left"><?=strtoupper($item->nama)?></div></td>
      <td>
		<div class="text-left"><?=strtoupper($item->no_ktp)?></div>
      </td>
     <td>
    <div class="text-left"><?=strtoupper($item->no_telp)?></div>
      </td>
      <td>
		<div class="text-left"><?=strtoupper($item->email)?></div>
      </td>
	   <td>
		<div class="text-left"><?=strtoupper($item->tmp_lahir)?>, <?=$item->tgl_lhr?></div>
      </td>
      <td>
		  <div class="text-left"><?=strtoupper($item->alamat)?>, <?=strtoupper($item->nm_kecamatan)?></div>
    </td>
	<td>
        <div class="text-left"><?=strtoupper($item->pendidikan)?>/SEDERAJAT<?=(($item->jurusan != "") ? ", ".strtoupper($item->jurusan) : "")?><?=(($item->tahun_lulus != "") ? ", Thn. ".$item->tahun_lulus : "")?></div>
    </td>
      <td>
		<div class="text-left"><?=$item->tgl_daftar?></div>
      </td>
     </tr>
    <?php
    $n++;
    }
    ?>
  </tbody>
  <tfoot style="background:#d5f2fc">
    <!--<tr>-->
    <!--  <th colspan="4" align="right"><div class="text-right" >Total</div></th>-->
    <!--  <th align="right"><div class="text-right"><?=number_format(array_sum($total))?></div></th>-->
    <!--</tr>-->
  </tfoot>
</table>
<small >
<center><i><div id="identitas">tercetak oleh system pada <?=date('d , M Y h:i:s')?> dengan IP <?=$this->input->ip_address()?></i> &copy; Dinas Tenaga Kerja Kabupaten Bogor. All Rights Reserved.</div>  </center></small>

<?php 
}
?>



