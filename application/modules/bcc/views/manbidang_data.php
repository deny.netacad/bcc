<script src="<?=$def_js?>sort.js"></script>
<script>
    $(document).ready(function(){
		$('table.sortable tbody').sortable({
			//alert('ok');
            update: function(event,ui){
                var bannerOrder_ = $(this).sortable('toArray').toString();
                $.ajax({
                    type: 'post',
                    url:'<?=base_url()?>auth/page/saveSortBidang',
                    data: { bannerOrder: bannerOrder_ },
                    success: function(){
                        $('#per_page').trigger('change');
                    }
                });
            }
        }).disableSelection();
    });
</script>
<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?10:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.bidang like '%".$cari."%' ) ";

    $n = intval($this->uri->segment(5));
    $q = "select * from mast_bidang a left join mast_bidang b on a.parent_id=b.idbidang";
    $rs = $this->db->query("$q $where");
	$row = $rs->row_array();
    $totrows = $rs->num_rows();
	
    $config['base_url'] = base_url().'auth/page/data/manbidang';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;
    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,IF(a.parent_id>0,a.parent_id,a.idbidang) AS idx,b.bidang as parent_bidang
		FROM mast_bidang a left join mast_bidang b on a.parent_id=b.idbidang $where
		ORDER BY a.menu_order,idx,a.parent_id limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
	
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate" style="padding:5px;">
    <span class="paginate_info" style="display:inline-block;margin-left:6px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover sortable">
    <colgroup>
        <col class="con0" style="width: 4%" />
    </colgroup>
    <thead>
    <tr>
        <th>#</th>
        <th>MENU</th>
        <th>PARENT</th>
       
       
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
        foreach($data['posts']->result() as $item){ $n++;
    ?>
    <tr <?php if($item->parent_id==0){?>id="<?=$item->menu_order."-".$item->idbidang?>"<?php }?>>
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->id?></span>
            <div class="text-left"><?=($item->parent_id==0)?$item->bidang:'<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$item->bidang.'</i>'?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->parent_bidang?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->idbidang?>"><i class="text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->idbidang?>"><i class=" text-red fa fa-trash"></i></a>
                <?php if($item->parent_id == 0){ ?>
                    <i class="fa fa-arrows"></i>
                <?php } ?>
            </div>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>