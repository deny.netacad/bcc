 <?php
$q = "SELECT a.*,DATE_FORMAT(a.register,'%d %M %Y') AS tgl_daftar,DATE_FORMAT(a.tgl_lahir,'%d %M,%Y') AS tgl_lhr,b.subdistrict_name AS nm_kecamatan FROM user_pelamar a
LEFT JOIN ngi_kecamatan b ON a.kecamatan_id=b.subdistrict_id WHERE iduser='".$this->input->post('iduser')."' ";
$rs = $this->db->query($q);
$item = $rs->row();
?>

 <div class="modal-body">
 <section class="content">
		
      <div class="row">
        <div class="col-md-12">
		
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_user.png')?>" alt="User profile picture">

              <h4 class="profile-username text-center"><?=$item->nama?></h4>
			  <!--<p class="text-center" style="color:blue;"><?=$item->email?></p>-->
              <p class="text-muted text-center"><?=$item->email?></p>
              <p class="text-muted text-center"><?=$item->no_ktp?></p>
              <p class="text-muted text-center"><?=$item->no_telp?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Terdaftar Sejak</b> <a class="pull-right"><?=$item->tgl_daftar?></a>
                </li>
              </ul>

              <!--<a href="page/pdfresumepencaker/cetak_resume" class="btn btn-primary btn-block"><b>Download Resume</b></a>-->
			    <form id="form_print" name="form_print" action="<?=base_url()?>auth/page/pdfresumepencaker/cetak_resume" method="post" target="_blank" enctype="multipart/form-data">
	<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
			<div>

						<input type="hidden" name="iduser" id="iduser" value="<?=$item->iduser?>" size="60" style="min-width:250px"></br></br>

						</div>
     
	 <input type="submit" class="btn btn-primary btn-block" value="Download Resume">
	 </form>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
               <?=$item->education?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted"><?=$item->alamat?> <?=$item->nm_kecamatan?></p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
            
                <span class="label label-warning"><?=$item->skill?></span>
              </p>

              <hr>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#timeline" data-toggle="tab">Histori <?=$item->nama?></a></li>
            </ul>
			
			 <?php
                                   $rs = $this->db->query("SELECT DATE_FORMAT(a.log,'%d %M %Y') AS tgl_daftar,DATE_FORMAT(a.log,'%T') AS jam,e.nama_perusahaan AS nm_perusahaan,b.*,d.nama_job AS nm_job FROM ngi_jobapplied a
LEFT JOIN user_pelamar b ON a.id_pelamar=b.iduser
LEFT JOIN ngi_joblist c ON a.id_job=c.id
LEFT JOIN ngi_perusahaan e ON c.idperusahaan=e.idperusahaan
LEFT JOIN ngi_job d ON c.posisi=d.id WHERE iduser='".$this->input->post('iduser')."' ORDER BY a.log DESC");
                                    foreach($rs->result() as $item2){
                                 
                                    ?>
            <div class="tab-content">
              <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          <?=$item2->tgl_daftar?>
                        </span>
                  </li>
                  <li>
                    <i class="fa fa-user bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> <?=$item2->jam?></span>

                      <h3 class="timeline-header">Melamar Sebagai <a href="#"><?=$item2->nm_job?></a> di&nbsp;<?=$item2->nm_perusahaan?></h3>
                    </div>
                  </li>
                 
                <?php } ?>
                
                 
                  <!-- END timeline item -->
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
</div>