<script>
    $(document).ready(function(){
        $(document).on('click','.lmrzknews2',function(){
        var $this = $('.lmrzknews2');
        var index = $this.index($(this));
        var vdata = $this.eq(index).attr('data').split('|');
        $('.sertifikat2').html(' <embed src="'+vdata[1]+'" width="100%" style="min-height:350px" />');
        $('#myModal2').modal('show');
    });
    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?25:$this->input->post('per_page'));
    $cari = $this->input->post('cari');
    
    if($cari!="") $where.= " where (a.nama_perusahaan like '%".$cari."%')";
    
    $n = intval($this->uri->segment(5));
    $q = "SELECT a.*,DATE_FORMAT(a.log_pengajuan,'%d %M %Y ') AS log_pengajuan,e.nama AS jns_usaha,b.province AS propinsi,c.subdistrict_name AS kec,d.city_name AS kota FROM ngi_perusahaan a 
LEFT JOIN ngi_provinsi b ON a.province_id=b.id
LEFT JOIN ngi_kecamatan c ON a.kec_id=c.subdistrict_id
LEFT JOIN ngi_kota d ON a.kab_id=d.city_id
LEFT JOIN `mast_jenisusaha` e ON a.jenisusaha=e.id ";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'bcc/page/data/manpenyedialowongan';

    $config['total_rows']           = $totrows;
    $config['per_page']             = $per_page;
    $config['uri_segment']          = 5;
    $config['is_ajax_paging']       = TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("$q $where order by a.nama_perusahaan limit ".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."
    ");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th align="center">#</th>
        <th>NAMA PERUSAHAAN</th>
        <th>ALAMAT </th>
        <th>DOMISILI PERUSAHAAN</th>
        <th>NAMA PENANGGUNG JAWAB</th>
		<th>JENIS USAHA</th>
        <th>TGL PENGAJUAN</th>
        <th>PROSES</th>
		<th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;

	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><a href="#" title="Detail" data="<?=$item->idperusahaan?>"><?=$item->nama_perusahaan?></a></div>
        </td>
        <td>
            <div class="text-left"><?=((strlen($item->alamat) > 15)? substr($item->alamat,0,15)."..." : $item->alamat)?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->kota?>,<?=$item->propinsi?></div>
        </td>
		  <td>
            <div class="text-left"><?=$item->nm_dir?></div>
        </td>
         <td>
            <div class="text-left"><?=$item->jns_usaha?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->log_pengajuan?>  &nbsp;</div>
        </td>
        <td>
            <div class="text-left">
                <?php
                    if($item->verified_disnaker == "2"){
                        echo "<font color='red'>Permohonan Aktivasi ditolak</font>";
                    }elseif ($item->verified_disnaker == "1") {
                        echo "<font color='green'>Permohonan Aktivasi diterima</font>
                       ";

                    }else{
                        echo "<button class='btn btn-success btn-sm acc' data='".$item->idperusahaan."'>Aktivasi</button><button class='btn btn-danger btn-sm dec' style='margin-left:10px' data='".$item->idperusahaan."'>Tolak</button>";
                    }
                ?>
				 
            </div>
        </td>
		<td>
          
        <?php if($item->verified_disnaker==1){?>
        <a href="#" data="pdf|<?=base_url()?>bcc/page/pdfver/cetak_ver?&o=L&id=<?=$item->idperusahaan?>" class="lmrzknews2"> <i class="fa fa-print"></i> sertifikat</a>
        <?php }?>
      
            <a href="#" title="Edit" data="<?=$item->idperusahaan?>"><i class=" text-green fa fa-pencil"></i></a>
            <a href="#" title="Delete" data="<?=$item->idperusahaan?>"><i class=" text-red fa fa-trash"></i></a>
		
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>

<div id="myModal2" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Sertifikat Perusahaann terverifikasi</h4>
      </div>
      <div class="modal-body sertifikat2">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->