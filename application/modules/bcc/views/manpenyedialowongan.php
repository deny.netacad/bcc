 <script>
	function PropinsiFormatResult(data) {
        var markup= "<li>"+ data.province+"</li>";
        return markup;
    }

    function PropinsiFormatSelection(data) {
        return data.province;
    }


function KabFormatResult(data) {
        var markup= "<li>"+ data.city_name+" ("+ data.type+")</li>";
        return markup;
    }

    function KabFormatSelection(data) {
     var markup= ""+ data.city_name+" ("+ data.type+")";
        
        return markup;
    }
function KecFormatResult(data) {
        var markup= "<li>"+ data.subdistrict_name+"</li>";
        return markup;
    }

    function KecFormatSelection(data) {
     var markup= ""+ data.subdistrict_name+"";
        
        return markup;
    }
     $(document).ready(function(){
        
		$('#per_page').change(function(){
			refreshData();
		});
		 $('#alert1').hide();
		  $('.select2').select2();
		 $('.thn').datepicker({
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years"
		}).on('changeDate',function(){
			$('.thn').datepicker('hide');
			loadDataKomoditas();
		});
		$('#cari').keyup( $.debounce(250,refreshData));
		$.ajax({
            type:'post',
            url:'<?=base_url()?>bcc/page/data/manpenyedialowongan',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
        
        $("#province_id").select2({
		    id: function(e) { return e.id }, 
		    placeholder: "PROVINSI",
		    minimumInputLength: 0,
		    multiple: false,
		    allowClear:true,
		    ajax: { 
		        url: "<?=base_url()?>bcc/ajaxfile/cariPropinsi",
		        dataType: 'jsonp',
		        type:'POST',
		        quietMillis: 100,
		        data: function (term, page) {
		            return {
		                keyword: term, //search term
		                per_page: 5, // page size
		                page: page, // page number
		                '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
		            };
		        },
		        results: function (data, page) { 
		            var more = (page * 5) < data.total;
		            return {results: data.rows, more: more};
		        }
		    },
		    formatResult: PropinsiFormatResult, 
		    formatSelection: PropinsiFormatSelection
		});


		$("#kab_id").select2({
		        id: function(e) { return e.city_id }, 
		        placeholder: "KABUPATEN / KOTA",
		        minimumInputLength: 0,
		        multiple: false,
		        allowClear:true,
		        ajax: { 
		            url: "<?=base_url()?>bcc/ajaxfile/cariKab",
		            dataType: 'jsonp',
		            type:'POST',
		            quietMillis: 100,
		            data: function (term, page) {
		                return {
		                    keyword: term, //search term
		                    per_page: 5, // page size
		                    page: page, // page number
		                    province_id:$('#province_id').val(),
		                    '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
		        
		                
		                };
		            },
		            results: function (data, page) { 
		                var more = (page * 5) < data.total;
		                return {results: data.rows, more: more};
		            }
		        },
		        formatResult: KabFormatResult, 
		        formatSelection: KabFormatSelection
		    });
			
        $("#kec_id").select2({
        id: function(e) { return e.subdistrict_id }, 
        placeholder: "KECAMATAN",
        minimumInputLength: 0,
        multiple: false,
        allowClear:true,
        ajax: { 
            url: "<?=base_url()?>bcc/ajaxfile/cariKec",
            dataType: 'jsonp',
            type:'POST',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    keyword: term, //search term
                    per_page: 5, // page size
                    page: page, // page number
                    province_id:$('#province_id').val(),
                    city_id:$('#kab_id').val(),
                    
                    '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
        
                
                };
            },
            results: function (data, page) { 
                var more = (page * 5) < data.total;
                return {results: data.rows, more: more};
            }
        },
        formatResult: KecFormatResult, 
        formatSelection: KecFormatSelection
    });
		/* action save */
         $("#form1").submit(function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			
			if(confirm('simpan data ..?')){
				$.post(url, $form.serialize(),
				function(data) {
					window.location = "#";
					var ret = $.parseJSON(data);
					$('#alert1').html(ret.text);
					$('#alert1').fadeIn(3000);
					$('#alert1').fadeOut(3000);
					$('#form1').trigger('reset');
				});		
			}else{
				$('#form1').trigger('reset');
				$('#province_id').trigger('reset');
				$('#kab_id').trigger('reset');
				$('#kecamatan_id').trigger('reset');
				$('#fasilitas').trigger('reset');
			}
		});

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
			$('#id_edit').val('');
        });
		
		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>bcc/page/editPerusahaan',
				type: 'post',
				data: { 
					'idperusahaan':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					$('#id_edit').val(ret['idperusahaan']);
					for(attrname in ret){
						$('#'+attrname).val(ret[attrname]);
					}
					$("#fasilitas").select2("val", ret["fasilitas"].split(","));
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>bcc/page/delPerusahaan',
					type: 'post',
					data: { 
						'idperusahaan':$this.eq(index).attr('data'),
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
		$(document).on('click','a[title="Detail"]',function(){
			var $this = $('a[title="Detail"]');
			var index = $this.index($(this));
			
			$.ajax({
				url:'<?=base_url()?>bcc/page/view/userpenyedia_data',
				type:'post',
				data:{ 'idperusahaan':$this.eq(index).attr('data'),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){},
				success:function(response){
					$('#div-detail').html(response);
					$('#myModal').modal('show',function(){
						keyboard:true
					});
					return false;
				}
			});
		
		}).on('click','.acc', function(){
			if(confirm("Apa anda yakin ingin Aktivasi?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>bcc/page/setaktiv',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						//table: 'ngi_perusahaan',
						val : '1'
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		}).on('click','.dec', function(){
			if(confirm("Apa anda yakin ingin menolak Aktivasi?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>bcc/page/setaktiv',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						//table:'ngi_perusahaan',
						val : '2'
					},
					beforeSend:function(){
					var $this = $('a[title="Detail"]');
					var index = $this.index($(this));
			
					$.ajax({
						url:'<?=base_url()?>bcc/page/view/userpenyedia_data',
						type:'post',
						data:{ 'idperusahaan':$this.eq(index).attr('data'),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
					});
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		}).on('click','.print', function(){
			if(confirm("Apa anda yakin ingin mencetak bukti terverifikasi?")){
				var id = $(this).attr('data');
				$.ajax({
		            type:'post',
		            teget:'_blank',
		            url:'<?=base_url()?>bcc/page/pdfresumepencaker/cetak_resume',
		            data:{ 
						'per_page':$('#per_page').val(),'cari':$('#cari').val(),
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
		            beforeSend:function(){
		                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
		            },
		            success:function(response){
		                $('#result').html(response);
		            }
		        })
				
			}
		})
	
	
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#title').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});

    });
	
	function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>bcc/page/data/manpenyedialowongan',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }
    $(function(){
        $(document).on('input', '#password_ori', function(){
			$('#password_check').html('<i class="fa fa-spinner fa-spin"></i>');
			
            if($(this).val() == $('#password').val()){
				$('#password_check').html('<i class="fa fa-check"></i>');
                $('#submit_btn').attr('disabled',false);
            }else{
				$('#password_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        }).on('input', '#username', function(){
            $('#username_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();            
            var regexp = /[A-Za-z0-9]{3,}$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkusername_perusahaan',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        username: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#username_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#username_check').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#username_check').html('<i class="fa fa-times"></i>');
                $('#regBtn').attr('disabled',true);
            }
        }).on('input', '#no_ktp', function(e){
     //    	var val = $(this).val();

     //    	if(val.length == '16'){
     //    		$.ajax({
     //    			url : 'https://ayokitakerja.kemnaker.go.id/tools/check_nik/' + val,
     //    			type: 'get',
					// contentType: 'text/plain',
					// xhrFields: {
					// 	withCredentials: false
					// },
					// header : {
					// 	'Access-Control-Allow-Origin' : '*'
					// },
     //    			success: function(rs){
     //    				console.log(rs)
     //    			}
     //    		})
     //    	}else{
     //    		$('#submit_btn').attr('disabled',true);
     //    	}
        }).on('input', '#email', function(){
            $('#email_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();            
            var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkemail',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        email: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#email_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#email_check').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#email_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        });
    })
    

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
		  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		 </ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Manajemen Data Penyedia Lowongan (Perusahaan)</h3>
				</div><!-- /.box-header -->
				<div id="alert1" class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">x</button>
		  <strong>Sukses!</strong> Data telah tersimpan
		</div>
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>bcc/page/saveperusahaan">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<label class="text-muted">AKUN LOGIN PERUSAHAAN</label>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Email</label>
						<div class="input-group">
							<input type="text" class="form-control" name="email" id="email" placeholder="Email*" required="">
							<span class="input-group-addon cek" id="email_check"></span>                  
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Username</label>
						<div class="input-group">
							<input type="text" class="form-control" name="username" id="username" placeholder="Username*" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka tanpa spasi, 3 - 10 karakter">
                           	<span class="input-group-addon cek" id="username_check"></span>
                                                
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Password</label>
						<div class="input-group">
							<input type="password" class="form-control" name="password" id="password" placeholder=" password*">
                            <span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Konfirmasi Password</label>
						<div class="input-group">
							<input type="password" class="form-control" name="password_ori" id="password_ori" placeholder="Konfirmasi password*">
                            <span class="input-group-addon cek" id="password_check"></span>
                                                
						</div>
					</div>
					<label class="text-muted">DATA PERUSAHAAN</label>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Nama Perusahaan</label>
						<div class="input-group">
							<input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" placeholder="Nama Perusahaan*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                      
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Tahun Berdiri</label>
						<div class="input-group">
							 <input type="text" class="form-control thn" name="tahun" id="tahun" placeholder="Tahun Berdiri*" required="" autocomplete="off">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Alamat Perusahaan</label>
						<div class="input-group">
							<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">No Telp</label>
						<div class="input-group">
							<input type="text" class="form-control" name="telp" id="telp" placeholder="Telp*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Nama Penanggung Jawab</label>
						<div class="input-group">
							<input type="text" class="form-control" name="nm_dir" id="nm_dir" placeholder="Nama Penanggung Jawab*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                      
						</div>
					</div>
					<!-- <div class="form-group">
						<label for="title" class="col-sm-2 control-label">No. Ktp Penanggung Jawab</label>
						<div class="input-group">
							<input type="text" class="form-control" name="noktpdir" id="noktpdir"  pattern="[0-9]{16,16}" title="No ktp harus berupa angka, 16 digit" placeholder="Nomor Ktp Penanggung Jawab*" maxlength="16" required=""> 
							<span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div> -->
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">No Hp Penanggung Jawab</label>
						<div class="input-group">
							<input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="No Hp*" required="">
							<span class="input-group-addon" style="visibility: hidden;"></span>                   
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Pusat/Cabang</label>
						<div class="input-group">
							<select id="lokasi" name="lokasi" class="form-control" required="">
								<option value="-">Pusat/Cabang*</option>
								<option value="Pusat">Pusat</option>
								<option value="Cabang">Cabang</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Jenis Usaha</label>
						<div class="input-group">
										<select name="jenisusaha" id="jenisusaha" class="form-control" required="">
											<option value="">Jenis Usaha*</option>
                                    <?php
                                        $rs = $this->db->query("select * from mast_jenisusaha")->result();

                                        foreach ($rs as $item) {
                                            echo '<option value="'.$item->id.'">'.$item->nama.'</option>';
                                        }
                                    ?>
										</select>
										<span class="input-group-addon" style="visibility: hidden;"></span>  
							</div>
                     </div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label" for="province_id">Provinsi</label>
						<div class="input-group">
							<input type="hidden" class="" placeholder="Provinsi" name="province_id" id="province_id" required="" style="width: 100%;">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label" for="kab_id">Kabupaten / Kota</label>
						<div class="input-group">
							<input type="hidden" class="" placeholder="Kabupaten / Kota" name="kab_id" id="kab_id" required="" style="width: 100%;">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label" for="kecamatan_id">Kecamatan / Kelurahan</label>
						<div class="input-group">
							<input type="hidden" class="" placeholder="Kecamatan / Kelurahan" name="kec_id" id="kec_id" required="" style="width: 100%;">
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					
					
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label" for="fasilitas_perusahaan">Fasilitas Perusahaan</label>
						<div class="input-group">
							<select class="select2" id="fasilitas" name="fasilitas[]" multiple="multiple" data-placeholder="Fasilitas Perusahaan"
							style="width: 100%;">
							<option value="Ruang Ibadah">Ruang Ibadah</option>
							<option value="Ruang Klinik">Ruang Klinik</option>
							<option value="Training Center">Training Center</option>
							<option value="Ruang Laktasi">Ruang Laktasi</option>
							<option value="Sarana Olahraga">Sarana Olahraga</option>
							<option value="Kantin">Kantin</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>                    
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Program Pelatihan Kerja</label>
						<div class="input-group">
							<select id="program" name="program" class="form-control" required="">
								<option value="-">Ada/Tidak*</option>
								<option value="Ada">Ada</option>
								<option value="Tidak">Tidak</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Program Pemagangan</label>
						<div class="input-group">
							<select id="magang" name="magang" class="form-control" required="">
								<option value="-">Ada/Tidak*</option>
								<option value="Ada">Ada</option>
								<option value="Tidak">Tidak</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2 control-label">Limbah Produksi</label>
						<div class="input-group">
							<select id="limbah" name="limbah" class="form-control" required="">
								<option value="-">Ada/Tidak*</option>
								<option value="Ada">Ada</option>
								<option value="Tidak">Tidak</option>
							</select>
							<span class="input-group-addon" style="visibility: hidden;"></span>  
						</div>
					</div>
					
					
					
					</div>
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right" >Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Daftar Data Perusahaan</h3>
					  <div class="box-tools">
						<div class="input-group pull-right" style="width: 150px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:200px" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						  </div>
						</div>
						<div class="btn-group">
						<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="4">4</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					</div>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
		
		
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modal-title2" style="text-transform:capitalize">DETAIL PERUSAHAAN</h4>
          </div>
          <div class="modal-body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                             <div id="div-detail">
	
	</div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>