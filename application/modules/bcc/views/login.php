<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$site_title?></title>
  <link rel="icon" type="image/png" href="<?=$def_ass?>dist/img/iconngosis.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?=$def_ass?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=$def_ass?>css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=$def_ass?>css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=$def_ass?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=$def_ass?>plugins/iCheck/square/blue.css">
  <link href="<?=$def_css?>animate.css" rel="stylesheet" type="text/css" />
   

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><img src="<?=base_url()?>template/HTML/job_dark/images/header/logo4.png" alt="" width=150 ></a>
  </div>
  <section id="content" class="form-box animated shake ">	
  <div class="login-box-body">
    <p class="login-box-msg">Selamat datang</p>
    <form action="<?=base_url()?>auth/login" enctype="multipart/formdata" method="post" >
		<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" required="" name="username" id="username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" required="" name="userpass" id="userpass">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
</form>
  </div>
</div>
</section>
<script src="<?=$def_ass?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=$def_ass?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?=$def_ass?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#username').focus();
	});

	$(function () {
	$('input').iCheck({
	  checkboxClass: 'icheckbox_square-blue',
	  radioClass: 'iradio_square-blue',
	  increaseArea: '20%' // optional
	});
	});
</script>
</body>
</html>

