
<style>
    div.footnotes { 
  page-break-after: always; 
  font-family:verdana;
}
.tengah{
    margin-top: 150px;
     text-align: center ;
}
.kiri{
    margin-top: 10px;
     text-align: left ;
}
.tengah1{
    margin-top: 50px;
     text-align: center ;
}
.tengah2{
    margin-top: -10px;
     text-align: center ;
}
img.tengah{
     width:150px;
     height:300px;
}
.h3{
	font-size:30px;
	text-align:center;
}
.p{
	font-size:30px;
	text-align:left;
}
.judul{
	margin-top:10px;
	 text-align: center ;
}
.isi{
	font-size:14px;
	margin-top:-10px;
	 text-align: center;
}

#divP {
  width: 900px;
  }
 
 
#div1 {
  font-family: Tahoma, Geneva, Helvetica, sans-serif;
  font-size: 14px;
  text-align: justify;
  padding-left:50px;
  padding: 10px;
  width: 360px;
  float: left;
  }
 #div2 {
  font-family: Tahoma, Geneva, Helvetica, sans-serif;
  font-size: 14px;
  text-align: justify;
  padding: 10px;
  width: 360px;
  float: right;
  }
</style>

<?php 

$rs=$this->db->query("SELECT a.*, b.nama_job, c.photo, c.nama_perusahaan, 
d.subdistrict_name, d.city, d.province, e.category,a.seo_url FROM (SELECT * FROM ngi_joblist) a
LEFT JOIN ngi_job b ON a.posisi = b.id
LEFT JOIN ngi_perusahaan c ON a.idperusahaan = c.idperusahaan
LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
LEFT JOIN ngi_jobcategory e ON a.idcategory = e.id 
WHERE a.is_aktif=1 and now() < date_add(a.date_end,interval 1 day) and a.idperusahaan = '".$this->input->post('idperusahaan')."'");
                           
foreach($rs->result() as $item){
    echo "<div class=footnotes>
        </br>
		</br>
		</br>
		</br>
		</br>
		</br>
     <div class=tengah><h1><b>".strtoupper($item->nama_perusahaan)."</b></h1></div>
	   <div class=isi><small>".$item->subdistrict_name.",".$item->city.",".$item->province."</small></div><br>
        <div class=tengah2><img src=\"https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=https://bogorcareercenter.bogorkab.go.id/portal/jobs/".$item->seo_url."&choe=UTF-8\" /></div><br>
       <div class=isi><small style=font-style:italic>Scan Disini untuk langsung melamar</small></div><br>
	   
	   <div class=judul><h1>".$item->title."</h1></div>
	   
<div id=divP>
      <div>
		  <div id=div1>
			<h2 class=kiri>Job Description</h2>
			<p>".$item->deskripsi."</p>
			</div>
			<div id=div2>
			<h2 class=kiri>Responsibilities</h2>
			<p>".$item->responsibility."</p>
		  </div>
	  </div>
	  <div>
	  <div id=div1>
        <h2 class=kiri>Qualifications</h2>
        <p>".$item->qualification."</p>
		</div>
		<div id=div2>
		<h2 class=kiri>Next Step</h2>
        <p>".$item->next_step."</p>
      </div>
	  </div>
    </div>            
     
        
    </div>";
    
}

?>