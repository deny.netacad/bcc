<script>
$(document).ready(function(){

		var options = {
			chart: {
				renderTo: 'graph1',
				defaultSeriesType: 'column'
			},
			title: {
				text: 'Grafik Statistik Kunjungan Pasien'
			},
			subtitle: {
				text: 'Source: Nusantara Global Inovasi'
			},
			xAxis: {
				categories: []
			},
			yAxis: {
				min:0,
				title: {
					text: 'Jumlah'
				}
			},
			series: [],
			exporting: {
				sourceWidth: 1200,
				sourceHeight:600
			}
		};
	
		$.post('<?=base_url()?>auth/graphStatistik',{ 'thn':'<?=$this->input->post('thn')?>' }, function(data) {
			var lines = data.split('\n');
			$.each(lines, function(lineNo, line) {
				var items = line.split(',');
				if(line!=''){
					if (lineNo == 0) {
						$.each(items, function(itemNo, item) {
						// judul di awal
							if (itemNo > 0) options.xAxis.categories.push(item);
						});
					}else {
						var series = { 
							data: []
						};
						$.each(items, function(itemNo, item) {
							if (itemNo == 0) {
								series.name = item;
							} else {
								series.data.push(parseFloat(item));
							}
						});
						options.series.push(series);
					}
				}
			});
			
			var chart = new Highcharts.Chart(options);
		});	
		

    
});



</script>
<div class="row-fluid">
	<div align="center">
		<h4></h4>
	</div>
</div>
<div id="graph1" style="width:95%"></div>

</div>
