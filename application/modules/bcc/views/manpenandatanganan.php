 <script>
	
    $(document).ready(function(){
    	$('.dt').datepicker({
			format:'yyyy-mm-dd',
			minViewMode:0,
			viewMode:1
		}).on('changeDate',function(){
			$('.dt').datepicker('hide');
			loadDataReturtgl();
		});
        
		$('#per_page').change(function(){
			refreshData();
		});
		
		$('#cari').keyup( $.debounce(250,refreshData));
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);

                        if(ret.success){
                        	$('#form1').trigger('reset');
                        }

                        alert(ret.text);
                    });
            }else{
                // $('#form1').trigger('reset');
            }
        });

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
			$('#id_edit').val('');
        });
		
		$(document).on('click','a.ajax-replace', function(){
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>bcc/page/editpejabat',
				type: 'post',
				data: { 
					'id_pejabat':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					var ret = $.parseJSON(response);
					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					$('#id_edit').val(ret['id_pejabat']);
					for(attrname in ret){
						$('#'+attrname).val(ret[attrname]);
					}
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>bcc/page/delpejabat',
					type: 'post',
					data: { 
						'id_pejabat':$this.eq(index).attr('data'),
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						var ret = $.parseJSON(response);
						alert(ret.text);
						refreshData();
					}
				});
			}
        });
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#title').focus();
				break;
				case 1:
					refreshData();
				break;
			}
		});

    });
	
	function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>bcc/page/data/manpenandatanganan',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
		  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		 </ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Manajemen Pejabat Penandatanganan Kartu AK-1</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>bcc/page/savepejabat">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					
					<div class="form-group">
						<label for="parent_id" class="col-sm-2 control-label">Nama Pejabat</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Pejabat">
						</div>
					</div>
					<div class="form-group">
						<label for="parent_id" class="col-sm-2 control-label">NIP</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nip" name="nip" placeholder="NIP">
						</div>
					</div>
					<div class="form-group">
						<label for="parent_id" class="col-sm-2 control-label">Jabatan</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Ex. Kepala Dinas/bidang/ a.n kepala dinas ,dll">
						</div>
					</div>
					


						</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Daftar Manajemen Event</h3>
					  <div class="box-tools">
						<div class="input-group pull-right" style="width: 150px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:200px" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						  </div>
						</div>
						<div class="btn-group">
						<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:60px">
						<option value="">-</option>
						<option value="4">4</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					</div>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>