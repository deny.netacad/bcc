<li class="treeview">
	<a href="#" ><i class="text-blue icomoon-eye"></i> <span> &nbsp; Pengawasan Pangan</span> <i class="fa fa-angle-left pull-right"></i></a>
	<ul class="treeview-menu">
		<li class="treeview">
		<a href="#" ><i class="text-blue icomoon-office"></i> <span> &nbsp; Produsen</span>  <i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li><a href="<?=base_url()?>pengawasan/prodsegar"><i class="text-blue icomoon-leaf"> </i> Pangan Segar</a></li>
			</ul>
		</li>
		<li class="treeview">
		<a href="#" ><i class="text-blue icomoon-truck"></i> <span> &nbsp; Distributor</span>  <i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li><a href="<?=base_url()?>pengawasan/distrisaji"><i class="text-blue icomoon-mug"> </i> Pangan Siap Saji</a></li>
				<li><a href="<?=base_url()?>pengawasan/distriolahan"><i class="text-blue icomoon-lab"> </i> Pangan Olahan</a></li>
				<li><a href="<?=base_url()?>pengawasan/distrisegar"><i class="text-blue icomoon-leaf"> </i> Pangan Segar</a></li>
			</ul>
		</li>

		<li><a href="<?=base_url()?>pengawasan/rekap"><i class="text-blue icomoon-mug"> </i> Rekap data survei</a></li>
	</ul>
</li>
<li class="treeview">
	<a href="#" ><i class="text-blue icomoon-profile"></i> <span> &nbsp; Sertifikasi Pangan</span> <i class="fa fa-angle-left pull-right"></i></a>
	<ul class="treeview-menu">
		<li><a href="<?=base_url()?>pengawasan/pangansiapsaji"><i class="text-blue icomoon-mug"> </i> Pangan Siap Saji (PSS)</a></li>
		<li><a href="<?=base_url()?>pengawasan/panganolahan"><i class="text-blue icomoon-lab"> </i> Pangan Olahan (PIRT)</a></li>
		<li><a href="<?=base_url()?>pengawasan/pangansegar"><i class="text-blue icomoon-leaf"> </i> Pangan Segar (PSAT)</a></li>
	</ul>
</li>