<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}
	
	
    $(document).ready(function(){
		
		
		var config_image = {
            toolbar : 'Image',
            height: '200',
            width: '100%'
        };
		
		var field = $('#ujilab').html();

		$('#tgl_survei').datepicker({
			format:'dd-mm-yyyy',
			autoclose:true
		});
		
        $('#links_thumb').ckeditor(config_image);
		
		$('#per_page').change(function(){
			if(status == 'data'){
				refreshData();
			}else{
				kesimpulanData();
			}
			
		});
		
		$('#cari').keyup(function(){
			if(status == 'data'){
				refreshData();
			}else{
				kesimpulanData();
			}
		});

		$('#pilih_pasar').change(function(){
			if(status == 'data'){
				refreshData();
			}else{
				kesimpulanData();
			}
		});
		
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                       	if(ret.success){
                        	$('#form1').trigger('reset');
                       	}
                        alert(ret.text);
                        console.log(ret.text);
                    });
            }
			$('#id_edit').val('');
        });
		
		
        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			CKupdate();
			$('#id_edit').val('');
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
			$('#jenis_pangan').html('<option value="">-Pilih Rantai Pangan Dahulu-</option>');
			$('#hasil').html('<p style="text-align:center"><b>Silahkan lengkapi form diatas terlebih dahulu</b></p>');
			$('#ujilab').html(field);
        });
		

		$(document).on('click','a.ajax-replace', function(){
			
			$.ajax({
				type:'post',
				url:$(this).attr('href'),
				data:{ 'per_page':$('#per_page').val(),'cari':$('#cari').val(), 'pasar':$('#pilih_pasar').val(), '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
         },
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
			return false;
		});
		
		
		$(document).on('click','a[title=Edit]', function(){
			var $this = $('a[title=Edit]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>pengawasan/page/editdatasurvei',
				type: 'post',
				data: { 
					'id':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					// console.log(response);
					var rs = $.parseJSON(response);
					var ret = rs.data1;
					var arr = rs.data2;

					$('#myTab li a').eq(0).trigger('click');
					$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
					$('#id_edit').val(ret.id);
					getHasilPengawasan(ret.rantai_pangan,arr);
					getJenisLokasi(ret.rantai_pangan,ret.lokasi,ret.lokasi2);
					getKelurahan(ret.kecamatan,ret.kelurahan,ret.kantin);
					ujiLab(ret.perkiraan,field);

					$("input[name=perkiraan][value='"+ret.perkiraan+"']").prop("checked",true);
					$("input[name=formalin][value='"+ret.formalin+"']").prop("checked",true);
					$("input[name=boraks][value='"+ret.boraks+"']").prop("checked",true);
					$("input[name=methanil][value='"+ret.methanil+"']").prop("checked",true);
					$("input[name=rhodamin][value='"+ret.rhodamin+"']").prop("checked",true);
					$("input[name=perkiraan][value='"+ret.perkiraan+"']").prop("checked",true);

					for(attrname in ret){
						$('#'+attrname).val(ret[attrname]);
					}
				}
			});
        });
		
		$(document).on('click','a[title=Delete]', function(){
			var $this = $('a[title=Delete]');
			var index = $this.index($(this));
			if(confirm('Hapus item ini...?')){
				$.ajax({
					url: '<?=base_url()?>pengawasan/page/deldatasurvei',
					type: 'post',
					data: { 
						'id':$this.eq(index).attr('data')
						,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					success: function(response) {
						alert(response);
						refreshData();
					}
				});
			}
        }).on('click','a[title=Detail]', function(){
			var $this = $('a[title=Detail]');
			var index = $this.index($(this));
			$.ajax({
				url: '<?=base_url()?>pengawasan/page/detaildatasurvei2',
				type: 'post',
				data: { 
					'id':$this.eq(index).attr('data')
					,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				success: function(response) {
					$('#output').html(response);
					$('#details').modal({
						 backdrop: 'static',
		    			keyboard: false
					});
				}
			});


        })
	
		
		$('#myTab').on('shown.bs.tab',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#links_title').focus();
				break;
				case 1:
					if(status == 'data'){
						refreshData();
					}else{
						kesimpulanData();
					}
				break;
			}
		});

		$(document).on('change','#rantai_pangan', function(){
			var id = $(this).val(); 
			getHasilPengawasan(id,'');

			$('#lokasi').html('<option value=""> Loading ... </option>');
			getJenisLokasi(id,'','');
			
		}).on('change','#lokasi', function(){
			var id = $(this).val(); 
			$('#lokasi2').html('<option value=""> Loading ... </option>');
			getPasar(id,'');
			
		}).on('change','#kecamatan', function(){
			var id = $(this).val(); 
			$('#kelurahan').html('<option value=""> Loading ... </option>');
			getKelurahan(id,'','');
			
		}).on('change','#kelurahan', function(){
			var id = $(this).val(); 
			$('#kantin').html('<option value=""> Loading ... </option>');
			getKantin(id,'');
			
		}).on('change','input[name=perkiraan]', function(){
			var id = $(this).val();
			ujiLab(id,field);

		}).on('click','.acc', function(){
			if(confirm("Apa anda yakin ingin meloloskan data ini?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>pengawasan/page/setlolos',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						val : '1'
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		}).on('click','.dec', function(){
			if(confirm("Apa anda yakin tidak ingin meloloskan data ini?")){
				var id = $(this).attr('data');

				$.ajax({
					url  : '<?=base_url()?>pengawasan/page/setlolos',
					type : 'post',
					data : {
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
						id : id,
						val : '0'
					},
					success: function(rs){
						alert(rs);
						refreshData();
					}
				})
			}
		})

	});

var status = 'data';
function refreshData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>pengawasan/page/data/distrisaji',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),'pasar':$('#pilih_pasar').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }

    function kesimpulanData(){
    	$('#footer').html('<button class="btn btn-warning pull-left" onclick="back()">Kembali</button>');

        $.ajax({
            type:'post',
            url:'<?=base_url()?>pengawasan/page/data/distrisajikesimpulan',
            data:{ 
				'per_page':$('#per_page').val(),'cari':$('#cari').val(),'pasar':$('#pilih_pasar').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }

   function kesimpulan(){
   		status = 'kesimpulan';
   		kesimpulanData();
   }

   function back(){
   		status = 'data';
   		$('#footer').html('<button class="btn btn-info pull-right" onclick="kesimpulan()">Kesimpulan</button>');
   		refreshData();
   }

   function getJenisPangan(id,idpangan){
    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/getjenispangan',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				$('#jenis_pangan').html(rs);

				if(idpangan != ''){
					$('#jenis_pangan').val(idpangan);
				}
			}
		})
    }

    function getJenisLokasi(id,idlokasi,idlokasi2){
    	var id = id - 5;

		if(id == 1){
			$('#pasar').hide();
		}else{
			$('#pasar').show();
		}

    	$('#simpan').html('Loading data....');
		$('#simpan').attr('disabled',true);

    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/getjenislokasi',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				$('#lokasi').html(rs);

				if(idlokasi != ''){
					$('#lokasi').val(idlokasi);

					if(id == 0){
						getPasar(idlokasi,idlokasi2);
					}
				}

				$('#simpan').html('Simpan');
				$('#simpan').attr('disabled',false);
			}
		});
    }

    function getPasar(id,array){
    	$('#simpan').html('Loading data....');
		$('#simpan').attr('disabled',true);

    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/getPasar',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				$('#lokasi2').html(rs);

				if(array != ''){
					$('#lokasi2').val(array);
				}

				$('#simpan').html('Simpan');
				$('#simpan').attr('disabled',false);
			}
		});
    }

    function getKelurahan(id,var1,var2){
    	$('#simpan').html('Loading data....');
		$('#simpan').attr('disabled',true);

    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/getKelurahan',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				$('#kelurahan').html(rs);

				if(var1 != ''){
					$('#kelurahan').val(var1);
					getKantin(var1,var2);
				}

				$('#simpan').html('Simpan');
				$('#simpan').attr('disabled',false);
			}
		});
    }

    function getKantin(id,array){
    	$('#simpan').html('Loading data....');
		$('#simpan').attr('disabled',true);

    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/getKantin',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				$('#kantin').html(rs);

				if(array != ''){
					$('#kantin').val(array);
				}

				$('#simpan').html('Simpan');
				$('#simpan').attr('disabled',false);
			}
		});
    }

    function getHasilPengawasan(id,array){
    	if(id == '5'){
    		$('#market').show();
    		$('#region').hide();
    		$('#nama_lokasi').html('UPTD');
    	}else if(id=='6'){
    		$('#market').show();
    		$('#region').show();
    		$('#nama_lokasi').html('Lokasi');
    	}else{
    		$('#market').hide();
    		$('#region').show();
    	}

    	if(id != '4'){
    		$('#kantins').hide();
    		$('#nama_pdg').html('Nama Pedagang');
    		$('#pluskantin').hide();
    	}else{
    		$('#kantins').show();
    		$('#nama_pdg').html('Nama Pengelola');
    		$('#pluskantin').show();
    	}

    	if(id == '6'){
    		$('#nama_pdg').html('Penanggung Jawab');
    	}


		$('#hasil').html('<div style="text-align:center; padding:20px;"><i class="fa fa-spinner fa-spin" style="font-size:48px;"></i></div>');
		$('#simpan').html('Loading data....');
		$('#simpan').attr('disabled',true);

    	$.ajax({
			url : '<?=base_url()?>pengawasan/ajaxfile/gethasilpengawasan2',
			type: 'post',
			data : {
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>',
				id : id
			},
			success: function(rs){
				if(array != ''){
					$.when($('#hasil').html(rs)).done(function() {
					  $('select[name="check[]"]').each(function(index){
					  		$(this).val(array[index].value);
					  })
					});
				}else{
					$('#hasil').html(rs);
				}

				$('#simpan').html('Simpan');
				$('#simpan').attr('disabled',false);
			}
		});
    }

    function ujiLab(id,field){
    	if( id == "0"){
			$('#ujilab').html('');
		}else{
			$('#ujilab').html(field);
		}
    }
    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="text-blue icomoon-pencil"></i> Update</a></li>
		  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <i class="text-blue icomoon-libreoffice"></i> Daftar</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Survei Distributor Pangan Siap Saji</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>pengawasan/page/savedatasurvei">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<input type="hidden" id="jenis_data" name="jenis_data" value="distrisaji" />
				<div class="box-body">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="links_title" class="col-sm-2 control-label">Jenis Lokasi</label>
							<div class="col-sm-10">
								<select class="form-control" id="rantai_pangan" name="rantai_pangan" required="true">
									<option value="">-Silahkan Pilih-</option>
									<?php
										$option = $this->db->query('select * from ref_rantaipangan where `group` = "pangan siap saji"')->result();
										foreach ($option as $opt) {
											echo '<option value="'.$opt->id.'">'.$opt->nama.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div id="market" style="display:none;">
							<div class="form-group">
								<label for="links_target" class="col-sm-2 control-label" id="nama_lokasi">Lokasi</label>
								<div class="col-sm-10">
									<select class="form-control" id="lokasi" name="lokasi" >
										<option value="">-Pilih Jenis Lokasi Dahulu-</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="display:none;" id="pasar">
								<label for="links_target" class="col-sm-2 control-label">Pasar</label>
								<div class="col-sm-10">
									<select class="form-control" id="lokasi2" name="lokasi2" >
										<option value="">-Pilih Lokasi Dahulu-</option>
									</select>
								</div>
							</div>
						</div>
						<div id="region" style="display:none;">
							<div class="form-group">
								<label for="links_type" class="col-sm-2 control-label">Kecamatan</label>
								<div class="col-sm-10">
									<select class="form-control" id="kecamatan" name="kecamatan">
										<option value="">-Silahkan Pilih-</option>
										<?php
											$option = $this->db->query('select * from ref_kecamatan')->result();
											foreach ($option as $opt) {
												echo '<option value="'.$opt->id.'">'.$opt->kecamatan.'</option>';
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="links_type" class="col-sm-2 control-label">Kelurahan</label>
								<div class="col-sm-10">
									<select class="form-control" id="kelurahan" name="kelurahan">
										<option value="">-Silahkan Pilih Kecamatan-</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="display:none;" id="kantins">
								<label for="links_type" class="col-sm-2 control-label">Kantin</label>
								<div class="col-sm-10">
									<select class="form-control" id="kantin" name="kantin">
										<option value="">-Silahkan Pilih Kelurahan-</option>
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="intro" class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-10">
								<input type="text-area" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required="true">
								
							</div>
						</div>

						<div class="form-group">
							<label for="links_title" class="col-sm-2 control-label">Tanggal Survei</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="tgl_survei" name="tgl_survei" placeholder="Tanggal Survei" required="true">
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="form-group">
							<label for="links_target" class="col-sm-2 control-label" id="nama_pdg">Nama Pedagang</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="nama_pedagang" name="nama_pedagang" placeholder="Nama Pedagang" required="true">
							</div>
						</div>
						<div id="pluskantin" style="display:none;">
							<div class="form-group">
								<label for="links_target" class="col-sm-2 control-label">Status Kepemilikan</label>
								<div class="col-sm-10">
									<select class="form-control" id="status_kep" name="status_kep">
										<option value="">-Silahkan Pilih Status Kepemilikan-</option>
										<option value="Milik Sekolah">Milik Sekolah</option>
										<option value="Perorangan">Perorangan</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="links_target" class="col-sm-2 control-label">Jumlah Pekerja</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="jml_pekerja" name="jml_pekerja" placeholder="Jumlah Pekerja" required="true">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="links_type" class="col-sm-2 control-label">Tempat</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="tempat" name="tempat" placeholder="Tempat" required="true">
							</div>
						</div>
						<div class="form-group">
							<label for="intro" class="col-sm-2 control-label">Keterangan</label>
							<div class="col-sm-10">
								<input type="text-area" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan">
								
							</div>
						</div>
					</div>

					
				</div><!-- /.box-body -->

				<div class="box-header with-border" style="padding-top: 20px">
					<h3 class="box-title">Hasil Pengawasan</h3>
				</div><!-- /.box-header -->
				<div class="box-body" id="hasil">
					<p style="text-align:center"><b>Silahkan lengkapi form diatas terlebih dahulu</b></p>
				</div>
				<div id="ujilab">
					<div class="box-header with-border" style="padding-top: 20px">
						<h3 class="box-title">Hasil Uji Lab</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="intro" class="col-sm-2 control-label">Formalin</label>
								<div class="col-sm-10">
									<label class="radio-inline">
								      	<input type="radio" name="formalin" value="1" required="true">Positif
								    </label>
								    <label class="radio-inline">
								     	<input type="radio" name="formalin" value="0" required="true">Negatif
								    </label>
								</div>
							</div>
							<div class="form-group">
								<label for="intro" class="col-sm-2 control-label">Boraks</label>
								<div class="col-sm-10">
									<label class="radio-inline">
								      	<input type="radio" name="boraks" value="1" required="true">Positif
								    </label>
								    <label class="radio-inline">
								     	<input type="radio" name="boraks" value="0" required="true">Negatif
								    </label>
								</div>
							</div>
							<div class="form-group">
								<label for="intro" class="col-sm-2 control-label">Pestisida</label>
								<div class="col-sm-10">
									<label class="radio-inline">
								      	<input type="radio" name="pestisida" value="1" required="true">Positif
								    </label>
								    <label class="radio-inline">
								     	<input type="radio" name="pestisida" value="0" required="true">Negatif
								    </label>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="intro" class="col-sm-2 control-label">Rhodamin B</label>
								<div class="col-sm-10">
									<label class="radio-inline">
								      	<input type="radio" name="rhodamin" value="1" required="true">Positif
								    </label>
								    <label class="radio-inline">
								     	<input type="radio" name="rhodamin" value="0" required="true">Negatif
								    </label>
								</div>
							</div>
							<div class="form-group">
								<label for="intro" class="col-sm-2 control-label">Methanil Yellow</label>
								<div class="col-sm-10">
									<label class="radio-inline">
								      	<input type="radio" name="methanil" value="1" required="true">Positif
								    </label>
								    <label class="radio-inline">
								     	<input type="radio" name="methanil" value="0" required="true">Negatif
								    </label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right" id="simpan">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
			<div class="tab-pane" id="tab_2">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">List Survei Distributor Pangan Siap Saji</h3>
					  <div class="box-tools">
					</div><!-- /.box-header -->
					
					</div>
					<div class="box-body">
						<div class="input-group pull-right" style="width: 120px;margin-left: 10px;">
						  <input type="text" name="cari" id="cari" class="form-control input-sm pull-right" style="min-width:100px;" placeholder="Search">
						  <div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						  </div>
						</div>
						<div class="btn-group pull-right">
							<select class="form-control form-cari-ngi"  id="per_page" name="per_page" style="width:50px">
								<option value="">-</option>
								<option value="4">4</option>
								<option value="25">25</option>
								<option value="50">50</option>
							</select>
						</div>
						<!-- <div class="btn-group">
							<select class="form-control form-cari-ngi"  id="pilih_pasar" name="pilih_pasar" style="width:100px">
								<?php
									$option = $this->db->query('select * from ref_pasar')->result();
									foreach ($option as $opt) {
										echo '<option value="'.$opt->id.'">'.$opt->nama.'</option>';
									}
								?>
							</select>
						</div> -->
					  </div>
					<div class="box-body table-responsive no-padding" id="result"></div><!-- /.box-body -->
					<!-- <div class="box-footer" id="footer">
						<button type="submit" class="btn btn-info pull-right" onclick="kesimpulan()">Kesimpulan</button>
					</div> --><!-- /.box-body -->
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>


<!-- Modal -->
<div class="modal fade" id="details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Detail survei distributor pangan siap saji</h4>
      </div>
      <div id="output">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="col-sm-12">
						<div class="form-group">
							<label for="intro" class="col-sm-1 control-label">Perkiraan</label>
							<div class="col-sm-5">
								<label class="radio-inline">
							      	<input type="radio" name="perkiraan" checked value="1">Meragukan
							    </label>
							    <label class="radio-inline">
							     	<input type="radio" name="perkiraan" value="0">Tidak meragukan
							    </label>
							</div>
						</div>
					</div> -->