<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');
    $pasar = $this->input->post('pasar');

    if($cari!="") $where.= " and (a.nama_pedagang like '%".$cari."%' 
	 or a.jenis_pangan like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "
        select a.nama, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak' from (select a.id, a.nama, count(a.id) as jml, c.lolos  from ref_jenispangan a inner join
        (select id from ref_rantaipangan where `group` = 'pangan segar') b on b.id = a.id_rantaipangan
        left join survei_data c on c.jenis_pangan = a.id
        where c.jenis_data = 'distrisegar' and
        c.pasar = '".$pasar."' and c.is_delete = '0' $where
        group by a.id, c.lolos) a group by a.id
	";
    $rs = $this->db->query("$q");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'pengawasan/page/data/distrisegarkesimpulan';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("   select a.nama, sum(if(a.lolos = '1', a.jml, 0)) as 'lolos', sum(if(a.lolos = '0', a.jml, 0)) as 'tidak' from (select a.id, a.nama, count(a.id) as jml, c.lolos  from ref_jenispangan a inner join
        (select id from ref_rantaipangan where `group` = 'pangan segar') b on b.id = a.id_rantaipangan
        left join survei_data c on c.jenis_pangan = a.id
        where c.jenis_data = 'distrisegar' and
        c.pasar = '".$pasar."' and c.is_delete = '0'
		 $where
          group by a.id, c.lolos) a group by a.id
		ORDER BY a.id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th align="center">#</th>
        <th>JENIS PANGAN</th>
        <th>JUMLAH</th>
        <th>TIDAK MEMENUHI SYARAT</th>
        <th>MEMENUHI SYARAT</th>
        <th>KESIMPULAN</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;

	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->nama?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->lolos+$item->tidak?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->tidak?></div>
        </td>
		<td>
            <div class="text-left"><?=$item->lolos?></div>
        </td>
        <td>
            <div class="text-left">
                <div>TMS = <?=$item->tidak?>/<?=$item->lolos+$item->tidak?> = <?=$item->tidak/($item->lolos+$item->tidak)*100?>%</div>
                <div>TS = <?=$item->lolos?>/<?=$item->lolos+$item->tidak?> = <?=$item->lolos/($item->lolos+$item->tidak)*100?>%</div>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>