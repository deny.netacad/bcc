<?php 
$route['(auth)/manbidang'] 	= "page/load/manbidang";
$route['(auth)/manmenu'] 	= "page/load/manmenu";
$route['(auth)/manuser'] 	= "page/load/manuser";
$route['(auth)/manmodul'] 	= "page/load/manmodul";
$route['(auth)/manhakakses'] 	= "page/load/manhakakses";
$route['(auth)/mansize']	= "page/load/mansize";
$route['(auth)/manbrand']	= "page/load/manbrand";

$route['(auth)/manuptd']	= "page/load/manuptd";
$route['(auth)/manpasar']	= "page/load/manpasar";
$route['(auth)/manpasarmodern']	= "page/load/manpasarmodern";
$route['(auth)/mankantin']	= "page/load/mankantin";
$route['(auth)/mankelurahan']	= "page/load/mankelurahan";
$route['(auth)/mancolor']	= "page/load/mancolor";
$route['(auth)/mankategori']	= "page/load/mankategori";

$route['(auth)/calonseller']	= "page/load/calonseller";
$route['(auth)/balasmail']	= "page/load/balasmail";

$route['(auth)/videogallery']	= "page/load/videogallery";
$route['(auth)/inpengaduan']	= "page/load/inpengaduan";


// DISNAKER BOGOR 
$route['(bcc)/manlowongankerja']			= "page/load/manlowongankerja";
$route['(bcc)/manevent']					= "page/load/manevent";
$route['(bcc)/manpenyedialowongan']		= "page/load/manpenyedialowongan";
$route['(bcc)/mantipepenyedialowongan']	= "page/load/mantipepenyedialowongan";
$route['(bcc)/manpostinglowongan']			= "page/load/manpostinglowongan";
$route['(bcc)/mantrackingpelamar']			= "page/load/mantrackingpelamar";
$route['(bcc)/mankelolapelamar']			= "page/load/mankelolapelamar";
$route['(bcc)/lappelamar']					= "page/load/lappelamar";
$route['(bcc)/laptrackevent']				= "page/load/laptrackevent";
$route['(bcc)/lappelamarevent']				= "page/load/lappelamarevent";
$route['(bcc)/lappenyedialowongan']		= "page/load/lappenyedialowongan";
$route['(bcc)/laplowongan']				= "page/load/laplowongan";
$route['(bcc)/laptracklowongan']			= "page/load/laptracklowongan";
$route['(bcc)/userpelamar']				= "page/load/userpelamar";
$route['(bcc)/userpenyedia_data']				= "page/load/userpenyedia_data";
$route['(bcc)/mankategorilowongan']				= "page/load/mankategorilowongan";
$route['(bcc)/cetakak1']				= "page/load/cetakak1";
$route['(bcc)/cetakak1_depan']				= "page/load/cetakak1_depan";
$route['(bcc)/manpenandatanganan']				= "page/load/manpenandatanganan";
$route['(bcc)/seo']				= "page/load/seo";






