<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {
	var $modules = "main";
	
	function __construct()
	{
		parent::__construct();
		
		
	}

	function index(){
		if ($this->session->userdata('is_login_portal_disnakerbogor'))
		{
			if($this->cekRole()==false){
				redirect('auth');
			}else{
				$this->load->view($this->modules.'/main',$this->data);
			}
		}
		else
		{
			$this->data['login_attempt'] =$this->session->userdata('login_attempt');
			$this->session->unset_userdata('login_attempt');
			$this->load->view('login',$this->data);
		}
	}
	function dashboard_json_(){
		// transaksi dan jumlah penumpang hari ini
		$rs=$this->db->query("
		SELECT FORMAT(SUM(a.kredit),0) AS amount,FORMAT(SUM(1),0) AS trx FROM transaksibis a
		INNER JOIN databis b ON a.Bis=b.Bis
		LEFT JOIN ref_koridor c ON b.koridor=c.koridor
		WHERE a.Tanggal=CURDATE()");
		$ret['trx'] = $rs->result();
		echo json_encode($ret);
	}


	function dashboard_json(){
		// transaksi dan jumlah penumpang hari ini
		$rs=$this->db->query("
					select format((a.amount+b.amount),0)  as amount,format(a.trx_1,0) as trx_1,format(b.trx_2,0) as trx_2,format((a.trx_1+b.trx_2),0) as trx_total,format(((a.trx_1+b.trx_2)/b.trx_2),2) as trx_3 from (
		SELECT a.Tanggal ,SUM(a.kredit) AS amount,SUM(1) AS trx_1 FROM transaksibis a
		INNER JOIN databis b ON a.Bis=b.Bis
		inner join tarif d on a.Jenis=d.`Jenis`
		where d.`is_cashless`=0 and a.Tanggal=CURDATE() ) a
		left join (
		SELECT a.Tanggal ,SUM(a.kredit) AS amount,SUM(1) AS trx_2 FROM transaksibis a
		INNER JOIN databis b ON a.Bis=b.Bis
		inner join tarif d on a.Jenis=d.`Jenis`
		where d.`is_cashless`=1 and a.Tanggal=CURDATE()
		) b on a.Tanggal=b.Tanggal");
		$ret['trx'] = $rs->result();
		echo json_encode($ret);
	}



	
	//========== new ===========//
	
	function exceltrxshelter(){
		//echo "halo";
		$this->load->view('exceltrxshelter');
	}
}