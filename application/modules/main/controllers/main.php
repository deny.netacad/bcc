<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {
	var $modules = "main";

	public function index()
	{
		if($this->session->userdata('is_login_portal_disnakerbogor')){
			$this->load->view($this->modules.'/main',$this->data);
		}else{
			redirect('/login');
		}
	}

	function keamananpangan(){
		$this->session->set_userdata('setmodule','pengawasan');
		$this->load->view($this->modules.'/main',$this->data);
	}

	function sertifikasi(){
		$this->session->set_userdata('setmodule','sertifikasi');
		$this->load->view($this->modules.'/main',$this->data);
	}


}
