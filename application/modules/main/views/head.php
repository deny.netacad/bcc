	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?=$def_ass?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=$def_ass?>css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=$def_ass?>css/ionicons.min.css">
	<link rel="stylesheet" href="<?=$def_ass?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="<?=$def_ass?>dist/css/AdminLTE.css">
	<link rel="stylesheet" href="<?=$def_ass?>dist/css/skins/_all-skins.min.css">

	<link href="<?=$def_css?>datepicker.css" rel="stylesheet">
	<link href="<?=$def_css?>datetimepicker.css" rel="stylesheet">
	<link href="<?=$def_js?>plugins/select2/select2.css" rel="stylesheet">
	<link href="<?=$def_css?>custom.css" rel="stylesheet">
	<link href="<?=$def_css?>icomoon/style.css" rel="stylesheet">
	<link href="<?=$def_js?>plugins/imgeffects/css/effects.min.css" rel="stylesheet">
	
	<!-- jQuery 2.1.4 -->
	<script src="<?=$def_ass?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="<?=$def_ass?>bootstrap/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="<?=$def_ass?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="<?=$def_ass?>plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?=$def_ass?>dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?=$def_ass?>dist/js/demo.js"></script>
	
	<script type="text/javascript" src="<?=$def_js?>plugins/select2/select2.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/debounce/jquery-debounce.min.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<!--<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/highcharts.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/highcharts/modules/exporting.js"></script>-->
	<script type="text/javascript" src="<?=$def_js?>plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="<?=$def_js?>plugins/jpodtable/jquery.jpodtable.js"></script>
	<script src="<?=$def_js?>holder.js"></script>
	<script src="<?=$def_js?>enterform.js"></script>
	<script src="<?=$def_js?>plugins/isloading/jquery.isloading.min.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->