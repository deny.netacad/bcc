<?php
$thn = $this->input->post('thn');
$rs = $this->db->query("
SELECT a.thn, a.nama
,SUM(IF(a.bln=1,a.visitor,0)) AS jan
,SUM(IF(a.bln=2,a.visitor,0)) AS feb
,SUM(IF(a.bln=3,a.visitor,0)) AS mar
,SUM(IF(a.bln=4,a.visitor,0)) AS apr
,SUM(IF(a.bln=5,a.visitor,0)) AS mei
,SUM(IF(a.bln=6,a.visitor,0)) AS jun
,SUM(IF(a.bln=7,a.visitor,0)) AS jul
,SUM(IF(a.bln=8,a.visitor,0)) AS agu
,SUM(IF(a.bln=9,a.visitor,0)) AS sep
,SUM(IF(a.bln=10,a.visitor,0)) AS okt
,SUM(IF(a.bln=11,a.visitor,0)) AS nov
,SUM(IF(a.bln=12,a.visitor,0)) AS des
FROM
(SELECT 'Visitor' as nama, YEAR(a.log) AS thn,MONTH(a.log) AS bln, 1 as visitor FROM ngi_webvisitor a WHERE YEAR(a.log)='".$thn."'
GROUP BY a.ip, YEAR(a.log), MONTH(a.log) ) a
group by a.thn

");
$data = '';
foreach($rs->result() as $item){
	$data.= '{	name: \''.$item->nama.'\',	data: ['.$item->jan.', '.$item->feb.', '.$item->mar.', '.$item->apr.', '.$item->mei.', '.$item->jun.', '.$item->jul.', '.$item->agu.', '.$item->sep.', '.$item->okt.', '.$item->nov.', '.$item->des.'] },';
}
?>

<script>
$(document).ready(function(){
	Highcharts.setOptions({
		lang: {
		  decimalPoint: '.',
		  thousandsSep: ','
		}
	});

	Highcharts.chart('container1', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Source: disnaker kab.bogor'
		},
		xAxis: {
			categories: [
				'Jan',
				'Feb',
				'Mar',
				'Apr',
				'May',
				'Jun',
				'Jul',
				'Aug',
				'Sep',
				'Oct',
				'Nov',
				'Dec'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah Pengunjung Web (Orang)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [ <?=substr($data,0,-1)?>]
	});
});
</script>
<div id="container1"></div>