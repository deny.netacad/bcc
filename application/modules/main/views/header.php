<header class="main-header">

	<!-- Logo -->
	 <a href="<?=base_url()?>main" class="logo" style="font-family:verdana">
		<div class="profile-img">
			<img src="<?=$def_img?>logo_small.png" alt="">
		</div>
	  </a>

	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
	  <!-- Sidebar toggle button-->
	  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
	  </a>
	  <!-- Navbar Right Menu -->
	  <div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
		  <li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  <img src="<?=$def_ass?>dist/img/avatar5.png" class="user-image" alt="User Image">
			  <span class="hidden-xs"><?=$this->session->userdata('nama')?></span>
			</a>
			<ul class="dropdown-menu">
			  <!-- User image -->
			  <li class="user-header">
				<img src="<?=$def_ass?>dist/img/avatar5.png" class="img-circle" alt="User Image">
				<p>
				  <?=$this->session->userdata('username')?>
				  <small><?=$this->session->userdata('nama')?></small>
				</p>
			  </li>
			  <li class="user-footer">
				<div class="pull-left">
				  <a href="<?=base_url()."passwd/gantipass"?>" class="btn btn-default btn-flat">Ganti Password</a>
				</div>
				<div class="pull-right">
				  <a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
				</div>
			  </li>
			</ul>
		  </li>
		  <!-- Control Sidebar Toggle Button -->
		  <li>
			<!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->.
		  </li>
		</ul>
	  </div>

	</nav>
</header>
  
  