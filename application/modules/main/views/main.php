<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?=$site_title?></title>
	<link rel="icon" type="image/png" href="<?=$def_ass?>dist/img/iconngosis.png"/>
	<?php
	$this->load->view('main/head');
	?>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

	<?php
	$this->load->view('main/header');
	?>
	<?php
	$this->load->view('main/sidebar');
	?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<?php
	#$session = unserialize($_COOKIE['ci_session']);
	$this->load->view('main/default');
	?>
	</div>
	<?php
	$this->load->view('main/footer');
	?>
</div>
</body>
</html>
