<footer class="main-footer">
	<div class="pull-right hidden-xs">
	  <b>Version</b> 1.0.2
	</div>
	<strong>Copyright &copy; <?php echo date("Y") ?> <a href="http://indoaksesmedia.co.id">Indo Akses Media</a>.</strong> All rights reserved.
</footer>