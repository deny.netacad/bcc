<?php
$thn = $this->input->post('thn');
$rs = $this->db->query("
SELECT a.thn,jk
,SUM(IF(a.bln=1,a.jmlpelamarnya,0)) AS jan
,SUM(IF(a.bln=2,a.jmlpelamarnya,0)) AS feb
,SUM(IF(a.bln=3,a.jmlpelamarnya,0)) AS mar
,SUM(IF(a.bln=4,a.jmlpelamarnya,0)) AS apr
,SUM(IF(a.bln=5,a.jmlpelamarnya,0)) AS mei
,SUM(IF(a.bln=6,a.jmlpelamarnya,0)) AS jun
,SUM(IF(a.bln=7,a.jmlpelamarnya,0)) AS jul
,SUM(IF(a.bln=8,a.jmlpelamarnya,0)) AS agu
,SUM(IF(a.bln=9,a.jmlpelamarnya,0)) AS sep
,SUM(IF(a.bln=10,a.jmlpelamarnya,0)) AS okt
,SUM(IF(a.bln=11,a.jmlpelamarnya,0)) AS nov
,SUM(IF(a.bln=12,a.jmlpelamarnya,0)) AS des
FROM
(SELECT a.*,b.gender AS jk,YEAR(a.log) AS thn,MONTH(a.log) AS bln,COUNT(a.id_pelamar) AS jmlpelamarnya FROM ngi_jobapplied a
LEFT JOIN user_pelamar b ON a.id_pelamar=b.iduser
WHERE YEAR(a.log)='".$thn."'
GROUP BY b.gender,YEAR(a.log),MONTH(a.log)
ORDER BY b.gender
)a
GROUP BY jk
");
$data = '';
foreach($rs->result() as $item){
	$data.= '{	name: \''.$item->jk.'\',data: ['.$item->jan.', '.$item->feb.', '.$item->mar.', '.$item->apr.', '.$item->mei.', '.$item->jun.', '.$item->jul.', '.$item->agu.', '.$item->sep.', '.$item->okt.', '.$item->nov.', '.$item->des.'] },';
}
?>

<script>
$(document).ready(function(){
	Highcharts.setOptions({
		lang: {
		  decimalPoint: '.',
		  thousandsSep: ','
		}
	});

	Highcharts.chart('container3', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Source: disnaker kab.bogor'
		},
		xAxis: {
			categories: [
				'Jan',
				'Feb',
				'Mar',
				'Apr',
				'May',
				'Jun',
				'Jul',
				'Aug',
				'Sep',
				'Oct',
				'Nov',
				'Dec'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah Peminat (Orang)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [ <?=substr($data,0,-1)?>]
	});
});
</script>
<div id="container3"></div>

