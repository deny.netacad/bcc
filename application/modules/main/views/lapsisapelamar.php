<?php
$rs = $this->db->query("
select a.tahun,
sum(if(a.bulan<=1 and month(current_date) >= 1,a.jml,0)) as 'jan',
sum(if(a.bulan<=2 and month(current_date) >= 2,a.jml,0)) as 'feb',
sum(if(a.bulan<=3 and month(current_date) >= 3,a.jml,0)) as 'mar',
sum(if(a.bulan<=4 and month(current_date) >= 4,a.jml,0)) as 'apr',
sum(if(a.bulan<=5 and month(current_date) >= 5,a.jml,0)) as 'mei',
sum(if(a.bulan<=6 and month(current_date) >= 6,a.jml,0)) as 'jun',
sum(if(a.bulan<=7 and month(current_date) >= 7,a.jml,0)) as 'jul',
sum(if(a.bulan<=8 and month(current_date) >= 8,a.jml,0)) as 'agu',
sum(if(a.bulan<=9 and month(current_date) >= 9,a.jml,0)) as 'sep',
sum(if(a.bulan<=10 and month(current_date) >= 10,a.jml,0)) as 'okt',
sum(if(a.bulan<=11 and month(current_date) >= 11,a.jml,0)) as 'nov',
sum(if(a.bulan<=12 and month(current_date) >= 12,a.jml,0)) as 'des' from
(select 'diterima' as 'status', count(*) as jml, month(a.log) as bulan, year(a.log) as tahun from (select * from user_pelamar a
inner join ngi_jobapplied b on a.iduser = b.id_pelamar
where status = 1
group by a.iduser) a
where a.log < date_add(current_date, interval 1 day)
group by Month(a.log), year(a.log)
order by Month(a.log)) a
group by a.tahun
");

$jml = $this->db->query(" select * from user_pelamar")->num_rows();
$jan = 0; $jul = 0;
$feb = 0; $agu = 0;
$mar = 0; $sep = 0;
$apr = 0; $okt = 0;
$mei = 0; $nov = 0;
$jun = 0; $des = 0;
$sum = 0; $bln = date('m');

foreach($rs->result() as $item){
	if($item->tahun == date('Y')){
		$jan = $sum + $item->jan;
		$feb = $sum + $item->feb;
		$mar = $sum + $item->mar;
		$apr = $sum + $item->apr;
		$mei = $sum + $item->mei;
		$jun = $sum + $item->jun;
		$jul = $sum + $item->jul;
		$agu = $sum + $item->agu;
		$sep = $sum + $item->sep;
		$okt = $sum + $item->okt;
		$nov = $sum + $item->nov;
		$des = $sum + $item->des;
	}else{
		$sum += $item->des;
	}
}

$data = '{	name: "User diterima",	data: ['.(($bln >= 1) ? $jan : 0).', '.(($bln >= 2) ? $feb : 0).', '.(($bln >= 3) ? $mar : 0).', '.(($bln >= 4) ? $apr : 0).', '.(($bln >= 5) ? $mei : 0).', '.(($bln >= 6) ? $jun : 0).', '.(($bln >= 7) ? $jul : 0).', '.(($bln >= 8) ? $agu : 0).', '.(($bln >= 9) ? $sep : 0).', '.(($bln >= 10) ? $okt : 0).', '.(($bln >= 11) ? $nov : 0).', '.(($bln >= 12) ? $des : 0).'] },';
$data .= '{	name: "Sisa user",	data: ['.(($bln >= 1) ? $jml-$jan : 0).', '.(($bln >= 2) ? $jml-$feb : 0).', '.(($bln >= 3) ? $jml-$mar : 0).', '.(($bln >= 4) ? $jml-$apr : 0).', '.(($bln >= 5) ? $jml-$mei : 0).', '.(($bln >= 6) ? $jml-$jun : 0).', '.(($bln >= 7) ? $jml-$jul : 0).', '.(($bln >= 8) ? $jml-$agu : 0).', '.(($bln >= 9) ? $jml-$sep : 0).', '.(($bln >= 10) ? $jml-$okt : 0).', '.(($bln >= 11) ? $jml-$nov : 0).', '.(($bln >= 12) ? $jml-$des : 0).'] },';
?>

<script>
$(document).ready(function(){
	Highcharts.setOptions({
		lang: {
		  decimalPoint: '.',
		  thousandsSep: ','
		}
	});

	Highcharts.chart('containersisa', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Source: disnaker kab.bogor'
		},
		xAxis: {
			categories: [
				'Jan',
				'Feb',
				'Mar',
				'Apr',
				'May',
				'Jun',
				'Jul',
				'Aug',
				'Sep',
				'Oct',
				'Nov',
				'Dec'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah Pengunjung Web (Orang)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [ <?=substr($data,0,-1)?>]
	});
});
</script>
<div id="containersisa"></div>