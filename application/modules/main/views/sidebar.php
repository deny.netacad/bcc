	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
	  <!-- Sidebar user panel -->
	  <div class="user-panel">
		<div class="pull-left image">
		  <img src="<?=$def_ass?>dist/img/avatar5.png" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
		  <p><?=$this->session->userdata('nama')?></p>
		  <a href="<?=base_url()?>"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	  </div>
	  <!-- sidebar menu: : style can be found in sidebar.less -->
	  <ul class="sidebar-menu">
			<li class="treeview"><a href="<?=base_url()?>main" tabindex="-1"><i class="text-orange fa fa-home"></i><span>DASHBOARD</span></a></li>
			<?php
				$this->load->view('main/menu');
			?>
		</ul>
	</section>
	<!-- /.sidebar -->
	</aside>