<!-- Content Header (Page header) -->
	<section class="content-header">
  <h1>
	DASHBOARD
	<small><?=$this->session->userdata('bidang')?></small>
  </h1>
</section>

<section class="content">
<div class="row">
  <div class="col-md-12">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Pengunjung Bogor Career Center Tahun <?=date('Y')?></h3>
        
          <input type="hidden" id="thn" name="thn" class='yr' value="<?=date('Y')?>"/> <span id="wait"></span>
      
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
        <div class="box-body">
              <div class="chart">
        <div class="row"></div>
          <div class="col-md-12" id="visitor">
          </div>
        </div>
        </div>
        </div>
        
            <!-- /.box-body -->
          <!-- </div> -->
     </div>
     
     <div class="col-md-12">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Pengunjung Bogor Career Center Bulan <?=date('F')?> <?=date('Y')?></h3>
      
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
        <div class="box-body">
              <div class="chart">
        <div class="row">
          <div class="col-md-12" id="visitors">
          </div>
        </div>
        </div>
        </div>
        
            <!-- /.box-body -->
          </div>
     </div>

     <div class="col-md-12">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Pelamar Yang Belum Diterima Tahun <?=date('Y')?></h3>
      
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
        <div class="box-body">
              <div class="chart">
        <div class="row">
          <div class="col-md-12" id="pelamarsisa">
          </div>
        </div>
        </div>
        </div>
        
            <!-- /.box-body -->
          </div>
     </div>

<div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Trending Lowongan </h3>
				
					<!-- <input type="hidden" id="thn" name="thn" class='yr' value="<?=date('Y')?>"/> <span id="wait"></span> -->
			
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
			  <div class="box-body">
              <div class="chart">
				<div class="row">
					<div class="col-md-12" id="result">
					</div>
				</div>
				</div>
				</div>
				
            <!-- /.box-body -->
          </div>
		 </div>
		 <div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Pelamar Diterima dan Tidak</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
			  <div class="box-body">
              <div class="chart">
				<div class="row">
					<div class="col-md-12" id="resultpelamar">
					</div>
				</div>
				</div>
				</div>
				
            <!-- /.box-body -->
          </div>
		 </div>
		 
		 
		 <div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Jenis Pendaftar Sistem</h3>
				
					<!-- <input type="hidden" id="thn" name="thn" class='yr' value="<?=date('Y')?>"/> <span id="wait"></span> -->
			
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
			  <div class="box-body">
              <div class="chart">
				<div class="row">
					<div class="col-md-12" id="jenispelamar">
					</div>
				</div>
				</div>
				</div>
				
            <!-- /.box-body -->
          </div>
		 </div>

		  <div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Jenis Pendaftar Lamaran</h3>
				
					<!-- <input type="hidden" id="thn" name="thn" class='yr' value="<?=date('Y')?>"/> <span id="wait"></span> -->
			
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
			  <div class="box-body">
              <div class="chart">
				<div class="row">
					<div class="col-md-12" id="jenispelamargender">
					</div>
				</div>
				</div>
				</div>
				
            <!-- /.box-body -->
          </div>
		 </div>
		 
</div>

</section>

<script>

$(document).ready(function(){
  loadGraphVisitors();
  loadGraphVisitorsMonth();
	loadGraphPelamar();
	loadGraphByJenis();
	loadjenispelamar();
	loadjenispelamargender();
  loadpelamarsisa();
});

function loadGraphVisitors(){
  $.ajax({
    type:'post',
    url:'<?=base_url()?>main/page/view/visitor',
    data:{ 'thn':$('#thn').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
    beforeSend:function(){
      $('#spinner').loading('stop');
      $('#spinner').loading();
    },
    success:function(response){
      $('#spinner').loading('stop');
      $('#visitor').html(response);
    }
  });
}

function loadGraphVisitorsMonth(){
  $.ajax({
    type:'post',
    url:'<?=base_url()?>main/page/view/visitor_day',
    data:{ '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
    beforeSend:function(){
      $('#spinner').loading('stop');
      $('#spinner').loading();
    },
    success:function(response){
      $('#spinner').loading('stop');
      $('#visitors').html(response);
    }
  });
}

function loadGraphByJenis(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>main/page/view/resumetrend',
		data:{ 'thn':$('#thn').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#result').html(response);
		}
	});
}

function loadGraphPelamar(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>main/page/view/lappelamar',
		data:{ 'thn':$('#thn').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#resultpelamar').html(response);
		}
	});
}

function loadjenispelamar(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>main/page/view/jenis_pelamar',
		data:{ 'thn':$('#thn').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#jenispelamar').html(response);
		}
	});
}
function loadjenispelamargender(){
	$.ajax({
		type:'post',
		url:'<?=base_url()?>main/page/view/jenis_pelamar_gender',
		data:{ 'thn':$('#thn').val(),'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
		beforeSend:function(){
			$('#spinner').loading('stop');
			$('#spinner').loading();
		},
		success:function(response){
			$('#spinner').loading('stop');
			$('#jenispelamargender').html(response);
		}
	});
}

function loadpelamarsisa(){
  $.ajax({
    type:'post',
    url:'<?=base_url()?>main/page/view/lapsisapelamar',
    data:{ '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>' },
    beforeSend:function(){
      $('#spinner').loading('stop');
      $('#spinner').loading();
    },
    success:function(response){
      $('#spinner').loading('stop');
      $('#pelamarsisa').html(response);
    }
  });
}



</script>		  
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>