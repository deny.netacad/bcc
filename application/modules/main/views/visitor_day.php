<?php
$thn = date('Y');
$bln = date('m');

$rs = $this->db->query("SELECT a.thn, a.nama
,SUM(IF(a.hari=1,a.visitor,0)) AS jan
,SUM(IF(a.hari=2,a.visitor,0)) AS feb
,SUM(IF(a.hari=3,a.visitor,0)) AS mar
,SUM(IF(a.hari=4,a.visitor,0)) AS apr
,SUM(IF(a.hari=5,a.visitor,0)) AS mei
,SUM(IF(a.hari=6,a.visitor,0)) AS jun
,SUM(IF(a.hari=7,a.visitor,0)) AS jul
,SUM(IF(a.hari=8,a.visitor,0)) AS agu
,SUM(IF(a.hari=9,a.visitor,0)) AS sep
,SUM(IF(a.hari=10,a.visitor,0)) AS okt
,SUM(IF(a.hari=11,a.visitor,0)) AS nov
,SUM(IF(a.hari=12,a.visitor,0)) AS des
,SUM(IF(a.hari=13,a.visitor,0)) AS jann
,SUM(IF(a.hari=14,a.visitor,0)) AS febb
,SUM(IF(a.hari=15,a.visitor,0)) AS marr
,SUM(IF(a.hari=16,a.visitor,0)) AS aprr
,SUM(IF(a.hari=17,a.visitor,0)) AS meii
,SUM(IF(a.hari=18,a.visitor,0)) AS junn
,SUM(IF(a.hari=19,a.visitor,0)) AS jull
,SUM(IF(a.hari=20,a.visitor,0)) AS aguu
,SUM(IF(a.hari=21,a.visitor,0)) AS sepp
,SUM(IF(a.hari=22,a.visitor,0)) AS oktt
,SUM(IF(a.hari=23,a.visitor,0)) AS novv
,SUM(IF(a.hari=24,a.visitor,0)) AS dess
,SUM(IF(a.hari=25,a.visitor,0)) AS jannn
,SUM(IF(a.hari=26,a.visitor,0)) AS febbb
,SUM(IF(a.hari=27,a.visitor,0)) AS marrr
,SUM(IF(a.hari=28,a.visitor,0)) AS aprrr
,SUM(IF(a.hari=29,a.visitor,0)) AS meiii
,SUM(IF(a.hari=30,a.visitor,0)) AS junnn
,SUM(IF(a.hari=31,a.visitor,0)) AS julll
FROM
(SELECT 'Visitor' as nama, YEAR(a.log) AS thn,MONTH(a.log) AS bln, DAY(a.log) as hari, 1 as visitor FROM ngi_webvisitor a 
	WHERE YEAR(a.log)='".$thn."' and MONTH(a.log) = '".$bln."'
GROUP BY a.ip, DAY(a.log))	 a
group by a.bln");

$data = '';
foreach($rs->result() as $item){
	$data.= '{	name: \''.$item->nama.'\',	data: ['.$item->jan.', '.$item->feb.', '.$item->mar.', '.$item->apr.', '.$item->mei.', '.$item->jun.', '.$item->jul.', '.$item->agu.', '.$item->sep.', '.$item->okt.', '.$item->nov.', '.$item->des.', '.$item->jann.', '.$item->febb.', '.$item->marr.', '.$item->aprr.', '.$item->meii.', '.$item->junn.', '.$item->jull.', '.$item->aguu.', '.$item->sepp.', '.$item->oktt.', '.$item->novv.', '.$item->dess.', '.$item->jannn.', '.$item->febbb.', '.$item->marrr.', '.$item->aprrr.', '.$item->meiii.', '.$item->junnn.', '.$item->julll.'] },';
}
?>

<script>
$(document).ready(function(){
	Highcharts.setOptions({
		lang: {
		  decimalPoint: '.',
		  thousandsSep: ','
		}
	});

	Highcharts.chart('container6', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Source: disnaker kab.bogor'
		},
		xAxis: {
			categories: [
				'01',
				'02',
				'03',
				'04',
				'05',
				'06',
				'07',
				'08',
				'09',
				'10',
				'11',
				'12',
				'13',
				'14',
				'15',
				'16',
				'17',
				'18',
				'19',
				'20',
				'21',
				'22',
				'23',
				'24',
				'25',
				'26',
				'27',
				'28',
				'29',
				'30',
				'31'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah Pengunjung Web (Orang)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [ <?=substr($data,0,-1)?>]
	});
});
</script>
<div id="container6"></div>