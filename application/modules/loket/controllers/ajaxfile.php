<?php

class Ajaxfile extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}



	function caribidang(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where ( bidang like '%".$keyword."%' )";
		$q = "select * from mast_bidang $where";
		$rs = $this->db->query($q);

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}
	function carimasterkepokmas(){
		$keyword 	= $this->input->post('keyword');

		$per_page 	= intval($this->input->post('per_page'));
		$start		= (intval($this->input->post('page'))-1)*$per_page;
		$page		= intval($this->input->post('page'));
		$where = " where ( title like '%".$keyword."%' )";
		$q = "select * from section_kepokmas $where";
		$rs = $this->db->query($q);

		$arr['result'] 		= $rs->num_rows();
		$arr['per_page'] 	= $per_page;
		$arr['page'] 		= (($page>0)?$page:1);
		$rs = $this->db->query("$q limit $start,$per_page");
		$arr['rows'] = $rs->result();
		echo $_GET['callback']." (".json_encode($arr).")";
	}

	public function kuotaHarian()
	{ $hasil = $this->db->query('
			SELECT id
			FROM h_antrian
			WHERE dtm_register LIKE "'.date('Y-m-d').'%"
		')->num_rows();

		$kuota = $this->db->query('
			SELECT kuota
			FROM r_kuota_harian
			WHERE tanggal = "'.date('Y-m-d').'"
		')->row();

		$default = $this->db->query('
			SELECT value
			FROM r_pengaturan
			WHERE parameter = "kuota_default"
		')->row();

		$result = 0;
		if(!empty($kuota)) $result = strval($kuota->kuota) - $hasil;
		else $result = strval($default->value) - $hasil;

		header('Content-Type: application/json');
		echo json_encode($result);
	}
	public function hitungTunggu()
	{ $hasil = $this->db->query('
			SELECT LPAD(COUNT(id),4,0) as jumlah FROM h_antrian
			WHERE mast_user_id = 0 AND panggil = "belum" AND dtm_register LIKE "'.date('Y-m-d').'%"
		')->row();

		header('Content-Type: application/json');
		echo json_encode($hasil);
	}
	public function cekPanggilan()
	{ $hasil = $this->db->query('
			SELECT * FROM h_antrian
			WHERE
				mast_user_id > 0
				AND panggil = "ulang"
				AND dtm_register LIKE "'.date('Y-m-d').'%"
			ORDER BY id ASC
		')->result();

		$this->db->query('
			UPDATE h_antrian
			SET panggil = "sudah"
			WHERE mast_user_id > 0 AND panggil = "ulang"
		');

		header('Content-Type: application/json');
		echo json_encode($hasil);
	}
	public function panggilNext()
	{	$loket = $this->input->post('loket');
		$mast_user_id = $this->session->userdata['iduser'];

		$getKosong = $this->db->query('
			SELECT * FROM h_antrian
			WHERE
				mast_user_id = 0
				AND panggil = "belum"
				AND dtm_register LIKE "'.date('Y-m-d').'%"
			ORDER BY id ASC
		')->row();

		$this->db->query('
			UPDATE h_antrian
			SET dtm_panggil = "'.date('Y-m-d H:i:s').'",
			mast_user_id = "'.$mast_user_id.'",
			loket = "'.$loket.'",
			panggil = "ulang"
			WHERE id = "'.$getKosong->id.'"
		');

		header('Content-Type: application/json');
		echo json_encode($getKosong);
	}
	public function panggilUlang()
	{	$id = $this->input->post('id');

		$this->db->query('
			UPDATE h_antrian
			SET dtm_panggil = "'.date('Y-m-d H:i:s').'",
			panggil = "ulang"
			WHERE id = "'.$id.'"
		');
	}
	public function selesaiAntrian()
	{	$input = $this->input->post();
		$this->db->query('
			UPDATE h_antrian
			SET dtm_selesai = "'.date('Y-m-d H:i:s').'",
			keterangan = "'.$input['keterangan'].'",
			panggil = "sudah"
			WHERE id = "'.$input['id'].'"
		');
	}
	public function sedangDilayani()
	{
		$query = $this->db->query('
			SELECT * FROM h_antrian
			WHERE id IN (
				SELECT MAX(id) as id
				FROM h_antrian
				WHERE mast_user_id > 0
				AND dtm_register LIKE "'.date('Y-m-d').'%"
				AND keterangan IS NULL
				GROUP BY loket
			)
			ORDER BY loket ASC
		')->result();

		header('Content-Type: application/json');
		echo json_encode($query);
	}

	public function saveKuotaHarian()
	{	$input = $this->input->post();

		$date1 = new DateTime($input['tanggal_start']);
		$date2 = new DateTime($input['tanggal_end']);
		$interval = $date1->diff($date2);

		if($interval->d == 1){
			$this->db->query("
				DELETE FROM r_kuota_harian
				WHERE tanggal = '".$input['tanggal_start']."'
			");
			if($input['kuota_harian'] != 0){
				$this->db->query("
					INSERT INTO r_kuota_harian
					VALUES(
						null,
						'".$input['tanggal_start']."',
						".$input['kuota_harian']."
					)
				");
			}
		} else {
			for ($i=0; $i < $interval->d; $i++) {
				$this->db->query("
					DELETE FROM r_kuota_harian
					WHERE tanggal = '".date('Y-m-d', strtotime($input['tanggal_start'].' + '.$i.' days'))."'
				");
				if($input['kuota_harian'] != 0){
					$this->db->query("
						INSERT INTO r_kuota_harian
						VALUES(
							null,
							'".date('Y-m-d', strtotime($input['tanggal_start'].' + '.$i.' days'))."',
							".$input['kuota_harian']."
							)
					");
				}
			}
		}
	}

	public function saveKategori()
	{
		$input = $this->input->post();
		$test = $this->db->query("
			SELECT *
			FROM r_layanan
			WHERE kode_antrian = '".$input['kode_antrian']."'
		")->num_rows();



		if($test && empty($input['id'])) echo "0";
		else {
			if(empty($input['id']))
			{	unset($input['id']);
				$this->db->insert('r_layanan', $input);
			} else $this->db->replace('r_layanan', $input);
			echo "1";
		}
	}


}
?>
