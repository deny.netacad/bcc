<?php
$route['(loket)/apps'] 		= "page/load/apps";
$route['(loket)/manmenu'] 		= "page/load/manmenu";
$route['(loket)/kontenumum'] 	= "page/load/kontenumum";
$route['(loket)/aboutus'] 		= "page/load/aboutus";
$route['(loket)/package'] 		= "page/load/package";
$route['(loket)/manproduk'] 		= "page/load/manproduk";
$route['(loket)/manslide'] 		= "page/load/manslide";
$route['(loket)/gallery'] 		= "page/load/gallery";
$route['(loket)/galleryupload'] 		= "page/load/galleryupload";
$route['(loket)/videogallery'] 	= "page/load/videogallery";
$route['(loket)/video'] 			= "page/load/video";
$route['(loket)/news'] 			= "page/load/news";
$route['(loket)/article'] 		= "page/load/article";
$route['(loket)/testimonial'] 	= "page/load/testimonial";
$route['(loket)/kontak'] 		= "page/load/kontak";
$route['(loket)/manvideo'] 		= "page/load/manvideo";
$route['(loket)/manuser'] 		= "page/load/manuser";
$route['(loket)/changepass'] 	= "page/load/changepass";
$route['(loket)/pejabat'] 		= "page/load/pejabat";
$route['(loket)/agenda'] 		= "page/load/agenda";
$route['(loket)/links'] 		= "page/load/links";
$route['(loket)/perencanaan'] 		= "page/load/perencanaan";

$route['(loket)/kepokmas'] 		= "page/load/kepokmas";

$route['(loket)/mkepokmas'] 		= "page/load/mkepokmas";
$route['(loket)/jwbpertanyaan'] 		= "page/load/jwbpertanyaan";
$route['(loket)/agendadinas'] 		= "page/load/agendadinas";
$route['(loket)/konten_menu'] 		= "page/load/konten_menu";
$route['(loket)/seo'] 		= "page/load/seo";


$route['(loket)/antrian'] 		= "page/load/antrian";
$route['(loket)/kuota'] 		= "page/load/kuota";
$route['(loket)/kategori'] 		= "page/load/kategori";
