<br>


<style type="text/css">
#thumbwrap {
	/*margin:75px auto;
	width:252px; height:252px;*/
}
.thumb {
	float:left; /* must be floated for same cross browser position of larger image */
	position:relative;
	margin:3px;
}
.thumb img { 
	border:1px solid #000;
	vertical-align:bottom;
}
.thumb:hover {
	border:0; /* IE6 needs this to show large image */
	z-index:1;
}
.thumb span { 
	position:absolute;
	visibility:hidden;
}
.thumb:hover span { 
	visibility:visible;
	top:37px; left:37px; 
}
		

</style>

<table class="table table-hover">
	<colgroup>
		<col style="width:4%" />

	</colgroup>
	<thead>
		<tr>
			<th><div class="text-center"><input type="checkbox" name="checkall" id="checkall" value="1" /></div></th>
			<th><div class="text-left">NAMA</div></th>
			<th><div class="text-center">EMAIL</div><div class="text-center">Log</div></th>
			<th><div class="text-center">NIK</div><div class="text-center">IP AKSES</div></th>
		
			<th><div class="text-center">Send Mail Notification</div></th>

		</tr>
	</thead>
	<tbody>
		<div id="thumbwrap">
		<?php
		$rs = $this->db->query("select *,date_format(log,'%d %M , %Y %h:%i:%s') as log_pertanyaan from utin_tanya order by id desc") ;
	
		foreach($rs->result() as $item){ 
		
		

	
	
			
	?>
		<tr>
			<td>
				<div class="text-center"><input type="checkbox" name="<?=$n?>[checkthis]" value="1" /></div>
				<input type="hidden" name="<?=$n?>[id]" value="<?=$item->id?>" />
			</td>
			<td>
				<div class="text-center"><?=$item->nama?></div>
			</td>
			<td>
				<div class="text-center"><font color=green><?=$item->email?>
				<div class="text-center"><?=$item->log_pertanyaan?></div></font></div>
			</td>
			<td>
				<div class="text-center">NIK :<?=$item->ktp?></div><div class="text-center"><?=$item->ip?></div>
			</td>
			<td>
				<div class="text-center"><?=($item->jawaban=="")?'<font color=red>*Belum dibalas*</font>':'<font color=green>TERBALAS ('.$item->waktubalas.')</font>'?></div>
				<div class="text-center"><?=($item->flag==1)?'<a title="sendmail" data="'.$item->id.'"><button>BALAS VIA EMAIL </button></a>':''?></div>
			</td>
			
		</tr>
	
	<?php }?>
	
	</div>
		
	</tbody>
	<tfoot>
		<tr>
			<th colspan="5"><div class="text-right"></div></th>
			<th><div class="text-right"></div></th>
		</tr>
	</tfoot>
</table>