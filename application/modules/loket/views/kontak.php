<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script>
	
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}
	
	
   	
	function loadData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>operatorweb/page/loadKontak',
            data:{ 
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                
            },
            success:function(response){
                var ret = $.parseJSON(response);
				for(attrname in ret){
					if(attrname=='contact_id'){
						$('#id_edit').val(ret[attrname]);
					}else{
						$('#'+attrname).val(ret[attrname]);
					}
				}
            }
        });
    }

	
    $(document).ready(function(){
        
		var config_image = {
            toolbar : 'Image',
            height: '200',
            width: '100%'
        };
		
		var config_pengantar = {
            toolbar : 'MyToolbar',
            height: '350'
        };

	 //$('#narasi').ckeditor(config_pengantar);
	
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        loadData();
                    });
            }else{
                loadData();
            }
        });

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			$('#<?=$this->security->get_csrf_token_name()?>').val('<?=$this->security->get_csrf_hash()?>');
        });
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#title').focus();
				break;
			}
		});
		
        loadData();

    });

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Section - Kontak</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>operatorweb/page/saveKontak">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="addr" class="col-sm-2 control-label">Office</label>
						<div class="col-sm-10">
							<input type="text" id="office_name" name="office_name" class="form-control" placeholder="Nama Kantor" />
						</div>
					</div>
					<div class="form-group">
						<label for="bus_hour" class="col-sm-2 control-label">Business Hours</label>
						<div class="col-sm-10">
							<textarea id="addr" name="addr" class="form-control" placeholder="Alamat"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="maps" class="col-sm-2 control-label">Kota/Kabupaten</label>
						<div class="col-sm-10">
							<input type="text" id="city" name="city" class="form-control" placeholder="Kota/Kabupaten" />
						</div>
					</div>
					<div class="form-group">
						<label for="maps" class="col-sm-2 control-label">URl FB</label>
						<div class="col-sm-10">
							<input type="text" id="fb" name="fb" class="form-control" placeholder="url facebook" />
						</div>
					</div>
						<div class="form-group">
						<label for="maps" class="col-sm-2 control-label">URL Twitter</label>
						<div class="col-sm-10">
							<input type="text" id="twitter" name="twitter" class="form-control" placeholder="url twitter" />
						</div>
					</div>
					<div class="form-group">
						<label for="bus_hour" class="col-sm-2 control-label">URL Instagram</label>
						<div class="col-sm-10">
							<input id="instagram" name="instagram" class="form-control" placeholder="url instagram">
						</div>
					</div>
					<div class="form-group">
						<label for="state" class="col-sm-2 control-label">Provinsi</label>
						<div class="col-sm-10">
							<input type="text" id="state" name="state" class="form-control" placeholder="Provinsi" />
						</div>
					</div>
					<div class="form-group">
						<label for="phone" class="col-sm-2 control-label">Telepon</label>
						<div class="col-sm-10">
							<input type="text" id="phone" name="phone" class="form-control" placeholder="(0323) 9999999" />
						</div>
					</div>
					<div class="form-group">
						<label for="fax" class="col-sm-2 control-label">Fax</label>
						<div class="col-sm-10">
							<input type="text" id="fax" name="fax" class="form-control" placeholder="(0323) 9999999" />
						</div>
					</div>
					<div class="form-group">
						<label for="postcode" class="col-sm-2 control-label">Kode Pos</label>
						<div class="col-sm-10">
							<input type="text" id="postcode" name="postcode" class="form-control" placeholder="50000" />
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" id="email" name="email" class="form-control" placeholder="bappeda@sampangkab.go.id" />
						</div>
					</div>
					<!-- <div class="form-group">
						<label for="narasi" class="col-sm-2 control-label">Narasi Perusahaan</label>
						<div class="col-sm-10">
							<textarea id="narasi" name="narasi" placeholder=""></textarea>
						</div>
					</div> -->
					<div class="form-group">
						<label for="bus_hour" class="col-sm-2 control-label">Narasi</label>
						<div class="col-sm-10">
							<textarea id="narasi" name="narasi" class="form-control" placeholder="Narasi"></textarea>
						</div>
					</div>
					
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>