<script src="<?=$def_js?>sort.js"></script>
<script>
    $(document).ready(function(){
		$('table.sortable tbody').sortable({
			//alert('ok');
            update: function(event,ui){
                var bannerOrder_ = $(this).sortable('toArray').toString();
                $.ajax({
                    type: 'post',
                    url:'<?=base_url()?>operator/page/saveSortMenu',
                    data: { bannerOrder: bannerOrder_ },
                    success: function(){
                        $('#per_page').trigger('change');
                    }
                });
            }
        }).disableSelection();
    });
</script>
<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?10:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (a.title like '%".$cari."%' or b.title like '%".$cari."%') ";

    $n = intval($this->uri->segment(5));
    $q = "select * from menu a left join menu b on a.parent_id=b.id";
    $rs = $this->db->query("$q $where");
	$row = $rs->row_array();
    $totrows = $rs->num_rows();
	
    $config['base_url'] = base_url().'operator/page/data/manmenu';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;
    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,IF(a.parent_id>0,a.parent_id,a.id) AS idx,b.title as parent_title,
		(SELECT case a.jenurl when 1 then 'Konten Umum' when 2 then 'Link' else '' end) as jenurl
		FROM menu a left join menu b on a.parent_id=b.id $where
		ORDER BY a.menu_order,idx,a.parent_id limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
	
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate" style="padding:5px;">
    <span class="paginate_info" style="display:inline-block;margin-left:6px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover sortable">
    <colgroup>
        <col class="con0" style="width: 4%" />
    </colgroup>
    <thead>
    <tr>
        <th>#</th>
        <th>MENU</th>
        <th>PARENT</th>
        <th>JENIS URL</th>
        <th>STATUS</th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
        foreach($data['posts']->result() as $item){ $n++;
    ?>
    <tr id="<?=$item->menu_order."-".$item->id?>">
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->id?></span>
            <div class="text-left"><?=($item->parent_id==0)?$item->title:'<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$item->title.'</i>'?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->parent_title?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->jenurl?></div>
        </td>
        <td>
            <div class="text-left"><?=($item->flag==1)?'Publish':'Unpublish'?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->id?>"><i class="text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->id?>"><i class=" text-red fa fa-trash"></i></a>
                <?php if($item->parent_id == 0){ ?>
                    <i class="fa fa-arrows"></i>
                <?php } ?>
            </div>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>