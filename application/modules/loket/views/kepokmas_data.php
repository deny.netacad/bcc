<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (c.title like '%".$cari."%' 
	) ";

    $n = intval($this->uri->segment(5));
    $q = "SELECT a.*,b.nama AS nm_pasar,c.nama AS nm_user FROM section_hargakomoditas a
LEFT JOIN ref_pasar b ON a.pasar=b.id
LEFT JOIN mast_user c ON a.iduser=c.iduser
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operatorweb/page/data/kepokmas';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.nama AS nm_pasar,c.nama AS nm_user FROM section_hargakomoditas a
LEFT JOIN ref_pasar b ON a.pasar=b.id
LEFT JOIN mast_user c ON a.iduser=c.iduser
		 $where
		ORDER BY a.docplan_id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>PASAR</th>
        <th>TANGGAL KOMODITAS</th>
        <th>DETAIL</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <div class="text-left"><?=$item->nm_pasar?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->ppid_tgl?></div>
        </td>
        <td>
            <div class="text-left"><small></small></div>
        </td>
		<td>
            <div class="text-center"><?=$item->nm_user?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->docplan_id?>"><i  class=" text-green fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->docplan_id?>"><i class=" text-red fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>