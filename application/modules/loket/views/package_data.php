<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?10:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " where (b.title like '%".$cari."%' or a.content like '%".$cari."%'
	 or c.user_name like '%".$cari."%'
	) ";

    $n = intval($this->uri->segment(5));
    $q = "select * from dokumen a 
	inner join menu b on a.menu_id=b.id
	left join ref_user c on a.iduser=c.user_id
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'main/page/data/kontenumum';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.title as menu_title,b.parent_id,c.user_name
		FROM dokumen a
		inner join menu b on a.menu_id=b.id
		left join ref_user c on a.iduser=c.user_id
		 $where
		ORDER BY b.menu_order limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    <colgroup>
        <col class="con0" style="width:25px" />
		<col class="con0" style="width:200px" />
		<col class="con0" style="width:auto" />
		<col class="con0" style="width:100px" />
		<col class="con0" style="width:100px" />
		<col class="con0" style="width:100px" />
		<col class="con0" style="width:70px" />
    </colgroup>
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>MENU</th>
        <th>TITLE</th>
        <th>STATUS</th>
		<th>TIMESTAMP</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
        ?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->doc_id?></span>
            <div class="text-left"><?=($item->parent_id==0)?$item->menu_title:'<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$item->menu_title.'</i>'?></div>
        </td>
        <td>
            <div class="text-left"><?=$item->title?></div>
			<div class="text-left"><small><?=substr(strip_tags($item->content,''),0,200)?> ...</small></div>
        </td>
        <td>
            <div class="text-left"><?=($item->flag==1)?'Publish':'Unpublish'?></div>
        </td>
		<td>
            <div class="text-center"><?=$item->tmin?></div>
        </td>
		<td>
            <div class="text-center"><?=$item->user_name?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->doc_id?>"><i class="fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->doc_id?>"><i class="fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>