<script>
	function loadVideo(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>main/page/view/videoframe',
            data:{ 
				'video_name':$('#video_name').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                $("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
            },
            success:function(response){
                $('#result').html(response);
            }
        });
    }
	
    $(document).ready(function(){
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        $('#form1').trigger('reset');
                    });
            }else{
                $('#form1').trigger('reset');
            }
			$('#id_edit').val('');
        });

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			$('#id_edit').val('');
			loadVideo();
        });
		
		$('#video_name').on('blur',function(){
			loadVideo();
		});
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#video_name').focus();
				break;
			}
		});
		
		loadVideo();

    });

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Section Video</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<?php
				$rs = $this->db->get("section_video");
				$item = $rs->row();
				?>
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>main/saveVideo">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="video_name" class="col-sm-2 control-label">Youtube Embed Code</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="video_name" name="video_name" placeholder="ex: HSCAMJskUrA" value="<?=$item->video_name?>">
						</div>
					</div>
					<div class="form-group">
						<label for="video_preview" class="col-sm-2 control-label">Preview</label>
						<div class="col-sm-10" id="result">
							
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>