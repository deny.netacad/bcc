<?php
  $events = $this->db->query("
    SELECT * FROM r_kuota_harian
  ")->result();
?>

<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>

<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/locales-all.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.css">

<script>
    function submitSelesai() {
      var kuotaHarian   = $('#kuotaHarian').val();
      var saveStartDate = $('#saveStartDate').val();
      var saveEndDate   = $('#saveEndDate').val();

      var postData = {
        <?php echo $this->security->get_csrf_token_name(); ?> : "<?php echo $this->security->get_csrf_hash(); ?>",
        kuota_harian : kuotaHarian,
        tanggal_start : saveStartDate,
        tanggal_end : saveEndDate,
      };

      if(kuotaHarian == ""){
        alert('Kuota Harian Harap Diisi!');
      } else {
        $.post("<?php echo base_url('loket/ajaxfile/saveKuotaHarian'); ?>", postData, function( data ) {
          $('#modalKuota').modal('hide');
          location.reload();
        });
      }
    }

    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        height: 550,
        width: "100%",
        initialView: 'dayGridMonth',
        selectable: true,
        select: function(info) {
          $('#modalKuota').modal('show');

          var start = new Date(info.startStr);
          var dd = String(start.getDate()).padStart(2, '0');
          var mm = String(start.getMonth() + 1).padStart(2, '0'); //January is 0!
          var yyyy = start.getFullYear();
          start = dd + '-' + mm + '-' + yyyy;
          $('#startDate').html(start);

          var end = new Date(info.endStr);
          end.setDate(end.getDate() - 1);
          var dd = String(end.getDate()).padStart(2, '0');
          var mm = String(end.getMonth() + 1).padStart(2, '0'); //January is 0!
          var yyyy = end.getFullYear();
          end = dd + '-' + mm + '-' + yyyy;
          $('#endDate').html(end);

          $('#saveStartDate').val(info.startStr);
          $('#saveEndDate').val(info.endStr);
        },
        events: [
          <?php
            foreach ($events as $key => $value) {
              echo "{
                title: '".$value->kuota." Orang',
                start: '".$value->tanggal."'
              },";
            }
          ?>
        ]
      });
      calendar.render();
    });

</script>

<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
  				<div class="box-header with-border">
            <h3 class="box-title">Kuota Antrian</h3>
  				</div><!-- /.box-header -->
          <div class="box-body">
            <form class="form-horizontal" action="<?php echo base_url('loket/defaultKuota'); ?>" method="POST">
              <div class="form-group">
    						<label for="no_loket" class="col-md-2 control-label">Kuota Default</label>
    						<div class="col-md-7">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
    							<input type="number" name="kuota_default" class="form-control" placeholder="Jumlah Kuota Harian Default" value="<?php echo $this->db->query('SELECT value FROM r_pengaturan WHERE parameter = "kuota_default"')->row()->value; ?>" required/>
    						</div>
    						<div class="col-md-2">
    							<button type="submit" class="btn btn-block btn-md btn-primary">Simpan</button>
    						</div>
    					</div>
            </form>
            <hr>
            <div class="form-horizontal">
              <div class="form-group">
    						<label for="no_loket" class="col-md-2 control-label">Kuota Harian</label>
    						<div class="col-md-9">
                  <div id='calendar'></div>
    						</div>
    					</div>
            </div>
          </div>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>

  <!-- Modal -->
<div class="modal fade" id="modalKuota" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4><span class="glyphicon glyphicon-calendar"></span> Atur Kuota Harian</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="bus_hour" class="col-sm-3 control-label">Rentang Tanggal</label>
            <div class="col-sm-9">
              <span id="startDate">A</span>
              &nbsp;-&nbsp;
              <span id="endDate">B</span>
            </div>
          </div>
          <div class="form-group">
            <label for="bus_hour" class="col-sm-3 control-label">Kuota</label>
            <div class="col-sm-8">
              <input type="hidden" id="saveStartDate" name="tanggalStart"/>
              <input type="hidden" id="saveEndDate" name="tanggalEnd"/>
              <input type="number" id="kuotaHarian" name="kuota_setting" class="form-control" placeholder="Jumlah Kuota Harian" required/>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button onclick="submitSelesai()" class="btn btn-primary">Simpan</button>
        <button class="btn btn-danger pull-left" data-dismiss="modal">Batalkan</button>
      </div>
    </div>
  </div>
</div>
</section>
