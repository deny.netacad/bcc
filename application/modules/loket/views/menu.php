<li class="treeview">
	<a href="#" ><i class="text-blue icomoon-laptop"></i> <span>PELAYANAN LOKET</span> <i class="fa fa-angle-left pull-right"></i></a>
	<ul class="treeview-menu">
		<li><a href="<?=base_url()?>loket/antrian"><i class="text-blue icomoon-users"> </i> Manajemen Antrian</a></li>
		<li><a href="<?=base_url()?>loket/kuota"><i class="text-blue icomoon-calendar"> </i> Kuota Antrian</a></li>
		<li><a href="<?=base_url()?>loket/kategori"><i class="text-blue icomoon-tags"> </i> Kategori Layanan</a></li>
	</ul>
</li>
