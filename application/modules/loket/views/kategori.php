<?php
  $kategori = $this->db->query("
    SELECT * FROM r_layanan
  ")->result();
?>

<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>

<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/locales-all.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.css">

<script>
    function tambahKategori() {
      $('#judulModal').html("Tambah Kategori Baru");

      $('#idKategori').val("");
      $('#kodeAntrian').val("");
      $('#kategoriAntrian').val("");
      $('#kategoriEnable').val(1);

      $('#modalKategori').modal('show');
    }

    function editKategori(param) {
      var data = $(param).data('kategori');

      $('#judulModal').html("Edit Kategori");

      $('#idKategori').val(data.id);
      $('#kodeAntrian').val(data.kode_antrian);
      $('#kategoriAntrian').val(data.nama_layanan);
      $('#kategoriEnable').val(data.enable);

      $('#modalKategori').modal('show');
    }

    function submitKirim () {
      var id = $('#idKategori').val();
      var kode_antrian = $('#kodeAntrian').val();
      var nama_layanan = $('#kategoriAntrian').val();
      var enable = $('#kategoriEnable').val();

      var postData = {
        <?php echo $this->security->get_csrf_token_name(); ?> : "<?php echo $this->security->get_csrf_hash(); ?>",
        id : id,
        kode_antrian : kode_antrian,
        nama_layanan : nama_layanan,
        enable : enable
      };

      if(kode_antrian == "" || nama_layanan == ""){
        alert('Semua Data Harap Diisi!');
      } else {
        $.post("<?php echo base_url('loket/ajaxfile/saveKategori'); ?>", postData, function( data ) {
          if(data == "1"){
            alert("Berhasil Disimpan!");
            location.reload();
          } else alert("Gagal menyimpan, perisak kembali data input!");
        });
      }
    }

</script>

<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
  				<div class="box-header with-border">
            <h3 class="box-title">Kategori Layanan</h3>
            <button class="btn btn-small btn-primary pull-right" onclick="tambahKategori()">Tambah Kategori</button>
  				</div><!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Antrian</th>
                  <th>Nama Layanan</th>
                  <th>Aktif</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($kategori as $key => $value): ?>
                <tr>
                  <td><?php echo $key+1; ?></td>
                  <td><?php echo $value->kode_antrian; ?></td>
                  <td><?php echo $value->nama_layanan; ?></td>
                  <td>
                    <?php if ($value->enable): ?>
                      <span class="label label-success">Enabled</span>
                    <?php else: ?>
                      <span class="label label-danger">Disable</span>
                    <?php endif; ?>
                  </td>
                  <td>
                    <button class="btn btn-sm btn-info" data-kategori='<?php echo json_encode($value); ?>' onclick="editKategori(this);"><i class="fa fa-pencil"></i>&nbsp;Edit</button>
                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>

  <!-- Modal -->
<div class="modal fade" id="modalKategori" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4><span class="glyphicon glyphicon-tags"></span> &nbsp; <span id="judulModal">Manajemen Kategori</span></h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <input type="hidden" id="idKategori" />
          <div class="form-group">
            <label for="bus_hour" class="col-sm-3 control-label">Kode Antrian</label>
            <div class="col-sm-8">
              <input type="text" id="kodeAntrian" class="form-control" placeholder="Kode Antrian" required />
            </div>
          </div>
          <div class="form-group">
            <label for="bus_hour" class="col-sm-3 control-label">Kategori Antrian</label>
            <div class="col-sm-8">
              <input type="text" id="kategoriAntrian" class="form-control" placeholder="Kategori Antrian" required />
            </div>
          </div>
          <div class="form-group">
            <label for="bus_hour" class="col-sm-3 control-label">Aktif</label>
            <div class="col-sm-8">
              <select class="form-control" id="kategoriEnable">
                <option value="1">Enable</option>
                <option value="0">Disable</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button onclick="submitKirim()" class="btn btn-primary">Simpan</button>
        <button class="btn btn-danger pull-left" data-dismiss="modal">Batalkan</button>
      </div>
    </div>
  </div>
</div>
</section>
