<link rel="stylesheet" href="<?=$def_js?>plugins/jquery-file-upload/css/jquery.fileupload-ui.css">
<script src="<?=$def_js?>plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/tmpl.min.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/load-image.min.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<script src="<?=$def_js?>plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
<style>
	canvas{
		overflow:hidden;
	}
</style>
<script>
function _formatBitrate(bits) {
	if (typeof bits !== 'number') {
		return '';
	}
	if (bits >= 1000000000) {
		return (bits / 1000000000).toFixed(2) + ' Gbit/s';
	}
	if (bits >= 1000000) {
		return (bits / 1000000).toFixed(2) + ' Mbit/s';
	}
	if (bits >= 1000) {
		return (bits / 1000).toFixed(2) + ' kbit/s';
	}
	return bits.toFixed(2) + ' bit/s';
}

function _formatPercentage(floatValue) {
    return (floatValue * 100).toFixed(2) + ' %';
}


function _formatFileSize(bytes) {
	if (typeof bytes !== 'number') {
		return '';
	}
	if (bytes >= 1000000000) {
		return (bytes / 1000000000).toFixed(2) + ' GB';
	}
	if (bytes >= 1000000) {
		return (bytes / 1000000).toFixed(2) + ' MB';
	}
	return (bytes / 1000).toFixed(2) + ' KB';
}


function _formatTime(seconds) {
	var date = new Date(seconds * 1000),
		days = Math.floor(seconds / 86400);
	days = days ? days + 'd ' : '';
	return days +
		('0' + date.getUTCHours()).slice(-2) + ':' +
		('0' + date.getUTCMinutes()).slice(-2) + ':' +
		('0' + date.getUTCSeconds()).slice(-2);
}

function _formatPercentage(floatValue) {
	return (floatValue * 100).toFixed(2) + ' %';
}

function _renderExtendedProgress(data) {
	return _formatBitrate(data.bitrate) + ' | ' +
		_formatTime(
			(data.total - data.loaded) * 8 / data.bitrate
		) + ' | ' +
		_formatPercentage(
			data.loaded / data.total
		) + ' | ' +
		_formatFileSize(data.loaded) + ' / ' +
		_formatFileSize(data.total);
}
var isAlbumSaved = 0;

$(document).on('ready',function(){
	isAlbumSaved = 0;
	$(document).on('click','#savegallery',function(){
		if($(this).find('span').html()=='Simpan Gallery'){
			$('#fileupload').submit();
		}
	});	
	
	$(document).on('click','#add-album',function(){
		$.ajax({
			url:'<?=base_url()?>operatorweb/page/view/galleryuploadform',
			type:'post',
			data:{ 
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
			beforeSend:function(){},
			success:function(response){
				$('#modal-content').html(response);
				$('#myModal').modal('show',function(){
					keyboard:false
				});
				
				return false;
			}
		});
	});
	
	$(document).on('click','#add-photo',function(){
		
		$.ajax({
			url:'<?=base_url()?>operatorweb/page/view/galleryuploadform',
			type:'post',
			data:{ 
				'album':$(this).attr('data'),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
			beforeSend:function(){},
			success:function(response){
				$('#modal-content').html(response);
				$('#myModal').modal('show',function(){
					keyboard:false
				});
				
				return false;
			}
		});
	});
	
	
		$(document).on('submit',"#fileupload",function(event) {
			event.preventDefault();
			var $form = $( this );
			$('input').each(function(index,item){
				$(item).attr('disabled',false);
			});
			$('select').each(function(index,item){
				$(item).attr('disabled',false);
			});
			
			var url = $form.attr( 'action' );
			if($('#album').val()==''){
				$('#album').focus();
			}else{
				if(confirm('Simpan album ..?')){
					$.post(url, $form.serialize(),
					function(data) {
						window.location = "#";
						var ret = $.parseJSON(data);
						isAlbumSaved = 1;
						alert(ret.text);
						$('#myModal').modal('hide');
						$.ajax({
							type:'post',
							url:'<?=base_url()?>operatorweb/page/data/galleryuploaddetail',
							data:{ 
								'album':$('#album').val(),
								'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
							},
							beforeSend:function(){
								$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
							},
							success:function(response){
								$('#result').html(response);
							}
						});
					});		
				}else{
					$('#myModal').modal('hide');
				}
			}
		});
	
		$(document).on('hide.bs.modal','#myModal', function () {
			 if(isAlbumSaved==0){
				 $('input.toggle').prop('checked',true);
				 $('button.delete').trigger('click');
			 }
		});
		
		$(document).on('click','.del-album',function(){
			var $this = $('.del-album');
			var index = $this.index($(this));
			if(confirm('Yakin akan menghapus album '+$this.eq(index).attr('data')+'...?')){
				$.ajax({
					type:'post',
					url:'<?=base_url()?>operatorweb/page/delGalleryupload',
					data:{ 
						'album':$this.eq(index).attr('data'),
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					beforeSend:function(){
						$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
					},
					success:function(response){
						refreshData();
					}
				});
			}
		});
		
		$(document).on('click','.view-album',function(){
			var $this = $('.view-album');
			var index = $this.index($(this));
			$.ajax({
				type:'post',
				url:'<?=base_url()?>operatorweb/page/data/galleryuploaddetail',
				data:{ 
					'album':$this.eq(index).attr('data'),
					'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
				},
				beforeSend:function(){
					$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
				},
				success:function(response){
					$('#result').html(response);
				}
			});
		});
		
		$(document).on('click','.del-photo',function(){
			var $this = $('.del-photo');
			var index = $this.index($(this));
			var arrdata = $this.eq(index).attr('data').split('|');
			if(confirm('Yakin akan menghapus photo ini ...?')){
				$.ajax({
					type:'post',
					url:'<?=base_url()?>operatorweb/page/delGalleryuploadphoto',
					data:{ 
						'filenameid':arrdata[0],
						'album':arrdata[1],
						'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
					},
					beforeSend:function(){
						$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
					},
					success:function(response){
						$.ajax({
							type:'post',
							url:'<?=base_url()?>operatorweb/page/data/galleryuploaddetail',
							data:{ 
								'album':arrdata[1],
								'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
							},
							beforeSend:function(){
								$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
							},
							success:function(response){
								$('#result').html(response);
							}
						});
					}
				});
			}
		});
		
		refreshData();
});

function refreshData(){
		$.ajax({
			type:'post',
			url:'<?=base_url()?>operatorweb/page/data/galleryupload',
			data:{ 
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
			beforeSend:function(){
				$("#result").isLoading({ text: "Loading...", position:'overlay', tpl:'<div class="loader"></div>'});
			},
			success:function(response){
				$('#result').html(response);
			}
		});
	}
</script>


<section class="content-header">
  <h1>
	Gallery Management
	<small>Manage your gallery</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
	<li><a href="#">Manajemen Konten</a></li>
	<li class="active">Gallery Management</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

 
	  <div class="row-fluid" id="result">
		
	  </div>
	  

</section><!-- /.content -->


<div class="modal fade bs-example-modal-lg" id="myModal">
  <div class="modal-dialog  modal-lg">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		<h4 class="modal-title">Upload Album</h4>
	  </div>
	  <div class="modal-body" id="modal-content">
		
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	  </div>
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>