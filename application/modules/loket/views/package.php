<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<?php
#print_r($this->session->userdata);
?>
<script>
	function CKupdate(){
		for ( instance in CKEDITOR.instances ){
			console.log(instance);
			CKEDITOR.instances[instance].updateElement();
			CKEDITOR.instances[instance].setData('');
		}
		
	}
	
	
	function loadData(){
        $.ajax({
            type:'post',
            url:'<?=base_url()?>main/loadPackage',
            data:{ 
				'pack_id':$('#pack_id').val(),
				'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
			},
            beforeSend:function(){
                
            },
            success:function(response){
                var ret = $.parseJSON(response);
				if(ret.pack_id){
					for(attrname in ret){
						if(attrname=='pack_id'){
							$('#id_edit').val(ret[attrname]);
						}
						$('#'+attrname).val(ret[attrname]);
					}
				}else{
					$('#id_edit').val('');
					$('#title').val('');
					$('#jml_prod').val('');
					CKupdate();
					$('#title').focus();
				}
            }
        });
    }

	
    $(document).ready(function(){
        
		var config_pengantar = {
            toolbar : 'MyToolbar',
            height: '350'
        };
		
		var config_image = {
            toolbar : 'Image',
            height: '150',
            width: '100%'
        };
		
		var config_image2 = {
            toolbar : 'Image',
            height: '287',
			width: '430'
        };
		
		var config_image3 = {
            toolbar : 'Image',
            height: '100',
            width: '200'
        };


        $('#thumb_pro').ckeditor(config_image);
		$('#bg_pack').ckeditor(config_image2);
		$('#bg_figure').ckeditor(config_image3);
		
		$('#content').ckeditor(config_pengantar);
		
		
		/* action save */
        $(document).on('submit','#form1',function(event) {
            event.preventDefault();
            var $form = $( this );
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });

            var url = $form.attr( 'action' );

            if(confirm('simpan data ..?')){
                $.post(url, $form.serialize(),
                    function(data) {
                        window.location = "#";
                        var ret = $.parseJSON(data);
                        alert(ret.text);
                        $('#form1').trigger('reset');
						loadData();
                    });
            }else{
                $('#form1').trigger('reset');
				loadData();
            }
        });

        /* untuk reset */
        $(document).on('reset','#form1',function(){
            $('input').each(function(index,item){
                $(item).attr('disabled',false);
            });
            $('select').each(function(index,item){
                $(item).attr('disabled',false);
            });
			CKupdate();
        });
		
		
		$('#myTab').on('shown',function(e){
			var index = $('#myTab a').index(e.target);
			switch(index){
				case 0:
					$('#pack_id').focus();
				break;
			}
		});
		
		$('#pack_id').focus();
		
		$(document).on('change','#pack_id',function(){
			loadData();
		});
    });

    
</script>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Section - Menu Paket</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?=base_url()?>main/savePackage">
				<input type="hidden" id="<?=$this->security->get_csrf_token_name()?>" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<input type="hidden" id="id_edit" name="id_edit" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Pilih Jenis Paket</label>
						<div class="col-sm-10">
							<select id="pack_id" name="pack_id" class="form-control" required>
								<option value="">-Pilih Jenis Paket-</option>
								<option value="1">Lamarizk Basic</option>
								<option value="2">Lamarizk Bisnis</option>
								<option value="3">Lamarizk Eksekutif</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Judul Paket</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="title" name="title" placeholder="ex: Lamarizk Eksekutif">
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Jumlah Produk</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="jml_prod" name="jml_prod" placeholder="0" /> Produk
						</div>
					</div>
					<div class="form-group">
						<label for="bg_pack" class="col-sm-2 control-label">Background Section Paket</label>
						<div class="col-sm-10">
							<textarea id="bg_pack" name="bg_pack" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="bg_figure" class="col-sm-2 control-label">Photo Figur</label>
						<div class="col-sm-10">
							<textarea id="bg_figure" name="bg_figure" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="thumb_pro" class="col-sm-2 control-label">Thumbnail Produk</label>
						<div class="col-sm-10">
							<textarea id="thumb_pro" name="thumb_pro" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-2 control-label">Deskripsi Produk</label>
						<div class="col-sm-10">
							<textarea id="content" name="content" placeholder=""></textarea>
						</div>
					</div>
					
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div><!-- /.box-footer -->
				</form>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>
</section>