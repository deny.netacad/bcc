<script>
    $(document).ready(function(){

    });
</script>

<?php
    $where = " ";
    $per_page = (($this->input->post('per_page')=='')?5:$this->input->post('per_page'));
    $cari = $this->input->post('cari');

    if($cari!="") $where.= " and (a.event_name like '%".$cari."%') ";

    $n = intval($this->uri->segment(5));
    $q = "select * from section_events a 
	left join mast_user b on a.iduser=b.iduser where a.idbidang='".$this->session->userdata('idbidang')."'
	";
    $rs = $this->db->query("$q $where");
    $row = $rs->row_array();
    $totrows = $rs->num_rows();

    $config['base_url'] = base_url().'operatorweb/page/data/gallery';

    $config['total_rows'] 			= $totrows;
    $config['per_page'] 			= $per_page;
    $config['uri_segment'] 			= 5;
    $config['is_ajax_paging']    	= TRUE;

    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();
    $data['posts'] = $this->db->query("SELECT a.*,b.username
		FROM section_events a
		left join mast_user b on a.iduser=b.iduser where a.idbidang='".$this->session->userdata('idbidang')."'
		 $where
		ORDER BY a.event_id desc limit
		".intval($this->uri->segment(5)).",".intval($this->pagination->per_page)."");
?>

<div class="dataTables_paginate paging_full_numbers" id="dyntable2_paginate">
    <span class="paginate_info" style="display:inline-block;margin-left:10px">Hal <?=$this->pagination->cur_page?> - <?=$this->pagination->num_pages?> dari <?=$this->pagination->total_rows?> Record</span>
    <?=$data['pagination']?>
</div>
<table class="table table-hover">
    
    <thead class="breadcrumb">
    <tr>
        <th>#</th>
        <th>NAMA EVENT</th>
		<th>THUMBNAIL</th>
		<th><div class="text-center">USER<div></th>
        <th>PROSES</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data['posts']->result() as $item){ $n++;
    preg_match_all('/<img[^>]+>/i',$item->event_thumb, $result);
	if(count($result[0])>0){
		$event_thumb = (string) reset(simplexml_import_dom(DOMDocument::loadHTML($result[0][0]))->xpath("//img/@src"));
	}
	?>
    <tr>
        <td align="center"><?=$n?>.</td>
        <td>
            <span class="ed1" style="display:none"><?=$item->event_id?></span>
            <div class="text-left"><?=$item->event_name?></div>
        </td>
		<td>
            <div class="text-center"><a href="<?=str_replace('.thumbs/','',$event_thumb)?>" rel="lightbox"><img src="<?=$event_thumb?>" /></a></div>
        </td>
		<td>
            <div class="text-center"><?=$item->username?></div>
        </td>
        <td>
            <div class="text-left">
                <a href="#" title="Edit" data="<?=$item->event_id?>"><i class="text-green  fa fa-edit"></i></a>
                <a href="#" title="Delete" data="<?=$item->event_id?>"><i class="text-red fa fa-trash"></i></a>
            </div>
        </td>
    </tr>
        <?php
    }
    ?>
    </tbody>
</table>