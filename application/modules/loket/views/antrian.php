<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/back/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/terbilang.js"></script>
<script>
    $(document).ready(function(){
      cekLoket();
      cekSisa();

      setInterval(function(){
        cekLoket();
        cekSisa();
      }, 3000);

      setInterval(function(){
        cekAntrian();
      }, 500);
    });

    function cekSisa() {
      $.get("<?php echo base_url('loket/ajaxfile/hitungTunggu'); ?>", function( data ) {
        $('#sisaAntrian').html(data.jumlah);
      });
    }
    function panggilNext() {
      var namaLoket = localStorage.getItem("noLoket");
      var postData = {
        <?php echo $this->security->get_csrf_token_name(); ?> : "<?php echo $this->security->get_csrf_hash(); ?>",
        loket: namaLoket
      };

      $.post("<?php echo base_url('loket/ajaxfile/panggilNext'); ?>", postData, function( data ) {
        localStorage.removeItem("antrianSekarang");
        if(data.length != 0) localStorage.setItem("antrianSekarang", JSON.stringify(data));
      });
    }
    function panggilUlang() {
      var antrian = JSON.parse(localStorage.getItem("antrianSekarang"));

      var postData = {
        <?php echo $this->security->get_csrf_token_name(); ?> : "<?php echo $this->security->get_csrf_hash(); ?>",
        id : antrian.id
      };

      if(confirm("Panggil Ulang Antrian "+antrian.no_antrian+"?")){
        $.post("<?php echo base_url('loket/ajaxfile/panggilUlang'); ?>", postData, function( data ) {});
      }

    }

    function cekAntrian() {
      var antrian = JSON.parse(localStorage.getItem("antrianSekarang"));
      if(antrian == null){
        $('#panggilAntrian').html('0000');
        $('#btnLihat').prop('disabled', true);
        $('#btnUlang').prop('disabled', true);
        $('#btnSelesai').prop('disabled', true);
        $('#btnBerikutnya').prop('disabled', false);
        $('#btnTutup').prop('disabled', false);
      } else {
        $('#panggilAntrian').html(antrian.no_antrian);
        $('#btnLihat').prop('disabled', false);
        $('#btnUlang').prop('disabled', false);
        $('#btnSelesai').prop('disabled', false);
        $('#btnBerikutnya').prop('disabled', true);
        $('#btnTutup').prop('disabled', true);
      }
    }

    function selesai() {
      $('#keterangan').val("");
      $('#modalSelesai').modal('show');
    }

    function submitSelesai() {
      var antrian = JSON.parse(localStorage.getItem("antrianSekarang"));
      var keterangan = $('#keterangan').val();

      var postData = {
        <?php echo $this->security->get_csrf_token_name(); ?> : "<?php echo $this->security->get_csrf_hash(); ?>",
        keterangan : keterangan,
        id : antrian.id
      };

      if(keterangan == ""){
        alert('Keterangan Harap Diisi!');
      } else {
        $.post("<?php echo base_url('loket/ajaxfile/selesaiAntrian'); ?>", postData, function( data ) {
          $('#modalSelesai').modal('hide');
          localStorage.removeItem("antrianSekarang");
        });
      }
    }

    function cekLoket() {
      var namaLoket = localStorage.getItem("noLoket");
      if(namaLoket == null){
        $('#btnBuka').show();
        $('#btnTutup').hide();
        $('#prosesLoket').hide();
      } else {
        $('#noLoket').val(namaLoket);
        $('#btnBuka').hide();
        $('#btnTutup').show();
        $('#prosesLoket').show();
      }
    }
    function bukaLoket() {
      var namaLoket = $('#noLoket').val();
      if(namaLoket == "") alert("Tolong isi Kode/Nomor Loket!");
      else {
        localStorage.setItem("noLoket", namaLoket);
        $('#btnBuka').hide();
        $('#btnTutup').show();
        $('#prosesLoket').show();
      }
    }
    function tutupLoket() {
      if(confirm("Tutup Loket "+localStorage.getItem("noLoket")+"?")){
        localStorage.removeItem("noLoket");
        localStorage.removeItem("antrianSekarang");
        $('#noLoket').val("");
        $('#btnBuka').show();
        $('#btnTutup').hide();
        $('#prosesLoket').hide();
      }
    }
</script>

<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update</a></li>
		  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Manajemen Antrian</h3>
				</div><!-- /.box-header -->
        <div class="box-body">
          <div class="form-horizontal">
            <div class="form-group">
  						<label for="no_loket" class="col-md-2 control-label">Kode Loket</label>
  						<div class="col-md-8">
  							<input type="text" id="noLoket" name="no_loket" class="form-control" placeholder="Tuliskan Kode/Nomor Loket" />
  						</div>
  						<div class="col-md-2">
  							<button type="button" class="btn btn-block btn-md btn-primary" id="btnBuka" onclick="bukaLoket()">Buka Loket</button>
  							<button type="button" class="btn btn-block btn-md btn-danger" id="btnTutup" onclick="tutupLoket()">Tutup Loket</button>
  						</div>
  					</div>
          </div>
          <hr>
          <div id="prosesLoket" class="row">
            <div class="col-lg-6">
              <div class="jumbotron text-center">
                <p>Antrian Sekarang</p>
                <h1 id="panggilAntrian">0000</h1>
                <p>
                  <button id="btnLihat" class="btn btn-info btn-sm">Lihat Keperluan</button>
                </p>
                <p>
                  <button id="btnUlang" onclick="panggilUlang()" class="btn btn-warning btn-md">Panggil Ulang</button>
                  <button id="btnSelesai" onclick="selesai()" class="btn btn-success btn-md">Selesai</button>
                  <button id="btnBerikutnya" class="btn btn-primary btn-md" onclick="panggilNext()" >Berikutnya</button>
                </p>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="jumbotron text-center">
                <p>Sedang Menunggu</p>
                <h1 id="sisaAntrian">0000</h1>
                <p>
                  Antrian Tersisa
                </p>
                <p>
                  <a class="btn btn-warning btn-md" href="#" style="visibility: hidden;" role="button"></a>
                </p>
              </div>
            </div>
          </div>
        </div>
				</div>
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div>

  <!-- Modal -->
<div class="modal fade" id="modalSelesai" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4><span class="glyphicon glyphicon-user"></span> Selesaikan Antrian</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="bus_hour" class="col-sm-2 control-label">Keterangan</label>
            <div class="col-sm-10">
              <textarea id="keterangan" class="form-control" placeholder="Keterangan Penyelesaian Antrian" required></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button onclick="submitSelesai()" class="btn btn-primary">Simpan</button>
        <button class="btn btn-danger pull-left" data-dismiss="modal">Batalkan</button>
      </div>
    </div>
  </div>
</div>
</section>
