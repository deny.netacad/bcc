<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MY_Controller {
	var $modules = "portal";
	
	public function index()
	{
		$this->load->view($this->modules.'/index',$this->data);
	}
	
	function subpage(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function testing(){
		echo $this->db->insert('ngi_webvisitor',array('ip' => $this->input->ip_address()));
	}

	function jobs(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function login(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function pengaturan(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function lamaran(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function profile(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function testqr(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function register(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function register_perusahaan(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function tentang(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}
	
	function kontak(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}
	
	function caripekerjaan(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function all_berita(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}
	function all_event(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}
	function event(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function berita(){
		$this->data['page'] = $this->uri->segment(3);
		$this->load->view($this->modules.'/index',$this->data);
	}

	function cariKab(){
		$result = $this->db->query('select city_id, city, type from ngi_kecamatan where province_id = "'.$_POST['id'].'" group by city_id')->result();
		$output = '<option value="">Semua Kab/Kota</option>';

		foreach ($result as $data) {
			if($data->city_id == $_POST['ids']){
				$output .= '<option value="'.$data->city_id.'" selected>'.$data->type.' '.$data->city.'</option>';
			}else{
				$output .= '<option value="'.$data->city_id.'">'.$data->type.' '.$data->city.'</option>';
			}

		}

		echo $output;
	}

	function lamar(){
		if($this->session->userdata('is_login_pelamar')){
			$input = array(
				'id_pelamar' => $_POST['id'],
				'id_job' => $_POST['id_job']
			);

			if($this->db->insert('ngi_jobapplied', $input)){
				echo json_encode(array('login' => true, 'success' => true));
			}else{
				echo json_encode(array('login' => true, 'success' => false));
			}
		}else{
			echo json_encode(array('login' => false));
		}
	}
	
	function readqr($qr){
		if($this->session->userdata('is_login_pelamar')){
			$input = array(
				'id_pelamar' => $_POST['id'],
				'id_job' => $_POST['id_job']
			);

			if($this->db->insert('ngi_jobapplied', $input)){
				echo json_encode(array('login' => true, 'success' => true));
			}else{
				echo json_encode(array('login' => true, 'success' => false));
			}
		}else{
			echo json_encode(array('login' => false));
		}
	}
	
	function absensi(){
		$input = array(
			'idevent' => '1',
			'iduser' => $_POST['iduser'],
			'log' => date('Y-m-d')
		);
		
		if($this->db->insert('ngi_pengunjungevent', $input)){
			echo json_encode(array('success' => true));
		}else{
			echo json_encode(array('success' => false));
		}
	}
	
	function saveprofil(){
		$arrnot = ['id_edit', 'province_id', 'kab_id'];

		foreach ($_POST as $key => $value) {
			if(!in_array($key, $arrnot)){
				$input[$key] = $value;
			}
		}

		if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar',$input)){
			echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."portal/pengaturan'</script>";
		}
	}

	function saveemail(){
		$arrnot = ['id_edit','link_enkripsi'];

		$input['verified'] = 0;
		foreach ($_POST as $key => $value) {
			if(!in_array($key, $arrnot)){
				$input[$key] = $value;
			}
		}

		if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar',$input)){
			$pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			 <head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title>Demystifying Email Design</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body style="margin: 0; padding: 0;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				 <tr width="100%">
				 	<td width="0%"></td>
				  <td width="100%">
				   	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
			  <tr>
			  <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
			 	<img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
				</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
			 <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
			 <b >Verifikasi alamat email</b>
			</td>
			 </tr>
			 <tr>
			  <td style="padding: 20px 0 30px 0;" align="center">
			   Silahkan verifikasi alamat email anda dengan menekan tombol dibawah. Jika tidak, anda bisa mengunjungi url berikut '.base_url().'portal/page/verifikasiemailpelamar/'.$_POST['link_enkripsi'].'
			  </td>
			 </tr>
			 <tr>
			  <td align="center">
			   <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/page/verifikasiemailpelamar/'.$_POST['link_enkripsi'].'">Verifikasi!</a>
			  </td>
			 </tr>
			 </table>
			</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
			   <table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <tr>
			 <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
			 &reg; Bogor Career Center, 2019<br/>
			</td>
			  <td align="right">
			 
			</td>
			 </tr>
			</table>
			  </td>
			 </tr>
			 </table>
				  </td>
				  <td width="0%"></td>
				 </tr>
			</table>
			</body>
			</html>';

			$from_email = "noreply@bogorcareercenter.bogorkab.go.id";
			$to_email = $_POST['email'];

			$this->load->library('email');
			$config = array();
			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
			$config['smtp_user'] = 'superkarir@cpanel1.bogorkab.go.id';
			$config['smtp_pass'] = 'Superkarir99';
			$config['smtp_port'] = 465;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $this->email->set_header("Content-Type", "text/html");
			$this->email->from($from_email, 'Bogor Career Center');
	        $this->email->to($to_email);
	        $this->email->subject('Bogor Career Center Verifikasi Email');
	        $this->email->message($pesan);
			#$result = $this->email->send();
			$this->email->send();

			echo "<script type='text/javascript'>alert('Berhasil menyimpan email. Cek email anda untuk melakukan verifikasi email');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."portal/pengaturan'</script>";
		}
	}

	function savelasteducation(){
		$arrnot = ['id_edit'];

		foreach ($_POST as $key => $value) {
			if(!in_array($key, $arrnot)){
				$input[$key] = $value;
			}
		}

		if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar',$input)){
			echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."portal/pengaturan'</script>";
		}
	}

	  function uploadlampiran(){
	    $path_parts = pathinfo($_FILES["files"]["name"]);    
	    $extension = $path_parts['extension'];
	    $save_path = BASEPATH.'../assets/img/lampiran/';
	    $filename = "lampiran_1_".time().".".$extension;  
	    $config['upload_path'] = $save_path;
	    $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';   
	    $config['max_size'] = '5000000';    
	    $config['file_name'] = $filename;
	    $this->load->library('upload', $config);  

	    if(file_exists($save_path.$filename)){
	      unlink($save_path.$filename);
	    }
	    if (!$this->upload->do_upload('files')){         
	        echo json_encode(array('success' => false, 'teks' => 'Gagal mengupload gambar'.$this->upload->display_errors())); 
	    }else{
	        foreach ($_POST as $key => $value) { 
	            if($key!='files'){
	              $data[$key] = $value;
	            }  
	        }
	        $data['idpelamar'] = $this->session->userdata('id');
	        $data['nama_file'] = $filename;
			
			$file = $this->db->query('select nama_file from ngi_jobattachment where idpelamar = "'.$this->session->userdata('id').'" 
			and idjob = "'.$_POST['idjob'].'" and idfile = "'.$_POST['idfile'].'"')->row();
			
			if ($file->nama_file == ""){	
				if($this->db->insert('ngi_jobattachment',$data)){
				  echo json_encode(array('success' => true, 'teks' => 'Berhasil mengupload gambar')); 
				}else{
				  unlink($save_path.$filename);
				  echo json_encode(array('success' => false, 'teks' => 'Gagal mengupload gambar'.$this->db->last_query())); 
				}
			}else{
				unlink($save_path.$file->nama_file);
				if($this->db->update('ngi_jobattachment',array('nama_file' => $filename),array('idpelamar' => $this->session->userdata('id'), 'idjob' => $_POST['idjob'], 'idfile' => $_POST['idfile']))){
				  echo json_encode(array('success' => true, 'teks' => 'Berhasil mengupload gambar')); 
				}else{
				  unlink($save_path.$filename);
				  echo json_encode(array('success' => false, 'teks' => 'Gagal mengupload gambar'.$this->db->last_query())); 
				}
			}

	    }
	  }

	function saveakun(){
		if($_FILES["scanfile"]["size"] != 0){
			$fiename = $_POST["filename"];
			$path_parts = pathinfo($_FILES["scanfile"]["name"]);		
			$extension = $path_parts['extension'];
			$save_path = BASEPATH.'../assets/img/profile/';
			if($filename!=""){
				$array = explode(".", $filename);
				$filename = $array[0].".".$extension;
			}else{
				$filename = time().".".$extension;
			}

			$config['upload_path'] = $save_path;
			$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';		
			$config['max_size']	= '5000000';		
			$config['file_name'] = $filename;
			$this->load->library('upload', $config);	

			if(file_exists($save_path.$filename)){
				unlink($save_path.$filename);
			}

			if (!$this->upload->do_upload('scanfile')){					
				echo "<script type='text/javascript'>alert('Gagal mengupload foto.".$this->upload->display_errors()."');top.location='".base_url()."portal/pengaturan'</script>";	
			}else{			
				if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar',array('profile_url' => $_POST['profile_url'], 'username' => $_POST['username'], 'photo' => $filename))){
					echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."portal/pengaturan'</script>";
				}else{
					unlink($save_path.$filename);
					echo "<script type='text/javascript'>alert('Gagal menyimpan. Username/profile url sudah digunakan');top.location='".base_url()."portal/pengaturan'</script>";
				}	
			}
		}else{
			if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar',array('profile_url' => $_POST['profile_url'], 'username' => $_POST['username']))){
				echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."portal/pengaturan'</script>";
			}else{
				echo "<script type='text/javascript'>alert('Gagal menyimpan. Username/profile url sudah digunakan');top.location='".base_url()."portal/pengaturan'</script>";
			}
		}
	}

	function savepassword(){
		if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar',array('password' => md5($_POST['pass_baru'])))){
			echo "<script type='text/javascript'>alert('Berhasil mengubah password.');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal mengubah password.');top.location='".base_url()."portal/pengaturan'</script>";
		}
	}

	function savekomentar(){
		foreach ($_POST as $key => $value) {
			if($key != 'url'){
				$input[$key] = $value;
			}
		}

		if($this->db->insert('ngi_jobcomment',$input)){
			redirect($_POST['url']);
		}else{
			echo "<script type='text/javascript'>alert('Gagal memberikan komentar. Silahkan coba lagi.');top.location='".$_POST['url']."'</script>";
		}
	}

	function hapuskomentar(){
		if($this->db->where('id', $_POST[id])->update('ngi_jobcomment', array('is_aktif' => 0))){
			redirect($_POST['url']);
		}else{
			echo "<script type='text/javascript'>alert('Gagal menghapus komentar. Silahkan coba lagi.');top.location='".$_POST['url']."'</script>";
		}
	}

	function checkusername(){
		$q = $this->db->query('select iduser from user_pelamar where username = "'.$_POST['username'].'"');
		$q2 = $this->db->query('select idperusahaan from ngi_perusahaan where username = "'.$_POST['username'].'"');

		if($q->num_rows() == 0 && $q2->num_rows() == 0){
			echo json_encode(array('available' => true));
		}else{
			echo json_encode(array('available' => false));
		}
	}
	function ceknik(){
		$q = $this->db->query('select iduser from user_pelamar where no_ktp = "'.$_POST['no_ktp'].'"');
	//	$q2 = $this->db->query('select idperusahaan from ngi_perusahaan where noktpdir = "'.$_POST['noktpdir'].'"');

		if($q->num_rows() == 0){
			echo json_encode(array('available' => true));
		}else{
			echo json_encode(array('available' => false));
		}
	}
	function checkusername_perusahaan(){
		$q = $this->db->query('select idperusahaan from ngi_perusahaan where username = "'.$_POST['username'].'"');
		$q2 = $this->db->query('select iduser from user_pelamar where username = "'.$_POST['username'].'"');

		if($q->num_rows() == 0 && $q2->num_rows() == 0){
			echo json_encode(array('available' => true));
		}else{
			echo json_encode(array('available' => false));
		}
	}

	function checkemail(){
		$q = $this->db->query('select idperusahaan from ngi_perusahaan where email = "'.$_POST['email'].'"');
		$q2 = $this->db->query('select iduser from user_pelamar where email = "'.$_POST['email'].'"');

		if($q->num_rows() == 0 && $q2->num_rows() == 0){
			echo json_encode(array('available' => true));
		}else{
			echo json_encode(array('available' => false));
		}
	}

	function checkurlprofile(){
		$q = $this->db->query(' select iduser from user_pelamar where profile_url = "'.$_POST['url'].'" ');

		if($q->num_rows() == 0){
			echo json_encode(array('available' => true));
		}else{
			echo json_encode(array('available' => false));
		}
	}

	function checkpassword(){
		$q = $this->db->query('select iduser from user_pelamar where iduser = "'.$this->session->userdata('id').'" and password = md5("'.$_POST['pass'].'") ');

		if($q->num_rows() == 0){
			echo json_encode(array('available' => false));
		}else{
			echo json_encode(array('available' => true));
		}
	}

	function searchskill(){
		$result = $this->db->query('select skill from ngi_jobskill where skill like "%'.$_POST['skill'].'%"');

		$output = '<ul class="ui-autocomplete">';
		if($result->num_rows() > 0){
			foreach ($result->result() as $item) {
				$output .= '<li class="ui-menu-item select-skill" data="'.$item->skill.'">
						    	<a>'.$item->skill.'</a>
						  	</li>';
			}

		}else{
			$output .= '<li class="ui-menu-item select-skill" data="'.$_POST['skill'].'">
							<a>Tambahkan "'.$_POST['skill'].'"</a>
						</li>';
		}
		
		$output .= '</ul>';

		echo $output;
	}

	function savenewskill(){
		if($this->db->insert('ngi_jobskill', array('skill' => $_POST['skill']))){
			echo true;
		}else{
			echo false;
		}
	}

	function saveeducationuser(){
		if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar', array('education' => $_POST['education']))){
			echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."portal/pengaturan'</script>";
		}
	}

	function searcheducation(){
		$result = $this->db->query('select nama_sekolah from ngi_pendidikan where nama_sekolah like "%'.$_POST['education'].'%"');

		$output = '<ul class="ui-autocomplete">';
		if($result->num_rows() > 0){
			foreach ($result->result() as $item) {
				$output .= '<li class="ui-menu-item select-education" data="'.$item->nama_sekolah.'">
						    	<a>'.$item->nama_sekolah.'</a>
						  	</li>';
			}

		}else{
			$output .= '<li class="ui-menu-item select-education" data="'.$_POST['education'].'">
							<a>Tambahkan "'.$_POST['education'].'"</a>
						</li>';
		}
		
		$output .= '</ul>';

		echo $output;
	}

	function saveneweducation(){
		if($this->db->insert('ngi_pendidikan', array('nama_sekolah' => $_POST['education']))){
			echo true;
		}else{
			echo false;
		}
	}

	function saveskilluser(){
		if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar', array('skill' => $_POST['skill']))){
			echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."portal/pengaturan'</script>";
		}
	}

	function saveexpuser(){
		if($this->db->where('iduser',$_POST['id_edit'])->update('user_pelamar', array('exp' => $_POST['exp']))){
			echo "<script type='text/javascript'>alert('Berhasil menyimpan.');top.location='".base_url()."portal/pengaturan'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal menyimpan. Terjadi kesalahan.');top.location='".base_url()."portal/pengaturan'</script>";
		}
	}

	function loginportal(){
		$q = $this->db->query('select iduser, nama, verified, email from user_pelamar where (email = "'.$_POST['username'].'" or username = "'.$_POST['username']. '" ) and password = "'.md5($_POST['password']).'"');

		if($q->num_rows() > 0) {
			$data = $q->row();

			if($data->verified == 1) {
				$setdata = array(
					'is_login_pelamar' => true,
					'id' => $data->iduser,
					'nama' => $data->nama
				);

				$this->session->set_userdata($setdata);
				$this->db->query("update user_pelamar set last_login = current_timestamp where iduser = '".$data->iduser."'");

				echo json_encode(array('success' => true, 'tipe' => 'pelamar'));
			}else{
				echo json_encode(array('success' => false, 'email' => $data->email, 'tipe' => 'pelamar'));
			}
		}else{
			$q = $this->db->query('select idperusahaan, nama_perusahaan, verified, verified_disnaker, email from ngi_perusahaan where (email = "'.$_POST['username'].'" or username = "'.$_POST['username']. '" ) and pass = "'.md5($_POST['password']).'"');

			if($q->num_rows() > 0) {
				$data = $q->row();

				if($data->verified == 1){
					if($data->verified_disnaker == 1){
						$setdata = array(
							'is_login_perusahaan' => true,
							'id' => $data->idperusahaan,
							'nama' => $data->nama_perusahaan
						);

						$this->session->set_userdata($setdata);
						$this->db->query("update ngi_perusahaan set last_login = current_timestamp where idperusahaan= '".$data->idperusahaan."'");

						echo json_encode(array('success' => true, 'tipe' => 'perusahaan'));
					}else{
						// $this->db->insert('ngi_logerror',array('data' => json_encode($_POST), 'error' => $this->db->_error_message()));
						echo json_encode(array('success' => false, 'tipe' => 'perusahaan'));
					}
				}else{
					echo json_encode(array('success' => false, 'email' => $data->email, 'tipe' => 'perusahaan'));
				}
			}else{
				// $this->db->insert('ngi_logerror',array('data' => json_encode($_POST), 'error' => $this->db->_error_message()));
				echo json_encode(array('success' => false));
			}
		}
	}

	function logoutportal(){
		$this->session->sess_destroy();
		redirect('/portal/login');
	}

	function resetpassword(){
		$temp = md5(time());
		$password_ori = substr($temp, 0, 5);
		$password = md5($password_ori);
        
        $q = $this->db->query('select * from user_pelamar where email = "'.$_POST['username'].'" or username = "'.$_POST['username'].'"');

        if($q->num_rows() > 0){
        	$data = $q->row();

        	if(!$this->db->query('update user_pelamar set password = "'.$password.'", password_ori = "'.$password_ori.'" where iduser = "'.$data->iduser.'"')){
        		die(json_encode(array('success' => false, 'teks' => 'Terjadi kesalahan pada server, silahkan cobalagi')));
        	}
        }else{
			$q2 = $this->db->query('select * from user_pelamar where email = "'.$_POST['username'].'" or username = "'.$_POST['username'].'"');

			if($q2->num_rows() > 0){
				$data = $q->row();

				if(!$this->db->query('update ngi_perusahaan set pass = "'.$password.'", pass_ori = "'.$password_ori.'" where idperusahaan = "'.$data->idperusahaan.'"')){
        			die(json_encode(array('success' => false, 'teks' => 'Terjadi kesalahan pada server, silahkan cobalagi')));
        		}
			}else{
				die(json_encode(array('success' => false, 'teks' => 'Username/email tidak ditemukan')));
			}
        }

        $pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			 <head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title>Demystifying Email Design</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body style="margin: 0; padding: 0;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				 <tr width="100%">
				 	<td width="0%"></td>
				  <td width="100%">
				   	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
			  <tr>
			  <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
			 	<img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
				</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
			 <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
			 <b >Reset password</b>
			</td>
			 </tr>
			 <tr>
			  <td style="padding: 20px 0 30px 0;" align="center">
			  	Password akun anda berhasil direset. Silahkan menggunakan password <b>'.$password_ori.'</b> untuk bisa login. Kemudian segera ubah password anda pada menu pengaturan.
			  </td>
			 </tr>
			 </table>
			</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
			   <table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <tr>
			 <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
			 &reg; Bogor Career Center, 2019<br/>
			</td>
			  <td align="right">
			 
			</td>
			 </tr>
			</table>
			  </td>
			 </tr>
			 </table>
				  </td>
				  <td width="0%"></td>
				 </tr>
			</table>
			</body>
			</html>';

			$from_email = "noreply@bogorcareercenter.bogorkab.go.id";
			$to_email = $data->email;

			$this->load->library('email');
			$config = array();
			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
			$config['smtp_user'] = 'superkarir@bcc.disnaker.bogorkab.go.id';
			$config['smtp_pass'] = 'Superkarir99';
			$config['smtp_port'] = 465;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $this->email->set_header("Content-Type", "text/html");
			$this->email->from($from_email, 'Bogor Career Center');
	        $this->email->to($to_email);
	        $this->email->subject('Bogor Career Center Reset Password');
	        $this->email->message($pesan);
			#$result = $this->email->send();
			if($this->email->send()){
	            echo json_encode(array('success' => true, 'email' => $data->email));
	        }else{
	            echo json_encode(array('success' => false, 'teks' => 'Alamat email tidak valid, silahkan hubungi admin'));
			}
	}

	function kirimulangemail(){
		$from_email = "noreply@bogorcareercenter.bogorkab.go.id";
        $to_email = $_POST['email'];

        if($_POST['tipe'] == 'pelamar'){
        	$q = $this->db->query('select link_enkripsi from user_pelamar where email = "'.$to_email.'"');
        }else{
        	$q = $this->db->query('select link_enkripsi from ngi_perusahaan where email = "'.$to_email.'"');
        }

        if($q->num_rows() > 0){
        	$pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			 <head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title>Demystifying Email Design</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body style="margin: 0; padding: 0;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				 <tr width="100%">
				 	<td width="0%"></td>
				  <td width="100%">
				   	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
			  <tr>
			  <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
			 	<img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
				</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
			 <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
			 <b >Verifikasi alamat email</b>
			</td>
			 </tr>
			 <tr>
			  <td style="padding: 20px 0 30px 0;" align="center">
			   Silahkan verifikasi alamat email anda dengan menekan tombol dibawah. Jika tidak, anda bisa mengunjungi url berikut '.base_url().'portal/page/verifikasiemail'.$_POST['tipe'].'/'.$q->row()->link_enkripsi.'
			  </td>
			 </tr>
			 <tr>
			  <td align="center">
			   <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/page/verifikasiemail'.$_POST['tipe'].'/'.$q->row()->link_enkripsi.'">Verifikasi!</a>
			  </td>
			 </tr>
			 </table>
			</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
			   <table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <tr>
			 <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
			 &reg; Bogor Career Center, 2019<br/>
			</td>
			  <td align="right">
			 
			</td>
			 </tr>
			</table>
			  </td>
			 </tr>
			 </table>
				  </td>
				  <td width="0%"></td>
				 </tr>
			</table>
			</body>
			</html>';

			$this->load->library('email');
			$config = array();
			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
			$config['smtp_user'] = 'superkarir@cpanel1.bogorkab.go.id';
			$config['smtp_pass'] = 'Superkarir99';
			$config['smtp_port'] = 465;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $this->email->set_header("Content-Type", "text/html");
			$this->email->from($from_email, 'Bogor Career Center');
	        $this->email->to($to_email);
	        $this->email->subject('Bogor Career Center Verifikasi Email');
	        $this->email->message($pesan);
			#$result = $this->email->send();
			if($this->email->send()){
	            echo json_encode(array('success' => true));
	        }else{
	            echo json_encode(array('success' => false));
			}
        }else{
        	echo json_encode(array('success' => false));
        }
	}
	

	function verifikasiemailpelamar(){
		$this->db->query("update user_pelamar set verified = 1 where link_enkripsi='".$this->uri->segment('4')."'");
		echo "<script type='text/javascript'>alert('Email berhasil diverifikasi. Silahkan login.');top.location='".base_url()."portal/login'</script>";
	}

	function verifikasiemailperusahaan(){
		$this->db->query("update ngi_perusahaan set verified = 1 where link_enkripsi='".$this->uri->segment('4')."'");
		echo "<script type='text/javascript'>alert('Email berhasil diverifikasi. Silahkan login.');top.location='".base_url()."portal/login'</script>";

	}

	function regPendaftar(){
		$username = str_replace(" ","",$this->input->post('username',true));

		$input = array(
			'username' => $username,
			'email' => $this->input->post('email',true),
			'password' => md5($this->input->post('password',true)),
			'password_ori' => $this->input->post('password_ori',true),
			'nama' => $this->input->post('nama',true),
			'no_ktp' => $this->input->post('no_ktp',true),
			'tmp_lahir' => $this->input->post('tmp_lahir',true),
			'tgl_lahir' => $this->input->post('tgl_lahir',true),
			'gender' => $this->input->post('gender',true),
			'agama' => $this->input->post('agama',true),
			'wn' => $this->input->post('wn',true),
			'sts' => $this->input->post('sts',true),
			'pendidikan' => $this->input->post('pendidikan',true),
			// 'ins_pend' => $this->input->post('ins_pend',true),
			'jurusan' => $this->input->post('jurusan',true),
			'tahun_lulus' => $this->input->post('tahun_lulus',true),
			'kecamatan_id' => $this->input->post('kecamatan_id',true),
			'link_enkripsi' => md5($this->input->ip_address().strtotime('now')),
      		'profile_url' => time(),
      		'alamat' => $this->input->post('alamat',true),
      		'no_telp' => $this->input->post('no_telp',true)
		);

		if($this->db->insert('user_pelamar',$input)){
			$from_email = "noreply@bogorcareercenter.bogorkab.go.id";
        	$to_email = $_POST['email'];

			$pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			 <head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title>Demystifying Email Design</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body style="margin: 0; padding: 0;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				 <tr width="100%">
				 	<td width="0%"></td>
				  <td width="100%">
				   	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
			  <tr>
			  <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
			 	<img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
				</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
			 <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
			 <b >Verifikasi alamat email</b>
			</td>
			 </tr>
			 <tr>
			  <td style="padding: 20px 0 30px 0;" align="center">
			   Silahkan verifikasi alamat email anda dengan menekan tombol dibawah. Jika tidak, anda bisa mengunjungi url berikut '.base_url().'portal/page/verifikasiemailpelamar/'.md5($this->input->ip_address().strtotime('now')).'
			  </td>
			 </tr>
			 <tr>
			  <td align="center">
			   <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/page/verifikasiemailpelamar/'.md5($this->input->ip_address().strtotime('now')).'">Verifikasi!</a>
			  </td>
			 </tr>
			 </table>
			</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
			   <table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <tr>
			 <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
			 &reg; Bogor Career Center, 2019<br/>
			</td>
			  <td align="right">
			 
			</td>
			 </tr>
			</table>
			  </td>
			 </tr>
			 </table>
				  </td>
				  <td width="0%"></td>
				 </tr>
			</table>
			</body>
			</html>';

			$this->load->library('email');
			$config = array();
			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
			$config['smtp_user'] = 'superkarir@cpanel1.bogorkab.go.id';
			$config['smtp_pass'] = 'Superkarir99';
			$config['smtp_port'] = 465;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $this->email->set_header("Content-Type", "text/html");
			$this->email->from($from_email, 'Bogor Career Center');
	        $this->email->to($to_email);
	        $this->email->subject('Bogor Career Center Verifikasi Email');
	        $this->email->message($pesan);
			#$result = $this->email->send();
			// if($this->email->send()){
	   //          $email 	= $_POST['email'];
				// $tipe 	= 'pelamar';

				// $dokuform="<form name=\"param_pass\" id=\"param_pass\" method=\"post\" action=\"".base_url()."portal/verifikasi\">\n"; //example redirect link

				// $dokuform.="<input name=\"email\" type=\"hidden\" id=\"order_number\" value=\"$email\">\n";
				// $dokuform.="<input name=\"tipe\" type=\"hidden\" id=\"order_number\" value=\"$tipe\">\n";
				// $dokuform.="<input name=\"".$this->security->get_csrf_token_name()."\" type=\"hidden\" id=\"order_number\" value=\"".$this->security->get_csrf_hash()."\">\n";

				// $dokuform.="</form>\n";
				// $dokuform.="<script language=\"JavaScript\" type=\"text/javascript\">";
				// $dokuform.="document.getElementById('param_pass').submit();";
				// $dokuform.="</script>";

				// //FIREFOX FIX
				// $redirect_url = str_replace('&amp;', '&', $redirect_url);

				// printf($dokuform);
				echo "<script type='text/javascript'>alert('Berhasil mendaftarkan akun. Silahkan login.');top.location='".base_url()."portal/login'</script>";

	  //       }else{
	  //       	$this->db->query('delete from user_pelamar where link_enkripsi = '.md5($this->input->ip_address().strtotime('now')));
	  //           echo "<script type='text/javascript'>alert('Gagal mendaftarkan akun. Alamat email tidak valid.');top.location='".base_url()."portal/register'</script>";
			// }
		}else{
    		echo "<script type='text/javascript'>alert('Gagal mendaftarkan akun. Silahkan coba lagi.');top.location='".base_url()."portal/register'</script>";
		}
	}
	
	function regPerusahaan(){
		$username = str_replace(" ","",$this->input->post('username',true));
		//$newDate = date("Y-m-d",strtotime($this->input->post('initialDate')));

		$link_enkripsi = md5($this->input->ip_address().strtotime('now'));
		$input = array(
			'username' => strtolower($username),
			'email' => $this->input->post('email',true),
			'pass' => md5($this->input->post('pass',true)),
			'pass_ori' => $this->input->post('password_ori',true),
			'nama_perusahaan' => $this->input->post('nama_perusahaan',true),
			'alamat' => $this->input->post('alamat',true),
			'telp' => $this->input->post('telp',true),
			'noktpdir' => $this->input->post('noktpdir',true),
			'nm_dir' => $this->input->post('nm_dir',true),
			'no_hp' => $this->input->post('no_hp',true),
			'lokasi' => $this->input->post('lokasi',true),
			'profile_url' =>  time(),
			'jenisusaha' => $this->input->post('jenisusaha',true),
			'province_id' => $this->input->post('province_id',true),
			'kab_id' => $this->input->post('kab_id',true),
			'kec_id' => $this->input->post('kec_id',true),
			'link_enkripsi' => $link_enkripsi
		);


		if($this->db->insert('ngi_perusahaan',$input)){
			$from_email = "noreply@bogorcareercenter.bogorkab.go.id";
        	$to_email = $_POST['email'];

			$pesan = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			 <head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title>Demystifying Email Design</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body style="margin: 0; padding: 0;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				 <tr width="100%">
				 	<td width="0%"></td>
				  <td width="100%">
				   	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
			  <tr>
			  <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
			 	<img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
				</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
			 <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
			 <b >Verifikasi alamat email</b>
			</td>
			 </tr>
			 <tr>
			  <td style="padding: 20px 0 30px 0;" align="center">
			   Silahkan verifikasi alamat email anda dengan menekan tombol dibawah. Jika tidak, anda bisa mengunjungi url berikut '.base_url().'portal/page/verifikasiemailperusahaan/'.$link_enkripsi.'
			  </td>
			 </tr>
			 <tr>
			  <td align="center">
			   <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="'.base_url().'portal/page/verifikasiemailperusahaan/'.$link_enkripsi.'">Verifikasi!</a>
			  </td>
			 </tr>
			 </table>
			</td>
			 </tr>
			 <tr>
			  <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
			   <table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <tr>
			 <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
			 &reg; Bogor Career Center, 2019<br/>
			</td>
			  <td align="right">
			 
			</td>
			 </tr>
			</table>
			  </td>
			 </tr>
			 </table>
				  </td>
				  <td width="0%"></td>
				 </tr>
			</table>
			</body>
			</html>';

			$this->load->library('email');
			$config = array();
			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['smtp_host'] = 'ssl://cpanel1.bogorkab.go.id';
			$config['smtp_user'] = 'superkarir@cpanel1.bogorkab.go.id';
			$config['smtp_pass'] = 'Superkarir99';
			$config['smtp_port'] = 465;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $this->email->set_header("Content-Type", "text/html");
			$this->email->from($from_email, 'Bogor Career Center');
	        $this->email->to($to_email);
	        $this->email->subject('Bogor Career Center Verifikasi Email');
	        $this->email->message($pesan);
			#$result = $this->email->send();
			// if($this->email->send()){
	  //           $email 	= $_POST['email'];
			// 	$tipe 	= 'perusahaan';

			// 	$dokuform="<form name=\"param_pass\" id=\"param_pass\" method=\"post\" action=\"".base_url()."portal/verifikasi\">\n"; //example redirect link

			// 	$dokuform.="<input name=\"email\" type=\"hidden\" id=\"order_number\" value=\"$email\">\n";
			// 	$dokuform.="<input name=\"tipe\" type=\"hidden\" id=\"order_number\" value=\"$tipe\">\n";
			// 	$dokuform.="<input name=\"".$this->security->get_csrf_token_name()."\" type=\"hidden\" id=\"order_number\" value=\"".$this->security->get_csrf_hash()."\">\n";

			// 	$dokuform.="</form>\n";
			// 	$dokuform.="<script language=\"JavaScript\" type=\"text/javascript\">";
			// 	$dokuform.="document.getElementById('param_pass').submit();";
			// 	$dokuform.="</script>";

			// 	//FIREFOX FIX
			// 	$redirect_url = str_replace('&amp;', '&', $redirect_url);

			// 	printf($dokuform);
	  //       }else{
	  //       	$this->db->query('delete from ngi_perusahaan where link_enkripsi = '.$link_enkripsi);
	  //           echo "<script type='text/javascript'>alert('Gagal mendaftarkan akun. Alamat email tidak valid.');top.location='".base_url()."portal/register_perusahaan'</script>";
			// }
			echo "<script type='text/javascript'>alert('Berhasil mendaftarkan akun. Silahkan login.');top.location='".base_url()."portal/login'</script>";
		}else{
			echo "<script type='text/javascript'>alert('Gagal mendaftarkan akun. \nSilahkan coba lagi.');top.location='".base_url()."portal/register_perusahaan'</script>";
		}
	}	
}
