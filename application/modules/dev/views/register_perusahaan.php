<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
<script src="<?=base_url()?>template/HTML/job_light/js/custom_II.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" /> 
<style type="text/css">

  
 .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
} 
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: rgb(24, 73, 129);
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #184981;
    border-color: #184981;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    z-index: 2;
    color: #fff;
    border: 1px solid #184981;
    background-color: #184981;
    border-color: none;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_bottom_cont_wrapper {
    float: left;
    width: 100%;
    background: #f9f9f9;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-bottom: 1px solid #184981;
    border-top: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
.mainmenu ul li a {

    padding: 0px 7px 47px 10px;
    }
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_hiring_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_hiring_heading_wrapper h2:before{
    content:'';
    border:1px solid #184981;
    width:8px;
    position:absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
  
    .login_wrapper button.btn {
        color: #fff;
        width: 100%;
        height: 50px;
        padding: 6px 25px;
        line-height: 36px;
        margin-bottom: 20px;
        text-align: left;
        border-radius: 8px;
        background-color: #337AB8;
        font-size: 16px;
        border: 1px solid #f9f9f9;
        text-align: center;
        text-transform: uppercase;
    }

   
    .login_wrapper button.btn {
    color: #fff;
    width: 100%;
    height: 50px;
    padding: 6px 25px;
    line-height: 36px;
    margin-bottom: 20px;
    text-align: left;
    border-radius: 8px;
    background-color: #184981;
    font-size: 16px;
    border: 1px solid #f9f9f9;
    text-align: center;
    text-transform: uppercase;
}
.register_tab_wrapper .register-tabs>li.active::after {
    content: "";
    position: absolute;
    left: 50%;
    bottom: -5px;
    margin-left: -10px;
    border-top: 5px solid #184981;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
}
.register_tab_wrapper .register-tabs>li.active>a, .register_tab_wrapper .register-tabs>li.active>a:focus, .register_tab_wrapper .register-tabs>li.active>a:hover {
    color: #fff;
    background-color: #184981;
    border: 0;
}
  .cek {
        border: 1px solid #f9f9f9;
        background: #f9f9f9;
        color: #6a7c98;
        font-size: 17px;
		vertical-alling: middle;
    }


.register_left_form input[type="text"], .register_left_form input[type="email"], .register_left_form input[type="password"], .register_left_form input[type="tel"], .register_left_form input[type="number"], .register_left_form input[type="url"], .register_left_form input, .register_left_form select, .register_left_form textarea {
	text-transform: none;
}
.mainmenu ul li a {

	padding: 0px 7px 47px 10px;
	}
    .jp_pricing_cont_heading h2:after {
    content: '';
    border: 1px solid #184981;
    width: 30px;
    position: absolute;
    bottom: -15px;
    left: 11px;
}
.jp_pricing_cont_heading h2:before {
    content: '';
    border: 1px solid #184981;
    width: 8px;
    position: absolute;
    bottom: -15px;
    left: 0;
}
</style>
<script>

   
$(document).ready(function(){
	
	
  $(document).on('change', 'select[name="province_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikab',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kab_id"]').html(result);
                }
            })
        }).on('change', 'select[name="kab_id"]', function(){
            var value = $(this).val();

            $.ajax({
                url: '<?=base_url()?>perusahaan/page/carikec',
                type: 'post',
                data: {
                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                    id: value,
                    ids: ''
                },
                success: function(result){
                    $('select[name="kecamatan_id"]').html(result);
                }
            })
        }).on('input', '#username', function(){
            $('#username_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();
            var regexp = /[A-Za-z0-9]{3,}$/

            if(value.match(regexp)){
            	$.ajax({
	                url: '<?=base_url()?>portal/page/checkusername_perusahaan',
	                type: 'post',
	                dataType: 'json',
	                data: {
	                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
	                    username: value
	                },
	                success: function(result){
	                    if(result.available){
	                        $('#username_check').html('<i class="fa fa-check"></i>');
	                        $('#regBtn').attr('disabled',false);
	                    }else{
	                        $('#username_check').html('<i class="fa fa-times"></i>');
	                        $('#regBtn').attr('disabled',true);
	                    }
	                }
	            });
            }else{
				$('#username_check').html('<i class="fa fa-times"></i>');
	            $('#regBtn').attr('disabled',true);
            }
        }).on('input', '#password_ori', function(){
			$('#password_check').html('<i class="fa fa-spinner fa-spin"></i>');
			
            if($(this).val() == $('#pass').val()){
				$('#password_check').html('<i class="fa fa-check"></i>');
                $('#regBtn').attr('disabled',false);
            }else{
				$('#password_check').html('<i class="fa fa-times"></i>');
                $('#regBtn').attr('disabled',true);
            }
        }).on('input', '#email', function(){
            $('#email_check').html('<i class="fa fa-spinner fa-spin"></i>');

            var value = $(this).val();            
            var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            if(value.match(regexp)){
                 $.ajax({
                    url: '<?=base_url()?>portal/page/checkemail',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
                        email: value
                    },
                    success: function(result){
                        if(result.available){
                            $('#email_check').html('<i class="fa fa-check"></i>');
                            $('#submit_btn').attr('disabled',false);
                        }else{
                            $('#email_check').html('<i class="fa fa-times"></i>');
                            $('#submit_btn').attr('disabled',true);
                        }
                    }
                });
            }else{
                $('#email_check').html('<i class="fa fa-times"></i>');
                $('#submit_btn').attr('disabled',true);
            }
        });

//alert('1');

});



</script>
 <div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Registrasi Perusahaan</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="<?=base_url()?>">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Registrasi Perusahaan</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp register wrapper start -->
    <div class="register_section">
        <!-- register_form_wrapper -->
        <div class="register_tab_wrapper">
            <div class="container">
                <div class="row">
				
                    <div class="col-md-10 col-md-offset-1">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_pricing_cont_heading">
                        <h2>Additional Information :</h2>
                    </div>
                    <div class="jp_pricing_cont_wrapper">
                        <p>1. Akun ini akan digunakan untuk keperluan anda memberikan informasi lowongan pekerjaan, penempatan kerja .<br>
						2. Setelah pendaftaran Akun selesai anda harus datang ke disnaker untuk verifikasi data dengan membawa kelengkapan sbb :
						<br> &nbsp; - Cap Perusahaan
						<br> &nbsp; - Surat Permohonan Pengajuan Kebutuhan Tenaga Kerja Dari Perusahaan Yang Ditujukan Kepada Disnaker Kabupaten Bogor
						<br> &nbsp; - KTP Penanggung Jawab Perusahaan
						<br> &nbsp; - Fotocopy SIUP
						<br>3. Setelah verifikasi data selesai maka administrator disnaker akan mengaktifkan akun anda.</p>
						<br>
                    </div>
				</div>
                        <div role="tabpanel">

                            <!-- Nav tabs -->
                            <ul id="tabOne" class="nav register-tabs">
                                <li class="active"><a href="#contentOne-1" data-toggle="tab">Perusahaan<br> <span>Form Pendaftaran Akun Perusahaan</span></a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active register_left_form" id="contentOne-1">
									
                                    <div class="jp_regiter_top_heading">
                                        <p><b>AKUN LOGIN PERUSAHAAN</b></p>
                                    </div>
								 <!--<form action="<?=base_url()?>portal/page/regPerusahaan" method="POST" id="daftarperusahaan">-->
								 <form id="formperusahaan" action="<?=base_url()?>portal/page/regPerusahaan" method="post">
									<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
									<div class="row">
                                        <!--Form Group-->
										<div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <input type="text" name="email" id="email" placeholder="Email*" required="">
                                                <span class="input-group-addon cek" id="email_check"></span>
                                            </div>
                                        </div> 
                                       <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <input type="text" name="username" id="username" placeholder="Username*" pattern="[A-Za-z0-9]{3,}" title="Hanya huruf dan angka tanpa spasi, 3 - 10 karakter" required="">
                                                <span class="input-group-addon cek" id="username_check"></span>
                                            </div>
                                        </div> 
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                            <input type="password" name="pass" id="pass" placeholder=" password*" required="">
                                        </div>
										 <div class="form-group col-md-6 col-sm-6 col-xs-12">
											<div class="input-group">
                                                <input type="password" name="password_ori" id="password_ori" placeholder="Konfirmasi password*" required="">
                                                <span class="input-group-addon cek" id="password_check"></span>
                                            </div>
                                        </div>
                                    </div>
									<div class="jp_regiter_top_heading">
                                        <p><b>DATA PERUSAHAAN</b></p>
                                    </div>
                                    <div class="row">
                                        <!--Form Group-->
										<div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="nama_perusahaan" id="nama_perusahaan" placeholder="Nama Perusahaan*" required="">
                                        </div>
										<div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="alamat" id="alamat" placeholder="Alamat Perusahaan*" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="telp" id="telp" placeholder="No. Telp*" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="noktpdir" id="noktpdir" placeholder="No. Ktp Penanggung Jawab*"  pattern="[0-9]{16,16}" title="No ktp harus berupa angka, 16 digit" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="nm_dir" id="nm_dir" placeholder="Nama Lengkap Penanggung Jawab*" required="">
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <input type="text" name="no_hp" id="no_hp" placeholder="No Hp Penanggung Jawab*" required="">
                                        </div>
                                        <!--Form Group-->
                                       
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
											<select name="lokasi" id="lokasi" required="">
												<option value="-">Pusat/Cabang*</option>
												<option value="Pusat">Pusat</option>
												<option value="Cabang">Cabang</option>
											</select>
										
                                        </div>
										<div class="form-group col-md-4 col-sm-4 col-xs-12">
										<select name="jenisusaha" required="">
											<option value="">Jenis Usaha*</option>
                                    <?php
                                        $rs = $this->db->query("select * from mast_jenisusaha")->result();

                                        foreach ($rs as $item) {
                                            if(isset($_POST)){
                                                if($_POST['jenisusaha'] == $item->id){
                                                    echo '<option value="'.$item->id.'" selected="true">'.$item->nama.'</option>';
                                                }else{
                                                    echo '<option value="'.$item->id.'">'.$item->nama.'</option>';    
                                                }
                                            }else{
                                                echo '<option value="'.$item->id.'">'.$item->nama.'</option>';
                                            }
                                        }
                                    ?>
										</select>
                                        </div>
                                   
                                  <div class="form-group col-md-4 col-sm-4 col-xs-12">
    										<select name="province_id" required="">
    											<option value="">Provinsi</option>
                                                <?php
                                                    $q = $this->db->query('select province, province_id from ngi_kecamatan group by province_id')->result();

                                                    foreach ($q as $i) {
                                                        echo '<option value="'.$i->province_id.'">'.$i->province.'</option>';
                                                    }

                                                ?>
    										</select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <select name="kab_id" required="">
                                                <option value="">Kabupaten / Kota</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                            <select name="kecamatan_id" required="">
                                                <option value="">Kecamatan / Kelurahan</option>
                                            </select>
                                        </div>
                                
						</div><!--end of row-->
							</br>
									<div class="login_btn_wrapper register_btn_wrapper login_wrapper">
										<button class="btn btn-primary btn-lg btn-block" type="submit" id="regBtn">Buat akun</button>
									</div>
									</form>
									
                                    <div class="login_message">
                                        <p>Sudah Mempunyai Akun? <a href="<?=base_url()?>portal/login" style="color: #164980;"> Login Disini </a> </p>
                                    </div>
									
                                </div>
								
                               

                            </div>
                            <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that you logout to prevent any un-authorized access to your account</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

