   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />
   <style type="text/css">
   	
 .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
} 
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: rgb(24, 73, 129);
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #184981;
    border-color: #184981;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    z-index: 2;
    color: #fff;
    border: 1px solid #184981;
    background-color: #184981;
    border-color: none;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_bottom_cont_wrapper {
    float: left;
    width: 100%;
    background: #f9f9f9;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-bottom: 1px solid #184981;
    border-top: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
.mainmenu ul li a {

    padding: 0px 7px 47px 10px;
    }
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_hiring_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_hiring_heading_wrapper h2:before{
    content:'';
    border:1px solid #184981;
    width:8px;
    position:absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
  .login_wrapper a.btn {
    color: #fff;
    width: 100%;
    height: 50px;
    padding: 6px 25px;
    line-height: 36px;
    margin-bottom: 20px;
    text-align: center;
    border-radius: 8px;
    background: #174980;
    font-size: 16px;
    border: 1px solid #3c97af;
}

   </style>

<?php if($this->session->userdata('is_login_pelamar')){
	redirect(base_url().'portal');
}else if($this->session->userdata('is_login_perusahaan')){
	redirect(base_url().'perusahaan');
}else{ ?>


<div class="login_section">
		<!-- login_form_wrapper -->
		<div class="login_form_wrapper">
			<div class="container">
				<div class="row">
					<?php echo $this->session->flashdata('email_sent'); ?>
					<div class="col-md-8 col-md-offset-2">
						<!-- login_wrapper -->
						<h1>Masuk ke akun anda</h1>
							<form id="formlogin" action="<?=base_url()?>portal/page/loginportal" method="post">
								<div class="login_wrapper">
									<!-- <div class="row">
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
											<a href="#" class="btn btn-primary"> <span>Login with Facebook</span> <i class="fa fa-facebook"></i> </a>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
											<a href="#" class="btn btn-primary google-plus"> Login  with Google <i class="fa fa-google-plus"></i> </a>
										</div>
									</div>
									<h2>or</h2> -->
									<div class="formsix-pos">
										<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
										<div class="form-group i-email">
											<input type="email" class="form-control" name="username" id="email2" placeholder="Email/Username*" required />
										</div>
									</div>
									<div class="formsix-e">
										<div class="form-group i-password">
											<input type="password" class="form-control" name="password" id="password2" placeholder="Password *" required />
										</div>
									</div>
									<div class="login_remember_box" style="margin-top: 0px !important; margin-bottom: 50px !important;">
										<!-- <label class="control control--checkbox">Ingat saya
											<input type="checkbox">
											<span class="control__indicator"></span>
										</label> -->
										<a href="<?=base_url()?>portal/lupapassword" class="forget_password">
											Lupa password
										</a>
									</div>
									<div class="login_btn_wrapper">
										<a class="btn btn-primary login_btn" id="login"> Login </a>
									</div>
									<div class="login_message">
										<p>Tidak punya akun ? <a href="<?=base_url()?>portal/register" style="color: #164980;"> Daftar sekarang </a> </p>
									</div>
								</div>
							</form>
						<!-- /.login_wrapper-->
					</div>
				</div>
			</div>
		</div>
		<!-- /.login_form_wrapper-->
	</div>

	<script type="text/javascript">
		$(function(){
      $('#email2').focus();

			$('#login').click(function(){
				$('#login').attr('disabled',true);
				$('#login').html('Loading....');
				$('#formlogin').submit();
			});

			$(document).on('submit','#formlogin', function(e){
				$.ajax({
					url : $(this).prop('action'),
					data : $(this).serialize(),
					dataType: 'json',
					method : 'post',
					success: function(rs){
						if(rs.success){
							if(rs.tipe == 'pelamar'){
								window.location = '<?=(($_POST['url'] == "")? base_url() : $_POST['url'])?>';
							}else{
								window.location = '<?=(($_POST['url'] == "")? base_url()."perusahaan" : $_POST['url'])?>';
							}
						}else{
							if(rs.tipe == undefined) {
								alert('Email/username atau password salah. Silahkan coba lagi.');
								$('#login').attr('disabled',false);
								$('#login').html('login');
							}else{
								if(rs.email == undefined) {
									alert('Silahkan lakukan verifikasi ke kantor disnaker dengan membawa berkas-berkas yang berkaitan.');
									$('#login').attr('disabled',false);
									$('#login').html('login');
								}else{
								 	post('<?=base_url()?>portal/verifikasi', {
					                    <?=$this->security->get_csrf_token_name()?> : '<?=$this->security->get_csrf_hash()?>',
					                    email : rs.email,
					                    tipe : rs.tipe
					                }, "post");
								}
							}
						}
					},
					error: function(e){
						alert('Terjadi error pada server. Silahkan coba lagi');
						$('#login').attr('disabled',false);
						$('#login').html('login');
					}
				});

				e.preventDefault();
				// $('#login').attr('disable',false);
				// $('#login').html('Loading....');
			})
		})

		function post(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }

	</script>

<?php } ?>