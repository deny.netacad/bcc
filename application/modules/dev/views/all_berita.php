<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/magnific-popup.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" /> 
<style>

 .gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
 .jp_tittle_img_overlay {
    position: absolute;
    top: 0%;
    right: 0%;
    left: 0%;
    bottom: 0%;
    background: #184981;
} 
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: rgb(24, 73, 129);
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #184981;
    border-color: #184981;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    z-index: 2;
    color: #fff;
    border: 1px solid #184981;
    background-color: #184981;
    border-color: none;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_first_blog_bottom_cont_wrapper {
    float: left;
    width: 100%;
    background: #f9f9f9;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-bottom: 1px solid #184981;
    border-top: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
.mainmenu ul li a {

	padding: 0px 7px 47px 10px;
	}
        .mainmenu .gc_main_navigation.parent:after,
.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega > a:before,
.mainmenu ul li.has-mega > a:after {
    content: "";
    width: 11px;
    height: 1px;
    background-color: #184981 !important;
    top: 20px;
    position: absolute;
    left:0;
    right: 0;
    margin:0px auto;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.mainmenu .gc_main_navigation.parent:before,
.mainmenu ul li.has-mega a:before {
    width: 0px;
    top: 50%;
    left:-40px;
}
.mainmenu .gc_main_navigation.parent:hover:before,
.mainmenu ul li.has-mega:hover > a:before {
    width: 10px;
    top: 50%;
}
.mainmenu .gc_main_navigation.parent:after,
.mainmenu ul li.has-mega a:after {
    width: 0px;
    top: 50%;
    left:5px;
}
.mainmenu .gc_main_navigation.parent:hover:after,
.mainmenu ul li.has-mega:hover > a:after {
    width: 30px;
    top: 50%;
}
.gc_main_navigation {
    color: #184981 !important;
}
.gc_main_navigation{
    color:#184981 !important;
}
.mainmenu ul li:hover .gc_main_navigation,
.mainmenu ul li.active > .gc_main_navigation {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    color:#3b6392 !important;
}
.nav > li > a:focus,
.nav > li > a:hover {
    background-color: #3b6392;
}
.menu-bar:after,
.menu-bar:before {
    background-color: #3b6392;
    content: "";
    height: 2px;
    position: absolute;
    right: 0;
    top: 8px;
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    width: 30px;
}
.mainmenu ul li:hover > ul,
.mainmenu ul li:hover > .mega-menu {
    opacity: 1;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    visibility: visible;
    top: 100%;
    background:#0e142773;
    border-top: 2px solid #3b6392;
}
.mainmenu ul ul li {
    display: block;
}
.mainmenu ul ul li a,
.mainmenu ul li.has-mega .mega-menu span a {
    color: #222;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding: 8px 10px 8px 20px;
    border-bottom: 1px solid #ffffff52;
}
.jp_hiring_heading_wrapper h2:after{
    content:'';
    border: 1px solid #184981;
    width:30px;
    position:absolute;
    bottom: -15px;
    left: 11px;
}
.jp_hiring_heading_wrapper h2:before{
    content:'';
    border:1px solid #184981;
    width:8px;
    position:absolute;
    bottom: -15px;
    left: 0;
}
.jp_navi_right_btn_wrapper li:last-child a:hover{
    background:#184981;
    border:1px solid #ffffff40;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
 .jp_add_resume_wrapper {
    width: 100%;
    height: 100%;
    background-position: center 0;
    background-size: cover;
    background-repeat: no-repeat;
    position: relative;
    text-align: center;
    padding-left: 5px;
    padding-right: 5px;
}
.mainmenu ul li.has-mega .mega-menu span a:hover{
    background-color: #00468c;
    color:#ffffff !important;
}
.mainmenu ul ul li:last-child > a {
    border-bottom: 0px;
}
.mainmenu ul ul li:hover > a {
    background-color: #3b6392;
    padding-left: 23px;
    color:#ffffff !important;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.jp_top_jobs_category_wrapper:hover{
    background:#184981;
    box-shadow: 0px 0px 37px #00000091;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}
.jp_top_jobs_category i {
    color: #f9f9f9;
    font-size: 25px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.mainmenu ul li a{
        text-transform: none !important;
        color: #000000;
        display: block;
        font-size: 14px;
        letter-spacing: 1px;
        padding: 0px 8px 47px 10px;
        text-transform: capitalize;
        font-family: 'Montserrat', sans-serif;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        position: relative;
        }
        .jp_footer_candidate ul li a {
        text-transform: none;
        }
        
.dropbtn {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  border: none;
  border-radius: 10px;
    width: 120px;
    height: 50px;
    line-height: 50px;

}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  display: inline-block;
  padding-right: 30px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
  
</style>
 <?php
    $row = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.position= 0 and menu=0 and a.art_thumb !='' and a.art_intro !='' order by a.art_date desc")->num_rows();
    $maxrow = (int)($row/1) + 3;
    $page = $this->uri->segment(4);
    $range = ($page - 1)*3;

    if ($page == ''){
        header("Location: /portal/page/all_berita/1");
        exit;
    }

    if ($page > $maxrow){
        header("Location: /portal/page/all_berita/$maxrow");
        exit;
    }else if($page == $maxrow){
         $paging = ($range+1)." - ".($row);
    }else{
         $paging = ($range+1)." - ".($page*3);
    }
 ?>
<div class="jp_tittle_main_wrapper">
        <div class="jp_tittle_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_tittle_heading_wrapper">
                        <div class="jp_tittle_heading">
                            <h2>Berita & Artikel</h2>
                        </div>
                        <div class="jp_tittle_breadcrumb_main_wrapper">
                            <div class="jp_tittle_breadcrumb_wrapper">
                                <ul>
                                    <li><a href="<?=base_url()?>">Home</a> <i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">Pages</a> <i class="fa fa-angle-right"></i></li>
                                    <li>Berita & Artikel</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Tittle Wrapper End -->
    <!-- jp blog_cate section Wrapper Start -->
    <div class="jp_blog_cate_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
					
					  <?php
                       $rs = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.position= 0 and a.art_thumb !='' and a.art_intro !='' order by a.art_date desc limit $range,3");
                        foreach($rs->result() as $item){
                        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                        $src = $xpath->evaluate("string(//img/@src)");
                        $src=str_replace('.thumbs','',$src);
						?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_blog_cate_left_main_wrapper">
                                <div class="jp_first_blog_post_main_wrapper">
                                    <div class="jp_first_blog_post_img">
                                        <img src="<?=$src?>" class="img-responsive" alt="<?=$item->art_title?>" />
                                    </div>
                                    <div class="jp_first_blog_post_cont_wrapper">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-calendar"></i> <?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($item->tmstamp)); ?></a></li>
                                            <li><a href="#"><i class="fa fa-user"></i> &nbsp;&nbsp;<?=$item->nama?></a></li>
                                        </ul>
                                        <h3><a href="<?=base_url()?>portal/page/berita/<?=$item->art_seo?>" name="<?=$item->art_id?>" class="counting"><?=$item->art_title?></a></h3>
                                        <p><?=($item->art_intro)?></p>
                                    </div>
                                    <div class="jp_first_blog_bottom_cont_wrapper">
                                       
                                        <div class="jp_blog_bottom_right_cont">
                                            <!-- <p class="hidden-xs"><a href="#" class="hidden-xs"><i class="fa fa-comments"></i></a></p>
                                            <ul>
                                                <li>SHARE :</li>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li class="hidden-xs"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li class="hidden-xs"><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                            </ul> -->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          <?php } ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
						<!-- <p>Showing: <?=$paging?> of <?php echo $row ?></p> -->
                            <div class="pager_wrapper gc_blog_pagination">
							 <?php if ($page == 1): ?>
                                <ul class="pagination">
									<li class="hidden"><a href="#">Prev.</a></li>
                                    <li class="active"><a href="<?=base_url()?>portal/page/all_berita/1">1</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/2">2</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/3">3</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/4">4</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page+1?>">Next</a></li>
                                </ul>
							 <?php elseif ($page == 2): ?>
                                <ul class="pagination">
									<li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-1?>">Prev.</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/1">1</a></li>
                                    <li class="active"><a href="<?=base_url()?>portal/page/all_berita/2">2</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/3">3</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/4">4</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page+1?>">Next</a></li>
                                </ul>
								 <?php elseif ($page == 3): ?>
                                <ul class="pagination">
									<li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-1?>">Prev.</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/1">1</a></li>
                                    <li ><a href="<?=base_url()?>portal/page/all_berita/2">2</a></li>
                                    <li class="active"><a href="<?=base_url()?>portal/page/all_berita/3">3</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/4">4</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page+1?>">Next</a></li>
                                </ul>
								<?php elseif ($page == 4): ?>
                                <ul class="pagination">
									<li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-1?>">Prev.</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/1">1</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/2">2</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/3">3</a></li>
                                    <li class="active"><a href="<?=base_url()?>portal/page/all_berita/4">4</a></li>
                                    <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page+1?>">Next</a></li>
                                </ul>
								 <?php elseif ($page == $maxrow): ?>
                               <ul class="pagination">
                        <li>
                            <a href="<?=base_url()?>portal/page/all_berita/<?=$page-1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-3?>"><?=$page-3?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-2?>"><?=$page-2?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-1?>"><?=$page-1?></a></li>
                        <li class="active"><a href="<?=base_url()?>portal/page/all_berita/<?=$page?>"><?=$page?></a></li>
                    </ul>
							 <?php else: ?>
                        <ul class="pagination">
                        <li>
                            <a href="<?=base_url()?>portal/page/all_berita/<?=$page-1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-2?>"><?=$page-2?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page-1?>"><?=$page-1?></a></li>
                        <li class="active"><a href="<?=base_url()?>portal/page/all_berita/<?=$page?>"><?=$page?></a></li>
                        <li><a href="<?=base_url()?>portal/page/all_berita/<?=$page+1?>"><?=$page+1?></a></li>
                        <li>
                            <a href="<?=base_url()?>portal/page/all_berita/<?=$page+1?>" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                    </ul>
                    <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="jp_first_right_sidebar_main_wrapper">
                    <div class="row">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont" style="background-color: white;">
                                    <img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:138px;height:75px;" title="Bogor Career Center" >
                                     <?php
                                        $rs = $this->db->query("select narasi from section_contact");
                                        $seo = $rs->row();

                                        ?>
                                    <h4 style="font-size:14px; text-align:center;color: black;">
                                        <?=$seo->narasi?>
                                    </h4>
                                    <ul>
                                        <li><a href="<?=base_url()?>portal/register_perusahaan" style="font-size:12px;"><i class="fa fa-plus-circle"></i> &nbsp;DAFTAR PERUSAHAAN</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_spotlight_main_wrapper">
                                <div class="spotlight_header_wrapper">
                                    <h4 style="font-size:12px;">Rekomendasi Lowongan</h4>
                                </div>
                                <div class="jp_spotlight_slider_wrapper">
                                    <div class="owl-carousel owl-theme">
                                    <?php 

                                            if($this->session->userdata('is_login_pelamar')){
                                                $person = $this->db->query('select skill from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();

                                                $skill = explode(',', $person->skill);

                                                $where = "";
                                                foreach ($skill as $s) {
                                                    $where .= " or a.skill like '%".$s."%'";
                                                }

                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ORDER BY RAND() LIMIT 5");
                                            }else{
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5");
                                            }

                                            if($joblist->num_rows() == 0 ){
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
                                                left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5");
                                            }

                                            foreach ($joblist ->result() as $item) {

                                            ?>
                                    
                                        <div class="item">
                                            <div class="jp_spotlight_slider_img_Wrapper">
                                                <img src="<?=base_url()?>template/HTML/job_light/images/content/spotlight.jpg" alt="spotlight_img" />
                                            </div>
                                            <div class="jp_spotlight_slider_cont_Wrapper">
                                                <h4><?=$item->nama_job?></h4>
                                                <p><?=$item->nama_perusahaan?></p>
                                                <ul>
                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->type?> <?=$item->city?>, <?=$item->province?></li>
                                                </ul>
                                            </div>
                                            <div class="jp_spotlight_slider_btn_wrapper">
                                                <div class="jp_spotlight_slider_btn">
                                                    <ul>
                                                        <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>"><i class="fa fa-plus-circle"></i> &nbsp;DETAIL</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }?>
                                    
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                    
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_spotlight_main_wrapper">
                                <div class="spotlight_header_wrapper">
                                    <h4 style="font-size:14px;">Pelatihan & Event</h4>
                                </div>
                                <div class="jp_spotlight_slider_wrapper">
                                    <div class="owl-carousel owl-theme">
                                         <?php
                                        //atiati karo iki wa. mau mati kabeh sak halaman;
                               
                                        $this->load->database('default', FALSE);
                                        $this->db2 = $this->load->database('pelatihan', TRUE);
                                        
                                        $pelatihan = $this->db2->query("SELECT a.*,DATE_FORMAT(a.tgl_mulai,'%d %M') AS tglmulai,DATE_FORMAT(a.tgl_ahir,'%d %M %Y') AS tglakhir FROM ref_pelatihan a ");
                                        
                                        $this->load->database('pelatihan', FALSE);
                                        $this->load->database('default', TRUE);
                                        
                                       // echo("test");
                                        foreach ($pelatihan->result() as $items) {
                                        ?>
                                        <div class="item">
                                            <div class="jp_spotlight_slider_img_Wrapper">
                                                <img src="<?=base_url()?>template/HTML/job_dark/images/content/resume_bg_1.jpg" alt="spotlight_img" />
                                            </div>
                                            <div class="jp_spotlight_slider_cont_Wrapper">
                        
                                                <p><?=$items->judul?></p>
                                                <ul>
                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?=$items->tempat?></li>
                                                    <li><i class="fa fa-calendar-o"></i>&nbsp;  <?=$items->tglmulai?> s/d <?=$items->tglakhir?></li>
                                                </ul>
                                            </div>
                                            <div class="jp_spotlight_slider_btn_wrapper">
                                                <div class="jp_spotlight_slider_btn">
                                                    <ul>
                                                        <li><a href="<?=base_url()?>../pelatihan/portal/dip_detail/<?=$item->id?>"><i class="fa fa-plus-circle"></i> &nbsp;DETAIL</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }?>
                                    
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                       
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
	
<script src="<?=base_url()?>template/HTML/job_dark/js/jquery.magnific-popup.js"></script>
<script src="<?=base_url()?>template/HTML/job_dark/js/custom_II.js"></script>