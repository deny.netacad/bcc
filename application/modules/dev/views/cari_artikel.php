<style type="text/css">
    .entry .entry-media{
        
    }
</style>

<div class="main">
                <div class="page-header larger parallax custom" style="background-image:url(assets/images/page-header-bg.jpg)">
                    <div class="container">
                        <h1>Cari Artikel</h1>
                        <ol class="breadcrumb">
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li><a href="#">Page</a></li>
                            <li class="active">Hasil pencarian dengan keyword '<?=$keyword?>'</li>
                        </ol>
                    </div><!-- End .container -->
                </div><!-- End .page-header -->

                <div class="container" >
                    <div id="board">
                        <?php
                        $rs = $this->db->query("select a.*, b.nama from section_article a inner join mast_user b on a.iduser = b.iduser where a.flag=1 and a.art_thumb !='' and a.art_intro !='' and (a.art_title like '%".$keyword."%' or a.art_content like '%".$keyword."%') order by a.art_date desc limit 3");
                        foreach($rs->result() as $item){
                        $xpath = new DOMXPath(@DOMDocument::loadHTML($item->art_thumb));
                        $src = $xpath->evaluate("string(//img/@src)");
                        $src=str_replace('.thumbs','',$src);
                        //  $url = (($item->apps_link!='')?$item->apps_link:base_url().'portal/aplikasi/'.$item->apps_seo);

                         if($item->position == '0'){
                                $art = 'berita';
                                $pop = 'counting';
                            }else{
                                $art = 'kepokmas';
                                $pop = ' ';
                            }
                        ?>
                           <article class="entry entry-list">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="entry-media">
                                            <figure>
                                                <a href="<?=base_url()?>portal/page/<?=$art?>/<?=$item->art_seo?>" class="<?=$pop?>" name="<?=$item->art_id?>"><img src="<?=$src?>" alt="<?=$item->art_title?>" style="background-color: white;" class="cover"></a>
                                            </figure>
                                            <div class="entry-meta">
                                                <span><i class="fa fa-calendar"></i><?php setlocale(LC_ALL, 'id_ID'); echo strftime("%d %B %Y", strtotime($item->tmstamp)); ?></span>
                                                <a href="#"><i class="fa fa-user"></i> <?=$item->nama?></a>
                                            </div><!-- End .entry-media -->
                                        </div><!-- End .entry-media -->
                                    </div><!-- End .col-md-6 -->
                                    <div class="col-md-6">
                                        <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="<?=base_url()?>portal/page/<?=$art?>/<?=$item->art_seo?>" class="<?=$pop?>" name="<?=$item->art_id?>"><?=$item->art_title?></a></h2>
                                        <div class="entry-content">
                                            <p><?=strtoupper($item->art_intro)?></p>
                                            <a href="<?=base_url()?>portal/page/<?=$art?>/<?=$item->art_seo?>" class="readmore <?=$pop?>" name="<?=$item->art_id?>">Read more<i class="fa fa-angle-right"></i></a>
                                        </div><!-- End .entry-content -->
                                    </div><!-- End .col-md-6 -->
                                </div><!-- End .row -->
                            </article>
                         <?php } ?>
                    </div>

                    <nav class="pagination-container">
                        <div class="text-center"><a href="" id="load">Muat lebih banyak...</a></div>
                    </nav>
                </div><!-- End .container -->
            </div><!-- End .main -->

<script type="text/javascript">
    var klik = 1;

    $(document).on('click','#load', function(e){
        $.ajax({
            url: '<?=base_url()?>portal/page/searchResult',
            type: 'post',
            data: {
                'range' : klik*3,
                'keyword' : '<?=$keyword?>',
                '<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'
            },
            success:function(rs){
                klik++;
                $('#board').append(rs);
            }
        });

        e.preventDefault();
    })
</script>