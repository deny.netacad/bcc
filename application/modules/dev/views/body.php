 <style>
 .jp_tittle_side_cont_wrapper {
    float: left;
    width: calc(100% - 65px);
    padding-top: 8px;
    padding-left: 11px;
}

.company_img {
    width: 100px !important;
    height: 95px;
}
 </style>
 <div class="jp_tittle_slider_main_wrapper" style="float:left; width:100%; margin-top:30px;">
        <div class="container">
            <div class="jp_tittle_name_wrapper">
                <div class="jp_tittle_name">
                    <h4 style="font-size: 14px;">Lowongan Kerja</h4>
                    <h4 style="font-size: 14px;">Kabupaten Bogor</h4>
                </div>
            </div>
            <div class="jp_tittle_slider_wrapper">
                <div class="jp_tittle_slider_content_wrapper">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
					               <?php
                              $rs = $this->db->query("select b.seo_url, d.photo, c.nama_job, d.nama_perusahaan from (select id_joblist, count(id) as jml from ngi_jobtrend where date(log) between current_date - interval 7 day and current_date group by id_joblist order by jml desc limit 0,3) a
                                left join ngi_joblist b on a.id_joblist = b.id
                                left join ngi_job c on c.id = b.posisi
                                left join ngi_perusahaan d on b.idperusahaan = d.idperusahaan
                                where b.is_aktif = 1 and now() < date_add(b.date_end, interval 1 day) 
                                ");
                              foreach($rs->result() as $item){
                          ?>
                                <div class="jp_tittle_slides_one" id="<?=$item->seo_url?>">
                                    <div class="jp_tittle_side_img_wrapper">
                                        <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="tittle_img" style="width: 65px; height: 62px; object-fit: contain;" />
                                    </div>
                                    <div class="jp_tittle_side_cont_wrapper">
                                        <h4><?=$item->nama_job?></h4>
                                        <p><?=((strlen($item->nama_perusahaan) > 25)? substr($item->nama_perusahaan,0,25)."..." : $item->nama_perusahaan)?></p>
                                    </div>
                                </div>
                          <?php } ?>

                        </div>

                        <?php
                              $rs = $this->db->query("select b.seo_url, d.photo, c.nama_job, d.nama_perusahaan from (select id_joblist, count(id) as jml from ngi_jobtrend where date(log) between current_date - interval 7 day and current_date group by id_joblist order by jml desc limit 3,3) a
                                left join ngi_joblist b on a.id_joblist = b.id
                                left join ngi_job c on c.id = b.posisi
                                left join ngi_perusahaan d on b.idperusahaan = d.idperusahaan
                                where b.is_aktif = 1 and now() < date_add(b.date_end, interval 1 day) 
                                ");

                              
                        ?>

                        <div class="item">
                         <?php
                              foreach($rs->result() as $item){
                          ?>
                            <div class="jp_tittle_slides_one" id="<?=$item->seo_url?>">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="tittle_img" style="width: 65px; height: 62px; object-fit: contain;" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4><?=$item->nama_job?></h4>
                                    <p><?=((strlen($item->nama_perusahaan) > 25)? substr($item->nama_perusahaan,0,25)."..." : $item->nama_perusahaan)?></p>
                                </div>
                            </div>
                          <?php }?>

                        </div>

                        <?php
                              $rs = $this->db->query("select b.seo_url, d.photo, c.nama_job, d.nama_perusahaan from (select id_joblist, count(id) as jml from ngi_jobtrend where date(log) between current_date - interval 7 day and current_date group by id_joblist order by jml desc limit 6,3) a
                                left join ngi_joblist b on a.id_joblist = b.id
                                left join ngi_job c on c.id = b.posisi
                                left join ngi_perusahaan d on b.idperusahaan = d.idperusahaan
                                where b.is_aktif = 1 and now() < date_add(b.date_end, interval 1 day) 
                                ");

                              
                        ?>
                        
                        <div class="item">
                         <?php
                              foreach($rs->result() as $item){
                          ?>
                            <div class="jp_tittle_slides_one" id="<?=$item->seo_url?>">
                                <div class="jp_tittle_side_img_wrapper">
                                    <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="tittle_img" style="width: 65px; height: 62px; object-fit: contain;" />
                                </div>
                                <div class="jp_tittle_side_cont_wrapper">
                                    <h4><?=$item->nama_job?></h4>
                                    <p><?=((strlen($item->nama_perusahaan) > 25)? substr($item->nama_perusahaan,0,25)."..." : $item->nama_perusahaan)?></p>
                                </div>
                            </div>
                          <?php }?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	</br>
	</br>
    <script type="text/javascript">
        $(function(){
            $(document).on('click','.jp_tittle_slides_one',function(){
                var id = $(this).prop('id');

                window.location = '<?=base_url()?>portal/jobs/'+id;
            });
        })
    </script>