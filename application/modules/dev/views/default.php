<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<style type="text/css">
  .jp_job_post_keyword_wrapper li i {
    padding-right: 5px;
    color: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
@media only screen and (max-device-width : 375px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 100%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}
@media only screen and (max-device-width : 411px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 100%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}
@media only screen and (max-device-width : 414px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 100%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}
/*@media only screen and (max-device-width : 768px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 85%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 30px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 300px;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}*/
/*@media only screen and (max-device-width : 1024px) {
.jp_tittle_name_wrapper {
    float: left;
    width: 15%;
    text-align: center;
    background: #184981;
    padding-top: 35px;
    padding-bottom: 114px;
}
.jp_form_btn_wrapper li a {
    float: left;
    width: 100%;
    height: 50px;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    background: #2978d2ed;
    color: #ffffff;
    line-height: 50px;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
}*/

.jp_job_post_right_cont p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.jp_job_post_main_wrapper_cont:hover .jp_job_post_keyword_wrapper {
    border: 1px solid #184981;
    border-top: 0;
    background: #184981;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
    .jp_add_resume_wrapper {
    width: 100%;
    height: 100%;
    background-position: center 0;
    background-size: cover;
    background-repeat: no-repeat;
    position: relative;
    text-align: center;
    padding-left: 5px;
    padding-right: 5px;
}
.jp_add_resume_img_overlay {
    position: absolute;
    top: 0%;
    left: 0%;
    right: 0%;
    bottom: 0%;
    background: #184981;
}
.jp_spotlight_slider_cont_Wrapper p {
    font-size: 16px;
    color: #184981;
    padding-top: 5px;
}
.jp_add_resume_cont li a {
    float: left;
    width: 160px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: #ffffff;
    font-weight: bold;
    background: #184981;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}
.spotlight_header_wrapper {
    float: left;
    width: 100%;
    background: #184981;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 20px;
    -webkit-border-top-left-radius: 10px;
    -moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #ffffff;
    border: 1px solid #3b6392;
    background: #184981;
    -webkit-border-top-right-radius: 0;
    -moz-border-top-right-radius: 0;
    border-top-right-radius: 0;
    -webkit-border-top-left-radius: 0;
    -moz-border-top-left-radius: 0;
    border-top-left-radius: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    transition: all 0.5s;
}

.jp_job_post_right_cont li:first-child {
    color: #6f7e98;
}

.jp_hiring_content_wrapper img{
    width: 100px !important;
}

.company_img {
    width: 100px !important;
    height: 95px;
    object-fit: contain;
}

</style>
<div class="jp_first_sidebar_main_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="jp_hiring_slider_main_wrapper">
                            <div class="jp_hiring_heading_wrapper">
                                <h2 style="font-size: 18px;">Perusahaan buka lowongan di kabupaten bogor terbaik </h2>
                            </div>
                            <div class="jp_hiring_slider_wrapper">
                                <div class="owl-carousel owl-theme">
                                    <?php 
                                        $q = $this->db->query('select a.profile_url, a.photo, a.alamat, a.nama_perusahaan, count(b.id) as jml  from ngi_perusahaan a 
                                        left join (select * from ngi_joblist where is_aktif = 1 
                                        and now() < date_add(date_end, interval 1 DAY)) b on a.`idperusahaan` = b.idperusahaan
                                        group by a.idperusahaan
                                        order by count(b.id) desc');
                                        $data = $q->result();

                                        foreach ($data as $item) {
                                    ?>
                                    <div class="item">
                                        <div class="jp_hiring_content_main_wrapper">
                                            <div class="jp_hiring_content_wrapper">
                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                <h4><?=((strlen($item->nama_perusahaan) > 10)? substr($item->nama_perusahaan,0,10)."..." : $item->nama_perusahaan)?></h4>
                                                <p>(<?=((strlen($item->alamat) > 15)? substr($item->alamat,0,15)."..." : $item->alamat)?>)</p>
                                                <ul>
                                                    <li><a href="<?=base_url()?>portal/company/<?=$item->profile_url?>"><?=$item->jml?> Opening</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="cc_featured_product_main_wrapper">
                            <div class="jp_hiring_heading_wrapper jp_job_post_heading_wrapper">
                                <h2 style="font-size: 18px;">Lowongan kerja di kabupaten bogor terbaru</h2>
                            </div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#best" aria-controls="best" role="tab" data-toggle="tab">Semuanya</a></li>
                                <li role="presentation"><a href="#trand" aria-controls="trand" role="tab" data-toggle="tab">Part Time</a></li>
                                <li role="presentation"><a href="#fulltime" aria-controls="fulltime" role="tab" data-toggle="tab">Full Time </a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="best">
                                <div class="ss_featured_products">
                                    <div class="">
                                        <div class="item" data-hash="zero">
                                            <?php 
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
												left join ngi_salary e on e.id = a.upah_kerja
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) ORDER BY log desc LIMIT 5");

                                                if($joblist->num_rows() > 0){
                                                foreach ($joblist->result() as $item) {

                                                $keyword = explode(',', $item->keyword);

                                            ?>
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?=$item->title?></h4>
                                                                <p><?=$item->nama_perusahaan?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-user"></i>&nbsp; <?=$item->nama_job?></li>
                                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                                </ul>
                                                                <ul>

                                                                </ul>
                                                                <ul>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                    <li><a href="#"> <?=$item->tipe?></a></li>
                                                                    <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <?php  foreach ($keyword as $kw) { ?>
                                                            <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>  
                                            <?php }}else{ ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_recent_resume_box_wrapper">
                                                    <h3 style="text-align: center; color: #184981;">Tidak ditemukan hasil</h3>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="trand">
                                <div class="ss_featured_products">
                                    <div class="owl-carousel owl-theme">
                                        <div class="item" data-hash="zero">
                                            <?php 
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
												left join ngi_salary e on e.id = a.upah_kerja
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) and a.tipe = 'part time' ORDER BY log desc LIMIT 5");

                                                if($joblist->num_rows() > 0){
                                                foreach ($joblist->result() as $item) {

                                                $keyword = explode(',', $item->keyword);

                                            ?>
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?=$item->title?></h4>
                                                                <p><?=$item->nama_perusahaan?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-user"></i>&nbsp; <?=$item->nama_job?></li>
                                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                                </ul>
                                                                <ul>

                                                                </ul>
                                                                <ul>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                    <li><a href="#"> <?=$item->tipe?></a></li>
                                                                    <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <?php  foreach ($keyword as $kw) { ?>
                                                            <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>  
                                            <?php }}else{ ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_recent_resume_box_wrapper">
                                                    <h3 style="text-align: center; color: #184981;">Tidak ditemukan hasil</h3>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="fulltime">
                                <div class="ss_featured_products">
                                    <div class="owl-carousel owl-theme">
                                        <div class="item" data-hash="zero">
                                            <?php 
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, b.photo, c.nama_job, d.province, d.city, d.subdistrict_name FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
												left join ngi_salary e on e.id = a.upah_kerja
                                                where a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 DAY) and a.tipe = 'full time' ORDER BY log desc LIMIT 5");

                                                if($joblist->num_rows() > 0){
                                                foreach ($joblist->result() as $item) {

                                                $keyword = explode(',', $item->keyword);

                                            ?>
                                            <div class="jp_job_post_main_wrapper_cont jp_job_post_main_wrapper_cont2">
                                                <div class="jp_job_post_main_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="jp_job_post_side_img">
                                                                <img src="<?=base_url()?>assets/img/profile/<?=(($item->photo != '')? $item->photo : 'default_company.jpg')?>" alt="company_img" class="company_img"/>
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><?=$item->title?></h4>
                                                                <p><?=$item->nama_perusahaan?></p>
                                                                <ul>
                                                                    <li><i class="fa fa-user"></i>&nbsp; <?=$item->nama_job?></li>
                                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                                </ul>
                                                                <ul>

                                                                </ul>
                                                                <ul>
                                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->city?>, <?=$item->province?></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                    <li><a href="#"> <?=$item->tipe?></a></li>
                                                                    <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>">Detail</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jp_job_post_keyword_wrapper">
                                                    <ul>
                                                        <li><i class="fa fa-tags"></i>Keywords :</li>
                                                        <?php  foreach ($keyword as $kw) { ?>
                                                            <li><a href="#" class="trending" id="<?=$kw?>"><?=$kw?>,</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>  
                                            <?php }}else{ ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="jp_recent_resume_box_wrapper">
                                                    <h3 style="text-align: center; color: white;">Tidak ditemukan hasil</h3>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="jp_first_right_sidebar_main_wrapper">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont" style="background-color: white;">
                                    <img src="<?=base_url()?>assets/logo/logo_bcc_new.png" alt="Logo Bogor Career Center" style="width:138px;height:75px;" title="Bogor Career Center" >
                                      <?php
                                        $rs = $this->db->query("select narasi from section_contact");
                                        $seo = $rs->row();

                                        ?>
                                    <h4 style="font-size:14px; text-align:center;color: black;">
                                        <?=$seo->narasi?>
                                    </h4>
                                    <ul>
                                        <li><a href="<?=base_url()?>portal/register_perusahaan" style="font-size:12px;"><i class="fa fa-plus-circle"></i> &nbsp;DAFTAR PERUSAHAAN</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_spotlight_main_wrapper">
                                <div class="spotlight_header_wrapper">
                                    <h4 style="font-size:12px;">Rekomendasi Lowongan <br>Kerja di Kabupaten Bogor </h4>
                                </div>
                                <div class="jp_spotlight_slider_wrapper">
                                    <div class="owl-carousel owl-theme">
                                    <?php 

                                            if($this->session->userdata('is_login_pelamar')){
                                                $person = $this->db->query('select skill from user_pelamar where iduser = "'.$this->session->userdata('id').'"')->row();

                                                $skill = explode(',', $person->skill);

                                                $where = "";
                                                foreach ($skill as $s) {
                                                    $where .= " or a.skill like '%".$s."%'";
                                                }

                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
												left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) ".$where." ORDER BY RAND() LIMIT 5");
                                            }else{
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
												left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5");
                                            }

                                            if($joblist->num_rows() == 0 ){
                                                $joblist = $this->db->query("SELECT a.*, e.name as upah, b.nama_perusahaan, c.nama_job, d.subdistrict_name, d.type, d.city, d.province FROM ngi_joblist a 
                                                LEFT JOIN ngi_perusahaan b ON a.idperusahaan = b.idperusahaan
                                                LEFT JOIN ngi_job c ON a.posisi = c.id
                                                LEFT JOIN ngi_kecamatan d ON a.idkecamatan = d.subdistrict_id
												left join ngi_salary e on e.id = a.upah_kerja
                                                WHERE a.is_aktif = 1 and now() < date_add(a.date_end, interval 1 day) and tipe='full time' ORDER BY RAND() LIMIT 5");
                                            }

                                            foreach ($joblist ->result() as $item) {

                                            ?>
                                    
                                        <div class="item">
                                            <div class="jp_spotlight_slider_img_Wrapper">
                                                <img src="<?=base_url()?>template/HTML/job_light/images/content/spotlight.jpg" alt="spotlight_img" />
                                            </div>
                                            <div class="jp_spotlight_slider_cont_Wrapper">
                                                <h4><?=$item->nama_job?></h4>
                                                <p><?=$item->nama_perusahaan?></p>
                                                <ul>
                                                    <li><i class="fa fa-money"></i>&nbsp; Rp. <?=($item->upah)?></li>
                                                    <li><i class="fa fa-map-marker"></i>&nbsp;  <?=$item->subdistrict_name?>, <?=$item->type?> <?=$item->city?>, <?=$item->province?></li>
                                                </ul>
                                            </div>
                                            <div class="jp_spotlight_slider_btn_wrapper">
                                                <div class="jp_spotlight_slider_btn">
                                                    <ul>
                                                        <li><a href="<?=base_url()?>portal/jobs/<?=$item->seo_url?>"><i class="fa fa-plus-circle"></i> &nbsp;DETAIL</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }?>
                                    
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                    
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_spotlight_main_wrapper">
                                <div class="spotlight_header_wrapper">
                                    <h4 style="font-size:14px;">Informasi Pelatihan</h4>
                                </div>
                                <div class="jp_spotlight_slider_wrapper">
                                    <div class="owl-carousel owl-theme">
										 <?php
                                        //atiati karo iki wa. mau mati kabeh sak halaman;
                               
										$this->load->database('default', FALSE);
										$this->db2 = $this->load->database('pelatihan', TRUE);
										
										$pelatihan = $this->db2->query("SELECT a.*,DATE_FORMAT(a.tgl_mulai,'%d %M') AS tglmulai,DATE_FORMAT(a.tgl_ahir,'%d %M %Y') AS tglakhir FROM ref_pelatihan a ");
										
										$this->load->database('pelatihan', FALSE);
										$this->load->database('default', TRUE);
										
                                       // echo("test");
                                        foreach ($pelatihan->result() as $items) {
                                        ?>
                                        <div class="item">
                                            <div class="jp_spotlight_slider_img_Wrapper">
                                                <img src="<?=base_url()?>template/HTML/job_dark/images/content/resume_bg_1.jpg" alt="spotlight_img" />
                                            </div>
                                            <div class="jp_spotlight_slider_cont_Wrapper">
                        
                                                <p><?=$items->judul?></p>
                                                <ul>
                                                    <li><i class="fa fa-map-marker"></i>&nbsp; <?=$items->tempat?></li>
                                                    <li><i class="fa fa-calendar-o"></i>&nbsp;  <?=$items->tglmulai?> s/d <?=$items->tglakhir?></li>
                                                </ul>
                                            </div>
                                            <div class="jp_spotlight_slider_btn_wrapper">
                                                <div class="jp_spotlight_slider_btn">
                                                    <ul>
                                                        <li><a href="<?=base_url()?>../pelatihan/portal/dip_detail/<?=$item->id?>"><i class="fa fa-plus-circle"></i> &nbsp;DETAIL</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }?>
                                    
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>