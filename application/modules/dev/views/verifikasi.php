   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/style_II.css" />
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>template/HTML/job_light/css/responsive2.css" />
   <style type="text/css">
   	.gc_main_navigation {
    color: #f9f9f9!important;
    font-weight: bold;
}
   </style>

<?php 
if(isset($_SESSION['email'])){
	$_POST['email'] = $_SESSION['email'];
	unset($_SESSION['email']);
}


if($this->session->userdata('is_login_pelamar')){
	redirect(base_url().'portal');
}else if($this->session->userdata('is_login_perusahaan')){
	redirect(base_url().'perusahaan');
}else if($_POST['email'] == ""){ 
	redirect(base_url().'portal/login');
}else{

?>


<div class="login_section">
		<!-- login_form_wrapper -->
		<div class="login_form_wrapper">
			<div class="container">
				<div class="row">
					<?php echo $this->session->flashdata('email_sent'); ?>
					<div class="col-md-8 col-md-offset-2">
						<!-- login_wrapper -->
						<h1>Verifikasi email anda</h1>
							<form id="formlogin" action="<?=base_url()?>portal/page/kirimulangemail" method="post">
								<p id="keterangan">Anda harus memverifikasi email anda terlebih dahulu sebelum login. Buka email anda, kemudian klik tautan dalam email yang sudah kami kirimkan. Jika anda tidak mendapatkan email, anda bisa mengirim ulang email tautan verifikasi.</p>
								<div class="login_wrapper">
									<!-- <div class="row">
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
											<a href="#" class="btn btn-primary"> <span>Login with Facebook</span> <i class="fa fa-facebook"></i> </a>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
											<a href="#" class="btn btn-primary google-plus"> Login  with Google <i class="fa fa-google-plus"></i> </a>
										</div>
									</div>
									<h2>or</h2> -->
									<div class="formsix-pos">
										<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
										<input type="hidden" name="email" value="<?=$_POST['email']?>">
										<input type="hidden" name="tipe" value="<?=$_POST['tipe']?>">
									</div>
									<div class="login_btn_wrapper">
										<a class="btn btn-primary login_btn" id="login"> Kirim ulang </a>
									</div>
									<div class="login_message">
										<p>Sudah verifikasi email ? <a href="<?=base_url()?>portal/login"> Login disini </a> </p>
									</div>
								</div>
							</form>
						<!-- /.login_wrapper-->
					</div>
				</div>
			</div>
		</div>
		<!-- /.login_form_wrapper-->
	</div>

	<script type="text/javascript">
		$(function(){
      $('#email2').focus();

			$('#login').click(function(){
				$('#login').attr('disabled',true);
				$('#login').html('Loading....');
				$('#formlogin').submit();
			});

			$(document).on('submit','#formlogin', function(e){
				$.ajax({
					url : $(this).prop('action'),
					data : $(this).serialize(),
					dataType: 'json',
					method : 'post',
					success: function(rs){
						if(rs.success){
							$('#keterangan').html('Email tautan verifikasi telah dikirimkan ulang ke alamat email <?=$_POST['email']?>. Silahkan cek email dan lakukan verifikasi.');
							$('#login').attr('disabled',false);
							$('#login').html('Kirim ulang');
						}else{
							$('#keterangan').html('Email tautan verifikasi gagal dikirimkan ke alamat email <?=$_POST['email']?>. Alamat email tidak valid.');
							$('#login').attr('disabled',false);
							$('#login').html('Kirim ulang');
						}
					},
					error: function(e){
						alert('Terjadi error pada server. Silahkan coba lagi');
						$('#login').attr('disabled',false);
						$('#login').html('Kirim ulang');
					}
				});

				e.preventDefault();
				// $('#login').attr('disable',false);
				// $('#login').html('Loading....');
			})
		})

	</script>

<?php } ?>