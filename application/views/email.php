<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Demystifying Email Design</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	 <tr width="100%">
	 	<td width="35%"></td>
	  <td width="30%">
	   	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #cccccc;">
  <tr>
  <td align="center" bgcolor="#00386d" style="padding: 40px 0 30px 0;">
 	<img src="https://bogorcareercenter.bogorkab.go.id/template/HTML/job_dark/images/header/logo4.png" style="display: block;" />
	</td>
 </tr>
 <tr>
  <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;" align="center">
 <b >Verifikasi alamat email</b>
</td>
 </tr>
 <tr>
  <td style="padding: 20px 0 30px 0;" align="center">
   Silahkan verifikasi alamat email anda dengan menekan tombol dibawah, jika tidak anda bisa mengunjungi url berikut "urlnya"
  </td>
 </tr>
 <tr>
  <td align="center">
   <a style="background-color: #ee4c50; font-size: 16px; border-radius: 5px; color: white; padding: 15px 32px; text-decoration: none;" href="facebook.com">Verifikasi!</a>
  </td>
 </tr>
 </table>
</td>
 </tr>
 <tr>
  <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr>
 <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
 &reg; Bogor Career Center, 2019<br/>
</td>
  <td align="right">
 
</td>
 </tr>
</table>
  </td>
 </tr>
 </table>
	  </td>
	  <td width="35%"></td>
	 </tr>
</table>
</body>
</html>