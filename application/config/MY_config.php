<?php

$config['data']['hari'] 			= array("","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu","Minggu");
$config['data']['bulan'] 			= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
$config['data']['site_title']  		= 'Dinas Tenaga Kerja Kabupaten Bogor';
$config['data']['site_heading']		= 'Dinas Tenaga Kerja Kabupaten Bogor';
$config['data']['versi']			= '1.0';
$config['data']['site_copyright']	= 'Copyright &copy; 2019 - Dinas Tenaga Kerja dan Transmigrasi Kabupaten Bogor';
$config['data']['site_desc']		= 'Dinas Tenaga Kerja dan Transmigrasi Kabupaten Bogor';
$config['base_url']					= 'http://'.$_SERVER['SERVER_NAME'].'/';
$config['data']['def_img']			=  $config['base_url'].'assets/img/';
$config['data']['def_css']			=  $config['base_url'].'assets/css/';
$config['data']['def_js']			=  $config['base_url'].'assets/js/';
$config['data']['def_ass']			=  $config['base_url'].'assets/';
