<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {
	
	function tes(){
		print_r($_POST);
		print_r($_FILES);
	}
	
	function do_upload() {
		$config['image_library'] = 'gd2';
		$config['upload_path'] = BASEPATH."../upliddir/image/";
        
		$config['allowed_types'] = 'gif|jpg|png|JPG';
		$a1 = explode(".",$_FILES['Filedata']['name']);
		$user = $this->input->post('newfile');
		$filename 			= $user.".".strtolower($a1[1]);
		$filename_thumb 	= $user."_thumb.".strtolower($a1[1]);
		$filename_crop	 	= $user."_crop.".strtolower($a1[1]);
		$ctime = mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('Y'));
		$config['file_name'] 	= $filename;
		$config['max_size']    	= '5000';
        $config['max_width']  	= '3000';
        $config['max_height']  	= '2400';
		
		
        
		$this->load->library('upload', $config);
		if(file_exists(BASEPATH."../upliddir/image/".$filename)) unlink(BASEPATH."../upliddir/image/".$filename);
		if(file_exists(BASEPATH."../upliddir/image/".$filename_thumb)) unlink(BASEPATH."../upliddir/image/".$filename_thumb);
		if(file_exists(BASEPATH."../upliddir/image/".$filename_crop)) unlink(BASEPATH."../upliddir/image/".$filename_crop);
		
		$this->upload->do_upload('Filedata');
        $res = $this->upload->data();
		
		// create thumbnail image
		
		$config['image_library'] 	= 'gd2';
		$config['source_image']		= BASEPATH."../upliddir/image/".$res['file_name'];
		$config['create_thumb'] 	= TRUE;
		$config['maintain_ratio'] 	= TRUE;
		$config['width']		 	= 95;
		$config['height']			= 95;
		$this->image_lib->initialize($config);
		if($this->image_lib->resize()){
			$this->image_lib->clear();
			empty($config);
			$config = array();
			// resize original image to 500 width
			$config['image_library'] 	= 'gd2';
			$config['create_thumb'] 	= FALSE;
			$config['source_image']		= BASEPATH."../upliddir/image/".$res['file_name'];
			$config['maintain_ratio'] 	= TRUE;
			$config['width']		 	= 400;
			$config['height']			= 400;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();
		}
		
		// get file id return
		$a = explode('.',$res['file_name']);
		$img = imagecreatefrompng(BASEPATH."../upliddir/image/".$user.'_thumb.'.strtolower($a[1]));
		ob_start();
		imagepng($img);
		$imagevariable = ob_get_contents();
		ob_end_clean();
		
		$file_id = md5($res['file_name'] + rand()*100000);
		$_SESSION["file_info"][$file_id] = $imagevariable;
		$ret['FILEID'] = $file_id;
		$ret['filename'] = $res['file_name'];
		echo json_encode($ret);
    }
	
}