<?php (defined('BASEPATH')) OR exit('No direct script access allowed');



class MY_Model extends CI_Model 

{

	var $modules = "";

	var $role = array();

	

	function __construct()

    {

        parent::__construct();

		$this->load->library("session");

		$this->role = $this->session->userdata('role');

    }

	

	function getRole(){

		return $this->role;

	}

	


	function kirimmail($kepada,$judul,$pesan){
	//echo "test mail";
		$this->load->library('email');
                $config['protocol'] = 'mail';
                $config['mailpath'] = '/usr/bin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['mailtype'] = 'html';
                $config['wordwrap'] = TRUE;

                $this->email->initialize($config);
                $this->email->from('noreply@ketahananpangan.semarangkota.go.id', $judul);
				#$this->email->from('noreply@nginovasi.com', $judul);
                $this->email->to($kepada);
                $cc = array("");
                $this->email->cc($cc);

                $html ='';

                $html=$pesan;




                $this->email->subject($judul);
                $this->email->message($html);
                #$this->email->attach(BASEPATH.'../memberarea/upliddir/LM1504200000001.jpg');

                $this->email->send();

                $this->email->print_debugger();
                // $ret['text']="Berhasil terkirim";
                // echo json_encode($ret);


		
	}



	function toMySQLDate($value=""){

		$value = (($value!='')?$value:"00-00-0000");

		$v = explode(" ",$value);

		$val = explode("-",$v[0]);

		if(count($v)>1){

			$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];

		}else{

			$value = $val[2]."-".$val[1]."-".$val[0];

		}

		return $value;

	}

	

	

	function now(){

		$rs = $this->db->query("select now() as tg");

		$item = $rs->row();

		return $item->tg;

	}

	

	function nowtext(){

		$rs = $this->db->query("select date_format(now(),'%d-%m-%Y %H:%i:%s') as tg");

		$item = $rs->row();

		return $item->tg;

	}

	

	function datenowtext(){

		$rs = $this->db->query("select date_format(now(),'%d-%m-%Y') as tg");

		$item = $rs->row();

		return $item->tg;

	}

	

	

	

	

	function replace($table,$arrdata,$arrwhere,$flag=0,&$text=""){

		$ret = true;

		$table_activity = "";

		$nip = "";

		

		

		$fields = $this->db->list_fields($table);

		

		

		$data['ip']			= $_SERVER['REMOTE_ADDR'];

		$data['tmstamp']	= $this->now();

		$data['useragent']	= $this->session->userdata('user_agent');

		$data['username']	= $this->session->userdata('username');

		/*

		if( array_key_exists($table,$this->arrtable)){

			$table_activity = $this->arrtable[$table];

		}

		*/

		

		if(in_array("nip",$fields)){

			$nip = 'NIP : '.$arrdata['nip'];

		}

		

		

		if($flag>0){

			if($this->role[$this->getCurModule()]['e']==1){

				if($table!="mast_user"){

					if(in_array("iduserupd",$fields)){

						$arrdata['iduserupd'] = $this->session->userdata('iduser');

					}

					if(in_array("updateon",$fields)){

						$arrdata['updateon'] = $this->now();

					}

				}

				$data['ket']		= 'akses update data di module '.$this->modules.' '.$table_activity.' '.$nip;

				$this->db->insert("log_user",$data);

				if(!$this->db->update($table,$arrdata,$arrwhere)) $ret = false;

			}else{

				$data['ket']		= 'akses update data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;

				$this->db->insert("log_user",$data);

				$ret = false;

				$text = "user tidak diperkenankan merubah data !";

			}

		}else{

			if(count($arrwhere)==0){

				if($this->role[$this->getCurModule()]['i']==1){

					if($table!="mast_user"){

						if(in_array("iduser",$fields)){

							$arrdata['iduser'] = $this->session->userdata('iduser');

						}

						if(in_array("createon",$fields)){

							$arrdata['createon'] = $this->now();

						}

					}

					$data['ket']		= 'akses insert data di module '.$this->modules.' '.$table_activity.' '.$nip;

					$this->db->insert("log_user",$data);

					if(!$this->db->insert($table,$arrdata)) $ret = false;

				}else{

					$data['ket']		= 'akses insert data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;

					$this->db->insert("log_user",$data);

					$ret = false;

					$text = "user tidak diperkenankan menginput data !";

				}

			}else{

				$rs = $this->db->get_where($table, $arrwhere);

				if($rs->num_rows()>0){

					if($this->role[$this->getCurModule()]['e']==1){

						if($table!="mast_user"){

							if(in_array("iduserupd",$fields)){

								$arrdata['iduserupd'] = $this->session->userdata('iduser');

							}

							if(in_array("updateon",$fields)){

								$arrdata['updateon'] = $this->now();

							}

						}

						$data['ket']		= 'akses update data di module '.$this->modules.' '.$table_activity.' '.$nip;

						$this->db->insert("log_user",$data);

						if(!$this->db->update($table,$arrdata,$arrwhere)) $ret = false;

					}else{

						$data['ket']		= 'akses update data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;

						$this->db->insert("log_user",$data);

						$ret = false;

						$text = "user tidak diperkenankan merubah data !";

					}

				}else{

					if($this->role[$this->getCurModule()]['e']==1){

						if($table!="mast_user"){

							if(in_array("iduser",$fields)){

								$arrdata['iduser'] = $this->session->userdata('iduser');

							}

							if(in_array("createon",$fields)){

								$arrdata['createon'] = $this->now();

							}

						}

						$data['ket']		= 'akses insert data di module '.$this->modules.' '.$table_activity.' '.$nip;

						$this->db->insert("log_user",$data);

						if(!$this->db->insert($table,$arrdata)) $ret = false;

					}else{

						$data['ket']		= 'akses update data(ditolak!) di module '.$this->modules.' '.$table_activity.' '.$nip;

						$this->db->insert("log_user",$data);

						$ret = false;

						$text = "user tidak diperkenankan menginput data !";

					}

				}

			}

		}

		return $ret;

	}

	

	

	function save($tablename="",$arrnot=array(),$keyedit=array(),$keyindex=array(),$keydate=array(),$keyin=array(),$keyout=array(),$data=array(),$ret=array(),$keynum=array()){

		$data = $data;

		$dt = array();

		$flag = 0;

		$ret['err']		= false;

		$ret['text']	= "Data Tersimpan";	

		foreach($_POST as $key=>$value){

			if(!is_array($value)){

				if(array_search($key,$keyin)!=""){

					$keys =  array_keys($keyin,$key);

					$key = $keyout[$keys[0]];

				}

				if(array_search($key,$keynum)!=''){

					$value = str_replace(",","",$value);

					$value = (($value>0)?$value:0);

				}

				if(array_search($key,$keydate)!=''){

					$value = (($value!='')?$value:"00-00-0000");

					$v = explode(" ",$value);

					$val = explode("-",$v[0]);

					if(count($v)>1){

						$value = $val[2]."-".$val[1]."-".$val[0]." ".$v[1];

					}else{

						$value = $val[2]."-".$val[1]."-".$val[0];

					}

				}

				if(array_search($key,$arrnot)==""){

					$data[$key] = $value;

				}

			}

		}



		foreach($_POST as $key=>$value){

			if(!is_array($value)){

				if(array_search($key,$keyin)!=""){

					$keys =  array_keys($keyin,$key);

					$key = $keys[0];

				}

				if(array_search($key,$keyedit)!=""){

					$keys =  array_keys($keyedit,$key);

					

					if($this->input->post($key)!=""){

						$dt[$keyindex[$keys[0]]] = $value;

						$flag = 1;

					}

				}

			}

		}

		

		if(!$this->replace($tablename,$data,$dt,$flag,$text)){

			$ret['err']		= true;

			$ret['text']	= "Terjadi Kesalahan\n".$text."\n".$this->db->_error_message();	

		}

		echo json_encode($ret);

	}

	

	function Terbilang($x)

	{

		$abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");

		if ($x < 12)

		return " " . $abil[$x];

		elseif ($x < 20)

		return $this->Terbilang($x - 10) . "belas";

		elseif ($x < 100)

		return $this->Terbilang($x / 10) . " puluh" . $this->Terbilang($x % 10);

		elseif ($x < 200)

		return " seratus" .$this-> Terbilang($x - 100);

		elseif ($x < 1000)

		return $this->Terbilang($x / 100) . " ratus" . $this->Terbilang($x % 100);

		elseif ($x < 2000)

		return " seribu" . $this->Terbilang($x - 1000);

		elseif ($x < 1000000)

		return $this->Terbilang($x / 1000) . " ribu" . $this->Terbilang($x % 1000);

		elseif ($x < 1000000000)

		return $this->Terbilang($x / 1000000) . " juta" . $this->Terbilang($x % 1000000);

		elseif ($x < 1000000000000)

		return $this->Terbilang($x / 1000000000) . " milyar" . $this->Terbilang(bcmod($x,1000000000));

		elseif ($x < 1000000000000000)

		return $this->Terbilang($x / 1000000000000) . " trilyun" . $this->Terbilang(bcmod($x,1000000000000));

	}

	

	

	function getCurModule(){

		return $this->modules;

	}

	

	function getUserAgent(){

		if ($this->agent->is_browser())

		{

			$agent = $this->agent->browser().' '.$this->agent->version();

		}

		elseif ($this->agent->is_robot())

		{

			$agent = $this->agent->robot();

		}

		elseif ($this->agent->is_mobile())

		{

			$agent = $this->agent->mobile();

		}

		else

		{

			$agent = 'Unidentified User Agent';

		}



		return $agent." ".$this->agent->platform();

	}

	

	

	function del_log($table,$data,$role){

		$tablename = "log_user";

		$dt['ip'] = $this->input->ip_address();

		$dt['username'] = $this->session->userdata('username');

		$dt['useragent'] = $this->getUserAgent();

		$dt['ket'] = (($role==1)?"(allow)":"(restrict)")."menghapus data dengan index ".json_encode($data);

		$this->db->insert($tablename,$dt);

	}



	function showmenu2($id='0',$indent='1'){

		$rs = $this->db->query("SELECT * FROM section_links WHERE  position=4");

		$num = $rs->num_rows();

        $nbsp = "";

		$i = 0;

		/*while($i<$indent){

			$nbsp.="&nbsp";

			$i++;

		}*/

		$style = (($id=='0')?" style=\"background-color:#EEEEEE\"":"");

		foreach($rs->result() as $item){

				$i++;

				echo "<option value=\"".$item->links_id."\" ".$style.">$nbsp".strtoupper($item->links_title)."</option>";

				//$this->showmenu2($item->id,$indent+4);     

		}

	}
	function showmenuppid($id='0',$indent='1'){

		$rs = $this->db->query("SELECT * FROM section_category_ppid WHERE sub_id=$id");

		$num = $rs->num_rows();

        $nbsp = "";

		$i = 0;

		while($i<$indent){

			$nbsp.="&nbsp";

			$i++;

		}

		$style = (($id=='0')?" style=\"background-color:#EEEEEE\"":"");

		foreach($rs->result() as $item){

				$i++;

				echo "<option value=\"".$item->id."\" ".$style.">$nbsp".$item->title."</option>";

				$this->showmenuppid($item->id,$indent+4);     

		}

	}
	
	function showkategorimenu($id='0',$indent='1'){

		$rs = $this->db->query("SELECT * FROM section_category WHERE sub_id=$id");

		$num = $rs->num_rows();

        $nbsp = "";

		$i = 0;

		while($i<$indent){

			$nbsp.="&nbsp";

			$i++;

		}

		$style = (($id=='0')?" style=\"background-color:#EEEEEE\"":"");

		foreach($rs->result() as $item){

				$i++;

				echo "<option value=\"".$item->id."\" ".$style.">$nbsp".$item->title."</option>";

				$this->showkategorimenu($item->id,$indent+4);     

		}

	}



	function showppid($id='0',$indent='1'){

		$rs = $this->db->query("SELECT * FROM section_category WHERE sub_id=$id and posisi=0");

		$num = $rs->num_rows();

        $nbsp = "";

		$i = 0;

		while($i<$indent){

			$nbsp.="&nbsp";

			$i++;

		}

		$style = (($id=='0')?" style=\"background-color:#EEEEEE\"":"");

		foreach($rs->result() as $item){

				$i++;

				echo "<option value=\"".$item->id."\" ".$style.">$nbsp".$item->title."</option>";

				$this->showppid($item->id,$indent+4);     

		}

	}

	
	

	function showbidang($id='0',$indent='1'){

		$rs = $this->db->query("SELECT * FROM mast_bidang WHERE parent_id=$id");

		$num = $rs->num_rows();

        $nbsp = "";

		$i = 0;

		while($i<$indent){

			$nbsp.="&nbsp";

			$i++;

		}

		$style = (($id=='0')?" style=\"background-color:#EEEEEE\"":"");

		foreach($rs->result() as $item){

				$i++;

				echo "<option value=\"".$item->idbidang."\" ".$style.">$nbsp".$item->bidang."</option>";

				$this->showbidang($item->idbidang,$indent+4);     

		}

	}

	

	

	function showmenu($id='0'){
		$rs = $this->db->query("SELECT * FROM section_category WHERE sub_id=$id and flag= 1 ");
		$num = $rs->num_rows();
        $i = 0;
          foreach($rs->result() as $item){
            if($i == 0 and $id==0) echo '<ul class="nav navbar-nav">';
            if($i == 0 and $id>0) echo 	'<ul class="dropdown-menu" role="menu">';
            if($i == 0 and $id==0) echo '<li><a href="'.base_url().'" class="active">HOME</a></li>';
            $i++;

            $url = (($item->url!='')?base_url().$item->url:(($item->jenurl==1)?base_url().'portal/page/konten/'.$item->id:'javascript:void(0)'));
            $target = (($item->target!='')?' target="'. $item->target.'"':'');
			
			$links=str_replace(' ','-',strtoupper($item->title));
			
			if(($item->sub_id==0) and ($item->jenurl==0) and ($item->mkategori==1)){
                echo '<li class="dropdown"><a href="'.base_url().'portal/page/konten/'.$item->id.'/'.$links.' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$item->title.'</a>';
            }elseif(($item->sub_id==0) and ($item->jenurl==0) and ($item->mkategori==0)){
              //  echo '<li><a href="'.$url.'" '.$target.'>'.$item->title.'</a>';
			   echo '<li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$item->title.'<span class="angle"></span></a>';
          
            }elseif(($item->menu_order==1)){
				  echo '<li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$item->title.'<span class="angle"></span></a>';
			}elseif(($item->jenurl==1)){
				echo '<li><a href="'.$url.'" '.$target.'>'.$item->title.'</a>';
			}
			else{
				echo '<li><a href="'.base_url().'portal/page/konten/'.$item->id.'/'.$links.'">'.$item->title.'</a>';
			}
			
            $this->showmenu($item->id);
            echo '</li>';
            if($i == $num){

				echo '</ul>';
			}
        }
    }
	
	function showmenu5($id='0'){
        $rs = $this->db->query("SELECT * FROM menu WHERE parent_id=$id and flag= 1 order by menu_order");
        $num = $rs->num_rows();
        $i = 0;
        foreach($rs->result() as $item){
			
            if($i == 0 and $id==0) echo '<ul class="nav navbar-nav">';
            if($i == 0 and $id>0) echo '<ul class="dropdown">';
            if($i == 0 and $id==0) echo '<li><a href="'.base_url().'" class="active">HOME</a></li>';
            $i++;

            $url = (($item->url!='')?$item->url:(($item->jenurl==1)?base_url().'portal/page/detail/'.$item->id:'javascript:void(0)'));
            $target = (($item->target!='')?' target="'. $item->target.'"':'');
            if(($item->parent_id==0) and ($item->jenurl==0)){
                echo '<li class="dropdown"><a href="'.$url.'" '.$target.' class="dropdown-toggle" data-toggle="dropdown">'.$item->title.'</a>';
            }else{
                echo '<li><a href="'.$url.'" '.$target.'>'.$item->title.'</a>';
            }
            $this->showmenus5($item->id);
            echo '</li>';
            if($i == $num){
	echo '<li><a class="" href="'.base_url().'portal/page/basket">Cart</a></li>';
						
			echo '<li><a class="btn btn-sm btn-success" href="'.base_url().'portal/register/'.$this->session->userdata('member_user').'">Join Now</a></li>';
				
				echo '</ul>';
			}
        }
    }

	

	function showmenus($id='0',$level=0){

        $rs = $this->db->query("SELECT * FROM menu WHERE parent_id=$id and flag = 1 order by parent_id,menu_order");

        $num = $rs->num_rows();

        $i = 0;

		$level++;

        foreach($rs->result() as $item){

			if($level==1){

				$class = "submenu";

			}

			if($level==2){

				$class = "sub-submenu";

			}

			

            if($i == 0 and $id==0) echo '<ul class="'.$class.'">';

            if($i == 0 and $id>0) echo '<ul class="'.$class.'">';

            $i++;



            switch($item->jenurl){

				case 1:

					$url = base_url().'portal/konten/'.$item->menu_seo;

				break;

				case 2:

					$url = $item->url;

				break;

				case 3:

					$url = base_url().'portal/berita';

				break;

				case 4:

					$url = base_url().'portal/gallery';

				break;

				case 5:

					$url = base_url().'portal/video';

				break;

			}

			

            $target = (($item->target!='')?' target="'. $item->target.'"':'');

            if($item->jenurl==0){

                echo '<li class="has-sub-submenu"><a href="'.$url.'" '.$target.'>'.$item->title.' </a>';

            }else{

                echo '<li class=""><a href="'.$url.'" '.$target.'>'.$item->title.'</a>';

            }

            $this->showmenus($item->id,$level);

            echo '</li>';

            if($i == $num)echo '</ul>';

        }

    }

}