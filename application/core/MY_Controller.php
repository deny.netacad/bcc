<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller
{
	var $data;
	var $modules;
	var $role = array();

    function __construct()
    {
        parent::__construct();
		$this->data = $this->config->item("data");
		$this->role = $this->session->userdata('role');
		$this->load->library("session");
    }

	function cekRole(){
		if(is_array($this->role) && array_key_exists($this->modules,$this->role)){
			if(is_array($this->role[$this->modules])){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function bar128($id=""){
		#$this->load->library('bargen/class/BCGFont');
		$this->load->library('bargen/class/BCGColor');
		$this->load->library('bargen/class/BCGDrawing');

		$this->load->library('bargen/class/BCGcode128');
		#$this->load->library('bargen/class/BCGcode39');
		#$this->font = new BCGFont(BASEPATH.'libraries/bargen/class/font/tahoma.ttf', 28);
		$this->color_black = new BCGColor(0, 0, 0);
		$this->color_white = new BCGColor(255, 255, 255);

		$this->code = new BCGcode128();
		#$this->code->setScale(5); // Resolution
		#$this->code->setThickness(30); // Thickness
		$this->code->setForegroundColor($this->color_black); // Color of bars
		$this->code->setBackgroundColor($this->color_white); // Color of spaces
		#$this->code->setFont($this->font);
		$this->code->setFont(0);		// Font (or 0)
		$this->code->parse($id); // Text

		/* Here is the list of the arguments
		1 - Filename (empty : display on screen)
		2 - Background color */
		$this->drawing = new BCGDrawing('', $this->color_white);
		$this->drawing->setBarcode($this->code);
		$this->drawing->draw();
		// Header that says it is an image (remove it if you save the barcode to a file)
		header('Content-Type: image/png');
		// Draw (or save) the image into PNG format.
		$this->drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	}

	function bar39($id=""){
		#$this->load->library('bargen/class/BCGFont');
		$this->load->library('bargen/class/BCGColor');
		$this->load->library('bargen/class/BCGDrawing');

		#$this->load->library('bargen/class/BCGcode128');
		$this->load->library('bargen/class/BCGcode39');
		#$this->font = new BCGFont(BASEPATH.'libraries/bargen/class/font/tahoma.ttf', 28);
		$this->color_black = new BCGColor(0, 0, 0);
		$this->color_white = new BCGColor(255, 255, 255);

		$this->code = new BCGcode39();
		#$this->code->setScale(5); // Resolution
		#$this->code->setThickness(30); // Thickness
		$this->code->setForegroundColor($this->color_black); // Color of bars
		$this->code->setBackgroundColor($this->color_white); // Color of spaces
		#$this->code->setFont($this->font);
		$this->code->setFont(0);		// Font (or 0)
		$this->code->parse($id); // Text

		/* Here is the list of the arguments
		1 - Filename (empty : display on screen)
		2 - Background color */
		$this->drawing = new BCGDrawing('', $this->color_white);
		$this->drawing->setBarcode($this->code);
		$this->drawing->draw();
		// Header that says it is an image (remove it if you save the barcode to a file)
		header('Content-Type: image/png');
		// Draw (or save) the image into PNG format.
		$this->drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	}

	function load(){
		if($this->modules != 'portal' && $this->modules != 'perusahaan'){
			if($this->cekRole()){
				$this->data['module'] = $this->session->userdata['role'][$this->modules]['urlmodule'];
				$this->data['page'] = $this->uri->segment(2);
				$this->data['role'] = $this->role;

				if($this->session->userdata('is_login_portal_disnakerbogor')){
					$this->load->view($this->modules.'/main',$this->data);
				}else{
					redirect('auth');
				}
			}else{
				redirect('auth');
			}
		}else{
			$this->insertVisitor();
			if($this->session->userdata('is_login_pelamar') || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'jobs' || $this->uri->segment(2) == 'verifikasi'){
				$this->data['page'] = $this->uri->segment(2);

				$cek = $this->input->get();
				if(isset($cek['refer'])) $this->data['redirect'] = $cek['refer'];
				else $this->data['redirect'] = "";
				$this->load->view($this->modules.'/index',$this->data);
			} else redirect('/portal/login?refer='.current_url());
		}
	}

	function insertVisitor(){
		$this->db->insert('ngi_webvisitor',array('ip' => $this->input->ip_address()));
	}

	function data(){
		error_reporting(0);
		$this->data['page'] = $this->uri->segment(4);
		$this->data['role'] = $this->role;
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'].'_data',$this->data);
	}

	function cetak(){
		$this->data['page'] = $this->uri->segment(4);
		$this->data['role'] = $this->role;
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'].'_print',$this->data);
	}

	function form(){
		$this->data['page'] = $this->uri->segment(4);
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'].'_form',$this->data);
	}

	function view(){
		$this->data['page'] = $this->uri->segment(4);
		$this->data['role'] = $this->role;
		$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'],$this->data);
		//$this->load->view([$this->modules]['urlmodule'].'/'.$this->data['page'],$this->data);

	}

	function viewmodal(){
		$this->data['page'] = $this->uri->segment(3);
		//$this->data['role'] = $this->role;
		//$this->load->view($this->session->userdata['role'][$this->modules]['urlmodule'].'/'.$this->data['page'],$this->data);
		$this->load->view('portal/'.$this->data['page'],$this->data);

	}

	function pdf(){
	//	if($this->session->userdata('is_login_ngosis')){
			ini_set('memory_limit','2048M');

			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);
			$this->load->library('pdf');
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # A4
				break;
				case "small":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # small
				break;
				default:
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # legal
				break;
			}


			switch($this->input->get('o')){
				case "P":
					$o = "P";
				break;
				default:
					$o = "L";
				break;
			}

			#$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('tmpl')==''){
				if($this->input->get('o')=='L'){
				 	$pdf->SetDocTemplate(BASEPATH.'../assets/temp_1.pdf',0);
				}else{
					$pdf->SetDocTemplate(BASEPATH.'../assets/temp_1.pdf',0);
				}
			}else{
				 $pdf->SetDocTemplate(BASEPATH.'../assets/'.$this->input->get('tmpl').'.pdf',0);
			}

			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            50, // margin top
            20, // margin bottom
            50, // margin header
            5); // margin footer

			$pdf->SetDisplayMode('fullpage');
			$plus = "";
			if($this->input->get('issuer')==1){
				$plus = "Issued By : ".$this->session->userdata('username')."|".$this->session->userdata('nama');
			}
			if($plus!=''){
				$pdf->SetHTMLFooter('<i>'.$plus.'</i>&nbsp;&nbsp;&nbsp;&nbsp;'.'bogorcareercenter.bogorkab.go.id'.'&nbsp;&nbsp;&nbsp;{PAGENO}&nbsp;&nbsp;&nbsp;'.date(DATE_RFC822));
			}
			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;

	//	}else{
	//		echo "<script> history.back() </script>";
	//	}
	}

	function pdfver(){
	//	if($this->session->userdata('is_login_ngosis')){
			ini_set('memory_limit','2048M');

			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);
			$this->load->library('pdf');

			#
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # A4
				break;
				case "small":
					$pdf = $this->pdf->load('en-GB-x', array(210,148)); # small
				break;
				default:
					$pdf = $this->pdf->load('en-GB-x', array(208,300)); # legal
				break;
			}


			switch($this->input->get('o')){
				case "L":
					$o = "L";
				break;
				default:
					$o = "L";
				break;
			}

			#$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('tmpl')==''){
				if($this->input->get('o')=='L'){
					$pdf->SetDocTemplate(BASEPATH.'../assets/perusahaan_ver.pdf',0);
				}else{
					$pdf->SetDocTemplate(BASEPATH.'../assets/perusahaan_ver.pdf',0);
				}
			}else{
				$pdf->SetDocTemplate(BASEPATH.'../assets/'.$this->input->get('tmpl').'.pdf',0);
			}

			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
            120, // margin_left
            20, // margin right
            50, // margin top
            15, // margin bottom
            0, // margin header
            5); // margin footer

			$pdf->SetDisplayMode('fullpage');
			$plus = "";
			if($this->input->get('issuer')==1){
				$plus = "Issued By : ".$this->session->userdata('username')."|".$this->session->userdata('nama');
			}
			if($plus!=''){
				$pdf->SetHTMLFooter('<i>'.$plus.'</i>&nbsp;&nbsp;&nbsp;&nbsp;'.'bogorcareercenter.bogorkab.go.id'.'&nbsp;&nbsp;&nbsp;{PAGENO}&nbsp;&nbsp;&nbsp;'.date(DATE_RFC822));
			}
			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;

	//	}else{
	//		echo "<script> history.back() </script>";
	//	}
	}
	function pdfresumepencaker(){
	//	if($this->session->userdata('is_login_ngosis')){
			ini_set('memory_limit','2048M');

			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);
			$this->load->library('pdf');

			#
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # A4
				break;
				case "small":
					$pdf = $this->pdf->load('en-GB-x', array(210,148)); # small
				break;
				default:
					$pdf = $this->pdf->load('en-GB-x', array(208,300)); # legal
				break;
			}


			switch($this->input->get('o')){
				case "L":
					$o = "L";
				break;
				default:
					$o = "P";
				break;
			}

			#$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('tmpl')==''){
				if($this->input->get('o')=='L'){
					$pdf->SetDocTemplate(BASEPATH.'../assets/template_resume.pdf',0);
				}else{
					$pdf->SetDocTemplate(BASEPATH.'../assets/template_resume.pdf',0);
				}
			}else{
				$pdf->SetDocTemplate(BASEPATH.'../assets/'.$this->input->get('tmpl').'.pdf',0);
			}

			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
            23, // margin_left
            15, // margin right
            10, // margin top
            15, // margin bottom
            0, // margin header
            5); // margin footer

			$pdf->SetDisplayMode('fullpage');
			$plus = "";
			if($this->input->get('issuer')==1){
				$plus = "Issued By : ".$this->session->userdata('username')."|".$this->session->userdata('nama');
			}
			if($plus!=''){
				$pdf->SetHTMLFooter('<i>'.$plus.'</i>&nbsp;&nbsp;&nbsp;&nbsp;'.'sipp.sampangkab.go.id'.'&nbsp;&nbsp;&nbsp;{PAGENO}&nbsp;&nbsp;&nbsp;'.date(DATE_RFC822));
			}
			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;

	//	}else{
	//		echo "<script> history.back() </script>";
	//	}
	}
	function pdfkartupeserta(){
	//	if($this->session->userdata('is_login_ngosis')){
			ini_set('memory_limit','2048M');

			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);
			$this->load->library('pdf');

			#
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # A4
				break;
				case "small":
					$pdf = $this->pdf->load('en-GB-x', array(210,148)); # small
				break;
				default:
					$pdf = $this->pdf->load('en-GB-x', array(106,155)); # legal
				break;
			}


			switch($this->input->get('o')){
				case "L":
					$o = "L";
				break;
				default:
					$o = "P";
				break;
			}

			$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('tmpl')==''){
				if($this->input->get('o')=='L'){
					$pdf->SetDocTemplate(BASEPATH.'../assets/cocard_pencaker.pdf',1);
				}else{
					$pdf->SetDocTemplate(BASEPATH.'../assets/cocard_pencaker.pdf',1);
				}
			}else{
				$pdf->SetDocTemplate(BASEPATH.'../assets/'.$this->input->get('tmpl').'.pdf',1);
			}

			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
            3, // margin_left
            3, // margin right
            40, // margin top
          3, // margin bottom
            0, // margin header
            5); // margin footer

			$pdf->SetDisplayMode('fullpage');
			$plus = "";
			if($this->input->get('issuer')==1){
				$plus = "Issued By : ".$this->session->userdata('username')."|".$this->session->userdata('nama');
			}
			if($plus!=''){
				$pdf->SetHTMLFooter('<i>'.$plus.'</i>&nbsp;&nbsp;&nbsp;&nbsp;'.'sipp.sampangkab.go.id'.'&nbsp;&nbsp;&nbsp;{PAGENO}&nbsp;&nbsp;&nbsp;'.date(DATE_RFC822));
			}
			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;

	//	}else{
	//		echo "<script> history.back() </script>";
	//	}
	}
	function pdflap(){
		ini_set('memory_limit','2048M');

			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);
			$this->load->library('pdf');

			#
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # A4
				break;
				case "small":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # small
				break;
				default:
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # legal
				break;
			}


			switch($this->input->get('o')){
				case "L":
					$o = "L";
				break;
				default:
					$o = "P";
				break;
			}

			$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('tmpl')==''){
				if($this->input->get('o')=='L'){
					$pdf->SetDocTemplate(BASEPATH.'../assets/template_lap.pdf',1);
				}else{
					$pdf->SetDocTemplate(BASEPATH.'../assets/template_lap.pdf',1);
				}
			}else{
				$pdf->SetDocTemplate(BASEPATH.'../assets/'.$this->input->get('tmpl').'.pdf',1);
			}

			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
           15, // margin_left
            15, // margin right
            45, // margin top
            30, // margin bottom
            15, // margin header
            15); // margin footer

			$pdf->SetDisplayMode('fullpage');
			$plus = "";
			if($this->input->get('issuer')==1){
				$plus = "Issued By : ".$this->session->userdata('username')."|".$this->session->userdata('nama');
			}
			if($plus!=''){
				$pdf->SetHTMLFooter('<i>'.$plus.'</i>&nbsp;&nbsp;&nbsp;&nbsp;'.'sipp.sampangkab.go.id'.'&nbsp;&nbsp;&nbsp;{PAGENO}&nbsp;&nbsp;&nbsp;'.date(DATE_RFC822));
			}
			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;
	}

	function pdfperusahaan(){
			ini_set('memory_limit','2048M');

			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);
			$this->load->library('pdf');

			#
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(297,420)); # A3
				break;
				case "small":
					$pdf = $this->pdf->load('en-GB-x', array(297,420)); # small
				break;
				default:
					$pdf = $this->pdf->load('en-GB-x', array(297,420)); # legal
				break;
			}


			switch($this->input->get('o')){
				case "L":
					$o = "L";
				break;
				default:
					$o = "P";
				break;
			}

			#$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('tmpl')==''){
				if($this->input->get('o')=='L'){
					$pdf->SetDocTemplate(BASEPATH.'../assets/template_disnaker_3.pdf',1);
				}else{
					$pdf->SetDocTemplate(BASEPATH.'../assets/template_disnaker_3.pdf',1);
				}
			}else{
			$pdf->SetDocTemplate(BASEPATH.'../assets/'.$this->input->get('tmpl').'.pdf',1);
			}

			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
            23, // margin_left
            15, // margin right
            10, // margin top
            15, // margin bottom
            0, // margin header
            5); // margin footer

			$pdf->SetDisplayMode('fullpage');
			$plus = "";
			if($this->input->get('issuer')==1){
				$plus = "Issued By : ".$this->session->userdata('username')."|".$this->session->userdata('nama');
			}
			if($plus!=''){
				$pdf->SetHTMLFooter('<i>'.$plus.'</i>&nbsp;&nbsp;&nbsp;&nbsp;'.'sipp.sampangkab.go.id'.'&nbsp;&nbsp;&nbsp;{PAGENO}&nbsp;&nbsp;&nbsp;'.date(DATE_RFC822));
			}
			#error_reporting(E_ALL);
			#$pdf->Image('https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=123456&choe=UTF-8',60,30,90,0,'PNG');

			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;
	}


	function pdfak1(){
	//	if($this->session->userdata('is_login_ngosis')){
			ini_set('memory_limit','2048M');

			$this->data['page'] = $this->uri->segment(4);
			$html = $this->load->view($this->modules.'/'.$this->data['page'].'_pdf',$this->data, true);
			$this->load->library('pdf');

			#
			switch($this->input->get('size')){
				case "A4":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # A4
				break;
				case "small":
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # small
				break;
				default:
					$pdf = $this->pdf->load('en-GB-x', array(210,297)); # legal
				break;
			}


			switch($this->input->get('o')){
				case "P":
					$o = "P";
				break;
				default:
					$o = "L";
				break;
			}

			#$pdf->showWatermarkImage = true;
			$pdf->SetImportUse();
			if($this->input->get('tmpl')==''){
				if($this->input->get('o')=='L'){
					$pdf->SetDocTemplate(BASEPATH.'../assets/ak1.pdf',0);
				}else{
					$pdf->SetDocTemplate(BASEPATH.'../assets/ak1.pdf',0);
				}
			}else{
				$pdf->SetDocTemplate(BASEPATH.'../assets/'.$this->input->get('tmpl').'.pdf',0);
			}

			$pdf->AddPage($o, // L - landscape, P - portrait
            '', '', '', '',
            5, // margin_left
            5, // margin right
            10, // margin top
            0, // margin bottom
            0, // margin header
            0); // margin footer

			$pdf->SetDisplayMode('fullpage');
			$plus = "";
			if($this->input->get('issuer')==1){
				$plus = "Issued By : ".$this->session->userdata('username')."|".$this->session->userdata('nama');
			}
			if($plus!=''){
				$pdf->SetHTMLFooter('<i>'.$plus.'</i>&nbsp;&nbsp;&nbsp;&nbsp;'.'bogorcareercenter.bogorkab.go.id'.'&nbsp;&nbsp;&nbsp;{PAGENO}&nbsp;&nbsp;&nbsp;'.date(DATE_RFC822));
			}
			$pdf->WriteHTML($html);
			$pdf->Output();
			exit;

	//	}else{
	//		echo "<script> history.back() </script>";
	//	}
	}

	function do_uploadphoto()
	{

		$file1 = $this->input->post('username');
		$file2 = explode(".",$_FILES['fileupload']['name']);
		$ext = $file2[count($file2)-1];
		$md5 = md5_file($_FILES['fileupload']['tmp_name']);

		$filename = $file1."_".$md5."-".date('Ymdhis').".".$ext;
		$filename_thumb = $file1."_".$md5."-".date('Ymdhis')."_thumb.".$ext;
		$filename_resize =  $file1."_resize_".$md5."-".date('Ymdhis').".".$ext;
		$config['upload_path'] = FCPATH.'/upliddir/';
		$config['allowed_types'] = 'jpg';
		$config['max_size']	= '2000000';
		$config['file_name'] = $filename;
		if(!file_exists($config['upload_path'].$filename)){
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('fileupload'))
			{
				$error = array('error' => $this->upload->display_errors());

				echo json_encode($error);
			}
			else
			{
				error_reporting(E_ALL);

				$data_upload = $this->upload->data();

				$upload['name'] 		= $data_upload['file_name'];
				$upload['name_resize'] 	= $filename_resize;
				$upload['name_thumb'] 	= $filename_thumb;
				$upload['size'] 		= $data_upload['file_size'];

				$upload['type'] = $data_upload['file_type'];
				$upload['url'] 	= base_url()."upliddir/".$data_upload['file_name'];
				$upload['thumbnailUrl'] = base_url()."upliddir/".$filename_thumb;
				$upload['deleteUrl'] = base_url()."dokumen/page/delUploaddoc/?filename=".$filename;
				$upload['deleteType'] = "DELETE";

				# resize image
				$config['maintain_ratio'] = TRUE;
				if($data_upload['image_width']>600){
					$config['width']	 = 600;
				}else{
					$config['width']	 = $data_upload['image_width'];
				}
				if($data_upload['image_width']>600){
					$config['height']	= 600;
				}else{
					$config['height']	= $data_upload['image_height'];
				}

				$config['new_image'] = BASEPATH."../upliddir/".$filename_resize;
				$config['source_image']	= BASEPATH."../upliddir/".$filename;
				$this->load->library('image_lib');
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				if ( ! $this->image_lib->resize())
				{
					echo $this->image_lib->display_errors();
				}

				# set thumbnail config
				$this->image_lib->clear();

				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = TRUE;
				$config['width']	 = 250;
				$config['height']	= 203;
				$config['new_image'] = BASEPATH."../upliddir/".$filename_thumb;
				$config['source_image']	= BASEPATH."../upliddir/".$filename;
				$this->load->library('image_lib');
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				if ( ! $this->image_lib->resize())
				{
					echo $this->image_lib->display_errors();
				}

				$data = array('files' => array($upload));
				if(file_exists(BASEPATH."../upliddir/".$filename)){
					unlink(BASEPATH."../upliddir/".$filename);
				}
				if(file_exists(BASEPATH."../upliddir/".$filename_resize)){
					rename(BASEPATH."../upliddir/".$filename_resize,BASEPATH."../upliddir/".$filename);
				}

				echo json_encode($data);
			}
		}else{
			$data = array('error'=>array("text"=>"File duplicate"));
			echo json_encode($data);
		}


	}		function do_upload_scanfile(){		$path_parts = pathinfo($_FILES["scanfile"]["name"]);		$extension = $path_parts['extension'];				$config['upload_path'] = BASEPATH.'../upliddir/';		$config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';		$config['max_size']	= '5000000';		$config['file_name'] = $this->input->post('filename').".".$extension;		$this->load->library('upload', $config);		if ( ! $this->upload->do_upload('scanfile'))		{			$error = array('error' => $this->upload->display_errors());			echo json_encode($error);		}		else		{			$data = array('upload_data' => $this->upload->data());			echo json_encode($data);		}			}



}
