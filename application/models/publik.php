<?php
class publik extends CI_Model {

   function __construct()
    {
        parent::__construct();
    }
   
   function useragent(){
		if ($this->agent->is_browser())
		{
			$agent = $this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
			$agent = $this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
			$agent = $this->agent->mobile();
		}
		else
		{
			$agent = 'Unidentified User Agent';
		}

		return $agent.$this->agent->platform();
   }
   
   function menu($id=0){
		$data['idparent'] = $id;
		$this->db->order_by("urut");
		$rs = $this->db->get_where("a_menu",$data);
		foreach($rs->result() as $item){
			$data2['idparent'] = $item->idmenu;
			$rs2 = $this->db->get_where("a_menu",$data2);
			if($rs2->num_rows()>0){
				echo "<li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">".$item->menu." <b class=\"caret\"></b></a>";
				echo "\n<ul class=\"dropdown-menu\">";
				$this->menu($item->idmenu);
				echo "</ul>\n";
			}else{
				if($item->jenlink!='Url'){
					$url = base_url()."sites/page/".strtolower($item->jenlink)."/".$item->idmenu;
				}else{
					$url = $item->url;
				}
				echo "<li><a href=\"".$url."\">".$item->menu."</a>";
			}
			echo "</li>\n";
		}
   }		
   
    
	
	function listBulan($idselect = 'bln',$selected = '', $css = '')
	{
		$month2[1] = "Januari";
		$month2[2] = "Februari";
		$month2[3] = "Maret";
		$month2[4] = "April";
		$month2[5] = "Mei";
		$month2[6] = "Juni";
		$month2[7] = "Juli";
		$month2[8] = "Agustus";
		$month2[9] = "September";
		$month2[10] = "Oktober";
		$month2[11] = "November";
		$month2[12] = "Desember";
		$print = "\n<!-- List Bulan -->\n<select name=$idselect id=$idselect style=\"width:150px\">\n";
		$now = 1;
		for ($i = 1; $i <= 12; $i++) {
			$bulan = $month2[$i];
			if (strlen($i) == 1) {
				$i = "0" . $i;
			}
			if ($now == $selected) {
				$print .= "<option value=$now selected>$now | $bulan</option>\n";
			} else {
				$print .= "<option value=$i>$i | $bulan</option>\n";
			}
			$now = 1;
			$now = $now + $i;
		}
		$print .= "</select>\n<!-- List Bulan -->\n";
		print $print;
	}
	
	
}
?>